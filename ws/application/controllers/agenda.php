<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Agenda extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('agenda_model');
    }

	function exportar_get()
	{
		$dados = $this->agenda_model->exportar_agenda($this->input->get('id'), $this->input->get('pacote'), $this->input->get('id_usuario'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar os Compromissos da sua agenda!'), 404);
        }
	}
	
	function tabela_get()
	{
		$dados = $this->agenda_model->exportar_agenda($this->get('id'), $this->input->get('pacote'), $this->input->get('id_usuario'), TRUE);
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar os Compromissos da sua agenda!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->agenda_model->retornar_total($this->input->get('id'), $this->input->get('id_usuario'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Compromissos da sua agenda!'), 404);
        }
	}
	
}