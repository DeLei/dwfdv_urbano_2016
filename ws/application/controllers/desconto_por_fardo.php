<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Desconto_por_fardo extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('desconto_por_fardo_model');
    }

	function exportar_get()
	{
		$dados = $this->desconto_por_fardo_model->exportar_desconto_por_fardo($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'));
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar desconto por categoria e quantidade fardos!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->desconto_por_fardo_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de desconto por categoria e quantidade fardos!'), 404);
        }
	}
	
}