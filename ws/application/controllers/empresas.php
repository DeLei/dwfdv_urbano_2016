<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Empresas extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
    }

	function exportar_get()
	{
		$dados = $this->obter_empresas();
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Empresas!'), 404);
        }
	}
	
	function total_get()
	{
	
		$total['total'] = 1;
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Empresas!'), 404);
        }
	}
	
	function obter_empresas()
	{
	
		if($this->config->item('euf') == TRUE && $this->config->item('empresas_euf') != FALSE)
		{
			$dados 	= $this->config->item('empresas_euf');
		}
		else
		{
			$dados 	= $this->config->item('empresas');
		}
	
		$empresas = array();
		
		if($dados)
		{
			foreach($dados as $indice => $valor)
			{
				$empresas[] = array('codigo' => (string) $indice, 'descricao' => $valor);
			}
		}
		
		return $empresas;
		
	}
	
}