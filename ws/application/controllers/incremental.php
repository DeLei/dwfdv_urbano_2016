<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Incremental extends REST_Controller
{
	
	/**
	* Metódo:		gerar
	* 
	* Descrição:	Função Utilizada para criar os pacotes json por representante
	* 
	* Data:			27/02/2013
	* Modificação:	27/02/2013
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function gerar_post()
	{
		$this->load->model('sincronizacoes_model');	
		$this->sincronizacoes_model->salvar_sincronizacao($this->input->post('modulo'), json_encode($_POST), $this->input->post('id_usuario'), $this->input->post('codigo_representante'));
		
		$this->load->model('autenticacao_model');
		
		
		if($this->autenticacao_model->validar_usuario()) {
		
			$codigo_representante = $this->input->post('codigo_representante');
			$modulo = $this->input->post('modulo');
			$ultima_sincronizacao = $this->input->post('ultima_sincronizacao');
			$id_usario = $this->input->post('id_usuario');
						
			$incremental = (strtoupper($this->input->post('incremental')) == 'TRUE')?true:false;
			$this->criar_diretorio_pacote('incremental');
			
			$codigos_representantes = $this->obter_codigos_representantes($codigo_representante);
			
			if($codigos_representantes)
			{
				foreach($codigos_representantes as $representante)
				{
					//------------------------------------------------------
					//ob_start para gravar o conteudo em arquivos txt (Logs)
					ob_start();
					
					if($representante['key'])
					{
						
					
						$this->key_representante = $representante['key'];
						
						// Passando a Key do representanto no header do file_get_contents
						$options = array(
							'http'=>array(
								'method'=> "GET",
								'header'=> "DEVELOPWEB-KEY-APP: " . $representante['key'] . "\r\n",
								'timeout'	=>	600 // 10 minuto
							)
						);
						
						$context = stream_context_create($options);
						
						//Criar diretorio para arquivos json com o codigo do representante
						$this->criar_diretorio_pacote($representante['codigo'].'/'.$id_usario, FALSE,TRUE);
						$this->criar_diretorio_pacote('temp/'.$representante['codigo'].'/'.$id_usario, FALSE,TRUE);
						
						// Verifica se esta na hora de realizar a atualizacao do pacote
						
						$parametros = array(
							'id_usuario' => $representante['id'] ,
							'codigo_representante' => $representante['codigo'],
							'gerar_pacote' => 1,
							'incremental' => $incremental,
							'ultima_sincronizacao' => $ultima_sincronizacao
						);
						
						//Obter total de pacotes
						$json_url_total 	= base_url() . $modulo . '/total?'.(http_build_query($parametros));
						$json_total 		= file_get_contents($json_url_total, 0, $context);
						$total 				= json_decode($json_total);
						//debug_pre($json_total);
						//Setando Total de Pacotes
						
					//debug_pre(base_url() . $modulo . '/total?'.(http_build_query($parametros)));
						
						$total->total = ($total->total > 0 ? $total->total : 1);
						$this->total_dos_pacotes[$modulo] = $total->total;
						
						
						
						$diretorio =  DIR_INCREMENTAL .'temp/'. $representante['codigo'].'/'.$id_usario;
						
						$nome_arquivo = $modulo.'_total.json';
						
						$this->gerar_arquivo($diretorio, $nome_arquivo, json_encode($total));
						
						//die('teste');
						
						//Paginas de pacotes
						for($p = (( $total->total >= 1 )? 1 : 0); $p <= $total->total; $p++)
						{
							
							$parametros = array(
						
								'id_usuario' => $representante['id'] ,
								'codigo_representante' => $representante['codigo'],
								'pacote' => $p,
								'gerar_pacote' => 1,
								'total_pacotes' => $total->total,
								'incremental' => $incremental,
								'ultima_sincronizacao' => $ultima_sincronizacao
						
							);
							
							
							$json_url_dados 	= base_url() . $modulo . '/exportar?'.(http_build_query($parametros));
							$json_dados 		= file_get_contents($json_url_dados, 0, $context);
							
							
							
							$nome_arquivo = $nome_arquivo = $modulo.'_'.$p.'.json';
								
								
						
								
							if(!$json_dados)
							{
								echo ' # ERRO - Não foi possivel gerar o json para o pacote ' . $pacote . ' e representante ' . $representante['codigo'] ."\n\n"; //Usado para ser gravado no log
							}
							else
							{
							
								
							
								$this->gerar_arquivo($diretorio, $nome_arquivo, $json_dados);
							}
						}
						
						// Gerando ZIP
						$resultado  = $this->gerar_zip($representante['codigo'], $id_usario, $modulo);
						
						$this->response( $resultado, 200);
					}
					else
					{
						echo ' # ERRO - Nao foi possivel encontrado uma key para o representante ' . $representante['codigo'] . "\n\n"; //Usado para ser gravado no log
					}
				}
			}
			
		} else {
		
			$retorno =   array( 
				'status' => 2,
				'mensagem' => 'Usuário inválido.'
			);
		
			$this->response( $resultado, 200);
			
		}
	
	}
	
	
	
	function gerar_arquivo($diretorio, $nome_arquivo, $dados){
		$escrita = 'w'; // Abre somente para escrita; coloca o ponteiro do arquivo no começo do arquivo e reduz o comprimento do arquivo para zero. Se o arquivo não existir, tenta criá-lo. 
		

		
		
		write_file( $diretorio . '/' . $nome_arquivo, $dados, $escrita);
		
	}

		
		
	/**
	* Metódo:		gerar_zip
	* 
	* Descrição:	Função Utilizada para zipar os arquivos json por representante
	* 
	* Data:			05/09/2013
	* Modificação:	05/09/2013
	* 
	* @access		public
	* @param		string 		$diretorio	diretorio a ser criado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function gerar_zip($codigo_representante, $id_usuario, $modulo)
	{
		$retorno =   array( 
			'status' => 1,
			'mensagem' => 'Solicitação pronta para Download'
		);
		
		if(extension_loaded('zip'))
		{
			$zip = new ZipArchive();
					
			$diretorio_temp 	= str_replace('/','\\',DIR_INCREMENTAL) .'temp\\'.$codigo_representante.'\\'.$id_usuario;
			$diretorio_pacotes 	= str_replace('/','\\',DIR_INCREMENTAL) .$codigo_representante.'\\'.$id_usuario.'\\'.$modulo;
						
			$zip_name = $diretorio_pacotes . '.zip';
			
			if ($zip->open($zip_name, ZIPARCHIVE::OVERWRITE) !== TRUE) {
				
				$retorno['status'] = 2;
				$retorno['mensagem'] = 'Não foi possível gerar o arquivo ZIP';
		
			}
			
			//Total
			$zip->addFile($diretorio_temp . '\\' . $modulo . '_total.json', $modulo . '_total.json'); // Adding files into zip
			
			for($n_pacote = 1; $n_pacote <= $this->total_dos_pacotes[$modulo]; $n_pacote++)
			{
			
				$zip->addFile($diretorio_temp . '\\' . $modulo . '_' . $n_pacote . '.json', $modulo . '_' . $n_pacote . '.json'); // Adding files into zip
			}
			
			
			if($modulo == 'catalogos'){
				//Obtendo Anexos
				$anexos= $this->db
					->from('anexos')
					->where('ativo', '1') // Opções: 1 Ativo - 2 Inativo	
					->where('modulo', $modulo) // Opções: 1 Ativo - 2 Inativo	
					->get()->result();
				
				$dir = RAIZ . 'dwpdr\\uploads\\'.$modulo.'\\';
				
				
				if(count($anexos)> 0){
					
					foreach($anexos as $anexo){						
									
						$zip->addFile($dir . $anexo->url, $anexo->url ); // Adding files				
					}
													
				}
								
			}
			//die();
			$zip->close();
			//$this->gerar_anexos_zip('catalogos');
			
			return $retorno;
		}
		else
		{
			$retorno['status'] = 3;
			$retorno['mensagem'] = 'Extensão <zip> não foi habilitada.';
			
			return $retorno;
		}
	}
		
	function criar_diretorio_pacote($diretorio, $temp = FALSE, $incremental = FALSE)
	{
		$dir = DIR_PACOTES;
		
		if($temp)
		{
			$dir = DIR_TEMP;
		}

		if($incremental){
			$dir = DIR_INCREMENTAL;
		}
		
		mkdir($dir . $diretorio, 0777,TRUE);
		chmod($dir . $diretorio, 0777);
	}
	
	function obter_codigos_representantes($codigo_representante = NULL)
	{
		$this->load->model('representantes_model');
		
		return $this->representantes_model->obter_codigos_representantes($codigo_representante);
	}
		
}