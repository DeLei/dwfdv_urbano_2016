<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Autenticacao extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('autenticacao_model');
		$this->load->model('representantes_model');
    }

	// http://localhost/ws_rest/trunk/autenticacao/autenticar?usuario=rep1&senha=1234
	function autenticar_get()
	{	
	
		
		$usuario = $this->autenticacao_model->autenticar($this->get('key'));
		
		if($usuario)
        {
			
			$this->session->set_userdata('usuario', $usuario);
			
			$representante = $this->representantes_model->exportar_representante($usuario->codigo);
			
			$this->session->set_userdata('representante', $representante);
			
			$this->response(array_merge((array)$usuario, $representante), 200);
        }
        else
        {
			
            $this->response(array('error' => 'Não foi possível buscar usuário!'), 200);
        }
	}
	
	function localizacao_get()
	{
		//Posição do Aparelho no GPS
		$detalhes = array(	'latitude'			=> $this->get('latitude'),
							'longitude'			=> $this->get('longitude'),
							'accuracy'			=> $this->get('accuracy'), 
							'altitude' 			=> $this->get('altitude'),
							'altitudeAccuracy' 	=> $this->get('altitudeAccuracy'),
							'heading' 			=> $this->get('heading'),
							'speed' 			=> $this->get('speed'),
							'timestamp' 		=> $this->get('timestamp'));
		
		//Salvar localização
		$local = $this->autenticacao_model->salvar_localizacao(	$this->get('id_usuario'), 
																$this->get('codigo_representante'), 
																$this->get('key'), 
																$this->get('latitude'), 
																$this->get('longitude'),
																$detalhes);
		if($local)
		{
			$this->response(array(''), 200);
		}else{
			$this->response(array('error' => 'Não foi possível salvar a localização do usuário!'), 200);
		}
	}
	
	function sair_get()
	{
		$this->session->sess_destroy();

	}
	
	
}