<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Zonas_setor extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('zonas_setor_model');
    }

	function exportar_get()
	{
		$dados = $this->zonas_setor_model->exportar_zonas($this->input->get('id'), $this->input->get('pacote'),$this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar as Zonas por Setor!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->zonas_setor_model->retornar_total($this->input->get('id'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar as Zonas por Setor!'), 404);
        }
	}
	
}