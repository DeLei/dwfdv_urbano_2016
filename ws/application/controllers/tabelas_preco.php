<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Tabelas_preco extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('tabelas_preco_model');
    }

	function exportar_get()
	{
		$tabelas_preco = $this->tabelas_preco_model->exportar_tabelas_preco($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($tabelas_preco)
        {
            $this->response($tabelas_preco, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Tabelas de preço!'), 404);
        }
	}
	
	
	function total_get()
	{
		$total['total'] = $this->tabelas_preco_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Tabelas de preço!'), 404);
        }
	}
	
}