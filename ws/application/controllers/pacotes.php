<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pacotes extends CI_Controller
{
	var $pacotes 			= array();
	var $pacotes_unicos 	= array();
	var $key_representante 	= NULL;
	
	var $total_dos_pacotes			= array();
	var $total_dos_pacotes_unicos	= array();

	function __construct()
    {
		parent::__construct();
		
		$this->pacotes 			= $this->config->item('pacotes');
		$this->pacotes_unicos 	= $this->config->item('pacotes_unicos');
    }

	/**
	* Metódo:		gerar
	* 
	* Descrição:	Função Utilizada para criar os pacotes json por representante
	* 
	* Data:			27/02/2013
	* Modificação:	27/02/2013
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function gerar($codigo_representante = NULL)
	{
		echo 'Iniciando: '.date('d/m/Y H:i:s') . "\n"; //Log para exibir no console
			
		// Exemplos para TESTE
		//$codigos_representantes = array('000005');
		//$keys = array('000005' => '17d11195eda3f9d2e60e3b6e586dce3e');
		
		//Criar diretorio TEMP para arquivos json com o codigo do representante
		$this->criar_diretorio_pacote('temp');
		
		$codigos_representantes = $this->obter_codigos_representantes($codigo_representante);
		
	
		if($codigos_representantes)
		{
			foreach($codigos_representantes as $representante)
			{
				//------------------------------------------------------
				//ob_start para gravar o conteudo em arquivos txt (Logs)
				ob_start();
			
				if($representante['key'])
				{
					$this->key_representante = $representante['key'];
				
					// Passando a Key do representanto no header do file_get_contents
					$options = array(
						'http'=>array(
							'method'=> "GET",
							'header'=> "DEVELOPWEB-KEY-APP: " . $representante['key'] . "\r\n",
							'timeout'	=>	600 // 10 minuto
						)
					);
					$context = stream_context_create($options);
				
					//Criar diretorio para arquivos json com o codigo do representante
					$this->criar_diretorio_pacote($representante['codigo'], TRUE);
					
					//Pacotes
					foreach($this->pacotes as $pacote => $time_atualizacao)
					{
						
						// Verifica se esta na hora de realizar a atualizacao do pacote
						if($this->verificar_atualizacao($representante['codigo'], $pacote))
						{
							echo 'Iniciando Pacote ' . $pacote . ': '.date('d/m/Y H:i:s') . "\n"; //Usado para ser gravado no log
						
							//Obter total de pacotes
							$json_url_total 	= base_url() . $pacote . '/total?id_usuario='.$representante['id'].'&codigo_representante='.$representante['codigo'].'&gerar_pacote=1';
							$json_total 		= file_get_contents($json_url_total, 0, $context);
							$total 				= json_decode($json_total);
							
							
							//Setando Total de Pacotes
							$this->total_dos_pacotes[$pacote] = ($total->total > 0 ? $total->total : 1);
							//$this->total_dos_pacotes[$pacote] = (isset($total->total) ? $total->total : 1);
							
							
							//Paginas de pacotes
							
							for($p = ($total->total >= 1 ? 1 : 0); $p <= $total->total; $p++)
							{
								
								$json_url_dados 	= base_url() . $pacote . '/exportar?id_usuario='.$representante['id'].'&codigo_representante='.$representante['codigo'].'&pacote='.$p. '&gerar_pacote=1&total_pacotes=' . $total->total;
								$json_dados 		= file_get_contents($json_url_dados, 0, $context);
						
								if(!$json_dados)
								{
									echo ' # ERRO - Não foi possivel gerar o json para o pacote ' . $pacote . ' e representante ' . $representante['codigo'] ."\n\n"; //Usado para ser gravado no log
								}
								else
								{
									echo ' - Pacote ' . $pacote . ' - ' . $p . ' para o representante ' . $representante['codigo']  . ' foi concluido!'."\n"; //Usado para ser gravado no log
								}
								
							}
														
							echo 'Concluído Pacote ' . $pacote . ':  ' . date('d/m/Y H:i:s') . "\n\n"; //Usado para ser gravado no log
						}
						else
						{
							//---------------
							//--------------------------------------------------
							//Obter total de pacotes existentes
							$json_url_total 	= base_url() . $pacote . '/total?id_usuario='.$representante['id'].'&codigo_representante='.$representante['codigo'];
							$json_total 		= file_get_contents($json_url_total, 0, $context);
							$total 				= json_decode($json_total);
							
							//Setando Total de Pacotes
							$this->total_dos_pacotes[$pacote] = ($total->total > 0 ? $total->total : 1);							
							//--------------------------------------------------
							//---------------
						
							echo ' # ATENCAO - Pacote  ' . $pacote . ' não foi atualizado porque esta programado para atualizar as ' . $this->verificar_atualizacao($representante['codigo'], $pacote, TRUE) . ' (Tentativa de atualizacao as ' . date('d/m/Y H:i:s'). ')' . "\n\n";
						}
						
					}
			
					
				}
				else
				{
					echo ' # ERRO - Nao foi possivel encontrado uma key para o representante ' . $representante['codigo'] . "\n\n"; //Usado para ser gravado no log
				}	
				
				// Gerando ZIP				
				$this->gerar_zip($representante['codigo']);
				
				
				// Salvando Logs
				$logs = ob_get_contents();
				
				//Criar diretorio do representante em logs
				$this->criar_diretorio_logs($representante['codigo']);
				
				write_file(APPPATH . 'logs/pacotes/' . $representante['codigo'] . '/pacotes_' . date('Ymd_His') . '.txt', $logs, 'w');

				
				ob_end_clean();
				//------------------------------------------------------
				
			}
			
		}
		
		
		
		//Gerando Pacotes Unicos
		$this->gerar_pacotes_unicos();
		
		echo 'Concluido: '  .date('d/m/Y H:i:s') . "\n";//Usado para exibir no console
	
		
		die('Pacote de dados gerado com sucesso');
	}
	
	/**
	* Metódo:		gerar_zip
	* 
	* Descrição:	Função Utilizada para zipar os arquivos json por representante
	* 
	* Data:			05/09/2013
	* Modificação:	05/09/2013
	* 
	* @access		public
	* @param		string 		$diretorio	diretorio a ser criado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function gerar_zip($diretorio)
	{

		if(extension_loaded('zip'))
		{
		
			$zip = new ZipArchive();
			
			$diretorio_temp 	= str_replace('/','\\',DIR_TEMP) . $diretorio;
			$diretorio_pacotes 	= str_replace('/','\\',DIR_PACOTES) . $diretorio;
			
			$zip_name = $diretorio_pacotes . '.zip';
			
			if ($zip->open($zip_name, ZIPARCHIVE::OVERWRITE) !== TRUE) {
				echo "Nao foi possivel abrir <$zip_name>\n\n";
			}
			
			echo "Adicionando " . $diretorio . " no .zip\n\n";
			
			if($diretorio == 'unicos')
			{
				
				foreach($this->pacotes_unicos as $pacote => $time_atualizacao)
				{
					if(isset($this->total_dos_pacotes_unicos[$pacote]) && $this->total_dos_pacotes_unicos[$pacote]  )  {
						echo "Adicionando pacote <" . $pacote . "_total.json> no .zip\n\n";
						
						//Total
						$zip->addFile($diretorio_temp . '\\' . $pacote . '_total.json', $pacote . '_total.json'); // Adding files into zip
					
						echo "Total de pacotes para o modulo " . $pacote . " <" . $this->total_dos_pacotes_unicos[$pacote] . "> \n\n";
						
						//Setando Total de Pacotes
						for($n_pacote = 1; $n_pacote <= $this->total_dos_pacotes_unicos[$pacote]; $n_pacote++)
						{
							echo "Adicionando pacote <" . $pacote . "_" . $n_pacote . ".json> no .zip\n\n";
							
							$zip->addFile($diretorio_temp . '\\' . $pacote . '_' . $n_pacote . '.json', $pacote . '_' . $n_pacote . '.json'); // Adding files into zip
						}
					}
				}
				
				//Produtos em destaque
				$this->load->model('produtos_model');
				$produtos_destaque = $this->produtos_model->obter_produtos_destaque();
				$dir = $_SERVER['DOCUMENT_ROOT'] . '/dwpdr/uploads/produtos_destaque/';
				//$dir = RAIZ . 'dwpdr\uploads\produtos_destaque\\';
				foreach($produtos_destaque as $produto)
				{
				
						echo "Adicionando produto em destaque <" . trim($produto->codigo) . ".jpeg> no .zip\n\n";
						
						$file = trim($produto->codigo) . '.jpeg';
						
						if(@getimagesize($dir . $file))
						{
							$zip->addFile($dir . $file, $file); // Adding files
						}
					
				}
				$zip->addFile($dir . 'sem_imagem.jpeg', 'sem_imagem.jpeg'); // Adding files
											
			}
			else
			{
			
				foreach($this->pacotes as $pacote => $time_atualizacao)
				{
					if(isset($this->total_dos_pacotes[$pacote]) && $this->total_dos_pacotes[$pacote]  )  {
						echo "Adicionando pacote <" . $pacote . "_total.json> no .zip\n\n";
						debug_pre("Adicionando pacote <" . $pacote . "_total.json> no .zip\n\n",false);
						//Total
						$zip->addFile($diretorio_temp . '\\' . $pacote . '_total.json', $pacote . '_total.json'); // Adding files into zip
					
						echo "Total de pacotes para o modulo " . $pacote . " <" . $this->total_dos_pacotes[$pacote] . "> \n\n";
						debug_pre("Total de pacotes para o modulo " . $pacote . " <" . $this->total_dos_pacotes[$pacote] . "> \n\n",false);	
						//Setando Total de Pacotes
						for($n_pacote = 1; $n_pacote <= $this->total_dos_pacotes[$pacote]; $n_pacote++)
						{
							echo "Adicionando pacote <" . $pacote . "_" . $n_pacote . ".json> no .zip\n\n";
							debug_pre("Adicionando pacote <" . $pacote . "_" . $n_pacote . ".json> no .zip\n\n",false);
							$zip->addFile($diretorio_temp . '\\' . $pacote . '_' . $n_pacote . '.json', $pacote . '_' . $n_pacote . '.json'); // Adding files into zip
						}
					}
				}
			}
			
			$zip->close();
				
			$this->gerar_anexos_zip('catalogos');
			
			echo "ZIP: <$zip_name> criado.\n\n";
			
		}
		else
		{
			echo "Extencao <zip> não foi habilitada.\n\n";
		}
	}
	
	
	
		/**
	* Metódo:		gerar_zip
	* 
	* Descrição:	Função Utilizada para zipar os arquivos json por representante
	* 
	* Data:			05/09/2013
	* Modificação:	05/09/2013
	* 
	* @access		public
	* @param		string 		$diretorio	diretorio a ser criado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function gerar_anexos_zip($pacote = 'catalogos')
	{
		
		if(extension_loaded('zip'))
		{							
			$time_atualizacao = 	$this->pacotes_unicos[$pacote];
				
					$anexos= $this->db
					->from('anexos')
					->where('ativo', '1') // Opções: 1 Ativo - 2 Inativo	
					->where('modulo', $pacote) // Opções: 1 Ativo - 2 Inativo	
					->get()->result();
				
			$dir = RAIZ . 'dwpdr\\uploads\\'.$pacote.'\\';
			
			$zip = new ZipArchive();
				
			$diretorio_temp 	= str_replace('/','\\',DIR_TEMP) . $pacote;
			$diretorio_pacotes 	= str_replace('/','\\',DIR_PACOTES) . $pacote;
					
			$zip_name = $diretorio_pacotes . '.zip';
			
			if(count($anexos)> 0){
				if ($zip->open($zip_name, ZIPARCHIVE::OVERWRITE) !== TRUE) {
						echo "Nao foi possivel abrir <$zip_name>\n\n";
				}
				foreach($anexos as $anexo){						
					echo "Adicionando " . $anexo->url . " no .zip\n\n";					
					$zip->addFile($dir . $anexo->url, $anexo->url ); // Adding files				
				}
				
			
				echo "ZIP: <$pacote> criado.\n\n";
			}
			
			$zip->close();
		
		}
		else
		{
			echo "Extencao <zip> não foi habilitada.\n\n";
		}
	}
	
	
	
	/**
	* Metódo:		gerar_pacotes_unicos
	* 
	* Descrição:	Função Utilizada para criar os pacotes unicos em json (para todos os representante)
	* 
	* Data:			28/02/2013
	* Modificação:	28/02/2013
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function gerar_pacotes_unicos()
	{
		//------------------------------------------------------
		//ob_start para gravar o conteudo em arquivos txt (Logs)
		ob_start();
	
		// Exemplo para TESTE
		//$this->key_representante = '17d11195eda3f9d2e60e3b6e586dce3e';
	
		if($this->key_representante)
		{
			// Passando a Key do representanto no header do file_get_contents
			$options = array(
				'http'=>array(
					'method'=> "GET",
					'header'=> "DEVELOPWEB-KEY-APP: " . $this->key_representante . "\r\n"
				)
			);
			$context = stream_context_create($options);
		
			//Criar diretorio para arquivos json chamado "unicos"
			$this->criar_diretorio_pacote('unicos', TRUE);
		
			//Pacotes
			foreach($this->pacotes_unicos as $pacote => $time_atualizacao)
			{
				// Verifica se esta na hora de realizar a atualizacao do pacote
				if($this->verificar_atualizacao('unicos', $pacote))
				{
					echo 'Iniciando Pacote ' . $pacote . ': '.date('d/m/Y H:i:s') . "\n"; //Usado para ser gravado no log
				
					//Obter total de pacotes
					$json_url_total 	= base_url() . $pacote . '/total?gerar_pacote=1';
					$json_total 		= file_get_contents($json_url_total, 0, $context);
					$total 				= json_decode($json_total);

					
					//Setando Total de Pacotes
					$this->total_dos_pacotes_unicos[$pacote] = ($total->total > 0 ? $total->total : 1);
					
					
					
					//Paginas de pacotes
					for($p = ($total->total >= 1 ? 1 : 0); $p <= $total->total; $p++)
					{
						$json_url_dados 	= base_url() . $pacote . '/exportar?pacote=' . $p . '&gerar_pacote=1&total_pacotes=' . $total->total;
						$json_dados 		= file_get_contents($json_url_dados, 0, $context);
						
						if(!$json_dados)
						{
							echo ' # ERRO - Não foi possivel gerar o json para o pacote ' . $pacote . ' (Pacotes Unicos) ' ."\n\n"; //Usado para ser gravado no log
						}
						else
						{
							echo ' - Pacote ' . $pacote . ' - ' . $p . ' (Pacotes Unicos) foi concluido!'."\n"; //Usado para ser gravado no log
						}
						
					}
					
					
					echo 'Concluído Pacote ' . $pacote . ':  ' . date('d/m/Y H:i:s') . "\n\n"; //Usado para ser gravado no log
				}
				else
				{
				
					//---------------
					//--------------------------------------------------
					//Obter total de pacotes existentes
					$json_url_total 	= base_url() . $pacote . '/total';
					$json_total 		= file_get_contents($json_url_total, 0, $context);
					$total 				= json_decode($json_total);
					
					//Setando Total de Pacotes
					$this->total_dos_pacotes_unicos[$pacote] = ($total->total > 0 ? $total->total : 1);
					//--------------------------------------------------
					//---------------
				
				
					echo ' # ATENCAO - Pacote  ' . $pacote . ' não foi atualizado porque esta programado para atualizar as ' . $this->verificar_atualizacao('unicos', $pacote, TRUE) . ' (Tentativa de atualizacao as ' . date('d/m/Y H:i:s'). ')' . "\n\n";
				}
				
			}
		}
		else
		{
	
			echo ' # ERRO - Nao foi possivel encontrado uma key para os pacotes unicos ' . "\n\n"; //Usado para ser gravado no log
					
		}
		
		// Gerando ZIP				
		$this->gerar_zip('unicos');
		
		// Salvando Logs
		$logs = ob_get_contents();
		
		//Criar diretorio do representante em logs
		$this->criar_diretorio_logs('unicos');
		
		write_file(APPPATH . 'logs/pacotes/unicos/pacotes_unicos_' . date('Ymd_His') . '.txt', $logs, 'w');
		
		ob_end_clean();
		//------------------------------------------------------
	}
	
	/**
	* Metódo:		obter_codigos_representantes
	* 
	* Descrição:	Função Utilizada para obter codigos e keys dos representantes
	* 
	* Data:			27/02/2013
	* Modificação:	27/02/2013
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_codigos_representantes($codigo_representante = NULL)
	{
		$this->load->model('representantes_model');
		
		return $this->representantes_model->obter_codigos_representantes($codigo_representante);
	}
	
	/**
	* Metódo:		verificar_atualizacao
	* 
	* Descrição:	Função Utilizada para verificar quando foi a ultima atualização do pacote e atualizar o mesmo se estiver dentro da data de atualização
	* 
	* Data:			27/02/2013
	* Modificação:	27/02/2013
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function verificar_atualizacao($diretorio, $pacote, $retornar_datas = FALSE, $retornar_data_geracao = FALSE)
	{
		if($diretorio == 'unicos')
		{
			$pacotes = $this->pacotes_unicos;
		}
		else
		{
			$pacotes = $this->pacotes;
		}
		
		$url = DIR_TEMP . $diretorio . '/' . $pacote . '_total.json';
		
		if (file_exists($url)) 
		{
			$data_geracao = filemtime($url);
		
			$time_atualizacao = filemtime($url) + $pacotes[$pacote];
			
			if(!$retornar_datas && !$retornar_data_geracao )
			{
				if(time() >= $time_atualizacao)
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else if( !$retornar_data_geracao)
			{
				return date('d/m/Y H:i:s', $time_atualizacao);
			}else{
				return array(
					'data_geracao' => date('d/m/Y H:i:s', $data_geracao),
					'data_atualizacao' => date('d/m/Y H:i:s', $time_atualizacao)

				);
			}
		}
		else
		{
			$hora_atualizacao	= strtotime($this->config->item('hora_atualizacao'));
			$hora_atual 		= strtotime(date('H:i'));
			
			if(!$retornar_datas && !$retornar_data_geracao)
			{
				if($hora_atual >= $hora_atualizacao)
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else if(!$retornar_data_geracao)
			{
				return date('H:i:s', $hora_atualizacao);
			}else{
				return array(
					'data_geracao' => 'NULL',
					'data_atualizacao' => date('d/m/Y H:i:s', $hora_atualizacao)

				);
				
			}
		}
	}
	
	
	/**
	* Metódo:		criar_diretorio_pacote
	* 
	* Descrição:	Função Utilizada para criar o diretorio do representante, onde serão gravados os pacotes do mesmo
	* 
	* Data:			25/02/2013
	* Modificação:	25/02/2013
	* 
	* @access		public
	* @param		string 		$codigo = codigo do representante 	
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function criar_diretorio_pacote($diretorio, $temp = FALSE, $incremental = FALSE)
	{
		$dir = DIR_PACOTES;
		
		if($temp)
		{
			$dir = DIR_TEMP;
		}

		if($incremental){
			$dir = DIR_INCREMENTAL;
		}
		
		mkdir($dir . $diretorio, 0777,TRUE);
		chmod($dir . $diretorio, 0777);
	}
	
	/**
	* Metódo:		criar_diretorio_logs_pacote
	* 
	* Descrição:	Função Utilizada para criar o diretorio do representante, onde serão gravados os logs
	* 
	* Data:			25/02/2013
	* Modificação:	25/02/2013
	* 
	* @access		public
	* @param		string 		$codigo = codigo do representante 	
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function criar_diretorio_logs($diretorio)
	{
		mkdir(APPPATH . 'logs/pacotes/' . $diretorio, 0777,TRUE);
		chmod(APPPATH . 'logs/pacotes/' . $diretorio, 0777);
	}
	
	
	
	/**
	* Metódo:		obter_status_atualizacao
	* 
	* Descrição:	Função Utilizada para obter a data de geração e previsão de próxima geração.
	* 
	* Data:			05/09/2014
	* Modificação:	05/09/2014
	* 
	* @access		public
	* @param		string 		$codigo = codigo do representante 	
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_status_atualizacao($codigo_representante = null){
		$codigos_representantes = $this->obter_codigos_representantes($codigo_representante);
		//debug_pre($codigos_representantes );
		
		$dados = array();
		if($codigos_representantes)
		{
			foreach($codigos_representantes as $representante)
			{
				$dados[$representante['codigo']] = array();
				
				//Pacotes
				foreach($this->pacotes as $pacote => $time_atualizacao)
				{
					
					// Verifica se esta na hora de realizar a atualizacao do pacote
					$dados[$representante['codigo']][$pacote] = $this->verificar_atualizacao($representante['codigo'], $pacote,true,true);
				}
			}
			
		}
		return $dados;
	}
		
		
	function gerar_representante($codigo_representante){
		
			
		$codigos_representantes = $this->obter_codigos_representantes($codigo_representante);
		
		
		$this->load->view('gerar_pacote', 
							array(
								'representante' => json_encode($codigos_representantes[0]),
								'pacotes'	 => json_encode($this->pacotes),
								'pacotes_unicos' => json_encode($this->pacotes_unicos)
								
							)
						);


		
	}	
		
}


