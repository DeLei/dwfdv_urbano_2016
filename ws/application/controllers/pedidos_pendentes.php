<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Pedidos_pendentes extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('pedidos_pendentes_model');
    }
	
	
	function exportar_get()
	{
		$pedidos = $this->pedidos_pendentes_model->exportar_pedidos($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'), $this->input->get('incremental'),$this->input->get('ultima_sincronizacao'));
		
		if($pedidos)
        {
            $this->response($pedidos, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Pedidos Pendentes!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->pedidos_pendentes_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'), $this->input->get('incremental'),$this->input->get('ultima_sincronizacao'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Pedidos Pendentes!'), 404);
        }
	}
	
	
	function importar_post()
	{
		if($this->input->post('retorno') && $this->input->post('id_usuario'))
		{
			$retorno = $this->pedidos_pendentes_model->importar($this->input->post('retorno'), $this->input->post('id_usuario'), $this->input->post('codigo_representante'), $this->input->get('incremental'),$this->input->get('ultima_sincronizacao'));
			
			if($retorno)
			{
				$this->response($retorno, 200);
			}
			else
			{
				$this->response(array('error' => 'Não foi possível enviar Pedidos!'), 404);
			}
		}
		else
		{
			$this->response(array('error' => 'Não foi possível enviar Pedidos!'), 404);
		}
	}
	
	function importar_novo_post()
	{
		
		
		/*
		Bloqueio de pedido por região
		$id_usuario = $this->input->post('id_usuario');
		
		$usuario = $this->db->from('usuarios')->where('id', $id_usuario)->get()->row();
		
		if($usuario->gerente_representante == '000003') {
		
			$this->response(array('erro' => 1, 'mensagem' => 'Os pedidos para a região 000006 estão bloqueados!'), 200);
			
		} else {
		
			if($this->input->post('retorno') && $this->input->post('id_usuario'))
			{
				$retorno = $this->pedidos_pendentes_model->importar_novo($this->input->post('retorno'), $this->input->post('id_usuario'), $this->input->post('codigo_representante'));
				
				if($retorno)
				{
					$this->response($retorno, 200);
				}
				else
				{
					$this->response(array('error' => 'Não foi possível enviar Pedidos!'), 404);
				}
			}
			else
			{
				$this->response(array('error' => 'Não foi possível enviar Pedidos!'), 404);
			}
		}
		*/
		
		$this->load->model('autenticacao_model');
		if($this->autenticacao_model->validar_usuario()) {		
			if($this->input->post('retorno') && $this->input->post('id_usuario'))
			{
				$retorno = $this->pedidos_pendentes_model->importar_novo($this->input->post('retorno'), $this->input->post('id_usuario'), $this->input->post('codigo_representante'));
				
				if($retorno)
				{
					$this->response($retorno, 200);
				}
				else
				{
					$this->response(array('error' => 'Não foi possível enviar Pedidos!'), 404);
				}
			}
			else
			{
				$this->response(array('error' => 'Não foi possível enviar Pedidos!'), 404);
			}
			
		}
		else
		{
			$retorno = array(
				'erro' => true,
				'mensagem' => 'Seu usuário é inválido.'
			);
			$this->response($retorno, 200);
		}
	}
}