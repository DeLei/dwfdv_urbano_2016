<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class: Download
 *
 *
 */
class Download extends CI_Controller
{
	/**
	 * method 	__construct()	 
	 */
	function __construct()
    {
		parent::__construct();
		
		//Salvar localização e outras informações do aparelho.
		$this->logs_model->salvar_informacoes();
    }
	
	function apk($nome_arquivo)
	{
		
		$nome_arquivo = $nome_arquivo . '.apk';
		$arquivo = file_get_contents(DIR_APK . $nome_arquivo);
		
		force_download($nome_arquivo, $arquivo);
		
	}
	
	function pacote($codigo_representante)
	{
		if($codigo_representante)
		{
			$this->realizar_download($codigo_representante);
		}
	}
	
	function unicos()
	{
		$this->realizar_download('unicos');
	}
	
	function realizar_download($nome_arquivo)
	{
		$nome_arquivo = $nome_arquivo . '.zip';
	
		$arquivo = file_get_contents(DIR_PACOTES . $nome_arquivo);
	
		force_download($nome_arquivo, $arquivo); 
	}
	
	function incremental($codigo_representante, $id_usuario, $modulo) {
		$nome_arquivo = $modulo . '.zip';
		
		$arquivo = 'incremental/' . $codigo_representante . '/' . $id_usuario . '/' . $modulo . '.zip';
		
		$arquivo = file_get_contents(DIR_PACOTES . $arquivo);
		
		force_download($nome_arquivo, $arquivo);
	}
}