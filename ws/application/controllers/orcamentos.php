<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Orcamentos extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('orcamentos_model');
    }
	
	
	function exportar_get()
	{
		$orcamentos = $this->orcamentos_model->exportar_orcamentos($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($orcamentos)
        {
            $this->response($orcamentos, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Orçamentos!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->orcamentos_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Orçamentos!'), 404);
        }
	}
	
	
	function importar_post()
	{
	
		if($this->input->post('retorno') && $this->input->post('id_usuario'))
		{
			$retorno = $this->orcamentos_model->importar($this->input->post('retorno'), $this->input->post('id_usuario'), $this->input->post('codigo_representante'));

			if($retorno)
			{
				$this->response($retorno, 200);
			}
			else
			{
				$this->response(array('error' => 'Não foi possível enviar Orçamentos!'), 404);
			}
		}
		else
		{
			$this->response(array('error' => 'Não foi possível enviar Orçamentos!'), 404);
		}
	}
	
}