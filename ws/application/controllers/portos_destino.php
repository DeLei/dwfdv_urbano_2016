<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Portos_destino extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('portos_destino_model');
    }

	function exportar_get()
	{
		$dados = $this->portos_destino_model->exportar_portos_destino($this->input->get('id'), $this->input->get('pacote'),$this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Portos Destino!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->portos_destino_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Portos Destinos!'), 404);
        }
	}
	
}