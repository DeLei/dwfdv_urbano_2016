<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Unidade_medidas_venda extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('unidade_medidas_venda_model');
    }

	function exportar_get()
	{
		$dados = $this->unidade_medidas_venda_model->exportar_unidades($this->get('id'), $this->input->get('pacote'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar!'), 404);
        }
	}
	
	function tabela_get()
	{
		$dados = $this->unidade_medidas_venda_model->exportar_unidades($this->get('id'), $this->input->get('pacote'), TRUE);
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->unidade_medidas_venda_model->retornar_total($this->input->get('id'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total!'), 404);
        }
	}
	
}