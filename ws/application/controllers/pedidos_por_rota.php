<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Pedidos_por_rota extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('pedidos_por_rota_model');
    }

	function exportar_get()
	{
		$dados = $this->pedidos_por_rota_model->exportar_pedidos_por_rota($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Pedidos por rota!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->pedidos_por_rota_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Pedidos por rota!'), 404);
        }
	}
	
}