<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Prospects extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('prospects_model');
    }

	function exportar_get()
	{
	
		
		$dados = $this->prospects_model->exportar_prospects($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'), $this->input->get('erros_prospects'),$this->input->get('incremental'),$this->input->get('ultima_sincronizacao'));
		
		if($dados)
        {
				$this->response($dados, 200); // 200 being the HTTP response code
		}
		else
        {
            $this->response(array('error' => 'Não foi possível buscar Prospects!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->prospects_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'), $this->input->get('erros_prospects'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Prospects!'), 404);
        }
	}
	
	function importar_post()
	{
		$this->load->model('autenticacao_model');
		if($this->autenticacao_model->validar_usuario()) {
		
			if($this->input->post('retorno') && $this->input->post('id_usuario'))
			{
				$retorno = $this->prospects_model->importar($this->input->post('retorno'), $this->input->post('id_usuario'), $this->input->post('codigo_representante'),$this->input->get('incremental'),$this->input->get('ultima_sincronizacao'));

				if($retorno)
				{
					$this->response($retorno, 200);
				}
				else
				{
					$this->response(array('error' => 'Não foi possível enviar Prospects!'), 404);
				}
			}
			
		}
		else
		{
			$retorno = array(
				'erro' => true,
				'mensagem' => 'Seu usuário é inválido.'
			);
			$this->response($retorno, 200);
		}
	}
	
}