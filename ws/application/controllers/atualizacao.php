<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Atualizacao extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('atualizacao_model');
    }

	function ultima_versao_get()
	{
		$versao = ($this->input->get('versao') ? $this->input->get('versao') : $this->input->post('versao'));
		
		
		$usuario_id = ($this->input->get('id_usuario') ? $this->input->get('id_usuario') : $this->input->post('id_usuario'));
		
		$dados = $this->atualizacao_model->verificar_versao($versao, $usuario_id);
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Eventos!'), 404);
        }
	}
	
}