<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class WebServices extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('filiais_model');
    }
	
	public function impostos_post()
	{		
		$pedido 	= json_decode($this->input->post('retorno'));
		
		
		// write_file(APPPATH.'/logs/webservices.log', $this->input->post('retorno'));
		//print_r($pedido);
		
		//Obter as informações do cliente
		$cliente 			= $pedido->dados_cliente->codigo;
		$cliente_loja 	= $pedido->dados_cliente->loja;
		$cliente_tipo  	= $pedido->dados_cliente->tipo;
		
		//Setar o Array de retorno
		$retorno 		= array();
		
		//Se existir itens então calcular
		if (count($pedido)>0) 
		{
			//Setar as variaveis usadas no laço
			$total_pedido 		 	= 0;
			$total_st_pedido 	 	= 0;
			$total_ipi_pedido   	= 0;
			$total_icms_pedido   = 0;
			$itens 				 	= array();
			
			//percorrer os itens e montar um array auxiliar que será enviado para o Webservices 
			foreach ($pedido->produtos as $produto) 
			{
				$desconto_r = $produto->preco_unitario - $produto->preco_venda;
				
				//Montar array para o webservices
				$itens[] = array(	
						'CCLIENTE' 		=> (string)$cliente, 		// Código do cliente 
						'CLOJA' 				=> (string)$cliente_loja, 	// Loja do Cliente
						'CTIPO' 				=> (string)$cliente_tipo, 	// Tipo do cliente / para calcular normalmente é S
						'NQTD' 				=> (string)$produto->quantidade, // Quantidade comprada do item 
						'NPRC' 				=> (string)number_format($produto->preco_unitario,3, '.', ''), // preço de venda (com .) sempre com 3decimais
						'NVALORDESC' 	=> (string)number_format($desconto_r * $produto->quantidade,3, '.', ''), // preço de venda (com .) sempre com 3decimais
						'NVALOR' 			=> (string)number_format($produto->quantidade * $produto->preco_venda,3, '.', ''), // quantidade de compra do item * preço de venda (com .) sempre com 3 decimais
						'CPRODUTO' 		=> (string)$produto->codigo);	
			}
			
			// Realizar a chamada ao webservices passando o array auxiliar 
			// com todos os parametros que o webservices precisa.
			$impostos = obter_st($itens);
			
			if(count($impostos))
			{
				//percorrer os impostos calculados pelo webservices
				foreach($impostos as $imposto)
				{

					// Montar array com os impostos calculados e o produto
					$retorno['produtos'][] = array( 
							'PRODUTO' 	=> $imposto['PRODUTO'],
							'TES'	  		=> $imposto['TES'],
							'CF'	  		=> $imposto['CF'],
							'CFOP'	  	=> $imposto['CFOP'],
							'ST'	  		=> $imposto['ST'],											
							'IPI'			=> $imposto['IPI'],
							'ICMS'		=> $imposto['ICMS']						
					);
					
					// Calcular os totais do pedido com impostos				
					$total_st_pedido 		= $total_st_pedido 		+ $imposto['ST'];
					$total_ipi_pedido 		= $total_ipi_pedido 		+ $imposto['IPI'];
					$total_icms_pedido 	= $total_icms_pedido 	+ $imposto['ICMS'];
				}
				// Montar array com os totais
				$retorno['totais'] = array(					
						'valor_st_total' 		=> formatar_numero($total_st_pedido, 	3, ',', '.'),
						'valor_ipi_total' 		=> formatar_numero($total_ipi_pedido, 	3, ',', '.'),
						'valor_icms_total' 	=> formatar_numero($total_icms_pedido, 	3, ',', '.'),					
				);
				
				//Imprimir o json
				$this->response(array('sucesso' => '1', 'retorno' => $retorno), 200);
			}
			else
			{
				//Imprimir o json
				$this->response(array('sucesso' => '0', 'retorno' => $retorno), 200);
			}
		}
		else
		{
			$this->response(array('error' => 'Não foi possível simular os impostos!'), 200);
		}
		
		
	}

}