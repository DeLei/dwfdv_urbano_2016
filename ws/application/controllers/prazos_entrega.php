<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Prazos_entrega extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('prazos_entrega_model');
    }

	function exportar_get()
	{
		$dados = $this->prazos_entrega_model->exportar_prazos_entrega($this->input->get('id'), $this->input->get('pacote'),$this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar os Prazos de Entrega!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->prazos_entrega_model->retornar_total($this->input->get('id'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Prazos de Entrega!'), 404);
        }
	}
	
}