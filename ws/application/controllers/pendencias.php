<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Pendencias extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('pendencias_model');
    }

	function exportar_get()
	{
		$dados = $this->pendencias_model->exportar_pendencias($this->input->get('id'), $this->input->get('pacote'), $this->input->get('id_usuario'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Pendências!'), 404);
        }
	}
	
	function importar_post()
	{
	
		if($this->input->post('retorno') && $this->input->post('id_usuario'))
		{
			$retorno = $this->pendencias_model->importar($this->input->post('retorno'), $this->input->post('id_usuario'), $this->input->post('codigo_representante'));

			if($retorno)
			{
				$this->response($retorno, 200);
			}
			else
			{
				$this->response(array('error' => 'Não foi possível enviar Pendências cadastradas!'), 404);
			}
		}
	}
	
	
	function tabela_get()
	{
		$dados = $this->pendencias_model->exportar_pendencias($this->get('id'), $this->input->get('pacote'), $this->input->get('id_usuario'), TRUE);
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Pendências!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->pendencias_model->retornar_total($this->input->get('id'), $this->input->get('id_usuario'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Pendências!'), 404);
        }
	}
	
}