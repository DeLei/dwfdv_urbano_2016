<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Tipos_transporte extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('tipos_transporte_model');
    }

	function exportar_get()
	{
		$dados = $this->tipos_transporte_model->exportar_tipos_transporte($this->input->get('id'), $this->input->get('pacote'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Tipos de Transporte!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->tipos_transporte_model->retornar_total($this->input->get('id'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Tipos de Transporte!'), 404);
        }
	}
	
}