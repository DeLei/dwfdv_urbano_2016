<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Recebimentos extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('recebimentos_model');
    }

	function exportar_get()
	{
		$dados = $this->recebimentos_model->exportar_recebimentos($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Clientes!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->recebimentos_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Clientes!'), 404);
        }
	}
	
}