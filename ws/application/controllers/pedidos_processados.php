<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Pedidos_processados extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('pedidos_processados_model');
    }
	
	// Pedidos Processados
	function exportar_get()
	{
	
		
		$pedidos = $this->pedidos_processados_model->exportar_pedidos(
		$this->input->get('id'), 
		$this->input->get('pacote'),
		$this->input->get('codigo_representante'),
		$this->input->get('incremental'),
		$this->input->get('ultima_sincronizacao')
		//$this->input->get('debug')
		);
		
		if($pedidos)
        {
            $this->response($pedidos, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Pedidos Processados!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->pedidos_processados_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Pedidos Processados!'), 404);
        }
	}
	
}