<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Prospects_processados extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('prospects_processados_model');
    }

	function exportar_get()
	{
		$dados = $this->prospects_processados_model->exportar_prospects_processados($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Prospects Processados!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->prospects_processados_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'), $this->input->get('incremental'), $this->input->get('ultima_sincronizacao'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Prospects Processados!'), 404);
        }
	}
	
}