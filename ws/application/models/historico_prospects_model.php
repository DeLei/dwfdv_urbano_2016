<?php

class Historico_prospects_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	* Metódo:		exportar_historico_prospects
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de Transportadoras
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @param		string 		$codigo_representante	- Codigo do represnetante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_historico_prospects($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
	
		$id 					= $dados['id'];
		$codigo_representante 	= $dados['codigo_representante'];
		
		//-------------------------------------------------------
	
		// Obter codigos dos clientes
		$codigos_prospects = consulta_union_empresas($this, $codigo_representante);
		
		$codigos_array = array();
		
		foreach($codigos_prospects as $codigo_prospect)
		{
			$codigos_array[] = $codigo_prospect['cgc'];
		}
	
		if(count($codigos_array) > 0)
		{
			$this->db->where_in('cpf', $codigos_array);
		}
		else
		{
			$this->db->where('cpf', NULL);
		}
	
		
	

	
		
		// Consulta
		$this->db->select('id');
		$this->db->select('timestamp');
		$this->db->select('id_prospect');
		$this->db->select('cpf');
		$this->db->select('pessoa_contato');
		$this->db->select('cargo');
		$this->db->select('email');
		$this->db->select('descricao');
		$this->db->select('protocolo');
		$this->db->select("'1' as exportado", false);
		$this->db->select("'0' as editado", false);
		$this->db->select("'0' as erro", false);
		$this->db->from('historicos_prospects');
	}
	
	//Consulta realizada no ERP
	function consulta_erp($dados = NULL)
	{
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		//--------------------------------------
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		
		//--------------------------------------
	
		$codigos_prospects = $this->db_cliente
			->select($this->_db_cliente['campos']['prospects']['cgc'] . " as cgc")
			->from($this->_db_cliente['tabelas']['prospects'])
			->where(array(
				$this->_db_cliente['campos']['prospects']['delecao'] . ' !=' => '*',
				$this->_db_cliente['campos']['prospects']['codigo_representante'] => $codigo_representante,
			));
			
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	
	/**
	* Metódo:		importar
	* 
	* Descrição:	Função Utilizada para inserir historico de prospects no banco, e inserir LOGS
	* 
	* Data:			25/09/2012
	* Modificação:	25/09/2012
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Prospects enviados pelo DW força de vendas
	* @param		string 		$id_usuario					- ID do usuário
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('historico_prospects', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			$historicos_prospects = json_decode($dados);
			
			foreach($historicos_prospects as $historico_prospect)
			{
				try{

					$valores['timestamp'] = 		isset_valor($historico_prospect->timestamp);
					//$valores['id_prospect'] = $historico_prospect->id_prospect;
					$valores['cpf'] = 				isset_valor($historico_prospect->cpf);
					$valores['pessoa_contato'] = 	isset_valor($historico_prospect->pessoa_contato);
					$valores['cargo'] = 			isset_valor($historico_prospect->cargo);
					$valores['email'] = 			isset_valor($historico_prospect->email);
					$valores['descricao'] = 		isset_valor($historico_prospect->descricao);
					$valores['protocolo'] = 		isset_valor($this->gerar_protocolo($codigo_representante));
				
					$this->db->insert('historicos_prospects', $valores);
					
				}
				catch(Exception $e)
				{
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($historico_prospect),$id_usuario, $codigo_representante); 
				}
			}
			
			$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
		
			if($dados_erros)
			{
				foreach($dados_erros as $dado_erro)
				{
					$dados_json = json_decode($dado_erro->dados); 
					$codigos_erro[] = $dados_json->id;
					$nome_erro[] = 'CPF: ' . $dados_json->cpf;
				}
				
				$erros['erro'] = $codigos_erro;
				$erros['erro_descricao'] = $nome_erro;
				
				return $erros;
			}
			else
			{
				return array('sucesso' => 'ok');
			}
		
		}
	}
	
	/**
	* Metódo:		gerar_protocolo
	* 
	* Descrição:	Função Utilizada para gerara protocolo do historico
	* 
	* Data:			25/09/2012
	* Modificação:	25/09/2012
	* 
	* @access		public
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function gerar_protocolo($codigo_representante = NULL)
	{
		//Obter número total do históricos de prospects
		$i = $this->db->from('historicos_prospects')->get()->num_rows();

		//Gerar o protocolo do Histórico
		return date('Y') . '/' . ($codigo_representante ? $codigo_representante : 0) . '.' . $i;
	}

}