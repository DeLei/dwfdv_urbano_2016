<?php

class Portos_destino_model extends CI_Model {


    function __construct()
    {
        parent::__construct();		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    
	
	/**
	* Método:		exportar_portos_destino
	* 
	* Descrição:	Função Utilizada para retornar dados de portos_destino Disponíveis
	* 
	* Data:			17/10/2013
	* Modificação:	N/A
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_portos_destino($id = NULL, $pacote = NULL,$codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SY9010');
		$parametros_consulta['tabelas_principal']		= 'SY9010';
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'Y9_DESCR', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Método:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que sera executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id 				= $dados['id'];
		$codigo_empresa 	= $dados['codigo_empresa'];
		$codigo_representante	= $dados['codigo_representante'];
	
		
		//Sub Select		
		$this->db_cliente_sub = $this->load->database('db_cliente', TRUE);
		
		
		$this->db_cliente_sub->select($this->_db_cliente['campos']['frete_carreteiro_por_rota']['tab_frete'])
		->from($this->_db_cliente['tabelas']['tabelas_precos'])
		->from($this->_db_cliente['tabelas']['frete_carreteiro_por_rota'])
		->where($this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['tabelas_precos']['delecao'] . ' !=','*')
		->where($this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['tabelas_precos']['recdel'], '0')
		->where($this->_db_cliente['campos']['tabelas_precos']['vinculo_representante'], $codigo_representante)
		->where($this->_db_cliente['tabelas']['frete_carreteiro_por_rota'] . '.' . $this->_db_cliente['campos']['frete_carreteiro_por_rota']['delecao'] . ' !=', '*');
		$subQuery = $this->db_cliente_sub->_compile_select();
		$this->db_cliente_sub->_reset_select();
		
		
		
		
		//Fim Sub Select
		
		// Campos para o SELECT
		
		$select[] = $this->db_cliente->select($this->_db_cliente['campos']['frete_carreteiro']['porto'].' AS porto');  
		$select[] = $this->db_cliente->select($this->_db_cliente['campos']['portos_aeroportos']['descricao'].' AS descricao');
		$select[] = $this->db_cliente->select($this->_db_cliente['campos']['portos_aeroportos']['tempo_entrega'].' AS tempo_entrega');
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['frete_carreteiro']['porto'] . ' =', $id);
		}
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['frete_carreteiro'].'.'.$this->_db_cliente['campos']['frete_carreteiro']['delecao'] . ' !=', '*');
			$this->db_cliente->where($this->_db_cliente['tabelas']['portos_aeroportos'].'.'.$this->_db_cliente['campos']['portos_aeroportos']['delecao'] . ' !=', '*');
		}
		$this->db_cliente->where($this->_db_cliente['campos']['frete_carreteiro']['porto'].' >','\' \'');
		
		
	
		
		
		$this->db_cliente->where($this->_db_cliente['campos']['frete_carreteiro']['tab_frete'].' IN ('.$subQuery.')');
		
		// Consulta
		
		$this->db_cliente->join($this->_db_cliente['tabelas']['frete_carreteiro'], $this->_db_cliente['campos']['portos_aeroportos']['filial'] . ' = ' . $this->_db_cliente['campos']['frete_carreteiro']['filial'] . ' AND ' . $this->_db_cliente['campos']['portos_aeroportos']['codigo'] . ' = ' . $this->_db_cliente['campos']['frete_carreteiro']['porto']);
		$this->db_cliente->from($this->_db_cliente['tabelas']['portos_aeroportos']);		
		$this->db_cliente->distinct()->select($select); //->from($this->_db_cliente['tabelas']['frete_carreteiro']);
		//debug_pre($this->db_cliente->_compile_select());				
		
		
	}
	
	/**
	* Método:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de Portos Destino
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$codigo_representante	- Utilizado para retornar Registros apenas do representante informado 
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id,$codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SY9010');
		$parametros_consulta['tabelas_principal']		= 'SY9010';
		
		return retornar_total($this, $parametros_consulta);
	}
	

}