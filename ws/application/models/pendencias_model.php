<?php

class Pendencias_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	* Metódo:		exportar_noticias
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de Transportadoras
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_pendencias($id = NULL, $pacote = NULL, $id_usuario = NULL, $campos = FALSE)
	{
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $id_usuario;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
	
		$id 							= $dados['id'];
		$codigo_representante 	= $dados['codigo_representante'];
		
		// Condições do SQL (WHERE)
		if($id)
		{
			// Buscar Filiais a partir deste ID
			$this->db->where(array('pendencias.id >' => $id));
		}
		
		//Buscar apenas pendencias do usuário
		if($codigo_representante)
		{
			$this->db->where('usuarios_pendencias.id_usuario', $codigo_representante);
			$this->db->or_where('usuarios_pendencias.id_usuario_remetente', $codigo_representante);
		}
		
		//Selecionar todos os campos para criar no navegador
		$this->db->select('pendencias.id as id'); 	
		$this->db->select('pendencias.timestamp as timestamp'); 	
		$this->db->select('pendencias.id_usuario as id_usuario'); 	
		$this->db->select('pendencias.nome_real_usuario as nome_real_usuario'); 	
		$this->db->select('pendencias.status as status'); 	
		$this->db->select('pendencias.prioridade as prioridade'); 	
		$this->db->select('pendencias.titulo as titulo'); 	
		$this->db->select('pendencias.descricao as descricao'); 	
		$this->db->select('pendencias.prazo_solucao as prazo_solucao'); 	
		$this->db->select('pendencias.prazo_solucao_timestamp as prazo_solucao_timestamp'); 	
		$this->db->select('pendencias.ultima_mensagem_timestamp as ultima_mensagem_timestamp'); 	
		$this->db->select('pendencias.id_usuario_ultima_mensagem as id_usuario_ultima_mensagem'); 	
		$this->db->select('pendencias.nome_usuario_ultima_mensagem as nome_usuario_ultima_mensagem'); 	
		$this->db->select('pendencias.encerramento_timestamp as encerramento_timestamp'); 	
		$this->db->select('pendencias.uploads as uploads'); 
		$this->db->select('pendencias.latitude as latitude'); 
		$this->db->select('pendencias.longitude as longitude'); 
		$this->db->select('pendencias.versao as versao'); 
		$this->db->select('GROUP_CONCAT(usuarios.nome_real) as para'); 
	
		$this->db->select('usuarios.codigo as codigo_usuario');
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = Não Exportado)
		$this->db->select("'1' as exportado", false);
		$this->db->select("'0' as editado", false);
		$this->db->select("'0' as remover", false);
		$this->db->select("'0' as erro", false);

		//Tabela
		$this->db->from('pendencias');
		
		//Joins
		$this->db->join('usuarios_pendencias', 'usuarios_pendencias.id_pendencia = pendencias.id');
		$this->db->join('usuarios', 'usuarios.id = usuarios_pendencias.id_usuario');		
		
		//Agrupar
		$this->db->group_by('usuarios_pendencias.id_pendencia');		
		
		
	}
	
	/**
	* Metódo:			importar
	* 
	* Descrição:		Função Utilizada para inserir pendência no banco, e inserir LOGS
	* 
	* Data:				25/09/2012
	* Modificação:	25/09/2012
	* 
	* @access		public
	* @param		json 			$dados							- Dados das Pendências enviados pelo DW força de vendas
	* @param		string 		$id_usuario						- ID do usuário
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('pendencias', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			$pendencias = json_decode($dados);
	
			foreach($pendencias as $pendencia)
			{
				try{				
					//ADICIONAR PENDÊNCIA
					if($pendencia->remover == '' AND $pendencia->editado == '' AND $pendencia->exportado == '')
					{					
						$valores['timestamp']						=	isset_valor($pendencia->timestamp);					
						$valores['id_usuario'] 						=	isset_valor($pendencia->id_usuario);
						$valores['nome_real_usuario'] 				=	isset_valor($pendencia->nome_real_usuario);
						$valores['status'] 							= 	isset_valor('em_aberto');
						$valores['prioridade'] 						= 	isset_valor($pendencia->prioridade);
						$valores['titulo'] 							=	isset_valor($pendencia->titulo);					
						$valores['descricao'] 						=	isset_valor($pendencia->descricao);					
						$valores['prazo_solucao'] 					=	isset_valor(date('d/m/Y', strtotime($pendencia->prazo_solucao)));
						$valores['prazo_solucao_timestamp'] 		=	isset_valor(strtotime($pendencia->prazo_solucao));
						$valores['latitude'] 						=	isset_valor($pendencia->latitude);					
						$valores['longitude'] 						=	isset_valor($pendencia->longitude);					
						$valores['versao'] 							=	isset_valor($pendencia->versao);					
						
						//Adicionar a ligação entre a pendência e o usuário
						if ($this->db->insert('pendencias', $valores)) 
						{
							$usuarios = array();
							//Verificar o último id adicionado
							$id_pendencia = $this->db->insert_id();
						
							//Obter usuários que devem receber a pendência
							$usuarios[] = $pendencia->id_usuario;
							
							//Varrer o post e salvar as ligações
							foreach ($usuarios as $id_usuario) 
							{
								//Adicionar a ligação entre a pendência e o usuário
								$this->db->insert('usuarios_pendencias', array(
									'id_pendencia' 				=> $id_pendencia,
									'id_usuario' 					=> $pendencia->codigo_usuario,
									'id_usuario_remetente' 	=> $pendencia->id_usuario
								));                     
							}                                           
						}                  
					}
					
					//ENCERRAR A PENDÊNCIA
					if(trim($pendencia->editado) == '1' AND trim($pendencia->status) == 'aguardando_encerramento')
					{
						//Executar UPDATE na pendência
						$this->db->where('status !=','encerrada');
						$this->db->where('id' ,$pendencia->id);
						
						$this->db->update('pendencias', array('status' => 'aguardando_encerramento'));	
						
					}
					
					//REMOVER PENDÊNCIA
					if($pendencia->remover == '1')
					{
						$this->db->delete('pendencias', array('id' 								=> $pendencia->id));
						$this->db->delete('pendencias_mensagens', array('id_pendencia' => $pendencia->id));
						$this->db->delete('usuarios_pendencias', array('id_pendencia' 	=> $pendencia->id));
					}
				}
				catch(Exception $e)
				{
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($pendencias),$id_usuario, $codigo_representante); 
				}
			}
			
			$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
		
			if($dados_erros)
			{
				foreach($dados_erros as $dado_erro)
				{
					$dados_json 		= json_decode($dado_erro->dados); 
					$codigos_erro[] 	= $dados_json->id;
					$nome_erro[] 		= 'Não foi possível salvar Pendência.';
				}
				
				$erros['erro'] = $codigos_erro;
				$erros['erro_descricao'] = $nome_erro;
				
				return $erros;
			}
			else
			{
				return array('sucesso' => 'ok');
			}
		
		}
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $codigo_representante)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
	
		return retornar_total($this, $parametros_consulta);
		
	}

}