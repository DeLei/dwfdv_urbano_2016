<?php

class Filiais_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	//-------------------------------------
	
	/**
	* Metódo:		exportar_filiais
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de Transportadoras
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_filiais($id = NULL, $pacote = NULL)
	{
		
		$parametros_consulta['id'] = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, FALSE, NULL);
		
		// Retorno Dados
		return $dados;
	}	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
		$id = $dados['id'];
	
		// Condições do SQL (WHERE)
		if($id)
		{
			// Buscar Filiais a partir deste ID
			$this->db->where(array('id >' => $id));
		}
		$this->db->where('status', 'ativo');
	
		// Selecionar
		$this->db->select('id');	
		$this->db->select('status');	
		$this->db->select('codigo');	
		$this->db->select('timestamp');	
		$this->db->select('codigo_cliente');	
		$this->db->select('razao_social');	
		$this->db->select('nome_fantasia');	
		$this->db->select('cnpj');	
		$this->db->select('inscricao_estadual');	
		$this->db->select('cep');	
		$this->db->select('endereco');	
		$this->db->select('numero');	
		$this->db->select('complemento');	
		$this->db->select('bairro');	
		$this->db->select('cidade');	
		$this->db->select('estado');	
		$this->db->select('telefone_1');	
		$this->db->select('email');	
		$this->db->select('site');	
		$this->db->select('observacao');	
		$this->db->select('telefone_2');	
		$this->db->select('notificacoes_nome');	
		$this->db->select('notificacoes_email');		
		
		// Consulta
		$this->db->from('empresas');
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] = $id;
	
		return retornar_total($this, $parametros_consulta);
	}

}