<?php

class Tabelas_preco_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    
	function exportar_tabelas_preco($id = NULL, $pacote = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		
		$parametros_consulta['id'] = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('DA0010');
		$parametros_consulta['tabelas_principal']		= 'DA0010';
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'DA0_CODTAB', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados)
	{
			
		$id 					= $dados['id'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		$codigo_representante	= $dados['codigo_representante'];
				
		$tabelas_precos_clientes_do_representante = $this->obter_tabelas_clientes_representante($codigo_representante);
		
		// * Retornar todos os campos
		$select = select_all($this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos'], NULL, FALSE, 'filial');
		
		$select += formatar_euf($this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos']['filial'], $codigo_empresa);
		
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['tabelas_precos']['recno'] . ' >', $id);
		}
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['tabelas_precos']['delecao'] . ' !=', '*');
		}
		$this->db_cliente->where($this->_db_cliente['campos']['tabelas_precos']['ativo'], '1');
		
		/*
		$por_representante = "(" . $this->_db_cliente['campos']['tabelas_precos']['vinculo_representante'] . " = '" . $codigo_representante . "' OR " . $this->_db_cliente['campos']['tabelas_precos']['vinculo_representante'] . " = ' ')";
		*/
		$por_representante = "(" . $this->_db_cliente['campos']['tabelas_precos']['vinculo_representante'] . " = '" . $codigo_representante. "'" ;
		if(COUNT($tabelas_precos_clientes_do_representante)>0){
			$por_representante .=  " OR (" . $this->_db_cliente['campos']['tabelas_precos']['codigo'] . " IN ('".implode("','",$tabelas_precos_clientes_do_representante)."') AND TRIM(".$this->_db_cliente['campos']['tabelas_precos']['vinculo_representante'].") is null   )";
		}
		$por_representante .= ")";
		
		$this->db_cliente->where($por_representante);
		
		$dataAtual = date('Ymd');
	
		$this->db_cliente->where($this->_db_cliente['campos']['tabelas_precos']['vigencia_inicio'] . ' <=', $dataAtual);
		$this->db_cliente->where("(" . $this->_db_cliente['campos']['tabelas_precos']['vigencia_final'] . " >= '" . $dataAtual . "' OR " . $this->_db_cliente['campos']['tabelas_precos']['vigencia_final'] . " = ' ')");
		
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['tabelas_precos']);
		
		//debug_pre($this->db_cliente->_compile_select());
		
						
	}
	
	/*
	Obt�m as tabelas de pre�os utilizadas pelos clientes do representante.
	*/
	function obter_tabelas_clientes_representante($codigo_representante){
		
		$dados = $this->db_cliente->select($this->_db_cliente['campos']['clientes']['tabela_preco'])
		->distinct()
		->from($this->_db_cliente['tabelas']['clientes'])
		->where($this->_db_cliente['campos']['clientes']['codigo_representante'], $codigo_representante)
		->where($this->_db_cliente['campos']['clientes']['delecao']. ' != ', '*')
		->where($this->_db_cliente['campos']['clientes']['tabela_preco']. ' != ', ' ')	
		->get()->result_array();		
		
		$tabelas_precos = array();		
		foreach($dados as $tabela_preco){
			$tabelas_precos[] = $tabela_preco[strtolower($this->_db_cliente['campos']['clientes']['tabela_preco'])];
		}
		
		return $tabelas_precos;
	}

	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id, $codigo_representante, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		$parametros_consulta['id'] 					= $id;
		$parametros_consulta['codigo_empresa']		= NULL;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SA1010');
		$parametros_consulta['tabelas_principal']		= 'SA1010';
		
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	/**
	* Met�do:		obter_dados
	* 
	* Descri��o:	Fun��o Utilizada para retornar os dados da consulta
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function obter_dados($dados)
	{
		$this->consulta($dados);
		
		//$this->db_cliente->get()->result_array();
		
		//debug_pre($this->db_cliente->last_query());
		
		return $this->db_cliente->get()->result_array();
	}
	
}