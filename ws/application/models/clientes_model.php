<?php

class Clientes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
	
	function exportar_clientes($id = NULL, $pacote = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SA1010');
		$parametros_consulta['tabelas_principal']		= 'SA1010';
		
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, ' A1_COD', $parametros_consulta);
		
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{

		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		// Campos para o SELECT
		// * Retornar todos os campos
		$select_clientes = select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], NULL, FALSE, 'filial');
		$select_statisticas = select_all($this->_db_cliente['tabelas']['view_estatisticas_cliente'], $this->_db_cliente['campos']['view_estatisticas_cliente'], 'est', FALSE, 'filial');
		
		//$select_statisticas  = array();
		$select = array_merge(
				$select_clientes
				,$select_statisticas
				);
		
		
		/* Valor fixo para apenas um cliente alterar após conclusão */
		
		$this->db_cliente->join($this->_db_cliente['tabelas']['view_estatisticas_cliente']
				,
				$this->_db_cliente['tabelas']['clientes'].'.'.$this->_db_cliente['campos']['clientes']['codigo'].' = '.$this->_db_cliente['tabelas']['view_estatisticas_cliente'].'.'.$this->_db_cliente['campos']['view_estatisticas_cliente']['codigo_cliente'] .  
				' AND '.
				$this->_db_cliente['tabelas']['clientes'].'.'.$this->_db_cliente['campos']['clientes']['loja'].' = '.$this->_db_cliente['tabelas']['view_estatisticas_cliente'].'.'.$this->_db_cliente['campos']['view_estatisticas_cliente']['codigo_loja']
				,'LEFT'				
				);
		
		
		
		$select += formatar_euf($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], $codigo_empresa);
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['recno'] . ' >', $id);
		}

		
		$this->db_cliente->where('('.$this->_db_cliente['campos']['clientes']['codigo_representante'] .' = \''. $codigo_representante.'\' OR '.$this->_db_cliente['campos']['clientes']['codigo_representante2'] .' = \''. $codigo_representante.'\' )');
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'] . ' !=', '*');
		}
	
	
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['clientes']);
		//debug_pre($this->db_cliente->_compile_select());
		
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
	
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
	
	
	
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SA1010');
		$parametros_consulta['tabelas_principal']		= 'SA1010';
	
		return retornar_total($this, $parametros_consulta);
	}
}