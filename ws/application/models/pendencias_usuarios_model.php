<?php

class Pendencias_Usuarios_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	* Metódo:		exportar_usuarios
	* 
	* Descrição:	Função retornar os usuários que podem recebe pendências do representante em questão
	* 
	* Data:				02/09/2013
	* Modificação:	02/09/2013
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_usuarios($id = NULL, $pacote = NULL, $id_usuario = NULL, $campos = FALSE)
	{
		$parametros_consulta['id'] 							= $id;
		$parametros_consulta['codigo_representante'] 	= $id_usuario;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	*/
	function consulta($dados = NULL)
	{
	
		$id 							= $dados['id'];
		$codigo_representante 	= $dados['codigo_representante'];

		//Select
		$this->db->select('id');
		$this->db->select('nome_real');
		$this->db->select('nome');
		$this->db->select('codigo');
		
		//Where
		$this->db->where('codigo', $codigo_representante);
		$this->db->or_where('grupo', 'gestores_comerciais');
		
		//Tabela
		$this->db->from('usuarios');		
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $codigo_representante)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
	
		return retornar_total($this, $parametros_consulta);
		
	}

}