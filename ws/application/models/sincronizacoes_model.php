<?php

class Sincronizacoes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		//$this->db_cliente = $this->load->database('db_cliente', TRUE);
		//$this->config->load('db_cliente_' . $this->db_cliente->erp);
        //$this->_db_cliente = $this->config->item('db_cliente');
    }
    
	/**
	* Met�do:		salvar_sincronizacao
	* 
	* Descri��o:	Fun��o Utilizada para salvar "DADOS (LOGS)" de sincroniza��es
	* 
	* Data:			21/09/2012
	* Modifica��o:	21/09/2012
	* 
	* @access		public
	* @param		string 		$tipo					- Model que esta sendo sincronizado (Ex: prospects, pedidos, historico de prospects)
	* @param		json 		$dados					- Dados em json enviados pela sincroniza��o
	* @param		string 		$id_usuario				- ID do usu�rio que est� enviando a sincroniaz��o
	* @param		string 		$codigo_representante	- CODIGO do REPRESENTANTE que est� enviando a sincroniaz��o
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function salvar_sincronizacao($tipo, $dados, $id_usuario, $codigo_representante)
	{
		
	
		
		$data = array(
			'tipo'					=> $tipo,
			'endereco_ip' 			=> $this->input->ip_address(),
			'navegador' 			=> $this->agent->browser() . ' ' . $this->agent->version(),
			'navegador_string' 		=> $this->agent->agent_string(),
			'so' 					=> $this->agent->platform(),
			'url' 					=> $this->agent->referrer(),
			'timestamp' 			=> time(),
			'data' 					=> date('Y-m-d H:i:s'),
			'id_usuario' 			=> $id_usuario,
			'codigo_representante'	=> $codigo_representante,
			'dados' 				=> $dados,
			'macAddress'			=> $_POST['macAddress'],
			'versao'				=> $_POST['versao']
		);
		
		$this->db->insert('usuarios_sincronizacoes', $data); 
		
		return $this->db->insert_id();
		
	}
	
	/**
	* Met�do:		salvar_erro
	* 
	* Descri��o:	Fun��o Utilizada para salvar "DADOS (LOGS)" de sincroniza��es com ERRO
	* 
	* Data:			21/09/2012
	* Modifica��o:	21/09/2012
	* 
	* @access		public
	* @param		string 		$id_sincronizacao					- ID da sincroniza��o realizada epelo metodo "salvar_sincronizacao"
	* @param		string 		$mensagem							- Mensagem com o erro
	* @param		json 		$dados								- dados do sql com erro
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function salvar_erro($id_sincronizacao, $mensagem, $dados, $id_usuario, $codigo_representante, $codigo_erro = 1)
	{
		$data = array(
			'id_sincronizacao'	=> $id_sincronizacao,
			'endereco_ip' 			=> $this->input->ip_address(),
			'navegador' 			=> $this->agent->browser() . ' ' . $this->agent->version(),
			'navegador_string' 		=> $this->agent->agent_string(),
			'so' 					=> $this->agent->platform(),
			'timestamp' 			=> time(),
			'data' 					=> date('Y-m-d H:i:s'),
			'id_usuario' 			=> $id_usuario,
			'codigo_representante'	=> $codigo_representante,
			'mensagem'				=> utf8_encode($mensagem),
			'dados'					=> $dados,
			'codigo_erro'			=> $codigo_erro
			
			
		);
		
		$this->db->insert('usuarios_sincronizacoes_erros', $data); 
		
		//Muda a situa��o do erro para "S" para indicar que h� erro
		$this->db->where('id', $id_sincronizacao);
		$this->db->update('usuarios_sincronizacoes', array('erro' => 'S')); 
	}
	

}