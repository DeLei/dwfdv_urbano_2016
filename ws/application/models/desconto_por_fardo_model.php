<?php

class Desconto_por_fardo_model extends CI_Model {


	public $compartilhada = TRUE;

    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }

	
	function exportar_desconto_por_fardo($id = NULL, $pacote = NULL,$codigo_representante = NULL)
	{
		
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		$parametros_consulta['ignorar_incremental']		= true;
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'R_E_C_N_O_', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Método:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
	
		$id 					= $dados['id'];
		$codigo_empresa 		= NULL;
		$codigo_representante  	= $dados['codigo_representante'];
	
		//Obter Tabelas Preços
		
		$tabelas_preco = $this->obter_tabelas_preco($codigo_representante);
		
		$select_desconto_por_fardo = select_all($this->_db_cliente['tabelas']['desconto_por_fardo'], $this->_db_cliente['campos']['desconto_por_fardo']);
		
		$select = array_merge(
				$select_desconto_por_fardo
		);
		
		
		
	
		
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['desconto_por_fardo'] . '.' . $this->_db_cliente['campos']['desconto_por_fardo']['recno'] . ' >', $id);
		}
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['desconto_por_fardo'] . '.' . $this->_db_cliente['campos']['desconto_por_fardo']['delecao'] . ' !=', '*');
		}
		
	
		
		
		$or = FALSE;
		if(COUNT($tabelas_preco)){
			$where = '(';
			
			foreach($tabelas_preco as $tabela_preco){
			
				if(!$or){				
					$where .= $this->_db_cliente['campos']['desconto_por_fardo']['codigo_tabela_precos']. ' = '. "'".$tabela_preco['codigo']."'"
					.' AND '. $this->_db_cliente['campos']['desconto_por_fardo']['codigo_filial'].' = '."'".$tabela_preco['filial_saida']."'";
							
				}else{	
					$where .= ' OR '.				
					$this->_db_cliente['campos']['desconto_por_fardo']['codigo_tabela_precos']. " = '". $tabela_preco['codigo']."'"
					.' AND '. $this->_db_cliente['campos']['desconto_por_fardo']['codigo_filial']." = '".$tabela_preco['filial_saida']."'";
					
				}
				$or = TRUE;			
			}
			$where .= ' )';
			
			$this->db_cliente->where($where);
		}
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['desconto_por_fardo']);
		
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	/**
	* Método:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $codigo_representante)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	/**
	* Método:		obter_tabelas_preco
	* 
	* Descrição:	Função Utilizada para retornar os codigos da tabelas de Preço
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_tabelas_preco($codigo_representante)
	{
		// Load Tabelas de Preço
		$this->load->model('tabelas_preco_model');
	
		$this->tabelas_preco_model->_db_cliente = $this->_db_cliente;
	
		$parametros_consulta['id'] = NULL;
		$parametros_consulta['codigo_empresa'] = NULL;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
				
		$tabelas_preco = $this->tabelas_preco_model->obter_dados($parametros_consulta);
		
		$codigos_tabelas_preco  = array();
		foreach($tabelas_preco as $tp)
		{
			$codigos_tabelas_preco[] = array('codigo' => $tp['codigo'], 'filial_saida' =>$tp['filial_saida']);
		}
		
		return $codigos_tabelas_preco; 
		//return array('CTB'); //@TODO: Fixado pois é retornado muito produto - verificar após conclusão
	}
	
	function obter_produtos_destaque()
	{
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
		
		$produtos = $this->db_cliente->select($this->mapeamento['campos']['produtos']['codigo'] . ' AS "codigo"')
									->from($this->mapeamento['tabelas']['produtos'])
									->where($this->mapeamento['tabelas']['produtos'] . '.' . $this->mapeamento['campos']['produtos']['delecao'] . ' !=', '*')
									->where($this->mapeamento['campos']['produtos']['situacao_produto'], 'DE')
									->get()->result();
		
		return $produtos;
	}

}