<?php

class Representantes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
    
	function exportar_representante($codigo_representante = NULL)
	{
		$representante = consulta_union_empresas($this, $codigo_representante, FALSE);
		
		return $representante;
	}
	
	//Consulta realizada no ERP
	function consulta_erp($dados = NULL)
	{
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		//--------------------------------------
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		
		//--------------------------------------
	
		$select = select_all($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes'], NULL, FALSE, 'filial');
		
		$select += formatar_euf($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['filial'], $codigo_empresa);
	
		$this->db_cliente
			->select($select)
			->from($this->_db_cliente['tabelas']['representantes'])
			->where(array(
				$this->_db_cliente['campos']['representantes']['codigo'] => $codigo_representante,
				$this->_db_cliente['campos']['representantes']['delecao'] . ' !=' => '*'
			));
			
	}
	
	
	function obter_codigos_representantes($codigo_representante = NULL)
	{
		if(!is_null($codigo_representante)){
			$this->db->where('usuarios.codigo', $codigo_representante);
		}
		
		$this->db
		->select('usuarios.id')
		->select('codigo')		
		->select('key')
		->from('usuarios_aparelhos')
		->join('usuarios', 'usuarios.id = usuarios_aparelhos.usuarios_id AND usuarios.codigo = usuarios_aparelhos.usuarios_codigo')
		->where(
			array(
				'usuarios.grupo' => 'representantes',
			//	'usuarios.status' => 'ativo'
			)
		);
		
		$dados = $this->db->get()->result_array();

		
		$_codigos = array();
		
		if($dados)
		{
			
			foreach($dados as $representante)
			{
				$_codigos[] = array(
											'key' 		=> trim($representante['key']),
											'codigo' 	=> trim($representante['codigo']),
											'id' 		=> $representante['id'],
										);
			}
		}
		
		
		return $_codigos;
	}
	

}