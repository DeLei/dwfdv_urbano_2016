<?php

class Titulos_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
        
        $this->db_cliente = $this->load->database('db_cliente', TRUE);
        
    }
	
	/**
	* Metódo:		exportar_titulos
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de títulos
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web / William Reis Fernandes
	* 
	*/
	function exportar_titulos($id = NULL, $pacote = NULL, $codigo_representante, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SE1010');
		$parametros_consulta['tabelas_principal']		= 'SE1010';
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'SE1010.R_E_C_N_O_', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar titulos
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web / William Reis Fernandes
	* 
	*/
	function consulta($dados)
	{
	
		$id 					= $dados['id'];
		$codigo_representante 	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
	
		$select['empresa'] = $codigo_empresa;
	
		// * Retornar todos os campos
		$select_titulos		= select_all($this->_db_cliente['tabelas']['titulos'], $this->_db_cliente['campos']['titulos'], 'titulo', NULL, FALSE, 'filial');
		$select_clientes 	= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente', NULL, FALSE, 'filial');
		
		$select = array_merge(
			$select_titulos, 
			$select_clientes
		);
		
		//Status
		$select[] = "(CASE WHEN RTRIM(LTRIM(" . $this->_db_cliente['campos']['titulos']['data_vencimento'] . ")) < " . obter_data_atual() . " AND " . $this->_db_cliente['campos']['titulos']['data_baixa'] . " = ' ' THEN 'vencido' 
						WHEN " . $this->_db_cliente['campos']['titulos']['data_baixa'] . " != ' ' THEN 'pago' 
						ELSE 'a_vencer' END) AS titulo_status_extenso";
		
		$select += formatar_euf($this->_db_cliente['tabelas']['titulos'], $this->_db_cliente['campos']['titulos']['filial'], $codigo_empresa);
		
		// Join
		//-----------------------------------------------
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['clientes'], 
			euf($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], $this->_db_cliente['tabelas']['titulos'], $this->_db_cliente['campos']['titulos']['filial']) .
			$this->_db_cliente['campos']['clientes']['codigo'] . " = " . $this->_db_cliente['campos']['titulos']['codigo_cliente'] . " AND " .
			$this->_db_cliente['campos']['clientes']['loja'] . " = " . $this->_db_cliente['campos']['titulos']['loja_cliente']
		);
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['titulos'].".".$this->_db_cliente['campos']['titulos']['recno'] . ' >', $id);
		}
				
		$this->db_cliente->where($this->_db_cliente['tabelas']['titulos'].".".$this->_db_cliente['campos']['titulos']['data_emissao']." >= ", date('Ymd', PERIODO_DADOS));		
		$this->db_cliente->where($this->_db_cliente['tabelas']['titulos'].".".$this->_db_cliente['campos']['titulos']['tipo'] . ' !=', 'NNC');
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['titulos'].".".$this->_db_cliente['campos']['titulos']['delecao'] . ' !=', '*');
		}
		
		// CUSTOM: 177 / 00874 - 18/10/2013 - Localizar: CUST-REPRESENTANTE-2
		
		$this->db_cliente->where(
		'('.
			$this->_db_cliente['tabelas']['titulos'].".".$this->_db_cliente['campos']['titulos']['codigo_representante'] .' = ' 
			."'".$codigo_representante."'"
			.' OR '.
			$this->_db_cliente['tabelas']['titulos'].".".$this->_db_cliente['campos']['titulos']['codigo_representante2']. ' = '
			."'".$codigo_representante."'"
		.')'
		);
		// FIM CUSTOM: 177 / 00874 - 18/10/2013
	
	
	
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['titulos']);
		
		
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		$parametros_consulta['codigo_empresa']		 = NULL;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SE1010');
		$parametros_consulta['tabelas_principal']		= 'SE1010';
		
		return retornar_total($this, $parametros_consulta);
	}

}