<?php

class Unidade_medidas_venda_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	//-------------------------------------
	
	/**
	* Metódo:		exportar_unidades
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de Transportadoras
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_unidades($id = NULL, $pacote = NULL)
	{
		
		$parametros_consulta['id'] = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, FALSE, NULL);
		
		// Retorno Dados
		return $dados;
	}	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
		$id = $dados['id'];
	
		// Selecionar
		$this->db->select('tipo_pedido');	
		$this->db->select('unidade');		
		
		// Consulta
		$this->db->from('unidade_medidas_venda');
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] = $id;
	
		return retornar_total($this, $parametros_consulta);
	}

}