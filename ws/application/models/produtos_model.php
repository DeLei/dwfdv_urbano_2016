<?php

class Produtos_model extends CI_Model {


	public $compartilhada = TRUE;

    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }

	
	function exportar_produtos($id = NULL, $pacote = NULL,$codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SB1010');
		$parametros_consulta['tabelas_principal']		= 'SB1010';
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'B1_COD', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Método:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
	
		$id 					= $dados['id'];
		$codigo_empresa 		= NULL;
		$codigo_representante  	= $dados['codigo_representante'];
		
		
		$produtos_destaque = $this->obter_produtos_destaque_regiao($codigo_representante);
		
		$select_categoria_produto[] = $this->obter_produto_categoria();
		
		//Obter Tabelas Preços
		
		$tabelas_preco = $this->obter_tabelas_preco($codigo_representante);
		
		if(count($tabelas_preco) <= 0){
			$tabelas_preco[]  = array('codigo' => 'a-Z0-9', 
			'filial_saida' => '99');
		}
		
		//debug_pre($tabelas_preco);
		//$select_categoria_produto = select_all($this->_db_cliente['tabelas']['categorias_produtos'], array('codigo_categoria' => $this->_db_cliente['campos']['categorias_produtos']['codigo_categoria'] ),'produto');
		//debug_pre($select_categoria_produto);
		
		// * Retornar todos os campos
		$select_produto = select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto',false,'situacao_produto');
		$select_estoque = select_all($this->_db_cliente['tabelas']['produtos_estoque'], $this->_db_cliente['campos']['produtos_estoque'], 'etq');
		$select_produto_tabela_preco = select_all($this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos'], 'ptp');
		$select_tabela_preco = select_all($this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos'], 'tb');
		
		
		if(count($produtos_destaque) > 0){
			$select_produto[] = "(CASE WHEN SB1010.B1_COD  IN ('".implode("','",$produtos_destaque)."')  AND SB1010.B1_SITPROD = 'DE' 
				THEN 'DE' END) AS produto_situacao_produto";
		}else{
			$select_produto[] = "'' AS  produto_situacao_produto"  ;
		}
		//debug_pre($select_produto);
		
		$select = array_merge(
				$select_categoria_produto,
				$select_produto, 				
				$select_estoque,
				$select_produto_tabela_preco,
				$select_tabela_preco
		);

		
		// * Obter quantidade disponivel
		$select[] = '(' . $this->_db_cliente['campos']['produtos_estoque']['quantidade_atual'] . ' - (' . 
			$this->_db_cliente['campos']['produtos_estoque']['quantidade_pedidos_venda'] . ' + ' . 
			$this->_db_cliente['campos']['produtos_estoque']['quantidade_empenhada'] . ' + ' . 
			$this->_db_cliente['campos']['produtos_estoque']['quantidade_reservada'] . ')) AS quantidade_disponivel_estoque';
		//*
		
		$select += formatar_euf($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], $codigo_empresa);
		$select[]  = ' TRIM( SUBSTR(F4_CF,2,3)) as cf';
		//Join
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos_estoque'], 
			euf(
				$this->_db_cliente['tabelas']['produtos_estoque'], $this->_db_cliente['campos']['produtos_estoque']['filial'], 
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial']
			) .
			$this->_db_cliente['campos']['produtos_estoque']['codigo'] . ' = ' . $this->_db_cliente['campos']['produtos']['codigo']
			. ' AND ' . $this->_db_cliente['tabelas']['produtos_estoque'].'.'.$this->_db_cliente['campos']['produtos_estoque']['delecao'] . " != '*' " 
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos_tabelas_precos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos']['filial'], 
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial']
			) .
			$this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_produto'] . ' = ' . $this->_db_cliente['campos']['produtos']['codigo']
			. ' AND ' . $this->_db_cliente['campos']['produtos_tabelas_precos']['filial'] . " = " . $this->_db_cliente['campos']['produtos_estoque']['filial']
			. ' AND ' . $this->_db_cliente['tabelas']['produtos_tabelas_precos'].'.'.$this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . " != '*' "   
		);
		// JOIN SF4010 ON sf4010.d_e_l_e_t_ != '*' AND f4_filial = b1_filial AND f4_codigo = b1_ts
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['tipo_entrada_saida'], 
			euf(
				$this->_db_cliente['tabelas']['tipo_entrada_saida'], $this->_db_cliente['campos']['tipo_entrada_saida']['filial'], 
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial']
			) .
			'('.$this->_db_cliente['campos']['produtos']['tipo_entrada_saida'] . ' = ' . $this->_db_cliente['campos']['tipo_entrada_saida']['codigo'] .' OR '.$this->_db_cliente['campos']['produtos']['tipo_entrada_saida'] .' IS NULL)'
			
			. ' AND ' . $this->_db_cliente['tabelas']['tipo_entrada_saida'].'.'.$this->_db_cliente['campos']['tipo_entrada_saida']['delecao'] . " != '*' "   
			,'LEFT'
		);
		
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['tabelas_precos'], 
			euf(
				$this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos']['filial'], 
				$this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos']['filial']
			) .
			$this->_db_cliente['campos']['tabelas_precos']['codigo'] . ' = ' . $this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_tabela_precos'] . ' AND ' . $this->_db_cliente['campos']['tabelas_precos']['ativo'] . " = '1'"
			. ' AND ' . $this->_db_cliente['campos']['tabelas_precos']['filial'] . " = " . $this->_db_cliente['campos']['produtos_tabelas_precos']['filial']
			. ' AND ' . $this->_db_cliente['tabelas']['tabelas_precos'].'.'.$this->_db_cliente['campos']['tabelas_precos']['delecao'] . " != '*' "   
		);
	/*
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['categorias_produtos'], 
			''.
				$this->_db_cliente['tabelas']['categorias_produtos'].'.'.$this->_db_cliente['campos']['categorias_produtos']['codigo_produto']. ' = '. $this->_db_cliente['tabelas']['produtos'].'.'.$this->_db_cliente['campos']['produtos']['codigo'].
				' AND '. $this->_db_cliente['tabelas']['categorias_produtos'].'.'.$this->_db_cliente['campos']['categorias_produtos']['delecao']. " != '*' "
			.'',
			'LEFT'
		);
	/*
		
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['recno'] . ' >', $id);
		}

		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['inativo'] . ' !=', '1');
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['delecao'] . ' !=', '*');
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_estoque'] . '.' . $this->_db_cliente['campos']['produtos_estoque']['delecao'] . ' !=', '*');
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' !=', '*');
		
		
		//CUSTOM 177 - 870 - 14/10/2013
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['ativo'] , '1');		
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['data_vigencia'].'<=' , date('Ymd'));		
		//FIM CUSTOM 177 - 870 - 14/10/2013
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' !=', '*');
		
		/*
		
		$this->db_cliente->where_in($this->_db_cliente['campos']['tabelas_precos']['codigo'], $tabelas_preco);
		*/
		//Chamado: 002226 - Produto não aparece
		//Utilizado local de acordo com o produto
		$this->db_cliente->where('B2_LOCAL = B1_LOCPAD');
		//$this->db_cliente->where('B1_FILIAL', '01');
		
		if(COUNT($tabelas_preco) > 0){
		$or = FALSE;
		
		$where = '(';
		
		foreach($tabelas_preco as $tabela_preco){
		
			if(!$or){				
				$where .= $this->_db_cliente['campos']['tabelas_precos']['codigo']. ' = '. "'".$tabela_preco['codigo']."'"
				.' AND '. $this->_db_cliente['campos']['tabelas_precos']['filial_saida'].' = '."'".$tabela_preco['filial_saida']."'";
						
			}else{	
				$where .= ' OR '.				
				$this->_db_cliente['campos']['tabelas_precos']['codigo']. " = '". $tabela_preco['codigo']."'"
				.' AND '. $this->_db_cliente['campos']['tabelas_precos']['filial_saida']." = '".$tabela_preco['filial_saida']."'";
				
			}
			$or = TRUE;			
		}
		$where .= ' )';
		
		$this->db_cliente->where($where);
		}
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['delecao'] . ' !=', '*');
		}
		
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['produtos']);
		
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	/**
	* Método:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $codigo_representante, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SB1010');
		$parametros_consulta['tabelas_principal']		= 'SB1010';
		
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	/**
	* Método:		obter_tabelas_preco
	* 
	* Descrição:	Função Utilizada para retornar os codigos da tabelas de Preço
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_tabelas_preco($codigo_representante)
	{
		// Load Tabelas de Preço
		$this->load->model('tabelas_preco_model');
	
		$this->tabelas_preco_model->_db_cliente = $this->_db_cliente;
	
		$parametros_consulta['id'] = NULL;
		$parametros_consulta['codigo_empresa'] = NULL;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
				
		$tabelas_preco = $this->tabelas_preco_model->obter_dados($parametros_consulta);
		$codigos_tabelas_preco = array();
		foreach($tabelas_preco as $tp)
		{
			$codigos_tabelas_preco[] = array('codigo' => $tp['codigo'], 'filial_saida' =>$tp['filial_saida']);
		}
		
		return $codigos_tabelas_preco; 
		//return array('CTB'); //@TODO: Fixado pois é retornado muito produto - verificar após conclusão

	}
	
	function obter_produtos_destaque()
	{
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
		
		$produtos = $this->db_cliente->select($this->mapeamento['campos']['produtos']['codigo'] . ' AS "codigo"')
									->from($this->mapeamento['tabelas']['produtos'])
									->where($this->mapeamento['tabelas']['produtos'] . '.' . $this->mapeamento['campos']['produtos']['delecao'] . ' !=', '*')
									->where($this->mapeamento['campos']['produtos']['situacao_produto'], 'DE')
									->get()->result();
		
		return $produtos;
	}

	function obter_produto_categoria(){
		$sql = " (SELECT  DISTINCT ACU2.ACU_COD FROM ACV010 ACV INNER JOIN ACU010 ACU1 ON ACU1.D_E_L_E_T_<>'*' AND ACU1.ACU_FILIAL=ACV.ACV_FILIAL AND ACU1.ACU_COD=ACV.ACV_CATEGO INNER JOIN ACU010 ACU2 ON ACU2.D_E_L_E_T_<>'*' AND ACU2.ACU_FILIAL=ACU1.ACU_FILIAL AND ACU2.ACU_COD=ACU1.ACU_CODPAI WHERE ACV.D_E_L_E_T_<>'*' AND ACV.ACV_FILIAL=' ' AND ACV.ACV_CODPRO IN (SELECT B1.B1_COD FROM SB1010 B1 WHERE B1.D_E_L_E_T_<>'*' AND TRIM(B1.B1_FILIAL)=TRIM(SB1010.B1_FILIAL) AND B1.B1_TIPO='PA' AND TRIM(B1.B1_COD)=TRIM(SB1010.B1_COD)))  AS produto_codigo_categoria";
		
		return $sql;
	}	
	
	function obter_produtos_destaque_regiao($codigo_representante){
		$this->load->model('representantes_model');
		
		
		$representante= $this->representantes_model->exportar_representante($codigo_representante);
		
		$dados = $this->db
			->select('produto_id')
			->from('produto_destaque_regiao')
			->where('regiao_id', $representante['codigo_gestor'])
			->get()->result();
		
		$produtos_destaque = array();
		
		foreach($dados as $produto){
			$produtos_destaque[] = $produto->produto_id;	
		}
		return $produtos_destaque;
	}
}