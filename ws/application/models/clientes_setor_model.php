<?php

class Clientes_setor_model extends CI_Model {


    function __construct()
    {
        parent::__construct();		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    
	
	/**
	* Método:		exportar_ramos
	* 
	* Descrição:	Função Utilizada para retornar dados de Ramos Disponíveis
	* 
	* Data:			15/10/2013
	* Modificação:	N/A
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_clientes_setor($id = NULL, $pacote = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		
		$parametros_consulta['id'] = $id;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('DA7010');
		$parametros_consulta['tabelas_principal']		= 'DA7010';
		
		
		
		//debug_pre($parametros_consulta);
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'DA7010.R_E_C_N_O_', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Método:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que sera executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id 				= $dados['id'];
		$codigo_empresa 	= $dados['codigo_empresa'];

	
		// Campos para o SELECT
		$select = select_all($this->_db_cliente['tabelas']['clientes_setor'], $this->_db_cliente['campos']['clientes_setor'], 'clientes_setor', FALSE, 'filial');
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes_setor']['chave'] . ' =', $id);
		}
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes_setor']['delecao'] . ' !=', '*');
		}
		
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['clientes_setor']);
		
		
		
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	/**
	* Método:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
		$parametros_consulta['id'] 					= $id;
		$parametros_consulta['codigo_empresa']		= NULL;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('DA7010');
		$parametros_consulta['tabelas_principal']		= 'DA7010';
		
		
		
		
		return retornar_total($this, $parametros_consulta);
	}
	

}