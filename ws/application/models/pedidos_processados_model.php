<?php

class Pedidos_processados_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
	
	
	/**
	* Metódo:		exportar_pedidos
	* 
	* Descrição:	Função Utilizada para pegar retornar dados dos Pedidos
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_pedidos($id = NULL, $pacote = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SC6010');
		$parametros_consulta['tabelas_principal']		= 'SC6010';
		//$parametros_consulta['debug']					= $debug;
		
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'C6_NUM', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que sera executado para retornar dados
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$codigo_representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
		
		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		//$debug				 	= $dados['debug'];
		
		//var_Dump($dados); die();
		$select_itens_pedidos 		= select_all($this->_db_cliente['tabelas']['itens_pedidos'], $this->_db_cliente['campos']['itens_pedidos'], 'ip');
		$select_pedidos 			= select_all($this->_db_cliente['tabelas']['pedidos'], $this->_db_cliente['campos']['pedidos'], 'pedido');
		//debug_pre($select_pedidos);
		
		$select_clientes 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_produtos 			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_formas_pagamento 	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras 	= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		$select_itens_notas_fiscais = select_all($this->_db_cliente['tabelas']['itens_notas_fiscais'], $this->_db_cliente['campos']['itens_notas_fiscais'], 'inf');
		
		$select = array_merge(
			$select_itens_pedidos, 
			$select_pedidos,
			$select_clientes,
			$select_produtos,
			$select_formas_pagamento,
			$select_transportadoras,
			$select_itens_notas_fiscais
		);
		
		
		
		// Sttaus do Pedido
		$select[] = "(SELECT
		(CASE 
		WHEN ( SUM(SC9010.R_E_C_N_O_) > 0) THEN 'rejeitado_credito' 
		WHEN SUM(" . $this->_db_cliente['campos']['itens_pedidos']['quantidade_faturada_produto'] . ") <= 0 THEN 'aguardando_faturamento' 
		WHEN SUM(C6_QTDENT) < SUM(" . $this->_db_cliente['campos']['itens_pedidos']['quantidade_faturada_produto'] . ") THEN 'parcialmente_faturado' 
		
		ELSE 'faturado' END) AS status
		FROM " . $this->_db_cliente['tabelas']['itens_pedidos'] . " 
		WHERE " . $this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'] . " = " . $this->_db_cliente['campos']['pedidos']['codigo'] . " AND " . 
		euf(
			$this->_db_cliente['tabelas']['itens_pedidos'], $this->_db_cliente['campos']['itens_pedidos']['filial'], 
			$this->_db_cliente['tabelas']['pedidos'], $this->_db_cliente['campos']['pedidos']['filial']
		) . 
		" D_E_L_E_T_ != '*'
		) AS status";
		
		$select += formatar_euf($this->_db_cliente['tabelas']['itens_pedidos'], $this->_db_cliente['campos']['itens_pedidos']['filial'], $codigo_empresa);
		
		
		
		// Join
		//-----------------------------------------------
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['pedidos'], 
			euf($this->_db_cliente['tabelas']['pedidos'], $this->_db_cliente['campos']['pedidos']['filial'], $this->_db_cliente['tabelas']['itens_pedidos'], $this->_db_cliente['campos']['itens_pedidos']['filial']) .
			$this->_db_cliente['campos']['pedidos']['codigo'] . " = " . $this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'] . " AND " .
			$this->_db_cliente['campos']['pedidos']['codigo_representante'] . " = '" . $codigo_representante . "' AND " .
			$this->_db_cliente['tabelas']['pedidos'] . "." . $this->_db_cliente['campos']['pedidos']['delecao'] . " != '*'"
		);
		
		//	Customização 002514 - [CUSTOM] Melhoria do DW Força de Vendas
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['pedidos_liberados'], 
			$this->_db_cliente['tabelas']['pedidos_liberados'] . "." . $this->_db_cliente['campos']['pedidos_liberados']['delecao'] . " != '*'" . ' AND ' .
			$this->_db_cliente['campos']['pedidos']['filial'] . " = " . $this->_db_cliente['campos']['pedidos_liberados']['codigo_filial']. " AND " .
			$this->_db_cliente['campos']['pedidos']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_liberados']['codigo_pedido'] . " AND " .
			$this->_db_cliente['campos']['itens_pedidos']['codigo_produto'] . " = " . $this->_db_cliente['campos']['pedidos_liberados']['codigo_produto'] . " AND " .
			$this->_db_cliente['campos']['pedidos_liberados']['situacao'] . " = '09' "	,'LEFT'
		);
		
		// FIM 	Customização 002514 - [CUSTOM] Melhoria do DW Força de Vendas
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['clientes'], 
			euf($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], $this->_db_cliente['tabelas']['pedidos'], $this->_db_cliente['campos']['pedidos']['filial']) .
			$this->_db_cliente['campos']['clientes']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos']['codigo_cliente'] . " AND " .
			$this->_db_cliente['campos']['clientes']['loja'] . " = " . $this->_db_cliente['campos']['pedidos']['loja_cliente'] . " AND " .
			$this->_db_cliente['tabelas']['clientes'] . "." . $this->_db_cliente['campos']['clientes']['delecao'] . " != '*'"
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], $this->_db_cliente['tabelas']['itens_pedidos'], $this->_db_cliente['campos']['itens_pedidos']['filial']) .
			$this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['itens_pedidos']['codigo_produto'] . " AND " .
			$this->_db_cliente['tabelas']['produtos'] . "." . $this->_db_cliente['campos']['produtos']['delecao'] . " != '*'"
		, 'left');
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], $this->_db_cliente['tabelas']['pedidos'], $this->_db_cliente['campos']['pedidos']['filial']) .
			$this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos']['codigo_forma_pagamento'] . " AND " .
			$this->_db_cliente['tabelas']['formas_pagamento'] . "." . $this->_db_cliente['campos']['formas_pagamento']['delecao'] . " != '*'"
		, 'left');
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], $this->_db_cliente['tabelas']['pedidos'], $this->_db_cliente['campos']['pedidos']['filial']) .
			$this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos']['codigo_transportadora'] . " AND " .
			$this->_db_cliente['tabelas']['transportadoras'] . "." . $this->_db_cliente['campos']['transportadoras']['delecao'] . " != '*'"
		, 'left');
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['itens_notas_fiscais'], 
			euf($this->_db_cliente['tabelas']['itens_notas_fiscais'], $this->_db_cliente['campos']['itens_notas_fiscais']['filial'], $this->_db_cliente['tabelas']['itens_pedidos'], $this->_db_cliente['campos']['itens_pedidos']['filial']) .
			$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_produto'] . " = " . $this->_db_cliente['campos']['itens_pedidos']['codigo_produto'] . " AND " .
			$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_pedido'] . " = " . $this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'] . " AND " .
			$this->_db_cliente['tabelas']['itens_notas_fiscais'] . "." . $this->_db_cliente['campos']['itens_notas_fiscais']['delecao'] . " != '*'"
		, 'left');
		
		// Where
		//-----------------------------------------------
		
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['itens_pedidos']['codigo_pedido'] . ' >', $id);
		}
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos'] . '.' . $this->_db_cliente['campos']['pedidos']['data_emissao'] . " >= ", date('Ymd', PERIODO_DADOS));	
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['itens_pedidos'] . '.' . $this->_db_cliente['campos']['itens_pedidos']['delecao'] . ' !=', '*');
		}
		
		
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['itens_pedidos']);
		
		
		//	debug_pre($this->db_cliente->_compile_select());
		
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		$parametros_consulta['codigo_empresa']		 = NULL;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SC6010');
		$parametros_consulta['tabelas_principal']		= 'SC6010';
	
		return retornar_total($this, $parametros_consulta);
	}
	

}