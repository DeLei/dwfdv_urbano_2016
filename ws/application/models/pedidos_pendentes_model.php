<?php

class Pedidos_pendentes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
    }
	
	
	/**
	* Metódo:		exportar_pedidos
	* 
	* Descrição:	Função Utilizada para pegar retornar dados dos Pedidos
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_pedidos($id = NULL, $pacote = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('ZB7010');
		$parametros_consulta['tabelas_principal']		= 'ZB7010';
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, $this->mapeamento['campos']['pedidos_dw']['id_pedido'], $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que sera executado para retornar dados
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$codigo_representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
		
		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		$select_pedido 				= select_all($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw'], 'pedido',FALSE,'observacao_comercial');
		$select_cliente 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_produto 			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_formas_pagamento 	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras 	= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		$select_tipo_transporte 	= select_all($this->_db_cliente['tabelas']['generica'], $this->_db_cliente['campos']['generica'], 'tp_trans');
		$select_porto_destino 		= select_all($this->_db_cliente['tabelas']['portos_aeroportos'], $this->_db_cliente['campos']['portos_aeroportos'], 'porto');
		
		//chamado: 002121 - Obs Pedido
		
		$select_pedido[] = 'TRIM(CONCAT(ZB7010.ZB7_OBSCOM,ZB7010.ZB7_OBSCO2)) AS pedido_observacao_comercial';
		
		
		
		$select = array_merge(
			$select_pedido, 
			$select_cliente,
			$select_produto,
			$select_formas_pagamento,
			$select_transportadoras,
			$select_tipo_transporte,
			$select_porto_destino
		);
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = Não Exportado)
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
		$select[] = "'0' as codigo_erro";
		$select[] = "'0' as converter_pedido_orcamento";
		$select[] = "'".date('YmdHis')."' as atualizado_em";
		
		$select += formatar_euf($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial'], $codigo_empresa);

		$select['F'] = $this->_db_cliente['campos']['pedidos_dw']['filial'] .' AS '. 'filial';
		
		// Where
		//-----------------------------------------------
		
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'] . ' >', $id);
		}
		
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $codigo_representante);
		
		$status = array('A', 'R','P','L');
		if(isset($dados['incremental']) && $dados['incremental'] == 1)
		{
			//$status[] = 'I';
		}
		
		$this->db_cliente->where_in($this->_db_cliente['campos']['pedidos_dw']['status'], $status);
		
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo_venda'] . ' !=', '*'); // Buscar Pedidos (* é orçamento)
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['delecao'] . ' !=', '*');
		}
		//-----------------------------------------------
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['clientes'], 
			euf(
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['clientes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'] . ' AND ' .
			$this->_db_cliente['campos']['clientes']['loja'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'] . ' AND ' .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['codigo_produto'] . " AND " .
			$this->_db_cliente['campos']['produtos']['filial'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['filial'] . " AND " .
			$this->_db_cliente['tabelas']['produtos'] . "." . $this->_db_cliente['campos']['produtos']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['condicao_pagamento'] . " AND " .
			$this->_db_cliente['tabelas']['formas_pagamento'] . "." . $this->_db_cliente['campos']['formas_pagamento']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf(
				$this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['codigo_transportadora'] . " AND " .
			$this->_db_cliente['tabelas']['transportadoras'] . "." . $this->_db_cliente['campos']['transportadoras']['delecao'] . " != '*'"
		, 'left');
		
		
		//CUSTOM - 000177/000870
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['generica'], 
			euf(
				$this->_db_cliente['tabelas']['generica'], $this->_db_cliente['campos']['generica']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['generica']['chave'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['tipo_transporte'] . " AND " .
			$this->_db_cliente['campos']['generica']['filial'] . " = '01' AND " .
			$this->_db_cliente['campos']['generica']['tabela'] . " = 'M5' AND " .
			$this->_db_cliente['tabelas']['generica'] . "." . $this->_db_cliente['campos']['transportadoras']['delecao'] . " != '*'"
		, 'left');
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['portos_aeroportos'], 
			euf(
				$this->_db_cliente['tabelas']['portos_aeroportos'], $this->_db_cliente['campos']['portos_aeroportos']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['portos_aeroportos']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['porto_destino'] . " AND " .
			$this->_db_cliente['tabelas']['portos_aeroportos'] . "." . $this->_db_cliente['campos']['portos_aeroportos']['delecao'] . " != '*'"
		, 'left');
		//FIM CUSTOM - 000177/000870
		
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['pedidos_dw']);
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		$parametros_consulta['codigo_empresa']		 = NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	//--------------
	//------------------
	//--------------------
	//------------------
	//--------------
	/**
	* Metódo:		importar
	* 
	* Descrição:	Função Utilizada para inserir pedidos no banco, e inserir LOGS
	* 
	* Data:			08/09/2012
	* Modificação:	08/09/2012
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Pedidos enviados pelo DW força de vendas
	* @param		string 		$id_usuario					- ID do usuário
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('pedidos_dw', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			$pedidos = json_decode($dados);
			
			foreach($pedidos as $pedido)
			{
				
				if($this->verificar_cliente_bloqueado($pedido->cliente_codigo, $pedido->cliente_loja))
				{
					if($this->verificar_produto_bloqueado($pedido->pedido_codigo_produto, $pedido->pedido_filial)){
						try{
							
							if($this->validar_pedido($pedido->pedido_id_pedido, $pedido->pedido_codigo_produto))
							{
								
								$pedido->editado = 1;
							}
							
							// Se editado == 1 - Editar o Pedidos, senao, inserir
							if($pedido->editado == 1)
							{
								
								$valores = $this->obter_campos_valores($pedido,false);	
								
								
								$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['id_pedido'], $pedido->pedido_id_pedido);
								$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['codigo_produto'], $pedido->pedido_codigo_produto);
								$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['status']. ' != ', 'I');
								$this->db_cliente->update($this->mapeamento['tabelas']['pedidos_dw'], $valores);
								
								
							}else{
								$valores = $this->obter_campos_valores($pedido);	
								$this->db_cliente->insert($this->mapeamento['tabelas']['pedidos_dw'], $valores);
							}
						}
						catch(Exception $e)
						{
							$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($pedido),$id_usuario, $codigo_representante); 
						}
					
					}else
					{				
						$this->sincronizacoes_model->salvar_erro($id_sincronizacao, 'Produto ' . $pedido->pedido_codigo_produto . ' - ' . $pedido->produto_descricao . ' bloqueado.', json_encode($pedido),$id_usuario, $codigo_representante); 
					}
				}else
				{
				
				
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, 'Cliente ' . $pedido->cliente_nome . ' - ' . $pedido->cliente_cpf . ' bloqueado.', json_encode($pedido),$id_usuario, $codigo_representante); 
				}	
			}
			
			
			
		}
		
		// Marcando o orçamento convertido para pedido como deletado
		if($pedido->converter_pedido_orcamento)
		{
			try{
				$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['id_pedido'], $pedido->pedido_id_pedido);
				$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['tipo_venda'], '*');
				$this->db_cliente->delete($this->mapeamento['tabelas']['pedidos_dw']);
			
			}
			catch(Exception $e)
			{
				$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($pedido),$id_usuario, $codigo_representante); 
			}	
		}
		
		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
			
		if($dados_erros)
		{
			$codigos_erro = array();
		
			foreach($dados_erros as $dado_erro)
			{
				$dados_json = json_decode($dado_erro->dados);
			
				if (!in_array($dados_json->pedido_id_pedido, $codigos_erro)) 
				{ 
					$codigos_erro[] = $dados_json->pedido_id_pedido;
					if(strpos($dado_erro->mensagem,'bloqueado') > -1){
						$nome_erro[] = $dado_erro->mensagem;
					}else{					
						$nome_erro[] = $dados_json->pedido_id_pedido . ' - ' . $dados_json->cliente_nome;
					}
				}
			}
			
			$erros['erro'] = $codigos_erro;
			$erros['erro_descricao'] = $nome_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');
		}
		die('teste');
	}
	
	function validar_pedido($id_pedido, $codigo_produto)
	{
		$pedido = $this->db_cliente->select($this->mapeamento['campos']['pedidos_dw']['id_pedido'])
									->from($this->mapeamento['tabelas']['pedidos_dw'])
									->where(array(
										$this->mapeamento['campos']['pedidos_dw']['id_pedido'] 		=> $id_pedido,
										$this->mapeamento['campos']['pedidos_dw']['codigo_produto'] => $codigo_produto
									))
									->limit(1)->get()->result();
		
		return $pedido;
	}
	
	
	/**
	* Metódo:		obter_campos_valores
	* 
	* Descrição:	Função Utilizada para retornar os campo com valores
	* 
	* Data:			21/09/2012
	* Modificação:	21/09/2012
	* 
	* @access		public
	* @param		array 		$pedido				- Dados dos Pedidos 
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_campos_valores($pedido, $gerar_recno = TRUE)
	{
		//Removendo observação do cliente, não utilizada e gerando problema com a observaçao do pedido.
		unset($pedido->observacao_comercial);
		foreach($pedido as $indice => $valor)
		{
			
			$indice = str_replace("pedido_", "", $indice, $count);
			
			if($gerar_recno)
		   {
				if($indice == 'chave') // gerar Recno
				{
				$valor = $this->gerar_recno();
				}
		   }
			
			
			if($count > 1)
			{
				$indice = "pedido_" . $indice;
			}

			if($indice == 'chave') // gerar Recno
			{
				$valor = $this->gerar_recno();
			}

			
			//Chamado: 002121 - Obs Pedido
			if($indice == 'observacao_comercial') // gerar Recno
			{
				
				if(strlen($valor)> 250){
				
					$valores['ZB7_OBSCO2'] =  substr($valor, 250);
					$valor = (substr($valor,0, 250));
				}
				
				
			}
			
			//debug_pre($valor. ' - '. strlen($valor)."\n",false);
			if(!empty($this->mapeamento['campos']['pedidos_dw'][$indice]))
			{
				
				if(empty($valor)) // Não podemos inserir valor NULL, se for NULL inserir em branco
				{						
					$valor = ' ';
				}
				
				if(strtoupper($valor) == 'UNDEFINED') // Não podemos inserir valor NULL, se for NULL inserir em branco
				{
					
					$valor = ' ';
				}
				
				if(in_array($indice, array('ipi','st','icms','longitude','latitude','time_emissao', 'desconto_item', 'desconto1', 'desconto2', 'desconto3', 'desconto4', 'dirigido_redespacho', 'despesa')))
				{
					
					$valor = (int) $valor;
				}
				
				
				if($indice == 'total_desconto_item') //  Calculando o valor do desconto
				{
					
					//Aplicar desconto em cima do preço de tabela que pedido_preco_unitario
					if($pedido->pedido_desconto1 > 0)
					{
						$pedido->pedido_preco_unitario = $pedido->pedido_preco_unitario - ($pedido->pedido_preco_unitario * ($pedido->pedido_desconto1 / 100));
					}
					
					//Calcular o valor do desconto
					$valor = $pedido->pedido_quantidade * ($pedido->pedido_preco_unitario * ($pedido->pedido_desconto_item / 100));
				}
				
				
				
			//	debug_pre('indice: '.$indice.' - '. $valor .' <br/>' ,false); 
			
				
			
				$valores[$this->mapeamento['campos']['pedidos_dw'][$indice]] = $valor;
			}
			
		}
		
		//CUSTOM - 000177/000870
		$valores[$this->mapeamento['campos']['pedidos_dw']['comissao']] = '0';
		$valores[$this->mapeamento['campos']['pedidos_dw']['comissao2']] = '0';
		$valores[$this->mapeamento['campos']['pedidos_dw']['comissao3']] = '0';
		$valores[$this->mapeamento['campos']['pedidos_dw']['comissao4']] = '0';
		$valores[$this->mapeamento['campos']['pedidos_dw']['comissao5']] = '0';
		$valores[$this->mapeamento['campos']['pedidos_dw']['tipo_entrada_saida']] = '0';
		//FIM CUSTOM - 000177/000870
		
		//$valores[$this->mapeamento['campos']['pedidos_dw']['versao']] = VERSAO;
		
		$valores[$this->mapeamento['campos']['pedidos_dw']['delecao']] = ' ';
		
		return $valores;
	}
	
	
	function gerar_recno()
	{
	
		$dados = $this->db_cliente
			->select('MAX(' . $this->mapeamento['campos']['pedidos_dw']['chave'] . ')+1 AS chave')
			->from($this->mapeamento['tabelas']['pedidos_dw'])
			->get()->row_array();
			
	
		if($dados['chave'])
		{
			return $dados['chave'];
		}
		else
		{
			return 1;
		}
	
	}

	/**
	* Método:		verificar_cliente_bloqueado
	* 
	* Descrição:	Função Utilizada para verificar se o cliente do pedido encontra-se bloqueado
	* 
	* Data:			31/07/2014
	* Modificação:	31/07/2014
	* 
	* @access		public
	* @param		string 		$codigo_cliente				- Código do Cliente
	* @param		string 		$loja_cliente				- Código do Loja
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function verificar_cliente_bloqueado($codigo_cliente, $loja_cliente)
	{
		$dados = NULL;
		
		if($codigo_cliente  && $loja_cliente)
		{
			$dados = $this->db_cliente
				->select($this->mapeamento['campos']['clientes']['codigo'] . ' AS codigo')
				->select($this->mapeamento['campos']['clientes']['nome_fantasia'] . ' AS nome')
				->from($this->mapeamento['tabelas']['clientes'])
				->where($this->mapeamento['campos']['clientes']['codigo'], $codigo_cliente)
				->where($this->mapeamento['campos']['clientes']['loja'], $loja_cliente)
				->where($this->mapeamento['campos']['clientes']['delecao'] . ' !=', '*')
				->where($this->mapeamento['campos']['clientes']['situacao'] . ' !=', '2')
				->get()->row_array();
		}
		
		if(isset($dados['codigo']))
		{
			return array('codigo_cliente' => $dados['codigo'], 'nome_cliente' => $dados['nome']);
		}
		else
		{
			return FALSE;
		}
	}
	
	
	/**
	* Método:		verificar_produto_bloqueado
	* 
	* Descrição:	Função Utilizada para verificar se o produto do pedido encontra-se bloqueado ou excluído
	* 
	* Data:			10/10/2014
	* Modificação:	10/10/2014
	* 
	* @access		public
	* @param		string 		$codigo_produto				- Código do Produto
	* @param		string 		$codigo_filial				- Código do Filial
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function verificar_produto_bloqueado($codigo_produto, $codigo_filial)
	{
		$dados = NULL;
		
		if($codigo_produto  && $codigo_filial)
		{
			$dados = $this->db_cliente
				->select($this->mapeamento['campos']['produtos']['codigo'] . ' AS codigo')
				->from($this->mapeamento['tabelas']['produtos'])
				->where($this->mapeamento['campos']['produtos']['codigo'], $codigo_produto)
				->where($this->mapeamento['campos']['produtos']['filial'], $codigo_filial)
				->where($this->mapeamento['campos']['produtos']['delecao'] . ' !=', '*')
				->where($this->mapeamento['campos']['produtos']['inativo'] . ' !=', '1')
				->get()->row_array();
		}
		
		if(isset($dados['codigo']))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	
	function obter_valores($pedido)
	{
		$valores = array();
		$produtos_bloqueados = array();
		
		$recno = $this->gerar_recno();
		foreach($pedido as $key => $item)
		{
			foreach($this->mapeamento['campos']['pedidos_dw'] as $indice => $campo)
			{
				
							
				
				if($indice == 'codigo_produto')
				{
					if(!$this->verificar_produto_bloqueado($item->pedido_codigo_produto, $item->pedido_filial))
					{
						$produtos_bloqueados[] = array('codigo_produto' => $item->pedido_codigo_produto, 'descricao_produto' => $item->produto_descricao);
					}
				}

				if($indice == 'delecao')
				{
					if($item->{'dw_delecao'}){
						$valores[$key][$campo] = '*';
					}else{
						$valores[$key][$campo] = ' ';
					}
				}
				else
				{
					$valor = $item->{'pedido_' . $indice};
					
					if($indice == 'observacao_comercial'){
						
						if(strlen($valor) > 250){	
							$valores[$key]['ZB7_OBSCO2'] = substr($valor,250);
							$valor = substr($valor, 0 , 250);
						}else{
							$valores[$key]['ZB7_OBSCO2'] = ' ';
						}
					
					}
					

					if(empty($valor)) // Não podemos inserir valor NULL, se for NULL inserir em branco
					{
						$valor = ' ';
					}
					
					if(strtoupper($valor) == 'UNDEFINED') // Não podemos inserir valor NULL, se for NULL inserir em branco
					{
						$valor = ' ';
					}
					
					if(in_array($indice, array('ipi','st','icms','quantidade','time_emissao', 'desconto_item', 'desconto1', 'desconto2', 'desconto3', 'desconto4', 'dirigido_redespacho', 'despesa')))
					{
						$valor = (int) $valor;
					}
					
					
					if(in_array($indice, array('time_importacao'))) {
						$valor = (int) time();
					}
					
					if(in_array($indice, array('latidude', 'longitude'))) {
						$valor = (string) $valor;
					}
					
					if(in_array($indice, array('preco_unitario', 'preco_venda', 'peso'))) {
						$valor = (float) $valor;
					}
					
					if($indice == 'total_desconto_item') //  Calculando o valor do desconto
					{
						
						//Aplicar desconto em cima do preço de tabela que pedido_preco_unitario
						if($item->pedido_desconto1 > 0)
						{
							$item->pedido_preco_unitario = $item->pedido_preco_unitario - ($item->pedido_preco_unitario * ($item->pedido_desconto1 / 100));
						}
						
						//Calcular o valor do desconto
						$valor = $item->pedido_quantidade * ($item->pedido_preco_unitario * ($item->pedido_desconto_item / 100));
					}
					
					if($indice == 'chave')
					{
						$valores[$key][$campo] = $recno;
					}
					elseif(in_array($indice, array('comissao', 'comissao2', 'comissao3', 'comissao4', 'comissao5', 'tipo_entrada_saida')))
					{
						if($indice == 'tipo_entrada_saida') {
							$valores[$key][$campo] = '0';
						} else {
							//CUSTOM - 000177/000870
							$valores[$key][$campo] = 0;
							//FIM CUSTOM - 000177/000870
						}
					}
					else
					{
						$valores[$key][$campo] = $valor;
					}
				}
			}
			$recno++;
		}
		//var_dump($valores);die;
		return array('valores' => $valores, 'produtos_bloqueados' => $produtos_bloqueados);
	}
	
	
	function importar_novo($dados, $id_usuario, $codigo_representante)
	{
			
	//	debug_pre($dados);
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('pedidos_dw', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			try
			{
				$dados = json_decode($dados);
				$pedido = $this->obter_valores($dados);
				
				
				if($pedido['produtos_bloqueados']) //Produtos bloqueados
				{
					$produtos_bloqueados = 'Os produtos abaixo encontram-se <b>bloqueados</b>: <p>';
					foreach($pedido['produtos_bloqueados'] as $produto) {
						$produtos_bloqueados .= '<b>' . $produto['codigo_produto'] . '</b> - ' . $produto['descricao_produto'] . '<br/>';
					}
					$produtos_bloqueados .= '</p>';
					
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $produtos_bloqueados, json_encode($pedido),$id_usuario, $codigo_representante); 
				}
				else
				{

					$primeiro_item = $pedido['valores'];
					$primeiro_item = array_shift($primeiro_item); //Pega o primeiro item do pedido para obter os valores iguais
					
					//Obtem o codigo/loja do cliente
					$codigo_cliente = $primeiro_item[$this->mapeamento['campos']['pedidos_dw']['codigo_cliente']];
					$loja_cliente = $primeiro_item[$this->mapeamento['campos']['pedidos_dw']['loja_cliente']];
					
					//Codigo do pedido
					$id_pedido = $primeiro_item[$this->mapeamento['campos']['pedidos_dw']['id_pedido']];
					$tabela_precos = $primeiro_item[$this->mapeamento['campos']['pedidos_dw']['tabela_precos']];
					
					$cliente_bloqueado = $this->verificar_cliente_bloqueado($codigo_cliente, $loja_cliente);
					
					if(!$cliente_bloqueado) //Cliente desbloqueado, grava o pedido
					{
						if($this->validar_tabela_precos($primeiro_item))
						{
						
							$this->db_cliente->trans_start(); //Inicia a transação com o BD
								
								$validar_pedido = $this->pedido_existe($id_pedido);
								$inserir_pedido = true;
								
								if($validar_pedido)
								{
									if($validar_pedido->status == 'I' )
									{
										$inserir_pedido = false;
										$this->sincronizacoes_model->salvar_erro($id_sincronizacao, 'O Pedido <b>' . $id_pedido . '</b> ja foi importado para o ERP.', json_encode($pedido), $id_usuario, $codigo_representante, '500');
									}
									else
									{
										//Marca como deletado o pedido anterior e reinsere o pedido editado
										if($validar_pedido->codigo_pedido)
										{
											$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['id_pedido'], $id_pedido);
											$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['status']. ' != ', 'I');
											$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['delecao']. ' != ', '*');
											$this->db_cliente->update($this->mapeamento['tabelas']['pedidos_dw'], array($this->mapeamento['campos']['pedidos_dw']['delecao'] => '*'));
										}
									}
								}
								
								if($inserir_pedido)
								{
									$this->db_cliente->insert_batch($this->mapeamento['tabelas']['pedidos_dw'], $pedido['valores']); //Realiza o insert do pedido em batch
								}
								
							$this->db_cliente->trans_complete(); //Fecha a transação
							
						}
						else
						{
						
							$this->sincronizacoes_model->salvar_erro($id_sincronizacao, 'A Tabela de pre&ccedil;os <b>' . $tabela_precos . '</b> encontra-se inativa.', json_encode($pedido), $id_usuario, $codigo_representante); 
							
						}
					}
					else //Cliente bloqueado, exibe a mensagem e não salva o pedido
					{
					
						$this->sincronizacoes_model->salvar_erro($id_sincronizacao, 'O Cliente <b>' . $cliente_bloqueado['codigo_cliente'] . ' - ' . $cliente_bloqueado['nome_cliente'] . '</b> encontra-se bloqueado.', json_encode($pedido), $id_usuario, $codigo_representante); 
					
					}
				}
			}
			catch(Exception $e)
			{
			
				$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($pedido), $id_usuario, $codigo_representante); 
				
			}
		}
		
		
		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
		
		if($dados_erros)
		{
			return array('erro' => true, 'mensagem' => $dados_erros[0]->mensagem , 'codigo_erro' => $dados_erros[0]->codigo_erro );
		}
		else
		{
			return array('sucesso' => 'ok');
		}
	}
	
	private function pedido_existe($id_pedido) {
		$pedido = $this->db_cliente->select($this->mapeamento['campos']['pedidos_dw']['id_pedido'] . ' AS "codigo_pedido"')
									->select($this->mapeamento['campos']['pedidos_dw']['status'] . ' AS "status"')
									->from($this->mapeamento['tabelas']['pedidos_dw'])
									->where($this->mapeamento['campos']['pedidos_dw']['id_pedido'], $id_pedido)
									//->where_in($this->mapeamento['campos']['pedidos_dw']['status'] , array('L','I'))
									->where($this->mapeamento['campos']['pedidos_dw']['delecao'] . ' !=', '*')
									->limit(1)->get()->row();
		
		return $pedido;
	}
	
	private function validar_tabela_precos($item)
	{
		$filial = $item[$this->mapeamento['campos']['pedidos_dw']['filial']];
		$codigo_tabela = $item[$this->mapeamento['campos']['pedidos_dw']['tabela_precos']];
		
		$dataAtual = date('Ymd');
		
		$tabela = $this->db_cliente->from($this->mapeamento['tabelas']['tabelas_precos'])
									->where($this->mapeamento['campos']['tabelas_precos']['filial'], $filial)
									->where($this->mapeamento['campos']['tabelas_precos']['codigo'], $codigo_tabela)
									->where($this->mapeamento['campos']['tabelas_precos']['ativo'], '1')
									->where($this->mapeamento['campos']['tabelas_precos']['delecao'] . '!=', '*')
									->where($this->mapeamento['campos']['tabelas_precos']['vigencia_inicio'] . ' <=', $dataAtual)
									->where("(" . $this->mapeamento['campos']['tabelas_precos']['vigencia_final'] . " >= '" . $dataAtual . "' OR " . $this->mapeamento['campos']['tabelas_precos']['vigencia_final'] . " = ' ')")
									->get()->num_rows();
		
		return $tabela;
	}
	
	
}