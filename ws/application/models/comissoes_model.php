<?php

class Comissoes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
	
	function exportar_comissoes($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
		
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['ignorar_incremental']		= true;
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'NUMERO_TITULO', $parametros_consulta);
		
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{

		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		$select = select_all($this->_db_cliente['tabelas']['comissoes_representantes'], $this->_db_cliente['campos']['comissoes_representantes'], NULL, FALSE);	
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['representantes_comissoes']['recno'] . ' >', $id);
		}

		// Consulta
		/*  
			Conforme solicitação durante a visita 03/07/2014
			Serão obtidas comissões sempre de acordo com a data atual -1 um ano.
			//
			Reduzido o periodo de comissões para 3 meses
		*/
		$data_menos_ano = date('Ymd',strtotime("-3 months", time()));
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['comissoes_representantes'].".".$this->_db_cliente['campos']['comissoes_representantes']['data_emissao']." >= ", $data_menos_ano);		
		$this->db_cliente->where($this->_db_cliente['tabelas']['comissoes_representantes'].".".$this->_db_cliente['campos']['comissoes_representantes']['codigo_representante'], $codigo_representante);
		
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['comissoes_representantes']);
		//debug_pre($this->db_cliente->_compile_select());
	
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
	
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
}