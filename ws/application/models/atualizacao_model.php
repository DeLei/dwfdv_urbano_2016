<?php

class Atualizacao_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	 * váriavel com as mensagens da verificação de versão
	 *
	 * @versoes = array com as msgs
	 */
	public  $versoes = array(
		'001' => 'Existe uma nova vers&atilde;o.', //&atilde;
		'002' => 'Sua vers&atilde;o est&aacute; atualizada.', //&aacute;
		'003' => 'Vers&atilde;o do seu aparelho &eacute; superior ao do servidor, por favor avise a equipe t&eacute;cnica.', //&eacute;
		'004' => 'Sem informa&ccedil;&oes.', //&cctilde;edil;
	);
	
	/**
	 * Função chamada pelo JS. 
	 * ----------------------------------------------------------------------
	 * @method 					verificar_versao	 
	 * @return 					json
	 * ----------------------------------------------------------------------
	 */
	public function verificar_versao($aparelho_versao = 0, $usuario_id = 0)
	{
		
		
		//Última versão
		$ultima_versao = $this->_verificar_versao($aparelho_versao,$usuario_id);
		
		//Encode
		$ultima_versao['versao']->chamados			= utf8_encode($ultima_versao['versao']->chamados);
		$ultima_versao['versao']->descricao			= utf8_encode($ultima_versao['versao']->descricao);
		$ultima_versao['mensagem']					= ($ultima_versao['mensagem']);
		
		//Formatar datas
		$ultima_versao['versao']->data_cadastro 	= date('d/m/Y', $ultima_versao['versao']->data_cadastro);
		$ultima_versao['versao']->data_alteracao 	= date('d/m/Y', $ultima_versao['versao']->data_alteracao);
		
		return $ultima_versao;
		
	}
	
	public function _verificar_versao($aparelho_versao, $usuario_id)
	{
	
	
		//Obtem região do representante
		
		$usuario = $this->db->from('usuarios')->where('id', $usuario_id)->get()->row();
		
		$codigo_regiao = $usuario->gerente_representante;
		
	
		//Retorno do JSON
		$json 				= array();
		
		
		//LEFT JOIN cfg_versoes_regiao ON `versao_id` = cfg_versoes.ID
		
		//Consultar no banco de dados o último cadastro
		$ultima_versao 	= 
			$this->db->select('cfg_versoes.*')
			->from('cfg_versoes')
			->join('cfg_versoes_regiao','versao_id = cfg_versoes.id', 'LEFT')
			->where('status', 1)
			->where('regiao_id', $codigo_regiao)
			->order_by("data_cadastro", "desc")->limit(1)->get()->row();
		
		//Versões
		$json['versao'] = $ultima_versao;
		
	
		
		//Verificar se a versão é forçar para atualização
		if($ultima_versao->forcar == 1)
		{
			$json['forcar'] = TRUE;
		}
		else
		{
			$json['forcar'] = FALSE;
		}		
		
		//Verificar as versões
		if(version_compare($aparelho_versao, $ultima_versao->versao, '<') )
		{
			$json['situacao'] 	= '001';
			$json['mensagem'] = $this->versoes['001'];
		}
		else if(version_compare($aparelho_versao, $ultima_versao->versao, '==') )
		{
			$json['situacao'] 	= '002';
			$json['mensagem'] = $this->versoes['002'];
		}
		else if(version_compare($aparelho_versao, $ultima_versao->versao, '>') )
		{
			$json['situacao'] 	= '003';
			$json['mensagem'] = $this->versoes['003'];
		}
		else
		{
			$json['situacao'] 	= '004';
			$json['mensagem'] = $this->versoes['004'];
		}
		
		//Retornar json
		return $json;		
	}	

}