<?php

class Motivos_troca_model extends CI_Model {


    function __construct()
    {
        parent::__construct();		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    
	
	/**
	* Método:		exportar_ramos
	* 
	* Descrição:	Função Utilizada para retornar dados de Motios de Troca
	* 
	* Data:			15/10/2013
	* Modificação:	N/A
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_motivos_troca($id = NULL, $pacote = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		
		$parametros_consulta['id'] = $id;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SX5010');
		$parametros_consulta['tabelas_principal']		= 'SX5010';
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'X5_CHAVE', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Método:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que sera executado para retornar motivos de troca
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id 				= $dados['id'];
		$codigo_empresa 	= $dados['codigo_empresa'];

	
		// Campos para o SELECT
		$select = select_all($this->_db_cliente['tabelas']['generica'], $this->_db_cliente['campos']['generica'], NULL, FALSE, 'troca');
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['generica']['chave'] . ' =', $id);
		}
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['generica']['delecao'] . ' !=', '*');
		}
		
		//$this->db_cliente->where($this->_db_cliente['campos']['generica']['filial'] . '', '01');
		$this->db_cliente->where($this->_db_cliente['campos']['generica']['tabela'] . '', '88'); 
		
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['generica']);
	}
	
	/**
	* Método:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de motivos de troca
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
		$parametros_consulta['id'] 					= $id;
		$parametros_consulta['codigo_empresa']		= NULL;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SX5010');
		$parametros_consulta['tabelas_principal']		= 'SX5010';
		
		return retornar_total($this, $parametros_consulta);
	}
	

}