<?php

class Prospects_processados_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
	
	function exportar_prospects_processados($id = NULL, $pacote = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SUS010');
		$parametros_consulta['tabelas_principal']		= 'SUS010';
		
		$parametros_consulta['prospects_aguardando_analise'] = $this->obter_prospects_aguardando_analise($codigo_representante);
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'US_COD', $parametros_consulta);
		
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados)
    {
        
		
		
		$id                   = NULL;
        $codigo_representante = $dados['codigo_representante'];
        $codigo_empresa       = $dados['codigo_empresa'];
		$prospects 			  = $dados['prospects_aguardando_analise'];			
		
		
		
        // Campos para o SELECT
        $select = select_all($this->_db_cliente['tabelas']['prospects_processados'], $this->_db_cliente['campos']['prospects_processados'], NULL, FALSE, 'filial');


        $select += formatar_euf($this->_db_cliente['tabelas']['prospects_processados'], $this->_db_cliente['campos']['prospects_processados']['filial'], $codigo_empresa);


        // Condições do SQL (WHERE)
        if ($id)
        {
            $this->db_cliente->where($this->_db_cliente['tabelas']['prospects_processados'] . '.' . $this->_db_cliente['campos']['prospects_processados']['codigo'] . ' >', $id);
        }
		
	
	
		if($prospects) {
			$this->db_cliente->where_not_in($this->_db_cliente['campos']['prospects_processados']['cgc'], $prospects);
		}
		
        $this->db_cliente->where($this->_db_cliente['campos']['prospects_processados']['codigo_representante'], $codigo_representante);

		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['prospects_processados'] . '.' . $this->_db_cliente['campos']['prospects_processados']['delecao'] . ' !=', '*');
		}


        // Consulta
        $this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['prospects_processados']);
        //debug_pre($this->db_cliente->_compile_select());
       
    }
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
	
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SUS010');
		$parametros_consulta['tabelas_principal']		= 'SUS010';
		
		$parametros_consulta['prospects_aguardando_analise'] = $this->obter_prospects_aguardando_analise($codigo_representante);
		
		return retornar_total($this, $parametros_consulta);
	}
	
	/**
		Obtem os codigos dos prospects aguardando análise para que não seja duplicados
	*/
	function obter_prospects_aguardando_analise($codigo_representante){
		
		
	
		$this->load->helper('db_cliente_protheus');
	
	
	
		$dados = $this->db_cliente->distinct()
			->select($this->mapeamento['campos']['prospects']['cgc']. ' AS cgc')
			->from($this->mapeamento['tabelas']['prospects'])
			->where($this->mapeamento['campos']['prospects']['codigo_representante'], $codigo_representante)
			->where($this->mapeamento['campos']['prospects']['status']. ' != ', 'I' )		
			->get()
			->result();
	
		$prospects_aguardando_analise = array();
			
		foreach($dados as $dado){
			$prospects_aguardando_analise[]  = $dado->CGC;
		}
				
		return $prospects_aguardando_analise;		
		
		

	
	}
}