<?php

class Categorias_produtos_model extends CI_Model {


	public $compartilhada = TRUE;

    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }

	
	function exportar_categorias_produtos($id = NULL, $pacote = NULL,$codigo_representante = NULL)
	{
		
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'B1_COD', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Método:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que sera executado para retornar as categorias dos produtos
	* 
	* Data:			11/10/2014
	* Modificação:	11/10/2014
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
	
		$id 					= $dados['id'];
		$codigo_empresa 		= NULL;
		$codigo_representante  	= $dados['codigo_representante'];
	
		//Obter Tabelas Preços
		$this->load->model('produtos_model');
		
		debug_pre($this->produtos_model->obter_tabelas_preco($codigo_representante));
		$tabelas_preco = $this->obter_tabelas_preco($codigo_representante);
	
		
	
		// * Retornar todos os campos
		$select_produto = select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_estoque = select_all($this->_db_cliente['tabelas']['produtos_estoque'], $this->_db_cliente['campos']['produtos_estoque'], 'etq');
		$select_produto_tabela_preco = select_all($this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos'], 'ptp');
		$select_tabela_preco = select_all($this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos'], 'tb');
		
		$select = array_merge(
				$select_produto, 
				$select_estoque,
				$select_produto_tabela_preco,
				$select_tabela_preco
			);

		
		// * Obter quantidade disponivel
		$select[] = '(' . $this->_db_cliente['campos']['produtos_estoque']['quantidade_atual'] . ' - (' . 
			$this->_db_cliente['campos']['produtos_estoque']['quantidade_pedidos_venda'] . ' + ' . 
			$this->_db_cliente['campos']['produtos_estoque']['quantidade_empenhada'] . ' + ' . 
			$this->_db_cliente['campos']['produtos_estoque']['quantidade_reservada'] . ')) AS quantidade_disponivel_estoque';
		//*
		
		$select += formatar_euf($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], $codigo_empresa);
		
		//Join
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos_estoque'], 
			euf(
				$this->_db_cliente['tabelas']['produtos_estoque'], $this->_db_cliente['campos']['produtos_estoque']['filial'], 
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial']
			) .
			$this->_db_cliente['campos']['produtos_estoque']['codigo'] . ' = ' . $this->_db_cliente['campos']['produtos']['codigo']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos_tabelas_precos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos']['filial'], 
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial']
			) .
			$this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_produto'] . ' = ' . $this->_db_cliente['campos']['produtos']['codigo']
			. ' AND ' . $this->_db_cliente['campos']['produtos_tabelas_precos']['filial'] . " = " . $this->_db_cliente['campos']['produtos_estoque']['filial']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['tabelas_precos'], 
			euf(
				$this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos']['filial'], 
				$this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos']['filial']
			) .
			$this->_db_cliente['campos']['tabelas_precos']['codigo'] . ' = ' . $this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_tabela_precos'] . ' AND ' . $this->_db_cliente['campos']['tabelas_precos']['ativo'] . " = '1'"
			. ' AND ' . $this->_db_cliente['campos']['tabelas_precos']['filial'] . " = " . $this->_db_cliente['campos']['produtos_tabelas_precos']['filial']
		);
	
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['recno'] . ' >', $id);
		}

		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['inativo'] . ' !=', '1');
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['delecao'] . ' !=', '*');
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_estoque'] . '.' . $this->_db_cliente['campos']['produtos_estoque']['delecao'] . ' !=', '*');
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' !=', '*');
		}
		
		
		//CUSTOM 177 - 870 - 14/10/2013
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['ativo'] , '1');		
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['data_vigencia'].'<=' , date('Ymd'));		
		//FIM CUSTOM 177 - 870 - 14/10/2013
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' !=', '*');
		
		/*
		
		$this->db_cliente->where_in($this->_db_cliente['campos']['tabelas_precos']['codigo'], $tabelas_preco);
		*/
		$this->db_cliente->where('B2_LOCAL', '01');
		//$this->db_cliente->where('B1_FILIAL', '01');
		
		
		$or = FALSE;
		
		$where = '(';
		
		foreach($tabelas_preco as $tabela_preco){
		
			if(!$or){				
				$where .= $this->_db_cliente['campos']['tabelas_precos']['codigo']. ' = '. "'".$tabela_preco['codigo']."'"
				.' AND '. $this->_db_cliente['campos']['tabelas_precos']['filial_saida'].' = '."'".$tabela_preco['filial_saida']."'";
						
			}else{	
				$where .= ' OR '.				
				$this->_db_cliente['campos']['tabelas_precos']['codigo']. " = '". $tabela_preco['codigo']."'"
				.' AND '. $this->_db_cliente['campos']['tabelas_precos']['filial_saida']." = '".$tabela_preco['filial_saida']."'";
				
			}
			$or = TRUE;			
		}
		$where .= ' )';
		
		$this->db_cliente->where($where);
		
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['produtos']);
		
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	/**
	* Método:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $codigo_representante)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	/**
	* Método:		obter_tabelas_preco
	* 
	* Descrição:	Função Utilizada para retornar os codigos da tabelas de Preço
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_tabelas_preco($codigo_representante)
	{
		// Load Tabelas de Preço
		$this->load->model('tabelas_preco_model');
	
		$this->tabelas_preco_model->_db_cliente = $this->_db_cliente;
	
		$parametros_consulta['id'] = NULL;
		$parametros_consulta['codigo_empresa'] = NULL;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
				
		$tabelas_preco = $this->tabelas_preco_model->obter_dados($parametros_consulta);
	
		foreach($tabelas_preco as $tp)
		{
			$codigos_tabelas_preco[] = array('codigo' => $tp['codigo'], 'filial_saida' =>$tp['filial_saida']);
		}
		
		
		
		return $codigos_tabelas_preco; 
		//return array('CTB'); //@TODO: Fixado pois é retornado muito produto - verificar após conclusão

	}
	


}