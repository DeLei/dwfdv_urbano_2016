<?php

class Formas_pagamento_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		
    }
    	
	
	function exportar_formas_pagamento($id = NULL, $pacote = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		
		$parametros_consulta['id'] = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('RZB010');
		$parametros_consulta['tabelas_principal']		= 'RZB010';
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'E4_CODIGO', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Função Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
	
		$id 				= $dados['id'];
		$codigo_empresa 	= $dados['codigo_empresa'];
		$codigo_representante	= $dados['codigo_representante'];
		
		$tabelas_preco = $this->obter_tabelas_preco($codigo_representante);
		
		if(count($tabelas_preco) <= 0){
			$tabelas_preco[]  = array('codigo' => 'a-Z0-9', 
			'filial_saida' => '99');
		}
		
		if(COUNT($tabelas_preco) > 0){
			$or = FALSE;
			
			$where = '(';
			
			foreach($tabelas_preco as $tabela_preco){
			
				if(!$or){				
					$where .= $this->_db_cliente['campos']['tabelas_precos']['codigo']. ' = '. "'".$tabela_preco['codigo']."'"
					.' AND '. $this->_db_cliente['campos']['tabelas_precos']['filial_saida'].' = '."'".$tabela_preco['filial_saida']."'";
							
				}else{	
					$where .= ' OR '.				
					$this->_db_cliente['campos']['tabelas_precos']['codigo']. " = '". $tabela_preco['codigo']."'"
					.' AND '. $this->_db_cliente['campos']['tabelas_precos']['filial_saida']." = '".$tabela_preco['filial_saida']."'";
					
				}
				$or = TRUE;			
			}
			$where .= ' )';
			
			$this->db_cliente->where($where);
		}
		
		
		
		
		$select[] = $this->_db_cliente['tabelas']['tabelas_precos']		.'.'.	$this->_db_cliente['campos']['tabelas_precos']['filial'].' as filial';
		$select[] = $this->_db_cliente['tabelas']['tabelas_precos']		.'.'.	$this->_db_cliente['campos']['tabelas_precos']['codigo'].' as codigo_tabela';
		
		$select[] = $this->_db_cliente['tabelas']['formas_pagamento_por_tabela_preco'].'.'.$this->_db_cliente['campos']['formas_pagamento_por_tabela_preco']['condicao_pagamento'].' as codigo_condicao_pagamento';
		
		$select[] = $this->_db_cliente['tabelas']['formas_pagamento']	.'.'.	$this->_db_cliente['campos']['formas_pagamento']['descricao'].' as descricao';
		$select[] = $this->_db_cliente['tabelas']['prazos_por_tabela_precos']	.'.'.	$this->_db_cliente['campos']['prazos_por_tabela_precos']['prazo'].' as prazo';
		
		$this->db_cliente->join($this->_db_cliente['tabelas']['formas_pagamento'],
				(
						''.
							$this->_db_cliente['tabelas']['formas_pagamento']		.'.'.	$this->_db_cliente['campos']['formas_pagamento']['filial'] .' = '.
							$this->_db_cliente['tabelas']['formas_pagamento_por_tabela_preco']		.'.'.	$this->_db_cliente['campos']['formas_pagamento_por_tabela_preco']['filial'] .' AND '.
							//AND
							$this->_db_cliente['tabelas']['formas_pagamento']		.'.'.	$this->_db_cliente['campos']['formas_pagamento']['codigo'] .' = '.
							$this->_db_cliente['tabelas']['formas_pagamento_por_tabela_preco']		.'.'.	$this->_db_cliente['campos']['formas_pagamento_por_tabela_preco']['condicao_pagamento'] .
						''
				),'LEFT');
		
		$this->db_cliente->join($this->_db_cliente['tabelas']['prazos_por_tabela_precos'],
				(
						''.
							$this->_db_cliente['tabelas']['prazos_por_tabela_precos']		.'.'.	$this->_db_cliente['campos']['prazos_por_tabela_precos']['prazo'] .' = '.
							$this->_db_cliente['tabelas']['formas_pagamento_por_tabela_preco']		.'.'.	$this->_db_cliente['campos']['formas_pagamento_por_tabela_preco']['codigo'] .' '.
							''
				),'LEFT');
		
		$this->db_cliente->join($this->_db_cliente['tabelas']['tabelas_precos'],
				(
						''.
						$this->_db_cliente['tabelas']['prazos_por_tabela_precos']		.'.'.	$this->_db_cliente['campos']['prazos_por_tabela_precos']['filial'] .' = '.
						$this->_db_cliente['tabelas']['tabelas_precos']		.'.'.	$this->_db_cliente['campos']['tabelas_precos']['filial'] .' AND '.
						//AND
						$this->_db_cliente['tabelas']['prazos_por_tabela_precos']		.'.'.	$this->_db_cliente['campos']['prazos_por_tabela_precos']['tabela_preco'] .' = '.
						$this->_db_cliente['tabelas']['tabelas_precos']		.'.'.	$this->_db_cliente['campos']['tabelas_precos']['codigo'] .' '.
						
						''
				),'LEFT');
		
		//$select += formatar_euf($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], $codigo_empresa);
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['formas_pagamento'].'.'.$this->_db_cliente['campos']['formas_pagamento']['delecao']. ' <> ','*');
			$this->db_cliente->where($this->_db_cliente['tabelas']['formas_pagamento_por_tabela_preco'].'.'.$this->_db_cliente['campos']['formas_pagamento_por_tabela_preco']['delecao']. ' <> ','*');
			$this->db_cliente->where($this->_db_cliente['tabelas']['prazos_por_tabela_precos'].'.'.$this->_db_cliente['campos']['prazos_por_tabela_precos']['delecao']. ' <> ','*');
			$this->db_cliente->where($this->_db_cliente['tabelas']['tabelas_precos'].'.'.$this->_db_cliente['campos']['tabelas_precos']['delecao']. ' <> ','*');
		}
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['tabelas_precos'].'.'.$this->_db_cliente['campos']['tabelas_precos']['recdel']. '',0);
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['tabelas_precos']		.'.'.	$this->_db_cliente['campos']['tabelas_precos']['ativo'],'1');
		$this->db_cliente->where($this->_db_cliente['tabelas']['formas_pagamento_por_tabela_preco']		.'.'.	$this->_db_cliente['campos']['formas_pagamento_por_tabela_preco']['filial'],' ');
		
		$this->db_cliente->order_by($this->_db_cliente['tabelas']['formas_pagamento_por_tabela_preco']		.'.'.	$this->_db_cliente['campos']['formas_pagamento_por_tabela_preco']['condicao_pagamento']);
		$this->db_cliente->order_by($this->_db_cliente['tabelas']['tabelas_precos']		.'.'.	$this->_db_cliente['campos']['tabelas_precos']['codigo']);
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['formas_pagamento_por_tabela_preco']);
		
		
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	/**
	* Metodo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $codigo_representante, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('RZB010');
		$parametros_consulta['tabelas_principal']		= 'RZB010';
		
		return retornar_total($this, $parametros_consulta);
	}
	
	function obter_tabelas_preco($codigo_representante){
		
	// Load Tabelas de Preço
		$this->load->model('tabelas_preco_model');
	
		$this->tabelas_preco_model->_db_cliente = $this->_db_cliente;
	
		$parametros_consulta['id'] = NULL;
		$parametros_consulta['codigo_empresa'] = NULL;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
				
		$tabelas_preco = $this->tabelas_preco_model->obter_dados($parametros_consulta);
		$codigos_tabelas_preco = array();
		foreach($tabelas_preco as $tp)
		{
			$codigos_tabelas_preco[] = array('codigo' => $tp['codigo'], 'filial_saida' =>$tp['filial_saida']);
		}
		
		return $codigos_tabelas_preco; 
		
		
		
	}

}