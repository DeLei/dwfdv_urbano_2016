<?php

class Eventos_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	* Metódo:		exportar_eventos
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de Transportadoras
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_eventos($id = NULL, $pacote = NULL)
	{
		
		
		$parametros_consulta['id'] 						= $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id = $dados['id'];
	
		// Condições do SQL (WHERE)
		if($id)
		{
			// Buscar Filiais a partir deste ID
			$this->db->where(array('id >' => $id));
		}
		$this->db->where('status', 'ativa');
	
		// Selecionar
		$this->db->select('id');	
		$this->db->select('timestamp');	
		$this->db->select('status');	
		$this->db->select('link_imagem');	
		$this->db->select('nome');	
		$this->db->select('data_inicial');	
		$this->db->select('data_inicial_timestamp');	
		$this->db->select('data_final');
		$this->db->select('data_final_timestamp');
			
		// Consulta
		$this->db->from('feiras');
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] = $id;
	
		return retornar_total($this, $parametros_consulta);
	}

}