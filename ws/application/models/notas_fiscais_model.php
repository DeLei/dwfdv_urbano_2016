<?php

class Notas_fiscais_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
        
        $this->db_cliente = $this->load->database('db_cliente', TRUE);
        
    }
	
	/**
	* Metódo:		exportar_notas_fiscais
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de notas fiscais
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web / William Reis Fernandes
	* 
	*/
	function exportar_notas_fiscais($id = NULL, $pacote = NULL, $codigo_representante, $incremental = NULL, $ultima_sincronizacao = NULL)
	{
		
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SF2010');
		$parametros_consulta['tabelas_principal']		= 'SF2010';
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'F2_DOC', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar titulos
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web / William Reis Fernandes
	* 
	*/
	function consulta($dados = NULL)
	{
	
		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
	
		// * Retornar todos os campos
		$select_notas_fiscais		= select_all($this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais'], 'nota');
		$select_itens_notas_fiscais	= select_all($this->_db_cliente['tabelas']['itens_notas_fiscais'], $this->_db_cliente['campos']['itens_notas_fiscais'], 'item');
		$select_produtos			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_clientes 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_formas_pagamento	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras		= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		
		
		
		
		$select = array_merge(
			$select_notas_fiscais,
			$select_itens_notas_fiscais,
			$select_produtos, 
			$select_clientes,
			$select_formas_pagamento,
			$select_transportadoras
		);
		
		
		
		
		//debug_pre($select);
		
		/*Oracle*/
		$select[] = "(RTRIM(LTRIM(".$this->_db_cliente['campos']['notas_fiscais']['codigo']." ))||'-'||RTRIM(LTRIM(".$this->_db_cliente['campos']['notas_fiscais']['serie']."))) as codigo_generico";
		/* mssql
		$select[] = "(RTRIM(LTRIM(cast(".$this->_db_cliente['campos']['notas_fiscais']['codigo']." as varchar(30))))+'-'+RTRIM(LTRIM(cast(".$this->_db_cliente['campos']['notas_fiscais']['serie']." as varchar(30))))) as codigo_generico";
		*/
		$select += formatar_euf($this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial'], $codigo_empresa);
		
		//-----------------------------------------------	
		
		// Join
		//-----------------------------------------------		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['itens_notas_fiscais'], 
			euf(
				$this->_db_cliente['tabelas']['itens_notas_fiscais'], $this->_db_cliente['campos']['itens_notas_fiscais']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['campos']['itens_notas_fiscais']['codigo_nota_fiscal'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['codigo'] . " AND " .
			$this->_db_cliente['campos']['itens_notas_fiscais']['serie'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['serie']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], 
				$this->_db_cliente['tabelas']['itens_notas_fiscais'], $this->_db_cliente['campos']['itens_notas_fiscais']['filial']
			) .
			$this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['itens_notas_fiscais']['codigo_produto']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['condicao_pagamento']
		, 'left outer');
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['clientes'], 
			euf(
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['campos']['clientes']['codigo'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['codigo_cliente'] . " AND " .
			$this->_db_cliente['campos']['clientes']['loja'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['loja_cliente']
			
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['representantes'], 
			euf(
				$this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['campos']['representantes']['codigo'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['codigo_representante']
			. ' OR '
			. $this->_db_cliente['campos']['representantes']['codigo'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['codigo_representante2']
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf(
				$this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], 
				$this->_db_cliente['tabelas']['notas_fiscais'], $this->_db_cliente['campos']['notas_fiscais']['filial']
			) .
			$this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['campos']['notas_fiscais']['codigo_transportadora'],
			'left outer'
		);
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'].".".$this->_db_cliente['campos']['notas_fiscais']['recno'] . ' >', $id);
		}
				
		$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'].".".$this->_db_cliente['campos']['notas_fiscais']['data_emissao']." >= ", date('Ymd', PERIODO_DADOS));		
		
		
		
		
		$this->db_cliente->where('('.$this->_db_cliente['tabelas']['notas_fiscais'].".".$this->_db_cliente['campos']['notas_fiscais']['codigo_representante'].' = \''. $codigo_representante.'\''
			.' OR '.
		
		$this->_db_cliente['tabelas']['notas_fiscais'].".".$this->_db_cliente['campos']['notas_fiscais']['codigo_representante2'].' = \''. $codigo_representante.'\')'
		);
		
		
		if(!isset($dados['incremental']) || $dados['incremental'] != 1)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'].".".$this->_db_cliente['campos']['notas_fiscais']['delecao'] . ' !=', '*');
			$this->db_cliente->where($this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . ' !=', '*');
		}
		//$this->db_cliente->where($this->_db_cliente['tabelas']['notas_fiscais'] . '.' . $this->_db_cliente['campos']['notas_fiscais']['duplicata'] . ' !=', '');
		
			
		//debug_pre($select);
		
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['notas_fiscais']);
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL, $incremental = NULL, $ultima_sincronizacao = NULL)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['codigo_empresa']			= NULL;
		
		$parametros_consulta['incremental']				= $incremental;
		$parametros_consulta['ultima_sincronizacao']	= $ultima_sincronizacao;
		$parametros_consulta['tabelas_controle']		= array('SF2010');
		$parametros_consulta['tabelas_principal']		= 'SF2010';
		
		return retornar_total($this, $parametros_consulta);
	}

}