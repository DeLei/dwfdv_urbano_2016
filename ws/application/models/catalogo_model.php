<?php

class Catalogo_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	* Metódo:		exportar_agenda
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de comprimissos da agenda
	* 
	* Data:				09/09/2013
	* Modificação:	09/09/2013
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_catalogo($id = NULL, $pacote = NULL, $id_usuario = NULL)
	{
		$parametros_consulta['id'] 							= $id;
		$parametros_consulta['id_usuario'] 					= $id_usuario;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar dados de comprimissos da agenda
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{	
		$id 				= $dados['id'];
		$id_usuario 	= $dados['id_usuario'];
		
		// Condições do SQL (WHERE)
		if($id)
		{
			// Buscar Compromissos a partir deste ID
			$this->db->where(array('id >' => $id));
		}
		
		// Condições do SQL (WHERE)
		
		// Travando por modulo
		$this->db->where(array('modulo' => 'catalogos'));
		//Obter apenas ativos
		$this->db->where(array('ativo' => '1'));
	
		//Ordenção
		$this->db->order_by('id');
	
		// Selecionar
		$this->db->select('id as id');
		$this->db->select('url as url');
		$this->db->select('ativo as ativo');	
		$this->db->select('titulo as titulo');
		$this->db->select('descricao as descricao');
		$this->db->select('data_cadastro as data_cadastro');
		$this->db->select('filial as filial');	
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = Não Exportado)
		$this->db->select("'1' as exportado", false);
		$this->db->select("'0' as editado", false);
		$this->db->select("'0' as remover", false);
		$this->db->select("'0' as erro", false);
			
		// Consulta
		$this->db->from('anexos as catalogo');
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $id_usuario)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['id_usuario'] = $id_usuario;
	
		return retornar_total($this, $parametros_consulta);
	}

}