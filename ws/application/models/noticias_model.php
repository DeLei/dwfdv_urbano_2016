<?php

class Noticias_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	* Metódo:		exportar_noticias
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de Transportadoras
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_noticias($id = NULL, $pacote = NULL,$codigo_representante = NULL)
	{
		$parametros_consulta['id'] = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		// Consulta com Pacote de Dados
		
		$dados = pacote_dados($this, $pacote, FALSE, FALSE,  $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
		$limitacao_por_regiao = ' (SELECT GROUP_CONCAT(noticia_regiao.regiao_id) FROM noticia_regiao WHERE noticia_regiao.noticia_id = noticias.id) ';
		//SubSQL
	//		$this->db
	//		->select( $limitacao_por_regiao.' AS regioes');
	//		$this->db->select('noticias.*');
			
		$this->load->model('representantes_model');
		$representante= $this->representantes_model->exportar_representante($dados['codigo_representante']);
		
		
		$id = $dados['id'];
	
		// Condições do SQL (WHERE)
		if($id)
		{
			// Buscar Filiais a partir deste ID
			$this->db->where(array('id >' => $id));
		}
		$this->db->where('status', 'ativa');
		
		
		$this->db->like(trim($limitacao_por_regiao), $representante['codigo_gestor']);
	
		// Selecionar
		// 	id	timestamp	status	link_imagem	titulo	conteudo
		$this->db->select('id');	
		$this->db->select('timestamp');	
		$this->db->select('status');	
		$this->db->select('link_imagem');	
		$this->db->select('titulo');	
		$this->db->select('conteudo');
			
		// Consulta
		$this->db->from('noticias');
		
		
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id,  $codigo_representante)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		
		return retornar_total($this, $parametros_consulta);
	}

}