<?php

class Autenticacao_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
        
        //Carregar helper
        $this->load->helper('date');
        //Carregar agentes
        $this->load->library('user_agent');
    }
    
	function autenticar($key = NULL)
	{
	
		$usuarios = $this->db->from('usuarios_aparelhos')
		->join('usuarios', 'usuarios.id = usuarios_aparelhos.usuarios_id AND usuarios.codigo = usuarios_aparelhos.usuarios_codigo')
		->where(
			array(
				'usuarios_aparelhos.key' => $key
				//,'usuarios.status' => 'ativo'
			)
		)
		->where_in('usuarios.grupo',  array('representantes','prepostos'))
		->get()->row();
		
		if(isset($usuarios->usuarios_codigo)){
		
		$usuarios->usuarios_codigo = trim($usuarios->usuarios_codigo);
		$usuarios->codigo = trim($usuarios->codigo);
		$usuarios->usuario = trim($usuarios->usuario);
		}
		if($usuarios)
		{
			//Não retornar os valores dos campos SENHA e USUÁRIO
			unset($usuarios->senha);
			$usuarios->usuario;
			
			
			
		}
		
			
		
		return $usuarios;
	}
	
	function salvar_localizacao($id_usuario, $codigo_representante, $key, $latitude, $longitude, $detalhes)
	{
		$dados = array(
				'usuarios_id' 			=> $id_usuario,
				'usuarios_codigo' 		=> $codigo_representante,
				'key' 					=> $key,
				'latitude' 				=> $latitude,
				'longitude' 			=> $longitude,
				'detalhes'				=> serialize($detalhes),
				'endereco_ip'			=> $this->input->ip_address(),
				'navegador'				=> $this->agent->browser(). ' ' . $this->agent->version(),
				'navegador_string'		=> $this->agent->agent_string(),
				'so' 					=> $this->agent->platform(),
				'data_hora' 			=> now()
            );

		return $this->db->insert('usuarios_aparelhos_localizacoes', $dados); 
	}
	
	function validar_usuario() {
		$headers = apache_request_headers();
		
		$key = $headers['DW_KEY_APP'];
		
		$this->load->model('autenticacao_model');
		
		$usuario = $this->autenticacao_model->autenticar($key);
		
		if($usuario) {
			return true;
		} else {
			return false;
		}
	}
	
	
	
}