<?php

class Pendencias_Mensagens_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	* Metódo:		exportar_usuarios
	* 
	* Descrição:	Função retornar os usuários que podem recebe pendências do representante em questão
	* 
	* Data:				02/09/2013
	* Modificação:	02/09/2013
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_mensagens($id = NULL, $pacote = NULL, $id_usuario = NULL, $campos = FALSE)
	{
		$parametros_consulta['id'] 							= $id;
		$parametros_consulta['codigo_representante'] 	= $id_usuario;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	*/
	function consulta($dados = NULL)
	{
	
		$id 							= $dados['id'];
		$codigo_representante 	= $dados['codigo_representante'];

		//Select
		$this->db->select('pendencias_mensagens.id as id');
		$this->db->select('pendencias_mensagens.timestamp as timestamp');
		$this->db->select('pendencias_mensagens.id_pendencia as id_pendencia');
		$this->db->select('pendencias_mensagens.id_usuario as id_usuario');
		$this->db->select('pendencias_mensagens.usuario as usuario');
		$this->db->select('pendencias_mensagens.conteudo as conteudo');
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = Não Exportado)
		$this->db->select("'1' as exportado", false);
		$this->db->select("'0' as editado", false);		
		$this->db->select("'0' as erro", false);
		
		//Where
		$this->db->where('usuarios_pendencias.id_usuario', $codigo_representante);
		$this->db->or_where('usuarios_pendencias.id_usuario_remetente', $codigo_representante);
		
		//View
		$this->db->from('usuarios_pendencias');		
		
		//Join
		$this->db->join('pendencias_mensagens', 'pendencias_mensagens.id_usuario = usuarios_pendencias.id_usuario OR pendencias_mensagens.id_usuario = usuarios_pendencias.id_usuario_remetente');
		
		//Agrupar
		$this->db->group_by('pendencias_mensagens.id');
	}
	
	
	/**
	* Metódo:			importar
	* 
	* Descrição:		Função Utilizada para inserir pendência no banco, e inserir LOGS
	* 
	* Data:				25/09/2012
	* Modificação:	25/09/2012
	* 
	* @access		public
	* @param		json 			$dados							- Dados das Pendências enviados pelo DW força de vendas
	* @param		string 		$id_usuario						- ID do usuário
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('pendencias_mensagens', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			$pendencias_mensagens = json_decode($dados);
	
			foreach($pendencias_mensagens as $mensagem)
			{
				try{				
					$this->db->insert('pendencias_mensagens', array(
						'timestamp' 	=> $mensagem->timestamp,
						'id_pendencia' 	=> $mensagem->id_pendencia,
						'id_usuario' 		=> $mensagem->id_usuario,
						'usuario' 		=> $mensagem->usuario,
						'conteudo' 		=> $mensagem->conteudo
					));

					$this->db->update('pendencias', array(
						'ultima_mensagem_timestamp' 		=> $mensagem->timestamp,
						'id_usuario_ultima_mensagem' 		=> $mensagem->id_usuario,
						'nome_usuario_ultima_mensagem' 	=> $mensagem->usuario), array('id' => $mensagem->id)
					);                
				}
				catch(Exception $e)
				{
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($mensagem),$id_usuario, $codigo_representante); 
				}
			}
			
			$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
		
			if($dados_erros)
			{
				foreach($dados_erros as $dado_erro)
				{
					$dados_json 		= json_decode($dado_erro->dados); 
					$codigos_erro[] 	= $dados_json->id;
					$nome_erro[] 		= 'Não foi possível salvar mensagem da pendência.';
				}
				
				$erros['erro'] = $codigos_erro;
				$erros['erro_descricao'] = $nome_erro;
				
				return $erros;
			}
			else
			{
				return array('sucesso' => 'ok');
			}
		
		}
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $codigo_representante)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
	
		return retornar_total($this, $parametros_consulta);
		
	}

}