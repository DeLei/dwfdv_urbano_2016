<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function verificar_SX2($tabela, $arquivo)
{
	$arquivo = DIR_DBF.$arquivo;
	
	if (file_exists($arquivo)) {
		$db = dbase_open($arquivo, 0);

		if ($db) 
		{
			$retorno = NULL;
		
			$total_registros = dbase_numrecords($db);
			
			for($i = 1; $i <= $total_registros; $i++)
			{
				$dados = dbase_get_record_with_names($db,$i);
				
				if($dados['X2_CHAVE'] == $tabela)
				{
					$retorno = $dados;
				}
			}
			
			dbase_close($db);
			
			return $retorno;
		}
		else
		{
			echo ('Falha na conexão com o DBF.');
		}
	}
	else
	{
		echo ('Arquivo não foi necontrado (' . $arquivo  . ').');
	}
}

/**
* Metódo:		formatar_euf
* 
* Descrição:	Função Utilizada para formatar o campo FILIAL da tabela em EMPRESA, UNIDADE e FILIAL
* 
* Data:			07/02/2012
* Modificação:	07/02/2012
* 
* @access		public
* @param		string 		$tabela 	
* @param		string 		$campo
* @param		string 		$empresa_tabela			- Código da empresa usada na tabela. Exemplo SA1990 = Código = '990';
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function formatar_euf($tabela, $campo, $empresa_tabela = NULL, $retornar_array = TRUE)
{
	$tamanho	= 0;
	$ordem		= array();
	$substrings	= array();
	$tamanho_inicial = array();

	$CI =& get_instance();
	
	if(count($CI->config->item('empresas')) > 1 && $CI->config->item('euf') == FALSE)
	{
		$empresa_tabela = $CI->config->item('empresa_matriz');
	}


	//if($CI->config->item('euf')){
	
		$db 		= $CI->load->database('db_cliente', TRUE);
		$map 		= mapeamento($CI->config->config, $CI->config->item('empresa_matriz'));
		
		if($db->dbdriver == 'oci8') // Se for oracle
		{
			$substring = 'SUBSTR';
		}
		else
		{
			$substring = 'SUBSTRING';
		}
		
		// A partir do Protheus 11
		try{
		
			$dados = $db->select($map['campos']['euf']['modo_filial'] . ' AS modo_filial')
							->select($map['campos']['euf']['modo_unidade'] . ' AS modo_unidade')
							->select($map['campos']['euf']['modo_empresa'] . ' AS modo_empresa')
					
							->select($map['campos']['euf']['tamanho_filial'] . ' AS tamanho_filial')
							->select($map['campos']['euf']['tamanho_empresa'] . ' AS tamanho_empresa')
							->select($map['campos']['euf']['tamanho_unidade'] . ' AS tamanho_unidade')
							
							->from($map['tabelas']['euf'])
							->where($map['campos']['euf']['chave'], substr($tabela, 0, 3))
							->where($map['campos']['euf']['delecao'] . ' !=', '*')
							->get()->row_array();
		
		
		
		
			foreach($CI->config->item('posicao_euf') as $posicao)
			{
				//Empresa
				if($posicao == 'E')
				{
					if((isset($dados['modo_empresa'])  AND $dados['modo_empresa'] == 'E') AND ( isset($dados['tamanho_empresa']) AND  $dados['tamanho_empresa'] > 0))
					{
						$tamanho 				+= $dados['tamanho_empresa'];
						$ordem['E'] 			= $dados['tamanho_empresa'];
						$tamanho_inicial['E'] 	= (($tamanho + 1) - $dados['tamanho_empresa']);
						$substrings['E'] 		= "$substring($campo, " . $tamanho_inicial['E'] . ", " . $ordem['E'] . ") AS empresa";
					}
					else
					{
						if($empresa_tabela)
						{
							$substrings['E'] 		= "'$empresa_tabela' AS empresa";
						}
						else
						{
							$substrings['E'] 		= "$substring($campo, 0, 0) AS empresa";
						}
					}
				}
				
				//Unidade
				if($posicao == 'U')
				{
					if((isset($dados['modo_unidade']) AND   $dados['modo_unidade'] == 'E') AND ( isset($dados['tamanho_unidade']) AND  $dados['tamanho_unidade'] > 0))
					{
						$tamanho 				+= $dados['tamanho_unidade'];
						$ordem['U'] 			= $dados['tamanho_unidade'];
						$tamanho_inicial['U'] 	= (($tamanho + 1) - $dados['tamanho_unidade']);
						$substrings['U'] 		= "$substring($campo, " . $tamanho_inicial['U'] . ", " . $ordem['U'] . ") AS unidade";
					}
					else
					{
						$substrings['U'] 		= "$substring($campo, 0, 0) AS unidade";
					}
				}
				
				
				//Filial
				if($posicao == 'F')
				{
					if((isset($dados['modo_filial']) AND  $dados['modo_filial'] == 'E') AND (isset($dados['tamanho_filial']) AND $dados['tamanho_filial'] > 0))
					{
						$tamanho	 			+= $dados['tamanho_filial'];
						$ordem['F'] 			= $dados['tamanho_filial'];
						$tamanho_inicial['F'] 	= (($tamanho + 1) - $dados['tamanho_filial']);
						$substrings['F'] 		= "$substring($campo, " . $tamanho_inicial['F'] . ", " . $ordem['F'] . ") AS filial";
					}
					else
					{
						$substrings['F'] 		= "$substring($campo, 0, 0) AS filial";
					}
				}
			}
			
		}
		catch(Exception $e) // Antes do Protheus 11
		{
			$dados_sx2 = verificar_SX2($tabela, 'sx2' . $CI->config->item('empresa_matriz') . '.dbf');
		
			$dados = NULL;
			$substrings['E'] 		= "'$empresa_tabela' AS empresa";
			$substrings['U'] 		= "'' AS unidade";
			$substrings['F'] 		= "$substring($campo, 1, " . ($dados_sx2['X2_MODULO'] ? $dados_sx2['X2_MODULO'] : 0) . ") AS filial";
		}
		
	
	
		if($retornar_array)
		{
			return $substrings;
		}
		else
		{
			return implode($substrings, ', ');
		}

	//}
	
}

/**
* Metódo:		euf
* 
* Descrição:	Função Utilizada para retornar a ligação entre EMPRESAS, UNIDADES e FILIAIS (EEUUFF)
* 
* Data:			07/02/2012
* Modificação:	07/02/2012
* 
* @access		public
* @param		string 		$tabela1 			- a 1ª tabela da ligação
* @param		string 		$campo1 			- a 1ª campo da ligacao
* @param		string 		$tabela2			- a 2ª tabela da ligação
* @param		string 		$campo2 			- a 2ª campo da ligacao
* @param		booleano 	$and 				- retornar "AND" mo final da ligação
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function euf($tabela1, $campo1, $tabela2, $campo2, $and = true)
{
	$CI =& get_instance();
	
	if($and == true)
	{
		$filtroAnd = ' AND ';
	}
	
	/*
	if($CI->config->item('euf'))
	{
	*/
	
		$tamanho1 	= 0;
		$tamanho2 	= 0;
		$filtro 	= '';
		$ordem1 	= array();
		$ordem2 	= array();
		$db 		= $CI->load->database('db_cliente', TRUE);
		$map 		= mapeamento($CI->config->config, $CI->config->item('empresa_matriz'));
		
		// A partir do Protheus 11
		try{
			//Verificar informações da tabela um
			$dadosCampo1 = $db->select($map['campos']['euf']['modo_filial'] . ' AS modo_filial')
							->select($map['campos']['euf']['modo_unidade'] . ' AS modo_unidade')
							->select($map['campos']['euf']['modo_empresa'] . ' AS modo_empresa')
					
							->select($map['campos']['euf']['tamanho_filial'] . ' AS tamanho_filial')
							->select($map['campos']['euf']['tamanho_empresa'] . ' AS tamanho_empresa')
							->select($map['campos']['euf']['tamanho_unidade'] . ' AS tamanho_unidade')
							
							->from($map['tabelas']['euf'])
							->where($map['campos']['euf']['chave'], substr($tabela1, 0, 3))
							->where($map['campos']['euf']['delecao'] . ' !=', '*')
							->get()->row_array();
			//debug_pre($db->last_query());		

		
			
			//Verificar informações da tabela dois
			$dadosCampo2 = $db->select($map['campos']['euf']['modo_filial'] . ' AS modo_filial')
							->select($map['campos']['euf']['modo_unidade'] . ' AS modo_unidade')
							->select($map['campos']['euf']['modo_empresa'] . ' AS modo_empresa')
					
							->select($map['campos']['euf']['tamanho_filial'] . ' AS tamanho_filial')
							->select($map['campos']['euf']['tamanho_empresa'] . ' AS tamanho_empresa')
							->select($map['campos']['euf']['tamanho_unidade'] . ' AS tamanho_unidade')
							
							->from($map['tabelas']['euf'])
							->where($map['campos']['euf']['chave'], substr($tabela2, 0, 3))
							->where($map['campos']['euf']['delecao'] . ' !=', '*')
							->get()->row_array();
			
			//debug_pre($dadosCampo2);
			/**
			 * Filtro do campo pai
			 */
			foreach($CI->config->item('posicao_euf') as $posicao)
			{
				if($posicao == 'E')
				{
					if(( isset($dadosCampo1['modo_empresa']) &&  $dadosCampo1['modo_empresa'] == 'E') AND (isset($dadosCampo1['tamanho_empresa']) && $dadosCampo1['tamanho_empresa'] > 0))
					{
						$tamanho1 		+= $dadosCampo1['tamanho_empresa'];
						$ordem1['E'] 	= $dadosCampo1['tamanho_empresa'];
					}
				}
				
				if($posicao == 'U')
				{
					if((isset($dadosCampo1['modo_unidade']) && $dadosCampo1['modo_unidade'] == 'E') AND (isset($dadosCampo1['tamanho_unidade']) && $dadosCampo1['tamanho_unidade'] > 0))
					{
						$tamanho1 		+= $dadosCampo1['tamanho_unidade'];
						$ordem1['U'] 	= $dadosCampo1['tamanho_unidade'];
					}
				}
				
				if($posicao == 'F')
				{
					if(( isset($dadosCampo1['modo_filial']) && $dadosCampo1['modo_filial'] == 'E') AND ( isset($dadosCampo1['tamanho_filial']) && $dadosCampo1['tamanho_filial'] > 0))
					{
						$tamanho1 		+= $dadosCampo1['tamanho_filial'];
						$ordem1['F'] 	= $dadosCampo1['tamanho_filial'];
					}
				}
			}
			
			
			
			

			/**
			 * Filtro do campo filho
			 */
			foreach($CI->config->item('posicao_euf') as $posicao)
			{
				if($posicao == 'E')
				{
				
					if(( isset($dadosCampo2['modo_empresa']) AND  $dadosCampo2['modo_empresa'] == 'E') AND ( isset($dadosCampo2['tamanho_empresa']) AND  $dadosCampo2['tamanho_empresa'] > 0))
					{
						$tamanho2 		+= $dadosCampo2['tamanho_empresa'];
						$ordem2['E'] 	= $dadosCampo2['tamanho_empresa'];
					}
				}
				
				if($posicao == 'U')
				{
					if(( isset($dadosCampo2['modo_unidade']) AND  $dadosCampo2['modo_unidade'] == 'E') AND ( isset($dadosCampo2['tamanho_unidade'])  AND $dadosCampo2['tamanho_unidade'] > 0))
					{
						$tamanho2 		+= $dadosCampo2['tamanho_unidade'];
						$ordem2['U'] 	= $dadosCampo2['tamanho_unidade'];
					}
				}
				
				if($posicao == 'F')
				{
					if((isset($dadosCampo2['modo_filial']) AND   trim($dadosCampo2['modo_filial']) == 'E') AND (isset($dadosCampo2['modo_filial']) AND trim($dadosCampo2['tamanho_filial']) > 0))
					{
						$tamanho2 		+= $dadosCampo2['tamanho_filial'];
						$ordem2['F'] 	= $dadosCampo2['tamanho_filial'];
					}
				}
			}
			
			/*
			echo ('Tamanho 1: '.$tamanho1);
			debug_pre($ordem1, FALSE);
			echo ('<hr />');
			echo ('Tamanho 2: '.$tamanho2);
			debug_pre($ordem2, FALSE);
			echo ('<hr />');
			*/
		
			
			
			
			
			if(($tamanho1 == 0) OR ($tamanho2 == 0))
			{
				$filtro = '';
			}
			elseif(($tamanho1 == $tamanho2))
			{
				//$tabela1 = 'SB1010', $campo1 = 'B1_FILIAL', $tabela2 = 'SC5010', $campo2
				$filtro = ' '.$tabela1.'.'.$campo1.' = '.$tabela2.'.'.$campo2.$filtroAnd;
			}
			elseif(($tamanho1 > $tamanho2))
			{
				$filtro = tratar_substr($ordem1, $ordem2, $tabela1, $campo1, $db->dbdriver) . ' = '.$tabela2.'.'.$campo2.$filtroAnd;
			}
			elseif(($tamanho2 > $tamanho1))
			{
				$filtro = ' '.$tabela1.'.'.$campo1.' =' . tratar_substr($ordem2, $ordem1, $tabela2, $campo2, $db->dbdriver) . $filtroAnd;
			}
			

		}
		catch(Exception $e)  // Antes do Protheus 11
		{
			$dados_sx2_tabela1 = verificar_SX2(substr($tabela1, 0, 3), 'sx2' . $CI->config->item('empresa_matriz') . '.dbf');
			$dados_sx2_tabela2 = verificar_SX2(substr($tabela2, 0, 3), 'sx2' . $CI->config->item('empresa_matriz') . '.dbf');
			
			if($dados_sx2_tabela1['X2_MODO'] == 'E' && $dados_sx2_tabela2['X2_MODO'] == 'E')
			{
				$filtro = ' '.$tabela1.'.'.$campo1.' = '.$tabela2.'.'.$campo2.$filtroAnd;
			}
			else
			{
				$filtro = '';
			}
		
		}
		
		return  $filtro;
}

/**
* Metódo:		tratar_substr
* 
* Descrição:	Função Utilizada para retornar o SUBSTRING (Concatenado ou não) com o valor adequado para comparação
* 
* Data:			07/02/2012
* Modificação:	07/02/2012
* 
* @access		public
* @param		array 		$ordem1 			- Ordenação de Empresa, Uniade, Filial se Existir do 1º campo. Exemplo 1 = EEFF, Exemplo 2 = EEUU, Exemplo 3 = UUFF
* @param		array 		$ordem2 			- Ordenação de Empresa, Uniade, Filial se Existir do 2º campo. Exemplo 1 = EEFF, Exemplo 2 = EEUU, Exemplo 3 = UUFF
* @param		string 		$tabela 			- tabela do campo que recebera o tratamento
* @param		string 		$campo 				- campo que recebera o tratamento
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function tratar_substr($ordem1, $ordem2, $tabela, $campo, $db_driver)
{
	$tamanho_inicial = 0;
	$subs = array();
	
	if($db_driver == 'oci8') // Se for oracle
	{
		$substring = 'SUBSTR';
	}
	else
	{
		$substring = 'SUBSTRING';
	}
	
	foreach($ordem1 as $indice => $valor)
	{
		$tamanho_inicial += $valor;
	
		if(isset($ordem2[$indice]))
		{
			$subs[$indice] = $substring . '(' . $tabela . '.' . $campo . ', ' . (($tamanho_inicial + 1) - $valor) . ', ' . $valor . ')';
		}
	}
	
	if(count($subs) > 1)
	{
		//$substr = ' CONCAT(' . implode($subs, ', ') . ')';
		$substr = ' ' . implode($subs, ' + ') . ' ';
	}
	else
	{
		$substr = ' ' . implode($subs);
	}
	
	return $substr;
}



/**
* Metódo:		select_all
* 
* Descrição:	Função Utilizada para pegar todos os campos do mapeamento e transformalos em select
* 
* Data:			10/09/2012
* Modificação:	10/09/2012
* 
* @access		public
* @param		string 		$tabela 			- EX: $this->_db_cliente['tabelas']['transportadoras']
* @param		string 		$campos 			- EX: $this->_db_cliente['campos']['transportadoras']
* @param		string 		$descricao_campo 	- Utilizado para diferenciar campos iguais em tabelas com join
* @param		string 		$exibir_delet 		- Utilizado para retornar o valor do campo D_E_L_E_T_
* @param		string 		$remover_campo 		- Campo será ignorado ignorado e não será retornado 
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function select_all($tabela = NULL, $campos = NULL, $descricao_campo = NULL, $exibir_delet = FALSE, $remover_campo = NULL)
{

	if($descricao_campo)
	{
		$descricao_campo = $descricao_campo . '_';
	}
	
	foreach ($campos as $alias => $campo) {
		if($alias && $campo)
		{
			if($remover_campo)
			{
				if($remover_campo == $alias)
				{
					continue;
				}
			}
			
			if(!$exibir_delet)
			{
				if($campo == 'D_E_L_E_T_')
				{
					continue;
				}
			}
			
			
			$select[] = 'TRIM(' . $tabela . '.' . $campo . ') AS ' . $descricao_campo . $alias;
			
		}
	}
	
	return $select;
}

/**
* Metódo:		consulta_union_empresas
* 
* Descrição:	Função Utilizada para consultar dados do ERP com union se existie mais de uma empresa (Inicialmente foi utilizada para buscar codigos dos clientes e juntar com uma consulta do MYSQL);
* 
* Data:			04/02/2013
* Modificação:	04/02/2013
* 
* @access		public
* @param		object 		$_this						- $_this é o "$this" de controler que está chamando esse helper
* @param		string 		$codigo_representante		
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function consulta_union_empresas($_this = NULL, $codigo_representante = NULL, $get_result = TRUE)
{
	$empresas = $_this->config->item('empresas');
	
	$query_empresa = NULL;
	
	// Se não existir o parametro "compartilhada" no model, fixar como "0" (FALSE = Não compartilhada | TRUE = Compartilhada)
	if(!isset($_this->compartilhada))
	{
		$_this->compartilhada = FALSE;
	}
	
	
	if(count($empresas) > 1 && $_this->compartilhada == FALSE && $_this->config->item('euf') == TRUE)
	{
	
		foreach($empresas as $codigo_empresa => $empresa)
		{
			$_this->_db_cliente = mapeamento($_this->config->config, $codigo_empresa);
			$_this->consulta_erp(array('codigo_empresa' => $codigo_empresa, 'codigo_representante' => $codigo_representante));
			$query_empresa[$codigo_empresa] = $_this->db_cliente->_compile_select();
			$_this->db_cliente->_reset_select();
			
		}

		$_this->db_cliente->from('(' . implode($query_empresa, ' UNION ') . ') union_empresas');
		
	}
	else
	{
		$_this->_db_cliente = mapeamento($_this->config->config, $_this->config->item('empresa_matriz'));
	
		$_this->consulta_erp(array('codigo_empresa' => $_this->config->item('empresa_matriz'), 'codigo_representante' => $codigo_representante));
	}
	
	if($get_result)
	{
		
	
		return $_this->db_cliente->get()->result_array();
	}
	else
	{
		return $_this->db_cliente->get()->row_array();
	}
	
}

/**
* Metódo:		union_empresas
* 
* Descrição:	Função Utilizada para unificar empresas por tabela, se existir mais de uma empresa
* 
* Data:			31/01/2013
* Modificação:	31/01/2013
* 
* @access		public
* @param		object 		$_this						- $_this é o "$this" de controler que está chamando esse helper
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function union_empresas($_this = NULL, $parametros_consulta = NULL)
{
	$empresas = $_this->config->item('empresas');
	
	$query_empresa = NULL;
	
	// Se não existir o parametro "compartilhada" no model, fixar como "0" (FALSE = Não compartilhada | TRUE = Compartilhada)
	if(!isset($_this->compartilhada))
	{
		$_this->compartilhada = FALSE;
	}
	
	
	if(count($empresas) > 1 && $_this->compartilhada == FALSE && $_this->config->item('euf') == TRUE)
	{
		foreach($empresas as $codigo_empresa => $empresa)
		{
			$parametros_consulta['codigo_empresa'] = $codigo_empresa;
		
			$_this->_db_cliente = mapeamento($_this->config->config, $codigo_empresa);
			$_this->consulta($parametros_consulta);
			$query_empresa[$codigo_empresa] = $_this->db_cliente->_compile_select();
			$_this->db_cliente->_reset_select();
			
		}

		$_this->db_cliente->from('(' . implode($query_empresa, ' UNION ') . ') union_empresas');
	
		return $_this->db_cliente->_compile_select();
		
	}
	else
	{
		return NULL;
	}
	
}


/**
* Metódo:		pacote_dados
* 
* Descrição:	Função Utilizada para retornar o dados em "pacotes ou paginas"
* 
* Data:			10/09/2012
* Modificação:	04/02/2013
* 
* @access		public
* @param		object 		$_this					- Utilizado para retornar o model, onde vou conseguir obter o $this->db_cliente ou $this->db e seus metodos;
* @param		String 		$pacote					- Utilizado para retornar dados da "pagina ou pacote" definido
* @param		String 		$_order_by				- O nome do campo utilizado para ordenar a consulta, usado somente para consultas do ERP
* @param		String 		$id						- Codigo utilizado para uniciar uma consulta a partir de um registro
* @param		String 		$codigo_representante	- Codigo utilizado para retornar os dados somente do representante, usado somente para consultas do ERP
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function join_controle_sincronismo($_this, $tabelas, $ultima_sincronizacao) {
	$where = array();
	
	foreach($tabelas as $tabela) {
		if(isset($ultima_sincronizacao) && $ultima_sincronizacao) {
			$_this->db_cliente->join('DW_' . $tabela, 'DW_' . $tabela . '.DW_R_E_C_N_O_ = ' . $tabela . '.R_E_C_N_O_');
			$where[] = 'DW_' . $tabela . '.DT_ATUALIZADO > to_date(\'' .  $ultima_sincronizacao . '\', \'yyyymmddhh24:mi:ss\')';
			
			$_this->db_cliente->where('(' . implode(' OR ', $where) . ')');
		} else {
			$_this->db_cliente->join('DW_' . $tabela, 'DW_' . $tabela . '.DW_R_E_C_N_O_ = ' . $tabela . '.R_E_C_N_O_', 'LEFT');
		}
	}
}


function pacote_dados($_this, $pacote = 1, $campos = FALSE, $_order_by = NULL, $parametros_consulta = NULL)
{

	
	$por_pacote = POR_PACOTE; // Número total de dados para retorno definido em config.php
	if($pacote>0)
	{
		$pacote--; //Pacote atual para retorno (Decrementar porque o valor inicial é 0 do pacote)
	}
	$pacote = $por_pacote * $pacote; // Total de dados X Pacote Atual é igual ao proximo pacote inicial
	
	// Entra no 1º IF se for uma consulta no banco do ERP, se não entra na consulta do MYSQL
	if(isset($_this->db_cliente))
	{
		
		$consulta_union = union_empresas($_this, $parametros_consulta);
		if($consulta_union)
		{
			$consulta = $consulta_union;
		}
		else
		{
			$_this->_db_cliente = mapeamento($_this->config->config, $_this->config->item('empresa_matriz'));
			
			// Se não existir o parametro "compartilhada" no model, fixar como "0" (FALSE = Não compartilhada | TRUE = Compartilhada)
			if(!isset($_this->compartilhada))
			{
				$_this->compartilhada = FALSE;
			}
			
			// Se o compartilhamento for TRUE, não exibir o código da empresa no retorno
			if($_this->compartilhada == TRUE)
			{
				$empresa_matriz = ' ';
			}
			else
			{
				$empresa_matriz = $_this->config->item('empresa_matriz');
			}
			
			$parametros_consulta['codigo_empresa'] = $empresa_matriz;
		
			$consulta = $_this->consulta($parametros_consulta);
		}
		
		
		// Ordenação
		if($_order_by)
		{
			$_this->db_cliente->order_by($_order_by);
		}
		
		
		if(! isset($parametros_consulta['ignorar_incremental'])  ||  !$parametros_consulta['ignorar_incremental']) {
		
			if(isset($parametros_consulta['incremental'])   &&  $parametros_consulta['incremental']) {
				join_controle_sincronismo($_this, $parametros_consulta['tabelas_controle'], $parametros_consulta['ultima_sincronizacao']);
			} else {
				join_controle_sincronismo($_this, $parametros_consulta['tabelas_controle']);
			}
			$_this->db_cliente->ar_select[] =		' to_char(' . 'DW_' . $parametros_consulta['tabelas_principal'] . '.DT_ATUALIZADO ' . ', \'yyyymmddhh24miss\') AS dt_atualizado';
			$_this->db_cliente->ar_select[] = $parametros_consulta['tabelas_principal'] . '.' . 'R_E_C_N_O_ AS recno';
			$_this->db_cliente->ar_select[] = 'DW_' . $parametros_consulta['tabelas_principal'] . '.' . 'DW_D_E_L_E_T_ AS dw_delecao';
		}
		
		if($_this->db_cliente)
		{
			$_this->db_cliente->limit($por_pacote, $pacote);
			$consulta;
			$array_select = $_this->db_cliente->ar_select;
			$array_select_para_union = $_this->db_cliente->ar_aliased_tables;
			//debug_pre($_this->db_cliente->_compile_select());
			
			//debug_pre($_this->db_cliente->_compile_select());

			
			$dados = $_this->db_cliente->get()->result_array();
			//debug_pre($_this->db_cliente->_compile_select());die;
			
			//debug_pre($_this->db_cliente->last_query());
			$dados = recursive_array_replace("'",'',$dados);
			
			if(!$dados OR ($campos == TRUE))
			{
				$dados = retornar_campos_tabela($array_select, $array_select_para_union);
			}
			
		
		//	debug_pre($this->db_cliente->last_query());
			return $dados;
		}
	}
	else // MYSQL
	{
		
		if($_this->db)
		{
			

			$_this->db->limit($por_pacote, $pacote);
			$_this->consulta($parametros_consulta);
			$array_select = $_this->db->ar_select;
			$dados = $_this->db->get()->result_array();
			
			$dados = recursive_array_replace("'",'',$dados);
			
			if(!$dados OR ($campos == TRUE))
			{
				$dados = retornar_campos_tabela($array_select);
			}
			

			
			
			
			return $dados;
		}
	}
}

function retornar_campos_tabela($selects = NULL, $selects_para_union = NULL)
{

	if($selects)
	{
		
		foreach($selects as $select)
		{
			$_campo = strrchr($select,' AS ');
			
			if($_campo)
			{
				$campo[trim($_campo)] = NULL;
			}
			else
			{
				$campo[trim($select)] = NULL;
			}
		}
		
		$dados[] = $campo;
		
		return $dados;
	}
	else if($selects_para_union)
	{
	
		foreach($selects_para_union as $select)
		{
			$_campo = $select;
			
			if($_campo)
			{
				$campo[trim($_campo)] = NULL;
			}
			else
			{
				$campo[trim($select)] = NULL;
			}
		}
	
		$dados[] = $campo;
		
		return $dados;
	}
}

/**
* Metódo:		retornar_total
* 
* Descrição:	Função Utilizada para retornar o número total de dados
* 
* Data:			10/09/2012
* Modificação:	10/09/2012
* 
* @access		public
* @param		object 		$db						- Utilizado para definir a base de dados (EX: $this->db_cliente)
* @param		object 		$consulta				- Utilizado para executar a estrutura de SQL montada no model
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function retornar_total($_this = NULL, $parametros_consulta = NULL)
{
	//die('testde');
	// Entra no 1º IF se for uma consulta no banco do ERP, se não entra na consulta do MYSQL
	if(isset($_this->db_cliente))
	{
		
		
		if(isset($parametros_consulta['incremental'])   &&  $parametros_consulta['incremental']) {
			
		
		
			join_controle_sincronismo($_this, $parametros_consulta['tabelas_controle'], $parametros_consulta['ultima_sincronizacao']);
			$_this->db_cliente->ar_select[] =		' to_char(' . 'DW_' . $parametros_consulta['tabelas_principal'] . '.DT_ATUALIZADO ' . ', \'dd/mm/yyyy hh24:mi:ss\') AS DT_ATUALIZADO';
			
		}
		
		
		$consulta_union = union_empresas($_this, $parametros_consulta);
		if($consulta_union)
		{
			$consulta = $consulta_union;
		}
		else
		{
			$_this->_db_cliente = mapeamento($_this->config->config, $_this->config->item('empresa_matriz'));
			$consulta = $_this->consulta($parametros_consulta);
		}

		//$this->db_cliente->get()->result();
		//debug_pre($this->db_cliente->last_query());
		//debug_pre($this->db_cliente->_compile_select());
		$total = $_this->db_cliente->count_all_results();
	}
	else // MYSQL
	{
		$_this->consulta($parametros_consulta);
		
		$total = $_this->db->count_all_results();
	}
	
	return ceil($total / POR_PACOTE);
}


/**
* Metódo:		obter_tipo_cliente
* 
* Descrição:	Função Utilizada para retornar o a descrição do tipo de cliente
* 
* Data:			10/09/2012
* Modificação:	10/09/2012
* 
* @access		public
* @param		string 		$tipo				- Tipo de Cliente

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tipo_cliente($tipo = NULL)
{
	switch ($tipo) {
		case 'L':
			return 'Produtor Rural';
			break;
			
		case 'F':
			echo 'Cons. Final';
			break;
			
		case 'R':
			echo 'Revendedor';
			break;
			
		case 'S':
			echo 'ICMS Solidário sem IPI na base';
			break;
			
		case 'X':
			echo 'Exportação';
			break;
			
	}
	
}

//--------

function formatar_numero($valor = NULL, $decimal = 2, $dec_point = '.', $thousands_sep = '')
{		
	return number_format(doubleval($valor), $decimal, $dec_point, $thousands_sep);	
}

function verificar_tipo_pessoa($cgc)
{
	if(strlen($cgc) == 11) //Se for CPF
	{
		return 'F';
	}
	else
	{
		return 'J';
	}
}


function debug_pre($array = NULL, $parar = TRUE)
{
	echo '<pre>';
	print_r($array);
	echo '</pre>';
	
	if($parar){
		die();
	}
}

/**
* Metódo:		obter_data_atual
* 
* Descrição:	Função Utilizada para obter a data atual pelo banco de dados
* 
* Data:			20/02/2013
* Modificação:	20/02/2013
* 
* @access		public

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_data_atual()
{
	$CI =& get_instance();
	$db = $CI->load->database('db_cliente', TRUE);

	
	if($db->dbdriver == 'oci8') // Se for oracle
	{
		return "to_char(sysdate,'YYYYMMDD')";
	}
	else
	{
		return "CONVERT(VARCHAR(12),GETDATE(),112)";
	}
}

function isset_valor($valor)
{
	if(isset($valor))
	{
		return $valor;
	}
	else
	{
		return ' ';
	}
}

function extrairTelefone($numero)
{
	$arrTelefone = array();
	$numero = preg_replace('/([^0-9])/','',$numero);	
	if(strlen($numero) >= 8 )
	{
		$arrTelefone['numero'] 			= substr($numero,strlen($numero)-8,strlen($numero));
	}
	if(strlen($numero) == 10)
	{
		$arrTelefone['codigoCidade'] 	= substr($numero, strlen($numero) - 10, 2);
	}
	if(strlen($numero) == 11)
	{
		$arrTelefone['codigoCidade'] 	= substr($numero, strlen($numero) - 11, 2);
	}
	return $arrTelefone;
}

/**
 * MAGIA NEGRA! O.o
 */
function recursive_array_search($needle,$haystack) 
{
    foreach($haystack as $key=>$value) 
    {
        $current_key=$key;
        if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) 
        {
            return $current_key;
        }
    }
    return false;
}



 // Recursive String Replace - recursive_array_replace(mixed, mixed, array);
function recursive_array_replace($find, $replace, $dados){
	if (!is_array($dados)) {
		return str_replace($find, $replace, $dados);
	}
	$newArray = array();
	foreach ($dados as $key => $value) {
		$newArray[$key] = recursive_array_replace($find, $replace, $value);
	}
	return $newArray;
}