<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//ini_set('display_errors', '1');
include "lib/nusoap.php";


/**
* Função:		obter_tes()
* 
* Descrição:	Retorna a TES de um produto.
* 
* 
* @param String $cliente 		= codigo do cliente.
* @param String $cliente_loja 	= numero da loja do cliente.
* @param String $produto 		= codigo do produto.
*/
function obter_tes($cliente, $cliente_loja, $produto)
{
	$ci =& get_instance();	
	$webservice = $ci->config->item('webservice');
	
	if ($webservice['TES'] != '') 
	{
		$client 		=	'';
		$result 		=	'';	 
		$filial  		=	'';
		$anat 			=	array(' ');
		$avalnat 		=	array(' ');
	 
		$client = new soapclient($webservice['TES']);
					
		$apar=array('parameters' =>array( 
											'CCLIENTE'  => (string)$cliente, //codigo cliente
											'CLOJA' =>(string)$cliente_loja, // loja do cliente
											'CPRODUTO' =>(string)$produto // codigo do produto
		));
								  
		$result = $client->__soapCall('RETORNATES',$apar);
	   
		return $result->RETORNATESRESULT;
   } 
   else 
   {
		return '0';
   }
               
}

/**
 * Função:		obter_st()
 * 
 * Descrição:	Retorna a ST de um produto.
 * 
 * @param 	String 	- $itens 		- Itens do pedido que devem ser calculados.
 * @return 	Array 	- $arrImpostos 	- Array com os calculos do webservices
 */
function obter_st($itens)
{
	//Setar instancia do codeigniter
	$ci 		=& get_instance();
	
	//Buscar a configuração com a URL do webservice
	$webservice = $ci->config->item('webservice');
	
	//Setar as variaveis da função
	$soapCliente 	='';
	$resultado 		='';
	$parametros 	= array();
	$arrImpostos 	= array();
 		
	//Verifixa se existe uma URL cadastrada
	if ($webservice['IMPOSTOS'] != '') 
	{	

		// Instanciar o cliente soap
		$soapCliente = new soapclient($webservice['IMPOSTOS']);
					
		// Montar array com os parametros para o soap cliente
		$parametros=array('parameters' => array('APRODUTO' => array('ITENS' => $itens)));

		//Realizar a chamada da função RETORNAST
		$resultado = $soapCliente->__soapCall('RETORNAST',$parametros);
		
		//Se for array entrar no foreach
		if(is_array($resultado->RETORNASTRESULT->STRRETORNOR))
		{
			//Percorrer o array de impostos retornados
			foreach($resultado->RETORNASTRESULT->STRRETORNOR as $imposto)
			{
				$arrImpostos[] = array('ICMS' 	=> $imposto->VALORICM, 
										'IPI' 				=> $imposto->VALORIPI, 
										'ST' 				=> $imposto->VALORST, 
										'TES' 				=> $imposto->TES,
										'CF' 				=> $imposto->CF,
										'CFOP' 			=> $imposto->CFOP, 
										'PRODUTO' 		=> $imposto->PRODUTO
				);
			}
				
		}
		else
		{
			$arrImpostos[] = array('ICMS' 		=> $resultado->RETORNASTRESULT->STRRETORNOR->VALORICM, 
									'IPI' 					=> $resultado->RETORNASTRESULT->STRRETORNOR->VALORIPI, 
									'ST' 					=> $resultado->RETORNASTRESULT->STRRETORNOR->VALORST, 
									'TES' 					=> $resultado->RETORNASTRESULT->STRRETORNOR->TES,
									'CF' 					=> $resultado->RETORNASTRESULT->STRRETORNOR->CF,
									'CFOP' 				=> $resultado->RETORNASTRESULT->STRRETORNOR->CFOP, 
									'PRODUTO' 			=> $resultado->RETORNASTRESULT->STRRETORNOR->PRODUTO
				);
		}
		
		//Retornar array com os impostos
		return $arrImpostos;
   } 
   else //Se não existir uma URL na configuração retornar zerado
   {
		return array('ICMS' => '0', 'IPI' => '0', 'ST' => '0', 'TES' => '0', 'CF' => '0', 'CFOP' => '0', 'PRODUTO' => '0');
   }
}


/**
* Função:		obter_parametros()
* 
* Descrição:	Retorna os parametros do protheus
*/
function obter_parametros($empresa = '010', $filial = '01')
{
	$ci =& get_instance();
	$webservice = $ci->config->item('webservice');
	
	if (verificar_webservices($webservice['MV'])) 
	{
		$result 		=	'';	 
		
		try
		{
			$client = new soapclient($webservice['MV']);
						
			$apar=array('parameters' =>array(
											 'NEMPRESA'  => (string)$empresa,
											 'NFILIAL'  => (string)$filial
			));
			
			$result = $client->__soapCall('RetornarMV',$apar);	   

			return $result->RETORNARMVRESULT->STRMV;
		}
		catch(Exception $e)
		{
			return FALSE;
		}
   } 
   else 
   {
		return '0';
   }
               
}

/**
 * Função: 			verificar_webservices()
 *
 * Descrição: 		Verificar se o link do webservices está rodando corretamente.
 *					se o webservices estiver rodando corretamente retornar TRUE se não FALSE
 *
 * @param 			$webservices - 	String
 * @return 			Boolean - True ou False
 */
function verificar_webservices($webservices)
{
	$cURL = curl_init($webservices);
	curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);

	// Seguir qualquer redirecionamento que houver na URL
	curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, true);
	//tempo de espera da pesquisa
	curl_setopt($cURL, CURLOPT_TIMEOUT, 60); 

	$resultado = curl_exec($cURL);

	// Pega o código de resposta HTTP
	$resposta = curl_getinfo($cURL, CURLINFO_HTTP_CODE);

	curl_close($cURL);

	if ($resposta == '404' OR $resposta == '0')
	{
		return false;
	} 
	else 
	{
		return true;
	}
}



/*
function httpRequest($host, $port, $method, $path, $params) {
	// Params are a map from names to values
	$paramStr = '';
	foreach ($params as $name, $val) {
		$paramStr .= $name . '=';
		$paramStr .= urlencode($val);
		$paramStr .= '&';
	}

	// Assign defaults to $method and $port, if needed
	if (empty($method)) {
		$method = 'GET';
	}
	$method = strtoupper($method);
	if (empty($port)) {
		$port = 80; // Default HTTP port
	}

	// Create the connection
	$sock = fsockopen($host, $port);
	if ($method == "GET") {
		$path .= "?" . $paramStr;
	}
	fputs($sock, "$method $path HTTP/1.1\r\n");
	fputs($sock, "Host: $host\r\n");
	fputs($sock, "Content-type: " .
               "application/x-www-form-urlencoded\r\n");
	if ($method == "POST") {
		fputs($sock, "Content-length: " . 
        strlen($paramStr) . "\r\n");
	}
	fputs($sock, "Connection: close\r\n\r\n");
	if ($method == "POST") {
		fputs($sock, $paramStr);
	}

	// Buffer the result
	$result = "";
	while (!feof($sock)) {
		$result .= fgets($sock,1024);
	}

	fclose($sock);
	return $result;
}*/