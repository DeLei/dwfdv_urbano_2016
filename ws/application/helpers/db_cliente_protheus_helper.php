<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function mapeamento($config, $codigo_empresa)
{

    $cod = $codigo_empresa;

    // Tabela de pedidos definada no arquivo "config/config.php"
    $pedido_tabela = $config['pedidos_dw']['tabela'];
    $pedido_campo  = $config['pedidos_dw']['campo'];

    // Tabela de prospects definada no arquivo "config/config.php"
    $prospects_tabela = $config['prospects']['tabela'];
    $prospects_campo  = $config['prospects']['campo'];

    $config['db_cliente'] = array(
        'configuracoes' => array(
            'pedidos' => array(
                'recno' => 'gerar' # auto - Para RECNO automatico gerado pelo banco | gerar - Para gerar um RECNO
            )
        ),
        'tabelas'       => array(
            'tabelas_precos'                    => 'DA0' . $cod,
            'produtos_tabelas_precos'           => 'DA1' . $cod,
            'prazos_por_tabela_precos'          => 'RZC' . $cod, //CUSTOM								  
            'formas_pagamento_por_tabela_preco' => 'RZB' . $cod, //CUSTOM
            'clientes'                          => 'SA1' . $cod,
            'representantes'                    => 'SA3' . $cod,
            'transportadoras'                   => 'SA4' . $cod,
            'produtos'                          => 'SB1' . $cod,
            'produtos_estoque'                  => 'SB2' . $cod,
            'grupos_produtos'                   => 'SBM' . $cod,
            'pedidos'                           => 'SC5' . $cod,
            'itens_pedidos'                     => 'SC6' . $cod,
            'metas'                             => 'SCT' . $cod,
            'itens_notas_fiscais'               => 'SD2' . $cod,
            'titulos'                           => 'SE1' . $cod,
            'premiacoes'                        => 'SE3' . $cod,
            'formas_pagamento'                  => 'SE4' . $cod,
            'tipo_entrada_saida'                => 'SF4' . $cod,
            'notas_fiscais'                     => 'SF2' . $cod,
            'pedidos_dw'                        => $pedido_tabela,
            'prospects'                         => $prospects_tabela,
            'regra_desconto'                    => 'ACO' . $cod,
            'cadastro_municipios'               => 'CC2' . $cod,
            'euf'                               => 'SX2' . $cod,
            'generica'                          => 'SX5' . $cod, // tabela genérica de dados do ERP
            'cadastro_pais'                     => 'SYA' . $cod,
            'representantes_pedidos'            => '', // em branco para protheus
            'opcionais_produtos'                => '',
            // CUSTOM: 177 / 000874  - 17/10/2013 - Localizar: CUST-PORTO-DESTINO
            'frete_carreteiro'                  => 'DUS' . $cod,
            'frete_carreteiro_por_rota'         => 'DTS' . $cod,
            'portos_aeroportos'                 => 'SY9' . $cod,
            // FIM CUSTOM: 177 / 000874  - 17/10/2013
            //CUSTOM
            'view_estatisticas_cliente'         => 'V_CLIENTE_PORTAL',
            //FIM CUSTOM	
            'prazos_entrega'                    => 'SZ3' . $cod,
            'clientes_setor'                    => 'DA7' . $cod, //CUSTOM-PRAZO ENTREGA => Clientes por setor
            'zonas_setor'                       => 'DA9' . $cod, //CUSTOM-PRAZO ENTREGA => Zonas por setor
            'rotas'                             => 'DA8' . $cod, //CUSTOM-PRAZO ENTREGA => Rotas
            'ceps'                              => 'JC2' . $cod, //CUSTOM-VALIDACAO DE CEPS
            'prospects_processados'             => 'SUS' . $cod, //CUSTOM-PROSPECTS na Empresa
			'comissoes_representantes'			=> 'V_COMISSOES_REPRESENTANTES',  		// 	CUSTOM E0179/2014 - 03/07/2014
			'recebimentos_representantes'		=> 'V_RECEBIMENTOS_REPRESENTANTE', // 	CUSTOM E0178/2014 - 03/07/2014
			'pedidos_por_rota'					=> 'V_ROTA_PORTAL', //  CUSTOM E0174/2014 - 04/07/2014
			'categorias_produtos'				=> 'ACV010', 	//  CUSTOM E0174/2014 - 11/07/2014
			'desconto_por_fardo'				=> 'RZA010',	//  CUSTOM E0174/2014 - 11/07/2014
			'pedidos_liberados'				=> 'SC9'.$cod,	//  CUSTOM 002514 - [CUSTOM] Melhoria do DW Força de Vendas
			
			
        ),
        'campos'        => array(
            'regra_desconto'                    => array(
                'filial'              => 'ACO_FILIAL',
                'codigo'              => 'ACO_CODREG',
                'descricao'           => 'ACO_DESCRI',
                'codigo_cliente'      => 'ACO_CODCLI',
                'codigo_loja'         => 'ACO_LOJA',
                'tabela_preco'        => 'ACO_CODTAB',
                'condicao_pagamento'  => 'ACO_CONDPG',
                'forma_pagamento'     => 'ACO_FORMPG',
                'percentual_desconto' => 'ACO_PERDES',
                'vigencia_inicio'     => 'ACO_DATDE',
                'vigencia_final'      => 'ACO_DATATE',
                'faixa'               => 'ACO_FAIXA',
                'delecao'             => 'D_E_L_E_T_'
            ),
            'cadastro_municipios'               => array(
                'codigo' => 'CC2_CODMUN',
                'nome'   => 'CC2_MUN',
                'uf'     => 'CC2_EST',
                'filial' => 'CC2_FILIAL'
            ),
            'euf'                               => array(
                'chave'           => 'X2_CHAVE',
                'modo_filial'     => 'X2_MODO',
                'modo_unidade'    => 'X2_MODOUN',
                'modo_empresa'    => 'X2_MODOEMP',
                'tamanho_filial'  => 'X2_TAMFIL',
                'tamanho_empresa' => 'X2_TAMEMP',
                'tamanho_unidade' => 'X2_TAMUN',
                'delecao'         => 'D_E_L_E_T_'
            ),
            'generica'                          => array(
                'filial'             => 'X5_FILIAL',
                'tabela'             => 'X5_TABELA',
                'chave'              => 'X5_CHAVE',
                'descricao'          => 'X5_DESCRI',
                'descricao_espanhol' => 'X5_DESCSPA',
                'descricao_ingles'   => 'X5_DESCENG',
                'delecao'            => 'D_E_L_E_T_',
            ),
            'cadastro_pais'                     => array(
                'filial'    => 'YA_FILIAL',
                'codigo'    => 'YA_CODGI',
                'descricao' => 'YA_DESCR',
                'sigla'     => 'YA_SIGLA'
            ),
            'prospects'                         => array(
                'codigo_empresa'       => $prospects_campo . '_CODEMP',
                'codigo'               => 'R_E_C_N_O_',
				'chave'               => 'R_E_C_N_O_',
                'codigo_loja'          => $prospects_campo . '_LOJA',
                'data_emissao'         => $prospects_campo . '_EMISSA',
                'time_emissao'         => $prospects_campo . '_TIMEEM',
                'tipo_pessoa'          => $prospects_campo . '_PESSOA',
                'id_feira'             => $prospects_campo . '_EVENTO',
                'tipo'                 => $prospects_campo . '_TIPO',
                'id_ramo'              => $prospects_campo . '_RAMO', // CUSTOM: 177 / 000874  - 16/10/2013 - Localizar: CUST-RAMOS 
                'nome'                 => $prospects_campo . '_NOME',
                'nome_fantasia'        => $prospects_campo . '_NREDUZ',
                'cgc'                  => $prospects_campo . '_CGC',
                'rg'                   => $prospects_campo . '_PFISIC',
                'inscricao_estadual'   => $prospects_campo . '_INSCR',
                'inscricao_municipal'  => $prospects_campo . '_INSCRM',
                'inscricao_rural'      => $prospects_campo . '_INSCRU',
                'data_nascimento'      => $prospects_campo . '_DTNASC',
                'site'                 => $prospects_campo . '_HPAGE',
                'ddi'                  => $prospects_campo . '_DDI',
                'ddd'                  => $prospects_campo . '_DDD',
                'telefone'             => $prospects_campo . '_TEL',
                'fax'                  => $prospects_campo . '_FAX',
                'telex'                => $prospects_campo . '_TELEX',
                'email'                => $prospects_campo . '_EMAIL',
                'email_fiscal'         => $prospects_campo . '_MAILFI', // CUSTOM: 177 / 000874  - 16/10/2013 - Localizar: CUST-EMAIL_FISCAL 
                'nome_contato'         => $prospects_campo . '_CONTAT',
                'cargo_contato'        => $prospects_campo . '_CONCAR',
                'cep'                  => $prospects_campo . '_CEP',
                'endereco'             => $prospects_campo . '_END',
                'bairro'               => $prospects_campo . '_BAIRRO',
                'complemento'          => $prospects_campo . '_COMPLE',
                'estado'               => $prospects_campo . '_EST',
                'codigo_municipio'     => $prospects_campo . '_CODMUN',
                'pais'                 => $prospects_campo . '_PAIS',
                'email_contato'        => $prospects_campo . '_CONMAI',
                'telefone_contato'     => $prospects_campo . '_CONTEL',
                /* Informações Financeiras */
                'faturamento'          => $prospects_campo . '_FATANU',
                'faturamento_anual'    => $prospects_campo . '_FATVAL',
                'ano_fat_atual'        => $prospects_campo . '_FATANO',
                'sugestao_lim_credito' => $prospects_campo . '_SUGLIM',
                'banco_cobranca'       => $prospects_campo . '_BANCO1',
                'banco'                => $prospects_campo . '_RBANCO',
                'agencia'              => $prospects_campo . '_RAGEN',
                'conta_corrente'       => $prospects_campo . '_RCONTA',
                'fone_banco'           => $prospects_campo . '_RFONEB',
                'referencia_1'         => $prospects_campo . '_RFORN1',
                'fone_referencia_1'    => $prospects_campo . '_RFONE1',
                'referencia_2'         => $prospects_campo . '_RFORN2',
                'fone_referencia_2'    => $prospects_campo . '_RFONE2',
                /* Fim Informações Financeiras */

                /* Outras Informações */
                'funcionarios'         => $prospects_campo . '_QTFUNC',
                'area_loja'            => $prospects_campo . '_AREAL',
                'numero_lojas'         => $prospects_campo . '_NUMLOJ',
                'numero_funcionarios'  => $prospects_campo . '_NUMFUN',
                'numero_veiculos'      => $prospects_campo . '_NUMVEI',
                'imovel_proprio'       => $prospects_campo . '_IMOVPR',
                'socio_1'              => $prospects_campo . '_SOCIO1',
                'cpf_socio_1'          => $prospects_campo . '_CPF1',
                'socio_2'              => $prospects_campo . '_SOCIO2',
                'cpf_socio_2'          => $prospects_campo . '_CPF2',
                'responsavel_compra'   => $prospects_campo . '_COMPRD',
                'resp_contas_pagar'    => $prospects_campo . '_CTPAGA',
                'condicoes_pagamento'  => $prospects_campo . '_COND',
                'observacao'           => $prospects_campo . '_OBS',
                'descricao'            => $prospects_campo . '_DESCCL',
                /* Fim Outras Informações */
                'bloqueado'            => $prospects_campo . '_MSBLQL',
                'codigo_representante' => $prospects_campo . '_VEND',
                'status'               => $prospects_campo . '_STATUS',
                'codigo_cliente'       => $prospects_campo . '_CLIENT',
                'time_importacao'      => $prospects_campo . '_TIMEIM',
                'filial'               => $prospects_campo . '_FILIAL',
                
                'latitude'             => $prospects_campo . '_LAT',
                'longitude'            => $prospects_campo . '_LONG',
                'versao'               => $prospects_campo . '_VERSAO',
				'codigo_preposto'		=>$prospects_campo . '_PREPOS',
				'suframa'			   => $prospects_campo . '_SUFRAM',  //CUSTOM E0180/2014 - 03/07/2014 
				'motivo_reprovacao'    => $prospects_campo . '_MOTREP',
                'delecao'              => 'D_E_L_E_T_',
				
				'acao'					=> $prospects_campo . '_ACAO',
            //'recno' 					=> 'R_E_C_N_O_'
            ),
            'pedidos_dw'                        => array(
                'codigo_empresa'        => $pedido_campo . '_CODEMP',
                'filial'                => $pedido_campo . '_FILIAL',
                'id_pedido'             => $pedido_campo . '_IDPED',
                'data_emissao'          => $pedido_campo . '_EMISSA',
                'time_emissao'          => $pedido_campo . '_TIMEEM',
                'time_importacao'       => $pedido_campo . '_TIMEIM', // Timestamp informando o momento em que o pedido foi  transmitido para a tabela de temporária zb7
                'time_conversao'        => $pedido_campo . '_TIMECV', // Timestamp do momento em que o pedido foi importado eo ERP.
                'id_usuario'            => $pedido_campo . '_IDUSER',
                'codigo_representante'  => $pedido_campo . '_VEND1',
                'codigo_representante2' => $pedido_campo . '_VEND2',
                'codigo_representante3' => $pedido_campo . '_VEND3',
                'codigo_representante4' => $pedido_campo . '_VEND4',
                'codigo_representante5' => $pedido_campo . '_VEND5',
                'comissao'              => $pedido_campo . '_COMIS1',
                'comissao2'             => $pedido_campo . '_COMIS2',
                'comissao3'             => $pedido_campo . '_COMIS3',
                'comissao4'             => $pedido_campo . '_COMIS4',
                'comissao5'             => $pedido_campo . '_COMIS5',
                'desconto_item'         => $pedido_campo . '_DESCON',
                'desconto1'             => $pedido_campo . '_DESC1',
                'desconto2'             => $pedido_campo . '_DESC2',
                'desconto3'             => $pedido_campo . '_DESC3',
                'desconto4'             => $pedido_campo . '_DESC4',
                'tabela_precos'         => $pedido_campo . '_TABELA',
                'id_feira'              => $pedido_campo . '_EVENTO',
                'status'                => $pedido_campo . '_STATUS',
                'motivo_reprovacao'     => $pedido_campo . '_MOTREP',
                'codigo_cliente'        => $pedido_campo . '_CLIENT',
                'loja_cliente'          => $pedido_campo . '_LOJACL',
                'id_prospects'          => $pedido_campo . '_PROSPE',
                'condicao_pagamento'    => $pedido_campo . '_CONDPA',
                'tipo_frete'            => $pedido_campo . '_TPFRET',
                'dirigido_redespacho'   => $pedido_campo . '_DR',
                'codigo_transportadora' => $pedido_campo . '_TRANSP',
                'despesa'               => $pedido_campo . '_DESPES',
                'tipo_pedido'           => $pedido_campo . '_TIPO',
                'tipo_cliente'          => $pedido_campo . '_TIPOCL',
                'mensagem_nota'         => $pedido_campo . '_MENNOT',
                'cliente_entrega'       => $pedido_campo . '_CLIEN',
                'loja_entrega'          => $pedido_campo . '_LOJAEN',
                'data_entrega'          => $pedido_campo . '_FECENT',
                'pedido_cliente'        => $pedido_campo . '_PEDCLI',
                'codigo_produto'        => $pedido_campo . '_PRODUT',
                'numero_item'           => $pedido_campo . '_ITEM',
                'unidade_medida'        => $pedido_campo . '_UM',
                'preco_unitario'        => $pedido_campo . '_PRUNIT',
                'preco_venda'           => $pedido_campo . '_PRCVEN',
                'quantidade'            => $pedido_campo . '_QTDVEN',
                'total_desconto_item'   => $pedido_campo . '_VALDES',
                'tipo_entrada_saida'    => $pedido_campo . '_TES',
                'codigo_fiscal'         => $pedido_campo . '_CF',
                'ipi'                   => $pedido_campo . '_IPI',
                'st'                    => $pedido_campo . '_ST',
                'icms'                  => $pedido_campo . '_ICMS',
                'local'                 => $pedido_campo . '_LOCAL',
                'latitude'              => $pedido_campo . '_LAT',
                'longitude'             => $pedido_campo . '_LONG',
                'tipo_bonificacao'      => $pedido_campo . '_MOTBON', // CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-BONIFICACAO
                'tipo_venda'            => $pedido_campo . '_VENDA', // CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA	
                'banco'                 => $pedido_campo . '_BANCO', //CUSTOM 177 / 000874 - 04/11/2013 - Localizar: CUST-BANCO
                'tipo_transporte'       => $pedido_campo . '_TIPTRA', //CUSTOM 177 / 000874 - 27/11/2013 - Localizar: CUST-TIPO TRANSPORTE
                'porto_destino'         => $pedido_campo . '_PORTO', //CUSTOM 177 / 000874 - 27/11/2013 - Localizar: CUST-PORTO
                'bloqueado'             => $pedido_campo . '_MSBLQL', //CUSTOM 177 / 000874 - 27/11/2013 - Localizar: CUST-BLOQUEADO
                'versao'                => $pedido_campo . '_VERSAO',
                'observacao_comercial'  => $pedido_campo . '_OBSCOM',
				
                'delecao'               => 'D_E_L_E_T_',
                'chave'                 => 'R_E_C_N_O_',
                'forma_pagamento_real'  => $pedido_campo . '_FORPAG', //forma_pagamento é utilizada pela condição de pagamento
                'codigo_rota'           => $pedido_campo . '_ROTA',
                'peso'                  => $pedido_campo . '_PESOL',
                'motivo_troca'          => $pedido_campo . '_MOTTRO', //Código do motivo de troca
                'lote_troca'            => $pedido_campo . '_LOTEUR', //Lote de troca
                'descricao_motivo'      => $pedido_campo . '_DESCMO', //Observações de (Troca ou Bonificação)
				'codigo_preposto'		=> $pedido_campo . '_PREPOS', //E0248/2014 - Preposto
            ),
            'pedidos'                           => array(
                'filial'                 => 'C5_FILIAL',
                'codigo_representante'   => 'C5_VEND1',
                'data_emissao'           => 'C5_EMISSAO',
                'codigo'                 => 'C5_NUM',
                'tipo'                   => 'C5_TIPO',
                'codigo_cliente'         => 'C5_CLIENTE',
                'codigo_ordem_compra'    => 'C5_PEDCLI',
                'loja_cliente'           => 'C5_LOJACLI',
                'codigo_nota_fiscal'     => 'C5_NOTA',
                'codigo_forma_pagamento' => 'C5_CONDPAG',
                'codigo_transportadora'  => 'C5_TRANSP',
                'tipo_frete'             => 'C5_TPFRETE',
                'tabela_precos'          => 'C5_TABELA',
                'mensagem'               => 'C5_MENNOTA',
                'data_entrega'           => 'C5_FECENT',
                'cliente_entrega'        => 'C5_CLIENT',
                'loja_entrega'           => 'C5_LOJAENT',
                'desconto1'              => 'C5_DESC1',
                'desconto2'              => 'C5_DESC2',
                'desconto3'              => 'C5_DESC3',
                'desconto4'              => 'C5_DESC4',
                'tipo_bonificacao'       => 'C5_MOTBON', // CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-BONIFICACAO
                'tipo_venda'             => 'C5_VENDA', // CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA
                'banco'                  => 'C5_BANCO', //CUSTOM 177 / 000874 - 04/11/2013 - Localizar: CUST-BANCO
                'bloqueado'              => 'C5_MSBLQL', //CUSTOM 177 / 000874 - 04/11/2013 - Localizar: CUST-BLOQUEADO
                'tipo_transporte'        => 'C5_TIPTRA', //CUSTOM 177 / 000874 - 27/11/2013 - Localizar: CUST-TIPO TRANSPORTE
                'porto_destino'          => 'C5_PORTO', //CUSTOM 177 / 000874 - 27/11/2013 - Localizar: CUST-PORTO
                'codigo_pedido_portal'   => 'C5_TABLET', // Solicitação via email, adicionar campo C5_tablet, para possibilitar a consulta de pedidos processados através do código gerado no pedido.				
                'codigo_preposto'		=>  'C5_PREPOST', // CUSTOM E0248/2014 - Preposto.	
				'delecao'                => 'D_E_L_E_T_',
                'forma_pagamento_real'   => 'C5_FORPAG',
            ),
            'itens_pedidos'                     => array(
                'filial'                      => 'C6_FILIAL',
                'codigo_pedido'               => 'C6_NUM',
                'valor_total_item'            => 'C6_VALOR',
                'total_desconto_item'         => 'C6_VALDESC',
                'quantidade_vendida_produto'  => 'C6_QTDVEN',
                'quantidade_faturada_produto' => 'C6_QTDENT',
                'preco_produto'               => 'C6_PRCVEN',
                'preco_unitario'              => 'C6_PRUNIT',
                'codigo_item'                 => 'C6_ITEM',
                'tes'                         => 'C6_TES',
                'codigo_produto'              => 'C6_PRODUTO',
                'descricao_produto'           => 'C6_DESCRI',
                'unidade_medida_produto'      => 'C6_UM',
                'pedido_cliente'              => 'C6_PEDCLI',
                'codigo_cliente'              => 'C6_CLI',
                'codigo_loja'                 => 'C6_LOJA',
                'local'                       => 'C6_LOCAL',
                'numero_nota_fiscal' 	      => 'C6_NOTA',
				'motivo_troca'       		  => 'C6_MOTTRO',
				'lote_troca'       			  => 'C6_LOTEURB',
                'delecao'                     => 'D_E_L_E_T_'
            ),
            'representantes_pedidos'            => array(// em branco para protheus
                'codigo_representante' => '',
                'codigo_pedido'        => '',
            ),
            'premiacoes'                        => array(
                'codigo_representante' => 'E3_VEND',
                'data_emissao'         => 'E3_EMISSAO',
                'valor'                => 'E3_COMIS',
                'delecao'              => 'D_E_L_E_T_'
            ),
            'metas'                             => array(
                'codigo_representante' => 'CT_VEND',
                'data'                 => 'CT_DATA',
                'valor'                => 'CT_VALOR',
                'delecao'              => 'D_E_L_E_T_'
            ),
            'formas_pagamento'                  => array(
                'filial'    => 'E4_FILIAL',
                'codigo'    => 'E4_CODIGO',
                'descricao' => 'E4_DESCRI',
                'delecao'   => 'D_E_L_E_T_'
            ),
            //CUSTOM
            'formas_pagamento_por_tabela_preco' => array(
                'filial'             => 'RZB_FILIAL',
                'codigo'             => 'RZB_CODIGO',
                'condicao_pagamento' => 'RZB_COND',
                'delecao'            => 'D_E_L_E_T_'
            ),
            // FIM CUSTOM	
            'transportadoras'                   => array(
                'codigo'   => 'A4_COD',
                'nome'     => 'A4_NOME',
                'telefone' => 'A4_TEL',
                'filial'   => 'A4_FILIAL',
                'delecao'  => 'D_E_L_E_T_',
            ),
            'tabelas_precos'                    => array(
                'filial'                => 'DA0_FILIAL',
                'filial_saida'          => 'DA0_FILIAL',
                'codigo'                => 'DA0_CODTAB',
                'descricao'             => 'DA0_DESCRI',
                'condicao_pagamento'    => 'DA0_CONDPG',
                // CUSTOM: 177 / 0008740 - 25/10/2013 - Localizar: CUST-TABELA-PRECO 	
                'vinculo_representante' => 'DA0_VEND',
                'tipo_frete'            => 'DA0_TIPFRT',
                'tipo_transporte'       => 'DA0_TIPTRA',
                'porto_destino'         => 'DA0_PORTO',
                // FIM CUSTOM: 177 / 0008740 - 25/10/2013						
                'vigencia_inicio'       => 'DA0_DATDE',
                'vigencia_final'        => 'DA0_DATATE',
                'ativo'                 => 'DA0_ATIVO',
				'cep_inicial'			=> 'DA0_CEPDE',
				'cep_final'				=> 'DA0_CEPATE',
                'recdel'                => 'R_E_C_D_E_L_',
                'delecao'               => 'D_E_L_E_T_',
            ),
            'produtos_tabelas_precos'           => array(
                'codigo_tabela_precos' => 'DA1_CODTAB',
                'codigo_produto'       => 'DA1_CODPRO',
                //CUSTOM 177 - 000870 - 14/10/2013
                'ativo'                => 'DA1_ATIVO',
                'data_vigencia'        => 'DA1_DATVIG',
                //FIM CUSTOM 177 - 000870 - 14/10/2013
                'preco'                => 'DA1_PRCVEN',
                //	'validacao_desconto' 	=> 'DA1_DESMAX',
                'variacao_maxima'      => 'DA1_VARMAX',
                'filial'               => 'DA1_FILIAL',
                'delecao'              => 'D_E_L_E_T_'
            ),
            // CUSTOM	
            'prazos_por_tabela_precos'          => array(
                'filial'       => 'RZC_FILIAL',
                'tabela_preco' => 'RZC_TABELA',
                'prazo'        => 'RZC_PRAZO',
                'delecao'      => 'D_E_L_E_T_'
            ),
            'prazo_pagamento'                   => array(
                'condicao_pagamento' => 'RZB_COND',
                'codigo'             => 'RZB_CODIGO',
                'filial'             => 'RZB_FILIAL',
                'delecao'            => 'D_E_L_E_T_'
            ),
            // FIM CUSTOM	
            'produtos'                          => array(
                'codigo'                 => 'B1_COD',
                'filial'                 => 'B1_FILIAL',
                'codigo_real'            => 'B1_COD',
                'codigo_grupo'           => 'B1_GRUPO',
                'codigo_classe'          => '',
                'descricao'              => 'B1_DESC',
                'quantidade_embalagem'   => 'B1_QE',
                'unidade_medida'         => 'B1_UM',
                'converter'              => 'B1_CONV',
                'tipo_converter'         => 'B1_TIPCONV',
                'ipi'                    => 'B1_IPI',
                'icms'                   => 'B1_PICM',
                'cofins'                 => 'B1_COFINS',
                'pis'                    => 'B1_PIS',
                'delecao'                => 'D_E_L_E_T_',
                'inativo'                => 'B1_MSBLQL',
                'peso'                   => 'B1_PESO',
                'locpad'                 => 'B1_LOCPAD',
                'filial'                 => 'B1_FILIAL',
                'tipo_entrada_saida'     => 'B1_TS',
                'disponivel_venda'       => '',
                'segunda_unidade_medida' => 'B1_SEGUM',
                'situacao_produto'       => 'B1_SITPROD', //CUSTOM 177 - 000870	
            ),
            'produtos_estoque'                  => array(
                'filial'                   => 'B2_FILIAL',
                'codigo'                   => 'B2_COD',
                'quantidade_disponivel'    => '', // protheus nao precisa
                'quantidade_atual'         => 'B2_QATU',
                'quantidade_pedidos_venda' => 'B2_QPEDVEN',
                'quantidade_empenhada'     => 'B2_QEMP',
                'quantidade_reservada'     => 'B2_RESERVA',
                'ano'                      => '', // protheus nao precisa
                'mes'                      => '', // protheus nao precisa
                'preco'                    => '', // protheus nao precisa
                'delecao'                  => 'D_E_L_E_T_'
            ),
            'opcionais_produtos'                => array(
                'codigo_produto' => '',
                'codigo'         => '',
                'descricao'      => ''
            ),
            'grupos_produtos'                   => array(
                'codigo'    => 'BM_GRUPO',
                'descricao' => 'BM_DESC',
                'delecao'   => 'D_E_L_E_T_'
            ),
            'clientes'                          => array(//SA1010
                'codigo'                 => 'A1_COD',
                'loja'                   => 'A1_LOJA',
                'filial'                 => 'A1_FILIAL',
                'nome'                   => 'A1_NOME',
                'cpf'                    => 'A1_CGC',
                'pessoa_contato'         => 'A1_CONTATO',
                'endereco'               => 'A1_END',
                'site'                   => 'A1_HPAGE',
                'bairro'                 => 'A1_BAIRRO',
                'cep'                    => 'A1_CEP',
                'cidade'                 => 'A1_MUN',
                'estado'                 => 'A1_EST',
                'codigo_municipio'       => 'A1_COD_MUN',
                'ddd'                    => 'A1_DDD',
                'telefone'               => 'A1_TEL',
                'email'                  => 'A1_EMAIL',
                'limite_credito'         => 'A1_LC',
                'pedidos_liberados'      => 'A1_SALPEDL',
                'total_titulos_aberto'   => 'A1_SALDUP',
                'codigo_representante'   => 'A1_VEND',
                'codigo_representante2'  => 'A1_VEND2', //CUSTOM 177 - 000870 - 14/10/2013
                'risco'                  => 'A1_RISCO', //CUSTOM 177 - 000870 - 28/10/2013
                'condicao_pagamento'     => 'A1_COND',
                'ultima_compra'          => 'A1_ULTCOM',
                'data_cadastro'          => 'A1_PRICOM',
                'data_primeira_compra'   => 'A1_PRICOM',
                'chave_primaria'         => 'R_E_C_N_O_',
                'tipo_pessoa'            => 'A1_PESSOA',
                'nome_fantasia'          => 'A1_NREDUZ',
                'rg'                     => 'A1_PFISICA',
                'inscricao_estadual'     => 'A1_INSCR',
                'inscricao_municipal'    => 'A1_INSCRM',
                'inscricao_rural'        => 'A1_INSCRUR',
                'valor_maior_atraso_dia' => 'A1_MATR',
                'media_atraso_dia'       => 'A1_METR',
                'valor_pago_atraso'      => 'A1_PAGATR',
                'desconto'               => 'A1_DESC',
                'numero'                 => '',
                'site'                   => 'A1_HPAGE',
                'observacao'             => 'A1_OBS',
                'tipo'                   => 'A1_TIPO',
                'tabela_preco'           => 'A1_TABELA',
                'transportadora'         => 'A1_TRANSP',
                'tipo_frete'             => 'A1_TPFRET',
                'situacao'               => 'A1_MSBLQL',
                'situacao_sintegra'      => 'A1_SINTHAB', //CUSTOM 177 - 000870 - 14/10/2013 - Sintegra	
                //'condicao_pagamento'		=> 'A1_CONDPAG',	//CUSTOM 177 - 000870 - 14/10/2013 - Condição Pagamento Padrão
                'aceita_chd'             => 'A1_ACCHD',
				'suframa'				=> 'A1_SUFRAMA', // CUSTOM E0180/2014 - 02/07/2014
				
				'cheques_devolvidos'	 => 'A1_CHQDEVO', //CHAMADO 001762
				'media_atraso'			 => 'A1_MATR',	  //CHAMADO 001762
                'delecao'                => 'D_E_L_E_T_',
				'chave'                => 'R_E_C_N_O_'
            ),
            'titulos'                           => array(
                'filial'                => 'E1_FILIAL',
                'codigo_pedido'         => 'E1_PEDIDO',
                'valor'                 => 'E1_VALOR',
                'prefixo'               => 'E1_PREFIXO',
                'data_emissao'          => 'E1_EMISSAO',
                'codigo_cliente'        => 'E1_CLIENTE',
                'loja_cliente'          => 'E1_LOJA',
                'data_vencimento'       => 'E1_VENCTO',
                'data_vencimento_real'  => 'E1_VENCREA',
                'data_baixa'            => 'E1_BAIXA',
                'codigo'                => 'E1_NUM',
                'codigo_representante'  => 'E1_VEND1',
                'codigo_representante2' => 'E1_VEND2',
                'parcela'               => 'E1_PARCELA',
                'saldo'                 => 'E1_SALDO',
                'serie'                 => 'E1_SERIE',
                'tipo'                  => 'E1_TIPO',
                'status'                => 'E1_STATUS',
                'delecao'               => 'D_E_L_E_T_',
                'R_E_C_D_E_L_'          => 'R_E_C_D_E_L_',
                'comissao'              => 'E1_COMIS1',
            ),
            'representantes'                    => array(
                'codigo'            => 'A3_COD',
                'filial'            => 'A3_FILIAL',
                'senha'             => 'A3_SENHA',
                'nome'              => 'A3_NOME',
                'cpf'               => 'A3_CGC',
                'rg'                => 'A3_INSCR',
                'endereco'          => 'A3_END',
                'bairro'            => 'A3_BAIRRO',
                'cep'               => 'A3_CEP',
                'cidade'            => 'A3_MUN',
                'estado'            => 'A3_EST',
                'telefone'          => 'A3_TEL',				
                'email'             => 'A3_EMAIL',
                'comissao'          => 'A3_COMIS',
                'grupo'             => 'A3_GRPREP',
                'codigo_supervisor' => 'A3_SUPER',
                'codigo_gestor'		=> 'A3_GEREN', // Projeto: E0177/2014  - Utilizado para obter o gestor responsável pelo representante.
				'delecao'           => 'D_E_L_E_T_'
            ),
            'notas_fiscais'                     => array(
                'codigo'                => 'F2_DOC',
                'data_emissao'          => 'F2_EMISSAO',
                'data_entrega'          => 'F2_DTENTR',
                'hora_emissao'          => 'F2_HORA',
                'valor_total'           => 'F2_VALFAT',
                'valor_icms'            => 'F2_VALICM',
                'valor_ipi'             => 'F2_VALIPI',
                'duplicata'             => 'F2_DUPL',
                'codigo_cliente'        => 'F2_CLIENTE',
                'loja_cliente'          => 'F2_LOJA',
                'codigo_transportadora' => 'F2_TRANSP',
                'codigo_representante'  => 'F2_VEND1',
                'codigo_representante2' => 'F2_VEND2', // CUSTOM: 177 / 000874  - 18/10/2013 - Localizar: CUST-VEND2 
                'base_calculo_icms'     => 'F2_BASEICM',
                'base_calculo_icms_st'  => 'F2_BASETST',
                'valor_icms_st'         => 'F2_VALTST',
                'valor_total_produto'   => 'F2_VALBRUT',
                'valor_frete'           => 'F2_FRETE',
                'valor_seguro'          => 'F2_SEGURO',
                'valor_desconto'        => 'F2_DESCONT',
                'despesas_necessarias'  => 'F2_DESPESA',
                'condicao_pagamento'    => 'F2_COND',
                'valor_mercadoria'      => 'F2_VALMERC',
                'tipo_frete'            => 'F2_TPFRETE',
                'filial'                => 'F2_FILIAL',
                'serie'                 => 'F2_SERIE',
                'delecao'               => 'D_E_L_E_T_'
            ),
            'itens_notas_fiscais'               => array(
                'codigo_nota_fiscal' => 'D2_DOC',
                'codigo_pedido'      => 'D2_PEDIDO',
                'serie'              => 'D2_SERIE',
                'codigo_produto'     => 'D2_COD',
                'codigo_cliente'     => 'D2_CLIENTE',
                'loja_cliente'       => 'D2_LOJA',
                'preco_unitario'     => 'D2_PRCVEN',
                'unidade_medida'     => 'D2_UM',
                'quantidade_entrega' => 'D2_QUANT',
                'total_faturado'     => 'D2_TOTAL',
                'filial'             => 'D2_FILIAL',
                'ipi'                => 'D2_VALIPI',
                'aliq_ipi'           => 'D2_IPI',
                'base_calc_icms'     => 'D2_BASEICM',
                'aliq_icms'          => 'D2_PICM',
                'valor_icms'         => 'D2_VALICM',
                'tes'                => 'D2_TES',
                'cf'                 => 'D2_CF',
                'st'                 => 'D2_ICMSRET',
                'item'               => 'D2_ITEMPV',
                'numero_item_nota'   => 'D2_ITEM',
                'delecao'            => 'D_E_L_E_T_'
            ),
            'tipo_entrada_saida'                => array(
                'codigo'                 => 'F4_CODIGO',
				'filial'                 => 'F4_FILIAL',
                'cf'                     => 'F4_CF',
                'base_icm'               => 'F4_BASEICM',
                'base_st'                => 'F4_BSICMST',
                'tipo_entrada_saida_ipi' => 'F4_IPI',
                'delecao'                => 'D_E_L_E_T_'
            ),
            // CUSTOM: 177 / 000874  - 17/10/2013 - Localizar: CUST-PORTO-DESTINO
            'frete_carreteiro'                  => array(
                'porto'     => 'DUS_PORTO',
                'filial'    => 'DUS_FILIAL',
                'tab_frete' => 'DUS_TABCAR',
                'delecao'   => 'D_E_L_E_T_',
            ),
            'portos_aeroportos'                 => array(
                'codigo'        => 'Y9_COD',
                'descricao'     => 'Y9_DESCR',
                'filial'        => 'Y9_FILIAL',
                'tempo_entrega' => 'Y9_LT_TRA',
                'delecao'       => 'D_E_L_E_T_',
            ),
            'frete_carreteiro_por_rota'         => array(
                'tab_frete' => 'DTS_TABCAR',
                'filial'    => 'DTS_FILIAL',
                'delecao'   => 'D_E_L_E_T_',
            ),
            'view_estatisticas_cliente'         => array(
				'codigo_cliente'             => 'CODCLI',
				'codigo_loja'                => 'LOJA',
              //  'razao_social'               => 'NOME',
              //  'nome_reduzido'              => 'NREDUZ',
                'numero_cheques_devolvidos'  => 'TOTAB',
              //  'ultimo_pedido'              => 'C5_NUM',
                'total_pedidos_vencidos'     => 'PEDVENC',
                'titulo_vencido_mais_antigo' => 'TITANT',
            ),
            // FIM CUSTOM: 177 / 000874 - 17/10/2013
            //CUSTOM-PRAZO DE ENTREGA
            'prazos_entrega'                    => array(
                'filial'              => 'Z3_FILIAL',
                'codigo_rota'         => 'Z3_ROTA',
                'tempo_administracao' => 'Z3_TIMEFAT',
                'tempo_entrega'       => 'Z3_TIMEENT',
                'delecao'             => 'D_E_L_E_T_'
            ),
            //CUSTOM-ROTAS
            'clientes_setor'                    => array(
                'filial'         => 'DA7_FILIAL',
                'codigo_setor'   => 'DA7_ROTA',
                'codigo_zona'    => 'DA7_PERCUR',
                'codigo_cliente' => 'DA7_CLIENT',
                'loja_cliente'   => 'DA7_LOJA',
                'cep_inicial'    => 'DA7_CEPDE',
                'cep_final'      => 'DA7_CEPATE',
                'delecao'        => 'D_E_L_E_T_'
            ),
            'zonas_setor'                       => array(
                'filial'       => 'DA9_FILIAL',
                'codigo_zona'  => 'DA9_PERCUR',
                'codigo_setor' => 'DA9_ROTA',
                'codigo_rota'  => 'DA9_ROTEIR',
                'delecao'      => 'D_E_L_E_T_'
            ),
            'rotas'                             => array(
                'filial'         => 'DA8_FILIAL',
                'codigo_rota'    => 'DA8_COD',
                'descricao_rota' => 'DA8_DESC',
                'delecao'        => 'D_E_L_E_T_'
            ),
            //CUSTOM-VALIDACAO DE CEPS
            'ceps'                              => array(
                'filial'     => 'JC2_FILIAL',
                'cep'        => 'JC2_CEP',
                'logradouro' => 'JC2_LOGRAD',
                'bairro'     => 'JC2_BAIRRO',
                'cidade'     => 'JC2_CIDADE',
                'estado'     => 'JC2_ESTADO',
                'delecao'    => 'D_E_L_E_T_',
            ),
            //custom prospects na empresa
            'prospects_processados'             => array(
                'status'               => 'US_STATUS',
                'filial'               => "US_FILIAL",
                'codigo'               => "US_COD",
                'codigo_loja'          => "US_LOJA",
                'tipo'                 => "US_TIPO",
                'id_ramo'              => "US_RAMO",
                'nome'                 => "US_NOME",
                'nome_fantasia'        => "US_NREDUZ",
                'cgc'                  => "US_CGC",
                'cep'                  => "US_CEP",
                'endereco'             => "US_END",
                'bairro'               => "US_BAIRRO",
                'estado'               => "US_EST",
                'nome_municipio'       => "US_MUN",
                'inscricao_estadual'   => "US_INSCR",
                'site'                 => "US_URL",
                'ddd'                  => "US_DDD",
                'telefone'             => "US_TEL",
                'fax'                  => "US_FAX",
                'email'                => "US_EMAIL",
                'email_fiscal'         => "US_MAILFIS",
                'faturamento'          => "US_FATANU",
                'faturamento_anual'    => "US_FATVAL",
                'ano_fat_atual'        => "US_FATANO",
                'sugestao_lim_credito' => "US_SUGLIM",
                'banco_cobranca'       => "US_BANCO1",
                'banco'                => "US_RBANCO",
                'agencia'              => "US_RAGEN",
                'conta_corrente'       => "US_RCONTA",
                'fone_banco'           => "US_RFONEBC",
                'referencia_1'         => "US_RFORN1",
                'fone_referencia_1'    => "US_RFONE1",
                'referencia_2'         => "US_RFORN2",
                'fone_referencia_2'    => "US_RFONE2",
                'funcionarios'         => "US_QTFUNC",
                'area_loja'            => "US_AREAL",
                'numero_lojas'         => "US_NUMLOJA",
                'numero_funcionarios'  => "US_NUMFUNC",
                'numero_veiculos'      => "US_NUMVEIC",
                'imovel_proprio'       => "US_IMOVPRO",
                'socio_1'              => "US_SOCIO1",
                'cpf_socio_1'          => "US_CPF1",
                'socio_2'              => "US_SOCIO2",
                'cpf_socio_2'          => "US_CPF2",
                'responsavel_compra'   => "US_COMPRAD",
                'resp_contas_pagar'    => "US_CTPAGAR",
                'condicoes_pagamento'  => "US_COND",
                'observacao'           => "US_OBS",
                //'descricao'            => "US_DESCCLI",
                'codigo_representante' 	=> "US_VEND",
				//Dados utilizados nos status
				'motivo_rejeicao'  	   	=> "US_OBSREJE",
				'data_rejeicao'			=> "US_DTREJ",
				'data_conversao'		=> "US_DTCONV",
				'codigo_cliente'		=> "US_CODCLI",	
				'suframa'				=> "US_SUFRAMA", // CUSTOM E180/2014  03/07/2014
				'codigo_preposto'		=> "US_PREPOST",
				//
				'delecao'              => "D_E_L_E_T_",
            ),
			// 	CONSULTA DE COMISSÕES POR REPRESENTANTE - 
			//  CUSTOM E0179/2014 - 03/07/2014
			'comissoes_representantes'             => array(
				'data_emissao'				=> 'DATA_EMISSAO',
				'situacao'					=> 'SITUACA',
				'codigo_representante'  	=> 'CODIGO_REP',
				'codigo_cliente'			=> 'CODIGO_CIENTE',
				'loja_cliente'  			=> 'LOJA_CLIENTE',
				'nome_fantasia_cliente' 	=> 'NOME',
				'numero_titulo' 			=> 'TITULO',
				'numero_pedido' 			=> 'PEDIDO',
				'filial_pedido' 			=> 'FILIAL',
				'data_vencimento_comissao' 	=> 'VENCIMENTO',
				'valor_base_pedido' 		=> 'VLRBASE',
				'percentual_comissao' 		=> 'PERCENTUAL',
				'valor_comissao' 			=> 'COMISSAO',			
				'municipio'					=> 'MUNICIPIO',
				
			),
			// 	FIM CUSTOM E0179/2014 - 03/07/2014
			// 	CONSULTA DE RECEBIMENTOS POR REPRESENTANTE - 
			//  CUSTOM E0178/2014 - 03/07/2014
			'recebimentos_representantes'             => array(
				'codigo_representante'  => 'CODIGO',
				'data_periodo'			=> 'MESANO',
				'valor' 				=> 'VALTOT',
				'saldo'					=> 'SALTOT',		
			),
			// 	FIM CUSTOM E0179/2014 - 03/07/2014			
			//  CUSTOM E0174/2014 - 04/07/2014
			'pedidos_por_rota'             => array(
				'filial'  				=> 'C9_FILIAL',
				'codigo'				=> 'DA8_COD',
				'descricao' 			=> 'DA8_DESC',
				'codigo_pedido'			=> 'C9_PEDIDO',
				'data_emissao'			=> 'C5_EMISSAO',				
				'total_kg'				=> 'TOTAL_KG',				
				'total_fd'				=> 'TOTAL_FD',				
				'codigo_cliente'		=> 'C9_CLIENTE',
				'loja_cliente'			=> 'C9_LOJA',				
				'codigo_representante'	=> 'C5_VEND1',				
				'situacao_credito'		=> 'CREDITO',				
				'situacao_estoque'		=> 'ESTOQUE',
				'data_previsao_entrega'	=> 'DT_SUG_ENTREGA',				
			),
			// 	FIM CUSTOM E0174/2014 - 04/07/2014		
			//  CUSTOM E0174/2014 - 11/07/2014
			'categorias_produtos' => array(
				'codigo_categoria'  => 'ACV_CATEGO',
				'codigo_produto'	=> 'ACV_CODPRO',
				'delecao' 			=> 'D_E_L_E_T_',								
			),
			'desconto_por_fardo' => array(
				'codigo_filial'  		=> 'RZA_FILIAL',
				'codigo_tabela_precos'	=> 'RZA_TABELA',
				'codigo_categoria'		=> 'RZA_CATEG',
				'quantidade'			=> 'RZA_QTDFD',
				'desconto_maximo_reais' => 'RZA_VARPRC',
				'delecao' 			=> 'D_E_L_E_T_',								
			),
			// 	FIM CUSTOM E0174/2014 - 11/07/2014
			
			//002514 - [CUSTOM] Melhoria do DW Força de Vendas
			'pedidos_liberados' => array(
				'codigo_filial'  	=> 'C9_FILIAL',
				'codigo_pedido'		=> 'C9_PEDIDO',
				'codigo_produto'		=> 'C9_PRODUTO',
				'situacao'  		=> 'C9_BLCRED',
				'delecao' 			=> 'D_E_L_E_T_',								
			),
			
			
		)
    );


    return $config['db_cliente'];
}
