<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 | -------------------------------------------------------------------
 | DATABASE CONNECTIVITY SETTINGS
 | -------------------------------------------------------------------
 | This file will contain the settings needed to access your database.
 |
 | For complete instructions please consult the "Database Connection"
 | page of the User Guide.
 |
 | -------------------------------------------------------------------
 | EXPLANATION OF VARIABLES
 | -------------------------------------------------------------------
 |
 |	['hostname'] The hostname of your database server.
 |	['username'] The username used to connect to the database
 |	['password'] The password used to connect to the database
 |	['database'] The name of the database you want to connect to
 |	['dbdriver'] The database type. ie: mysql.  Currently supported:
 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
 |	['dbprefix'] You can add an optional prefix, which will be added
 |				 to the table name when using the  Active Record class
 |	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
 |	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
 |	['cache_on'] TRUE/FALSE - Enables/disables query caching
 |	['cachedir'] The path to the folder where cache files should be stored
 |	['char_set'] The character set used in communicating with the database
 |	['dbcollat'] The character collation used in communicating with the database
 |
 | The $active_group variable lets you choose which connection group to
 | make active.  By default there is only one group (the "default" group).
 |
 | The $active_record variables lets you determine whether or not to load
 | the active record class
 */
$active_group = "default";
$active_record = TRUE;
$db['default']['hostname'] = "localhost";
$db['default']['username'] = "root";
$db['default']['password'] = "urb4n0s";
$db['default']['dbdriver'] = "mysql";
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

//HOMOLOGACAO
/*
$db['default']['database'] = "dw_homologacao";
$db['db_cliente']['hostname'] = "192.168.1.207/homol";
$db['db_cliente']['password'] = "forca_venda";
*/

//PRODUCAO

$db['default']['database'] = "dw_producao";
$db['db_cliente']['hostname'] = "racscan.urbano.corp/urbano.urbano.corp";
$db['db_cliente']['password'] = "forca";


$db['db_cliente']['username'] = "forca_venda";
$db['db_cliente']['dbdriver'] = "oci8";
$db['db_cliente']['dbprefix'] = "";
$db['db_cliente']['pconnect'] = FALSE;
$db['db_cliente']['db_debug'] = FALSE;  // Anterior TRUE
$db['db_cliente']['cache_on'] = FALSE;
$db['db_cliente']['cachedir'] = "";
$db['db_cliente']['char_set'] = "utf8";
$db['db_cliente']['dbcollat'] = "utf8_general_ci";
$db['db_cliente']['swap_pre'] = '';
$db['db_cliente']['autoinit'] = TRUE;  // Anterior TRUE
$db['db_cliente']['stricton'] = FALSE;
$db['db_cliente']['erp'] = "protheus";
/* End of file database.php */
/* Location: ./system/application/config/database.php */