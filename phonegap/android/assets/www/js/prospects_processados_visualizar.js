var prospect;

$(document).ready(function() {
	
	//$('.caixa ul li a').button();
	
	db.transaction(function(x) {
		var id = parse_url(location.href).fragment;
		console.log("Id: "+ id);
		x.executeSql(
			'SELECT prospects_processados.*, ramos.descricao AS nome_ramo FROM prospects_processados LEFT JOIN ramos ON ramos.chave  = prospects_processados.id_ramo WHERE codigo = ? ', [id], function(x, dados) {
				console.log('SELECT prospects_processados.*, ramos.descricao AS nome_ramo FROM prospects_processados LEFT JOIN ramos ON ramos.chave  = prospects_processados.id_ramo WHERE codigo = ? ');
				
				if (dados.rows.length)
				{
					prospect = dados.rows.item(0);
					
					if(prospect.status == '5')
					{
						$('#solicitar_reanalise').live('click', function(){

							apprise('Você deseja solicitar reanálise?', {
								'verify': true,
								'textYes': 'Sim',
								'textNo': 'Não'
							}, function (dado) {
								if (dado)
								{
									
									
									editar_pedido_em_reanalise(prospect, function(){
										
										window.location = 'prospects_editar.html#' + prospect.codigo;
									});
									
									//
								}
							});
						
						});
						
						$('#solicitar_reanalise').show();
						$('#exibir_motivo_rejeicao').show();
					}
					
					$('td[id]').each(function() {
						
						var html = (prospect[$(this).attr('id')] ? prospect[$(this).attr('id')] : 'N/A');
					
						if ($(this).attr('id') == 'tipo')
						{
							html = obter_tipo_cliente(html);
						}
						
						
						// CUSTOM: 177 / 000874 - 16/10/2013 - Localizar: CUST-RAMOS
						if ($(this).attr('id') == 'ramo')
						{
							if(prospect.id_ramo && prospect.id_ramo!= 0)
							{
								html = prospect.id_ramo + ' - ' + prospect.nome_ramo;
							}
							else
							{
								html = 'N/A';
							}
						}
						// FIM CUSTOM: 177 / 000874 - 16/10/2013
						
						
						// Verificação para Prospects Aguardando Sincronização
						if($(this).attr('id') == 'telefone')
						{
							var telefone 	= prospect.telefone;
							var ddd 		= prospect.ddd;
						
							if(telefone.length >= 8)
							{
								html = formatar_telefone(ddd+telefone);								
							}
							else
							{
								html = 'N/A';
							}
						}
						
						// Verificação para Prospects Aguardando Sincronização
						if($(this).attr('id') == 'fax')
						{
							var fax = prospect.fax;
							var ddd = prospect.ddd;
						
							if(fax.length >= 8)
							{
								html = formatar_telefone(ddd+fax);
							}
							else
							{
								html = 'N/A';
							}
						}
						
						if($(this).attr('id') == 'fone_referencia_1'){
							var fone_referencia_1 = prospect.fone_referencia_1;
							var ddd = prospect.ddd_referencia_1;
						
							if(fone_referencia_1.length >= 8)
							{
								html = formatar_telefone(ddd+fone_referencia_1);
							}
							else
							{
								html = 'N/A';
							}
						}
					
						
						if($(this).attr('id') == 'fone_referencia_2'){
							var fone_referencia_2 = prospect.fone_referencia_2;
							var ddd = prospect.ddd_referencia_2;
						
							if(fone_referencia_2.length >= 8)
							{
								html = formatar_telefone(ddd+fone_referencia_2);
							}
							else
							{
								html = 'N/A';
							}
						}
						
						//--------------
						
						if($(this).attr('id') == 'data_nascimento')
						{
							if(prospect.data_nascimento)
							{
								html = protheus_data2data_normal(prospect.data_nascimento);
							}
							else
							{
								html = 'N/A';
							}
						}
						
						if($(this).attr('id') == 'data_rejeicao')
						{
							if(prospect.data_rejeicao)
							{
								html = protheus_data2data_normal(prospect.data_rejeicao);
							}
							else
							{
								html = 'N/A';
							}
						}
						
						if($(this).attr('id') == 'data_conversao')
						{
							if(prospect.data_conversao)
							{
								html = protheus_data2data_normal(prospect.data_conversao);
							}
							else
							{
								html = 'N/A';
							}
						}
						
						if($(this).attr('id') == 'condicoes_pagamento')
						{
							if(prospect.condicoes_pagamento && prospect.condicoes_pagamento != '0')
							{
								obter_descricao_condicao(prospect.condicoes_pagamento);
							}
							else
							{
								html = 'N/A';
							}
						}
						
						
						if($(this).attr('id') == 'codigo_municipio')
						{
							if(prospect.nome_municipio)
							{
								html = prospect.nome_municipio;
							}
							else
							{
								html = 'N/A';
							}
						}
						
						$(this).html(html);

					});
					
					$('.nome').html(prospect.nome);
					$('.cgc').html(formatar_cpfCNPJ(prospect.cgc));
					
					
					$('input[name=pessoa_contato]').val(prospect.contato);
					$('input[name=email]').val(prospect.email);
					
					
					var cgc_prospect = prospect.cgc;
					
					
				}
				else
				{
					window.location = 'index.html';
				}
			}
		);
	});
	
	// ver histórico
	
	
	
	
});

/*
//temporaria

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
//fim temporaria
*/




//CUSTOM 20140610 - Reanálise dos prospects rejeitados
function alterar_status_para_solicitada_reanalise (prospect, callback){
	status_solicitada_reanalise = 9;
	
	db.transaction(function(x) {
		var query = 'UPDATE prospects_processados SET status = ? WHERE codigo = ? AND codigo_loja = ? '; 
		x.executeSql(query, [status_solicitada_reanalise,prospect.codigo, prospect.codigo_loja], function (tx, result) {
            //alert("Query Success");
            callback();            
        },function (tx, error) {
           alert("Desculpe, ocorreu um erro  por favor entre em contato.");
           alert(error.message);
           
        });
		
			
	});

}



$("#exibir_motivo_rejeicao").live('click',function(){
	
	
	exibir_motivo_rejeicao(prospect.codigo);
	
});


function exibir_motivo_rejeicao(id){
	db.transaction(function(x) {
	x.executeSql(
			'SELECT prospects_processados.motivo_rejeicao  FROM prospects_processados  WHERE codigo = ? ', [id], function(x, dados) {
				if (dados.rows.length)
				{
					dado = dados.rows.item(0);
					
					
					mensagem( dado.motivo_rejeicao , '', 'Motivo da Rejeição');
					
				}
				
			}
		);
	});	
}


function editar_pedido_em_reanalise(prospect, callback)
{
	console.log('Solicitar Reanálise: ');
	
	console.log(' - - - Prospect: ');
	console.log(prospect);
	console.log(' - - - FIM Prospect: ');
	
	var tmp_1 = [];
	var tmp_2 = [];
	var tmp_3 = [];
	
	
	var verificar = [
		"status",
		"codigo",
		"codigo_loja",
		"tipo",
		"id_ramo",
		"nome",
		"nome_fantasia",
		"cgc",
		"cep",
		"endereco",
		"bairro",
		"estado",
		"nome_municipio",		
		"inscricao_estadual",
		"site",
		"ddd",
		"telefone",
		"fax",
		"email",
		"email_fiscal",
		"faturamento",
		"faturamento_anual",
		"ano_fat_atual",
		"sugestao_lim_credito",
		"banco_cobranca",
		"banco",
		"agencia",
		"conta_corrente",
		"fone_banco",
		"referencia_1",
		"fone_referencia_1",
		"referencia_2",
		"fone_referencia_2",
		"funcionarios",
		"area_loja",
		"numero_lojas",
		"numero_funcionarios",
		"numero_veiculos",
		"imovel_proprio",
		"socio_1",
		"cpf_socio_1",
		"socio_2",
		"cpf_socio_2",
		"responsavel_compra",
		"resp_contas_pagar",
		"condicoes_pagamento",
		"observacao",
		"codigo_representante",
		"empresa",
		"unidade",
		"suframa",
		"filial"
		
		
	];
			
	var sucesso = [];
	var falha = [];
	
	
	$.each( prospect, function(name, value){
		if(in_array(name,verificar )){
			sucesso.push(name);
			tmp_1.push(name);
			tmp_2.push('?');
			tmp_3.push(value);
		}else{
			falha.push(name);
		}
		
		
	});
	
	
	
	
	
	
	
	tmp_1.push('data_emissao');
	tmp_2.push('?');
	tmp_3.push(date('Ymd'));
	
	tmp_1.push('time_emissao');
	tmp_2.push('?');
	tmp_3.push(time());
	
	
	tmp_1.push('codigo_empresa');
	tmp_2.push('?');
	tmp_3.push(info.empresa);
	
	tmp_1.push('empresa');
	tmp_2.push('?');
	tmp_3.push(info.empresa);
	
	tmp_1.push('latitude');
	tmp_2.push('?');
	tmp_3.push(localStorage.getItem('gps_latitude'));
	
	tmp_1.push('longitude');
	tmp_2.push('?');
	tmp_3.push(localStorage.getItem('gps_longitude'));
	
	tmp_1.push('versao');
	tmp_2.push('?');
	tmp_3.push(localStorage.getItem('versao'));
	
	//CUSTOM 20140610 - Reanálise dos prospects rejeitados
	tmp_1.push('acao');
	tmp_2.push('?');
	tmp_3.push('A');
	// FIM CUSTOM 20140610 t
	
	
	console.log('SQL Reajuste do prospect: ');
	
	console.log('INSERT INTO prospects (' + tmp_1.join(', ') + ') VALUES (' + tmp_3.join(', ') + ')');
	/*console.log(' - Dados: ');
	console.log(tmp_3);
	*/
	
	
	 
	obter_codigo_municipio(prospect.estado, prospect.nome_municipio, function(codigo_municipio){
		
		tmp_1.push('codigo_municipio');
		tmp_2.push('?');
		tmp_3.push(codigo_municipio);
		
		db.transaction(function(x) {	
			var query = 'INSERT INTO prospects (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')'; 
			x.executeSql(query, tmp_3, function (tx, result) {
	            //alert("Query Success");
	            alterar_status_para_solicitada_reanalise(prospect, function(){
	            	callback();
	            });
	            
	        },function (tx, error) {
	           alert("Desculpe, ocorreu um erro  por favor entre em contato.");
	           alert(error.message);
	           
	        });
			
				
		}
		
		
		);
		//x.executeSql('INSERT INTO prospects (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')', tmp_3);
	});
	
}



function obter_codigo_municipio(UF, nome_municipio, callback){
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM municipios  WHERE uf = ? AND nome = ?', [UF,nome_municipio],function(x, dados){
			var municipio = dados.rows.item(0);
			
			
			callback(municipio.codigo);
			
		});
	});	
	
}


// FIM CUSTOM 20140610




function obter_descricao_condicao(condicao)
{
	if(condicao)
	{
		db.transaction(function(x) {
			x.executeSql('SELECT codigo, descricao FROM condicoes_pagamento WHERE codigo = ?', [condicao], function(x, dados){
				var dado = dados.rows.item(0);
				
				$('#condicoes_pagamento').html(dado.codigo + ' - ' + dado.descricao);
			})
		});
	}
}

function obter_prospect_orcamento(codigo_proscpet)
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM prospects_precessados WHERE codigo = ?', [codigo_proscpet], 
			function(x, dados) 
			{				
				var dado = dados.rows.item(0);
				
				//Cabeçalho pedido
				var pedido_copiado = [];
							
				pedido_copiado['filial'] 						= dado.filial;
				pedido_copiado['tipo_pedido'] 					= '*';
				pedido_copiado['cliente_prospect'] 				= 'P';
				pedido_copiado['codigo_prospect'] 				= dado.codigo;
				pedido_copiado['loja_prospect']					= dado.codigo_loja;
				pedido_copiado['descricao_prospect']			= dado.codigo + '/' + dado.codigo_loja + ' - ' + dado.nome + ' - ' + dado.cgc;
				
				sessionStorage['sessao_orcamento'] = serialize(pedido_copiado);
				window.location = "pedidos_adicionar.html#O";
			
			}
		)
	});
}