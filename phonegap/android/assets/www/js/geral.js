var 	classNotificacoes 		= 'notificacoes';
var 	localizacao 			= {};
var 	info 					= {};

		info.id_rep 						= localStorage.getItem('id');
		info.cod_rep 						= localStorage.getItem('codigo');
		info.usuario 						= localStorage.getItem('usuario');
		info.versao 						= config.versao;
		info.key 							= localStorage.getItem('key');
		info.nome_representante 			= localStorage.getItem('nome_real');
		info.nome_curto_representante 		= localStorage.getItem('nome');
		info.empresa 						= localStorage.getItem('empresa');
		info.empresa_representante 			= localStorage.getItem('empresa_representante');
		info.codigo_gestor		 			= localStorage.getItem('codigo_gestor');



var ufs;

$.each({ AC: 'Acre', AL: 'Alagoas', AP: 'Amapá', AM: 'Amazonas', BA: 'Bahia', CE: 'Ceará', DF: 'Distrito Federal', ES: 'Espírito Santo', GO: 'Goiás', MA: 'Maranhão', MT: 'Mato Grosso', MS: 'Mato Grosso do Sul', MG: 'Minas Gerais', PA: 'Pará', PB: 'Paraíba', PR: 'Paraná', PE: 'Pernambuco', PI: 'Piauí', RJ: 'Rio de Janeiro', RN: 'Rio Grande do Norte', RS: 'Rio Grande do Sul', RO: 'Rondônia', RR: 'Roraima', SC: 'Santa Catarina', SP: 'São Paulo', SE: 'Sergipe', TO: 'Tocantins' }, function(i, v) {
	ufs += '<option value="' + i + '">' + v + '</option>';
});

//CUSTOM: 177 / 000874 - 22/10/2013 - Localizar: CUST-BANCO
// Obter bancos
	bancos_cobranca = '<option value="10">BANCO DO BRASIL</option>';
	bancos_cobranca += '<option value="11">BRADESCO</option>';
	bancos_cobranca += '<option value="399">HSBC</option>';
	bancos_cobranca += '<option value="84">CARTEIRA</option>';
	bancos_cobranca += '<option value="85">ESPECIE</option>';
//FIM CUSTOM: 177 / 000874 - 22/10/2013




//Função para verificar existe conexao com a internet
function verificar_status()
{
	//Verificar se existe notificações
	$('.notificacoes').attr('onclick', 'javascript: abrirNotificacoes();').addClass(classNotificacoes);
	
	$('#status_offline_2').hide();
	
	if(window.navigator.onLine)
	{
		$('#status_online').show();
		$('#status_offline').hide();
		
		if(count(info.nome_representante) > 0)
		{
			salvar_localizacao();
		}
	}
	else
	{
		$('#status_online').hide();
		$('#status_offline').show();
	}
}

$(document).ready(function() {
	
	//-----------------------------------------------------
	//
	//-----------------------------------------------------
	document.addEventListener("deviceready", function() {
		if (typeof plugins !== "undefined") 
		{
			// To SHOW a modal waiting dialog
			plugins.waitingDialog.fecharDialog();
		}	
	}, false);
	
	$(window).unload(function() {
		if (typeof plugins !== "undefined") 
		{
			// To SHOW a modal waiting dialog
			plugins.waitingDialog.mostrarDialog("Aguarde carregando...");
		}
	});		
	//-----------------------------------------------------
	//
	//-----------------------------------------------------
	
	
	//-----------------------------------------------------
	// Verificar Atualização {
	//-----------------------------------------------------
	
	// Botão Atualizar Versão
	$('#btn_atualizar').live('click', function(){
	
		confirmar('Você tem certeza que deseja atualizar a versão?', 
		function () {
			$(this).dialog('close');
			
			//------------------
			//---------------------------------
	
			location.href = "atualizacao.html";
			
			//---------------------------------
			//------------------
			
			$("#confirmar_dialog").remove();
		});
	});
	
	// Botão Não Atualizar Versão
	$('#btn_naoAtualizar').live('click', function(){
	
		var html = '<h2>Lembrar mais tarde</h2>';
				html += '<table cellspacing="0">';
				html += '<tr>';
				html += '	<td><b>Quando você deseja ser lembrado da atualização?</b></td>';
				html += '</tr>';
				html += '<tr>';
				html += '	<td>';
				html += '		<form><select style="width:300px;" id="data_atualizacao_dwfdv" name="data_atualizacao_dwfdv">';
				html += '			<option value="1" selected>Daqui 1 hora</option>';
				html += '			<option value="2">Daqui 2 horas</option>';
				html += '			<option value="4">Daqui 4 horas</option>';
				html += '		</select></form>';
				html += '	</td>';
				html += '</tr>';
				html += '</table>';
				
		confirmar(html, 
			function () {
				$(this).dialog('close');
				
				//------------------
				//---------------------------------
				
				var data_atualizacao = time() + (60 * 60 * + parseInt($('#data_atualizacao_dwfdv').val()));
		
				localStorage.setItem('data_atualizacao_dwfdv', data_atualizacao);
				
				$('#atualizacao_dwfdv').hide();
				
				//---------------------------------
				//------------------
				
				$("#confirmar_dialog").remove();
		});
		
	});
	
	
	// Verificar Versão
	function verificar_atualizacao()
	{
		
		//Atualização dos dados do representante
		
		$.ajax({
			url: config.ws_url + 'autenticacao/autenticar',
			headers : {
				"DW_KEY_APP" : localStorage.getItem('key')
			},     
			data: {
				key: localStorage.getItem('key')
			},
			dataType: 'json',
			type: 'GET',
			success: function(dados) {
				if (!dados['error'])
				{
				
					db.transaction(function(x) {
						var campos 			= [];
						var interrogacoes  	= [];
						var valores		 	= [];
					
						$.each(dados, function(indice, valor) {
							campos.push(indice);
							interrogacoes.push(indice + ' = ? ');
							valores.push(valor);
							
							if(indice == 'empresa')
							{
								indice = 'empresa_representante';
							}
							
							// Gravando dados do representante na sessao
							localStorage.setItem(indice, valor);
							
						});
						
						
					
						
					
						x.executeSql('UPDATE informacoes_do_representante  SET ' + interrogacoes.join(', ') + ' ', valores);
						
						
					
					});
					
				}
				else
				{
					//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
				 //	mensagem('Seus dados de acesso foram alterados. Não será permitida a inclusão de pedidos.');
				}
				
				
				
			},
			error: function() {
				//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
				//mensagem(' 2 Não foi possível se conectar com o servidor, verifique sua conexão com a internet. Por favor tente novamente.');
			}
		});
		
		
		// fim Atualização dos dados do representante
		
		
		
		$.ajax({
			type: 	"GET",
			url: 	config.ws_url + 'atualizacao/ultima_versao',
			data: { 
				
					versao : config.versao,					
					id_usuario : info.id_rep,
					macAddress:  localStorage.getItem('macAddress')
					
				},
			success: function(data) {
				
				localStorage.setItem('dados_atualizacao', json_encode(data));
				
				if(data.situacao == '001')
				{
				
					var html = '<div id="atualizacao_dwfdv" class="info message"><div class="clear"></div>';
						html += '	<b>Atualização de Software</b> - O sistema encontrou uma nova versão.<br /><br />';
						html += '	<p>Versão em execução atualmente: <b>' + config.versao + '</b>.<br />';
						html += '	Versão disponível para atualização: <b>' + data.versao.versao + '</b>.</p>';
						html += '	<br />';
						if(data.forcar)
						{
							html += '	<p class="pergunta">Você deve atualizar a versão.</p><br /><br />';
							html += ' 	<p><button id="btn_atualizar" type="button" class="btn btn-large btn-success">ATUALIZAR</button>';
							html += ' 	</p>';
						}
						else
						{
							html += '	<p class="pergunta">Deseja atualizar a versão?</p><br /><br />';
							html += ' 	<p><button id="btn_atualizar" type="button" class="btn btn-large btn-success">SIM</button>';
							html += ' 	<button id="btn_naoAtualizar"type="button" class="btn btn-large btn-warning">AGORA NÃO</button>';
							html += ' 	</p>';
						}
						
						html += '</div>';
					
					//Montar no conteúdo
					$('#conteudo').before(html);
								
				}
				
			}
		});
	}
	
	if($('.atualizarAPK').length <= 0)
	{	
		// Se o storage data_atualizacao_dwfdv for igual a vazio, adicionar 0
		if(!localStorage.getItem('data_atualizacao_dwfdv'))
		{
			localStorage.setItem('data_atualizacao_dwfdv', 0);
		}
		
		// Se a data for maior que a data de atualização, exibir mensagem de atualizar
		if(localStorage.getItem('data_atualizacao_dwfdv') <= time())
		{
			verificar_atualizacao();
		}
	}
	
	$('.btnCadastro').live('click', function(e){
		
		var dados_atualizacao = json_decode(localStorage.getItem('dados_atualizacao'));
		
		if(dados_atualizacao.forcar && dados_atualizacao.situacao == '001')
		{
			e.preventDefault();
				

			var html = '	<b>Atualização de Software</b> - O sistema encontrou uma nova versão.<br /><br />';
			html += '	<p>Versão em execução atualmente: <b>' + config.versao + '</b>.<br />';
			html += '	Versão disponível para atualização: <b>' + dados_atualizacao.versao.versao + '</b>.</p>';
			html += '	<br />';
			html += '	<p class="pergunta">Deseja atualizar a versão?</p>';
			
			confirmar(html, 
			function () {
				$(this).dialog('close');
				
				//------------------
				//---------------------------------
		
				location.href = "atualizacao.html";
				
				//---------------------------------
				//------------------
				
				$("#confirmar_dialog").remove();
			});
		}
		
	});

	//-----------------------------------------------------
	// } Verificar Atualização
	//-----------------------------------------------------
	
	if(navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod')
	{
		$(".status_online").css("position", "static");
	};


	//----------------------------------
	//Barra
	var 	botao = '';
			botao += ' <div id="home-link"><a href="index.html"></a></div>';
			botao += ' <div id="voltar-link"><a class="notificacoes" href="#"></a></div>';
			
	if($('#autenticar').length > 0 || $('#sair').length > 0)
	{
		$('.barra').append('<div id="logo_home"><img src="img/logo_dwpdr.png" width="39" height="40"></div>');
	}
	else if($('.dashboard').length > 0)
	{
		$('.barra').append('<div id="logo_home"><img src="img/logo_dwpdr.png" width="39" height="40"></div><div id="voltar-link"><a class="notificacoes" href="#"></a></div>');
	}
	else
	{
		$('.barra').append('<div id="voltar"><a href="#" onclick="voltar_pagina(); return false;"><img src="img/btn_voltar.gif" width="86" height="46"></a></div>'+botao);
	}
	
	//----------------------------------
	
	//Btn Offline
	$('.btnOffline').live('click', function(e){
	
		if(!window.navigator.onLine)
		{
			e.preventDefault();
			mensagem('<strong style="color: #900;">Atenção!</strong><br><br>Para sincronizar você precisa estar conectado na internet. Você não está conectado na internet ou sua conexão está muito lenta.<br><br>Por favor verifique sua conexão com a internet e tente novamente.', function() {
				window.location = 'index.html';
			});
		}
		
	});
	
	
	//-----------------------------------------------------------


	$('.btn_ui').button();
	// VERIFICA SE O USUÁRIO SUPORTA GEOLOCALIZAÇÃO
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (position) {
			
				//Setar objetos
				localizacao.latitude 			= position.coords.latitude;
				localizacao.longitude 			= position.coords.longitude;
				localizacao.accuracy 			= position.coords.accuracy;
				localizacao.altitude 			= position.coords.altitude;
				localizacao.altitudeAccuracy 	= position.coords.altitudeAccuracy;
				localizacao.heading 			= position.coords.heading;
				localizacao.speed 				= position.coords.speed;
				localizacao.timestamp 			= position.coords.timestamp;
				
				console.log("Lat: " + position.coords.latitude);
				console.log("Long: " + position.coords.longitude);
				
				//Salvar no localStorage
				localStorage.setItem('gps_latitude', 			position.coords.latitude);
				localStorage.setItem('gps_longitude', 			position.coords.longitude);
				localStorage.setItem('gps_accuracy', 			position.coords.accuracy);
				localStorage.setItem('gps_altitude', 			position.coords.altitude);
				localStorage.setItem('gps_altitudeAccuracy', 	position.coords.altitudeAccuracy);
				localStorage.setItem('gps_heading', 			position.coords.heading);
				localStorage.setItem('gps_speed', 				position.coords.speed);
				localStorage.setItem('gps_timestamp', 			position.coords.timestamp);
			}, 
			function (error) {
				//localizacao = '';
				switch(error.code)  
				{  
					case error.PERMISSION_DENIED: /*alert("Usuário não compartilhou dados de geolocalização.")*/;  
					break;  
					case error.POSITION_UNAVAILABLE: /*alert("Não poderia detectar a posição atual.")*/;  
					break;  
					case error.TIMEOUT: /*alert("Tempo para recuperar posição terminou.")*/;  
					break;  
					default: /*alert("Error não identificado.")*/;  
					break;  
				}   
			}
		);
	}
	
	//-----
	if(count(info.nome_representante) == 0)
	{
		
		if ($('#autenticar').length <= 0)
		{
			window.location = 'autenticar.html';
		}
	}
	
	//Adicionar boas vindas a barra top
	$('#conteudo').prepend('<div class="status_online" class="nao_exibir_impressao"><div id="representante">Olá ' + info.nome_representante + '<span id="localizacaoteste"></span></div><div id="internet"><strong>Internet </strong><img id="status_online" style="display:none" src="img/ativo.png" width="10" weight="10" /><img id="status_offline" style="display:none" src="img/inativo.png" width="10" weight="10" /><img id="status_offline_2" src="img/offline.png" width="10" weight="10" /></div></div>');

	if(info.empresa_representante)
	{
		var where = " WHERE codigo = '" + info.empresa_representante + "'";
	}
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM empresas ' + where + ' ORDER BY codigo', [], function(x, dados) {
				if (dados.rows.length)
				{
					var options = "";
					for (i = 0; i < dados.rows.length; i++)
					{
						var empresa = dados.rows.item(i);
						empresa.codigo;
						empresa.decricao;
						
						//options += '<option value="' + empresa.codigo + '">' + empresa.descricao + '</option>';
						$('.barra').prepend('<div class="empresas"> <button class="btn btn-warning btn-large" type="button" onclick="abrirEmpresas()">' + empresa.descricao + '</button></div>');
						
						// Selecionar empresa caso não existir a sessao
						if(!localStorage.getItem('empresa'))
						{
							localStorage.setItem('empresa', empresa.codigo);
						}
					}
					//----------------
				}
			});
	});
	
	// ------------------------------------------------------------------------------------------------
	// Avisar usuário que o mesmo tem pendências em aberto
	// Verifica pendências
	// ------------------------------------------------------------------------------------------------
	db.transaction(function(x) {
		x.executeSql(
				'SELECT * FROM pendencias WHERE timestamp <= ? AND status = \'em_aberto\' ORDER BY timestamp DESC', [time()], function(x, dados) {
					if (dados.rows.length)
					{
						if(time() >= localStorage.getItem('lembrar_pendencias'))
						{
						
							//Tem Notificações
							classNotificacoes = 'notificacoes-ativo';
							//Adicionar HTML
							var html = '<div class="notificacao warning message"><div class="clear"></div>';
								html += '	<h3>Aviso</h3>';
								html += '	<p>Você tem '+dados.rows.length+' pendências em aberto. ';
								html += ' 	<a href="pendencias_listar.html">Ver Pendências</a> ou ';
								html += ' 	<a href="" id="lembrar_mais_tarde">Lembrar mais tarde?</a>';
								html += ' 	</p>';
								html += '</div>';
							$('#conteudo').before(html);							
						}						
					}
				});
	});

	//AO SELECIONAR UM HORÁRIO SALVAR NO BANCO
	$('#quando_mais_tarde').live('change', function(){
		if($(this).val() > 0){
			
			$('#dialog_lembrar').dialog('close');
			
			localStorage.setItem('lembrar_pendencias', (time() + $(this).val()*3600));
			$('.notificacao').hide();

			var timeStamp 	= (time() + $(this).val()*3600);			
			var date 		= new Date(timeStamp*1000);
			
			document.addEventListener("deviceready", function(){
				//Avisar que existe pendencias							
				if (typeof plugins !== "undefined") 
				{
                    plugins.localNotification.add({
                        date : date,
                        message : 'Você tem pendências em aberto.',
                        ticker : 'Você tem pendências em aberto.',
                        repeatDaily : false,
                        id : 4
                    });
                    console.log('Exibir alerta depois.');
				}else{
					console.log('Não carregou os plugins.');
				}
			}, false);
			
			document.location.reload();
		}		
	});
	
	// ABRIR LIGHTBOX COM OPÇÕES DE HORAS PARA LEMBRAR
	$('#lembrar_mais_tarde').live('click', function(e){
			e.preventDefault();
			
			var html = '<h2>Lembrar mais tarde</h2>';
				html += '<table cellspacing="0">';
				html += '<tr>';
				html += '	<td><b>Quando você deseja ser lembrado?</b></td>';
				html += '</tr>';
				html += '<tr>';
				html += '	<td>';
				html += '		<form><select style="width:300px;" id="quando_mais_tarde" name="quando_mais_tarde">';
				html += '			<option value="">Selecione uma opção</option>';
				html += '			<option value="1">Daqui 1 hora</option>';
				html += '			<option value="2">Daqui 2 horas</option>';
				html += '			<option value="4">Daqui 4 horas</option>';
				html += '		</select></form>';
				html += '	</td>';
				html += '</tr>';
				html += '</table>';
				
		exibirMensagem('dialog_lembrar', html, '', 'Atenção');
	});	
	// ------------------------------------------------------------------------------------------------
	// Avisar usuário que o mesmo tem pendências em aberto
	// Verifica pendências
	// ------------------------------------------------------------------------------------------------
	// verificar se a sincronização é muito antiga
	db.transaction(function(x) {
		x.executeSql(
				'SELECT * FROM sincronizacoes ORDER BY timestamp DESC LIMIT 1', [], function(x, dados) {
					//Se existir sincronizações então realizar alguma verificações
					if (dados.rows.length)
					{
						var sinc = dados.rows.item(0);
						
						//Se config.forca_sincronizar for igual a true então obriga o usuário a sincronizar
						if ((time() - sinc.timestamp) >= (config.sincronizar * 3600) && (config.forca_sincronizar == true))
						{
							//Se não existir input hidden na página então redireciona para pagina de sincronizar.
							if ($('#sincronizar_hidden').length <= 0)
							{
								window.location = 'sincronizar_incremental.html';
							}
							//Selecionar todos os modulos para sincronizar
							$("#btnSelecionarTodos").trigger('click');
							//Esconde o botão de selecionar
							$("#btnSelecionarTodos").hide();
							//Desabilita todos os checkbox
							$("input[name='modulos[]']").attr('disabled', 'disabled');
							//Esconder navegação
							$('#navegacao').hide();
							
							//Tem Notificações
							classNotificacoes = 'notificacoes-ativo';
							
							//Adicionar mensagem
							$('#conteudo').before('<div class="notificacao error message"><h3>Atenção</h3><p>Você está sem efetuar uma sincronização há mais de '+config.sincronizar+' horas.'+(config.forca_sincronizar ? '<b> A sincronização é obrigatória.</b>': '')+'</p></div>');							
						}
						
						//Se config.forca_sincronizar for igual a false apenas mostra mensagem
						if ((time() - sinc.timestamp) >= (config.sincronizar * 3600) && (config.forca_sincronizar == false))
						{
							//Selecionar todos os modulos
							$("#btnSelecionarTodos").trigger('click');
							
							//Tem Notificações
							classNotificacoes = 'notificacoes-ativo';
							
							//Adicionar mensagem
							$('#conteudo').before('<div class="notificacao error message"><h3>Atenção</h3><p>Você está sem efetuar uma sincronização há mais de '+config.sincronizar+' horas.'+(config.forca_sincronizar ? '<b> A sincronização é obrigatória.</b>': ' <a href="sincronizar_incremental.html">Você deseja sincronizar agora</a>?')+'</p></div>');							
						}
					}
					else //Caso ainda não exista sincronizações então executa a sincronização automaticamente.
					{
						$('#btnCancelar').remove();
						//Selecionar todos os modulos para sincronizar
						$("#btnSelecionarTodos").trigger('click');
						//Esconde o botão de selecionar
						$("#btnSelecionarTodos").hide();
						//Desabilita todos os checkbox
						$("input[name='modulos[]']").attr('disabled', 'disabled');
						//Esconder navegação
						$('#navegacao').hide();
						//Executar sincronização
						sinc_modulos_selecionados();
						
					}
				}
		);
	});
	
	// Fixando a KEY para todas as requisições AJAX.
	$.ajaxSetup({
		headers : {
			"DW_KEY_APP" : localStorage.getItem('key')
		},
		data: {
			
			id_usuario: 					localStorage.getItem('id'),
			codigo_representante: 			localStorage.getItem('codigo'),
			latitude: 						localStorage.getItem('gps_latitude'),
			longitude: 						localStorage.getItem('gps_longitude'),
			accuracy: 						localStorage.getItem('gps_accuracy'), 
			altitude: 						localStorage.getItem('gps_altitude'), 
			altitudeAccuracy: 				localStorage.getItem('gps_altitudeAccuracy'), 
			heading: 						localStorage.getItem('gps_heading'), 
			speed: 							localStorage.getItem('gps_speed'),
			timestamp: 						localStorage.getItem('gps_timestamp'),
			macAddress: 					localStorage.getItem('macAddress'),
			versao: 						config.versao,	
		},
		dataType: 'json',
		type: 'GET'
	});
	
	// máscaras
	$('input:text').setMask();
	$('input:button').button();
	$('input:submit').button();

	// -----------------------------------------------------------------------------------------
	// datepicker

	//Inserir tradução do datepicker
	$('head').append('<script src="js/ui/i18n/jquery.ui.datepicker-pt-BR.js"></script>');	
	
	//Configurar o datepicker
	var datepicker = $('.datepicker').datepicker({
		dateFormat: 'dd/mm/yy',
		onSelect: function(data) {
			datepicker.not(this).datepicker('option', $(this).hasClass('data_inicial') ? 'minDate' : 'maxDate', $.datepicker.parseDate($(this).data("datepicker").settings.dateFormat || $.datepicker._defaults.dateFormat, data, $(this).data("datepicker").settings));
		}
	}, $.datepicker.regional['pt-BR']);

	
	$('.datepicker_data_minima_hoje').datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: new Date()
	});
	
	// datepicker
	// -----------------------------------------------------------------------------------------
	
	$('#limpar_filtros').live('click', function(){
		$('.datepicker').each(function() {			
			$(this).datepicker('option', 'minDate', '');
			$(this).datepicker('option', 'maxDate', '');
		});
	});
	
	// misc
	$('.somente_numeros_e_virgula').live('keypress blur', function (evento) {
		var _this = this;

		if(evento.keyCode != 9 || evento.keyCode != 48)
		{
			setTimeout(function () {
				if ($(_this).val().match(/[^0-9,]/gi))
				{
					$(_this).val($(_this).val().replace(/[^0-9,]/gi, ''));
				}
			}, 1);
		}
	});
	
	$('.somente_numeros').live('keypress blur', function (evento) {
		var _this = this;
		
		if(evento.keyCode != 9 || evento.keyCode != 48)
		{	
			setTimeout(function () {
				if ($(_this).val().match(/[^0-9]/gi))
				{
					$(_this).val($(_this).val().replace(/[^0-9]/gi, ''));
				}
			}, 1);
		}
	});
	
	
	$('#ver_titulos').live('click', function() {
		var codigo 	= $(this).attr('data-codigo')+'';
		var serie	= $(this).attr('data-serie')+'';
		
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM titulos WHERE titulo_codigo = ? AND titulo_serie = ? ', [codigo, serie], function(x, dados) {
					console.log('SELECT * FROM titulos WHERE titulo_codigo = '+codigo+' AND titulo_prefixo = '+serie);
					var html = '<p>' + nl2br('<h2>Consultar Títulos</h2>') + '</p>';
						html += '<table class="novo_grid">';
						html += '<thead>';
						html += '	<tr>';
						html += '		<th>Código N.F.</th>';
						html += '		<th>Parcela</th>';
						html += '		<th>Status</th>';
						html += '		<th>Valor (R$)</th>';
						html += '		<th>Vencimento</th>';
						html += '		<th>Pagamento</th>';
						html += '	</tr>';
						html += '</thead>';
						html += '<tbody>';
						
						
						if(dados.rows.length)
						{
							//LOOP se existir mais de um titulo
							for( i=0 ; i < dados.rows.length; i++)
							{
								var titulo = dados.rows.item(i);
								
								//-------------------------------------------------------------
								//Status do Titulos
								var dias					= '';
								var status 					= '';
								var timestamp_vencimento 	= strtotime(strtotime_data2data_normal(titulo.titulo_data_vencimento));
								var timestamp_baixa		 	= strtotime(strtotime_data2data_normal(titulo.titulo_data_baixa));
								
								if(timestamp_baixa){
									dias = round((timestamp_baixa - timestamp_vencimento) / (3600 * 24));
									console.log(dias);
									if (timestamp_baixa > timestamp_vencimento){
										status = '<span style="color: #060;">Pago com atraso de '+dias+' dia(s)</span>';
									}else{
									    status = '<span style="color: #060;">Pago em dia</span>';
									}
									
								}else{
									dias = abs(round((time() - timestamp_vencimento) / (3600 * 24)));
									
									if (time() > timestamp_vencimento){
										status = '<span style="color: #900;">'+dias+' dia(s) vencido</span>';
									}else{
									    status = '<span style="color: #060;">'+dias+' dia(s) para vencer</span>';
									}					            
								}
								//Status do Titulos
								//-------------------------------------------------------------
								
								var parcela = titulo.titulo_parcela ? titulo.titulo_parcela : '1';
								
								html += '	<tr>';
								html += '		<td class="right">'+titulo.titulo_codigo+'/'+titulo.titulo_prefixo+'</td>';
								html += '		<td class="right">'+parcela+'</td>';
								html += '		<td class="center">'+status+'</td>';
								html += '		<td class="right">'+number_format(titulo.titulo_valor, 3, ',', '.')+'</td>';
								html += '		<td class="center">'+protheus_data2data_normal(titulo.titulo_data_vencimento)+'</td>';
								html += '		<td class="center">'+(titulo.titulo_data_baixa ? protheus_data2data_normal(titulo.titulo_data_baixa) : 'N/A')+'</td>';
								html += '	</tr>';
								
							}
							//LOOP se existir mais de um titulo
						}
						else
						{
							html += '	<tr>';
							html += '		<td>N/A</td>';	
							html += '		<td>N/A</td>';		
							html += '		<td>N/A</td>';		
							html += '		<td>N/A</td>';		
							html += '		<td>N/A</td>';		
							html += '		<td>N/A</td>';									
							html += '	</tr>';
						}
						
						html += '</tbody>';					
						html += '</table>';
					
					//$.colorbox({ html: html });
					exibirMensagem('dialog_titulos', html, '', 'Detalhes');
			
					alterarCabecalhoTabelaConteudo();	
				}
			);
		});
		
		alterarCabecalhoTabelaResolucao();
		return false;
	});
	
	//gravar valor da empresa na sessao
	$('#empresa_select').live('change', function(){
		localStorage.setItem('empresa', $(this).val());
		
		document.location.reload();
	});
	
	$("textarea[maxlength]").live('keypress', function(event){
	    var key = event.which;
	    
	    
	    //todas as teclas incluindo enter
	    if(key >= 33 || key == 13) {
	    	
	        var maxLength = $(this).attr("maxlength");
	        
	        
	        var length = this.value.length;
	        
	        
	        if(length >= maxLength) {
	            event.preventDefault();
	            $(this).blur();
	        }
	    }
	});
	
});

function alterarCabecalhoTabelaResolucao()
{
	// loop through the header group table headers
		$('thead th', '.novo_grid').each(function(num) {
		
			// set the text from the table headers to a variable
			var header = str_replace('▲', '', $(this).text());
				header = str_replace('▼', '', header);
			
			// loop through the tbody rows
			$('tbody tr', '.novo_grid').each(function() {
				
				if($('td', this).eq(num).html() == '')
				{
					$('td', this).eq(num).html('N/A');
				}
				
				$('td', this).eq(num).removeAttr('width');
				
				// prepend the header text to each column
				// I put it in a span tag so I could style it.
				$('td', this).eq(num).attr('data-content',trim(header)+': ');

			});             
		});
}



function alterarCabecalhoTabelaConteudo()
{
	// loop through the header group table headers
		$('.tabela').each(function() {
			
			var tabela = $(this);
			
			$('tr th', tabela).each(function(num_th) {
				
				
				var header = $(this).text();
				

				if($('td', tabela).eq(num_th).text() == '')
				{
					$('td', tabela).eq(num_th).text('N/A');
				}
				
				$('td', tabela).eq(num_th).attr('data-content',trim(header)+': ');
				
				//$(this).parent('tr').remove();
				//$(this).parent('tr').css('display', 'none');
				$(this).parent('tr').addClass('hideHeader');
			});
		});
}

function alterarCabecalhoListagem(id_tabela)
{
	var colspan = 0;

	$('thead th', id_tabela).each(function(num) {
		
		// set the text from the table headers to a variable
		var header = str_replace('▲', '', $(this).text());
			header = str_replace('▼', '', header);
			
		var class_th = $(this).attr('class');
		
		
		// loop through the tbody rows
		
		$('tbody tr', id_tabela).each(function() {
			
			if($(this).attr('class') == 'novo_grid_rodape')
			{
				if(class_th != 'opcoes')
				{
					if($('td', this).eq(num).attr('colspan') > 0)
					{
						colspan = parseInt($('td', this).eq(num).attr('colspan'));
					}
					
					if(num >= colspan)
					{
						$('td', this).eq(num - (colspan - 1)).attr('data-content',trim(header)+': ');
					}
				}
				
			
			}
			else
			{
				
				if($('td', this).eq(num).html() == '')
				{
					$('td', this).eq(num).text('N/A');
				}
				
				$('td', this).eq(num).removeAttr('width');
				
				// prepend the header text to each column
				// I put it in a span tag so I could style it.
				$('td', this).eq(num).attr('data-content',trim(header)+': ');
				
				
			}

		});             
	});
}


//Verifica se existe conexao com a internet a cada 5 segundos
var status_dwfdv = setInterval('verificar_status()', 1000);

/*
function mensagem(conteudo)
{
	// o timeout corrige um bug: por algum motivo sem o timeout a mensagem não é enviada em alguns casos
	setTimeout(function() { apprise(conteudo); }, 100);
}
*/

function scroll(elemento)
{
	$('html, body').animate({ scrollTop: $(elemento).offset().top }, 1000);
}

function sleep(segundos) {
	var time = new Date().getTime();

	for (var i = 0; i < 1e7; i++)
	{
		if ((new Date().getTime() - time) > segundos * 1000)
		{
			break;
		}
	}
}

function prc2sql(str)
{
	str = str_replace('.', '', str);
	str = str_replace(',', '.', str);

	return floatval(str);
}

function protheus_data2data_normal(data)
{
	ano = substr(data, 0, 4);
	mes = substr(data, 4, 2);
	dia = substr(data, 6, 2);

	return dia + '/' + mes + '/' + ano;
}

function protheus_data2data_mesano(data)
{
	ano = substr(data, 0, 4);
	mes = substr(data, 4, 2);
	

	return  mes + '/' + ano;
}



function formatar_datahora(datahora)
{
	
	if(!!datahora){
	
	ano = substr(datahora, 0, 4);
	mes = substr(datahora, 4, 2);
	dia = substr(datahora, 6, 2);
	
	
	hh = substr(datahora, 8, 2);
	mm = substr(datahora, 10, 2);
	ss = substr(datahora, 12, 2);	 

	return dia + '/' + mes + '/' + ano + ' '+hh+':'+mm+':'+ss;
	}
	
	return 'N/A';
}


//CUSTOM data para timestamp
function diferenca_entre_datas(data1,data2){
	if(!data1)
	{
		data1 = date('Ymd');
	}
	//Número de milissegundos de um dia 
	DAY = 1000 * 60 * 60  * 24

	/*Data 01*/
	ano1 = substr(data1, 0, 4);
	mes1 = substr(data1, 4, 2);
	dia1 = substr(data1, 6, 2);
	
	/*Data 02*/
	ano2 = substr(data2, 0, 4);
	mes2 = substr(data2, 4, 2);
	dia2 = substr(data2, 6, 2);
	
	d1 = new Date(mes1+'/'+dia1+'/'+ano1);
	d2 = new Date(mes2+'/'+dia2+'/'+ano2);

	dias_entre_datas = Math.round((d2.getTime() - d1.getTime()) / DAY)

	return dias_entre_datas; 
	
}

//FIM CUSTOM


function data_normal2protheus(data)
{
	
	var data_array = explode('/', data);
	
	ano = data_array[2];
	mes = data_array[1];
	dia = data_array[0];

	return ano + mes + dia;
}

function strtotime_data2data_normal(data)
{
	ano = substr(data, 0, 4);
	mes = substr(data, 4, 2);
	dia = substr(data, 6, 2);

	return ano + '-' + mes + '-' +dia;
}

function validar_cep(cep)
{
	var RegExp = /^\d+$/;
	if (RegExp.test(cep) == true && cep.length == 8) {
		return true;
	}
	else
	{
		return false;
	}
}

function validar_ddd(ddd)
{
	var RegExp = /^\d+$/;
	if (RegExp.test(ddd) == true && ddd.length == 2  && parseInt(ddd) > 0 ) {
		return true;
	}
	else
	{
		return false;
	}
}

function validar_telefone(telefone)
{
	var RegExp = /^\d+$/;
	if (RegExp.test(telefone) == true && (telefone.length == 8  || telefone.length == 9  )) {
		return true;
	}
	else
	{
		return false;
	}
}

//third_party

function validar_email(mail)
{
	var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
	if(typeof(mail) == "string")
	{
		if(er.test(mail))
		{ 
			return true; 
		}
	}else if(typeof(mail) == "object"){
		if(er.test(mail.value))
		{ 
			return true; 
		}
	}else{
		return false;
	}
}

function validar_cpf(cpf)
{
	cpf = cpf.replace(/[^0-9]/g, '');
	
	var numeros, digitos, soma, i, resultado, digitos_iguais;
	digitos_iguais = 1;	
	if (cpf.length != 11){		
		
		return false;
	}	
	for (i = 0; i < cpf.length - 1; i++)
		if (cpf.charAt(i) != cpf.charAt(i + 1))
		{
			digitos_iguais = 0;
			break;
		}
	if (!digitos_iguais)
	{
		numeros = cpf.substring(0,9);
		digitos = cpf.substring(9);
		soma = 0;
		for (i = 10; i > 1; i--)
			soma += numeros.charAt(10 - i) * i;
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;
		numeros = cpf.substring(0,10);
		soma = 0;
		for (i = 11; i > 1; i--)
			soma += numeros.charAt(11 - i) * i;
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			return false;
		return true;
	}
	else
		return false;
}

function validar_cnpj(cnpj)
{
	cnpj = cnpj.replace(/[^0-9]/g, '');

	var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
	digitos_iguais = 1;
	if (cnpj.length < 14 && cnpj.length < 15)
		return false;
	for (i = 0; i < cnpj.length - 1; i++)
		if (cnpj.charAt(i) != cnpj.charAt(i + 1))
		{
			digitos_iguais = 0;
			break;
		}
	if (!digitos_iguais)
	{
		tamanho = cnpj.length - 2
		numeros = cnpj.substring(0,tamanho);
		digitos = cnpj.substring(tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--)
		{
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;
		tamanho = tamanho + 1;
		numeros = cnpj.substring(0,tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--)
		{
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			return false;
		return true;
	}
	else
		return false;
}



function validar_cpf_cnpj(valor)
{
	if(validar_cpf(valor))
	{
		console.log('cpf valido');
		return true;
	}
	else if(validar_cnpj(valor))
	{
		console.log('cnpj valido');
		return true;
	}
	else
	{
		console.log('invalido');	
		return false
	}

}
/**
 * ----------------------------------------------------------------------------
 * 	LOCALIZAÇÃO DO APARELHO
 * ----------------------------------------------------------------------------
 */
function salvar_localizacao()
{
	db.transaction(function(x) {
		x.executeSql(
				'SELECT * FROM localizacoes ORDER BY timestamp DESC LIMIT 1', [], function(x, dados) {
					if (dados.rows.length)
					{
						var sinc = dados.rows.item(0);
					
						if (time() - sinc.timestamp >= 2 * 3600) //2 * 3600
						{
							enviar_localizacao();
						}
					}else{
						enviar_localizacao();
						console.log("INSERT INTO localizacoes (timestamp, latitude, longitude) VALUES ('"+time()+"', '"+localStorage.getItem('gps_latitude')+"', '"+localStorage.getItem('gps_longitude')+"'");
						x.executeSql('INSERT INTO localizacoes (timestamp, latitude, longitude) VALUES (?, ?, ?)', [time(), localStorage.getItem('gps_latitude'), localStorage.getItem('gps_longitude')]);
					}
				}
		);
	});
}
/**
 * 
 */
function enviar_localizacao()
{
	if(localizacao)
	{
		$.ajax({
			type: 	"GET",
			url: 	config.ws_url + 'autenticacao/localizacao',
			data: { latitude: localizacao.latitude, longitude: localizacao.longitude, accuracy: localizacao.accuracy, 
				altitude: localizacao.altitude, altitudeAccuracy: localizacao.altitudeAccuracy, heading: localizacao.heading, 		
				speed: localizacao.speed, timestamp: localizacao.timestamp, key: localStorage.getItem('key') },
			success: function(data) {
				db.transaction(function(x) {
					x.executeSql('INSERT INTO localizacoes (timestamp, latitude, longitude) VALUES (?, ?, ?)', [time(), localStorage.getItem('gps_latitude'), localStorage.getItem('gps_longitude')]);
				});
			}
		});
	}
}
/**
 * ----------------------------------------------------------------------------
 * 	LOCALIZAÇÃO DO APARELHO
 * ----------------------------------------------------------------------------
 */

/**
 * Metódo:		obter_campos_tabela
 * 
 * Descrição:	Função Utilizada para retornar todos os campos ta tabela em string (Exemplo: campo1 TEXT, campo2 TEXT, campo3 TEXT)
 * 
 * Data:			17/09/2013
 * Modificação:	17/09/2013
 * 
 * @access		public
 * @param		object(JSON) 		dados					- Dados em JSON retornados do WS
 * @param		Boolean 			multiplos_objetos		- Parametro para saber se os "dados" retornados são multiplos ou nao 
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_campos_tabela(dados, multiplos_objetos)
{
	var campos = [];
	var tipo_campo = ' TEXT';
	$.each(dados, function(key, item) {

		if(multiplos_objetos == true)
		{

			$.each(item, function(campo, valor) {
				
				if(campo == 'quantidade'){
					tipo_campo = ' REAL';
				}
					campos.push(campo + tipo_campo);
				
			});

			return false;

		}
		else
		{
			if(key == 'quantidade'){
				tipo_campo = ' REAL';
			}	
			campos.push(key + tipo_campo);
		}
	});

	return campos.join(', ');

}


/**
 * Metódo:		verificar_dados
 * 
 * Descrição:	Função Utilizada para verificar se todos os valores dos campos sao nulos, se forem irá FALSE para não ser inserido no banco
 * 
 * Data:			17/09/2013
 * Modificação:	17/09/2013
 * 
 * @access		public
 * @param		object(JSON) 		dados				- Dados em JSON retornados do WS
 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function verificar_dados(dados)
{

	var retorno = true;
	if(dados.length == 1)
	{
		$.each(dados, function(i, objeto) {

			var campos_nulos = [];
			var total_campos = Object.keys(objeto).length;


			$.each(objeto, function(indice, valor) {
				if(!valor)
				{
					campos_nulos.push(valor);
				}
			});

			if(total_campos == campos_nulos.length)
			{

				retorno = false;
			}

		});
	}

	return retorno;
}


/**
 * Metódo:		obter_tipo_cliente
 * 
 * Descrição:	Função Utilizada para retornar o a descrição do tipo de cliente
 * 
 * Data:			10/09/2013
 * Modificação:	10/09/2013
 * 
 * @access		public
 * @param		string 		$tipo				- Tipo de Cliente

 * @version		1.0
 * @author 		DevelopWeb Soluções Web
 * 
 */
function obter_tipo_cliente(tipo)
{
	switch (tipo) {
	case 'L':
		return 'Produtor Rural';
		break;

	case 'F':
		return 'Cons. Final';
		break;

	case 'R':
		return 'Revendedor';
		break;

	case 'S':
		return 'ICMS Solidário sem IPI na base';
		break;

	case 'X':
		return 'Exportação';
		break;

	default:
		return 'N/A';
	break;

	}

}




function concatenar_html(tag_inicial, tag_final, array)
{
	var string = '';
	$.each(array, function(indice, valor) {

		string += tag_inicial + valor + tag_final;
	});
	return string;
}


//----------
function formatar_telefone(string)
{
	 


	 string = (string.replace(new RegExp("[^0-9]", "g"), ""));
	 
	 
	console.log('Telefone: '+ string);
	console.log('TE = ' + string.length );

	if(string.length == 8)
	{
		var primeiros_digitos = string.substring(0,4);
		var ultimos_digitos = string.substring(4,8);

		return primeiros_digitos + '-' + ultimos_digitos;
	}
	if(string.length == 10)
	{
		var ddd = string.substring(0,2);
		var primeiros_digitos = string.substring(2,6);
		var ultimos_digitos = string.substring(6,10);

		return '(' + ddd + ') ' + primeiros_digitos + '-' + ultimos_digitos;
	}
	else if(string.length == 11)
	{
		
		//04732146455 
		var ddd = string.substring(0,3);
		var primeiros_digitos = string.substring(3,7);
		var ultimos_digitos = string.substring(7,11);

		return '(' + ddd + ') ' + primeiros_digitos + '-' + ultimos_digitos;
	}
	else
	{
		return string;
	}
}

function formatar_moeda(string, casas)
{
	return number_format(string, casas, ',', '.')
}

function formatar_cpfCNPJ(string)
{
	if(string.length == 11)
	{
		string=string.replace(/\D/g,"");                // Remove tudo o que não é dígito
		string=string.replace(/(\d{3})(\d)/,"$1.$2");   // Coloca ponto entre o terceiro e o quarto dígitos
		string=string.replace(/(\d{3})(\d)/,"$1.$2");   // Coloca ponto entre o setimo e o oitava dígitos
		string=string.replace(/(\d{3})(\d)/,"$1-$2");   // Coloca ponto entre o decimoprimeiro e o decimosegundo dígitos
		return string;
	}
	else
	{
		string=string.replace(/\D/g,"");                //Remove tudo o que não é dígito
		string=string.replace(/(\d{2})(\d)/,"$1.$2");
		string=string.replace(/(\d{3})(\d)/,"$1.$2");
		string=string.replace(/(\d{3})(\d)/,"$1/$2"); 
		string=string.replace(/(\d{4})(\d)/,"$1-$2"); 
		return string
	}
}

function formatar_cep(string)
{
	if(string.length == 8)
	{
		var primeiros = string.substring(0,5);
		var ultimos = string.substring(5,8);

		return primeiros + '-' + ultimos;
	}
	else
	{
		return string;
	}
}

function remover_caracteres_telefone(string)
{
	string = str_replace("(", "", string);
	string = str_replace(")", "", string);
	string = str_replace("-", "", string);
	string = str_replace(" ", "", string);

	if(string)
	{
		return string;
	}
	else
	{
		return '';	
	}
}

function obter_tipo_frete(tipo)
{
	if(tipo == 'F')
	{
		return 'FOB';
	}
	else if(tipo = 'C')
	{
		return 'CIF';
	}
	else
	{
		return 'N/A';
	}
}

function isEmpty(obj) {
	if (typeof obj == 'undefined' || obj === null || obj === '') return true;
	if (typeof obj == 'number' && isNaN(obj)) return true;
	if (obj instanceof Date && isNaN(Number(obj))) return true;
	return false;
}

/**
 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
 * @param obj1
 * @param obj2
 * @returns obj3 a new object based on obj1 and obj2
 */
function merge_options(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

function validar_decimal(pStr)
{

	var pStr = converter_preco(pStr);

	var reMoeda = /^(\d+|\d{1,3}(\.\d{3})+)*\,\d+$/;
	
	if (reMoeda.test(pStr)) {
		return true;
	} else if (pStr != null && pStr != "") {
		return false;
	}
}

//Converter numeros inteiros em decimais (Exp: 10 para 10,00)
function converter_preco(preco_campo)
{
	
	if(preco_campo)
	{
		var virgula 		= preco_campo.indexOf(','); 
		var ponto 			= preco_campo.indexOf('.'); 
		
		
		if(virgula <= 0 && ponto <= 0)
		{
			var preco =   number_format(preco_campo, 2, ',', '');
		}
		else
		{
			var preco = preco_campo;
		}
		
		return preco;
	}
}

function validar_digitos(pStr)
{
	var reDigits = /^\d+$/;

	if (reDigits.test(pStr)) {
		//alert(pStr + " contém apenas dígitos.");
		return true;
	} else if (pStr != null && pStr != "") {
		//alert(pStr + " NÃO contém apenas dígitos.");
		return false;
	}
}

/**
 * Converter valor decimal para que possa ser calculado. Exemplo (1.250,650 => 1250.650)
 * @param valor
 */
function converter_decimal(valor)
{
	var valor = str_replace(".", "", valor);
	var valor = str_replace(",", ".", valor);
	
	if(!valor)
	{
		valor = 0;
	}
	return valor;
}


function remover_zero_esquerda(sStr){
   var i;
   for(i=0;i<sStr.length;i++)
      if(sStr.charAt(i)!='0')
         return sStr.substring(i);
   return sStr;
}


/**
 * ----------------------------------------------------------
 * Função cria um elemento html para gerar o Dialog
 * ----------------------------------------------------------
 * 
 * @param aDivID
 * @param aMensagem
 * @param aTitulo
 * @returns {Boolean}
 */
function criarDialog(aDivID, aMensagem, aTitulo)
{
	if (aDivID == ''){
		aDivID = 'dialog';
	}
	
	if (document.getElementById(aDivID) == null){		
		novoDiv = '<div id="'+aDivID+'" title="'+aTitulo+'" style="display:none;">';
		novoDiv = novoDiv + '<p>' + aMensagem + '</p>';
		novoDiv = novoDiv + '</div>';
		$('body').append(novoDiv);
		return true;
	} else {
		return false;
	}
}

/**
 * ----------------------------------------------------------
 * Função exibi um dialog com mensagem para o usuário.
 * ----------------------------------------------------------
 * 
 * @param aDivID
 * @param aMensagem
 * @param aAcao
 * @param aTitulo
 */
function exibirMensagem(aDivID, aMensagem, aAcao, aTitulo)
{
	criarDialog(aDivID, aMensagem, aTitulo);
	
	$(function() {
		$("#"+aDivID).dialog({
			title: '<span class="ui-icone-dialog icone-alerta"></span> '+aTitulo,
			bgiframe: true,			
			minWidth: 600,
			resizable: true,
			draggable: true,
			zIndex: 9999,
			modal: true,
			dialogClass: "no-close",
			open: function (event, ui) {
				$(".ui-widget-overlay").css('z-index', '0');
				$("input").blur();
	        },
			buttons: {
				'OK': function() {
					$(this).dialog('close');
					if (aAcao != ''){
						eval(aAcao);
					}
					$("#"+aDivID).remove();
				},	
				'Cancelar': function(){
					$(this).dialog('close');
					$("#"+aDivID).remove();
				}
			}
		});
	});
}


/**
 * ----------------------------------------------------------
 * Função exibi um dialog com mensagem para o usuário.
 * ----------------------------------------------------------
 * 
 * @param aDivID
 * @param aMensagem
 * @param aAcao
 * @param aTitulo
 */
function exibirMensagemSimNao(aDivID, aMensagem, aAcaoSim, aAcaoNao, aTitulo)
{
	criarDialog(aDivID, aMensagem, aTitulo);
	
	$(function() {
		$("#"+aDivID).dialog({
			title: '<span class="ui-icone-dialog icone-alerta"></span> '+aTitulo,
			bgiframe: true,			
			minWidth: 600,
			resizable: true,
			draggable: true,
			zIndex: 9999,
			modal: true,
			dialogClass: "no-close",
			open: function (event, ui) {
				$(".ui-widget-overlay").css('z-index', '0');
				$("input").blur();
	        },
			buttons: {
				'Sim': function() {
					$(this).dialog('close');
					if (aAcaoSim != ''){
						eval(aAcaoSim);
					}
					$("#"+aDivID).remove();
				},	
				'Não': function(){
					$(this).dialog('close');
					if (aAcaoNao != ''){
						eval(aAcaoNao);
					}
					$("#"+aDivID).remove();
				}
			}
		});
	});
}

function mensagem(mensagem, acao, titulo)
{
	
	if(!!!titulo){		
		titulo = 'ATENÇÃO';		
	}
	
	criarDialog('alerta_dialog', mensagem, titulo);
	
	$(function() {
		$("#alerta_dialog").dialog({
			title: '<span class="ui-icone-dialog icone-alerta"></span> '+titulo,
			bgiframe: true,			
			minWidth: 600,
			resizable: false,
			draggable: false,
			zIndex: 9999,
			modal: true,
			dialogClass: "no-close",
			open: function (event, ui) {
				$(".ui-widget-overlay").css('z-index', '0');
				$("input").blur();
	        },
			buttons: {
				'OK': function() {
					$(this).dialog('close');
					
					if (acao != ''){
						eval(acao);
					}
					
					$("#alerta_dialog").remove();
				}
			}
		});
	});
}



function confirmar(mensagem, acao, acaoCancelar)
{
	criarDialog('confirmar_dialog', mensagem, 'ATENÇÃO');
	
	$(function() {
		$("#confirmar_dialog").dialog({
			bgiframe: true,
			title: '<span class="ui-icone-dialog icone-alerta"></span> ATENÇÃO',
			minWidth: 600,
			resizable: false,
			draggable: false,
			zIndex: 9999,
			modal: true,
			dialogClass: "no-close",
			open: function (event, ui) {
				$(".ui-widget-overlay").css('z-index', '0');
				$("input").blur();
	        },
			buttons: {
				'Sim':eval(acao),	
				'Cancelar': function(){
					
					if(acaoCancelar)
					{
						acaoCancelar();
					}
					else
					{
						$(this).dialog('close');
						$("#confirmar_dialog").remove();
					}
				}
			}
		});
	});
}

function verificarAparelho()
{
	var ua = navigator.userAgent.toLowerCase();
	var isiPad = ua.match(/ipad/i) != null;
	var isiPhone = ua.match(/iphone/i) != null;
	var isAndroid = ua.indexOf("android") > -1;

	// Exibir opção de ver mapas somente para Tablets (Pois tem GPS)
	if(isiPad || isiPhone || isAndroid)
	{
		return true;
	}else{
		return false;
	}
}

function sair()
{
	localStorage.setItem('nome_real', '');
	
	window.location = 'autenticar.html';
	
}

function abrirNotificacoes()
{
	$('.notificacao').toggle();
}


//------------------------------------------------------------------------------------------------
// Obter Empresas
// ------------------------------------------------------------------------------------------------
function abrirEmpresas()
{
	
	if(info.empresa_representante)
	{
		var where = " WHERE codigo = '" + info.empresa_representante + "'";
	}
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM empresas ' + where + ' ORDER BY codigo', [], function(x, dados) {
				if (dados.rows.length)
				{
					var options = "";
					for (i = 0; i < dados.rows.length; i++)
					{
						var empresa = dados.rows.item(i);
						empresa.codigo;
						empresa.decricao;
						
						options += '<option value="' + empresa.codigo + '">' + empresa.descricao + '</option>';
					}
				
					//$('.barra').prepend('<div class="empresas"><select id="empresa_select" style="padding:0px;padding-right:25px;" name="empresas">' + options + '</select></div>');

					//----------------
					

					var html = '';
						html += '<table cellspacing="0">';
						html += '<tr>';
						html += '	<td><b>Para alterar a empresa, selecione abaixo.</b></td>';
						html += '</tr>';
						html += '<tr>';
						html += '	<td>';
						html += '		<form><select style="width:300px;" id="empresa_select" name="empresas">';
						html += options;			
						html += '		</select></form>';
						html += '	</td>';
						html += '</tr>';
						html += '</table>';
						
					exibirMensagem('dialog_lembrar', html, '', 'Empresas');
					
					// Selecionar empresa da sessao
					$('#empresa_select option').each(function(){
						if($(this).val() == localStorage.getItem('empresa')){
							$(this).attr('selected',true);
						}
					});
					
					// Selecionar empresa caso não existir a sessao
					if(!localStorage.getItem('empresa'))
					{
						$('#empresa_select').change();
					}
					
					//----------------
				}
			});
	});
	
}
/**
 * Método: verificar_dia_semana
 * 
 * Descrição: Retorna o dia da semana de acordo com o timestamp
 * 
 * Retorno: 0 => Domingo, 1 => Segunda, ..., 6 => Sábado
 * 
 * @param timestamp
 * @returns int
 */
function verificar_dia_semana(timestamp)
{
	var data = new Date(date('Y', timestamp), (date('m', timestamp) - 1), date('d', timestamp));
	
	return data.getDay();
}

/**
 * Método: adicionar_dias_uteis()
 * 
 * Descrição: Adiciona X dias úteis a partir da data passada por paremetro. 
 * 
 * @param timestamp		=> Data que será incrementada
 * @param dias			=> Dias úteis a serem adicionados a data
 * @param contar_sabado	=> Contar sábado aos dias úteis
 * @returns time
 */
function adicionar_dias_uteis(timestamp, dias, contar_sabado)
{
	for(i = 0; i < dias; i++)
	{
		timestamp += 3600 * 24 * 1;
		if(verificar_dia_semana(timestamp) == 6)
		{
			if(!contar_sabado)
			{
				timestamp += 3600 * 24 * 2;
			}
		}
		else if(verificar_dia_semana(timestamp) == 0)
		{
			timestamp += 3600 * 24 * 1;
		}
	}
	
	return timestamp;
}

function obter_meses(mes)
{
	var meses = new Array();
	meses['01'] = 'Janeiro';
	meses['02'] = 'Fevereiro';
	meses['03'] = 'Março';
	meses['04'] = 'Abril';
	meses['05'] = 'Maio';
	meses['06'] = 'Junho';
	meses['07'] = 'Julho';
	meses['08'] = 'Agosto';
	meses['09'] = 'Setembro';
	meses['10'] = 'Outubro';
	meses['11'] = 'Novembro';
	meses['12'] = 'Dezembro';
	
	if(mes)
	{
		return meses[mes];
	}
	
	return meses;
}

function obter_formas_pagamento(forma)
{
	var formas_pagamento = new Array();
	
	formas_pagamento['CH'] = 'CHEQUE';
	formas_pagamento['BL'] = 'BOLETO';
	formas_pagamento['ES'] = 'ESPECIE';
	
	if(forma)
	{
		if(array_key_exists(forma, formas_pagamento))
		{
			return formas_pagamento[forma];
		}
		else
		{
			return 'N/A';
		}
	}
	
	return formas_pagamento;
}

//CUSTOM Catalogo
function datetime2date(datetime){
	var data = explode(' ',datetime);
	console.log(data[0]);
	data = str_replace('-','',data[0]);
	
	return protheus_data2data_normal(data);
	
	
}
//FIM CUSTOM Catalogo
//CUSTOM Catalogo
function obter_situacao(codigo){
	if(codigo == 'OK'){
		return 'Normal';
	}else if(codigo == 'BLQ'){
		return 'Bloqueado';
	}else{
		return codigo;
	}
	
}
//FIM CUSTOM Catalogo

/** Verifica se o campo é um número válido
 *  
 * @var text valor - Valor do input.
 * @return boolean 
 * 
 */

function validar_campo_numerico(valor){	
	valor = trim(valor);
	valor = str_replace(',','.',valor);
	
	
	return is_numeric(valor);
	
}



function obter_status_pedidos_pendentes(status){
	
	
	if(status == 'R'){
		return 'reprovado';
	}else if(status == 'P'){
		return 'preposto';
	}else if(status == 'L'){
		return 'status/verde';	
	}else{
		return 'aguardando_analise';
	}
	
}


function isValidDate(s) {
    // format D(D)/M(M)/(YY)YY
    var dateFormat = /^\d{1,4}[\.|\/|-]\d{1,2}[\.|\/|-]\d{1,4}$/;

    if (dateFormat.test(s)) {
        // remove any leading zeros from date values
        s = s.replace(/0*(\d*)/gi,"$1");
        var dateArray = s.split(/[\.|\/|-]/);
      
        // correct month value
        dateArray[1] = dateArray[1]-1;

        // correct year value
        if (dateArray[2].length<4) {
            // correct year value
            dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
        }

        var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
        if (testDate.getDate()!=dateArray[0] || testDate.getMonth()!=dateArray[1] || testDate.getFullYear()!=dateArray[2]) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

/*
document.addEventListener('deviceready', function(){
	document.addEventListener("backbutton", function (e) {
		
		console.log(strpos(window.location.href,'prospects_visualizar'));
		console.log(strpos(window.location.href,'pedidos_espelho'));
		
		
		if(strpos(window.location.href,'pedidos_espelho') != false){
			var codigo_e_empresa 	= parse_url(location.href).fragment;
			array_codigo_e_empresa 	= codigo_e_empresa.split('|');
			var permitir_retorno		=isset(array_codigo_e_empresa[3])?false:true;
			console.log('permitir: '+permitir_retorno);
			
			if(!permitir_retorno){
				location.href = 'pedidos_adicionar.html';
				//navigator.app.backHistory();
			}else{
				navigator.app.backHistory();
			}
			
		}else if(strpos(window.location.href,'prospects_visualizar') != false){
			
			var parametros = parse_url(location.href).fragment;
			var array_codigo_e_inclusao 	= parametros.split('|');
				
			var permitir_retorno		=isset(array_codigo_e_inclusao[1])?false:true;
			
			if(!permitir_retorno){
				
				  document.addEventListener("endcallbutton", function(){
					  
					  mensagem('back complete');
					  
				  }, false);
				
				//navigator.app.backHistory();
				location.href = 'prospects_adicionar.html';
			}else{
				navigator.app.backHistory();
			}
		}else{
			navigator.app.backHistory();
			
		}
		
	 }, false);
	
},false);



*/
function voltar_pagina(){
	navigator.app.backHistory();
	/*
	if(strpos(window.location.href,'pedidos_espelho') != false){
		var codigo_e_empresa 	= parse_url(location.href).fragment;
		array_codigo_e_empresa 	= codigo_e_empresa.split('|');
		var permitir_retorno		=isset(array_codigo_e_empresa[3])?false:true;
		console.log('permitir: '+permitir_retorno);
		
		if(!permitir_retorno){
			location.href = 'pedidos_adicionar.html';
			//navigator.app.backHistory();
		}else{
			navigator.app.backHistory();
		}
		
	}else if(strpos(window.location.href,'prospects_visualizar') != false){
		
		var parametros = parse_url(location.href).fragment;
		var array_codigo_e_inclusao 	= parametros.split('|');
			
		var permitir_retorno		=isset(array_codigo_e_inclusao[1])?false:true;
		
		if(!permitir_retorno){
			//navigator.app.backHistory();
			location.href = 'prospects_adicionar.html';
		}else{
			navigator.app.backHistory();
		}
	}else{
		navigator.app.backHistory();
		
	}
	*/
}




function retirar_acentos(palavra) { 
    com_acento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ~´'; 
    sem_acento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC  '; 
    nova=''; 
    for(i=0;i<palavra.length;i++) { 
        if (com_acento.search(palavra.substr(i,1))>=0) { 
            nova+=sem_acento.substr(com_acento.search(palavra.substr(i,1)),1); 
        } 
        else { 
            nova+=palavra.substr(i,1); 
        } 
    } 
    return nova; 
}



function dataMaiorQue(data, data_minima){
	
	if (!!!data_minima){
		var data_atual = date('Ymd');
		data_minima = data_atual;
	}	
	
	if(data < data_minima){
		
		return false;
	}
	return true;
	
}