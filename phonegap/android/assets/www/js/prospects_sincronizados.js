$(document).ready(function() {
	// obter prospects
	
	sessionStorage['prospects_ordem_atual'] = 'codigo DESC'; 
	
	obter_prospects(sessionStorage['prospects_pagina_atual'], sessionStorage['prospects_ordem_atual']);
	
	
	// Obter Estados
	$('select[name*=estado]').html('<option value> Selecione... </option>' + ufs);
	
	
	// Obter Municipios
	
	$('select[name=estado]').live('change', function(){
		$('select[name=municipio]').attr('disabled', 'disabled');
		$('select[name=municipio]').html('<option value> Carregando... </option>');
		
		var estado = $(this).val();
		
		obter_municipios(estado);
		
	});
	
	function obter_municipios(estado, municipio)
	{
		// obter Municipios
		db.transaction(function(x) {
			x.executeSql('SELECT DISTINCT codigo, nome FROM municipios WHERE uf = ? ORDER BY nome ASC', [estado], function(x, dados) {
					
					$('select[name=municipio]').empty();
					$('select[name=municipio]').removeAttr('disabled');
					$('select[name=municipio]').append('<option value> Selecione... </option>');
					if (dados.rows.length)
					{
						for (i = 0; i < dados.rows.length; i++)
						{
							var dado = dados.rows.item(i);
							
							var selected = '';
							
							if(municipio == dado.codigo)
							{
								selected = 'selected="selected"';
							}
							
							$('select[name=municipio]').append('<option value="' + dado.codigo + '" ' + selected + '>' + dado.nome + '</option>');
						}
					}
					else
					{
						$('select[name=municipio]').empty();
						$('select[name=municipio]').attr('disabled', 'disabled');
						$('select[name=municipio]').append('<option value> Selecione um Estado </option>');
					}
					
				}
			);
			
		});
	}

	
	$('select[name=municipio]').live('change', function(){
		
		var nome_municipio = $('select[name=municipio] option:selected').html();
		$('input[name=nome_municipio]').val(nome_municipio);
		
	});
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_prospects(sessionStorage['prospects_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_prospects(sessionStorage['prospects_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['prospects_' + name] != 'undefined' ? sessionStorage['prospects_' + name] : '');
	});
	
	$('#filtrar').click(function() {
		sessionStorage['prospects_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['prospects_' + name] = $(this).val();
		});
		
		obter_prospects(sessionStorage['prospects_pagina_atual'], sessionStorage['prospects_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['prospects_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['prospects_' + name] = '';
		});
		
		obter_prospects(sessionStorage['prospects_pagina_atual'], sessionStorage['prospects_ordem_atual']);
	});
});

function obter_prospects(pagina, ordem)
{
	
	
	obter_grupo_permissao(function(grupo_permissao) {
		pagina = pagina ? pagina : 1;
		ordem = ordem ? ordem : 'codigo DESC';
		
		// setar pagina e ordem atual
		sessionStorage['prospects_pagina_atual'] 	= pagina;
		sessionStorage['prospects_ordem_atual'] 	= ordem;
		
		// definir seta
		var ordem_atual = explode(' ', sessionStorage['prospects_ordem_atual']);
		
		if (ordem_atual[1] == 'ASC')
		{
			$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'ASC');
			
			$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▲');
		}
		else
		{
			$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'DESC');
			
			$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▼');
		}
		
		// calcular offset
		var offset = (sessionStorage['prospects_pagina_atual'] - 1) * 20;
		
		// gerar filtros
		
		prospects_status = sessionStorage['prospects_status'];
		dt_inicial 		= sessionStorage['prospects_dt_inicial'];
		dt_final 		= sessionStorage['prospects_dt_final'];
		nome 			= sessionStorage['prospects_nome'];
		nome_fantasia 	= sessionStorage['prospects_nome_fantasia'];
		estado 			= sessionStorage['prospects_estado'];
		municipio 		= sessionStorage['prospects_municipio'];
		telefone 		= sessionStorage['prospects_telefone'];
		
		var wheres = '';
		
		if (prospects_status && prospects_status != 'undefined')
		{
			wheres += " AND status = '" + prospects_status + "'";
		}
		
		
		if (dt_inicial && dt_inicial != 'undefined')
		{	
			var ex = explode('/', dt_inicial);
			
			wheres += ' AND data_emissao >= "' + ex[2] + ex[1] + ex[0] + '"';
		}
	
		if (dt_final && dt_final != 'undefined')
		{
			var ex = explode('/', dt_final);
		
			wheres += ' AND data_emissao <= "' + ex[2] + ex[1] + ex[0] + '"';
		}
		
		if (nome && nome != 'undefined')
		{
			wheres += ' AND nome LIKE "%' + nome + '%"';
		}
		
		if (nome_fantasia && nome_fantasia != 'undefined')
		{
			wheres += ' AND nome_fantasia LIKE "%' + nome_fantasia + '%"';
		}
		
		if (estado && estado != 'undefined')
		{
			wheres += ' AND estado = "' + estado + '"';
		}
		
		if (municipio && municipio != 'undefined')
		{
			wheres += ' AND codigo_municipio = "' + municipio + '"';
		}
		
		if (telefone && telefone != 'undefined')
		{
			wheres += ' AND telefone LIKE "%' + telefone + '%"';
		}
		
		if (info.empresa)
		{
			wheres += " AND empresa = '" + info.empresa + "'";
		}
		
		
		if (grupo_permissao.grupo == 'preposto') {
			
			wheres += ' AND codigo_preposto = \''+ grupo_permissao.codigo_preposto+'\'';
		}
		
		
		
		
		wheres += ' AND exportado = "1"';
		
		console.log('SELECT * FROM prospects WHERE codigo > 0 ' + wheres + ' ORDER BY ' + sessionStorage['prospects_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);	
		
		db.transaction(function(x) {
		
			// Exibir Prospects
			x.executeSql('SELECT * FROM prospects WHERE recno > 0 ' + wheres + ' ORDER BY ' + sessionStorage['prospects_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados){
				
				if(dados.rows.length)
				{
					$('table tbody').empty();
				
					for (i = 0; i < dados.rows.length; i++)
					{
						var item = dados.rows.item(i);
						
						var status = '';
						
						if(item.status == 'A')
						{
							status = 'aguardando_analise';
						}
						else if(item.status == 'L')
						{
							status = 'faturado';
						}else if(item.status == 'P'){
							status = 'preposto';
						}else if(item.status == 'R'){
							status = 'reprovado';
						}else if(item.status == 'I')
						{
							status = 'inativo';
						}else{
							status = 'faturado';
						}
						
						var html = '';
							html += '<td align="center"><img src="img/' + status + '.png"></td>';
							html += '<td>'+item.nome+'</td>';
							html += '<td>'+(item.nome_fantasia ? item.nome_fantasia : 'N\A')+'</td>';
							html += '<td>'+ (item.estado ? item.estado : '' ) + (item.nome_municipio ? '/' + item.nome_municipio : 'N\A') + '</td>';
							html += '<td width="150" align="center">'+(item.telefone ? formatar_telefone(item.ddd+item.telefone) : 'N\A')+'</td>';
							html += '<td width="150" align="center">'+(item.data_emissao ? protheus_data2data_normal(item.data_emissao) : 'N\A')+'</td>';
							html += '<td width="80" align="center">'+'<a href="prospects_visualizar.html#' + item.codigo + '" class="btn btn-large btn-primary">+ Opções</a>'+'</td>';
						
						$('table tbody').append('<tr>' + html + '</tr>');
						
						alterarCabecalhoTabelaResolucao();
					}
				}
				else
				{
					$('table tbody').html('<tr><td colspan="7" style="color: #900; padding: 10px;"><strong>Nenhum registro encontrado.</strong></td></tr>');
				}
				
			});
			
			
			//Exibir totais Prospects
			
			x.executeSql(
				'SELECT COUNT(codigo) AS total FROM prospects WHERE recno > 0 ' + wheres, [], function(x, dados) {
					var dado = dados.rows.item(0);
					
					$('#total').text(number_format(dado.total, 0, ',', '.'));
					
					// paginação
					
					$('#paginacao').html('');
					
					var total = ceil(dado.total / 20);
					
					if (total > 1)
					{
						if (sessionStorage['prospects_pagina_atual'] > 6)
						{
							$('#paginacao').append('<a href="#" onclick="obter_prospects(1, \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
						}
						
						if (sessionStorage['prospects_pagina_atual'] > 1)
						{
							$('#paginacao').append('<a href="#" onclick="obter_prospects(' + (intval(sessionStorage['prospects_pagina_atual']) - 1) + ', \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">&lt;</a> ');
						}
						
						for (i = intval(sessionStorage['prospects_pagina_atual']) - 6; i <= intval(sessionStorage['prospects_pagina_atual']) + 5; i++)
						{
							if (i <= 0 || i > total)
							{
								continue;
							}
							
							if (i == sessionStorage['prospects_pagina_atual'])
							{
								$('#paginacao').append('<strong>' + i + '</strong> ');
							}
							else
							{
								$('#paginacao').append('<a href="#" onclick="obter_prospects(' + i + ', \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">' + i + '</a> ');
							}
						}
						
						if (sessionStorage['prospects_pagina_atual'] < total)
						{
							$('#paginacao').append('<a href="#" onclick="obter_prospects(' + (intval(sessionStorage['prospects_pagina_atual']) + 1) + ', \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">&gt;</a> ');
						}
						
						if (sessionStorage['prospects_pagina_atual'] <= total - 6)
						{
							$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_prospects(' + total + ', \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">Última Página</a> ');
						}
					}
				}
			);
			
			
			
		});
		
		// exibir pedido e calcular totais
		/*
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM prospects WHERE id > 0 ' + wheres + ' ORDER BY ' + sessionStorage['prospects_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
					if (dados.rows.length)
					{
						$('table tbody').empty();
						
						for (i = 0; i < dados.rows.length; i++)
						{
							var p = dados.rows.item(i);
							
							$('table tbody').append('<tr><td>' + p.nome + '</td><td>' + (p.nome_fantasia ? p.nome_fantasia : '-') + '</td><td>' + (p.municipio ? p.municipio : '-') + '</td><td>' + (p.telefone ? p.telefone : '-') + '</td><td><a href="prospects_visualizar_' + (p.tipo_pessoa == 'pessoa_fisica' ? 'pf' : 'pj') + '.html#' + p.id + '">+ Opções</a></td></tr>');
						}
					}
					else
					{
						$('table tbody').html('<tr><td colspan="6" style="color: #900; padding: 10px;"><strong>Nenhum prospect encontrado.</strong></td></tr>');
					}
				}
			);
			
			// calcular totais
			
			x.executeSql(
				'SELECT COUNT(id) AS total FROM prospects WHERE id > 0 ' + wheres, [], function(x, dados) {
					var dado = dados.rows.item(0);
					
					$('#total').text(number_format(dado.total, 0, ',', '.'));
					
					// paginação
					
					$('#paginacao').html('');
					
					var total = ceil(dado.total / 20);
					
					if (total > 1)
					{
						if (sessionStorage['prospects_pagina_atual'] > 6)
						{
							$('#paginacao').append('<a href="#" onclick="obter_prospects(1, \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
						}
						
						if (sessionStorage['prospects_pagina_atual'] > 1)
						{
							$('#paginacao').append('<a href="#" onclick="obter_prospects(' + (intval(sessionStorage['prospects_pagina_atual']) - 1) + ', \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">&lt;</a> ');
						}
						
						for (i = intval(sessionStorage['prospects_pagina_atual']) - 6; i <= intval(sessionStorage['prospects_pagina_atual']) + 5; i++)
						{
							if (i <= 0 || i > total)
							{
								continue;
							}
							
							if (i == sessionStorage['prospects_pagina_atual'])
							{
								$('#paginacao').append('<strong>' + i + '</strong> ');
							}
							else
							{
								$('#paginacao').append('<a href="#" onclick="obter_prospects(' + i + ', \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">' + i + '</a> ');
							}
						}
						
						if (sessionStorage['prospects_pagina_atual'] < total)
						{
							$('#paginacao').append('<a href="#" onclick="obter_prospects(' + (intval(sessionStorage['prospects_pagina_atual']) + 1) + ', \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">&gt;</a> ');
						}
						
						if (sessionStorage['prospects_pagina_atual'] <= total - 6)
						{
							$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_prospects(' + total + ', \'' + sessionStorage['prospects_ordem_atual'] + '\'); return false;">Última Página</a> ');
						}
					}
				}
			);
		});
		*/
	});
}
