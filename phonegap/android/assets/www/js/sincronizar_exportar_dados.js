document.addEventListener("deviceready", onDeviceReady2, false);

function onDeviceReady2(){
	console.log('Obtendo envios necessarios');
	obterRegistrosNaoExportados();
	
	// Enviar Todos os Pedidos
	$("#btnEnviarPedidos").live('click', function(e){
		e.preventDefault();
		
		confirmar('Deseja enviar <b>TODOS</b> os pedidos?', 
			function () {
				$(this).dialog('close');
				
				$("#boxLabelSincronismo").show();
				$("#labelStatusSincronismo").html("Sincronização em andamento...");
				$("#progressbar").show();
				$("#botoesSincronismo").hide();
				$("#statusSincronismo").html("");
				$("#statusSincronismo").show();
				$("#modulos").hide();
				$("#btnVisualizarLogSync").hide();
				$("#btnVisualizarModulosSync").hide();
				
				//$('.barra').html('<div id="logo_home"><img src="img/logo_dwpdr.png" width="39" height="40"></div>');
				
				$('[name="modulos[pedidos_pendentes]"]').attr('checked', 'checked');
				
				var usuarios = "";
				var senha = "";
				db.transaction(function(x) {
					x.executeSql('SELECT * FROM informacoes_do_representante', [], function(x, dados) {
							if (dados.rows.length > 0)
							{
								var dado = dados.rows.item(0);
								usuarios = dado.usuario;
								senha = dado.senha;

								//
								
								$.ajax({
									url: config.ws_url + 'autenticacao/autenticar',
									headers : {
										"DW_KEY_APP" : md5(usuarios.toUpperCase() + senha.toUpperCase() )
									},     
									data: {
										key: md5(usuarios.toUpperCase()  + senha.toUpperCase())
									},
									dataType: 'json',
									type: 'GET',
									success: function(dados) {
									if (!dados['error'] && dados.status != 'inativo' )
									{
										exportarPedidos(false, function(){
											iniciarSincronismo(0, function(retorno){
												finalizarSincronismo(sincronizado);
											});
										});
										
										$("#confirmar_dialog").remove();
									}
									else
									{
										$("#confirmar_dialog").remove();
										//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
										mensagem('Representante Inativo.');
										
										$("#boxLabelSincronismo").hide();
										$("#labelStatusSincronismo").html("");
										$("#progressbar").hide();
										$("#botoesSincronismo").show();
										$("#statusSincronismo").html("");
										$("#statusSincronismo").hide();
										$("#modulos").show();
										$("#btnVisualizarLogSync").show();
										$("#btnVisualizarModulosSync").show();
									}
									
									
									
								},
								error: function() {
									//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
									mensagem('Não foi possível se conectar com o servidor, verifique sua conexão com a internet. Por favor tente novamente.');
								}
							});
								
								//
							}
						}
					);
				});
				
				
					
			}
		);
	});
	
	
	// Enviar Todos os Prospects
	$("#btnEnviarProspects").live('click', function(e){	
		e.preventDefault();
		
		confirmar('Deseja enviar <b>TODOS</b> os prospects?', 
			function () {
				$(this).dialog('close');
				
				$("#boxLabelSincronismo").show();
				$("#labelStatusSincronismo").html("Sincronização em andamento...");
				$("#progressbar").show();
				$("#botoesSincronismo").hide();
				$("#statusSincronismo").html("");
				$("#statusSincronismo").show();
				$("#modulos").hide();
				$("#btnVisualizarLogSync").hide();
				$("#btnVisualizarModulosSync").hide();
				
				$('[name="modulos[prospects]"]').attr('checked', 'checked');
				
				//
				var usuarios = "";
				var senha = "";
				db.transaction(function(x) {
					x.executeSql('SELECT * FROM informacoes_do_representante', [], function(x, dados) {
							if (dados.rows.length > 0)
							{
								var dado = dados.rows.item(0);
								usuarios = dado.usuario;
								senha = dado.senha;

								//
								
								$.ajax({
									url: config.ws_url + 'autenticacao/autenticar',
									headers : {
										"DW_KEY_APP" : md5(usuarios.toUpperCase() + senha.toUpperCase() )
									},     
									data: {
										key: md5(usuarios.toUpperCase()  + senha.toUpperCase())
									},
									dataType: 'json',
									type: 'GET',
									success: function(dados) {
									if (!dados['error'] && dados.status != 'inativo' )
									{
										exportarDados('codigo', 'prospects', 'Prospects', '');
										
										$("#confirmar_dialog").remove();
									}
									else
									{
										$("#confirmar_dialog").remove();
										//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
										mensagem('Representante Inativo.');
										
										$("#boxLabelSincronismo").hide();
										$("#labelStatusSincronismo").html("");
										$("#progressbar").hide();
										$("#botoesSincronismo").show();
										$("#statusSincronismo").html("");
										$("#statusSincronismo").hide();
										$("#modulos").show();
										$("#btnVisualizarLogSync").show();
										$("#btnVisualizarModulosSync").show();
									}
									
									
									
								},
								error: function() {
									//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
									mensagem('Não foi possível se conectar com o servidor, verifique sua conexão com a internet. Por favor tente novamente.');
								}
							});
								
								//
							}
						}
					);
				});
				//
			}
		);
		
	});
	
		
	$('[name="modulos[pedidos_pendentes]"], [name="modulos[prospects]"]').click(function(){
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		var modulo = $(this).val();
		//Icone do campo
		if(checkbox) {
			$('input[name="' + modulo + '[]"]').button("option", "icons", {primary: "ui-icon-circle-check"});
		} else {
			$('input[name="' + modulo + '[]"]').button("option", "icons", {primary: "ui-icon-circle-close"});
		}
	});
	
	
};	

	function obterRegistrosNaoExportados() 
	{
		
		console.log('Obtendo pedidos pendentes nao enviados');
		//Obtem a lista de pedidos não sincronizados
		obterListaDadosNaoExportados('pedidos_pendentes', ['pedido_id_pedido', 'cliente_nome'], 'pedido_id_pedido');
		
		//Obtem a lista de prospects não sincronizados
		obterListaDadosNaoExportados('prospects', ['codigo', 'nome', 'nome_fantasia'], 'codigo');
		
		//Obtem a lista de historico de prospects não sincronizados
		obterListaDadosNaoExportados('historico_prospects', ['id', 'pessoa_contato'], 'id');
		
		//Obtem a lista de historico de clientes não sincronizados
		obterListaDadosNaoExportados('historico_clientes', ['id', 'pessoa_contato'], 'id');
		
		//Obtem a lista de pendencias não sincronizados
		obterListaDadosNaoExportados('pendencias', ['id', 'titulo'], 'id');
		
		//Obtem a lista de pendencias não sincronizados
		obterListaDadosNaoExportados('pendencias_mensagens', ['id', 'timestamp'], 'id');
	}
	
	/**
	 * Exibe a lista de dados que não foram sincronizados
	 * 
	 * @param modulo	= Tabela no sqlite
	 * @param campos	= Campos que serão selecionados e montados o label
	 * @param chave		= Chave identificadora do módulo
	 */
	function obterListaDadosNaoExportados(modulo, campos, chave) 
	{
		console.log('SELECT DISTINCT ' + campos.join(', ') + ' FROM ' + modulo + ' WHERE (exportado IS NULL OR erro = 1)');
		db.transaction(function(x) {
			x.executeSql('SELECT DISTINCT ' + campos.join(', ') + ' FROM ' + modulo + ' WHERE (exportado IS NULL OR erro = 1)', [], function(x, dados){
				var total = dados.rows.length;
				console.log(modulo+': '+total);
				$('#lista_' + modulo + '_nao_exportados').empty();
				
				if(total > 0) {
					for (i = 0; i < total; i++) {
						var item = dados.rows.item(i);
						
						var style_erro = '';
						var msg_erro = '';
						var label = '';
						if(item.erro == 1) {
							style_erro = ' class="erro" ';
							msg_erro = 'Problema ao sincronizar.';
						}
						
						for(y = 0; y < campos.length; y++) {
							label += item[campos[y]];
							if((y + 1) < campos.length) {
								label += ' - ';
							}
						}
						var html = '<input type="checkbox" value="' + item[chave] + '" name="' + modulo + '[]" id="' + modulo + '-' + i + '" /><label for="' + modulo + '-' + i + '" ' + style_erro + '>' + label + '</label>';
						console.log(html);
						$('#lista_' + modulo + '_nao_exportados').append('<li>' + html + '</li>');
					}
				} else {
					
					$('#lista_' + modulo + '_nao_exportados').append('<li>Nenhum registro para sincronizar.</li>');
					
				}
				
				$("input[name='" + modulo + "[]']").button({ icons: {primary:'ui-icon-circle-close'} });
			});
		});
	}
	
	/**
	 * Obtem a lista de codigos de pedidos a serem sincronizados
	 * 
	 * @param exportar_codigos	= Array com códigos de pedido especificos a serem sincronizados
	 * @param callback			= Função de callback
	 */
	function exportarPedidos(exportar_codigos, callback) 
	{
		var codigo_modulo = 'pedido_id_pedido';
		var modulo = 'pedidos_pendentes';
		var descricao_modulo = 'Pedidos Pendentes';
		
		$( "#progressbar" ).progressbar({value: 0});
		
		var usuarios = "";
		var senha = "";
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM informacoes_do_representante', [], function(x, dados) {
					if (dados.rows.length > 0)
					{
						var dado = dados.rows.item(0);
						usuarios = dado.usuario;
						senha = dado.senha;

						//
						
						$.ajax({
							url: config.ws_url + 'autenticacao/autenticar',
							headers : {
								"DW_KEY_APP" : md5(usuarios.toUpperCase() + senha.toUpperCase() )
							},     
							data: {
								key: md5(usuarios.toUpperCase()  + senha.toUpperCase())
							},
							dataType: 'json',
							type: 'GET',
							success: function(dados) {
							if (!dados['error'] && dados.status != 'inativo' )
							{
		
		var pedidos = new Array();
			
		db.transaction(function(x) {
			
			var where = 'exportado IS NULL';
			
			if(exportar_codigos && exportar_codigos.length > 0) {
				where = codigo_modulo + " IN ('" + implode('\', \'', exportar_codigos) + "')";
			}
			
			//Obtem os pedidos/orçamento a serem sincronizados
			x.executeSql('SELECT ' + codigo_modulo + ' FROM ' + modulo + ' WHERE ' + where + ' GROUP BY ' + codigo_modulo, [], function(x, dados) {
				var total = dados.rows.length;
				
				if(total > 0) {
					
					for(i = 0; i < total; i++) {
						var dado = dados.rows.item(i);
						
						pedidos[i] = dado[codigo_modulo];
					}
					
					//Chama a funçao enviarPedidos() para realizar o sincronismo dos pedidos individualmente
					enviarPedidos(pedidos, false, modulo, codigo_modulo, descricao_modulo, function(){
						callback();
					});
					
				} else {
					
					sincronizado = 2;
					
					callback();
					
				}
				
			}, function(){
				
				callback();
				
			});
		});
		
							}
							else
							{
								$("#confirmar_dialog").remove();
								//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
								mensagem('Representante Inativo.');
								
								$("#boxLabelSincronismo").hide();
								$("#labelStatusSincronismo").html("");
								$("#progressbar").hide();
								$("#botoesSincronismo").show();
								$("#statusSincronismo").html("");
								$("#statusSincronismo").hide();
								$("#modulos").show();
								$("#btnVisualizarLogSync").show();
								$("#btnVisualizarModulosSync").show();
							}
							
							
							
						},
						error: function() {
							//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
							mensagem('Não foi possível se conectar com o servidor, verifique sua conexão com a internet. Por favor tente novamente.');
						}
					});
						
						//
					}
				}
			);
		});
		
	}

	/**
	 * Função utilizada para realizar o envio dos pedidos ao webservice
	 * 
	 * @param pedidos			= Array com os códigos de pedidos para sincronizar
	 * @param iteracaoAtual		= Indice atual do pedido que esta sendo enviado no array "pedidos"
	 * @param modulo			= Nome do módulo do sincronismo
	 * @param codigo_modulo		= Campo chave da tabela
	 * @param descricao_modulo	= Descrição do módulo
	 * @param callback			= Função de callback
	 */
	function enviarPedidos(pedidos, iteracaoAtual, modulo, codigo_modulo, descricao_modulo, callback) 
	{
		//
		var usuarios = "";
		var senha = "";
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM informacoes_do_representante', [], function(x, dados) {
					if (dados.rows.length > 0)
					{
						var dado = dados.rows.item(0);
						usuarios = dado.usuario;
						senha = dado.senha;

						//
						
						$.ajax({
							url: config.ws_url + 'autenticacao/autenticar',
							headers : {
								"DW_KEY_APP" : md5(usuarios.toUpperCase() + senha.toUpperCase() )
							},     
							data: {
								key: md5(usuarios.toUpperCase()  + senha.toUpperCase())
							},
							dataType: 'json',
							type: 'GET',
							success: function(dados) {
							if (!dados['error'] && dados.status != 'inativo' )
							{
		
		if(iteracaoAtual == false) {
			var iteracao = 0;
		} else {
			var iteracao = iteracaoAtual;
		}
		
		if(iteracao == pedidos.length) { //Termino de envio dos pedidos/orçamentos
			
			callback();
			
		} else { //Envia o próximo pedido/orçamento
			
			//Atualiza a barra de progresso
			
			$("#labelStatusSincronismo").html('Enviando pedido ' + pedidos[iteracao].toUpperCase() + ' (' + (iteracao+1) + ' de ' + pedidos.length + ')…');
			var progress = $('#progressbar').progressbar('option', 'value') + (100 / pedidos.length);
			$( "#progressbar" ).progressbar({value: progress});
			
			//Obtem os dados do pedido/orçamento para realizar o sincronismo 
			db.transaction(function(x) {
			    x.executeSql('SELECT * FROM ' + modulo + ' WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '"', [], function(x, dados) {
		    		var total = dados.rows.length;
					
		    		var pedido = new Array();
		    		for(i = 0; i < total; i++) {
		    			pedido[i] = dados.rows.item(i);
		    		}
		    		console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		    		console.log("ITERACAO " + iteracao + " >> ENVIANDO PEDIDO " + pedidos[iteracao]);
		    		
		    		$("#statusSincronismo").prepend("<div class=\"info\">104 - Enviando pedido " + pedidos[iteracao] + ".</div>");
		    		
		    		//Envia o pedido selecionado
		    		var ajax = $.ajax({
						url: config.ws_url + modulo + '/importar_novo',
						type: 'POST',
						data: {
							retorno: json_encode(pedido)							
						},
						success: function(dados) {							
							if(dados.sucesso == 'ok') {								
								sincronizado = 2;
								
								//Marcando o pedido como exportado
								db.transaction(function(x) {
									x.executeSql('DELETE FROM ' + modulo + ' WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '"', [], function(){
										console.log("SQL SUCESSO >> " + 'DELETE FROM ' + modulo + ' WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '"');
										console.log("PEDIDO " + pedidos[iteracao] + " ENVIADO");
										console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
										
										$("#statusSincronismo").prepend("<div class=\"sucesso\">200 - Pedido " + pedidos[iteracao] + " enviado com sucesso.</div>");
										
										enviarPedidos(pedidos, iteracao + 1, modulo, codigo_modulo, descricao_modulo, callback);
									});
								});
								
							} else if(dados.erro) {
								
								sincronizado = 4;
								var mensagem ='Entre em contato com TI da sua empresa.';
								var codigo_erro = dados.codigo_erro;
								if(dados.mensagem){
									
									mensagem = dados.mensagem;
									
								}
								
								//Marcando o pedido como erro
								db.transaction(function(x) {
									x.executeSql('UPDATE ' + modulo + ' SET exportado = NULL, erro = \'1\', codigo_erro = \''+codigo_erro+'\'  WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '"', [], function(){
										
										apprise('<b>ATENÇÃO - ' + descricao_modulo + '!</b> <br /><br />Não foi possível sincronizar o pedido <b>' + pedidos[iteracao] + '.</b><br/><br />'+mensagem, {
											'textYes': 'OK'
										}, function (dado) {
											console.log("SQL ERRO >> " + 'UPDATE ' + modulo + ' SET exportado = NULL, erro = \'1\' WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '"');
											console.log("ERRO PEDIDO " + pedidos[iteracao] + " -> PROXIMA CHAMADA");
											console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
											
											$("#statusSincronismo").prepend("<div class=\"erro\">405 - Erro ao enviar o pedido " + pedidos[iteracao] + ".</div>");
											
											enviarPedidos(pedidos, iteracao + 1, modulo, codigo_modulo, descricao_modulo, callback);
										});
										
									});
								});
							}
							
						},
						error: function(){
							
							sincronizado = 4;
							
							console.log("ERRO NA REQUISIÇÃO AJAX -> PEDIDO " + pedidos[iteracao] + " -> PROXIMA CHAMADA");
							
							//Erro na requisição
							apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível se conectar ao servidor para realizar o envio do pedido/orçamento: <b><p>' + pedidos[iteracao] + '.</p></b><br />Tente novamente mais tarde.', {
								'textYes': 'OK'
							}, function (dado) {
								$("#statusSincronismo").prepend("<div class=\"erro\">405 - Erro ao enviar o pedido " + pedidos[iteracao] + ".</div>");
								
								enviarPedidos(pedidos, iteracao + 1, modulo, codigo_modulo, descricao_modulo, callback);
							});
						}
		    		});

			    });
			});
			
		}
		//
							}
							else
							{
								$("#confirmar_dialog").remove();
								//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
								mensagem('Representante Inativo.');
								
								$("#boxLabelSincronismo").hide();
								$("#labelStatusSincronismo").html("");
								$("#progressbar").hide();
								$("#botoesSincronismo").show();
								$("#statusSincronismo").html("");
								$("#statusSincronismo").hide();
								$("#modulos").show();
								$("#btnVisualizarLogSync").show();
								$("#btnVisualizarModulosSync").show();
							}
							
							
							
						},
						error: function() {
							//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
							mensagem('Não foi possível se conectar com o servidor, verifique sua conexão com a internet. Por favor tente novamente.');
						}
					});
						
						//
					}
				}
			);
		});
	}
	
	
	function exportarDados(codigo_modulo, modulo, descricao_modulo, proxima_funcao, exportar_codigos) 
	{
		
		var selecionado = $('[name="modulos[' + modulo + ']"]').attr('checked');
		
		if(selecionado) {
			
			var where_codigos = '';
			if(exportar_codigos && exportar_codigos.length > 0) {
				where_codigos = 'AND ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\')';
			}
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM ' + modulo + ' WHERE exportado IS NULL OR erro = 1 ' + where_codigos, [], function(x, dados){
					var total = dados.rows.length;
					
					var retorno = new Array();
		    		for(i = 0; i < total; i++) {
		    			retorno[i] = dados.rows.item(i);
		    		}
		    		
		    		$("#statusSincronismo").prepend("<div class=\"info\">104 - Enviando " + descricao_modulo + ".</div>");
		    		
		    		var ajax = $.ajax({
						url: config.ws_url + modulo + '/importar',
						type: 'POST',
						data: {
							retorno: json_encode(retorno)
						},
						success: function(dados) {
							if(dados.sucesso == 'ok') {
								sincronizado = 2;
								
								//Apagando os dados com erro na sessao
								localStorage.setItem('erros_' + modulo, '');
								
								$("#statusSincronismo").prepend("<div class=\"sucesso\">200 - " + descricao_modulo + " enviado com sucesso.</div>");
								
								if(exportar_codigos && exportar_codigos.length > 0) {
									
									// Marcando os dados especificados no "exportar_codigos" como exportados
									db.transaction(function(x) {
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\',  erro = \'0\'  WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\')', [], function(){
											$( "#progressbar" ).progressbar({value: 100});
											finalizarSincronismo(sincronizado, proxima_funcao);
										});
									});
									
								} else {									
									// Marcando todos os dados como exportados
									db.transaction(function(x) {
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\', erro = \'0\' WHERE exportado IS NULL', [], function(){
											$( "#progressbar" ).progressbar({value: 100});
											finalizarSincronismo(sincronizado, proxima_funcao);
										});
									});									
								}
	
							} else if(dados.erro) {
								
								sincronizado = 4;
								console.log(dados.erro);
								var codigos_sessao = [];
								var codigos = [];
								$.each(dados.erro, function(i, codigo) {
									codigos.push("'" + codigo + "'");
									
									if(is_numeric(codigo)) {
										codigos_sessao.push(codigo);
									}
									
								});
								
								//Gravando na sessao dos dados com erro
								localStorage.setItem('erros_' + modulo, codigos_sessao);
								
								
								var descricoes = [];
								$.each(dados.erro_descricao, function(i, descricao) {
									descricoes.push(descricao);
								});
								
								$("#statusSincronismo").prepend("<div class=\"erro\">405 - Erro ao enviar " + descricao_modulo + ".</div>");
								
								db.transaction(function(x) {
								
									//Precisamos marcar quais dados estao com erro, e depois apagar todos os dados que não estao com erro
									
									//Atualiza os pedidoss que deu erro
									x.executeSql('UPDATE ' + modulo + ' SET erro = \'1\' WHERE ' + codigo_modulo + ' IN (' + codigos.join(', ') + ')');
									
									if(exportar_codigos && exportar_codigos.length > 0) {
										// Marcando os dados especificados no "exportar_codigos" como exportados, exceto os pedidos em que ocorreu erro
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\') AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
									} else {
										// Marcando todos os dados como exportados, exceto os pedidos em que ocorreu erro
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE exportado IS NULL AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
									}
									
									apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível sincronizar:<b><br />(' + descricoes.join(', ') + ').</b><br />Entre em contato com TI da sua empresa.', {
										'textYes': 'OK'
									}, function (dado) {
									
										$( "#progressbar" ).progressbar({value: 100});
										finalizarSincronismo(sincronizado, proxima_funcao);
										
									});
	
									
								});
								
							}
						},
						//chamdo 
						error: function (xhr, ajaxOptions, thrownError) {							
							
							apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível se conectar ao servidor para realizar o envio: <b></b><br />Tente novamente mais tarde.', {
								'textYes': 'OK'
							}, function (dado) {
							
								$( "#progressbar" ).progressbar({value: 100});
								
								console.log('Sincronizado: '+ sincronizado );
								console.log('Próxima: '+ proxima_funcao);
								
								
								finalizarSincronismo(sincronizado, proxima_funcao);
								
							});
					    }
						
						
					});
				});
			});
			
		} else {
			
			finalizarSincronismo(sincronizado, proxima_funcao);
			
		}
	}
	
	
	function chamarProximaFuncao(tabela, proxima_funcao) {
		//Salvando Informações da sincronização
		db.transaction(function(x) {
			x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, tabela]);
		});
		
		//Mensagem para o usuário
		$('#'+tabela).after('<span class="ui-icon-white ui-icon-check"></span> ');
		$('#modulos-sincronizados').prepend('<li><b>Sucesso.</b> - '+modulos[tabela]+'</li>').show();
		
		//Proxima funcao
		if(proxima_funcao) {
			
			eval(proxima_funcao);
			
		} else {
			
			finalizarSincronismo(sincronizado);
			
		}

	}
	