$(document).ready(function() {
	// obter pedidos
	
	
	obter_pedidos(sessionStorage['orc_pagina_atual'], sessionStorage['orc_ordem_atual'], sessionStorage['orc_situacao'], sessionStorage['orc_cliente'], sessionStorage['orc_codigo'], sessionStorage['orc_dt_inicial'], sessionStorage['orc_dt_final'], sessionStorage['orc_filial']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_pedidos(sessionStorage['orc_pagina_atual'], $(this).data('campo') + ' DESC', sessionStorage['orc_situacao'], sessionStorage['orc_cliente'], sessionStorage['orc_codigo'], sessionStorage['orc_dt_inicial'], sessionStorage['orc_dt_final'], sessionStorage['orc_filial']);
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_pedidos(sessionStorage['orc_pagina_atual'], $(this).data('campo') + ' ASC', sessionStorage['orc_situacao'], sessionStorage['orc_cliente'], sessionStorage['orc_codigo'], sessionStorage['orc_dt_inicial'], sessionStorage['orc_dt_final'], sessionStorage['orc_filial']);
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	// obter filiais
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT * FROM filiais', [], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=filial]').append('<option value="">Todos</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
				}
				
			}
		);
	});
	
	
	$('select[name=situacao]').val(sessionStorage['orc_situacao']);
	$('input[name=cliente]').val(sessionStorage['orc_cliente']);
	$('input[name=codigo]').val(sessionStorage['orc_codigo']);
	$('input[name=dt_inicial]').val(sessionStorage['orc_dt_inicial']);
	$('input[name=dt_final]').val(sessionStorage['orc_dt_final']);
	
	$('#filtrar').click(function() {
		sessionStorage['orc_pagina_atual'] = 1;
		sessionStorage['orc_filial'] = $('select[name=filial]').val();
		sessionStorage['orc_situacao'] = $('select[name=situacao]').val();
		sessionStorage['orc_cliente'] = $('input[name=cliente]').val();
		sessionStorage['orc_codigo'] = $('input[name=codigo]').val();
		sessionStorage['orc_dt_inicial'] = $('input[name=dt_inicial]').val();
		sessionStorage['orc_dt_final'] = $('input[name=dt_final]').val();
		
		obter_pedidos(sessionStorage['orc_pagina_atual'], sessionStorage['orc_ordem_atual'], sessionStorage['orc_situacao'], sessionStorage['orc_cliente'], sessionStorage['orc_codigo'], sessionStorage['orc_dt_inicial'], sessionStorage['orc_dt_final'], sessionStorage['orc_filial']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['orc_pagina_atual'] = 1;
		sessionStorage['orc_situacao'] = '';
		sessionStorage['orc_filial'] = '';
		sessionStorage['orc_cliente'] = '';
		sessionStorage['orc_codigo'] = '';
		sessionStorage['orc_dt_inicial'] = '';
		sessionStorage['orc_dt_final'] = '';

		$('select[name=situacao]').val(sessionStorage['orc_situacao']);
		$('select[name=filial]').val(sessionStorage['orc_filial']);
		$('input[name=cliente]').val(sessionStorage['orc_cliente']);
		$('input[name=codigo]').val(sessionStorage['orc_codigo']);
		$('input[name=dt_inicial]').val(sessionStorage['orc_dt_inicial']);
		$('input[name=dt_final]').val(sessionStorage['orc_dt_final']);
		
		obter_pedidos(sessionStorage['orc_pagina_atual'], sessionStorage['orc_ordem_atual'], sessionStorage['orc_situacao'], sessionStorage['orc_cliente'], sessionStorage['orc_codigo'], sessionStorage['orc_dt_inicial'], sessionStorage['orc_dt_final'], sessionStorage['orc_filial']);
	});
});

function obter_pedidos(pagina, ordem, situacao, cliente, codigo, dt_inicial, dt_final, filial)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'pedido_data_emissao DESC';
	situacao = situacao ? situacao : '';
	filial = filial ? filial : '';
	cliente = cliente ? cliente : '';
	codigo = codigo ? codigo : '';
	dt_inicial = dt_inicial ? dt_inicial : '';
	dt_final = dt_final ? dt_final : '';
	
	// setar pagina e ordem atual
	sessionStorage['orc_pagina_atual'] = pagina;
	sessionStorage['orc_ordem_atual'] = ordem;
	sessionStorage['orc_situacao'] = situacao;
	sessionStorage['orc_filial'] = filial;
	sessionStorage['orc_cliente'] = cliente;
	sessionStorage['orc_codigo'] = codigo;
	sessionStorage['orc_dt_inicial'] = dt_inicial;
	sessionStorage['orc_dt_final'] = dt_final;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['orc_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'ASC');
		
		$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'DESC');
		
		$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['orc_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	var wheres = '';
	
	/*
	if (situacao)
	{
		wheres += 'AND pedido_status LIKE "%' + situacao + '%"';
	}
	else
	{
		wheres += 'AND pedido_status IS NOT NULL';
	}
	*/
	
	if (filial > 0)
	{
		wheres += ' AND pedido_filial = "' + filial + '"';
	}
	
	if (cliente)
	{
		wheres += ' AND (cliente_nome LIKE "%' + cliente + '%"';
		wheres += ' OR prospect_nome LIKE "%' + cliente + '%")';
	}
	
	if (codigo)
	{
		wheres += ' AND pedido_id_pedido LIKE "%' + codigo + '%"';
	}
	
	if (dt_inicial && dt_inicial != 'undefined')
	{	
		var ex = explode('/', dt_inicial);
		
		wheres += ' AND pedido_data_emissao >= "' + ex[2] + ex[1] + ex[0] + '"';
	}

	if (dt_final && dt_final != 'undefined')
	{
		var ex = explode('/', dt_final);
	
		wheres += ' AND pedido_data_emissao <= "' + ex[2] + ex[1] + ex[0] + '"';
	}

	wheres += ' AND exportado IS NOT NULL';
	
	// exibir pedido e calcular totais
	db.transaction(function(x) {
		x.executeSql(
			'SELECT pedido_id_pedido, SUM((pedido_preco_venda * pedido_quantidade) + ((pedido_preco_venda * pedido_quantidade) * (CAST(produto_ipi AS FLOAT) / 100))) AS valor_total, SUM(pedido_quantidade) AS quantidade_vendida, * FROM orcamentos WHERE pedido_id_pedido > 0 ' + wheres + ' GROUP BY pedido_id_pedido ORDER BY ' + sessionStorage['orc_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
				if (dados.rows.length)
				{
					$('table tbody').empty();
					
					for (i = 0; i < dados.rows.length; i++)
					{
						
						var pedido = dados.rows.item(i);
						
						var pedidos = [];

						pedidos.push(pedido.pedido_id_pedido + '/' + pedido.pedido_filial);
						pedidos.push(pedido.cliente_nome ? pedido.cliente_nome : pedido.prospect_nome);
						
						pedidos.push(protheus_data2data_normal(pedido.pedido_data_emissao));
						pedidos.push(number_format(pedido.quantidade_vendida, 0, ',', '.'));
						pedidos.push(number_format(pedido.valor_total, 3, ',', '.'));
						pedidos.push('<a href="pedidos_espelho.html#' + pedido.pedido_id_pedido + '|' + pedido.pedido_filial + '|O" class="btn btn-large btn-primary">+ Opções</a>');
						
						var html = concatenar_html('<td>', '</td>', pedidos);
					
						$('table tbody').append('<tr>' + html + '</tr>');
						
						alterarCabecalhoTabelaResolucao();
					}
				}
				else
				{
					$('table tbody').html('<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum pedido encontrado.</strong></td></tr>');
				}
			}
		);
		
		// calcular totais
		
		x.executeSql(
			'SELECT COUNT(DISTINCT pedido_id_pedido) AS total, SUM((pedido_preco_venda * pedido_quantidade) + ((pedido_preco_venda * pedido_quantidade) * (CAST(produto_ipi AS FLOAT) / 100))) AS valor_total FROM orcamentos WHERE pedido_id_pedido != "" ' + wheres, [], function(x, dados) {
				var dado = dados.rows.item(0);
				
				$('#total').text(number_format(dado.total, 0, ',', '.'));
				$('#valor_total').text(number_format(dado.valor_total, 3, ',', '.'));
				
				// paginação
				
				$('#paginacao').html('');
				
				var total = ceil(dado.total / 20);
				
				if (sessionStorage['orc_pagina_atual'] > 6)
				{
					$('#paginacao').append('<a href="#" onclick="obter_pedidos(1, \'' + sessionStorage['orc_ordem_atual'] + '\', \'' + sessionStorage['orc_situacao'] + '\', \'' + sessionStorage['orc_cliente'] + '\', \'' + sessionStorage['orc_codigo'] + '\', \'' + sessionStorage['orc_dt_inicial'] + '\', \'' + sessionStorage['orc_dt_final'] + '\', \'' + sessionStorage['orc_filial'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
				}
				
				if (sessionStorage['orc_pagina_atual'] > 1)
				{
					$('#paginacao').append('<a href="#" onclick="obter_pedidos(' + (intval(sessionStorage['orc_pagina_atual']) - 1) + ', \'' + sessionStorage['orc_ordem_atual'] + '\', \'' + sessionStorage['orc_situacao'] + '\', \'' + sessionStorage['orc_cliente'] + '\', \'' + sessionStorage['orc_codigo'] + '\', \'' + sessionStorage['orc_dt_inicial'] + '\', \'' + sessionStorage['orc_dt_final'] + '\', \'' + sessionStorage['orc_filial'] + '\'); return false;">&lt;</a> ');
				}
				
				for (i = intval(sessionStorage['orc_pagina_atual']) - 6; i <= intval(sessionStorage['orc_pagina_atual']) + 5; i++)
				{
					if (i <= 0 || i > total)
					{
						continue;
					}
					
					if (i == sessionStorage['orc_pagina_atual'])
					{
						$('#paginacao').append('<strong>' + i + '</strong> ');
					}
					else
					{
						$('#paginacao').append('<a href="#" onclick="obter_pedidos(' + i + ', \'' + sessionStorage['orc_ordem_atual'] + '\', \'' + sessionStorage['orc_situacao'] + '\', \'' + sessionStorage['orc_cliente'] + '\', \'' + sessionStorage['orc_codigo'] + '\', \'' + sessionStorage['orc_dt_inicial'] + '\', \'' + sessionStorage['orc_dt_final'] + '\', \'' + sessionStorage['orc_filial'] + '\'); return false;">' + i + '</a> ');
					}
				}
				
				if (sessionStorage['orc_pagina_atual'] < total)
				{
					$('#paginacao').append('<a href="#" onclick="obter_pedidos(' + (intval(sessionStorage['orc_pagina_atual']) + 1) + ', \'' + sessionStorage['orc_ordem_atual'] + '\', \'' + sessionStorage['orc_situacao'] + '\', \'' + sessionStorage['orc_cliente'] + '\', \'' + sessionStorage['orc_codigo'] + '\', \'' + sessionStorage['orc_dt_inicial'] + '\', \'' + sessionStorage['orc_dt_final'] + '\', \'' + sessionStorage['orc_filial'] + '\'); return false;">&gt;</a> ');
				}
				
				if (sessionStorage['orc_pagina_atual'] <= total - 6)
				{
					$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_pedidos(' + total + ', \'' + sessionStorage['orc_ordem_atual'] + '\', \'' + sessionStorage['orc_situacao'] + '\', \'' + sessionStorage['orc_cliente'] + '\', \'' + sessionStorage['orc_codigo'] + '\', \'' + sessionStorage['orc_dt_inicial'] + '\', \'' + sessionStorage['orc_dt_final'] + '\', \'' + sessionStorage['orc_filial'] + '\'); return false;">Última Página</a> ');
				}
			}
		);
	});
}
