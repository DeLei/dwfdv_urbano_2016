var unidade_medida_permitidas = [];

$(document).ready(function(){
	$('[name=cliente]').val('');
	$('[name=forma_pagamento]').val('');
	
	$('input[type=text], textarea').keyup(function(){
		$(this).val($(this).val().toUpperCase());
	});
	
	
	$('[name=tipo_venda]').trigger('change');
	$('[name=tipo_frete]').trigger('change');
	
	if(!localStorage.getItem('data_atualizacao_estoque'))
	{
		localStorage.setItem('data_atualizacao_estoque', 0);
	}
	/*
	if(localStorage.getItem('data_atualizacao_estoque') <= time())
	{
		var mensagem_html = 'Deseja atualizar o estoque dos produtos?<br /><br />';
		mensagem_html += '<div style="float:left; padding: 10px 5px 0 0;">Lembrar de atualizar o estoque depois de: </div>';
		mensagem_html += '		<form><select id="data_atualizacao_estoque" name="data_atualizacao_estoque">';
		mensagem_html += '			<option selected value="' + (60 * 60) + '">1 hora</option>';
		mensagem_html += '			<option value="' + (60 * 60 * 4) + '">4 horas</option>';
		mensagem_html += '			<option value="' + (60 * 60 * 24) + '">1 dia</option>';
		mensagem_html += '			<option value="' + (60 * 60 * 24 * 7) + '">1 semana</option>';
		mensagem_html += '		</select></form>';
		
		confirmar(mensagem_html, 
			function () {
				$(this).dialog('close');
				
				//------------------
				//---------------------------------
				
				localStorage.setItem('data_atualizacao_estoque', time() + parseInt($('#data_atualizacao_estoque').val()) );
		
				location.href = "sincronizar.html?produtos=sincronizar";
				
				//---------------------------------
				//------------------
				
				$("#confirmar_dialog").remove();
			});
	}
	*/
	var orcamento = parse_url(location.href).fragment;
	
	// Salvando o Tipo de Sessaão (Orçamento ou pedido)
	if(orcamento == 'O')
	{
		sessionStorage['sessao_tipo'] = 'sessao_orcamento';
		
		// **
		// Atalho de criar orçamento no espelho do prospect (retornar prospect selecionado)
		if(sessionStorage[sessionStorage['sessao_tipo']])
		{
			var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
			
			
			if(sessao_pedido.codigo_prospect)
			{
				$('select[name="tabela_precos"]').removeAttr('disabled');
			}
		}
		
		// Atalho de criar orçamento no espelho do prospect
		// **
		
		// Trocando a descrição de Pedido para Orçamento
		$('.descricao_pedido').html('Orçamento');
		
		// Exibindo as opções de Cliente e Prospect
		$('#cliente_prospect').show();
		
		//CUSTOM - 000177/000870
		$('.tipo_venda').hide();
		salvar_sessao('tipo_venda', '99');
	}
	else
	{
		sessionStorage['sessao_tipo'] = 'sessao_pedido';
		

	}
	

	// Ativando Biblioteca "TABS"
	$( "#tabs" ).tabs();

	// Desativar todos os campos quando clicar em id_informacao_cliente
	$('#id_informacao_cliente').click(function(){
		
		$('#id_adicionar_produtos').addClass('tab_desativada');
		$('#id_outras_informacoes').addClass('tab_desativada');
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select',0);
		
		ativar_tabs();
	});
	
	ativar_tabs();
	
	obter_dados_cliente();
	
	obter_filiais();
	
	obter_tipo_pedido();
	
	// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA
	obter_tipo_venda();
	// FIM  CUSTOM: 177 / 000874 - 04/11/2013
	
	// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-BONIFICACAO
	obter_tipo_bonificacao();
	// FIM CUSTOM: 177 / 000874 - 04/11/2013
	
	if(obter_valor_sessao('filial'))
	{
		obter_clientes(obter_valor_sessao('filial'));		
		obter_prospects(obter_valor_sessao('filial'));
		obter_tabela_precos(obter_valor_sessao('filial'));
		obter_parametros(obter_valor_sessao('filial'));
	}
	
	//Seleciona a forma de pagamento contida na sessão
	if(obter_valor_sessao('forma_pagamento'))
	{
		$('select[name="forma_pagamento"] option[value="' + obter_valor_sessao('forma_pagamento') + '"]').attr('selected', 'selected');
	}
	
	/**
	* 
	* Classe:		select_gravar_sessao
	*
	* Descrição:	Classe utilizada para "select", com a finalizade de gravar na sessão o valor do campo quando o usuário usar o "change"
	*
	*/
	$('.select_gravar_sessao').live('change', function(){
		salvar_sessao($(this).attr('name'), $(this).val());
		
		if($(this).attr('name') == 'filial')
		{
			salvar_sessao($('select[name="tipo_pedido"]').attr('name'), $('select[name="tipo_pedido"]').val());
			
			obter_clientes($(this).val());
			obter_prospects($(this).val());
			obter_tabela_precos(obter_valor_sessao('filial'));
			obter_parametros(obter_valor_sessao('filial'));
		}
		
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA
		if($(this).attr('name') == 'tipo_venda')
		{
			$('[name=condicao_pagamento]').html('<option value="">SELECIONE...</option>');
			if($(this).val() == '02')
			{
				$('p.tipo_bonificacao').removeClass('hidden_input');
				salvar_sessao('tipo_venda', '02');
			}
			else
			{
				salvar_sessao('tipo_venda', $(this).val());
				$('p.tipo_bonificacao').addClass('hidden_input');
				$('[name=tipo_bonificacao]').val('');
				//CUST-CONDICAO-PAGAMENTO-AUTOCOPLETE 
				
				$('[name=condicao_pagamento]').val('');
				$('[name=codigo_condicao_pagamento]').val('');				
				//FIM CUST-CONDICAO-PAGAMENTO-AUTOCOPLETE
				
			}
			obter_forma_pagamento();
			obter_banco();
			obter_condicao_pagamento();
		}
		// FIM CUSTOM: 177 / 000874 - 04/11/2013
		
		if($(this).attr('name') == 'tipo_bonificacao')
		{
			var descricao = $('select[name="tipo_bonificacao"] option[value="'+obter_valor_sessao('tipo_bonificacao')+'"]').data('descricao');
			
			salvar_sessao('descricao_bonificacao', descricao);
		}
		
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TRANSPORTADORA-ALTERACAO
		if($(this).attr('name') == 'tipo_frete')
		{
			var tipo_transporte = $('[name=tipo_transporte]').val();
			var prefixo_tipo_transporte = tipo_transporte.split('', 1);
			
			if($(this).val() == 'C' && prefixo_tipo_transporte == 'M')
			{
				$('p.porto_destino').removeClass('hidden_input');
			}
			else
			{
				$('p.porto_destino').addClass('hidden_input');
				$('[name=porto_destino]').val('');
			}
			
			obter_tipo_transporte();
			regra_tabela_precos();
			obter_condicao_pagamento();
		}
		
		if($(this).attr('name') == 'tipo_transporte')
		{
			//Tipo de transporte maritmo
			var tipo_transporte = $(this).val().split('', 1);
			if(tipo_transporte == 'M' && $('[name=tipo_frete]').val() == 'C')
			{
				$('p.porto_destino').removeClass('hidden_input');
				
				var tipo_venda = $('[name=tipo_venda]').val();
				if(!in_array(tipo_venda, ['02', '20', '30']) && obter_valor_sessao('tipo_venda')){
					$('select[name="banco"] option[value="10"]').attr('selected', 'selected');
					$('select[name="banco"]').attr('disabled', 'disabled');
					$('select[name="banco"]').data('banco_fixo',true);
					$('#aviso_banco').html('O banco 10 é fixo para este tipo de transporte.');
				}
			}
			else
			{
				$('p.porto_destino').addClass('hidden_input');
				$('[name=porto_destino]').val('');
				salvar_sessao('porto_destino', null);
				
				var tipo_venda = $('[name=tipo_venda]').val();
				if(!in_array(tipo_venda, ['02', '20', '30']) && obter_valor_sessao('tipo_venda')){
					$('select[name="banco"] option[value=""]').attr('selected', 'selected');
					$('#aviso_banco').html('');
					$('select[name="banco"]').data('banco_fixo', false);
				}
			}
			
			salvar_sessao('banco', $('select[name="banco"]').val());
			regra_tabela_precos();
		}
		
		if($(this).attr('name') == 'porto_destino')
		{
			regra_tabela_precos();
		}
		
		
		if($(this).attr('name') == 'forma_pagamento')
		{
		
			obter_bancos($(this).val());
		
			var tipo_venda = $('[name=tipo_venda]').val();
			if(!in_array(tipo_venda, ['02', '20', '30']) && obter_valor_sessao('tipo_venda')){
				if($(this).val() == 'CH')
				{
					
					$('select[name="banco"] option[value="84"]').attr('selected', 'selected');
					$('select[name="banco"]').attr('disabled', 'disabled');
					$('#aviso_banco').html('O banco 84 é fixo para este tipo de transporte.');
					$('select[name="banco"]').data('banco_fixo',true);
				}
				else if($(this).val() == 'ES')
				{
					
					$('select[name="banco"] option[value="85"]').attr('selected', 'selected');
					$('select[name="banco"]').attr('disabled', 'disabled');
					$('#aviso_banco').html('O banco 85 é fixo para este tipo de transporte.');
					$('select[name="banco"]').data('banco_fixo',true);
				}
				else
				{
				
					
					
					
					$('select[name="banco"] option[value="84"]').hide();
					$('select[name="banco"] option[value="85"]').hide();
					
					$('select[name="banco"] option[value=""]').attr('selected', 'selected');
					
					$('#aviso_banco').html('');
					
					
					
					$('select[name="banco"]').data('banco_fixo',false);
					if(obter_valor_sessao('banco'))
					{
						$('select[name="banco"]').val(obter_valor_sessao('banco'));
					}
				}
			}
			salvar_sessao('banco', $('select[name="banco"]').val());
		}
		// FIM CUSTOM: 177 / 000874 - 04/11/2013
	});
	
	// CUSTOM: 177 / 000874 - 06/11/2013 - Localizar:CUST-TRANSPORTADORA-ALTERACAO
	obter_tipo_frete();
	obter_tipo_transporte();	
	obter_tipo_porto_destino();	
	// FIM CUSTOM: 177 / 000874 - 06/11/2013
	
	obter_tabela_precos();
	
	//obter_bancos();
	
	obter_condicao_pagamento();
	
	remover_disabled();
	
	/**
	* 
	* ID:			trocar_cliente
	*
	* Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
	*
	*/
	$('#trocar_cliente').live('click', function(){
		
		salvar_sessao('dados_cliente', null);
		salvar_sessao('descricao_cliente', null);
		salvar_sessao('codigo_cliente', null);
		salvar_sessao('loja_cliente', null);
		
		
		$('input[name="cliente"]').val("");
		$('input[name="codigo_cliente"]').val("");
		$('input[name="loja_cliente"]').val("");
		
		
		//FIM CUSTOM
		
		salvar_sessao('tabela_preco', null);
		salvar_sessao('tabela_fixa', null);
		$('select[name="tabela_precos"]').val("");
		$('#aviso_tabela_preco').html('').removeData('tabela_fixa');
		
		
		//Rotina tipo de frete
		salvar_sessao('tipo_frete', null);
		salvar_sessao('tipo_transporte', null);
		$('select[name="tipo_frete"]').val("");
		$('select[name="tipo_transporte"]').val("");
		

		//rotina porto destino		
		salvar_sessao('porto_destino', null);
		$('select[name="porto_destino"]').val("");
		
		//$('select[name="banco"]').val("");
		//salvar_sessao('banco', null);
		
		//CUSTOM - 21/11/213
		/*Limpa a condição de pagamento fixa caso não tenha sido fixada pelo tipo de venda (BONIFICACAO) */
		$('#aviso_condicao_pagamento').html('').removeData('condicao_fixa');
		salvar_sessao('condicao_pagamento', null);
		//CUST-CONDICAO-PAGAMENTO-AUTOCOPLETE
		$('[name="condicao_pagamento"]').val("");
		$('[name="codigo_condicao_pagamento"]').val("");
		//FIM CUST-CONDICAO-PAGAMENTO-AUTOCOPLETE
		$(this).hide();
		$('#info_cli').hide();
		
		
		$('.tipo_transporte').addClass('hidden_input');
		$('.porto_destino').addClass('hidden_input');
		
		$('.select_gravar_sessao').attr('disabled', 'disabled');
		$('[name=filial]').removeAttr('disabled');
		$('[name=tipo_venda]').removeAttr('disabled');
		$('[name=tipo_bonificacao]').removeAttr('disabled');
		$('[name=cliente_prospect]').removeAttr('disabled');
	});
	
	
	/**
	* 
	* ID:			trocar_prospect
	*
	* Descrição:	Utilizado para apagar todos os dados do prospect na sessão, e forçar que o representante selecione outro prospect
	*
	*/
	$('#trocar_prospect').live('click', function(){
		
		salvar_sessao('descricao_prospect', null);
		salvar_sessao('codigo_prospect', null);
		salvar_sessao('loja_prospect', null);
		
		$('input[name="prospect"]').val("");
		$('input[name="codigo_prospect"]').val("");
		$('input[name="loja_prospect"]').val("");
		
		//-----------------------------------------------
		
//FIM CUSTOM
		
		salvar_sessao('tabela_preco', null);
		salvar_sessao('tabela_fixa', null);
		$('select[name="tabela_precos"]').val("");
		$('#aviso_tabela_preco').html('').removeData('tabela_fixa');
		
		
		//Rotina tipo de frete
		salvar_sessao('tipo_frete', null);
		salvar_sessao('tipo_transporte', null);
		$('select[name="tipo_frete"]').val("");
		$('select[name="tipo_transporte"]').val("");
		

		//rotina porto destino		
		salvar_sessao('porto_destino', null);
		$('select[name="porto_destino"]').val("");
		
		$('select[name="banco"]').val("");
		salvar_sessao('banco', null);
		
		//CUSTOM - 21/11/213
		/*Limpa a condição de pagamento fixa caso não tenha sido fixada pelo tipo de venda (BONIFICACAO) */
		$('#aviso_condicao_pagamento').html('').removeData('condicao_fixa');
		salvar_sessao('condicao_pagamento', null);
		//CUST-CONDICAO-PAGAMENTO-AUTOCOPLETE
		$('[name="condicao_pagamento"]').val("");
		$('[name="codigo_condicao_pagamento"]').val("");
		//FIM CUST-CONDICAO-PAGAMENTO-AUTOCOPLETE
		//-----------------------------------------------
		
		$(this).hide();
		$('#info_pro').hide();
		
		
		$('.tipo_transporte').addClass('hidden_input');
		$('.porto_destino').addClass('hidden_input');
		
		
		$('.select_gravar_sessao').attr('disabled', 'disabled');
		$('[name=filial]').removeAttr('disabled');
		$('[name=tipo_venda]').removeAttr('disabled');
		$('[name=tipo_bonificacao]').removeAttr('disabled');
		$('[name=cliente_prospect]').removeAttr('disabled');
	});
	
	
	$('select[name="cliente_prospect"]').live('change', function(){
			
		if($(this).val() == 'P')
		{
			$('#campo_cliente').hide();
			$('#campo_prospect').show();
			
			//Limpando Dados do Cliente e Prospect
			$('#trocar_cliente').click();
			$('#trocar_cliente_entrega').click();
			
		}
		else
		{
			$('#campo_prospect').hide();
			$('#campo_cliente').show();
			
			//Limpando Dados do Cliente e Prospect
			$('#trocar_prospect').click();
		}
	});

	
	/**
	* 
	* CLASSE:		avancar
	*
	* Descrição:	Utilizado para validar os campos, se estiver tudo correto, passar para a proxima ABA
	*
	*/
	
	
	
	$('#avancar_passo_1').live('click', function(e){
		e.preventDefault();
		
		//unidade de medidas permitidas de acordo com o tipo de venda
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM unidade_medidas_venda', [], function(x, dados) {

					unidade_medida_permitidas = [];
					
					var total = dados.rows.length;
					
					for(i = 0; i < total; i++)
					{
						var dado = dados.rows.item(i);
						if (!(dado.tipo_pedido in unidade_medida_permitidas)) {
							unidade_medida_permitidas[dado.tipo_pedido] = [];
						}
						unidade_medida_permitidas[dado.tipo_pedido].push(dado.unidade);
					}
					
				}
			);
		});
		
		if(sessionStorage[sessionStorage['sessao_tipo']])
		{
			var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		}
		else
		{
			var sessao_pedido = [];
		}
		
		
		var tipo_transporte = $('[name=tipo_transporte]').val();
		var prefixo_tipo_transporte = tipo_transporte.split('', 1);
		
		if(!sessao_pedido.filial)
		{
			mensagem('Selecione uma <strong>Filial</strong>.');
		}
		else if(!sessao_pedido.tipo_pedido)
		{
			mensagem('Selecione um <strong>Tipo de Pedido</strong>.');
		}
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA
		else if(!sessao_pedido.tipo_venda)
		{
			mensagem('Selecione um <strong>Tipo de Venda</strong>.');
		}
		// FIM CUSTOM: 177 / 000874 - 04/11/2013
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-BONIFICACAO
		else if(sessao_pedido.tipo_venda == '02'  && !sessao_pedido.tipo_bonificacao)
		{
			mensagem('Selecione um <strong>Tipo de Bonificação</strong>.');
		}
		// FIM CUSTOM: 177 / 000874 - 04/11/2013
		else if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_pedido')
		{
			mensagem('Selecione um <strong>Cliente</strong>.');
		}
		else if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && (sessao_pedido.cliente_prospect == 'C' || sessao_pedido.cliente_prospect == null))
		{
			mensagem('Selecione um <strong>Cliente</strong>.');
		}
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-BONIFICACAO
		else if(!sessao_pedido.tipo_frete)
		{
			mensagem('Selecione um <strong>Tipo de Frete</strong>.');
		}
		else if(sessao_pedido.tipo_frete == 'C' && !sessao_pedido.tipo_transporte)
		{
			mensagem('Selecione um <strong>Tipo de Transporte</strong>.');
		}
		else if(sessao_pedido.tipo_frete == 'C' && !sessao_pedido.porto_destino && prefixo_tipo_transporte == 'M')
		{
			mensagem('Selecione um <strong>Porto de Destino</strong>.');
		}
		else if(!sessao_pedido.banco)
		{
			mensagem('Selecione um <strong>Banco</strong>.');
		}
		else if(!sessao_pedido.codigo_prospect && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && sessao_pedido.cliente_prospect == 'P')
		{
			mensagem('Selecione um <strong>Prospect</strong>.');
		}
		else if(!sessao_pedido.tabela_precos)
		{
			mensagem('Selecione uma <strong>Tabela de Preços</strong>.');
		}
		else if(!sessao_pedido.condicao_pagamento)
		{
			mensagem('Selecione uma <strong>Condição de Pagamento</strong>.');
		}
		else if(!sessao_pedido.forma_pagamento)
		{
			mensagem('Selecione uma <strong>Forma de Pagamento</strong>.');
		}
		else
 		{

			salvar_dados_forma_pagamento(sessao_pedido.condicao_pagamento);
			
			salvar_dados_cliente(sessao_pedido.codigo_cliente, sessao_pedido.loja_cliente);

			salvar_dados_prospect(sessao_pedido.codigo_prospect, sessao_pedido.loja_prospect);
			
			salvar_dados_porto_destino(sessao_pedido.porto_destino);
			
			salvar_dados_tipo_transporte(sessao_pedido.tipo_transporte, function(){
				// Ativando Botao "Adicionar Produtos"
				$('#id_adicionar_produtos').removeClass('tab_desativada');
				
				ativar_tabs();
				
				//Chamando as funções do arquivo adicionar produtos
				adicionar_produtos();
				
				// Acionando (Click) botao "Adicionar Produtos"
				$("#id_adicionar_produtos").click();
			});
		}
	});
	
	
	$('.cancelar').click(function(e){
		e.preventDefault();
		
		
		confirmar('Deseja cancelar esse pedido?', 
			function () {
				$(this).dialog('close');
		
				//-----------------
				
				sessionStorage[sessionStorage['sessao_tipo']] = '';
				location.reload();
				
				//-----------------
				
				$("#confirmar_dialog").remove();
				
			}
		);
	});
	
});


/**
* Metódo:		ativar_tabs
* 
* Descrição:	Função Utilizada ativar a biblioteca, e desativar tabs com a classe tab_desativada
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function ativar_tabs()
{
	// Desativando tabs com classe tab_desativada
	var tabs_desativar = [];
			
	$( "#tabs ul li a" ).each(function(e, i){
		var classname = i.className;
		
		if(classname === 'tab_desativada')
		{
			tabs_desativar.push(e);
		}
	});
	
	$("#tabs").tabs({disabled: tabs_desativar});

}


/**
* Metódo:		obter_filiais
* 
* Descrição:	Função Utilizada para retornar as filiais
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_filiais()
{
	//Obtem somente as filiais em que o representante/clientes possuem tabela de preço
	var sql = '';
	sql += 'SELECT filial_saida ';
	sql += 'FROM clientes ';
	sql += 'JOIN tabelas_preco ON (tabelas_preco.codigo = clientes.tabela_preco OR tabelas_preco.vinculo_representante = "' + localStorage.getItem('codigo') + '") ';
	sql += 'WHERE (clientes.codigo_representante = "' + localStorage.getItem('codigo') + '" OR clientes.codigo_representante2 = "' + localStorage.getItem('codigo') + '") ';
	sql += 'GROUP BY filial_saida';
	
	db.transaction(function(x) {
		x.executeSql(sql, [], function(x, dados){
			var total = dados.rows.length;
			var filiais_permitidas = new Array();
			if(total > 0)
			{
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					
					filiais_permitidas[i] = '"' + dado.filial_saida + '"';
				}
			}
			filiais_permitidas = implode(', ', filiais_permitidas);
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM filiais WHERE codigo IN (' + filiais_permitidas + ')', [], function(x, dados) {
					var total = dados.rows.length;
					
					$('select[name=filial]').append('<option value="">SELECIONE...</option>');
					
					for(i = 0; i < total; i++)
					{
						var dado = dados.rows.item(i);
						$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
					}
					
					// Selecionar campo se existir na sessão
					if(obter_valor_sessao('filial'))
					{
						$('select[name="filial"] option[value="' + obter_valor_sessao('filial') + '"]').attr('selected', 'selected');
					}
					
				});
			});
			
		})
	});
}



/**
* Metódo:		obter_tabela_precos
* 
* Descrição:	Função Utilizada para retornar Tabelas Preços
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tabela_precos(filial)
{
	$('select[name=tabela_precos]').html('');
	var where = " WHERE codigo > 0 ";
	
	if(info.empresa)
	{
		where += " AND empresa = '" + info.empresa + "' ";
	}

	//CUSTOM
	//Descrição: Será exibida apenas as tabelas  de preço quando a filial de saida for igual a selecionada durante a inclusão do pedido/Orcamento
	//where += " AND filial_saida = '"+filial+"' ";
	//FIM CUSTOM 
	
	//DATA INICIO DE VIGENCIA DA TABELA DE PREÇO
	where += " AND vigencia_inicio <= '" + date('Ymd') + "'";
	
	//DATA FINAL DE VIGENCIA DA TABELA DE PREÇO
	where += " AND (vigencia_final >= '" + date('Ymd') + "' OR vigencia_final = '')";
	
	
	where += " AND filial_saida = '" + obter_valor_sessao('filial') + "' ";
	$('select[name=tabela_precos]').append('<option value="">SELECIONE...</option>');
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM tabelas_preco ' + where, [], function(x, dados) {
			
				var total = dados.rows.length;
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					/*Verifica se há alguma tabela de preço vinculada ao Representante*/
					$('select[name=tabela_precos]').append('<option value="' + dado.codigo + '" data-condicao_pagamento="' + dado.condicao_pagamento + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
				}
				
				rotina_tabelas_preco(filial);
				$('[name=tipo_venda]').trigger('change');
				$('[name=tipo_frete]').trigger('change');
			}
		);
	});
}


/**
* Metódo:		obter_condicao_pagamento
* 
* Descrição:	Função Utilizada para retornar Condições de pagamento
* 
* Data:			09/10/2013
* Modificação:	09/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_condicao_pagamento()
{
	
	$('#aviso_condicao_pagamento').html('').removeData('condicao_fixa');	
	$('[name=condicao_pagamento]').val();
	$('[name=codigo_condicao_pagamento]').val();
	$('#trocar_condicao_pagamento').hide();
	
	
	var dados_cliente = obter_valor_sessao('dados_cliente');
	
	if(obter_valor_sessao('tipo_venda') == '02') //Bonificação
	{
		obter_condicao_fixa('bonificacao', '003');
	}
	else if(obter_valor_sessao('tipo_venda') == '30') //Amostra
	{
		obter_condicao_fixa('tipo_venda', '003');
	}
	else if(obter_valor_sessao('tipo_venda') == '20') //Troca
	{
		obter_condicao_fixa('tipo_venda', '020');
	}
	else if(dados_cliente.condicao_pagamento) //Cliente
	{
		obter_condicao_fixa('cliente', dados_cliente.condicao_pagamento);
	}
	else
	{
		if(obter_valor_sessao('filial') && obter_valor_sessao('tabela_precos'))
		{
			// CUSTOM - 177 - 000874 - 08/11/2013
			var where = '';
			where += 'WHERE filial = "' + obter_valor_sessao('filial') + '"';
			where += " AND codigo_tabela = '" + obter_valor_sessao('tabela_precos') + "'";
			
			// FIM CUSTOM - 177 - 000874 - 08/11/2013
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM formas_pagamento ' + where, [], function(x, dados) {
					var condicoes_pagamento = new Array(); 					
					//CUST-CONDICAO-PAGAMENTO-AUTOCOPLETE
					$('[name=condicao_pagamento]').val();
					$('[name=codigo_condicao_pagamento]').val();
					
					
					var total = dados.rows.length;
					
					for(i = 0; i < total; i++)
					{
						var dado = dados.rows.item(i);	
						
						condicao_pagamento = {
								codigo: dado.codigo_condicao_pagamento,
								descricao: dado.descricao,
								label: 	dado.codigo_condicao_pagamento +' - '+ dado.descricao
								
						}
						
						condicoes_pagamento.push(condicao_pagamento);
					}
					
					obter_condicao_pagamento_autocomplete(condicoes_pagamento);
					if(obter_valor_sessao('condicao_pagamento'))
					{
						condicao_selecionada = 	 condicoes_pagamento.filter(function( obj ) {
							  return obj.codigo == obter_valor_sessao('condicao_pagamento');
						});
						
						$('input[name=condicao_pagamento]').val(condicao_selecionada[0].label);
						$('input[name=codigo_condicao_pagamento]').val(condicao_selecionada[0].codigo);	
						$('input[name=condicao_pagamento]').attr('disabled', 'disabled');
						$('#trocar_condicao_pagamento').show();	
						obter_forma_pagamento();
					}
				});
			});
		}
		else
		{
			$('[name="condicao_pagamento"]').val('');
			
			//$('select[name="condicao_pagamento"] option[value="' + obter_valor_sessao('condicao_pagamento') + '"]').attr('selected', 'selected');
		}
	}
}



function obter_condicao_fixa(regra, condicao)
{
	
	
	
	if(regra == 'bonificacao')
	{
		if(obter_valor_sessao('tipo_venda') == '02')
		{
			
			/*$('select[name=condicao_pagamento]').append('<option value="003">003 - BONIFICACAO</option>');
			$('select[name="condicao_pagamento"] option[value="' + condicao + '"]').attr('selected', 'selected');
			$('select[name=condicao_pagamento]').attr('disabled', 'disabled');
			*/
			$('#aviso_condicao_pagamento').html('A condição de pagamento ' + condicao + ' é fixa para este tipo de venda.').data('condicao_fixa','tipo_venda');
			condicao_pagamento = {
			                    	   codigo: '003',
			                    	   descricao: 'BONIFICACAO',
			                    	   label: '003 - BONIFICACAO'
			                       };
			
			fixar_condicao_pagamento(condicao_pagamento);	
			
			salvar_sessao('condicao_pagamento', condicao);
			obter_forma_pagamento();
		}
	}
	else
	{
		var where = 'WHERE codigo = "' + condicao + '"';
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM condicoes_pagamento ' + where, [], function(x, dados) {
				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);
					
					$('select[name="condicao_pagamento"]').append('<option value="' + dado.codigo + '">' + dado.descricao + '</option>');
					$('select[name="condicao_pagamento"] option[value="' + condicao + '"]').attr('selected', 'selected');
					if(regra == 'cliente')
					{
						$('#aviso_condicao_pagamento').html('A condição de pagamento ' + condicao + ' é fixa para este cliente.').data('condicao_fixa','tipo_venda');
					}
					else if(regra == 'tipo_venda')
					{
						$('#aviso_condicao_pagamento').html('A condição de pagamento ' + condicao + ' é fixa para este tipo de venda.').data('condicao_fixa','tipo_venda');
					}
					
					
					condicao_fixa = {
										codigo: dado.codigo,
										descricao: dado.descricao,
										label:	dado.codigo + ' - '+ dado.descricao
									};
					fixar_condicao_pagamento(condicao_fixa);	
					
					salvar_sessao('condicao_pagamento', condicao);
					obter_forma_pagamento();
				}
			})
		});
	}	
}


/**
* Metódo:		fixar_condicao_pagamento
* 
* Descrição:	Função Utilizada para fixar a condição de pagamento no autocomplete 
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function fixar_condicao_pagamento(condicao_fixa){
	$('#trocar_condicao_pagamento').hide();
	$('input[name=condicao_pagamento]').val(condicao_fixa.label);
	$('input[name=condicao_pagamento]').attr('disabled', 'disabled');
	$('input[name=codigo_condicao_pagamento]').val(condicao_fixa.codigo);	
}

function obter_condicao_pagamento_autocomplete(condicoes_pagamento){
	/*Auto complete **/
	$('#aviso_condicao_pagamento').html('').removeData('condicao_fixa');
	$('#aviso_condicao_pagamento').html('<span class="class_info">* Digite pelo menos <b>2</b> caracteres do <b>CÓDIGO</b> ou <b>DESCRIÇÂO</b> da condição de pagamento.</span>');
	
	$('input[name=condicao_pagamento]').removeAttr('disabled');
	
	$('input[name=condicao_pagamento]').autocomplete({
		minLength: 2,
		source: function(request, response) {
					
			var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
			results = ($.grep(condicoes_pagamento, function(value) {										
				return matcher.test(value.label);		
				
			}));
			
			
	       response(results);
		
		},
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
			
			// Bloquear campo quando selecionar cliente
			$('input[name=condicao_pagamento]').attr('disabled','disabled');
			$('#trocar_condicao_pagamento').show();
			
							
			$('input[name=condicao_pagamento]').val(ui.item.label);
			$('input[name=codigo_condicao_pagamento]').val(ui.item.codigo);
			
			salvar_sessao('condicao_pagamento',ui.item.codigo);
			obter_forma_pagamento();
			return false;			
			
		}
	});
	
	//$('input[name=condicao_pagamento]').removeAttr('disabled');
	
	/*Fim autocomplete*/

}



/**
* 
* ID:			trocar_cliente
*
* Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
*
*/
//CUST-CONDICAO-PAGAMENTO-AUTOCOPLETE
$('#trocar_condicao_pagamento').live('click', function(){
	
	salvar_sessao('condicao_pagamento', null);
	$('#aviso_condicao_pagamento').html('').removeData('condicao_fixa');
	
	
	$('[name="condicao_pagamento"]').val("");
	$('[name="codigo_condicao_pagamento"]').val("");
	
	$('#trocar_condicao_pagamento').hide();
	obter_condicao_pagamento();

});
//FIM CUST-CONDICAO-PAGAMENTO-AUTOCOPLETE

/**
* Metódo:		obter_tipo_pedido
* 
* Descrição:	Função Utilizada para retornar Tipos de pedido
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tipo_pedido()
{

	/* Não será adicionada a opção selecione para que seja sempre obtido o valor :
	 * '*' para orçamentos e 'N' para Pedidos.
	 * 
	$('select[name=tipo_pedido]').append('<option value="">Selecione...</option>');
	*/
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento')
	{
		$('select[name=tipo_pedido]').append('<option value="*">Orçamento</option>');
	}
	else
	{
		$('select[name=tipo_pedido]').append('<option value="N">Venda Normal</option>');
	}
	
	// Selecionar campo se existir na sessão
	if(obter_valor_sessao('tipo_pedido'))
	{		
		$('select[name="tipo_pedido"] option[value="' + obter_valor_sessao('tipo_pedido') + '"]').attr('selected', 'selected');
	}
}

/**
* Metódo:		obter_tipo_venda
* 
* Descrição:	Função Utilizada para retornar Tipos de venda
* 				CUSTOM 000177/000870
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tipo_venda()
{

	$('select[name=tipo_venda]').append('<option value="">SELECIONE...</option>');
	
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento')
	{
		$.each( lista_tipo_venda, function( key, tipo_venda ) {
			if(tipo_venda.valor != '02'){			
				$('select[name=tipo_venda]').append('<option value="'+tipo_venda.valor+'">'+tipo_venda.descricao+'</option>');
			}
		});	
	}
	else
	{
		$.each( lista_tipo_venda, function( key, tipo_venda ) {
			$('select[name=tipo_venda]').append('<option value="'+tipo_venda.valor+'">'+tipo_venda.descricao+'</option>');
		});		
	}
	
	// Selecionar campo se existir na sessão
	if(obter_valor_sessao('tipo_venda'))
	{		
		$('select[name="tipo_venda"] option[value="' + obter_valor_sessao('tipo_venda') + '"]').attr('selected', 'selected');
	}	
}
// FIM CUSTOM: 177 / 000874 - 04/11/2013


/**
* Metódo:		obter_tipo_bonificacao
* 
* Descrição:	Função Utilizada para retornar Tipos de Bonificação
* 
* Data:			04/11/2013
* Modificação:	04/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
	function obter_tipo_bonificacao()
	{
		$('select[name=tipo_bonificacao]').append('<option value="">SELECIONE...</option>');
		
		if(sessionStorage['sessao_tipo'] == 'sessao_orcamento')
		{
			$('select[name=tipo_bonificacao]').append('<option value="*">Orçamento</option>');
		}
		else
		{
			$.each( lista_tipo_bonificacao, function( key, tipo_bonificacao ) {
				$('select[name=tipo_bonificacao]').append('<option value="'+tipo_bonificacao.valor+'" data-descricao="'+ tipo_bonificacao.descricao_protheus+'">'+tipo_bonificacao.descricao+'</option>');
			});		
		}
		
		// Selecionar campo se existir na sessão
		if(obter_valor_sessao('tipo_bonificacao'))
		{		
			$('select[name="tipo_bonificacao"] option[value="' + obter_valor_sessao('tipo_bonificacao') + '"]').attr('selected', 'selected');
		}
		
	}


// FIM CUSTOM: 177 / 000874 - 04/11/2013

//CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-BANCO

	/**
	* Metódo:		obter_bancos
	* 
	* Descrição:	Função Utilizada para retornar os Bancos
	* 
	* Data:			07/11/2013
	* Modificação:	07/11/2013
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_bancos(codigo_forma_pagamento)
	{
		$('select[name=banco]').html('');
		$('select[name=banco]').append('<option value="">SELECIONE...</option>');
		
		
			if(sessionStorage['sessao_tipo'] == 'sessao_orcamento')
			{
				$.each( lista_banco, function( key, banco ) {
					if( codigo_forma_pagamento == 'BL' &&   banco.valor != '85'  && banco.valor != '84'  ){				
						$('select[name=banco]').append('<option value="'+banco.valor+'">'+banco.descricao+'</option>');
					}else if( codigo_forma_pagamento != 'BL') {
						$('select[name=banco]').append('<option value="'+banco.valor+'">'+banco.descricao+'</option>');
					}
				});	
			}
			else
			{
				$.each( lista_banco, function( key, banco ) {	
					if(codigo_forma_pagamento == 'BL' &&  banco.valor != '85'  && banco.valor != '84'  ){				
						$('select[name=banco]').append('<option value="'+banco.valor+'">'+banco.descricao+'</option>');
					}else if( codigo_forma_pagamento != 'BL') {
						$('select[name=banco]').append('<option value="'+banco.valor+'">'+banco.descricao+'</option>');
					}
				});		
			}
		
		// Selecionar campo se existir na sessão
		if(obter_valor_sessao('banco'))
		{
			$('select[name="banco"]').val(obter_valor_sessao('banco'));
		}
	}
// FIM CUSTOM: 177 / 000874 - 04/11/2013

/**
* Metódo:		obter_clientes
* 
* Descrição:	Função Utilizada para retornar Todos os Clientes
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_clientes(filial)
{
	if(info.empresa)
	{
		var wheres = " WHERE empresa = '" + info.empresa + "'";
	}
	if(filial)
	{
		wheres += " AND (filial = '" + filial + "' OR filial = '')";
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM clientes ' + wheres, [], function(x, dados) {
				if (dados.rows.length)
				{
					var clientes = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						clientes.push(
								{ 
									label: dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf
									, codigo: dado.codigo
									, loja: dado.loja
									, cep: dado.cep
									, tabela_preco: dado.tabela_preco
									, desconto: dado.desconto
									, condicao_pagamento: dado.condicao_pagamento
									, situacao: dado.situacao // CUSTOM: 177 / 000870 - 24/10/2013 - Localizar: CUST-PEDIDO-CLIENTE-BLOQUEADO
									, situacao_sintegra: dado.situacao_sintegra // CUSTOM: 177 / 000870 - 24/10/2013 - Localizar: CUST-PEDIDO-CLIENTE-SITUACAO-SINTEGRA
									, risco: dado.risco // CUSTOM 177 - 000870 - 12/11/2013 - Localizar CUST-RISCO 
										
									
									}
								);
					}
					
					buscar_clientes(clientes);
					
					// Selecionar campo se existir na sessão
					if(obter_valor_sessao('codigo_cliente'))
					{
						$('input[name=cliente]').val(obter_valor_sessao('descricao_cliente'));
						$('input[name=codigo_cliente]').val(obter_valor_sessao('codigo_cliente'));
						$('input[name=loja_cliente]').val(obter_valor_sessao('loja_cliente'));
						
						exibir_informacoes_cliente(obter_valor_sessao('codigo_cliente'), obter_valor_sessao('loja_cliente'));
						
						// Bloquear campo quando selecionar cliente
						$('input[name=cliente]').attr('disabled', 'disabled');
						$('#trocar_cliente').show();
					}
				
				}
			}
		);
	});

}



/**
* Metódo:		obter_prospects
* 
* Descrição:	Função Utilizada para retornar Todos os Prospects
* 
* Data:			13/11/2013
* Modificação:	13/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_prospects(filial)
{
	var wheres = "WHERE status = 'A' OR status IS NULL";
	
	
	if(info.empresa)
	{
		wheres += " AND empresa = '" + info.empresa + "' ";
	}
	
	if(filial)
	{
		wheres += " AND (filial = '" + filial + "' OR filial = '')";
	}

	db.transaction(function(x) {
		x.executeSql('SELECT * FROM prospects ' + wheres, [], function(x, dados) {
				if (dados.rows.length)
				{
					var prospects = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						prospects.push({ label: dado.codigo + '/' + dado.codigo_loja + ' - ' + dado.nome + ' - ' + dado.cgc, codigo: dado.codigo, loja: dado.codigo_loja});
					}
					
					buscar_prospects(prospects);
					
					// Selecionar campo se existir na sessão
					if(obter_valor_sessao('codigo_prospect'))
					{
						$('input[name=prospect]').val(obter_valor_sessao('descricao_prospect'));
						$('input[name=codigo_prospect]').val(obter_valor_sessao('codigo_prospect'));
						$('input[name=loja_prospect]').val(obter_valor_sessao('loja_prospect'));
						
						exibir_informacoes_prospect(obter_valor_sessao('codigo_prospect'), obter_valor_sessao('loja_prospect'));
						
						// Bloquear campo quando selecionar cliente
						$('input[name=prospect]').attr('disabled', 'disabled');
						$('#trocar_prospect').show();
						
						
						//----------------
						// Selecionar a opção prospect se existir na sessao
						$('select[name="cliente_prospect"] option[value="P"]').attr('selected', 'selected');
						$('#campo_cliente').hide();
						$('#campo_prospect').show();
						
						if(obter_valor_sessao('tabela_precos'))
						{
							$('select[name="tabela_precos"]').removeAttr('disabled');
						}
						
					}
				
				}
			}
		);
	});
	
	
	
	//--------------
	// Selecionar campo se existir na sessão
	if(obter_valor_sessao('codigo_prospect'))
	{
		$('select[name="filial"] option[value="' + obter_valor_sessao('filial') + '"]').attr('selected', 'selected');
	}

}



/**
* Metódo:		buscar_prospects
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_prospects(prospects)
{
	$('input[name=prospect]').autocomplete({
		minLength: 3,
		source: prospects,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=prospect]').val(ui.item.label);
			$('input[name=codigo_prospect]').val(ui.item.codigo);
			$('input[name=loja_prospect]').val(ui.item.loja);
			
			salvar_sessao('descricao_prospect', ui.item.label);
			salvar_sessao('codigo_prospect', ui.item.codigo);
			salvar_sessao('loja_prospect', ui.item.loja);
			salvar_sessao('desconto_prospect', ui.item.desconto);
			
			
			//--------------
			
			exibir_informacoes_prospect(ui.item.codigo, ui.item.loja);
			
			//--------------
			
			//$('select[name=tabela_precos]').removeAttr('disabled', '');
			
			// Bloquear campo quando selecionar prospect
			$('input[name=prospect]').attr('disabled', 'disabled');
			$('#trocar_prospect').show();

			return false;
		}
	});
}



/**
* Metódo:		buscar_clientes
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_clientes(clientes)
{
	
	$('input[name=cliente]').autocomplete({
		minLength: 3,
		source: clientes,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
			
			// Bloquear campo quando selecionar cliente
			$('input[name=cliente]').attr('disabled', 'disabled');
			$('#trocar_cliente').show();
			
			var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
			
			//Validar pedido de bonificação
			//Retirado a pedido do cliente.
			//bonificacao_disponivel_para_cliente({codigo: ui.item.codigo, loja: ui.item.loja}, function(){
				
				//Validar Cliente Bloqueado
				verificar_cliente_bloqueado({codigo : ui.item.codigo, loja : ui.item.loja}, function(){
					
					//Validar Cheques em Atraso
					verificar_cheques_atraso({codigo : ui.item.codigo, loja : ui.item.loja}, function() {
						
						//Validar Titulos Vencidos
						verificar_titulos_vencidos({codigo : ui.item.codigo, loja : ui.item.loja}, function() {
							
							//Validade se pedido possui uma rota disponível
							verificar_disponibilidade_rota({codigo : ui.item.codigo, loja : ui.item.loja},function(){
							
								$('input[name=cliente]').val(ui.item.label);
								$('input[name=codigo_cliente]').val(ui.item.codigo);
								$('input[name=loja_cliente]').val(ui.item.loja);
								
								salvar_sessao('codigo_cliente', ui.item.codigo);
								salvar_sessao('descricao_cliente', ui.item.label);
								salvar_sessao('loja_cliente', ui.item.loja);
								salvar_sessao('desconto_cliente', ui.item.desconto);
								
								
	//							CUSTOM: 177/ 000870- 24/10/2013 - Localizar: CUST-PEDIDO-CLIENTE-BLOQUEADO
								dados_cliente =  {};			 
								dados_cliente.situacao = ui.item.situacao; // CUSTOM: 177/ 000870- 24/10/2013 - Localizar: CUST-PEDIDO-CLIENTE-SITUACAO-SINTEGRA		
								dados_cliente.situacao_sintegra = ui.item.situacao_sintegra;// CUSTOM: 177/ 000870- 24/10/2013 - Localizar: CUST-PEDIDO-CLIENTE-SITUACAO-SINTEGRA
								dados_cliente.tabela_preco = ui.item.tabela_preco;
								dados_cliente.condicao_pagamento = ui.item.condicao_pagamento;
								dados_cliente.cep = ui.item.cep; //Chamado-Custom 002280 - Tabela de preço por faixa de CEP
								
								salvar_sessao('dados_cliente', dados_cliente);
	//							FIM CUSTOM: 177/ 000870- 24/10/2013 
								
								
								//--------------
								// Salvando Cliente de Entrega
								salvar_sessao('descricao_cliente_entrega', ui.item.label);
								salvar_sessao('codigo_cliente_entrega', ui.item.codigo);
								salvar_sessao('loja_cliente_entrega', ui.item.loja);
								//--------------
								
								exibir_informacoes_cliente(ui.item.codigo, ui.item.loja);
								
								//CUSTOM 000177/000870 - 25/11/2013
								regra_tabela_precos(ui.item.tabela_preco);
								obter_condicao_pagamento();
								
								return false;
								
							
							});
						}); //Fim Validação Titulos Vencidos
						
					}); //Fim Validação Cheques em Atraso
					
				}); //Fim Validação Cliente Bloqueado
				
			//}); //Fim validação bonificação de cliente.
		}
	});
}

/**
* Metódo:		exibir_informacoes_cliente
* 
* Descrição:	Função Utilizada para exibir informações do cliente
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_informacoes_cliente(codigo, loja)
{
	if(codigo && loja)
	{
		if(info.empresa)
		{
			var where = " AND empresa = '" + info.empresa + "'";
		}
	
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM clientes WHERE codigo = ? AND loja = ? ' + where, [codigo, loja], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						var html = '';
						
						$('.info_nome').html(dado.nome);
						$('.info_cpf').html(dado.cpf);

						
						obter_ultimo_pedido_cliente(codigo, loja);

						$('.info_limite_credito').html(number_format(dado.limite_credito, 3, ',', '.'));
						$('.info_titulos_aberto').html(number_format(dado.total_titulos_aberto, 3, ',', '.'));
						$('.info_titulos_vencidos').html(number_format(dado.total_titulos_ventidos, 3, ',', '.'));
						$('.info_credito_disponivel').html(number_format(dado.limite_credito - dado.total_titulos_aberto, 3, ',', '.'));

						$('.info_endereco').html(dado.endereco);
						$('.info_bairro').html(dado.bairro);
						$('.info_cidade').html(dado.cidade);
						$('.info_estado').html(dado.estado);
						
						$('.info_cep').html(dado.cep);
						
						//CUSTOM - Informações do cliente
						//Chamado 001762
						$('.info_cheques_devolvidos').html(dado.cheques_devolvidos);
						$('.info_media_atraso').html(dado.media_atraso);
						$('.info_risco_cliente').html(dado.risco);
						
						
						/*Tratamento telefone*/
						var telefone = '';
						if(dado.ddd)
						{
							telefone = '('+dado.ddd+') ' + dado.telefone;
						}
						else
						{
							if(dado.telefone)
							{
								telefone = dado.telefone;
							}						
						}
						/*Tratamento telefone*/
						$('.info_telefone').html(telefone);
						
						$('#info_cli').show();
						
					}
					
				}
			);
		});
	}
}



/**
* Metódo:		exibir_informacoes_prospect
* 
* Descrição:	Função Utilizada para exibir informações do prospect
* 
* Data:			13/11/2013
* Modificação:	13/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Prospect
* @param		String 			var loja		- Loja do Prospect
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_informacoes_prospect(codigo, loja)
{

	if(codigo && loja)
	{
		if(info.empresa)
		{
			var where = " AND empresa = '" + info.empresa + "'";
		}
	
	
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM prospects WHERE codigo = ? AND codigo_loja = ? ' + where, [codigo, loja], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						var html = '';
						
						$('.info_nome').html(dado.nome);
						$('.info_cpf').html(dado.cgc);

						$('.info_endereco').html(dado.endereco);
						$('.info_bairro').html(dado.bairro);
						$('.info_cidade').html(dado.nome_municipio);
						$('.info_estado').html(dado.estado);
						
						$('.info_cep').html(dado.cep);
						
						/*Tratamento telefone*/
						var telefone = '';
						if(dado.ddd)
						{
							telefone = '('+dado.ddd+') ' + dado.telefone;
						}
						else
						{
							if(dado.telefone)
							{
								telefone = dado.telefone;
							}						
						}
						/*Tratamento telefone*/
						$('.info_telefone').html(telefone);
						
						$('#info_pro').show();
						
					}
					
				}
			);
		});
	}
}




/**
* Metódo:		salvar_sessao
* 
* Descrição:	Função Utilizada para salvar dados do pedido na sessão
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		String 			var nome_campo		- Nome do campo que será utilizado na sessao
* @param		String 			var valor			- Valor do campo que será utilizado na sessao
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_sessao(nome_campo, valor)
{
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	// Adicionar novo valor no array
	sessao_pedido[nome_campo] = valor;
	
	// Gravar dados do Array na sessão
	sessionStorage[sessionStorage['sessao_tipo']] = serialize(sessao_pedido);
	
	remover_disabled();
}

/**
* Metódo:		obter_valor_sessao
* 
* Descrição:	Função Utilizada para retornar o valor de campo da sessão
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @param		String 			var campo		- Nome do campo que será utilizado para retornar o valor na sessao
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_valor_sessao(campo)
{
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		var sessao = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao)
	{
		if(sessao[campo])
		{
			return sessao[campo];
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

/**
* Metódo:		obter_ultimo_pedido_cliente
* 
* Descrição:	Função Utilizada para pegar o ultimo pedido do cliente e exibir os valores nas informações do cliente
* 
* Data:			10/10/2013
* Modificação:	10/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do Cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_ultimo_pedido_cliente(codigo, loja)
{
	db.transaction(function(x) {	
		x.executeSql('SELECT SUM(ip_valor_total_item) AS valor_pedido, SUM(ip_total_desconto_item) AS valor_desconto, * FROM pedidos_processados WHERE cliente_codigo = ? AND cliente_loja = ? GROUP BY pedido_codigo ORDER BY pedido_codigo DESC', [codigo, loja], function(x, dados) {
			if(dados.rows.length)
			{
				$('#conteudo_ultimo_pedido ul').show();
				$('#conteudo_ultimo_pedido div').hide();
				
				var dado = dados.rows.item(0);
				
				$('.info_pedido_data_emissao').html(protheus_data2data_normal(dado.pedido_data_emissao));
				$('.info_pedido_codigo').html(dado.pedido_codigo);
				$('.info_pedido_codigo').attr('href', 'pedidos_espelho.html#' + dado.pedido_codigo + '|' + dado.pedido_filial);
				$('.info_forma_pagamento').html(dado.forma_pagamento_descricao);
				$('.info_valor_pedido').html('R$ ' + number_format(dado.valor_pedido, 3, ',', '.'));
				$('.info_valor_desconto').html('R$ ' + number_format(dado.valor_desconto, 3, ',', '.'));
			}
			else
			{
				$('#conteudo_ultimo_pedido ul').hide();
				$('#conteudo_ultimo_pedido div').show();
			}
		});
	});
}


//---------------------------------------------------------------------------
//----------  ----------  Rotinas (Regras) da Incusão do Pedido ----------  ----------
//---------------------------------------------------------------------------

/**
* Metódo:		rotina_cliente
* 
* Descrição:	Função Utilizada para selecionar a tabela de preços do cliente (Se existir, se não seleciona a opção vazia "Selecione..." e bloqueia o campo e condicao_pagamento)
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function rotina_cliente(tabela)
{
	/*
	 * Tabela de preço será utilizada conforme regra:
	 * 
	 *  1 - Se o cliente possuir um tabela de preço no campo A1_TABELA  tabela será fixada e não será mais possível alteração.
	 *  2 - Caso não pertenca a regra acima, será utilizada a Tabela da preço do campo A0_VEND
	 * */
	
	if(tabela)
	{
		
		
		
		$('select[name="tabela_precos"] option[value="' + tabela + '"]').attr('selected', 'selected');
		salvar_sessao('tabela_precos', tabela);
		$('select[name="tabela_precos"]').attr('disabled', 'disabled');
		$('#aviso_tabela_preco').html('A tabela de preço '+tabela+' é fixa para esse cliente.').data('tabela_fixa','1');
	}	
	else
	{	
		$('select[name="tabela_precos"] option[value=""]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').attr('disabled', 'disabled');
		$('#aviso_tabela_preco').html('').removeData('tabela_fixa');
		
		if($('#aviso_condicao_pagamento').data('condicao_fixa')){
			if($('#aviso_condicao_pagamento').data('condicao_fixa') != 'tipo_venda'){
				$('select[name="condicao_pagamento"] option[value=""]').attr('selected', 'selected');
				$('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
				$('#aviso_condicao_pagamento').html('').removeData('condicao_fixa');
			}
		}
		
	}
}

/**
 * Regra de tabela de preços de acordo com a customização 000177/000870
 * 
 * @param tabela
 */
function regra_tabela_precos(tabela)
{
	var dados_cliente = obter_valor_sessao('dados_cliente');
	
	if(dados_cliente.tabela_preco)
	{
		tabela = dados_cliente.tabela_preco;
	}
	
	/* Fixa a tabela de preço por cliente campo A1_TABELA */
	
	
	
	if(tabela)
	{
		var filial = $('[name=filial]').val();	
		obter_tabela_preco(filial, tabela, function(tabela_preco){
			$('select[name="tabela_precos"] option[value="' + tabela + '"]').attr('selected', 'selected');
			salvar_sessao('tabela_precos', $('select[name="tabela_precos"]').val());
			$('select[name="tabela_precos"]').attr('disabled', 'disabled');
			$('#aviso_tabela_preco').html('A tabela de preço '+tabela+' é fixa para esse cliente.').data('tabela_fixa','1');
			salvar_sessao('tabela_fixa', 1);
			
			
			if(!tabela_preco){
				mensagem('Tabela de preço vinculada ao cliente não foi encontrada. Realize a sincronização do módulo tabela de preços.<br/>'
						+'<br/<br/>Se o problema persistir entre em contato com seu gestor comercial.');
			}
			
			
		});
			
			
		
		
		
		
	}
	/* Obtem de acordo com a regra */
	else if(!dados_cliente.tabela_preco && !obter_valor_sessao('tabela_preco'))
	{
		var filial = $('[name=filial]').val();
		var tipo_frete =  $('[name=tipo_frete]').val();
		var tipo_transporte = $('[name=tipo_transporte]').val();
		var porto_destino = $('[name=porto_destino]').val();
		
		var prefixo_tipo_transporte = tipo_transporte.split('', 1);
		
		var obter = false;
		
		if(filial && tipo_frete == 'C' && tipo_transporte && prefixo_tipo_transporte == 'M' && porto_destino)
		{
			obter = true;
		}
		else if(filial && tipo_frete && tipo_transporte && prefixo_tipo_transporte != 'M')
		{
			obter = true;
		}
		
		
		if(obter)
		{
			var where = '';
			where += 'filial_saida = "' + filial + '"';
			where += ' AND vinculo_representante = "' + localStorage.getItem('codigo') + '"';
			where += ' AND tipo_frete = "' + tipo_frete + '"';
			
			if(tipo_transporte)
			{
				where += ' AND tipo_transporte = "' + tipo_transporte + '"';
			}
			
			if(porto_destino)
			{
				where += ' AND porto_destino = "' + porto_destino + '"';
			}
			
			var faixa_cep = '';
			if(dados_cliente.cep) {
				faixa_cep += ' AND cep_inicial <= "' + dados_cliente.cep + '" AND cep_final >= "' + dados_cliente.cep + '"';
			}
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM tabelas_preco WHERE ' + where + faixa_cep, [], function(x, dados) {
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						$('select[name="tabela_precos"] option[value="' + dado.codigo + '"]').attr('selected', 'selected');
						salvar_sessao('tabela_precos', $('select[name="tabela_precos"]').val());
						$('select[name="tabela_precos"]').attr('disabled', 'disabled');
						$('#aviso_tabela_preco').html('A tabela de preço ' + dado.codigo + ' é fixa para essa faixa de CEP, de acordo com o tipo de frete/transporte.').data('tabela_fixa','1');
						salvar_sessao('tabela_fixa', 1);
						
						obter_condicao_pagamento();
					}
					else
					{
						x.executeSql('SELECT * FROM tabelas_preco WHERE ' + where + ' AND cep_inicial = "" AND cep_final = ""', [], function(x, dados) {
							if(dados.rows.length)
							{
								var dado = dados.rows.item(0);
								
								$('select[name="tabela_precos"] option[value="' + dado.codigo + '"]').attr('selected', 'selected');
								salvar_sessao('tabela_precos', $('select[name="tabela_precos"]').val());
								$('select[name="tabela_precos"]').attr('disabled', 'disabled');
								$('#aviso_tabela_preco').html('A tabela de preço ' + dado.codigo + ' é fixa para esse tipo de frete/transporte.').data('tabela_fixa','1');
								salvar_sessao('tabela_fixa', 1);
								
								obter_condicao_pagamento();
							}
							else
							{
								limpar_tabela_preco();
							}
						});
					}
				})
			});
		}
		else
		{
			limpar_tabela_preco();
		}
	}
}

function limpar_tabela_preco()
{
	$('select[name="tabela_precos"] option[value=""]').attr('selected', 'selected');
	$('select[name="tabela_precos"]').attr('disabled', 'disabled');
	$('#aviso_tabela_preco').html('').removeData('tabela_fixa');
	salvar_sessao('tabela_precos', null);
	salvar_sessao('tabela_fixa', null);
}



/**
* Metódo:		rotina_tabelas_preco
* 
* Descrição:	Função Utilizada bucar a condição de pagamento da tabela de preços e depois selecionar a condição de pagamento e bloquea-la (Se não existir condição de pagamento na tabela de preços, o campo de condição de pagamento fica livre para seleção)
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function rotina_tabelas_preco(filial)
{
	$('select[name=tabela_precos]').change(function(){
		salvar_sessao('tabela_precos', $(this).val());
		
		obter_condicao_pagamento();	
		
		// chamando regra de desconto para salvar na sessao
		regra_desconto();
	});
	
	// Selecionar campo se existir na sessão
	if(obter_valor_sessao('tabela_precos'))
	{
		$('select[name="tabela_precos"] option[value="' + obter_valor_sessao('tabela_precos') + '"]').attr('selected', 'selected');
	}
	
}

//CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-BONIFICACAO
function rotina_bonificacao()
{
	if($('select[name=tipo_venda]').val() == '02')
	{
		$('[name=condicao_pagamento]').removeAttr('disabled');
		$('[name=condicao_pagamento]').val('004 - BONIFICACAO');		
		$('#aviso_condicao_pagamento').html('A condição de pagamento 004 é fixa para o tipo de venda Bonificação.').data('condicao_fixa','1');
		$('select[name=condicao_pagamento]').change().attr('disabled','disabled');		
	}
	else
	{
		$('[name=condicao_pagamento]').val('');
		$('[name=condicao_pagamento]').attr('disabled','disabled');	
		$('#aviso_condicao_pagamento').html('').removeData('condicao_fixa');
		/*Limpa valor do campo tipo_bonificacao*/
		$('select[name=tipo_bonificacao]').val('');
		
	}
}

//FIM CUSTOM: 177 / 000874 - 04/11/2013


/**
* Metódo:		regra_desconto
* 
* Descrição:	Função Utilizada para salvar o valor da regra de desconto na sessao ( A regra padrão e pela tabela de preços)
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function regra_desconto()
{
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT percentual_desconto FROM regra_desconto WHERE tabela_preco = ?', [obter_valor_sessao('tabela_precos')], function(x, dados) {
				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);
					
					salvar_sessao('regra_desconto', dado.percentual_desconto);
				}
				else
				{
					salvar_sessao('regra_desconto', 0);
				}
				
			}
		);
	});
}


/**
* Metódo:		remover_disabled
* 
* Descrição:	Função Utilizada para habilitar os botoes que estao bloqueados
* 
* Data:			09/10/2013
* Modificação:	09/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function remover_disabled()
{
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		
		// Se existir Filial na sessao, habilitar TIPO DE PEDIDO
		if(sessao_pedido.filial)
		{
			$('select[name=tipo_pedido]').removeAttr('disabled');
			$('select[name=filial]').attr('disabled', 'disabled');
		}
		
		
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA
		// Se existir Filial na sessao, habilitar TIPO DE VENDA
		if(sessao_pedido.filial && !sessao_pedido.produtos)
		{
			$('select[name=tipo_venda]').removeAttr('disabled');
		}
		else
		{
			$('select[name=tipo_venda]').attr('disabled', 'disabled');
		}
		// FIM CUSTOM: 177 / 000874 - 04/11/2013 
		
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-BONIFICACAO
		// Se existir Filial na sessao, habilitar TIPO DE VENDA
		if(sessao_pedido.tipo_venda)
		{
			if(sessao_pedido.tipo_venda == '02'){
				$('select[name=tipo_bonificacao]').removeAttr('disabled');
				$('p.tipo_bonificacao').removeClass('hidden_input');
			}
		}
		
		// FIM CUSTOM: 177 / 000874 - 04/11/2013
		
		
		
		// Se existir Tipo de Pedido na sessao, habilitar CLIENTE
		if(sessao_pedido.tipo_pedido && !sessao_pedido.codigo_cliente)
		{
			$('input[name=cliente]').removeAttr('disabled');
		}
		
		// Se existir Tipo de Pedido na sessao, habilitar Propect
		if(sessao_pedido.tipo_pedido && !sessao_pedido.codigo_prospect)
		{
			$('input[name=prospect]').removeAttr('disabled');
		}

		
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TRANSPORTADORA-ALTERACAO
		
		if((sessao_pedido.tipo_pedido && sessao_pedido.codigo_prospect) || (sessao_pedido.tipo_pedido && sessao_pedido.codigo_cliente))
		{
			$('select[name=tipo_frete]').removeAttr('disabled');
		}
		
		
		
		// Se existir Tipo de Frete na sessao, e for igual a C, ou seja CIF  habilitar TIPO DE TRANSPORTE
		if(sessao_pedido.tipo_frete)
		{
			$('select[name=tipo_transporte]').removeAttr('disabled');
			$('p.tipo_transporte').removeClass('hidden_input');
		}
		
		if(sessao_pedido.tipo_frete && sessao_pedido.tipo_transporte)
		{
			$('select[name=porto_destino]').removeAttr('disabled');
		}
		
		if(!sessao_pedido.tabela_fixa && sessao_pedido.tipo_frete)
		{
			//$('select[name=tabela_precos]').removeAttr('disabled');
		}
		
		if(sessao_pedido.tipo_frete && sessao_pedido.tipo_transporte && sessao_pedido.tabela_precos && !$('#aviso_condicao_pagamento').data('condicao_fixa'))
		{
			if(! obter_valor_sessao('condicao_pagamento')){
				$('[name=condicao_pagamento]').removeAttr('disabled');
			}
		}
		// FIM CUSTOM: 177 / 000874 - 04/11/2013
		
		if(sessao_pedido.tipo_frete && sessao_pedido.tipo_transporte && sessao_pedido.tabela_precos && sessao_pedido.condicao_pagamento && !obter_valor_sessao('forma_pagamento_fixa'))
		{
			$('[name=forma_pagamento]').removeAttr('disabled');
		}
		
		if((sessao_pedido.codigo_cliente || sessao_pedido.codigo_prospect)&& sessao_pedido.tabela_precos && sessao_pedido.tipo_frete && sessao_pedido.condicao_pagamento && sessao_pedido.forma_pagamento)
		{
			var tipo_venda = trim($('[name=tipo_venda]').val());
			if(tipo_venda != '02' && tipo_venda != '20' && tipo_venda != '30' && !$('select[name="banco"]').data('banco_fixo'))
			{
				$('select[name=banco]').removeAttr('disabled');
			}
		}
		
		//-------------------------------------------------------------------------------------------------------------
		//------------ Desativar botao "Adicionar Produtos" se uma opção nao for selecionada --------------------------
		//-------------------------------------------------------------------------------------------------------------
		
		if(!sessao_pedido.filial)
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.tipo_pedido)
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		
		
		//----------------------------
		//------------------------------------------
		if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_pedido')
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		else if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && (sessao_pedido.cliente_prospect == 'C' || sessao_pedido.cliente_prospect == null))
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		else if(!sessao_pedido.codigo_prospect && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && sessao_pedido.cliente_prospect == 'P')
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		//------------------------------------------
		//----------------------------
		
		
		
		if(!sessao_pedido.tabela_precos)
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.condicao_pagamento)
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		ativar_tabs();

	}
}


/**
* Metódo:		salvar_dados_cliente
* 
* Descrição:	Função Utilizada para salvar dados do cliente
* 
* Data:			07/11/2013
* Modificação:	07/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_cliente(codigo, loja)
{
	if(codigo && loja)
	{
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja], function(x, dados) {
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);

						salvar_sessao('dados_cliente', dado);
						
					}
					
				}
			);
		});
	}
}



/**
* Metódo:		salvar_dados_prospect
* 
* Descrição:	Função Utilizada para salvar dados do prospect
* 
* Data:			16/11/2013
* Modificação:	16/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Prospect
* @param		String 			var loja		- Loja do Prospect
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_prospect(codigo, loja)
{
	if(codigo && loja)
	{
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM prospects WHERE codigo = ? AND codigo_loja = ?', [codigo, loja], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						salvar_sessao('dados_prospect', dado);
						
					}
					
				}
			);
		});
	}
}



/**
* Metódo:		salvar_dados_forma_pagamento
* 
* Descrição:	Função Utilizada para salvar dados da forma de pagamento
* 
* Data:			07/11/2013
* Modificação:	07/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo forma de pagamento
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_forma_pagamento(codigo)
{
	if(codigo)
	{
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM condicoes_pagamento WHERE codigo = ?', [codigo], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);

						salvar_sessao('dados_forma_pagamento', dado);
						
					}
					
				}
			);
		});
	}
}

function salvar_dados_porto_destino(codigo)
{
	if(codigo)
	{
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM portos_destino WHERE porto = ?', [codigo], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);

						salvar_sessao('dados_porto_destino', dado);
						
					}
					
				}
			);
		});
	}
	else
	{
		salvar_sessao('dados_porto_destino', null);
	}
}

function salvar_dados_tipo_transporte(codigo, callback)
{
	if(codigo)
	{
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM tipos_transporte WHERE chave = ?', [codigo], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);

						salvar_sessao('dados_tipo_transporte', dado);
						
						callback();
					}
					
				}
			);
		});
	}
}

/**
* Metódo:		obter_descricao_pedido
* 
* Descrição:	Função Utilizada para obter a descrição do pedido
* 
* Data:			13/11/2013
* Modificação:	13/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function obter_descricao_pedido(tipo)
{
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento')
	{
		var descricao_tipo_pedido = 'Orçamento';
	}
	else
	{
		var descricao_tipo_pedido = 'Pedido';
	}
	
	if(tipo == 'upper')
	{
		return descricao_tipo_pedido.toUpperCase();
	}
	else if(tipo == 'lower')
	{
		return descricao_tipo_pedido.toLowerCase();
	}
	else
	{
		return descricao_tipo_pedido;
	}
}




/**
* Metódo:		obter_tipo_transporte
* 
* Descrição:	Função Utilizada para retornar Tipos de Transporte
* 
* Data:			06/11/2013
* Modificação:	06/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tipo_transporte()
{
	$('select[name=tipo_transporte]').html('');
	$('select[name=tipo_transporte]').append('<option value="">SELECIONE...</option>');
	
	var where = '';
	if($('[name=tipo_frete]').val() == 'F')
	{
		where += 'WHERE chave IN ("C1", "C2")';
	}
	else if($('[name=tipo_frete]').val() == 'C') //Chamado 002788 - [CUSTOM] - Melhoria - digitação tipo de transporte 
	{
		where += 'WHERE chave NOT IN ("C1", "C2")';
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM tipos_transporte ' + where,[],function(x, dados) {
			if (dados.rows.length) {
				$('select[name=tipo_transporte]').html('');
				$('select[name=tipo_transporte]').append('<option value="">SELECIONE...</option>');
				
				for (i = 0; i < dados.rows.length; i++) {
					var dado = dados.rows.item(i);
					
					$('select[name=tipo_transporte]').append('<option value="'+dado.chave+'">'+dado.descricao+'</option>')
				}
				$('select[name="tipo_transporte"] option[value="' + obter_valor_sessao('tipo_transporte') + '"]').attr('selected', 'selected');
				regra_tabela_precos();
			}
		});
	});
}




//////////////////////////////////////////////////////////////////////////////
//Portos Destino
//////////////////////////////////////////////////////////////////////////////

/**
* Metódo:		obter_tipo_porto_destino
* 
* Descrição:	Função Utilizada para retornar Portos Destino
* 
* Data:			06/11/2013
* Modificação:	06/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tipo_porto_destino()
{
	var porto_destino = '';
	
	$('select[name=porto_destino]').html('<option value="">SELECIONE...</option>');
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM portos_destino ',[],function(x, dados) {
			if (dados.rows.length)
			{
				for (i = 0; i < dados.rows.length; i++) {
					var dado = dados.rows.item(i);
					
					$('select[name=porto_destino]').append('<option value="'+dado.porto+'">'+dado.descricao+'</option>')
				}
				$('select[name="porto_destino"] option[value="' + obter_valor_sessao('porto_destino') + '"]').attr('selected', 'selected');
				if(obter_valor_sessao('porto_destino'))
				{
					$('.porto_destino').removeClass('hidden_input');
				}
			}
			else
			{
				$('select[name=porto_destino]').append('<option value=" ">Nenhum porto destino disponível</option>').attr('selected', 'selected');
			}
		});
	});
}


function obter_dados_cliente()
{
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		
		if(sessao_pedido.codigo_cliente)
		{
			salvar_dados_cliente(sessao_pedido.codigo_cliente, sessao_pedido.loja_cliente);
		}
	}
}






/**
 * TODO: Obter o numero máximo de dias de atraso (MV_RISCOX)
 */
function obter_limites_dias_atraso(risco)
{
	var dias = 0;
	if(risco == 'B')
	{
		dias = obter_valor_sessao('MV_RISCOB');
		return (dias ? dias : 0);
	}
	else if(risco == 'C')
	{
		dias = obter_valor_sessao('MV_RISCOC');
		return (dias ? dias : 0);
	}
	else if(risco == 'D')
	{
		dias = obter_valor_sessao('MV_RISCOD');
		return (dias ? dias : 0);
	}
	else
	{
		return 0;
	}
}

/**
* Metódo:		obter_parametros
* 
* Descrição:	Obtem os valores de acordo com a tabela de parametros
* 
* Data:			04/12/2013
* Modificação:	--/--/----
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_parametros(filial)
{
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM parametros WHERE filial = ?', [filial], function(x, dados) {
			if (dados.rows.length)
			{
				for (i = 0; i < dados.rows.length; i++)
				{
					var dado = dados.rows.item(i);
					
					salvar_sessao(dado.mv, dado.valor);
				}
			}
		})
	});
}

/**
* Metódo:		bonificacao_disponivel_para_cliente()
* 
* Descrição:	Função Utilizada para verificar se o cliente pode receber uma bonificação.
* 				CUSTOM 000177/000870
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function bonificacao_disponivel_para_cliente(parametros, callback)
{
	
	if(sessionStorage['sessao_tipo'] != 'sessao_orcamento' && $('[name=tipo_venda]').val() == '02' && $('[name=tipo_bonificacao]').val() != '02')
	{
		
		/*Verifica se cliente possui pelo menos um pedido faturado*/
		var wheres ="pedido_codigo_cliente = '" + parametros.codigo + "' AND pedido_loja_cliente = '" + parametros.loja + "' AND ip_numero_nota_fiscal != ''"
		
		db.transaction(function(x) {
			x.executeSql('SELECT ip_numero_nota_fiscal FROM pedidos_processados WHERE ' + wheres+ ' LIMIT 1', [], function(x, dados) {
				if (dados.rows.length)
				{
					salvar_sessao('pedido_bloqueado', '1');
					callback();					
				}
				else
				{
					mensagem('Para fazer uma <strong>bonificação</strong> o cliente precisa ter ao menos um pedido faturado.');
					$('#trocar_cliente').trigger('click');
				}
			});
		});
		
	
	}
	else
	{
		callback();
	}
	
}

//CUSTOM 177 - 000870 - 20/11/2013
//Verifica se cliente está bloqueado
function verificar_cliente_bloqueado(parametros, callback)
{	
	/*Continuar daqui*/
	if(parametros.codigo && parametros.loja)
	{
		
		db.transaction(function(x) {
			x.executeSql(
				'SELECT nome, situacao, situacao_sintegra FROM clientes WHERE codigo = ? AND loja = ?', [parametros.codigo, parametros.loja], function(x, dados) {
					
					//----------------------------
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						if(dado.situacao == 1 )
						{
							mensagem('Não é possível incluir pedido para o cliente <strong>' + dado.nome + '</strong>. <br /><br />O cliente está <strong>Bloqueado</strong>.');
							$('#trocar_cliente').trigger('click');
						}
						else if(dado.situacao_sintegra != 'S' &&  dado.situacao_sintegra != 'I')
						{
							mensagem('Não é permitida a inclusão de pedidos para o clientes com a <strong>Situação do Sintegra</strong> desabilitada.');
							$('#trocar_cliente').trigger('click');
						}
						else
						{
							callback();
						}
						
					}
					
					//----------------------------
					
				}
			);
		});
	}
	else
	{
		callback();
	}
}
//FIM CUSTOM - 000870

//Verifica se há títulos Vencidos
function verificar_titulos_vencidos(parametros, callback)
{
		if(parametros.codigo && parametros.loja)
		{
			
			db.transaction(function(x) {
				x.executeSql(
					'SELECT nome, est_titulo_vencido_mais_antigo, risco FROM clientes WHERE codigo = ? AND loja = ?', [parametros.codigo, parametros.loja], function(x, dados) {
						
						//----------------------------
						
						if(dados.rows.length)
						{
							var dado = dados.rows.item(0);
							
							var data_atual = date('Ymd');
							var limite_dias_atraso = obter_limites_dias_atraso(dado.risco);
							
							if(dado.risco == 'A')
							{
								callback();
							}
							/*Chamado 001717 - Títulos Vencidos
							 * Removido para que seja realizada a validação de títulos vencidos também para clientes riscos E e Z.
							 * else if(dado.risco == 'E' || dado.risco == 'Z')
							{
								callback();
							}*/
							else if(diferenca_entre_datas(dado.est_titulo_vencido_mais_antigo,data_atual) <= limite_dias_atraso)
							{
								callback();
							}
							else
							{									
								mensagem('Este cliente <strong>Risco '+dado.risco+' </strong>está com limite de dias de títulos vencidos acima do tolerável. <br /><br />O cliente possui título(s) em aberto(s).');
								$('#trocar_cliente').trigger('click');
							}
							
						}
						
						//----------------------------
						
					}
				);
			});
			
		}
		else
		{
			callback();
		}	
}



//Verifica se há Rota disponível
function verificar_disponibilidade_rota(parametros, callback)
{
		if(parametros.codigo && parametros.loja)
		{
			obter_cliente(parametros.codigo,  parametros.loja, function(dados_cliente){
				
				
				var buscar_rota = '';
				buscar_rota += 'SELECT * ';
				buscar_rota += 'FROM rotas ';
				buscar_rota += 'JOIN zonas_setor ON zonas_setor_codigo_rota = rota_codigo_rota AND zonas_setor_filial = "' + obter_valor_sessao('filial') + '" ';
				//buscar_rota += 'JOIN prazos_entrega ON rota_codigo_rota = prazo_codigo_rota AND prazo_filial = "' + obter_valor_sessao('filial') + '"';
				
				//Primeiro busca exclusiva por cliente, se não encontrar busca por faixa de CEP
				var busca_por_cliente = 'JOIN clientes_setor ON clientes_setor_codigo_zona = zonas_setor_codigo_zona AND clientes_setor_codigo_setor = zonas_setor_codigo_setor AND clientes_setor_codigo_cliente = ? AND clientes_setor_loja_cliente = ? ';
				
				
				var buscar_por_faixa_cep = 'JOIN clientes_setor ON clientes_setor_codigo_zona = zonas_setor_codigo_zona AND clientes_setor_codigo_setor = zonas_setor_codigo_setor AND clientes_setor_cep_inicial <= ? AND clientes_setor_cep_final >= ? ';
				
					console.log('Por cliente: '+buscar_rota + busca_por_cliente + 'WHERE rota_filial = "' + obter_valor_sessao('filial') + '"');
				
					
					console.log('Por faixa de CEP: '+buscar_rota + buscar_por_faixa_cep + 'WHERE rota_filial = "' + obter_valor_sessao('filial') + '"');
				db.transaction(function(x) {
					x.executeSql(buscar_rota + busca_por_cliente + 'WHERE rota_filial = "' + obter_valor_sessao('filial') + '"', [dados_cliente.codigo, dados_cliente.est_codigo_loja], function(x, dados) {
							
						if(dados.rows.length)
						{	
							console.log('Rota por Cliente');
							callback();
						}
						else
						{
							
							x.executeSql(buscar_rota + buscar_por_faixa_cep + 'WHERE rota_filial = "' + obter_valor_sessao('filial') + '"', [dados_cliente.cep, dados_cliente.cep], function(x, dados) {
								if(dados.rows.length)
								{
									console.log('Rota por CEP');
									callback();
								}
								else
								{
									mensagem('Este cliente não possui rotas disponíveis.');
										$('#trocar_cliente').trigger('click');
								}
							});
						}		
							
					
					});
				});	
			});
		}
		else
		{
			callback();
		}	
}


//FIM CUSTOM 177 - 000870 - 12/11/2013

//CUSTOM 177 - 000870 - 12/11/2013
//Verifica se há cheques em atraso
function verificar_cheques_atraso(parametros, callback)
{
	if(parametros.codigo && parametros.loja)
	{
		db.transaction(function(x) {
			x.executeSql(
				'SELECT nome, est_numero_cheques_devolvidos, risco FROM clientes WHERE codigo = ? AND loja = ?', [parametros.codigo, parametros.loja], function(x, dados) {
					
					//----------------------------
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						
						console.log('Numero_cheques_devolvidos <' + dado.est_numero_cheques_devolvidos + '>');
						console.log('Risco <'+dado.risco+'>');
						
						if(dado.risco == 'A')
						{
							callback();
						}
						else if(dado.est_numero_cheques_devolvidos <= 0 )
						{
							callback();								
						}						
						else
						{
							mensagem('Não é possível incluir pedido para o cliente <strong>' + dado.nome + '</strong>. <br /><br />O cliente possui cheque(s) devolvido(s).');
							$('#trocar_cliente').trigger('click');
						}
					}
					
					//----------------------------
					
				}
			);
		});
		
	}
	else
	{
		callback();
	}
}

/**
* Metódo:		obter_banco()
* 
* Descrição:	Função Utilizada obter o banco de acordo com o tipo de venda selecionado na inclusão do pedido
* 				Se o tipo de venda selecionado for "BONIFICACAO", "TROCA" ou "AMOSTRA",
* 					O banco será fixado como "84 - ESPECIE", não permitindo alteração
* 
* 				Caso contrário o representante poderá selecionar qualquer opção
* 				CUSTOM
* 
* Data:			10/01/2014
* Modificação:	10/01/2014
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_banco()
{
	var tipo_venda = $('[name=tipo_venda]').val();
	
	if(tipo_venda == '02' || tipo_venda == '20' || tipo_venda == '30')
	{
		
		obter_bancos('CH');
		$('select[name="banco"] option[value="85"]').attr('selected', 'selected');
		$('select[name="banco"] option[value="84"]').attr('selected', 'selected');
		$('select[name="banco"]').attr('disabled', 'disabled');		
		salvar_sessao('banco', $('select[name="banco"]').val());
		$('[name=forma_pagamento]').change();
		
	}
	else
	{
		var banco = '';
		if(obter_valor_sessao('banco'))
		{
			banco = obter_valor_sessao('banco');
		}
		$('select[name="banco"] option[value="' + banco + '"]').attr('selected', 'selected');
		
		$('[name=forma_pagamento]').change()
		salvar_sessao('banco', $('select[name="banco"]').val());
	}
}

function obter_forma_pagamento(){
	var tipo_venda = $('[name=tipo_venda]').val();
	salvar_sessao('forma_pagamento_fixa', false);
	
	console.log('Obtendo Forma de pagmento');
	
	
	
	
	var tipo_venda = $('[name=tipo_venda]').val();
	var codigo_condicao_pagamento = $('[name=codigo_condicao_pagamento]').val();
	
	salvar_sessao('forma_pagamento_fixa', false);
	$('select[name="forma_pagamento"]').html('<option value="">SELECIONE...</option>');
	
	$('select[name="forma_pagamento"]').html('<option value="">SELECIONE...</option>');
	$.each(lista_forma_pagamento, function(i, forma_pagamento){
		$('select[name="forma_pagamento"]').append('<option value="'+forma_pagamento.valor+'">'+forma_pagamento.descricao+'</option>');
		
			
	});
	
	
	
	
	
	
	
	//Forma de pagamento fixa para os tipos de venda BONIFICAÇÃO, TROCA
	if(in_array(tipo_venda, ['02', '20', '30'])){
		$('select[name="forma_pagamento"] option[value="CH"]').attr('selected', 'selected');
		salvar_sessao('forma_pagamento', $('select[name="forma_pagamento"]').val());
		salvar_sessao('forma_pagamento_fixa', true);
		$('select[name="forma_pagamento"]').attr('disabled', 'disabled');
	}
	
	
	
	var codigo_condicao_pagamento = $('[name=codigo_condicao_pagamento]').val();
	console.log(' x x x x x x codigo_condicao_pagamento: '+codigo_condicao_pagamento);
	if(in_array(codigo_condicao_pagamento, ['1'])){
		$('select[name="forma_pagamento"] option[value="BL"]').remove();
	
	}
	
	
	if(obter_valor_sessao('forma_pagamento'))
	{
		$('select[name="forma_pagamento"]').val(obter_valor_sessao('forma_pagamento'));
	}
	
	
}



function obter_cliente(codigo, loja, callback){
	
	var cliente = {};

	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja], function(x, dados) {
				
				//----------------------------
				
				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);
					cliente = dado;
				}
				
				callback(cliente);
			});
	});
				

}






function obter_tabela_preco(filial, codigo, callback)
{
	
	var where = " WHERE codigo = ? ";
	
	if(info.empresa)
	{
		where += " AND empresa = '" + info.empresa + "' ";
	}

	
	
	//DATA INICIO DE VIGENCIA DA TABELA DE PREÇO
	where += " AND vigencia_inicio <= '" + date('Ymd') + "'";
	
	//DATA FINAL DE VIGENCIA DA TABELA DE PREÇO
	where += " AND (vigencia_final >= '" + date('Ymd') + "' OR vigencia_final = '')";
	
	
	where += " AND filial_saida = '" + obter_valor_sessao('filial') + "' ";
	
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM tabelas_preco ' + where, [codigo], function(x, dados) {
			
				var total = dados.rows.length;
				
				if(total > 0){
					var dado = dados.rows.item(0);
						
							
					callback(dado.codigo);
							
					
				}else{
					callback(false);
				}
				
			
			}
		);
	});
}
