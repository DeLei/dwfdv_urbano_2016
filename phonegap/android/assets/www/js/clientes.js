$(document).ready(function() {
	// obter clientes
	
	obter_clientes(sessionStorage['clientes_pagina_atual'], sessionStorage['clientes_ordem_atual']);
	
	// Obter Estados
	$('select[name*=estado]').html('<option value> Selecione... </option>' + ufs);
	
	
	// Obter Municipios
	
	$('select[name=estado]').live('change', function(){
		$('select[name=municipio]').attr('disabled', 'disabled');
		$('select[name=municipio]').html('<option value> Carregando... </option>');
		
		var estado = $(this).val();
		
		obter_municipios(estado);
		
	});
	
	function obter_municipios(estado, municipio)
	{
		// obter Municipios
		db.transaction(function(x) {
		
			
			x.executeSql('SELECT DISTINCT codigo, nome FROM municipios WHERE uf = ? ORDER BY nome ASC', [estado], function(x, dados) {
					
					$('select[name=municipio]').empty();
					$('select[name=municipio]').removeAttr('disabled');
					$('select[name=municipio]').append('<option value> Selecione... </option>');
					if (dados.rows.length)
					{
						for (i = 0; i < dados.rows.length; i++)
						{
							var dado = dados.rows.item(i);
							
							var selected = '';
							
							if(municipio == dado.codigo)
							{
								selected = 'selected="selected"';
							}
							
							$('select[name=municipio]').append('<option value="' + dado.codigo + '" ' + selected + '>' + dado.nome + '</option>');
						}
					}
					else
					{
						$('select[name=municipio]').empty();
						$('select[name=municipio]').attr('disabled', 'disabled');
						$('select[name=municipio]').append('<option value> Selecione um Estado </option>');
					}
					
				}
			);
			
		});
	}

	
	$('select[name=municipio]').live('change', function(){
		
		var nome_municipio = $('select[name=municipio] option:selected').html();
		$('input[name=nome_municipio]').val(nome_municipio);
		
	});
	
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_clientes(sessionStorage['clientes_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_clientes(sessionStorage['clientes_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['clientes_' + name] != 'undefined' ? sessionStorage['clientes_' + name] : '');
	});
	
	$('#filtrar').click(function() {
		sessionStorage['clientes_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['clientes_' + name] = $(this).val();
		});
		
		obter_clientes(sessionStorage['clientes_pagina_atual'], sessionStorage['clientes_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['clientes_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['clientes_' + name] = '';
		});
		
		obter_clientes(sessionStorage['clientes_pagina_atual'], sessionStorage['clientes_ordem_atual']);
	});
});

function obter_clientes(pagina, ordem)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'nome ASC';
	
	// setar pagina e ordem atual
	sessionStorage['clientes_pagina_atual'] = pagina;
	sessionStorage['clientes_ordem_atual'] = ordem;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['clientes_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'ASC');
		
		$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'DESC');
		
		$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['clientes_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	codigo = 			sessionStorage['clientes_codigo'];
	loja = 				sessionStorage['clientes_loja'];
	nome = 				sessionStorage['clientes_nome'];
	nome_fantasia =		sessionStorage['clientes_nome_fantasia'];
	cpf = 				sessionStorage['clientes_cpf'];
	telefone = 			sessionStorage['clientes_telefone'];
	municipio = 		sessionStorage['clientes_municipio'];
	estado = 			sessionStorage['clientes_estado'];
	
	var wheres = '';
	
	if (codigo && codigo != 'undefined')
	{
		wheres += ' AND codigo LIKE "%' + codigo + '%"';
	}
	
	if (loja && loja != 'undefined')
	{
		wheres += ' AND loja LIKE "%' + loja + '%"';
	}
	
	if (nome && nome != 'undefined')
	{
		wheres += ' AND nome LIKE "%' + nome + '%"';
	}
	
	if (nome_fantasia && nome_fantasia != 'undefined')
	{
		wheres += ' AND nome_fantasia LIKE "%' + nome_fantasia + '%"';
	}
	
	if (cpf && cpf != 'undefined')
	{
		wheres += ' AND cpf LIKE "%' + cpf + '%"';
	}
	
	if (telefone && telefone != 'undefined')
	{
		wheres += ' AND telefone LIKE "%' + telefone + '%"';
	}
	
	if (info.empresa)
	{
		wheres += " AND empresa = '" + info.empresa + "'";
	}
	
	if (estado && estado != 'undefined')
	{
		wheres += ' AND estado = "' + estado + '"';
	}
	
	if (municipio && municipio != 'undefined')
	{
		wheres += ' AND codigo_municipio = "' + municipio + '"';
	}

	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM clientes WHERE codigo > 0 ' + wheres + ' ORDER BY ' + sessionStorage['clientes_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
				if (dados.rows.length)
				{
					$('table tbody').empty();
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var p = dados.rows.item(i);
						
						var html = '';
							html += '<td align="center">'+p.codigo + '/' + p.loja+'</td>';
							html += '<td>'+p.nome+'</td>';
							html += '<td>'+(p.nome_fantasia ? p.nome_fantasia : 'N\A')+'</td>';
							html += '<td width="160">'+(p.cpf ? formatar_cpfCNPJ(p.cpf) : 'N\A')+'</td>';
							html += '<td width="160">'+(p.telefone ? formatar_telefone(p.ddd+str_replace('-', '', p.telefone)) : 'N\A')+'</td>';
							html += '<td width="80">'+'<a href="clientes_visualizar.html#' + p.codigo + '_' + p.loja + '" class="btn btn-large btn-primary">+ Opções</a>'+'</td>';
					
						$('table tbody').append('<tr>' + html + '</tr>');
					}
					
					alterarCabecalhoTabelaResolucao();
				}
				else
				{
					$('table tbody').html('<tr><td colspan="6" style="color: #900; padding: 10px;"><strong>Nenhum cliente encontrado.</strong></td></tr>');
				}
			}
		);
		
		// calcular totais
		
		x.executeSql(
			'SELECT COUNT(cpf) AS total FROM clientes WHERE codigo > 0 ' + wheres, [], function(x, dados) {
				var dado = dados.rows.item(0);
				
				$('#total').text(number_format(dado.total, 0, ',', '.'));
				
				// paginação
				
				$('#paginacao').html('');
				
				var total = ceil(dado.total / 20);
				
				if (total > 1)
				{
					if (sessionStorage['clientes_pagina_atual'] > 6)
					{
						$('#paginacao').append('<a href="#" onclick="obter_clientes(1, \'' + sessionStorage['clientes_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
					}
					
					if (sessionStorage['clientes_pagina_atual'] > 1)
					{
						$('#paginacao').append('<a href="#" onclick="obter_clientes(' + (intval(sessionStorage['clientes_pagina_atual']) - 1) + ', \'' + sessionStorage['clientes_ordem_atual'] + '\'); return false;">&lt;</a> ');
					}
					
					for (i = intval(sessionStorage['clientes_pagina_atual']) - 6; i <= intval(sessionStorage['clientes_pagina_atual']) + 5; i++)
					{
						if (i <= 0 || i > total)
						{
							continue;
						}
						
						if (i == sessionStorage['clientes_pagina_atual'])
						{
							$('#paginacao').append('<strong>' + i + '</strong> ');
						}
						else
						{
							$('#paginacao').append('<a href="#" onclick="obter_clientes(' + i + ', \'' + sessionStorage['clientes_ordem_atual'] + '\'); return false;">' + i + '</a> ');
						}
					}
					
					if (sessionStorage['clientes_pagina_atual'] < total)
					{
						$('#paginacao').append('<a href="#" onclick="obter_clientes(' + (intval(sessionStorage['clientes_pagina_atual']) + 1) + ', \'' + sessionStorage['clientes_ordem_atual'] + '\'); return false;">&gt;</a> ');
					}
					
					if (sessionStorage['clientes_pagina_atual'] <= total - 6)
					{
						$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_clientes(' + total + ', \'' + sessionStorage['clientes_ordem_atual'] + '\'); return false;">Última Página</a> ');
					}
				}
			}
		);
	});
}
