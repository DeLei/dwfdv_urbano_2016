function ValidaDocumentos(){ 
 
    this.validaAC= function(ie){
		if (strlen(ie) != 13) {
            return 0;
        } else {
            if (substr(ie, 0, 2) != '01') {
                return 0;
            } else {
                $peso = 4;
                $soma = 0;
                for ($i = 0; $i <= 10; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                    if ($peso == 1) {
                        $peso = 9;
                    }
                }
                $dig = 11 - ($soma % 11);
                if ($dig >= 10) {
                    $dig = 0;
                }
                if (!($dig == ie[11])) {
                    return 0;
                } else {
                    $peso = 5;
                    $soma = 0;
                    for ($i = 0; $i <= 11; $i++) {
                        $soma += ie[$i] * $peso;
                        $peso--;
                        if ($peso == 1) {
                            $peso = 9;
                        }
                    }
                    $dig = 11 - ($soma % 11);
                    if ($dig >= 10) {
                        $dig = 0;
                    }

                    return ($dig == ie[12]);
                }
            }
        }
    };
    
   
 // Alagoas
    this.validaAL=  function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            if (substr(ie, 0, 2) != '24') {
                return 0;
            } else {
                $peso = 9;
                $soma = 0;
                for ($i = 0; $i <= 7; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                }
                $soma *= 10;
                $dig = $soma - ( parseInt(  ($soma / 11) ) * 11 );
                if ($dig == 10) {
                    $dig = 0;
                }

                return ($dig == ie[8]);
            }
        }
    };
   
  //Amazonas
    this.validaAM= function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            $peso = 9;
            $soma = 0;
            for ($i = 0; $i <= 7; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            if ($soma <= 11) {
                $dig = 11 - $soma;
            } else {
                $r = $soma % 11;
                if ($r <= 1) {
                    $dig = 0;
                } else {
                    $dig = 11 - $r;
                }
            }

            return ($dig == ie[8]);
        }
    };
   
    
  //Amapá
    this.validaAP= function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            if (substr(ie, 0, 2) != '03') {
                return 0;
            } else {
                $i = substr(ie, 0, -1);
                
                
                if (($i >= 3000001) && ($i <= 3017000)) {
                    $p = 5;
                    $d = 0;
                }else if (($i >= 3017001) && ($i <= 3019022)) {
                    $p = 9;
                    $d = 1;
                }else if ($i >= 3019023) {
                    $p = 0;
                    $d = 0;
                }

                $peso = 9;
                $soma = $p;
                for ($i = 0; $i <= 7; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                }
                $dig = 11 - ($soma % 11);
                if ($dig == 10) {
                    $dig = 0;
                } else if ($dig == 11) {
                    $dig = $d;
                }

                return ($dig == ie[8]);
                
            }
        }
    };    
  

  //Bahia
    this.validaBA= function(ie) {
        if (strlen(ie) > 9) {
            return 0;
        } else if (strlen(ie) == 8){

            $inicial1 = array('0', '1', '2', '3', '4', '5', '8');
            $inicial2 = array('6', '7', '9');

            $i = substr(ie, 0, 1);

            if (in_array($i, $inicial1)) {
                $modulo = 10;
            } else if (in_array($i, $inicial2)) {
                $modulo = 11;
            }

            $peso = 7;
            $soma = 0;
            for ($i = 0; $i <= 5; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }

            $i = $soma % $modulo;
            if ($modulo == 10) {
                if ($i == 0) {
                    $dig = 0;
                } else {
                    $dig = $modulo - $i;
                }
            } else {
                if ($i <= 1) {
                    $dig = 0;
                } else {
                    $dig = $modulo - $i;
                }
            }
            if (!($dig == ie[7])) {
                return 0;
            } else {
                $peso = 8;
                $soma = 0;
                for ($i = 0; $i <= 5; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                }
                $soma += ie[7] * 2;
                $i = $soma % $modulo;
                if ($modulo == 10) {
                    if ($i == 0) {
                        $dig = 0;
                    } else {
                        $dig = $modulo - $i;
                    }
                } else {
                    if ($i <= 1) {
                        $dig = 0;
                    } else {
                        $dig = $modulo - $i;
                    }
                }

                return ($dig == ie[6]);
            }
        }else if (strlen(ie) == 9){
            $segDig1 = array('0', '1', '2', '3', '4', '5', '8');
            $segDig2 = array('6', '7', '9');
            
            
            //$i = substr(ie, 0, 1);
            $i = substr(ie, 1, 1);
                          
            if (in_array($i, $segDig1)) {
                $modulo = 10;
            } else if (in_array($i, $segDig2)) {
                $modulo = 11;
            }
            console.log($modulo);
            $peso = 8;
            $soma = 0;
            for ($i = 0; $i <= 6; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            $i = $soma % $modulo;
            if ($modulo == 10) {
                if ($i == 0) {
                    $dig = 0;
                } else {
                    $dig = $modulo - $i;
                }
            }else {
                if ($i <= 1) {
                    $dig = 0;
                } else {
                    $dig = $modulo - $i;
                }
            }
            if (!($dig == ie[8])) {
                return 0;
            } else {
                $peso = 9;
                $soma = 0;
                for ($i = 0; $i <= 6; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                }
                $soma += ie[8] * 2;
                $i = $soma % $modulo;
                if ($modulo == 10) {
                    if ($i == 0) {
                        $dig = 0;
                    } else {
                        $dig = $modulo - $i;
                    }
                } else {
                    if ($i <= 1) {
                        $dig = 0;
                    } else {
                        $dig = $modulo - $i;
                    }
                }

                return ($dig == ie[7]);
            }
        }
    };

 
  //Ceará
    this.validaCE = function (ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            $peso = 9;
            $soma = 0;
            for ($i = 0; $i <= 7; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            $dig = 11 - ($soma % 11);

            if ($dig >= 10) {
                $dig = 0;
            }

            return ($dig == ie[8]);
        }
    };

 // Distrito Federal
    this.validaDF = function(ie) {
        if (strlen(ie) != 13) {
            return 0;
        } else {
            if (substr(ie, 0, 2) != '07') {
                return 0;
            } else {
                $peso = 4;
                $soma = 0;
                for ($i = 0; $i <= 10; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                    if ($peso == 1) {
                        $peso = 9;
                    }
                }
                $dig = 11 - ($soma % 11);
                if ($dig >= 10) {
                    $dig = 0;
                }

                if (!($dig == ie[11])) {
                    return 0;
                } else {
                    $peso = 5;
                    $soma = 0;
                    for ($i = 0; $i <= 11; $i++) {
                        $soma += ie[$i] * $peso;
                        $peso--;
                        if ($peso == 1) {
                            $peso = 9;
                        }
                    }
                    $dig = 11 - ($soma % 11);
                    if ($dig >= 10) {
                        $dig = 0;
                    }

                    return ($dig == ie[12]);
                }
            }
        }
    };
    
  //Espirito Santo
    this.validaES = function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            $peso = 9;
            $soma = 0;
            for ($i = 0; $i <= 7; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            $i = $soma % 11;
            if ($i < 2) {
                $dig = 0;
            } else {
                $dig = 11 - $i;
            }

            return ($dig == ie[8]);
        }
    };
    
  
  //Goias
    this.validaGO = function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            $n = substr(ie, 0, 7);

            if ($n == 11094402) {
                if (ie[8] != 0) {                    
                        return 0;                  
                } else if (ie[8] != 1){
                    return 0;
                }
                else {
                    return 1;
                }
            } else {
                $peso = 9;
                $soma = 0;
                for ($i = 0; $i <= 7; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                }
                $i = $soma % 11;
                if ($i == 0) {
                    $dig = 0;
                } else {
                    if ($i == 1) {
                        if (($n >= 10103105) && ($n <= 10119997)) {
                            $dig = 1;
                        } else {
                            $dig = 0;
                        }
                    } else {
                        $dig = 11 - $i;
                    }
                }

                return ($dig == ie[8]);
            }
        }
    };
    
    
 // Maranhão
    this.validaMA = function (ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            if (substr(ie, 0, 2) != 12) {
                return 0;
            } else {
                $peso = 9;
                $soma = 0;
                for ($i = 0; $i <= 7; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                }
                $i = $soma % 11;
                if ($i <= 1) {
                    $dig = 0;
                } else {
                    $dig = 11 - $i;
                }

                return ($dig == ie[8]);
            }
        }
    };

 // Mato Grosso
    this.validaMT = function(ie) {
    	
    	
    	ie = str_pad(ie, 11, "0", 'STR_PAD_LEFT');
    	
    	if (strlen(ie) != 11) {
            return 0;
        } else {
            $peso = 3;
            $soma = 0;
            for ($i = 0; $i <= 9; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
                if ($peso == 1) {
                    $peso = 9;
                }
            }
            $i = $soma % 11;
            if ($i <= 1) {
                $dig = 0;
            } else {
                $dig = 11 - $i;
            }

            return ($dig == ie[10]);
        }
    };
    
    
 // Mato Grosso do Sul
    this.validaMS = function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            if (substr(ie, 0, 2) != 28) {
                return 0;
            } else {
                $peso = 9;
                $soma = 0;
                for ($i = 0; $i <= 7; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                }
                $i = $soma % 11;
                if ($i == 0) {
                    $dig = 0;
                } else {
                    $dig = 11 - $i;
                }

                if ($dig > 9) {
                    $dig = 0;
                }

                return ($dig == ie[8]);
            }
        }
    };

  //Minas Gerais
  this.validaMG = function(ie) {
        if (strlen(ie) != 13) {
            return 0;
        } else {
            ie2 =  substr(ie, 0, 3) + '0' +   substr(ie, 3);

            $peso = 1;
            $soma = "";
            for ($i = 0; $i <= 11; $i++) {
                $soma +=  ie2[$i] * $peso;
                $peso++;
                if ($peso == 3) {
                    $peso = 1;
                }
            }
            $s = 0;
            for ($i = 0; $i < strlen($soma); $i++) {
                $s += $soma[$i];
            }
            $i = substr($s, 0, 1);
            //$i = $s[0]+1;
            $i = ($i+1)+'0';
            
            $dig = $i - $s;
           console.log($i + ' ' +$s + ' ' + $dig + ' ' + ie[11]);
           
                $peso = 3;
                $soma = 0;
                for ($i = 0; $i <= 11; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                    if ($peso == 1) {
                        $peso = 11;
                    }
                }
                $i = $soma % 11;
                if ($i < 2) {
                    $dig = 0;
                } else {
                    $dig = 11 - $i;
                };

                return ($dig == ie[12]);
            
        }
    };

  //Pará
    this.validaPA = function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            if (substr(ie, 0, 2) != 15) {
                return 0;
            } else {
                $peso = 9;
                $soma = 0;
                for ($i = 0; $i <= 7; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                }
                $i = $soma % 11;
                if ($i <= 1) {
                    $dig = 0;
                } else {
                    $dig = 11 - $i;
                }

                return ($dig == ie[8]);
            }
        }
    };
    
  //Paraíba
    this.validaPB = function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            $peso = 9;
            $soma = 0;
            for ($i = 0; $i <= 7; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            $i = $soma % 11;
            if ($i <= 1) {
                $dig = 0;
            } else {
                $dig = 11 - $i;
            }

            if ($dig > 9) {
                $dig = 0;
            }

            return ($dig == ie[8]);
        }
    };
    
  //Paraná
    this.validaPR = function(ie) {
        if (strlen(ie) != 10) {
            return 0;
        } else {
            $peso = 3;
            $soma = 0;
            for ($i = 0; $i <= 7; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
                if ($peso == 1) {
                    $peso = 7;
                }
            }
            $i = $soma % 11;
            if ($i <= 1) {
                $dig = 0;
            } else {
                $dig = 11 - $i;
            }

            if (!($dig == ie[8])) {
                return 0;
            } else {
                $peso = 4;
                $soma = 0;
                for ($i = 0; $i <= 8; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                    if ($peso == 1) {
                        $peso = 7;
                    }
                }
                $i = $soma % 11;
                if ($i <= 1) {
                    $dig = 0;
                } else {
                    $dig = 11 - $i;
                }

                return ($dig == ie[9]);
            }
        }
    };
 
  //Pernambuco
    this.validaPE = function(ie) {
        if (strlen(ie) == 9) {
            $peso = 8;
            $soma = 0;
            for ($i = 0; $i <= 6; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            $i = $soma % 11;
            if ($i <= 1) {
                $dig = 0;
            } else {
                $dig = 11 - $i;
            }

            if (!($dig == ie[7])) {
                return 0;
            } else {
                $peso = 9;
                $soma = 0;
                for ($i = 0; $i <= 7; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso--;
                }
                $i = $soma % 11;
                if ($i <= 1) {
                    $dig = 0;
                } else {
                    $dig = 11 - $i;
                }

                return ($dig == ie[8]);
            }
        } else if (strlen(ie) == 14) {
            $peso = 5;
            $soma = 0;
            for ($i = 0; $i <= 12; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
                if ($peso == 0) {
                    $peso = 9;
                }
            }
            $dig = 11 - ($soma % 11);
            if ($dig > 9) {
                $dig = $dig - 10;
            }

            return ($dig == ie[13]);
        } else {
            return 0;
        }
    };

  //Piauí
   this.validaPI = function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            $peso = 9;
            $soma = 0;
            for ($i = 0; $i <= 7; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            $i = $soma % 11;
            if ($i <= 1) {
                $dig = 0;
            } else {
                $dig = 11 - $i;
            }
            if ($dig >= 10) {
                $dig = 0;
            }

            return ($dig == ie[8]);
        }
    };
    
    
 // Rio de Janeiro
    this.validaRJ = function (ie) {
        if (strlen(ie) != 8) {
            return 0;
        } else {
            $peso = 2;
            $soma = 0;
            for ($i = 0; $i <= 6; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
                if ($peso == 1) {
                    $peso = 7;
                }
            }
            $i = $soma % 11;
            if ($i <= 1) {
                $dig = 0;
            } else {
                $dig = 11 - $i;
            }

            return ($dig == ie[7]);
        }
    };
    
  //Rio Grande do Norte
    this.validaRN = function(ie) {
        if (!( (strlen(ie) == 9) || (strlen(ie) == 10) )) {
            return 0;
        } else {
            $peso = strlen(ie);
            if ($peso == 9) {
                $s = 7;
            } else {
                $s = 8;
            }
            $soma = 0;
            for ($i = 0; $i <= $s; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            $soma *= 10;
            $dig = $soma % 11;
            if ($dig == 10) {
                $dig = 0;
            }

            $s += 1;
            return ($dig == ie[$s]);
        }
    };
    
 // Rio Grande do Sul
    this.validaRS = function(ie) {
        if (strlen(ie) != 10) {
            return 0;
        } else {
            $peso = 2;
            $soma = 0;
            for ($i = 0; $i <= 8; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
                if ($peso == 1) {
                    $peso = 9;
                }
            }
            $dig = 11 - ($soma % 11);
            if ($dig >= 10) {
                $dig = 0;
            }

            return ($dig == ie[9]);
        }
    };
    
 // Rondônia
   this.validaRO = function(ie) {
	   
	   	if(strlen(ie) < 9){
	   		ie = str_pad(ie,9,'0','STR_PAD_LEFT');
	   		
	   	}
	   
        if (strlen(ie) == 9) {
            $peso = 6;
            $soma = 0;
            for ($i = 3; $i <= 7; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            $dig = 11 - ($soma % 11);
            if ($dig >= 10) {
                $dig = $dig - 10;
            }

            return ($dig == ie[8]);
        } else if (strlen(ie) == 14) {
            $peso = 6;
            $soma = 0;
            for ($i = 0; $i <= 12; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
                if ($peso == 1) {
                    $peso = 9;
                }
            }
            $dig = 11 - ( $soma % 11);
            if ($dig > 9) {
                $dig = $dig - 10;
            }

            return ($dig == ie[13]);
        } else {
            return 0;
        }
    };
    
    
  //Roraima
    this.validaRR = function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            if (substr(ie, 0, 2) != 24) {
                return 0;
            } else {
                $peso = 1;
                $soma = 0;
                for ($i = 0; $i <= 7; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso++;
                }
                $dig = $soma % 9;

                return ($dig == ie[8]);
            }
        }
    };
    
    
  //Santa Catarina
    this.validaSC = function(ie) {
    	if (strlen(ie) != 9) {
            return 0;
        } else {
            peso = 9;
            soma = 0;
            for (i = 0; i <= 7; i++) {
               soma += ie[i] * peso;
                peso--;
            }
            
            
            dig = 11 - (soma % 11);
            
            // Alteração pois uma inscrição estadual válida estava sendo considerada inválida.
			//if (dig <= 1) {
            //
			/** Quando o resto da divisão for igual a 0 ou 1 será considerado como 0 (Zero)*/
			if ( (soma % 11) <= 1) {
				dig = 0;
            }
            return (dig == ie[8]);
        }
    };
    
    
  //São Paulo
    this.validaSP = function(ie) {
        if (strtoupper(substr(ie, 0, 1)) == 'P') {
            if (strlen(ie) != 13) {
                return 0;
            } else {
                $peso = 1;
                $soma = 0;
                for ($i = 1; $i <= 8; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso++;
                    if ($peso == 2) {
                        $peso = 3;
                    }
                    if ($peso == 9) {
                        $peso = 10;
                    }
                }
                $dig = $soma % 11;
                return ($dig == ie[9]);
            }
        } else {
            if (strlen(ie) != 12) {
                return 0;
            } else {
                $peso = 1;
                $soma = 0;
                for ($i = 0; $i <= 7; $i++) {
                    $soma += ie[$i] * $peso;
                    $peso++;
                    if ($peso == 2) {
                        $peso = 3;
                    }
                    if ($peso == 9) {
                        $peso = 10;
                    }
                }
                $dig = $soma % 11;
                if ($dig > 9) {
                	
                	console.log($dig);
                	var a = $dig.toString();
                	
                	
                	$dig = a[1];
                }
                console.log('out');
                if ($dig != ie[8]) {
                    return 0;
                } else {
                	
                	console.log('in');
                    $peso = 3;
                    $soma = 0;
                    for ($i = 0; $i <= 10; $i++) {
                        $soma += ie[$i] * $peso;
                        $peso--;
                        if ($peso == 1) {
                            $peso = 10;
                        }
                    }
                    $dig = $soma % 11;

                    if ($dig > 9) {                   
                    	var a = $dig.toString();
                    	$dig = a[1];
                   
                    }
                    
                    
                    return ($dig == ie[11]);
                }
            }
        }
    };
    
    

  //Sergipe
    this.validaSE = function(ie) {
        if (strlen(ie) != 9) {
            return 0;
        } else {
            $peso = 9;
            $soma = 0;
            for ($i = 0; $i <= 7; $i++) {
                $soma += ie[$i] * $peso;
                $peso--;
            }
            $dig = 11 - ($soma % 11);
            if ($dig > 9) {
                $dig = 0;
            }

            return ($dig == ie[8]);
        }
    };
    
  
   //Tocantins
    this.validaTO = function(ie) {
      
    	
    	/*
		 * if (strlen(ie) != 11 && strlen(ie) != 9 ) { return 0; } else
		 * if(strlen(ie) == 11){ $s = substr(ie, 2, 2); if (!( ($s == '01') ||
		 * ($s == '02') || ($s == '03') || ($s == '99') )) { return 0; } else {
		 * $peso = 9; $soma = 0; for ($i = 0; $i <= 9; $i++) { if (!(($i == 2) ||
		 * ($i == 3))) { $soma += ie[$i] * $peso; $peso--; } } $i = $soma % 11;
		 * if ($i < 2) { $dig = 0; } else { $dig = 11 - $i; }
		 * 
		 * return ($dig == ie[10]); } }else{
		 *  }
		 */
    	
    	
    	 if ((strlen(ie) != 9) && (strlen(ie) != 11 )){
    	      return false;
    	 }else{
    		 
    	    nro = ie;
    	    b=9;
    	    soma=0;
    	    for (i=0; i < 8; i++ ){
    	      soma = soma + ( nro[i] * b );
    	      b--;
    	    }
    	    
    	    ver = soma % 11;
    	    
    		dig = 0;
    	    
    	    if ( ver >= 2 ){
    	      dig = 11 - ver;
    	    }
    	    
    	    if(dig == nro[8]){
    	      return true;
    	    }
    	    
    	  
    	   
    	    b=9;
    	    soma=0;
    	    s = substr(ie, 2, 2);
    	   
    	    
    	    if ((s == '01') || (s == '02') || (s == '03') || (s == '99')){
    	    	
    	   	
    	    	
    	      for (i=0; i <= 10; i++)
    	      {
    	    	  
    	    	
    	    	console.log('i: '+i+ ' N: '+ nro[i] );
    	        if ((i != 2) && (i != 3))
    	        {
    	        
    	        
    	          soma = soma + ( nro[i] * b );
    	          b--;
    	        }
    	      }
    	      
    	     
    	      resto = soma % 11;
    	      if (resto >= 2 ){
    	    	  dig = 11 - resto;
    	      }else{    	    	  
    	    	  dig = 0;
    	      }
    	     
    	      
    	      
    	      if (dig == nro[10])
    	        return true;
    	    }
    	    return false;
    	  }
    	  // Tocantins
    	
    };
    this.validaIE =  function (ie, uf) {
    	  if (strtoupper(ie) == 'ISENTO') {
              return 1;
          } else {
             uf = strtoupper(uf);
               
             ie = str_replace(array('\\','\/', ',', ':', '[', ']', '(',
                     ')', '-', '.'),'',ie);
            
             //Executa o comando de acordo com o estado selecionado.
             eval('resultado = this.valida'+uf+'(\''+ie+'\')');
             console.log('Validando I.E: '+ie+ ' - Estado: '+uf);
             console.log(' - Resultado: '+ ((resultado == 0)?'Inválido': 'Válido') );
             
             
             return resultado;
          }
    	
    };
    
    
    
    
   
}


