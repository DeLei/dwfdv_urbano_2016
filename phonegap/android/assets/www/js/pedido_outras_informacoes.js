$(document).ready(function(){
	
	
	
	$('[name=data_entrega]').change(function(){
		var data_sugerida = data_normal2protheus(obter_valor_sessao('data_entrega_sugerida'));
		var data_informada = data_normal2protheus($(this).val());
		console.log('S: '+data_sugerida +' I: '+data_informada);
		
		if(data_informada < data_sugerida)
		{
			mensagem('A <b>data de entrega</b> informada é menor que a data calculada pelo sistema. A Logistica irá avaliar a data de entrega digitada, mas não há garantia que a mesma seja atendida.');
		}
	});
	
	// Desativar todos os campos quando clicar em id_informacao_cliente
	
	$('#id_outras_informacoes').click(function(){
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select',2);
		
		ativar_tabs();
	});

	/**
	* 
	* tipo_frete
	*
	* Descrição:	Utilizado para obter o tipo frete
	*
	*/
	$('select[name=tipo_frete]').change(function(){
		obter_dirigido_redespacho($(this).val());
	});
	
	
	/**
	* 
	* ID:			trocar_cliente
	*
	* Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
	*
	*/
	$('#trocar_cliente_entrega').live('click', function(){
		
		salvar_sessao('descricao_cliente_entrega', null);
		salvar_sessao('codigo_cliente_entrega', null);
		salvar_sessao('loja_cliente_entrega', null);
		
		$('input[name="cliente_entrega"]').val("");
		$('input[name="codigo_cliente_entrega"]').val("");
		$('input[name="loja_cliente_entrega"]').val("");
		
		$('.endereco_entrega').html('N/A');
		$('.bairro_entrega').html('N/A');
		$('.cep_entrega').html('N/A');
		$('.cidade_entrega').html('N/A');
		$('.estado_entrega').html('N/A');
		
		$('input[name="cliente_entrega"]').removeAttr('disabled');
		
	});
	
	
	$('#avancar_passo_3').click(function(e){
		e.preventDefault();
		
		
		
		/* Removido para a Etapa Informações do Cliente
		if(!$('select[name=tipo_frete]').val())
		{
			mensagem('Selecione um <b>Tipo de Frete</b>.');
		}
		else if(!$('input[name=codigo_transportadora]').val())
		{
			mensagem('Selecione uma <b>Transportadora</b>.');
		}
		else*/ 
		if(!$('input[name=codigo_cliente_entrega]').val() && !obter_valor_sessao('codigo_prospect'))
		{
			mensagem('Selecione um <b>Cliente de Entrega</b>.');
		}else if(!$('input[name=data_entrega]').val()){
			mensagem('Informe a <b>Data de Entrega Sugerida</b>.');
		}else if( $('input[name=data_entrega]').val().length != 10 ||  !isValidDate($('input[name=data_entrega]').val())){
			mensagem('<b>Data de Entrega Sugerida</b> inválida.');
					
		}else if( !dataMaiorQue(data_normal2protheus($('input[name=data_entrega]').val()))){
			mensagem('<b>Data de Entrega Sugerida</b> não pode ser menor que a data atual.');
					
		}else if(obter_valor_sessao('tipo_venda') == '02' && !trim($('[name=observacao_bonificacao]').val()))
		{
			mensagem('Informe o <b>Motivo da Bonificação</b>.');
		}
		else if(obter_valor_sessao('tipo_venda') == '02' && trim($('[name=observacao_bonificacao]').val()).length < 15)
		{
			mensagem('O <b>Motivo da Bonificação</b> deve conter pelo menos 15 caracteres.');
		}
		else if(obter_valor_sessao('tipo_venda') == '20' && !trim($('[name=observacao_troca]').val()))
		{
			mensagem('Informe o <b>Motivo da Troca</b>.');
		}
		else if(obter_valor_sessao('tipo_venda') == '20' && trim($('[name=observacao_troca]').val()).length < 15)
		{
			mensagem('O <b>Motivo da Troca</b> deve conter pelo menos 15 caracteres.');
		}
		else
		{
		
			//
			salvar_dados_transportadoras($('input[name=codigo_transportadora]').val());
		
			//
			salvar_sessao('observacao_comercial', $('textarea[name=observacao_comercial]').val());
			//salvar_sessao('mennota', $('textarea[name=mennota]').val());
			
			//
			salvar_sessao('pedido_cliente', $('input[name=pedido_cliente]').val());
			salvar_sessao('data_entrega', $('input[name=data_entrega]').val());
			salvar_sessao('codigo_cliente_entrega', $('input[name=codigo_cliente_entrega]').val());
			
			//Pedido de bonificação
			if(obter_valor_sessao('tipo_venda') == '02')
			{
				salvar_sessao('descricao_motivo', $('[name=observacao_bonificacao]').val());
			}
			//Pedido de troca
			else if(obter_valor_sessao('tipo_venda') == '20')
			{
				salvar_sessao('descricao_motivo', $('[name=observacao_troca]').val());
			}
			else
			{
				salvar_sessao('descricao_motivo', '');
			}
			
			// Ativando Botao "Finalizar"
			$('#id_finalizar').removeClass('tab_desativada');
			
			ativar_tabs();
			
			//Chamando as funções do arquivo finalizar
			finalizar();
			
			// Acionando (Click) botao "finalizar"
			$("#id_finalizar").click();
		}
		
		
		
	});
	
	// Pegando valores da sessao e adicionado no html
	$('input[name=pedido_cliente]').val(obter_valor_sessao('pedido_cliente') ? obter_valor_sessao('pedido_cliente') : '');
	$('input[name=data_entrega]').val(obter_valor_sessao('data_entrega') ? obter_valor_sessao('data_entrega') : date('d/m/Y'));
	$('input[name=codigo_cliente_entrega]').val(obter_valor_sessao('codigo_cliente_entrega') ? obter_valor_sessao('codigo_cliente_entrega') : '');
	//$('textarea[name=mennota]').val(obter_valor_sessao('mennota')? obter_valor_sessao('mennota') : '');
	$('textarea[name=observacao_comercial]').val(obter_valor_sessao('observacao_comercial') ? obter_valor_sessao('observacao_comercial') : '');
	
	

});

function outras_informacoes()
{

	obter_eventos();
	
	obter_tipo_frete();
	
	obter_dirigido_redespacho(obter_valor_sessao('dirigido_redespacho'));
	
	obter_transportadoras(obter_valor_sessao('filial'));
	
	obter_clientes_entrega();
	
	buscar_data_entrega();
	
	//Se for um pedido de bonificação, exibe o campo de observação
	if(obter_valor_sessao('tipo_venda') == '02'){
		$('#menbonificacao').show();
	}else{
		$('#menbonificacao').hide();
	}
	
	if(obter_valor_sessao('tipo_venda') == '20'){
		$('#mentroca').show();
	}else{
		$('#mentroca').hide();
	}
	
	// Rotina para orçamento e prospect(Não Não existe cliente de entrega para prospect.)
	if(obter_valor_sessao('codigo_prospect'))
	{
		$('input[name=cliente_entrega]').val('Não existe cliente de entrega para prospect.');
		$('input[name=cliente_entrega]').attr('disabled', 'disabled');
		$('#trocar_cliente_entrega').hide();
	}
	else
	{
		//Não é permitido alterar o cliente de entrega
		//$('#trocar_cliente_entrega').show();
	}
	

}


/**
* Metódo:		obter_eventos
* 
* Descrição:	Função Utilizada para retornar Eventos
* 
* Data:			24/10/2013
* Modificação:	24/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_eventos()
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM eventos', [], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=evento]').append('<option value="">Selecione...</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=evento]').append('<option value="' + dado.id + '">' + dado.nome + '</option>');
				}
				
				// Selecionar campo se existir na sessão
				if(obter_valor_sessao('evento'))
				{
					$('select[name="evento"] option[value="' + obter_valor_sessao('evento') + '"]').attr('selected', 'selected');
				}
				
			}
		);
	});
}




/**
* Metódo:		obter_tipo_frete
* 
* Descrição:	Função Utilizada para retornar Tipos de Frete
* 
* Data:			24/10/2013
* Modificação:	24/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tipo_frete()
{
	$('select[name=tipo_frete]').empty();
	$('select[name=tipo_frete]').append('<option value="">SELECIONE...</option>');
	$('select[name=tipo_frete]').append('<option value="F">FOB</option>');
	$('select[name=tipo_frete]').append('<option value="C">CIF</option>');
	
	
	var tipo_frete 	= obter_valor_sessao('tipo_frete');
	
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		var sessao_pedido 	= [];		
			sessao_pedido 	= unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		
		if(sessao_pedido.dados_cliente)
		{
			//------------------------
			// DEBUG
			//------------------------
			console.log('Tipo Frete: '+ sessao_pedido.dados_cliente.tipo_frete);
			console.log('Transportadora: '+ sessao_pedido.dados_cliente.transportadora);
			//------------------------
			// DEBUG
			//------------------------
			if(sessao_pedido.dados_cliente.tipo_frete)
			{
				tipo_frete = sessao_pedido.dados_cliente.tipo_frete;
			}
		}	
	}
	
	
	// Selecionar campo se existir na sessão
	if(obter_valor_sessao('tipo_frete'))
	{
		$('select[name="tipo_frete"] option[value="' + obter_valor_sessao('tipo_frete') + '"]').attr('selected', 'selected');

		//Alterar
	//	obter_dirigido_redespacho(obter_valor_sessao('tipo_frete'));
	}
	else
	{
		$('select[name="tipo_frete"] option[value="' + tipo_frete + '"]').attr('selected', 'selected');

		//Alterar
	//	obter_dirigido_redespacho(tipo_frete);
	}
}

/**
* Metódo:		obter_dirigido_redespacho
* 
* Descrição:	Função Utilizada para retornar tipo de frete (Dirigido ou Redespacho)
* 
* Data:			24/10/2013
* Modificação:	24/10/2013
* 
* @access		public
* @param		string 			tipo	- Tipo de Frete
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_dirigido_redespacho(tipo)
{
	if(tipo)
	{
		$('select[name=dirigido_redespacho]').empty();
		$('select[name=dirigido_redespacho]').removeAttr('disabled');
	
		//if(tipo == 'F')
		//{
			//$('select[name=dirigido_redespacho]').append('<option value="0">Não Dirigido</option>');
			//$('select[name=dirigido_redespacho]').append('<option value="1">Dirigido</option>');
		//}
		//else
		//{
			$('select[name=dirigido_redespacho]').append('<option value="0">Sem Redespacho</option>');
			$('select[name=dirigido_redespacho]').append('<option value="1">Redespacho</option>');
		//}
		
		// Selecionar campo se existir na sessão
		if(obter_valor_sessao('dirigido_redespacho'))
		{
			$('select[name="dirigido_redespacho"] option[value="' + obter_valor_sessao('dirigido_redespacho') + '"]').attr('selected', 'selected');
		}
		
		$('select[name=dirigido_redespacho]').change();
	}
	else
	{
		$('select[name=dirigido_redespacho]').html('<option value="0">Selecione Tipo de Frete</option>');
		$('select[name=dirigido_redespacho]').attr('disabled', 'disabled');
	}
}


//////////////////////////////////////////////////////////////////////////////
// TRANSPORTADORAS
//////////////////////////////////////////////////////////////////////////////

/**
* Metódo:		obter_transportadoras
* 
* Descrição:	Função Utilizada para retornar Transportadoras
* 
* Data:			24/10/2013
* Modificação:	24/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_transportadoras(filial)
{
	if(info.empresa)
	{
		var wheres = " WHERE empresa = '" + info.empresa + "'";
	}
	if(filial)
	{
		wheres += " AND (filial = '" + filial + "' OR filial = '')";
	}
	
	console.log('TRANSPORTADORAS: ');
	console.log('SELECT * FROM transportadoras ' + wheres);


	var transportadora 	= '';
	
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		var sessao_pedido 	= [];		
			sessao_pedido 	= unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
			
		if(sessao_pedido.dados_cliente)
		{
			//------------------------
			// DEBUG
			//------------------------		
			console.log('Transportadora: '+ sessao_pedido.dados_cliente.transportadora);
			//------------------------
			// DEBUG
			//------------------------
			if(sessao_pedido.dados_cliente.transportadora)
			{
				transportadora = sessao_pedido.dados_cliente.transportadora;
			}
		}
	}
	
	// Selecionar campo se existir na sessão
	if(!transportadora)
	{
		if(obter_valor_sessao('codigo_transportadora'))
		{
			transportadora = obter_valor_sessao('codigo_transportadora');
		}
		
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM transportadoras ' + wheres, [], function(x, dados) {
				if (dados.rows.length)
				{
					var transportadoras = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						transportadoras.push({ 
												label: 		dado.codigo + ' - ' + dado.nome + ' - Telefone: '+(dado.telefone ? dado.telefone : 'N/A'), 
												codigo:	 	dado.codigo, 
												nome:	 	dado.nome
											});
						
						//Selecionar transportadora padrão
						if(transportadora == dado.codigo)
						{
							var label = dado.codigo + ' - ' + dado.nome + ' - Telefone: '+(dado.telefone ? dado.telefone : 'N/A');
							$('input[name=transportadora]').val(label);
							$('input[name=codigo_transportadora]').val(dado.codigo);
							$('input[name=nome_transportadora]').val(dado.nome);
							
							salvar_sessao('transportadora', 		label);
							salvar_sessao('codigo_transportadora', 	dado.codigo);
							salvar_sessao('nome_transportadora', 	dado.nome);
							
							$('input[name=transportadora]').attr('disabled', 'disabled');
							$('#trocar_transportadora').show();
							
							console.log('-----------------------');
							console.log('TROCAR 1');
							console.log('-----------------------');
							
						}
					}
					
					buscar_transportadoras(transportadoras);
					
					// Selecionar campo se existir na sessão
					if(obter_valor_sessao('codigo_transportadora'))
					{
						$('input[name=codigo_transportadora]').val(obter_valor_sessao('codigo_transportadora'));
						$('input[name=nome_transportadora]').val(obter_valor_sessao('nome_transportadora'));						
					}					
				}
			}
		);
	});

}
/**
* Metódo:		buscar_transportadoras
* 
* Descrição:	Função Utilizada para buscar as transportadoras digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos as transportadoras 
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_transportadoras(transportadoras)
{
	console.log('Transportadoras: ');
	console.log(transportadoras);
	
	$('input[name=transportadora]').autocomplete({
		minLength: 3,
		source: transportadoras,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=transportadora]').val(ui.item.label);
			$('input[name=codigo_transportadora]').val(ui.item.codigo);
			$('input[name=nome_transportadora]').val(ui.item.nome);
			
			salvar_sessao('transportadora', ui.item.label);
			salvar_sessao('codigo_transportadora', ui.item.codigo);
			salvar_sessao('nome_transportadora', ui.item.nome);

			$('input[name=transportadora]').attr('disabled', 'disabled');
			$('#trocar_transportadora').show();
			
			return false;
		}
	});
}
/**
* 
* ID:			trocar_cliente
*
* Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
*
*/
$('#trocar_transportadora').live('click', function(){
	
	salvar_sessao('transportadora', null);
	salvar_sessao('codigo_transportadora', null);
	salvar_sessao('nome_transportadora', null);

	$('input[name="transportadora"]').val("");
	$('input[name="codigo_transportadora"]').val("");
	$('input[name="nome_transportadora"]').val("");
	
	$('input[name=transportadora]').removeAttr('disabled');
	
	console.log('-----------------------');
	console.log('TROCAR 2');
	console.log('-----------------------');
	$(this).hide();	
});
//////////////////////////////////////////////////////////////////////////////
//TRANSPORTADORAS
//////////////////////////////////////////////////////////////////////////////

/**
* Metódo:		obter_clientes_entrega
* 
* Descrição:	Função Utilizada para retornar Todos os Clientes
* 
* Data:			25/10/2013
* Modificação:	25/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_clientes_entrega()
{

	if(info.empresa)
	{
		var where = " WHERE empresa = '" + info.empresa + "'";
	}

	db.transaction(function(x) {
		x.executeSql('SELECT * FROM clientes ' + where, [], function(x, dados) {
				if (dados.rows.length)
				{
					var clientes = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						clientes.push({ label: dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf, codigo: dado.codigo, loja: dado.loja, endereco: dado.endereco, bairro: dado.bairro, cep: dado.cep, cidade: dado.cidade, estado: dado.estado});
					}
					
					buscar_clientes_entrega(clientes);
					
					// Selecionar campo se existir na sessão
					if(obter_valor_sessao('codigo_cliente_entrega'))
					{
						$('input[name=cliente_entrega]').val(obter_valor_sessao('descricao_cliente_entrega'));
						$('input[name=codigo_cliente_entrega]').val(obter_valor_sessao('codigo_cliente_entrega'));
						$('input[name=loja_cliente_entrega]').val(obter_valor_sessao('loja_cliente_entrega'));
						
						exibir_informacoes_cliente_entrega(obter_valor_sessao('codigo_cliente_entrega'), obter_valor_sessao('loja_cliente_entrega'));
						
						// Bloquear campo quando selecionar cliente
						$('input[name=cliente_entrega]').attr('disabled', 'disabled');
					}
				
				}
			}
		);
	});

}

/**
* Metódo:		buscar_clientes_entrega
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_clientes_entrega(clientes)
{
	$('input[name=cliente_entrega]').autocomplete({
		minLength: 3,
		source: clientes,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=cliente_entrega]').val(ui.item.label);
			$('input[name=codigo_cliente_entrega]').val(ui.item.codigo);
			$('input[name=loja_cliente_entrega]').val(ui.item.loja);
			
			salvar_sessao('descricao_cliente_entrega', ui.item.label);
			salvar_sessao('codigo_cliente_entrega', ui.item.codigo);
			salvar_sessao('loja_cliente_entrega', ui.item.loja);
			
			exibir_informacoes_cliente_entrega(ui.item.codigo, ui.item.loja);
			
			// Bloquear campo quando selecionar cliente
			$('input[name=cliente_entrega]').attr('disabled', 'disabled');

			return false;
		}
	});
}


/**
* Metódo:		exibir_informacoes_cliente_entrega
* 
* Descrição:	Função Utilizada para exibir endereço do cliente de entrega
* 
* Data:			26/10/2013
* Modificação:	26/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_informacoes_cliente_entrega(codigo, loja)
{
	if(codigo && loja)
	{
		if(info.empresa)
		{
			var where = " AND empresa = '" + info.empresa + "'";
		}
	
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM clientes WHERE codigo = ? AND loja = ? ' + where, [codigo, loja], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						$('.endereco_entrega').html(dado.endereco ? dado.endereco : 'N/A');
						$('.bairro_entrega').html(dado.bairro ? dado.bairro : 'N/A');
						$('.cep_entrega').html(dado.cep ? dado.cep : 'N/A');
						$('.cidade_entrega').html(dado.cidade ? dado.cidade : 'N/A');
						$('.estado_entrega').html(dado.estado ? dado.estado : 'N/A');
						
					}
					
				}
			);
		});
	}
}




/**
* Metódo:		salvar_dados_transportadoras
* 
* Descrição:	Função Utilizada para salvar dados da transportadora
* 
* Data:			07/11/2013
* Modificação:	07/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo forma de pagamento
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_transportadoras(codigo)
{
	if(codigo)
	{
		if(info.empresa)
		{
			var where = " AND empresa = '" + info.empresa + "'";
		}
	
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM transportadoras WHERE codigo = ? ' + where, [codigo], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);

						salvar_sessao('dados_transportadoras', dado);
						
					}
					
				}
			);
		});
	}
}




function buscar_data_entrega()
{
	var dados_cliente = obter_valor_sessao('dados_cliente');
	var tempo_administracao = 0;
	console.log(JSON.stringify(dados_cliente));
	console.log('Buscando data de entrega');
	
	
	//SQL para obter a rota
	var buscar_rota = '';
	buscar_rota += 'SELECT * ';
	buscar_rota += 'FROM rotas ';
	buscar_rota += 'JOIN zonas_setor ON zonas_setor_codigo_rota = rota_codigo_rota AND zonas_setor_filial = "' + obter_valor_sessao('filial') + '" ';
	//buscar_rota += 'JOIN prazos_entrega ON rota_codigo_rota = prazo_codigo_rota AND prazo_filial = "' + obter_valor_sessao('filial') + '"';
	
	//Primeiro busca exclusiva por cliente, se não encontrar busca por faixa de CEP
	var busca_por_cliente = 'JOIN clientes_setor ON clientes_setor_codigo_zona = zonas_setor_codigo_zona AND clientes_setor_codigo_setor = zonas_setor_codigo_setor AND clientes_setor_codigo_cliente = ? AND clientes_setor_loja_cliente = ? ';
	var buscar_por_faixa_cep = 'JOIN clientes_setor ON clientes_setor_codigo_zona = zonas_setor_codigo_zona AND clientes_setor_codigo_setor = zonas_setor_codigo_setor AND clientes_setor_cep_inicial <= ? AND clientes_setor_cep_final >= ? ';
	
	console.log('por cliente');
	
	console.log(buscar_rota + busca_por_cliente + 'WHERE rota_filial = "' + obter_valor_sessao('filial') + '"');
	console.log('Por CEP');
	console.log(buscar_rota + buscar_por_faixa_cep + 'WHERE rota_filial = "' + obter_valor_sessao('filial') + '"');
	
	db.transaction(function(x) {
		x.executeSql(buscar_rota + busca_por_cliente + 'WHERE rota_filial = "' + obter_valor_sessao('filial') + '"', [dados_cliente.codigo, dados_cliente.est_codigo_loja], function(x, dados) {
			if(dados.rows.length)
			{
				var dado = dados.rows.item(0);
				console.log('-----------------------------');
				console.log('ROTA POR CLIENTE');
				console.log(dado);
				console.log('-----------------------------');
				
				//Grava a rota do cliente para salvar na ZW
				salvar_sessao('codigo_rota', dado.rota_codigo_rota);
				
				db.transaction(function(x) {
					x.executeSql('SELECT * FROM prazos_entrega WHERE prazo_filial = ? AND prazo_codigo_rota = ?', [obter_valor_sessao('filial'), dado.rota_codigo_rota], function(x, dados) {
						if(dados.rows.length)
						{
							var dado = dados.rows.item(0);
							
							obter_data_entrega(dado.prazo_tempo_administracao, dado.prazo_tempo_entrega);
						}else{
							obter_data_entrega(0, 0);
						}
					})
				});
			}
			else
			{
				x.executeSql(buscar_rota + buscar_por_faixa_cep + 'WHERE rota_filial = "' + obter_valor_sessao('filial') + '"', [dados_cliente.cep, dados_cliente.cep], function(x, dados) {
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						console.log('-----------------------------');
						console.log('ROTA POR CEP');
						console.log(dado);
						console.log('-----------------------------');
						
						//Salva a rota do cliente para salvar na ZW
						salvar_sessao('codigo_rota', dado.rota_codigo_rota);
						
						db.transaction(function(x) {
							x.executeSql('SELECT * FROM prazos_entrega WHERE prazo_filial = ? AND prazo_codigo_rota = ?', [obter_valor_sessao('filial'), dado.rota_codigo_rota], function(x, dados) {
								if(dados.rows.length)
								{
									console.log('IN');
									var dado = dados.rows.item(0);
									
									obter_data_entrega(dado.prazo_tempo_administracao, dado.prazo_tempo_entrega);
								}else{
									obter_data_entrega(0, 0);
									
								}
							})
						});
					}
					else
					{
						console.log('-----------------------------');
						console.log('NENHUMA ROTA ENCONTRADA')
						salvar_sessao('codigo_rota', '');
						console.log('-----------------------------');
					}
				});
			}
			
			//SE O TRANSPORTE FOR MARITMO OBTEM O TEMPO DE ENTREGA DA TABELA DE PORTOS
			var tipo_transporte = obter_valor_sessao('tipo_transporte').split('', 1);
			if(tipo_transporte == 'M')
			{
				db.transaction(function(x) {
					x.executeSql('SELECT * FROM portos_destino WHERE porto = ?', [obter_valor_sessao('porto_destino')], function(x, dados) {
						if(dados.rows.length)
						{
							var dado_porto = dados.rows.item(0);
							console.log('-----------------------------');
							console.log('TRANSPORTE MARITIMO');
							console.log(dado_porto);
							console.log('-----------------------------');
							obter_data_entrega(tempo_administracao, dado_porto.tempo_entrega);
						}
					})
				});
			}
			
		});
	});
}

function obter_data_entrega(tempo_administracao, tempo_entrega)
{
	console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
	
	var timestamp_entrega = time();
	
	console.log('EMISSAO: ' + date('d/m/Y', timestamp_entrega ));
	
	//Liberação crédito e comercial
	timestamp_entrega = adicionar_dias_uteis(timestamp_entrega, 1);
	
	
	
	console.log('CREDITO (+ 1 DIA S/ SAB. e DOM.): ' + date('d/m/Y', timestamp_entrega ));
	
	//Tempo de administração
	timestamp_entrega = adicionar_dias_uteis(timestamp_entrega, tempo_administracao);
	console.log('ADMINISTRACAO (+ ' + tempo_administracao + ' Dia(s) S/ SAB. e DOM.): ' + date('d/m/Y', timestamp_entrega ));
	
	//Tempo de entrega (contando sabados)
	timestamp_entrega = adicionar_dias_uteis(timestamp_entrega, tempo_entrega);
	console.log('ENTREGA (+ ' + tempo_entrega + ' S/ SAB. e DOM.): ' + date('d/m/Y', timestamp_entrega ));
	
	console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
	
	
	// 002369 - Dt Entrega não esta funcionando
	/* Aplicada solução conforme DtEntrega_REVISADOdocx.docx
	 */
	
	obter_tempo_entrega_maritimo(function(tempo_porto){		
	
		//Tempo de entrega (contando sabados)
		timestamp_entrega = adicionar_dias_uteis(timestamp_entrega, tempo_porto);
		
		console.log('Timestamp Entrega: '+timestamp_entrega  );
				
		console.log('PORTO (+ ' + tempo_entrega + ' S/ SAB. e DOM.): ' + date('d/m/Y', timestamp_entrega ));
				
		$('input[name=data_entrega]').val(date('d/m/Y', timestamp_entrega));
	
		console.log('Entrega Selecionada: '+ obter_valor_sessao('data_entrega'));
		
		
	
		salvar_sessao('data_entrega_sugerida', date('d/m/Y', timestamp_entrega));
		
		if(!!obter_valor_sessao('data_entrega')){
			console.log('Alterada data para: '+ obter_valor_sessao('data_entrega'));
			$('input[name=data_entrega]').val(obter_valor_sessao('data_entrega'));
			$('input[name=data_entrega]').change();
		}
		console.log('data_entrega_sugerida: '+ timestamp_entrega);

	});
	
	
	// FIm chamado 002369 - Dt Entrega não esta funcionando
}



function obter_tempo_entrega_maritimo(callback){
	//SE O TRANSPORTE FOR MARITMO OBTEM O TEMPO DE ENTREGA DA TABELA DE PORTOS
	var tipo_transporte = obter_valor_sessao('tipo_transporte').split('', 1);
	if(tipo_transporte == 'M'  )
	{
		
		
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM portos_destino WHERE porto = ?', [obter_valor_sessao('porto_destino')], function(x, dados) {
				if(dados.rows.length)
				{
					var dado_porto = dados.rows.item(0);
					console.log('-----------------------------');
					console.log('TRANSPORTE MARITIMO');
					console.log(dado_porto);
					console.log('-----------------------------');
					console.log(dado_porto.tempo_entrega);
					
					callback(dado_porto.tempo_entrega);
				}else{
					callback(0);
				}
			})
		});
	}else{
		callback(0);
	}
	
	
}