$(document).ready(function() {
	// obter notícias
	
	obter_noticias(sessionStorage['noticias_pagina_atual'], sessionStorage['noticias_ordem_atual'], sessionStorage['noticias_criacao_inicial'], sessionStorage['noticias_criacao_final'], sessionStorage['noticias_titulo']);
	
	//Ordenação do grid
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_noticias(sessionStorage['noticias_pagina_atual'], $(this).data('campo') + ' DESC', sessionStorage['noticias_criacao_inicial'], sessionStorage['noticias_criacao_final'], sessionStorage['noticias_titulo']);
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_noticias(sessionStorage['noticias_pagina_atual'], $(this).data('campo') + ' ASC', sessionStorage['noticias_criacao_inicial'], sessionStorage['noticias_criacao_final'], sessionStorage['noticias_titulo']);
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	//Filtrar dados
	$('input[name=titulo]').val(sessionStorage['noticias_titulo']);
	$('input[name=criacao_inicial]').val(sessionStorage['noticias_criacao_inicial']);
	$('input[name=criacao_final]').val(sessionStorage['noticias_criacao_final']);

	$('#filtrar').click(function() {
		
		sessionStorage['noticias_pagina_atual'] 		= 1;
		sessionStorage['noticias_titulo'] 			= $('input[name=titulo]').val();
		sessionStorage['noticias_criacao_inicial'] 	= $('input[name=criacao_inicial]').val();
		sessionStorage['noticias_criacao_final'] 	= $('input[name=criacao_final]').val();
		
		obter_noticias(sessionStorage['noticias_pagina_atual'], sessionStorage['noticias_ordem_atual'], sessionStorage['noticias_criacao_inicial'], sessionStorage['noticias_criacao_final'], sessionStorage['noticias_titulo']);
	});
	
	$('#limpar_filtros').click(function() {
		
		sessionStorage['noticias_pagina_atual'] 		= 1;
		sessionStorage['noticias_titulo'] 			= '';
		sessionStorage['noticias_criacao_inicial'] 	= '';
		sessionStorage['noticias_criacao_final'] 	= '';

		$('input[name=titulo]').val(sessionStorage['noticias_titulo']);
		$('input[name=criacao_inicial]').val(sessionStorage['noticias_criacao_inicial']);
		$('input[name=criacao_final]').val(sessionStorage['noticias_criacao_final']);
		
		obter_noticias(sessionStorage['noticias_pagina_atual'], sessionStorage['noticias_ordem_atual'], sessionStorage['noticias_criacao_inicial'], sessionStorage['noticias_criacao_final'], sessionStorage['noticias_titulo']);
	});
	
	//Abrir notícia
	$('#ver_noticia').live('click', function() {
		var id = $(this).attr('data-id');
		
		console.log('id = ' + id);
		
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM noticias WHERE id = ?', [id], function(x, dados) {
					var noticia = dados.rows.item(0);
					
					var html = '<table cellspacing="5">';
						html += '<tr><td><h2><img src="img/news-icon.png" width="25"> ' + noticia.titulo + '</h2></td></tr>';
						html += '<tr><td>' + (noticia.conteudo) + '</td></tr>';
						html += '</table>';
					
					if(window.navigator.onLine && noticia.link_imagem)
					{
						html += '<img src="' + config.url_online + noticia.link_imagem + '" />';
					}
					//$.colorbox({ html: html, maxWidth: '995px' });
					exibirMensagem('dialog_noticias', html, '', 'Detalhes');
				}
			);
		});
		
		return false;
	});
	
});

function obter_noticias(pagina, ordem, dt_inicial, dt_final, titulo)
{
	//Filtros
	pagina 				= pagina 		?	pagina 			: 1;
	ordem 				= ordem 		?	ordem 			: 'timestamp DESC';
	titulo 				= titulo 		?	titulo 			: '';
	criacao_inicial 	= dt_inicial 	?	dt_inicial 		: '';
	criacao_final 		= dt_final 		? 	dt_final 		: '';
	
	// setar pagina e ordem atual
	sessionStorage['noticias_pagina_atual'] 	= pagina;
	sessionStorage['noticias_ordem_atual'] 		= ordem;
	sessionStorage['noticias_titulo'] 			= titulo;
	sessionStorage['noticias_criacao_inicial'] 	= criacao_inicial;
	sessionStorage['noticias_criacao_final'] 	= criacao_final;
	
	// definir seta
	var ordem = explode(' ', sessionStorage['noticias_ordem_atual']);
	
	if (ordem[1] == 'ASC')
	{
		$('a[data-campo=' + ordem[0] + ']').data('ordem', 'ASC');
		
		$('a[data-campo=' + ordem[0] + ']').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo=' + ordem[0] + ']').data('ordem', 'DESC');
		
		$('a[data-campo=' + ordem[0] + ']').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['noticias_pagina_atual'] - 1) * 20;
	
	//Where
	var wheres = '';
	
	if (titulo)
	{
		wheres += 'AND titulo LIKE "%' + titulo + '%"';
	}
	
	if (criacao_inicial && criacao_inicial != 'undefined')
	{	
		var ex = explode('/', criacao_inicial);
		
		wheres += ' AND timestamp >= "' + mktime(0, 0, 0, ex[1], ex[0], ex[2]) + '"';
	}

	if (criacao_final && criacao_final != 'undefined')
	{
		var ex = explode('/', criacao_final);
	
		wheres += ' AND timestamp <= "' + mktime(23, 59, 59, ex[1], ex[0], ex[2]) + '"';
	}
	
	// exibir pedido e calcular totais
	// 	id	timestamp	status	link_imagem	titulo	conteudo
	console.log('SELECT * FROM noticias WHERE id > 0 ' + wheres + ' ORDER BY ' + sessionStorage['noticias_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM noticias WHERE id > 0 ' + wheres + ' ORDER BY ' + sessionStorage['noticias_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
				if (dados.rows.length)
				{
					var noticias_visualizadas = new Array();
					$('table tbody').empty();

					for (i = 0; i < dados.rows.length; i++)
					{
						
						var noticia = dados.rows.item(i);
						
						var conteudo = trim(strip_tags(noticia.conteudo));
						
						var html = '';
							//html += '<td align="center">'+date('d/m/Y H:i:s', noticia.timestamp)+'</td>';
							//html += '<td>'+noticia.titulo+'</td>';
							//html += '<td><a href="#" id="ver_noticia" data-id="' + noticia.id + '">Ver Detalhes</a></td>';
							
							
						html += '<li>';					
							html += '<div class="titulo">';
								html += '<span class="data">' + date('d/m/Y', noticia.timestamp) + '</span> - ' + noticia.titulo;
							html += '</div>';
							
							html += '<div class="descricao">';
								html += conteudo.substring('0', '250');
								html += (strlen(conteudo) > '250' ? '...' : '');
							html += '</div>';
							
							if(strlen(conteudo) > '250' || (window.navigator.onLine && noticia.link_imagem))
							{
								html += '<div class="vermais">';
									html += '<a href="#" id="ver_noticia" data-id="' + noticia.id + '" class="btn btn-large btn-primary">Ler notícia</a>';
								html += '</div>';
							}
						html += '</li>';
						
						
						$('.noticias').append(html);
						
						//alterarCabecalhoTabelaResolucao();
						
						noticias_visualizadas.push(noticia.id); //Adiciona a noticia ao array de noticias visualizadas
					}
					localStorage.setItem('noticias_visualizadas', noticias_visualizadas);
				}
				else
				{
					$('table tbody').html('<tr><td colspan="3" style="color: #900; padding: 10px;"><strong>Nenhuma notícia encontrada.</strong></td></tr>');
				}
			});
		
		// calcular totais
		x.executeSql(
				'SELECT COUNT(DISTINCT id) AS total FROM noticias WHERE id != "" ' + wheres, [], function(x, dados) {
				var dado = dados.rows.item(0);

				$('#total').text(number_format(dado.total, 0, ',', '.'));
				// paginação

				$('#paginacao').html('');

				var total = ceil(dado.total / 20);

				if (sessionStorage['noticias_pagina_atual'] > 6)
				{
					obter_noticias(sessionStorage['noticias_pagina_atual'], sessionStorage['noticias_ordem_atual'], sessionStorage['noticias_criacao_inicial'], sessionStorage['noticias_criacao_final'], sessionStorage['noticias_titulo']);
					$('#paginacao').append('<a href="#" onclick="obter_noticias(1, \'' + sessionStorage['noticias_pagina_atual'] + '\', \'' + sessionStorage['noticias_ordem_atual'] + '\', \'' + sessionStorage['noticias_criacao_inicial'] + '\', \'' + sessionStorage['noticias_criacao_final'] + '\', \'' + sessionStorage['noticias_titulo'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
				}

				if (sessionStorage['noticias_pagina_atual'] > 1)
				{
					$('#paginacao').append('<a href="#" onclick="obter_noticias(' + (intval(sessionStorage['noticias_pagina_atual']) - 1) + ', \'' + sessionStorage['noticias_ordem_atual'] + '\', \'' + sessionStorage['noticias_criacao_inicial'] + '\', \'' + sessionStorage['noticias_criacao_final'] + '\', \'' + sessionStorage['noticias_titulo'] + '\'); return false;">&lt;</a> ');
				}

				for (i = intval(sessionStorage['noticias_pagina_atual']) - 6; i <= intval(sessionStorage['noticias_pagina_atual']) + 5; i++)
				{
					if (i <= 0 || i > total)
					{
						continue;
					}

					if (i == sessionStorage['noticias_pagina_atual'])
					{
						$('#paginacao').append('<strong>' + i + '</strong> ');
					}
					else
					{
						$('#paginacao').append('<a href="#" onclick="obter_noticias(' + i + ', \'' + sessionStorage['noticias_ordem_atual'] + '\', \'' + sessionStorage['noticias_criacao_inicial'] + '\', \'' + sessionStorage['noticias_criacao_final'] + '\', \'' + sessionStorage['noticias_titulo'] + '\'); return false;">' + i + '</a> ');
					}
				}

				if (sessionStorage['noticias_pagina_atual'] < total)
				{
					$('#paginacao').append('<a href="#" onclick="obter_noticias(' + (intval(sessionStorage['noticias_pagina_atual']) + 1) + ', \'' + sessionStorage['noticias_ordem_atual'] + '\', \'' + sessionStorage['noticias_criacao_inicial'] + '\', \'' + sessionStorage['noticias_criacao_final'] + '\', \'' + sessionStorage['noticias_titulo'] + '\'); return false;">&gt;</a> ');
				}

				if (sessionStorage['noticias_pagina_atual'] <= total - 6)
				{
					$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_noticias(' + total + ', \'' + sessionStorage['noticias_pagina_atual'] + '\', \'' + sessionStorage['noticias_ordem_atual'] + '\', \'' + sessionStorage['noticias_criacao_inicial'] + '\', \'' + sessionStorage['noticias_criacao_final'] + '\', \'' + sessionStorage['noticias_titulo'] + '\'); return false;">Última Página</a> ');
				}
		});
	});
}