function adicionar_produtos()
{

	// Exibir valores do cabeçalho
	exibir_cabecalho();
	
	// Obter Produtos (Autocomplete)
	obter_produtos();
	
	exibir_produtos();
	
	
	$('#trocar_produto').live('click', function(){
		
		$('input[name="produto"]').val("");
		$('input[name="codigo_produto"]').val("");
		$('input[name="descricao_produto"]').val("");
		$('input[name="preco_unitario"]').val("");
		$('input[name="preco_venda"]').val("");
		$('input[name="quantidade"]').val("");
		$('input[name="desconto"]').val("");
		$('input[name="ipi"]').val("");
		$('input[name="total_item"]').val("");
		
		$(this).hide();
		$('input[name="produto"]').removeAttr('disabled');
		$('#erro_item').hide();
		
	});
	
	
	//---------------------------
	//--------------------
	//-----------
	//---

	
	$('#adicionar_produto').live('click', function(e){
		e.preventDefault();
		
		var campos = [];
		campos['codigo_produto'] 	= 'input[name=codigo_produto]';
		campos['preco_venda'] 		= 'input[name=preco_venda]';
		campos['quantidade'] 		= 'input[name=quantidade]';
		campos['desconto'] 			= 'input[name=desconto]';

		
		if(validar_campos(null, serialize(campos)))
		{
					
			var codigo_produto 		= $('input[name=codigo_produto]').val();
			var descricao_produto 	= $('input[name=descricao_produto]').val();
			var preco_unitario 		= converter_decimal($('input[name=preco_unitario]').val());
			var preco_venda 		= converter_decimal($('input[name=preco_venda]').val());
			var quantidade 			= $('input[name=quantidade]').val();
			var desconto 			= converter_decimal($('input[name=desconto]').val());
			var ipi 				= converter_decimal($('input[name=ipi]').val());
			
			
			var produtos = new Array();
			produtos[codigo_produto] = new Array();
			produtos[codigo_produto]['codigo'] 			= codigo_produto;
			produtos[codigo_produto]['descricao'] 		= descricao_produto;
			produtos[codigo_produto]['preco_unitario'] 	= preco_unitario;
			produtos[codigo_produto]['preco_venda'] 	= preco_venda;
			produtos[codigo_produto]['quantidade'] 		= quantidade;
			produtos[codigo_produto]['desconto'] 		= desconto;
			produtos[codigo_produto]['ipi'] 			= ipi;
			
			var produtos = serialize(produtos);
			
			salvar_produto_sessao(produtos);
			
			exibir_produtos();
			
			$('#erro_item').hide('fast');
		}

		
	});
	
	
	//---------------------------
	//--------------------
	//-----------
	//---
	
	
	calculos_automaticos('input[name=preco_venda]', 'input[name=preco_unitario]', 'input[name=desconto]', 'input[name=quantidade]', 'input[name=ipi]', 'input[name=total_item]');
	
	
	//---
	//-----------
	//--------------------
	//---------------------------
	
	
	
	
	$('#salvar').live('click', function(){
	
		$('.erro_itens').remove();
		
		var sessao_produtos = obter_produtos_sessao();
		var sucesso = true;
		
		if(sessao_produtos)
		{	
			$.each(sessao_produtos, function(key, objeto){
			
				var codigo_produto 	= objeto.codigo;
				var desconto 		= converter_decimal($('input[name=editar_desconto_' + codigo_produto + ']').val());
				var preco_venda 	= converter_decimal($('input[name=editar_preco_venda_' + codigo_produto + ']').val());
				var quantidade 		= converter_decimal($('input[name=editar_quantidade_' + codigo_produto + ']').val());
				
				// Zerando sempre o background
				$('.produto_' + codigo_produto).css('background-color', '');
				
				var produtos = new Array();
				produtos[codigo_produto] = new Array();
				produtos[codigo_produto]['codigo'] 			= codigo_produto;
				produtos[codigo_produto]['descricao'] 		= objeto.descricao;
				produtos[codigo_produto]['preco_unitario'] 	= objeto.preco_unitario;
				produtos[codigo_produto]['preco_venda'] 	= preco_venda;
				produtos[codigo_produto]['quantidade'] 		= quantidade;
				produtos[codigo_produto]['desconto'] 		= desconto;
				produtos[codigo_produto]['ipi'] 			= objeto.ipi;
				
				var produtos = serialize(produtos);
				
				salvar_produto_sessao(produtos);
				
				//-----------------//
				//--- Validação ---//
				//-----------------//
				
				
				var campos = [];
				campos['desconto'] 		= 'input[name=editar_desconto_' + codigo_produto + ']';
				campos['preco_venda'] 	= 'input[name=editar_preco_venda_' + codigo_produto + ']';
				campos['quantidade'] 	= 'input[name=editar_quantidade_' + codigo_produto + ']';
				
				if(!validar_campos(codigo_produto, serialize(campos)))
				{
					sucesso = false;
				}
				
			});

		}
	});
	
	
}


/**
* Metódo:		calculos_automaticos
* 
* Descrição:	Função Utilizada realizar os calculos automaticos de desconto e preço via keyup para tudo 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			campo_preco_venda		-	campo input do preco de venda
* @param		string 			campo_preco_unitario	-	campo input do preco unitario
* @param		string 			campo_desconto			-	campo input do desconto
* @param		string 			campo_quantidade		-	campo input do quantidade
* @param		string 			campo_ipi				-	campo input do ipi
* @param		string 			campo_total_geral		-	campo input do total geral
* @param		string 			campo_total_item		-	campo input do ptotal do item
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function calculos_automaticos(campo_preco_venda, campo_preco_unitario, campo_desconto, campo_quantidade, campo_ipi, campo_total_geral, campo_total_item)
{
	
	// Calcular Desconto
	$(campo_preco_venda).keyup(function(){
		var preco_unitario 			=  converter_decimal($(campo_preco_unitario).val());
		var preco_venda 			=  converter_decimal($(this).val());
		
		var valor_diferenca 		= preco_unitario - preco_venda;
		var percentagem 			= valor_diferenca / preco_unitario * 100;
		
		if(percentagem < 0)
		{
			percentagem = 0;
		}
		
		// Iserindo percentagem no campo
		$(campo_desconto).val(number_format(percentagem, 3, ',', '.'));
		
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);
	});
	
	// Calcular Preço de Venda pelo Desconto
	$(campo_desconto).keyup(function(){
		var preco_unitario 			= converter_decimal($(campo_preco_unitario).val());
		var desconto 				= converter_decimal($(this).val());
		
		var percentagem 			= desconto / 100;
		var valor_diferenca 		= preco_unitario * percentagem;
		var preco_venda 			= preco_unitario - valor_diferenca;
		
		if(preco_venda < 0)
		{
			preco_venda = 0;
		}
		
		// Iserindo preco_venda no campo
		$(campo_preco_venda).val(number_format(preco_venda, 3, ',', '.'));
		
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);
		
	});
	
	// Calcular Preço Total pela quantidade
	$(campo_quantidade).keyup(function(){
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);
	});
}


/**
* Metódo:		validar_campos
* 
* Descrição:	Função Utilizada para exibir os erros 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo_produto 	- codigo do produto usado para identificar qual linha da coluna sera inserido o erro
* @param		string 			mensagem_erro	- mensagem do erro
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function validar_campos(codigo_produto, campos)
{
	var campos = unserialize(campos);
	var sucesso = true;
	
	// Se desconto for vazio, inserir o valor 0
	if(!$(campos['desconto']).val())
	{
		$(campos['desconto']).val(number_format(0, 3, ',', '.'));
	}
	
	//------------------
	console.log('CP = ' + $(campos['codigo_produto']).val());
	console.log('CAMPO CP = ' + campos['codigo_produto']);
	
	if(!$(campos['codigo_produto']).val() && campos['codigo_produto']) // Validação do Produto
	{
		erro_itens('Selecione um Produto.');
		
		sucesso = false;
	}
	else if(!validar_decimal($(campos['preco_venda']).val())) // Validação do Preço de Venda
	{
		if($(campos['preco_venda']).val())
		{
			erro_itens('O valor "<b>' + $(campos['preco_venda']).val() + '</b>" no campo <b>Preço de Venda</b> não é um valor decimal válido.<br /><br />Exemplo de valor decimal válido: <b>1.100,250<b/>', codigo_produto);
			
			sucesso = false;
		}
		else
		{
			erro_itens('Digite um valor para o <b>Preço de Venda</b>.', codigo_produto);
			
			sucesso = false;
		}
	}
	else if(!validar_digitos($(campos['quantidade']).val()) || $(campos['quantidade']).val() <= 0) // Validação da Quantidade
	{
		if($(campos['quantidade']).val() > 0)
		{
			erro_itens('O valor "<b>' + $(campos['quantidade']).val() + '</b>" no campo <b>Quantidade</b> não contém apenas digitos.', codigo_produto);
			
			sucesso = false;
		}
		else
		{
			erro_itens('Digite uma quantidade.', codigo_produto);
			
			sucesso = false;
		}
	}
	else if(!validar_decimal($(campos['desconto']).val())) // Validação do Desconto
	{
		erro_itens('O valor "<b>' + $(campos['desconto']).val() + '</b>" no campo <b>Desconto</b> não é um valor decimal válido.<br /><br />Exemplo de valor decimal válido: <b>50,250<b/>', codigo_produto);
		
		sucesso = false;
	}
	else if(converter_decimal($(campos['desconto']).val()) > 100) // Validação do Desconto
	{
		erro_itens('O valor "<b>' + $(campos['desconto']).val() + '</b>" no campo <b>Desconto</b> não pode ser maior que 100.', codigo_produto);
		
		sucesso = false;
	}
	
	
	return sucesso;
}


/**
* Metódo:		erro_itens
* 
* Descrição:	Função Utilizada para exibir os erros 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo_produto 	- codigo do produto usado para identificar qual linha da coluna sera inserido o erro
* @param		string 			mensagem_erro	- mensagem do erro
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function erro_itens(mensagem_erro, codigo_produto)
{
	if(codigo_produto)
	{
		$('<tr class="erro_itens" style="background-color: #F0798E"><td colspan="9">' + mensagem_erro + '</td></tr>').insertAfter($('.produto_' + codigo_produto));
	
		$('.produto_' + codigo_produto).css('background-color', '#F0798E');
	}
	else
	{
		console.log('MSG = ' + mensagem_erro);
	
		$('#erro_item').html(mensagem_erro);
		$('#erro_item').show('fast');
	}
}


/**
* Metódo:		obter_produtos_sessao
* 
* Descrição:	Função Utilizada para obter os itens inseridos na sessão
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos_sessao()
{
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage['sessao_pedido'])
	{
		sessao_pedido = unserialize(sessionStorage['sessao_pedido']);
	}
	
	if(sessao_pedido['produtos'])
	{
		return sessao_pedido['produtos'];
	}
	else
	{
		return false;
	}
}


/**
* Metódo:		exibir_produtos
* 
* Descrição:	Função Utilizada para exibir os itens dos produtos inseridos na sessao
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_produtos()
{
	
	var sessao_produtos = obter_produtos_sessao();
	
	if(sessao_produtos)
	{
		
		$('.itens_pedido').empty();
		
		$.each(sessao_produtos, function(key, objeto){
			var html = '';
			html += '<td align="center">' + objeto.codigo + '</td>';
			html += '<td>' + objeto.descricao + '</td>';
			html += '<td align="right">' + number_format(objeto.preco_unitario, 3, ',', '.') + '<input type="hidden" name="editar_preco_unitario_' + objeto.codigo + '" value="' + number_format(objeto.preco_unitario, 3, ',', '.') + '" /></td>';
			html += '<td align="center"><input name="editar_desconto_' + objeto.codigo + '" type="text" size="13" value="' + number_format(objeto.desconto, 3, ',', '.') + '" /></td>';
			html += '<td align="center"><input name="editar_preco_venda_' + objeto.codigo + '" type="text" size="13" value="' + number_format(objeto.preco_venda, 3, ',', '.') + '" /></td>';
			html += '<td align="center"><input name="editar_quantidade_' + objeto.codigo + '" type="text" size="8" value="' + objeto.quantidade + '" /></td>';
			html += '<td align="right"><input name="editar_total_item_' + objeto.codigo + '" type="text" size="13" value="' + number_format(objeto.preco_venda * objeto.quantidade, 3, ',', '.')  + '" disabled="disabled" /></td>';
			html += '<td align="right">' +  number_format(objeto.ipi, 3, ',', '.') + '<input type="hidden" name="editar_ipi_' + objeto.codigo + '" value="' + number_format(objeto.ipi, 3, ',', '.') + '" /></td>';
			html += '<td align="center"><input name="editar_total_geral_' + objeto.codigo + '" type="text" size="13" value="' + number_format((parseFloat(objeto.preco_venda) + parseFloat(objeto.preco_venda * (objeto.ipi / 100))) * objeto.quantidade, 3, ',', '.')  + '" disabled="disabled" /></td>';
			
			
			$('.itens_pedido').append('<tr class="produto_' + objeto.codigo + '">' + html + '</tr>');
			
			// Chamar calculo automatico (Funções do keyup)
			calculos_automaticos('input[name=editar_preco_venda_' + objeto.codigo + ']', 'input[name=editar_preco_unitario_' + objeto.codigo + ']', 'input[name=editar_desconto_' + objeto.codigo + ']', 'input[name=editar_quantidade_' + objeto.codigo + ']', 'input[name=editar_ipi_' + objeto.codigo + ']', 'input[name=editar_total_geral_' + objeto.codigo + ']', 'input[name=editar_total_item_' + objeto.codigo + ']');
		});
		
	}
	
}


/**
* Metódo:		calcular_total
* 
* Descrição:	Função Utilizada para calcular o valor total dos itens e exibi-los
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			campo_preco_venda - input do preco de venda
* @param		string 			campo_ipi
* @param		string 			campo_quantidade
* @param		string 			campo_total_geral
* @param		string 			campo_total_item
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item)
{
	var preco_venda 			= converter_decimal($(campo_preco_venda).val());
	var ipi 					= converter_decimal($(campo_ipi).val());
	var valor_ipi				= preco_venda * (ipi / 100);
	var quantidade 				= $(campo_quantidade).val();
	
	if(!quantidade)
	{
		quantidade = 1;
	}
	
	// Iserindo total_item no campo
	if(campo_total_item)
	{
		$(campo_total_item).val(number_format(parseFloat(preco_venda) * quantidade, 3, ',', '.'));
	}
	
	$(campo_total_geral).val(number_format((parseFloat(preco_venda) + parseFloat(valor_ipi)) * quantidade, 3, ',', '.'));
}


/**
* Metódo:		obter_produtos
* 
* Descrição:	Função Utilizada para buscar os produtos na tabela de produtos onde a tabela de preços for igual a selecionada
* 
* Data:			17/10/2013
* Modificação:	17/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos()
{
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM produtos WHERE tb_codigo = ?', [obter_valor_sessao('tabela_precos')], function(x, dados) {
				if (dados.rows.length)
				{
					var produtos = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						var preco = aplicar_descontos_cabecalho(dado.ptp_preco);
						
						produtos.push({ label: dado.produto_codigo + ' - ' + dado.produto_descricao + ' - ESTOQUE ATUAL: ' + number_format(dado.etq_quantidade_atual, 0, ',', '.') + ' - PREÇO: R$ ' + number_format(preco, 3, ',', '.'), codigo: dado.produto_codigo, preco: preco, ipi: dado.produto_ipi, descricao: dado.produto_descricao});
					}
					
					buscar_produtos(produtos);

				}
			}
		);
	});
	
}


/**
* Metódo:		buscar_produtos
* 
* Descrição:	Função Utilizada para pesquisar os produtos pelo autocomplete e adicionar os valores nos campso quando o produto for selecionado
* 
* Data:			17/10/2013
* Modificação:	17/10/2013
* 
* @access		public
* @param		array 			var produtos		- Todos os produtos
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_produtos(produtos)
{
	$('input[name=produto]').autocomplete({
		minLength: 3,
		source: produtos,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {

			$('input[name=produto]').val(ui.item.label);
			$('input[name=descricao_produto]').val(ui.item.descricao);
			$('input[name=codigo_produto]').val(ui.item.codigo);
			$('input[name=preco_unitario]').val(number_format(ui.item.preco, 3, ',', '.'));
			$('input[name=preco_venda]').val(number_format(ui.item.preco, 3, ',', '.'));
			$('input[name=ipi]').val(number_format(ui.item.ipi, 3, ',', ''));
			
			calcular_total();
			
			// Bloquear campo quando selecionar cliente
			$('input[name=produto]').attr('disabled', 'disabled');
			$('#trocar_produto').show();
			
			return false;
			
		}
	});
}

function aplicar_descontos_cabecalho(preco_produto)
{
	// Aplicando Desconto do "Cliente" no preço do produto
	if(obter_valor_sessao('desconto_cliente') > 0)
	{
		var preco = preco_produto - (preco_produto * (obter_valor_sessao('desconto_cliente') / 100));
	}
	else
	{
		var preco = preco_produto;
	}

	// Aplicando Desconto da "Regra de desconto" no preço do produto
	if(obter_valor_sessao('regra_desconto')  > 0)
	{
		var preco = preco - (preco * (obter_valor_sessao('regra_desconto') / 100));
	}
	
	return preco;
}


/**
* Metódo:		exibir_cabecalho
* 
* Descrição:	Função Utilizada para exibir os valores do cabeçalho
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_cabecalho()
{
	// ------
	// Obter descrição da FILIAL e adicionar no cabeçalho
	obter_descricao('filiais', 'razao_social', 'codigo', obter_valor_sessao('filial'), '.filial');
	
	
	// ------
	// Obter descrição da TIPO DE PEDIDO e adicionar no cabeçalho
	var tipo_pedido = obter_valor_sessao('tipo_pedido');
	if(tipo_pedido = 'N')
	{
		$('.tipo_pedido').html('Venda Normal');
	}
	else
	{
		$('.tipo_pedido').html('N/A');
	}
	
	
	// ------
	// Obter descrição do CLIENTE e adicionar no cabeçalho
	$('.cliente').html(obter_valor_sessao('descricao_cliente'));
	
	
	// ------
	// Obter descrição da TABELA DE PREÇOS e adicionar no cabeçalho
	obter_descricao('tabelas_preco', 'descricao', 'codigo', obter_valor_sessao('tabela_precos'), '.tabela_precos');
	
	
	// ------
	// Obter descrição da CONDIÇÃO DE PAGAMENTO e adicionar no cabeçalho
	obter_descricao('formas_pagamento', 'descricao', 'codigo', obter_valor_sessao('condicao_pagamento'), '.condicao_pagamento');
	
	
	// ------
	// Obter DESCONTO do CLIENTE e adicionar no cabeçalho
	$('.desconto_cliente').html(number_format(obter_valor_sessao('desconto_cliente'), 3, ',', '.') + ' %');

	
	// ------
	// Obter REGRA DE DESCONTO e adicionar no cabeçalho
	$('.regra_desconto').html(number_format(obter_valor_sessao('regra_desconto'), 3, ',', '.') + ' %');
}

/**
* Metódo:		obter_descricao
* 
* Descrição:	Função Utilizada para obter a descrição e codigo de um valor na sessão
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					tabela				- A tabela do navegador que será utilizada para realizar o "SELECT" do retorna de descrição
* @param		string 					campo_descricao		- O campo "Descrição" da "tabela" passada no primeiro parametro
* @param		string 					campo_codigo		- O campo "Codigo" da "tabela" passada no primeiro parametro
* @param		string 					codigo				- O Codigo que será usado para comparação no "WHERE"
* @param		string 					classe				- Classe html (.classe) que será adiciona o valor do retorno no html
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_descricao(tabela, campo_descricao, campo_codigo, codigo, classe)
{
	
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT ' + campo_descricao + ' AS descricao, ' + campo_codigo + ' AS codigo FROM ' + tabela + ' WHERE ' + campo_codigo + ' = ?', [codigo], function(x, dados) {

				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);
				
					$(classe).html(dado.codigo + ' - ' + dado.descricao);
				}
				else
				{
					$(classe).html('N/A');
				}
				
			}
		);
	});
	
}

/**
* Metódo:		salvar_produto_sessao
* 
* Descrição:	Função Utilizada para salvar produtos adicionados
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produto_sessao(produtos_paramentro)
{
	var produtos_paramentro = unserialize(produtos_paramentro);
	
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage['sessao_pedido'])
	{
		sessao_pedido = unserialize(sessionStorage['sessao_pedido']);
	}
	
	if(sessao_pedido['produtos'])
	{
		var sessao_produtos = sessao_pedido['produtos'];
	
		var produtos = merge_options(sessao_produtos, produtos_paramentro);
	}
	else
	{
		var produtos = produtos_paramentro;
	}
	
	salvar_sessao('produtos', produtos);

}
