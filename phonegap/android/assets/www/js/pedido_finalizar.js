$(document).ready(function(){

	//----------------------------
	// Exibir botão impostos
	//----------------------------
	if(config.ws_url_impostos)
	{
		$('#botao_impostos').show();
	}
	//----------------------------
	// Exibir botão impostos
	//----------------------------
	
	//----------------------------
	// BOTÃO SIMULAR IMPOSTOS
	//----------------------------
	$('#botao_impostos').click(function(e){
		e.preventDefault();
		
		//Sessão do pedido
		var sessao_pedido = [];

		//Obter sessão do pedido
		if(sessionStorage[sessionStorage['sessao_tipo']])
		{
			sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		}
				
		$.ajax({
			url: config.ws_url_impostos,
			type: 'POST',			
			data: {
				retorno: json_encode(sessao_pedido)
			},
			beforeSend: function(){			
				$('#msgImpostos').addClass('info');
				$('#msgImpostos').html('Aguarde simulação impostos em andamento...');
				$('#botao_impostos').attr('disabled', 'disabled');
			},
			success: function(dados) {	
				//-------------------------------
				// ITENS
				//-------------------------------
				console.log(JSON.stringify(dados));
				//-------------------------------
				// ITENS
				//-------------------------------
				
				//-------------------------------
				// OBTER SESSÃO DOS PRODUTOS
				//-------------------------------
				var sessao_produtos 	= obter_produtos_sessao();
				if(sessao_produtos)
				{
					var total_produtos 		= Object.keys(sessao_produtos).length;
				}
				var indice = 0;
				//-------------------------------
				// OBTER SESSÃO DOS PRODUTOS
				//-------------------------------
				
				
				if(dados.sucesso)
				{
					//Verificar itens retornados pelo WS - impostos
					$.each(dados.retorno.produtos, function(keyPedido, imposto){						
							console.log('Produto: '+imposto.PRODUTO);
							console.log('TES: '+imposto.TES);
							console.log('CF: '+imposto.CF);
							console.log('ST: '+imposto.ST);
							console.log('IPI: '+imposto.IPI);
							console.log('ICMS: '+imposto.ICMS);	
							
							//Verificar itens na sessão do pedido
							$.each(sessao_produtos, function(key, produto){
								//Verificar se é o mesmo código
								if(imposto.PRODUTO == produto.codigo)
								{
									console.log('Adicionar impostos a sessão do PRODUTO.');
									var codigo_produto 	= remover_zero_esquerda(produto.codigo);
									
									//--------
									//-------------------
									//--------
								
									var produtos = new Array();
									produtos[codigo_produto] = new Array();
									produtos[codigo_produto]['codigo'] 			= produto.codigo;
									produtos[codigo_produto]['descricao'] 		= produto.descricao;
									produtos[codigo_produto]['preco_tabela'] 	= produto.preco_tabela;
									produtos[codigo_produto]['preco_unitario'] 	= produto.preco_unitario;
									produtos[codigo_produto]['preco_venda'] 	= produto.preco_venda
									produtos[codigo_produto]['quantidade'] 		= produto.quantidade;
									produtos[codigo_produto]['desconto'] 		= produto.desconto;							
									produtos[codigo_produto]['unidade_medida'] 	= produto.unidade_medida;
									produtos[codigo_produto]['local'] 			= produto.local;
									//Impostos
									produtos[codigo_produto]['TES'] 			= imposto.TES;
									produtos[codigo_produto]['CF'] 				= imposto.CF;
									produtos[codigo_produto]['ST'] 				= imposto.ST;
									produtos[codigo_produto]['IPI'] 			= imposto.IPI;
									produtos[codigo_produto]['ICMS'] 			= imposto.ICMS;
									
									var produtos = serialize(produtos);
									
									salvar_produto_sessao(produtos);
									
									//--------
									//-------------------
									//--------
									
									indice++;
									
									if(indice == total_produtos)
									{
										exibir_produtos_finalizar();
										$('#msgImpostos').addClass('success');
										$('#msgImpostos').html('Simulação de impostos realizado com sucesso.');
										$('#botao_impostos').removeAttr('disabled');
									}
								}
								//Verificar se é o mesmo código
							});
						
					});
				}
			},
			error: function(jqXHR, textStatus,errorThrown) {
				$('#msgImpostos').addClass('warning');
				$('#msgImpostos').html('Não foi possível realziar simulação de impostos.');
				$('#botao_impostos').removeAttr('disabled');				
			}
		});
	});
	
	//----------------------------
	// BOTÃO FINALIZAR PEDIDO
	//----------------------------	
	$('#botao_finalizar').click(function(e){
	
		e.preventDefault();
		
		var descricao_tipo_pedido = obter_descricao_pedido();
		
		
		confirmar('Tem certeza que deseja FINALIZAR o ' + descricao_tipo_pedido + ' ?', 
			function () {
				$(this).dialog('close');
				//---------------------------------------------------------
				//-----------------------------------------------------------------------------------------------
				
				salvar_pedido();
				
				//-----------------------------------------------------------------------------------------------
				//---------------------------------------------------------
				$("#confirmar_dialog").remove();
				
			}
		);
	
	});

});

function finalizar()
{

	exibir_produtos_finalizar();
	
	exibir_outras_informacoes();
	
}

function exibir_produtos_finalizar()
{
	
	var sessao_produtos = obter_produtos_sessao();
	
	if(sessao_produtos)
	{
		
		$('.itens_pedido_finalizar').empty();
		
		var total_preco_unitario 	= 0;
		var total_desconto 			= 0;
		var total_preco_venda 		= 0;
		var total_quantidade		= 0;
		var total					= 0;
		var total_ipi				= 0;
		var total_icms 				= 0;
		var total_st 				= 0;
		var total_geral				= 0;
		var total_peso				= 0;
		
		$.each(sessao_produtos, function(key, objeto){
			
			console.log('---------------------');
			console.log('-PRODUTO FINALIZAR');
			console.log('---------------------');
			console.log(JSON.stringify(objeto));
			
			var html = '';
				html += '<td align="center">' + objeto.codigo + '</td>';
				html += '<td>' + objeto.descricao + '</td>';
				html += '<td align="right">' + objeto.quantidade + '</td>';
				html += '<td align="right">' + number_format(objeto.preco_venda, 3, ',', '.') + '</td>';
				html += '<td align="right">' + number_format(objeto.preco_unitario, 3, ',', '.') + '<input type="hidden" name="editar_preco_unitario_' + objeto.codigo + '" value="' + number_format(objeto.preco_unitario, 3, ',', '.') + '" /></td>';
				html += '<td align="right">' + number_format(objeto.desconto, 3, ',', '.') + '</td>';
				
				
				
				html += '<td align="right">' + objeto.peso * objeto.quantidade + '</td>';
				
				//Total sem impostos
				//html += '<td align="right">' + number_format(objeto.preco_venda * objeto.quantidade, 3, ',', '.')  + '</td>';
				
				//Impostos
				//html += '<td align="right">' + (objeto.IPI ? number_format(objeto.IPI, 3, ',', '.') : '0,000') + '</td>';
				//html += '<td align="right">' + (objeto.ICMS ? number_format(objeto.ICMS, 3, ',', '.') : '0,000') + '</td>';
				//html += '<td align="right">' + (objeto.ST ? number_format(objeto.ST, 3, ',', '.') : '0,000') + '</td>';			
				
				//Total com Impostos
				var total_item_impostos = (objeto.preco_venda * objeto.quantidade) + (parseFloat(objeto.IPI) + parseFloat(objeto.ST));
				html += '<td align="right">' + number_format(total_item_impostos, 3, ',', '.')  + '</td>';	
				
				$('.itens_pedido_finalizar').append('<tr class="produto_' + objeto.codigo + '">' + html + '</tr>');
				
			
			// variaveis para os TOTAIS
			total_preco_unitario 	+= parseFloat(objeto.preco_unitario);
			total_preco_venda		+= parseFloat(objeto.preco_venda);
			total_quantidade		+= parseFloat(objeto.quantidade);
			total_ipi				+= parseFloat(objeto.IPI);
			total_icms				+= parseFloat(objeto.ICMS);
			total_st				+= parseFloat(objeto.ST);
			total					+= parseFloat(objeto.preco_venda * objeto.quantidade);
			total_geral				+= parseFloat(objeto.preco_venda * objeto.quantidade);
			total_peso				+= parseFloat(objeto.peso * objeto.quantidade);
		});
		
		total_desconto 			= (total_preco_unitario - total_preco_venda) * 100 / total_preco_unitario;
		//total_ipi				= (total_geral - total) * 100 / total_geral;
		
		
		var descricao_tipo_pedido = obter_descricao_pedido('upper');
		
		var html = '';
		html += '<td colspan="2">TOTAIS DO ' + descricao_tipo_pedido + '</td>';
		html += '<td align="right">' + total_quantidade + '</td>';
		html += '<td align="right">' /*+ number_format(total_preco_venda, 3, ',', '.')*/ + '</td>';
		html += '<td align="right">' /*+ number_format(total_preco_unitario, 3, ',', '.')*/ + '</td>';
		html += '<td align="right">' /*+ number_format(total_desconto, 3, ',', '.')*/ + '</td>';
		
		html += '<td align="right">' + total_peso + '</td>';
		
		//Total sem impostos
		//html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
		
		//Impostos
		//html += '<td align="right">' + number_format(total_ipi, 3, ',', '.')  + '</td>';
		//html += '<td align="right">' + number_format(total_icms, 3, ',', '.')  + '</td>';
		//html += '<td align="right">' +  number_format(total_st, 3, ',', '.')  + '</td>';
		
		//Total com impostos
		html += '<td align="right">' + number_format(total_geral + (total_ipi + total_st), 3, ',', '.') + '</td>';
		
		$('.itens_pedido_finalizar').append('<tr class="novo_grid_rodape">' + html + '</tr>');
		
		alterarCabecalhoListagem('#itens_pedido_finalizar_tabela');
		
	}
	
	
}

function exibir_outras_informacoes()
{
	var sessao_pedido = [];
	
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	$('.pedido_cliente').html(sessao_pedido.pedido_cliente ? sessao_pedido.pedido_cliente : 'N/A');
	$('.data_entrega').html(sessao_pedido.data_entrega ? sessao_pedido.data_entrega : 'N/A');
	obter_evento(sessao_pedido.evento);
	$('.tipo_frete').html(sessao_pedido.tipo_frete == 'F' ? 'FOB' : 'CIF');
	obter_transportadora(sessao_pedido.codigo_transportadora);
	
	$('.cliente_entrega').html(sessao_pedido.descricao_cliente_entrega ? sessao_pedido.descricao_cliente_entrega : 'N/A');

	//$('.mennota').html(sessao_pedido.mennota ? sessao_pedido.mennota : 'N/A');
	$('.observacao_comercial').html(sessao_pedido.observacao_comercial ? sessao_pedido.observacao_comercial : 'N/A');

}

function obter_evento(codigo)
{
	if(codigo)
	{
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM eventos WHERE id = ?', [codigo], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
					
						$('.evento').html(dado.id + ' - ' + dado.nome);
					}
					else
					{
						$('.evento').html('N/A');
					}
					
				}
			);
		});
	}
	else
	{
		$('.evento').html('N/A');
	}
}

function obter_transportadora(codigo)
{
	if(codigo)
	{
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM transportadoras WHERE codigo = ?', [codigo], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
					
						$('.transportadora').html(dado.codigo + ' - ' + dado.nome);
					}
					else
					{
						$('.transportadora').html('N/A');
					}
					
				}
			);
		});
	}
	else
	{
		$('.transportadora').html('N/A');
	}
}

//Função que retorna o item modificiado ( de 1 para 00)
function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}


function salvar_pedido()
{

	var sessao_pedido = [];

	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	
	//Definindo Tabela de pedidos ou or�amentos
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento')
	{
		var tabela 	= 'orcamentos';
		var tipo 	= 'O';
	}
	else
	{
		var tabela 	= 'pedidos_pendentes';
		var tipo 	= 'P';
	}


	var sessao_produtos = sessao_pedido.produtos;
	
	if(sessao_produtos)
	{
		var total_produtos 		= Object.keys(sessao_produtos).length;
	}
	else
	{
		var total_produtos 		= 0;
	}
	
	
	//-----------------
	var dados_cliente 			= sessao_pedido.dados_cliente;
	var dados_prospect			= sessao_pedido.dados_prospect;
	var dados_transportadoras 	= sessao_pedido.dados_transportadoras;
	var dados_forma_pagamento	= sessao_pedido.dados_forma_pagamento;
	var dados_porto_destino		= sessao_pedido.dados_porto_destino;
	var numero_itens 			= 1;
	
	
	if(total_produtos > 0)
	{
	
		var editar = sessao_pedido.editar;
	
		if(editar)
		{
			var id_pedido 		= sessao_pedido.id_pedido;
		}
		else
		{
			var id_pedido 		= uniqid();
		}
		
		
		if(sessao_pedido.converter_pedido_orcamento)
		{
			id_pedido 		= sessao_pedido.id_pedido;
		}
		
		
		var data_emissao 	= date('Ymd');
		var time_emissao 	= time();
		var produtos		= [];
		
		$.each(sessao_produtos, function(key, item){
			console.log('item:');
			console.log(item);
			var campos 			= [];
			var interrogacoes  	= [];
			var valores		 	= [];
			
		
			campos.push('converter_pedido_orcamento');					
			interrogacoes.push('?');
			valores.push(sessao_pedido.converter_pedido_orcamento);
			
			campos.push('empresa');										
			interrogacoes.push('?');
			valores.push(info.empresa);
									
			campos.push('filial');										
			interrogacoes.push('?');
			valores.push(sessao_pedido.filial);
						
			campos.push('pedido_filial');								
			interrogacoes.push('?');
			valores.push(sessao_pedido.filial);
			
			campos.push('pedido_numero_item');							
			interrogacoes.push('?');
			valores.push(String(pad(numero_itens)));
						
			campos.push('pedido_id_pedido'); 							
			interrogacoes.push('?');
			valores.push(id_pedido);
						
			campos.push('pedido_data_emissao'); 						
			interrogacoes.push('?');
			valores.push(data_emissao);
						
			campos.push('pedido_time_emissao'); 						
			interrogacoes.push('?');
			valores.push(time_emissao);
			
			campos.push('pedido_id_usuario'); 							
			interrogacoes.push('?');
			valores.push(info.id_rep);
			
			campos.push('pedido_codigo_representante'); 				
			interrogacoes.push('?');
			valores.push(info.cod_rep);
						
			campos.push('pedido_tabela_precos');						
			interrogacoes.push('?');
			valores.push(sessao_pedido.tabela_precos);
			
			campos.push('pedido_id_feira');								
			interrogacoes.push('?');
			valores.push(sessao_pedido.evento);
			
			campos.push('pedido_codigo_cliente'); 						
			interrogacoes.push('?');
			valores.push(sessao_pedido.codigo_cliente);
		
			campos.push('pedido_loja_cliente');							
			interrogacoes.push('?');
			valores.push(sessao_pedido.loja_cliente);
			
			campos.push('pedido_condicao_pagamento');					
			interrogacoes.push('?');
			valores.push(sessao_pedido.condicao_pagamento);
			
			campos.push('pedido_tipo_frete');							
			interrogacoes.push('?');
			valores.push(sessao_pedido.tipo_frete);
			
			campos.push('pedido_codigo_transportadora');				
			interrogacoes.push('?');
			valores.push(sessao_pedido.codigo_transportadora);
			
			campos.push('pedido_tipo_pedido');							
			interrogacoes.push('?');
			valores.push(sessao_pedido.tipo_pedido);
			
			//campos.push('pedido_mensagem_nota');					
			//interrogacoes.push('?');
			//valores.push(utf8_encode(sessao_pedido.mennota));
			
			campos.push('pedido_observacao_comercial');					
			interrogacoes.push('?');			
			valores.push(utf8_encode(sessao_pedido.observacao_comercial));			
			
			campos.push('pedido_cliente_entrega');						
			interrogacoes.push('?');
			valores.push(sessao_pedido.codigo_cliente_entrega);
			
			campos.push('pedido_loja_entrega');							
			interrogacoes.push('?');
			valores.push(sessao_pedido.loja_cliente_entrega);
			
			campos.push('pedido_data_entrega');							
			interrogacoes.push('?');
			valores.push(data_normal2protheus(sessao_pedido.data_entrega));
			
			campos.push('pedido_pedido_cliente');						
			interrogacoes.push('?');
			valores.push(sessao_pedido.pedido_cliente);
			
			//---
			//Posição da venda
			campos.push('pedido_latitude');								
			interrogacoes.push('?');			
			valores.push(localStorage.getItem('gps_latitude'));
			
			campos.push('pedido_longitude');							
			interrogacoes.push('?');
			valores.push(localStorage.getItem('gps_longitude'));
			
			
			campos.push('pedido_versao');								
			interrogacoes.push('?');
			//Versão			
			valores.push(config.versao);
			
			//---
			
			//CUSTOM 000177/000870
			campos.push('pedido_banco');								
			interrogacoes.push('?');
			valores.push(sessao_pedido.banco);
			
			campos.push('pedido_tipo_venda');							
			interrogacoes.push('?');
			valores.push(sessao_pedido.tipo_venda);
						
			campos.push('pedido_tipo_bonificacao');						
			interrogacoes.push('?');
			valores.push(sessao_pedido.tipo_bonificacao);
						
			campos.push('pedido_tipo_transporte');						
			interrogacoes.push('?');
			valores.push(sessao_pedido.tipo_transporte);
			
			
			
			campos.push('pedido_porto_destino');						
			interrogacoes.push('?');
			campos.push('porto_descricao');								
			interrogacoes.push('?');
			
			if(dados_porto_destino)
			{
				valores.push(dados_porto_destino.porto);
				valores.push(dados_porto_destino.descricao);
			}
			else
			{
				valores.push('');
				valores.push('');
			}			
			
			campos.push('pedido_bloqueado');							
			interrogacoes.push('?');
			valores.push(sessao_pedido.pedido_bloqueado);
			
			
			campos.push('pedido_codigo_rota');							
			interrogacoes.push('?');
			valores.push(sessao_pedido.codigo_rota);
			//FIM CUSTOM 000177/000870
			
			//-- Cliente/Prospect
			if(dados_prospect)
			{
				campos.push('pedido_id_prospects');							
				interrogacoes.push('?');
				valores.push(dados_prospect.cgc);
				
				
				campos.push('prospect_codigo');								
				interrogacoes.push('?');
				valores.push(dados_prospect.codigo);
				
				
				campos.push('prospect_codigo_loja');						
				interrogacoes.push('?');
				valores.push(dados_prospect.codigo_loja);
				
				campos.push('prospect_nome');								
			 	interrogacoes.push('?');
				valores.push(dados_prospect.nome);
				
				campos.push('prospect_cgc');								
				interrogacoes.push('?');
				valores.push(dados_prospect.cgc);
				
				campos.push('prospect_nome_contato');						
				interrogacoes.push('?');
				valores.push(dados_prospect.nome_contato);
				
				campos.push('prospect_cargo_contato');						
				interrogacoes.push('?');
				valores.push(dados_prospect.cargo_contato);
				
				campos.push('prospect_email_contato');						
				interrogacoes.push('?');
				valores.push(dados_prospect.email_contato);
				
				campos.push('prospect_telefone_contato');					
				interrogacoes.push('?');
				valores.push(dados_prospect.telefone_contato);
				
				campos.push('prospect_endereco');							
				interrogacoes.push('?');
				valores.push(dados_prospect.endereco);
				
				campos.push('prospect_bairro');								
				interrogacoes.push('?');
				valores.push(dados_prospect.bairro);
				
				campos.push('prospect_cep');								
				interrogacoes.push('?');
				valores.push(dados_prospect.cep);
				
				campos.push('prospect_codigo_municipio');					
				interrogacoes.push('?');
				valores.push(dados_prospect.codigo_municipio);
				
				campos.push('prospect_estado');								
				interrogacoes.push('?');
				valores.push(dados_prospect.estado);
				
				campos.push('prospect_telefone');							
				interrogacoes.push('?');
				valores.push(dados_prospect.telefone);
				
				campos.push('prospect_email');								
				interrogacoes.push('?');
				valores.push(dados_prospect.email);
			}
			else //Cliente
			{
				campos.push('cliente_codigo');								
				interrogacoes.push('?');
				valores.push(dados_cliente.codigo);
				
				
				
				campos.push('cliente_loja');								
				interrogacoes.push('?');
				valores.push(dados_cliente.loja);
				
				campos.push('cliente_nome');								
				interrogacoes.push('?');
				valores.push(dados_cliente.nome);
				
				campos.push('cliente_cpf');									
				interrogacoes.push('?');
				valores.push(dados_cliente.cpf);
				
				campos.push('cliente_pessoa_contato');						
				interrogacoes.push('?');
				valores.push(dados_cliente.pessoa_contato);
				
				campos.push('cliente_endereco');							
				interrogacoes.push('?');
				valores.push(dados_cliente.endereco);
				
				campos.push('cliente_bairro');								
				interrogacoes.push('?');
				valores.push(dados_cliente.bairro);
				
				campos.push('cliente_cep');									
				interrogacoes.push('?');
				valores.push(dados_cliente.cep);
				
				campos.push('cliente_cidade');								
				interrogacoes.push('?');
				valores.push(dados_cliente.cidade);
				
				campos.push('cliente_estado');								
				interrogacoes.push('?');
				valores.push(dados_cliente.estado);
				
				campos.push('cliente_telefone');							
				interrogacoes.push('?');
				valores.push(dados_cliente.telefone);
				
			 	campos.push('cliente_email');								
				interrogacoes.push('?');
				valores.push(dados_cliente.email);
				
				campos.push('pedido_tipo_cliente');							
				interrogacoes.push('?');
				valores.push(dados_cliente.tipo);
				
				campos.push('pedido_desconto1');							
				interrogacoes.push('?');
				valores.push(sessao_pedido.desconto_cliente);
				
			}
			
		//Transportadoras
		//	campos.push('transportadora_codigo');						
		//	interrogacoes.push('?');
		//	valores.push(dados_transportadoras.codigo);
			
		//	campos.push('transportadora_nome');							
		//	interrogacoes.push('?');
		//	valores.push(dados_transportadoras.nome);
			
			//Formas pagamento
			campos.push('forma_pagamento_codigo');						
			interrogacoes.push('?');
			valores.push(dados_forma_pagamento.codigo);
			
			campos.push('forma_pagamento_descricao');					
			interrogacoes.push('?');
			valores.push(dados_forma_pagamento.descricao);
			
			//Produtos
			campos.push('pedido_codigo_produto');						
			interrogacoes.push('?');
			valores.push(item.codigo);									
			
			campos.push('pedido_preco_unitario');						
			interrogacoes.push('?');
			if(item.preco_tabela)
			{
				valores.push(item.preco_tabela);
			}
			else
			{
				valores.push(item.preco_unitario);
			}
			
			campos.push('pedido_preco_venda');							
			interrogacoes.push('?');
			valores.push(item.preco_venda);
			
			campos.push('pedido_quantidade');							
			interrogacoes.push('?');
			valores.push(item.quantidade);
			
			
			campos.push('pedido_desconto_item');						
			interrogacoes.push('?');
			valores.push(item.desconto);
			
			campos.push('produto_descricao');							
			interrogacoes.push('?');
			valores.push(item.descricao);
			
			campos.push('produto_ipi');									
			interrogacoes.push('?');
			valores.push(item.ipi);
			
			campos.push('pedido_unidade_medida');						
		 	interrogacoes.push('?');
			valores.push(item.unidade_medida);
			
			campos.push('pedido_local');								
			interrogacoes.push('?');
			valores.push(item.local);
			
			//Impostos
			campos.push('pedido_tipo_entrada_saida');					
			interrogacoes.push('?');
			valores.push(item.TES);
		
			campos.push('pedido_codigo_fiscal');						
			interrogacoes.push('?');
			valores.push(item.CF);
			

			campos.push('pedido_st');									
			interrogacoes.push('?');
			valores.push(item.ST);
			
			campos.push('pedido_ipi');									
			interrogacoes.push('?');
			valores.push(item.IPI);
			
			campos.push('pedido_icms');									
			interrogacoes.push('?');
			valores.push(item.ICMS);
			
			campos.push('produto_peso');								
			interrogacoes.push('?');
			valores.push(item.peso);	//Peso do produto
		
			campos.push('pedido_peso');									
			interrogacoes.push('?');
			valores.push(item.peso * item.quantidade);	//Peso total do pedido
			
			// Valores Fixos
			campos.push('pedido_status');								
			interrogacoes.push('?');
			campos.push('pedido_codigo_preposto');						
			interrogacoes.push('?');
			
			if(!!localStorage.getItem('codigo_preposto') &&  localStorage.getItem('codigo_preposto') != 'null'  ){
				valores.push('P');
				valores.push(localStorage.getItem('codigo_preposto'));
				
			}else{			
				valores.push('L');
				valores.push((sessao_pedido.codigo_preposto)?sessao_pedido.codigo_preposto:'');
				
			}
		
			
			campos.push('pedido_codigo_empresa');						
			interrogacoes.push('?');
			valores.push(info.empresa);
									
			campos.push('pedido_forma_pagamento_real');					
			interrogacoes.push('?');
			valores.push(sessao_pedido.forma_pagamento);
			
			campos.push('pedido_motivo_troca');							
			interrogacoes.push('?');
			valores.push((item.motivo_troca ? item.motivo_troca : ''));
			
			campos.push('pedido_lote_troca');							
			interrogacoes.push('?');
			valores.push((item.lote_troca ? item.lote_troca : ''));
			
			
			campos.push('pedido_descricao_motivo');						
			interrogacoes.push('?');
			valores.push((sessao_pedido.descricao_motivo ? sessao_pedido.descricao_motivo : ''));
			//------------------------
			
			
			
			//------------------------------------
			//---- PRODUTOS
			produtos.push(item.codigo);
			//------------------------------------
			
			if(editar)
			{
				
				campos.push('exportado');  interrogacoes.push('?');
				campos.push('editado');  interrogacoes.push('?');
				campos.push('atualizado_em');  interrogacoes.push('?');
				valores.push(null);
				valores.push('1');
				valores.push(date('YmdHis'));
				
				db.transaction(function(x) {
					x.executeSql('UPDATE ' + tabela + ' SET ' + campos.join(' = ?, ') + ' = ? WHERE pedido_id_pedido = "' + id_pedido + '" AND pedido_codigo_produto = "' + item.codigo + '"', valores, function(e, resultado){
						console.log('UPDATE FOI! = ' + id_pedido);
				
						if(!resultado.rowsAffected)
						{
							
							x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
								console.log('FOI! = ' + id_pedido);
								
								total_produtos--;
								
								if(total_produtos == 0)
								{
									
									finalizar_pedido_editado(id_pedido, sessao_pedido.filial, produtos);
								}
							},
							function (e, t){
								console.log(e);
								console.log(t);
							});
							
						}
						else
						{
							total_produtos--;
							
							if(total_produtos == 0)
							{
								
								
								finalizar_pedido_editado(id_pedido, sessao_pedido.filial, produtos);
							}
						}
					},
					function (e, t){
						console.log(e);
						console.log(t);
					});
				});

				
			
			}
			else
			{
			
		
				db.transaction(function(x) {
					x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
						console.log('FOI! = ' + id_pedido);
						
						total_produtos--;
						
						if(total_produtos == 0)
						{
							// Se o sistema estiver transformando orçamento em pedido, exluir orçamento apos 
							if(sessao_pedido.converter_pedido_orcamento)
							{
								x.executeSql('DELETE FROM orcamentos WHERE pedido_id_pedido = "' + id_pedido + '"', [], function(){
		
									// Apagando pedido da sessao
									location.href="pedidos_espelho.html#" + id_pedido + '|' + sessao_pedido.filial + '|' + tipo+'|true';
									sessionStorage[sessionStorage['sessao_tipo']] = '';
									
								});
							}
							else
							{
								
								// Apagando pedido da sessao
								
								location.href="pedidos_espelho.html#" + id_pedido + '|' + sessao_pedido.filial + '|' + tipo+'|true';
								sessionStorage[sessionStorage['sessao_tipo']] = '';
							}
						}
					},
					function (e, t){
						console.log(JSON.stringify(e));
						console.log(JSON.stringify(t));
					});
				});
				
			}

			numero_itens++;
		
		});
		
	}
	

}

function finalizar_pedido_editado(id_pedido, filial, produtos)
{

	//Definindo Tabela de pedidos ou or�amentos
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento')
	{
		var tabela = 'orcamentos';
		var tipo = 'O';
	}
	else
	{
		var tabela = 'pedidos_pendentes';
		var tipo = 'P';
	}

	db.transaction(function(x) {
	
		x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_id_pedido = "' + id_pedido + '" AND pedido_codigo_produto NOT IN ("' + produtos.join('", "') + '")', [], function(){
		
			// Apagando pedido da sessao
			location.href="pedidos_espelho.html#" + id_pedido + '|' + filial + '|' + tipo+'|true';
			sessionStorage[sessionStorage['sessao_tipo']] = '';
			
			
			
			
			
		});
	
	});
								
	
}
