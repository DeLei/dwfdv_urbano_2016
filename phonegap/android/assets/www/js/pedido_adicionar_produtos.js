var percentual_maximo_desconto = 0;
var percentual_maximo_acrescimo = 0;

$(document).ready(function(){
	
	//obtem percentual maximo de desconto e acrescimo para produtos
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM configuracao', [], function(x, dados) {
				
				var dado = dados.rows.item(0);
				
				percentual_maximo_desconto = dado.percentual_maximo_desconto;
				percentual_maximo_acrescimo = dado.percentual_maximo_acrescimo;
				
				
			}
		);
	});
	
	//Seleciona o valor atual ao entrar no campo
	$('.selecionar_ao_entrar').live('click', function(){
		$(this).select();
	});
	
	// Desativar todos os campos quando clicar em id_informacao_cliente
	$('#id_adicionar_produtos').click(function(){
		
		$('#id_outras_informacoes').addClass('tab_desativada');
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select',1);
		
		ativar_tabs();
		
		
		//CUSTOM - 000177/000870 - Produtos em destaque
		verificar_produtos_destaque();
	});
	
	
	
	/**
	* 
	* ID:			trocar_produto
	*
	* Descrição:	Utilizado para apagar todos os valores dos campos ligados ao produto par aque o representante possa selecionar um novo produto
	*
	*/
	$('#trocar_produto').live('click', function(){
		
		limpar_dados_produtos();
		
	});

	
	/**
	* 
	* ID:			adicionar_produto
	*
	* Descrição:	Utilizado para adicionar um produto e seus valores na sessao
	*
	*/
	$('#adicionar_produto').live('click', function(e){
		e.preventDefault();
		
		var campos = [];
		campos['codigo_produto'] 	= 'input[name=codigo_produto]';
		campos['preco_venda'] 		= 'input[name=preco_venda]';
		campos['quantidade'] 		= 'input[name=quantidade]';
		campos['desconto'] 			= 'input[name=desconto]';
		campos['preco_unitario']	= converter_decimal($('input[name=preco_unitario]').val());
		
		if(verificar_existe($('input[name=codigo_produto]').val()))
		{
			erro_itens('O produto <b>' + $('input[name=codigo_produto]').val() + ' - ' + $('input[name=descricao_produto]').val() + '</b> já foi adicionado!');
		}
		else if(obter_valor_sessao('tipo_venda') == '20' && !$('select[name=motivo_troca]').val())
		{
			erro_itens('Selecione um <b>Motivo de Troca</b>.')
		}
		else if(!$('input[name=lote_troca]').val() && obter_valor_sessao('tipo_venda') == '20')
		{
			erro_itens('Informe o <b>Lote de Troca</b>.');
		}
		else if(obter_valor_sessao('tipo_venda') == '20' && !validar_lote()) //Se for um pedido de troca, valida o lote
		{
			erro_itens('O <b>Lote</b> informado é inválido.');
		}
		else
		{
			
			validar_campos(null, serialize(campos),false,function(resultado){
				if(resultado){
				
					var codigo_produto 		= $('input[name=codigo_produto]').val();
					var descricao_produto 	= $('input[name=descricao_produto]').val();
					var preco_tabela 		= converter_decimal($('input[name=preco_tabela]').val());
					var preco_unitario 		= converter_decimal($('input[name=preco_unitario]').val());
					var preco_venda 		= converter_decimal($('input[name=preco_venda]').val());
					var quantidade 			= $('input[name=quantidade]').val();
					var desconto 			= converter_decimal($('input[name=desconto]').val());
					var ipi 				= converter_decimal($('input[name=ipi]').val());
					var unidade_medida 		= $('input[name=unidade_medida]').val();
					var local 				= $('input[name=local]').val();
					var variacao_maxima		= $('input[name=variacao_maxima]').val();
					var peso				= $('input[name=peso]').val();
					var motivo_troca		= $('select[name=motivo_troca]').val();
					var lote_troca			= strtoupper($('input[name=lote_troca]').val());
					var converter			= converter_decimal($('input[name=converter]').val());
					
					var produtos = new Array();
					produtos[codigo_produto] = new Array();
					produtos[codigo_produto]['codigo'] 			= codigo_produto;
					produtos[codigo_produto]['descricao'] 		= descricao_produto;
					produtos[codigo_produto]['preco_tabela'] 	= preco_tabela;
					produtos[codigo_produto]['preco_unitario'] 	= preco_unitario;
					produtos[codigo_produto]['preco_venda'] 	= preco_venda;
					produtos[codigo_produto]['quantidade'] 		= quantidade;
					produtos[codigo_produto]['desconto'] 		= desconto;
					produtos[codigo_produto]['ipi'] 			= ipi;
					produtos[codigo_produto]['unidade_medida'] 	= unidade_medida;
					produtos[codigo_produto]['local'] 			= local;
					produtos[codigo_produto]['variacao_maxima'] = variacao_maxima;
					produtos[codigo_produto]['peso']			= peso;
					produtos[codigo_produto]['motivo_troca']	= motivo_troca;
					produtos[codigo_produto]['lote_troca']		= lote_troca;
					produtos[codigo_produto]['converter']		= converter;
					
					var produtos = serialize(produtos);
					
					salvar_produto_sessao(produtos);
					
					exibir_produtos();
					
					$('#erro_item').hide('fast');
					
					limpar_dados_produtos();
					
					$('input[name=quantidade]').focus();
				}
				
			});
		}
		
	});
	
	
	
	/**
	* 
	* ID:			avancar_passo_2
	*
	* Descrição:	Utilizado para salvar os dados editados e passar para o proximo passo
	*
	*/
	$('#avancar_passo_2').live('click', function(e){
			
		e.preventDefault();
	
		$('.erro_itens').remove();
		
		var sessao_produtos = obter_produtos_sessao();
		if(sessao_produtos)
		{
			var total_produtos 		= Object.keys(sessao_produtos).length;
		}
		else
		{
			var total_produtos 		= 0;
		}
		
		var sucesso = true;
		
		if(total_produtos > 0)
		{	
			$.each(sessao_produtos, function(key, objeto){
				var codigo_produto 	= objeto.codigo;
				var desconto 		= converter_decimal($('input[name=editar_desconto_' + codigo_produto + ']').val());
				var preco_venda 	= converter_decimal($('input[name=editar_preco_venda_' + codigo_produto + ']').val());
				var quantidade 		= converter_decimal($('input[name=editar_quantidade_' + codigo_produto + ']').val());
				
				// Zerando sempre o background
				$('.produto_' + codigo_produto).css('background-color', '');
				
				var produtos = new Array();
				produtos[codigo_produto] = new Array();
				produtos[codigo_produto]['codigo'] 			= codigo_produto;
				produtos[codigo_produto]['descricao'] 		= objeto.descricao;
				produtos[codigo_produto]['preco_tabela'] 	= objeto.preco_tabela;
				produtos[codigo_produto]['preco_unitario'] 	= objeto.preco_unitario;
				produtos[codigo_produto]['preco_venda'] 	= preco_venda;
				produtos[codigo_produto]['quantidade'] 		= quantidade;
				produtos[codigo_produto]['desconto'] 		= desconto;
				produtos[codigo_produto]['ipi'] 			= objeto.ipi;
				produtos[codigo_produto]['unidade_medida'] 	= objeto.unidade_medida;
				produtos[codigo_produto]['local'] 			= objeto.local;
				produtos[codigo_produto]['TES'] 			= objeto.TES;
				produtos[codigo_produto]['CF'] 				= objeto.CF;
				produtos[codigo_produto]['ST'] 				= 0;
				produtos[codigo_produto]['IPI'] 			= 0;
				produtos[codigo_produto]['ICMS'] 			= 0;
				produtos[codigo_produto]['variacao_maxima'] = objeto.variacao_maxima;
				produtos[codigo_produto]['peso']			= objeto.peso;
				produtos[codigo_produto]['motivo_troca']	= (objeto.motivo_troca ? objeto.motivo_troca : '');
				produtos[codigo_produto]['lote_troca']		= (objeto.lote_troca ? objeto.lote_troca : '');
				produtos[codigo_produto]['converter'] = objeto.converter;
				var produtos = serialize(produtos);
				
				salvar_produto_sessao(produtos);
				
				//-----------------//
				//--- Validação ---//
				//-----------------//
				
				
				var campos = [];
				campos['desconto'] 		 = 'input[name=editar_desconto_' + codigo_produto + ']';
				campos['preco_venda'] 	 = 'input[name=editar_preco_venda_' + codigo_produto + ']';
				campos['quantidade'] 	 = 'input[name=editar_quantidade_' + codigo_produto + ']';
				campos['preco_unitario'] = objeto.preco_unitario;
				
				validar_campos(codigo_produto, serialize(campos), true,function(resultado){	
					if(!resultado){
						sucesso = resultado;
					}
				});
			});
			
			//verificar_bloqueio_pedido();
			validar_descontos(function(resultados){
				$.each(resultados, function(key, item) {
					if(item.codigo != 1){
						sucesso = false;
					}
				});
				
				if(sucesso == true)
				{
					// Ativando Botao "Adicionar Produtos"
					$('#id_outras_informacoes').removeClass('tab_desativada');
					
					ativar_tabs();
					
					//Chamando as funções do outras_informacoes
					outras_informacoes();
					
					// Acionando (Click) botao "Adicionar Produtos"
					$("#id_outras_informacoes").click();
				}else{
					mensagem('Um ou mais produtos encontram-se fora da política de vendas.');
				}
				
			});
			
			
			/*
			if(sucesso == true)
			{
				// Ativando Botao "Adicionar Produtos"
				$('#id_outras_informacoes').removeClass('tab_desativada');
				
				ativar_tabs();
				
				//Chamando as funções do outras_informacoes
				outras_informacoes();
				
				// Acionando (Click) botao "Adicionar Produtos"
				$("#id_outras_informacoes").click();
			}*/
		}
		else
		{
			var descricao_tipo_pedido = obter_descricao_pedido('lower');
			mensagem('Adicione um produto no ' + descricao_tipo_pedido + '.');
		}
	});
	
	
	
	/**
	* 
	* CLASSE:			excluir_item
	*
	* Descrição:	Utilizado para excluir um item na sessao
	*
	*/
	$('.excluir_item').live('click', function(e){
		e.preventDefault();
		
		var codigo_produto = remover_zero_esquerda($(this).attr('href'));
		
		var sessao_produtos = obter_produtos_sessao();
		
		var produto = sessao_produtos[codigo_produto];
		
		confirmar('Deseja excluir o item <b>' + produto.codigo + ' - ' + produto.descricao + '</b>?', 
			function () {
				$(this).dialog('close');
			
				delete sessao_produtos[codigo_produto]; 
		
				salvar_produtos_excluidos(serialize(sessao_produtos));
				
				exibir_produtos();
				
				$("#confirmar_dialog").remove();
				
			}
		);
		
	});
	
	
	$('.salvar_sessao_edicao').live('keyup', 'focusout', function(){
		var codigo_produto = $(this).data('codigo_produto');
		
		var desconto = converter_decimal($('input[name=editar_desconto_' + codigo_produto + ']').val());
		var quantidade = converter_decimal($('input[name=editar_quantidade_' + codigo_produto + ']').val());
		var preco_venda = converter_decimal($('input[name=editar_preco_venda_' + codigo_produto + ']').val());
		
		var sessao_produtos = obter_produtos_sessao();
		
		sessao_produtos[codigo_produto]['desconto'] = desconto;
		sessao_produtos[codigo_produto]['quantidade'] = quantidade;
		sessao_produtos[codigo_produto]['preco_venda'] = preco_venda;
		
		var produtos = serialize(sessao_produtos);
		
		salvar_produto_sessao(produtos);
	});
});

function adicionar_produtos()
{

	
	
	// Recalcular valores caso ouver alteração no cliente ou tabela de preços
	recalcular_precos();

	// Exibir valores do cabeçalho
	exibir_cabecalho();
	
	// Obter Produtos (Autocomplete)
	obter_produtos();
	
	exibir_produtos();
	
	// Função para os calculos do KEY UP
	calculos_automaticos('input[name=preco_venda]', 'input[name=preco_unitario]', 'input[name=desconto]', 'input[name=quantidade]', 'input[name=ipi]', 'input[name=total_item]', '', 'input[name=peso]', 'input[name=peso_total]');
	
	
	alterarCabecalhoTabelaConteudo();
	
	//Se for um pedido de troca, exibe os detalhes da troca
	if(obter_valor_sessao('tipo_venda') == '20')
	{
		obter_motivos_troca();
		$('.dados_troca').show();
	}
	else
	{
		$('.dados_troca').hide();
	}
}

function obter_motivos_troca(){
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM motivos_troca WHERE filial = ?', [obter_valor_sessao('filial')], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=motivo_troca]').html('<option value="">SELECIONE...</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					//Não exibe os grupos de motivo cadastrados no Protheus
					if(!strstr(dado.chave, '--')){
						$('select[name=motivo_troca]').append('<option value="' + dado.chave + '">' + dado.descricao + '</option>');
					}
				}
				
				// Selecionar campo se existir na sessão
				if(obter_valor_sessao('motivo_troca'))
				{
					$('select[name="motivo_troca"] option[value="' + obter_valor_sessao('motivo_troca') + '"]').attr('selected', 'selected');
				}
				
			}
		);
	});
}

/**
* Metódo:		recalcular_precos
* 
* Descrição:	Função Utilizada para para recalcular preços e descontos quando a tabela de preços ou cliente é alterado
* 
* Data:			22/10/2013
* Modificação:	22/10/2013
* 
* @access		public
* @param		string 			codigo 	- codigo do produto

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function recalcular_precos()
{

	var sessao_produtos 	= obter_produtos_sessao();
	if(sessao_produtos)
	{
		var total_produtos 		= Object.keys(sessao_produtos).length;
	}
	var indice = 0;

	
	if(sessao_produtos)
	{
		$.each(sessao_produtos, function(key, produto){
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM produtos WHERE tb_codigo = ? AND produto_codigo = ?', [obter_valor_sessao('tabela_precos'), produto.codigo], function(x, dados) {
						if (dados.rows.length)
						{

							var dado = dados.rows.item(0);
							
							var preco 			= aplicar_descontos_cabecalho(dado.ptp_preco);
							var codigo_produto 	= produto.codigo;
							
							//------------------------
							
							var produtos = new Array();
							produtos[codigo_produto] = new Array();
							produtos[codigo_produto]['codigo'] 			= produto.codigo;
							produtos[codigo_produto]['descricao'] 		= produto.descricao;
							produtos[codigo_produto]['preco_tabela'] 	= dado.ptp_preco;
							produtos[codigo_produto]['preco_unitario'] 	= preco;
							produtos[codigo_produto]['preco_venda'] 	= preco - (preco * (produto.desconto / 100));
							produtos[codigo_produto]['quantidade'] 		= produto.quantidade;
							produtos[codigo_produto]['desconto'] 		= produto.desconto;
							produtos[codigo_produto]['ipi'] 			= produto.ipi;
							produtos[codigo_produto]['unidade_medida'] 	= produto.unidade_medida;
							produtos[codigo_produto]['local'] 			= produto.local;
							produtos[codigo_produto]['TES'] 			= produto.TES;
							produtos[codigo_produto]['CF'] 				= produto.CF;
							produtos[codigo_produto]['ST'] 				= produto.ST;
							produtos[codigo_produto]['IPI'] 			= produto.IPI;
							produtos[codigo_produto]['ICMS'] 			= produto.ICMS;
							produtos[codigo_produto]['peso'] 			= produto.peso;
							var variacao_maxima = dado.ptp_variacao_maxima;
							if(!variacao_maxima)
							{
								variacao_maxima = obter_valor_sessao('MV_VARPREJ');
							}
							produtos[codigo_produto]['variacao_maxima'] = variacao_maxima;
							produtos[codigo_produto]['motivo_troca'] 	= (produto.motivo_troca ? produto.motivo_troca : '');
							produtos[codigo_produto]['lote_troca'] 		= (produto.lote_troca ? produto.lote_troca : '');
							
							var produtos = serialize(produtos);
							
							salvar_produto_sessao(produtos);
							
							//------------------------

						}
						else
						{
						
							var codigo_produto 	= remover_zero_esquerda(produto.codigo);
						
							//--------
							//-------------------
							//--------
						
							var produtos = new Array();
							produtos[codigo_produto] = new Array();
							produtos[codigo_produto]['codigo'] 			= produto.codigo;
							produtos[codigo_produto]['descricao'] 		= produto.descricao;
							produtos[codigo_produto]['preco_tabela'] 	= produto.preco_tabela;
							produtos[codigo_produto]['preco_unitario'] 	= produto.preco_unitario;
							produtos[codigo_produto]['preco_venda'] 	= produto.preco_venda
							produtos[codigo_produto]['quantidade'] 		= produto.quantidade;
							produtos[codigo_produto]['desconto'] 		= produto.desconto;
							produtos[codigo_produto]['ipi'] 			= produto.ipi;
							produtos[codigo_produto]['unidade_medida'] 	= produto.unidade_medida;
							produtos[codigo_produto]['local'] 			= produto.local;
							produtos[codigo_produto]['TES'] 			= produto.TES;
							produtos[codigo_produto]['CF'] 				= produto.CF;
							produtos[codigo_produto]['ST'] 				= produto.ST;
							produtos[codigo_produto]['IPI'] 			= produto.IPI;
							produtos[codigo_produto]['ICMS'] 			= produto.ICMS;
							produtos[codigo_produto]['peso'] 			= produto.peso;
							
							var produtos = serialize(produtos);
							
							salvar_produto_removido_sessao(produtos);
							
							//--------
							//-------------------
							//--------
						
							
						
							delete sessao_produtos[codigo_produto]; 
		
							salvar_produtos_excluidos(serialize(sessao_produtos));
						}
						
						indice++;
						
						if(indice == total_produtos)
						{
							exibir_produtos();
						}
						
					}
				);
			});
			
		});
	
	}
	
}

/**
* Metódo:		verificar_existe
* 
* Descrição:	Função Utilizada para saber se existe o produto na sessão
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo 	- codigo do produto

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function verificar_existe(codigo)
{
	var codigo_produto 		= remover_zero_esquerda(codigo);
	var sessao_produtos 	= obter_produtos_sessao();
		
	if(sessao_produtos[codigo_produto])
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
* Metódo:		limpar_dados_produtos
* 
* Descrição:	Função Utilizada para apagar todos os campos ligados ao produto
* 
* Data:			19/10/2013
* Modificação:	23/10/2013
* 
* @access		public

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function limpar_dados_produtos()
{
	$('input[name="produto"]').val("");
	$('input[name="codigo_produto"]').val("");
	$('input[name="descricao_produto"]').val("");
	$('input[name="preco_tabela"]').val("");
	$('input[name="preco_unitario"]').val("");
	$('input[name="preco_venda"]').val("");
	$('input[name="quantidade"]').val("");
	$('input[name="desconto"]').val("");
	$('input[name="ipi"]').val("");
	$('input[name="total_item"]').val("");
	$('input[name="unidade_medida"]').val("");
	$('input[name="local"]').val("");
	$('input[name="variacao_maxima"]').val("");
	$('input[name="peso"]').val("");
	$('input[name="peso_total"]').val("");
	$('select[name="motivo_troca"] option[value=""]').attr('selected', 'selected');
	$('input[name="lote_troca"]').val("");
	
	$('#trocar_produto').hide();
	$('input[name="produto"]').removeAttr('disabled');
	$('#erro_item').hide();
}

/**
* Metódo:		calculos_automaticos
* 
* Descrição:	Função Utilizada realizar os calculos automaticos de desconto e preço via keyup para tudo 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			campo_preco_venda		-	campo input do preco de venda
* @param		string 			campo_preco_unitario	-	campo input do preco unitario
* @param		string 			campo_desconto			-	campo input do desconto
* @param		string 			campo_quantidade		-	campo input do quantidade
* @param		string 			campo_ipi				-	campo input do ipi
* @param		string 			campo_total_geral		-	campo input do total geral
* @param		string 			campo_total_item		-	campo input do ptotal do item
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function calculos_automaticos(campo_preco_venda, campo_preco_unitario, campo_desconto, campo_quantidade, campo_ipi, campo_total_geral, campo_total_item, campo_peso_unitario, campo_peso_total)
{
	
	// Calcular Desconto
	$(campo_preco_venda).keyup(function(){
		var preco_unitario 			=  converter_decimal($(campo_preco_unitario).val());
		var preco_venda 			=  converter_decimal($(this).val());
		
		var valor_diferenca 		= preco_unitario - preco_venda;
		var percentagem 			= valor_diferenca / preco_unitario * 100;
		
		if(percentagem < 0)
		{
			percentagem = 0;
		}
		
		// Iserindo percentagem no campo
		$(campo_desconto).val(number_format(percentagem, 3, ',', '.'));
		
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item, campo_peso_unitario, campo_peso_total);
		
		exibir_totais();
	});
	
	// Calcular Preço de Venda pelo Desconto
	$(campo_desconto).keyup(function(){
		var preco_unitario 			= converter_decimal($(campo_preco_unitario).val());
		var desconto 				= converter_decimal($(this).val());
		
		var percentagem 			= desconto / 100;
		var valor_diferenca 		= preco_unitario * percentagem;
		var preco_venda 			= preco_unitario - valor_diferenca;
		
		if(preco_venda < 0)
		{
			preco_venda = 0;
		}
		
		// Iserindo preco_venda no campo
		$(campo_preco_venda).val(number_format(preco_venda, 3, ',', '.'));
		
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item, campo_peso_unitario, campo_peso_total);
		
		exibir_totais();
		
	});
	
	// Calcular Preço Total pela quantidade
	$(campo_quantidade).keyup(function(){
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item, campo_peso_unitario, campo_peso_total);
		
		exibir_totais();
	});
}


/**
* Metódo:		validar_campos
* 
* Descrição:	Função Utilizada para exibir os erros 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo_produto 	- codigo do produto usado para identificar qual linha da coluna sera inserido o erro
* @param		string 			mensagem_erro	- mensagem do erro
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function validar_campos(codigo_produto, campos, avancar,callback)
{
	var campos = unserialize(campos);
	var sucesso = true;
	
	var preco_unitario = parseFloat(campos.preco_unitario);
	
	var variacao_maxima = parseFloat(percentual_maximo_acrescimo);
	var preco_maximo_permitido = preco_unitario + (preco_unitario * (variacao_maxima / 100));
	
	var variacao_minima = parseFloat(percentual_maximo_desconto);
	var preco_minimo_permitido = preco_unitario - (preco_unitario * (variacao_minima / 100));
	
	// Se desconto for vazio, inserir o valor 0
	if(!$(campos['desconto']).val())
	{
		$(campos['desconto']).val(number_format(0, 3, ',', '.'));
	}
	
	//------------------
	
	if(!$(campos['codigo_produto']).val() && campos['codigo_produto']) // Validação do Produto
	{
		erro_itens('Selecione um Produto.');
		
		sucesso = false;
	}
	else if(!validar_decimal($(campos['preco_venda']).val())) // Validação do Preço de Venda
	{
		if($(campos['preco_venda']).val())
		{
			erro_itens('O valor "<b>' + $(campos['preco_venda']).val() + '</b>" no campo <b>Preço de Venda</b> não é um valor decimal válido.<br /><br />Exemplo de valor decimal válido: <b>1.100,250<b/>', codigo_produto);
			
			sucesso = false;
		}
		else
		{
			erro_itens('Digite um valor para o <b>Preço de Venda</b>.', codigo_produto);
			
			sucesso = false;
		}
	}
	else if(!validar_digitos($(campos['quantidade']).val()) || $(campos['quantidade']).val() <= 0) // Validação da Quantidade
	{
		if(!$(campos['quantidade']).val())
		{
			erro_itens('Digite uma quantidade.', codigo_produto);
			
			sucesso = false;
		}
		else
		{
			erro_itens('O valor "<b>' + $(campos['quantidade']).val() + '</b>" no campo <b>Quantidade</b> não contém apenas digitos.', codigo_produto);
			
			sucesso = false;
		}
	}
	else if(!validar_decimal($(campos['desconto']).val())) // Validação do Desconto
	{
		erro_itens('O valor "<b>' + $(campos['desconto']).val() + '</b>" no campo <b>Desconto</b> não é um valor decimal válido.<br /><br />Exemplo de valor decimal válido: <b>50,250<b/>', codigo_produto);
		
		sucesso = false;
	}
	else if(converter_decimal($(campos['desconto']).val()) > 99.990) // Validação do Desconto
	{
		erro_itens('O valor "<b>' + $(campos['desconto']).val() + '</b>" no campo <b>Desconto</b> não pode ser maior que 99.990%.', codigo_produto);
		
		sucesso = false;
	}
	else if(converter_decimal($(campos['desconto']).val()) > 100) // Validação do Desconto
	{
		erro_itens('O valor "<b>' + $(campos['desconto']).val() + '</b>" no campo <b>Desconto</b> não pode ser maior que 100.', codigo_produto);
		
		sucesso = false;
	}
	else if(converter_decimal($(campos['preco_venda']).val()) > parseFloat(number_format(preco_maximo_permitido,3)))
	{
		
		//mensagem(converter_decimal($(campos['preco_venda']).val()) +' - '+ number_format(preco_maximo_permitido,3));
		erro_itens('O valor do item está acima do <b>limite máximo</b> permitido.', codigo_produto);
		
		sucesso = false;
	}
	else if(converter_decimal($(campos['preco_venda']).val()) < parseFloat(number_format(preco_minimo_permitido,3)))
	{
		
		//mensagem(converter_decimal($(campos['preco_venda']).val()) +' - '+ number_format(preco_minimo_permitido,3));
		erro_itens('O valor do item está abaixo do <b>limite mínimo</b> permitido.', codigo_produto);
		
		sucesso = false;
	}
	
	if(!!codigo_produto){
	
		/* Verifica se o produto está dentro da regra de desconto de acordo com a quantidade de fardos inclusa no pedido e variação percentual de descontos. */
		verificar_variacao_preco(codigo_produto, avancar,function(resultado){
			if(resultado.codigo != 1){
				erro_itens('O preço aplicado no produto '+ resultado.mensagem, codigo_produto);
				sucesso = false;		
			}
			
			callback(sucesso);		
		});
	}else{
		callback(sucesso);
	}
	
	
}

/*
 * 	Retorno:
 * 		1 - Pedido entra como Bloqueado
 * 		2 - Pedido entra como Liberado
 * 		3 - Não permite a inclusão do pedido
 */
function validar_variacao_maxima(preco_venda, preco_tabela, variacao_maxima)
{
	var retorno = '2';
	var variacao_parametro = number_format(floatval(obter_valor_sessao('MV_VARPREJ')), 3);
	
	var variacao_reais = (variacao_maxima / 100) * preco_tabela;
	var variacao_reais_parametro = (variacao_parametro / 100) * preco_tabela;
	
	var minimo_permitido = number_format((preco_tabela - variacao_reais), 3);
	var minimo_permitido_parametro = number_format((preco_tabela - variacao_reais_parametro), 3);
	
	if(parseFloat(preco_venda) < parseFloat(minimo_permitido))
	{
		if(parseFloat(preco_venda) < parseFloat(minimo_permitido_parametro))
		{
			retorno = '3';
		}
		else
		{
			retorno = '1';;
		}
	}
	
	return retorno;
}

/**
* Metódo:		verificar_variacao_preco
* 
* Descrição:	Função Utilizada para verificar a variação máxima e mínima permitida para o preço de venda
* 				CUSTOM 000177/000870 
* 
* 				Retorno:
* 				1 - Pedido entra como Bloqueado
* 				2 - Pedido entra como Liberado
* 				3 - Não permite a inclusão do pedido
*  
* @access		public
* @param		bool 			avancar 	- identifica se a validação será sobre o item adicionado ou sobre todos os itens adicionados
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function verificar_variacao_preco(codigo_produto, avancar,callback)
{
	var retorno = {	};
	retorno.codigo = 1;
	retorno.mensagem = 'Sucesso.';
	
	//Adicionar produtos
	if(!avancar)
	{
		/** Validação foi removida para que ocorra apenas ao usuário clica no "Avançar" */
		/*
		//Valida o item adicionado
		var preco_venda = number_format(floatval(converter_decimal($('[name=preco_venda]').val())));
		var preco_tabela = number_format(floatval(converter_decimal($('[name=preco_unitario]').val())));
		var variacao_maxima = number_format(floatval($('[name=variacao_maxima]').val()));
		
		
		retorno = validar_variacao_maxima(preco_venda, preco_tabela, variacao_maxima);
		*/
		callback(retorno);
	}
	//Avançar outras informações
	else
	{
		var preco_venda = number_format(floatval(converter_decimal($('[name=editar_preco_venda_' + codigo_produto + ']').val())), 3);
		var preco_tabela = number_format(floatval(converter_decimal($('[name=editar_preco_unitario_' + codigo_produto + ']').val())), 3);
		var variacao_maxima = number_format(floatval($('[name=editar_variacao_maxima_' + codigo_produto + ']').val()), 3);
		
		
		validar_desconto_item(codigo_produto,function(status){		
			retorno = status;
			//retorno = validar_variacao_maxima(preco_venda, preco_tabela, variacao_maxima);
			//Salva como desbloqueado se estiver dentro da variação maxima e se o pedido não for de bonificação
			if(obter_valor_sessao('tipo_venda') != '02')
			{
				salvar_sessao('pedido_bloqueado', parseInt(retorno.codigo).toString());
			}
			else
			{
				salvar_sessao('pedido_bloqueado', '1');
			}
			
			callback(retorno);
		});	
	}	
}




function verificar_bloqueio_pedido()
{
	var retorno = '2';
	
	var sessao_produtos 	= obter_produtos_sessao();
	var total_produtos 	= Object.keys(sessao_produtos).length;
	var variacao_parametro = obter_valor_sessao('MV_VARPREJ');
		
	if(total_produtos > 0)
	{
		$.each(sessao_produtos, function(key, objeto){
			var preco_venda = number_format(floatval(converter_decimal($('[name=editar_preco_venda_' + objeto.codigo + ']').val())), 3);
			var preco_tabela = number_format(floatval(converter_decimal($('[name=editar_preco_unitario_' + objeto.codigo + ']').val())), 3);
			var variacao_maxima = number_format(floatval($('[name=editar_variacao_maxima_' + objeto.codigo + ']').val()), 3);
			
			var variacao_reais = (variacao_maxima / 100) * preco_tabela;
			var variacao_reais_parametro = (variacao_parametro / 100) * preco_tabela;
			
			var minimo_permitido = number_format((preco_tabela - variacao_reais), 3);
			var minimo_permitido_parametro = number_format((preco_tabela - variacao_reais_parametro), 3);
			
			if(parseFloat(preco_venda) < parseFloat(minimo_permitido))
			{
				//Bloqueia a inclusão se o preço de venda e o percentual da tabela for menor que o percentual do parametro MV
				if((parseFloat(preco_venda) < parseFloat(minimo_permitido_parametro)) && (parseFloat(minimo_permitido) < parseFloat(minimo_permitido_parametro)))
				{
					retorno = '3';
				}
				else
				{
					retorno = '1';;
				}
			}
		});
		
		salvar_sessao('pedido_bloqueado', retorno);
		
		//Exibe a mensagem de alerta referente ao bloqueio de pedido
		if(obter_valor_sessao('pedido_bloqueado') == '1')
		{
			mensagem('Seu pedido estará sujeito a <b>aprovação comercial</b>.<br />Um ou mais produtos econtram-se acima da política de vendas permitida.');
		}
	}
}

/**
* Metódo:		erro_itens
* 
* Descrição:	Função Utilizada para exibir os erros 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo_produto 	- codigo do produto usado para identificar qual linha da coluna sera inserido o erro
* @param		string 			mensagem_erro	- mensagem do erro
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function erro_itens(mensagem_erro, codigo_produto)
{
	var icone = '<img src="img/warning.png" style="vertical-align: middle" /> ';

	if(codigo_produto)
	{
		$('<tr class="erro_itens" style="background-color: #F0798E"><td colspan="10">' + icone + mensagem_erro + '</td></tr>').insertAfter($('.produto_' + codigo_produto));
	
		$('.produto_' + codigo_produto).css('background-color', '#F0798E');
	}
	else
	{
	
		$('#erro_item').html(icone + mensagem_erro);
		$('#erro_item').show('fast');
	}
}


/**
* Metódo:		obter_produtos_sessao
* 
* Descrição:	Função Utilizada para obter os itens inseridos na sessão
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos_sessao()
{
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos'])
	{
		return sessao_pedido['produtos'];
	}
	else
	{
		return false;
	}
}


/**
* Metódo:		obter_produtos_removidos_sessao
* 
* Descrição:	Função Utilizada para obter os itens removidos na sessão
* 
* Data:			22/10/2013
* Modificação:	22/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos_removidos_sessao()
{
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos_removidos'])
	{
		return sessao_pedido['produtos_removidos'];
	}
	else
	{
		return false;
	}
}

/**
* Metódo:		exibir_totais
* 
* Descrição:	Função Utilizada para exibir os totais dos itens
* 
* Data:			29/10/2013
* Modificação:	29/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_totais()
{

	var total_preco_unitario 	= 0;
	var total_desconto 			= 0;
	var total_preco_venda 		= 0;
	var total_quantidade		= 0;
	var total					= 0;
	var total_ipi				= 0;
	var total_geral				= 0;
	var total_peso				= 0;
	
	var sessao_produtos = obter_produtos_sessao();
	
	if(sessao_produtos)
	{

		$.each(sessao_produtos, function(key, objeto){

			// -------
			// Variaveis para os TOTAIS
			
			total_preco_unitario 	+= parseFloat(converter_decimal($('input[name=editar_preco_unitario_' + objeto.codigo + ']').val()));
			total_preco_venda		+= parseFloat(converter_decimal($('input[name=editar_preco_venda_' + objeto.codigo + ']').val()));
			total_quantidade		+= parseFloat(converter_decimal($('input[name=editar_quantidade_' + objeto.codigo + ']').val()));
			total					+= parseFloat(converter_decimal($('input[name=editar_total_item_' + objeto.codigo + ']').val()));
			total_geral				+= parseFloat(converter_decimal($('input[name=editar_total_geral_' + objeto.codigo + ']').val()));
			total_peso				+= parseFloat(converter_decimal($('input[name=editar_total_peso_' + objeto.codigo + ']').val()));
			//-------
			
		});
		
		total_desconto 			= (total_preco_unitario - total_preco_venda) * 100 / total_preco_unitario;
		total_ipi				= (total_geral - total) * 100 / total_geral;
		
	}


	var descricao_tipo_pedido = obter_descricao_pedido('upper');
	

	// Totais
	var html = '';
	html += '<td colspan="5">TOTAIS DO ' + descricao_tipo_pedido + '</td>';
	//html += '<td align="right">' + number_format(total_preco_unitario, 3, ',', '.') + '</td>';
	//html += '<td align="right">' + number_format(total_desconto, 3, ',', '.') + '</td>';
	//html += '<td align="right">' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
	html += '<td align="right">' + total_quantidade + '</td>';
	html += '<td align="right">' + total_peso + '</td>';
	html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
	//html += '<td align="right">' + number_format(total_ipi, 3, ',', '.') +'</td>';
	//html += '<td align="right">' + number_format(total_geral, 3, ',', '.') + '</td>';
	html += '<td align="right"></td>';

	$('.novo_grid_rodape').html(html);

}

/**
* Metódo:		exibir_produtos
* 
* Descrição:	Função Utilizada para exibir os itens dos produtos inseridos na sessao e Os itens excluídos (Um item é excluido quando o representante altera a tabela de preços e o item não existe nessa tabela)
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_produtos()
{
	
	var sessao_produtos 	= obter_produtos_sessao();
	if(sessao_produtos)
	{
		var total_produtos 		= Object.keys(sessao_produtos).length;
	}
	else
	{
		var total_produtos 		= 0;
	}
	
	if(total_produtos > 0)
	{
		$('.itens_pedido').empty();

		$.each(sessao_produtos, function(key, objeto){
			
			var html = '';
			html += '<td align="center">' + objeto.codigo + '</td>';
			html += '<td>' + objeto.descricao + '</td>';
			html += '<td align="right">' + number_format(objeto.preco_unitario, 3, ',', '.') + '<input type="hidden" name="editar_preco_unitario_' + objeto.codigo + '" value="' + number_format(objeto.preco_unitario, 3, ',', '.') + '" /></td>';
			html += '<td align="center"><input name="editar_desconto_' + objeto.codigo + '" class="salvar_sessao_edicao" data-codigo_produto="' + trim(objeto.codigo) + '" type="text" disabled="disabled" size="8" value="' + number_format(objeto.desconto, 3, ',', '.') + '" /></td>';
			var variacao = (objeto.variacao_maxima ? objeto.variacao_maxima : 0);
			if(variacao <= 0)
			{
				variacao = obter_valor_sessao('MV_VARPREJ');
			}
			html += '<td align="center"><input name="editar_preco_venda_' + objeto.codigo + '" type="text" size="8" value="' + number_format(objeto.preco_venda, 3, ',', '.') + '" class="salvar_sessao_edicao selecionar_ao_entrar" data-codigo_produto="' + trim(objeto.codigo) + '" />' + '<input type="hidden" name="editar_variacao_maxima_' + objeto.codigo + '" value="' + variacao + '" /></td>';
			html += '<td align="center"><input name="editar_quantidade_' + objeto.codigo + '" type="text" size="8" value="' + objeto.quantidade + '" class="salvar_sessao_edicao selecionar_ao_entrar" data-codigo_produto="' + trim(objeto.codigo) + '" /></td>';
			html += '<td align="center"><input name="editar_total_peso_' + objeto.codigo + '" disabled="disabled" type="text" size="8" value="' + objeto.peso * objeto.quantidade + '" data-codigo_produto="' + trim(objeto.codigo) + '" /><input name="editar_peso_' + objeto.codigo + '" type="hidden" value="' + objeto.peso + '" data-codigo_produto="' + trim(objeto.codigo) + '" /></td>';
			html += '<td align="right"><input name="editar_total_item_' + objeto.codigo + '" type="text" size="13" value="' + number_format(objeto.preco_venda * objeto.quantidade, 3, ',', '.')  + '" disabled="disabled" /></td>';
			//html += '<td align="right">' +  number_format(objeto.ipi, 3, ',', '.') + '<input type="hidden" name="editar_ipi_' + objeto.codigo + '" value="' + number_format(objeto.ipi, 3, ',', '.') + '" /></td>';
			//html += '<td align="center"><input name="editar_total_geral_' + objeto.codigo + '" type="text" size="13" value="' + number_format((parseFloat(objeto.preco_venda) + parseFloat(objeto.preco_venda * (objeto.ipi / 100))) * objeto.quantidade, 3, ',', '.')  + '" disabled="disabled" /></td>';
			html += '<td align="center"><a class="excluir_item botao_c_grid" href="' + objeto.codigo + '">Excluir</a></td>';
			
			$('.itens_pedido').append('<tr class="produto_' + objeto.codigo + '">' + html + '</tr>');
			
			// Chamar calculo automatico (Funções do keyup)
			calculos_automaticos('input[name=editar_preco_venda_' + objeto.codigo + ']', 'input[name=editar_preco_unitario_' + objeto.codigo + ']', 'input[name=editar_desconto_' + objeto.codigo + ']', 'input[name=editar_quantidade_' + objeto.codigo + ']', 'input[name=editar_ipi_' + objeto.codigo + ']', 'input[name=editar_total_geral_' + objeto.codigo + ']', 'input[name=editar_total_item_' + objeto.codigo + ']', 'input[name=editar_peso_' + objeto.codigo + ']', 'input[name=editar_total_peso_' + objeto.codigo + ']');
		
			
		});
		
		$('.itens_pedido').append('<tr class="novo_grid_rodape"></tr>');
		
		exibir_totais();
		
	}
	else
	{
		$('.itens_pedido').empty();
	
		$('.itens_pedido').append('<tr><td colspan="10" style="color:red">Nenhum produto adicionado.</td></tr>');
	
		$('.itens_pedido').append('<tr class="novo_grid_rodape"></tr>');
		
		exibir_totais();
	}
	
	//-------------------
	//-- Itens Excluidos
	//-------------------
	
	var sessao_produtos_removidos = obter_produtos_removidos_sessao();
	if(sessao_produtos_removidos)
	{
		$('.itens_removidos').empty();
		var titulo = '<b>Produto removido por não existir na tabela de preços.</b>';
		$('.itens_removidos').append(titulo);
		
		$.each(sessao_produtos_removidos, function(key, objeto){
			
			var html = '';
			html += '<li>' + objeto.codigo + ' - ' + objeto.descricao + '</li>';
			
			$('.itens_removidos').append('<ul>' + html + '</ul>');
		});
	}
	
	alterarCabecalhoListagem('#itens_pedido_tabela');
	
	
}


/**
* Metódo:		calcular_total
* 
* Descrição:	Função Utilizada para calcular o valor total dos itens e exibi-los
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			campo_preco_venda - input do preco de venda
* @param		string 			campo_ipi
* @param		string 			campo_quantidade
* @param		string 			campo_total_geral
* @param		string 			campo_total_item
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item, campo_peso_unitario, campo_peso_total)
{
	var preco_venda 			= converter_decimal($(campo_preco_venda).val());
	var ipi 					= converter_decimal($(campo_ipi).val());
	//var valor_ipi				= preco_venda * (ipi / 100);
	var valor_ipi				= 0;
	var quantidade 				= $(campo_quantidade).val();
	var peso_unitario			= $(campo_peso_unitario).val();
	
	if(!quantidade)
	{
		quantidade = 1;
	}
	
	// Iserindo total_item no campo
	if(campo_total_item)
	{
		$(campo_total_item).val(number_format(parseFloat(preco_venda) * quantidade, 3, ',', '.'));
	}
	
	if(campo_peso_total)
	{
		$(campo_peso_total).val(quantidade * peso_unitario);
	}
	
	$(campo_total_geral).val(number_format((parseFloat(preco_venda) + parseFloat(valor_ipi)) * quantidade, 3, ',', '.'));
}


/**
* Metódo:		obter_produtos
* 
* Descrição:	Função Utilizada para buscar os produtos na tabela de produtos onde a tabela de preços for igual a selecionada
* 
* Data:			17/10/2013
* Modificação:	17/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos()
{
	
	$('#erro_item').hide();
	$('#carregando_produtos').show();
	
	
	if(info.empresa)
	{
		//var where = " AND empresa = '" + info.empresa + "'";
	}
	
	
	// Chamado: 002261 - Produto Amostra em Pedido Venda
	var wheres = '';
	if (obter_valor_sessao('tipo_venda') && obter_valor_sessao('tipo_venda') != 'undefined'){
		
		if(obter_valor_sessao('tipo_venda') == '30'){
			 wheres += " AND (cf = '911') ";
		}else if(obter_valor_sessao('tipo_venda') == '01'){
			 wheres += " AND (cf != '911' OR cf = '') ";
		}
		
		//Chamado 002328 - Produto Pallet pedido venda
		if(obter_valor_sessao('tipo_venda') != '99') {
			wheres += " AND (produto_codigo != '94900') ";
		}
		
		
		//Chamado 002719 - Unidade de medidas habilitadas por tipo de venda - configurador.
		var sql_unidades_medidas_habilitadas = [];
		for (y in unidade_medida_permitidas[obter_valor_sessao('tipo_venda')]) {
			sql_unidades_medidas_habilitadas.push("produto_unidade_medida = '" + unidade_medida_permitidas[obter_valor_sessao('tipo_venda')][y] + "'");
		}
		wheres += ' AND (' + sql_unidades_medidas_habilitadas.join(' OR ') + ')';
		
	}
	
	
	if (obter_valor_sessao('filial') && obter_valor_sessao('filial') != 'undefined')
	{
		wheres += " AND (produto_filial = '" + obter_valor_sessao('filial') + "' OR produto_filial = ' ')";
	}
	/* fim chamado 002261 - Produto Amostra em Pedido Venda*/
	//CUSTOM - 000177/000874 - 26/11/2013
	wheres += ' AND ptp_ativo = "1" AND ptp_data_vigencia <= "' + date('Ymd') + '"';
	//CUSTOM - 000177/000874 - 26/11/2013
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM produtos WHERE tb_codigo = ?' + wheres, [obter_valor_sessao('tabela_precos')], function(x, dados) {
				if (dados.rows.length)
				{
					var produtos = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						//CUSTOM 177 - 000870 - 12/11/2013
						var variacao = dado.ptp_variacao_maxima;
						if(variacao <= 0)
						{
							variacao = obter_valor_sessao('MV_VARPREJ');
						}
						var preco = aplicar_descontos_cabecalho(dado.ptp_preco);
						produtos.push({ label: dado.produto_codigo + ' - ' + dado.produto_descricao 
							+ ' - PREÇO: R$ ' + number_format(preco, 3, ',', '.')
							, codigo: dado.produto_codigo
							, preco: preco
							, preco_tabela: dado.ptp_preco
							, ipi: dado.produto_ipi
							, descricao: dado.produto_descricao
							, unidade_medida: dado.produto_unidade_medida
							, local: dado.produto_locpad
							, variacao_maxima: variacao
							, peso: dado.produto_peso
							, converter: dado.produto_converter
						});
						//FIM CUSTOM
					}
					
					$('#carregando_produtos').hide();					
					buscar_produtos(produtos);

				}
				else
				{
					$('#erro_item').text('Nenhum produto encontrado para à filial ('+obter_valor_sessao('filial')+').');
					$('#carregando_produtos').hide();
					$('#erro_item').show();
				}
			}
		);
	});
	
}


/**
* Metódo:		buscar_produtos
* 
* Descrição:	Função Utilizada para pesquisar os produtos pelo autocomplete e adicionar os valores nos campso quando o produto for selecionado
* 
* Data:			17/10/2013
* Modificação:	17/10/2013
* 
* @access		public
* @param		array 			var produtos		- Todos os produtos
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_produtos(produtos)
{
	
	
	
	$('input[name=produto]').autocomplete({
		minLength: 3,
	//	source: produtos,
		source: function(request, response) {
			
			    var term = retirar_acentos(request.term);
		
		    	var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");
		    	response($.grep(produtos, function(value) {          
	                 
	         
	            return matcher.test(value.label);           
	          
	        }));
	    },
		
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
			
			$('input[name=quantidade]').val('1');
			$('input[name=quantidade]').focus();
			$('input[name=quantidade]').select();
			
			$('input[name=produto]').val(ui.item.label);
			$('input[name=descricao_produto]').val(ui.item.descricao);
			$('input[name=codigo_produto]').val(ui.item.codigo);
			
			$('input[name=preco_tabela]').val(number_format(ui.item.preco_tabela, 3, ',', '.'));
			$('input[name=preco_unitario]').val(number_format(ui.item.preco, 3, ',', '.'));
			$('input[name=preco_venda]').val(number_format(ui.item.preco, 3, ',', '.'));
			
			$('input[name=ipi]').val(number_format(ui.item.ipi, 3, ',', ''));
			$('input[name=unidade_medida]').val(ui.item.unidade_medida);
			$('input[name=local]').val(ui.item.local);
			
			
			$('input[name=variacao_maxima]').val(ui.item.variacao_maxima);
			$('input[name=converter]').val(ui.item.converter);
			
			$('input[name=peso]').val(ui.item.peso);
			$('input[name=peso_total]').val(ui.item.peso);
			
			calcular_total();
			
			// Bloquear campo quando selecionar cliente
			$('input[name=produto]').attr('disabled', 'disabled');
			$('#trocar_produto').show();
			
			return false;
			
		}
	});
}


/**
* Metódo:		aplicar_descontos_cabecalho
* 
* Descrição:	Função Utilizada para aplicar os descontos do cabeçalho (Desconto do Cliente, Regra de Desconto)
* 
* Data:			23/10/2013
* Modificação:	23/10/2013
* 
* @access		public
* @param		string 					preco_produto		- Preço do Produto
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function aplicar_descontos_cabecalho(preco_produto)
{
	// Aplicando Desconto do "Cliente" no preço do produto
	if(obter_valor_sessao('desconto_cliente') > 0)
	{
		var preco = preco_produto - (preco_produto * (obter_valor_sessao('desconto_cliente') / 100));
	}
	else
	{
		var preco = preco_produto;
	}

	// Aplicando Desconto da "Regra de desconto" no preço do produto
	if(obter_valor_sessao('regra_desconto')  > 0)
	{
		var preco = preco - (preco * (obter_valor_sessao('regra_desconto') / 100));
	}
	
	return preco;
}


/**
* Metódo:		exibir_cabecalho
* 
* Descrição:	Função Utilizada para exibir os valores do cabeçalho
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_cabecalho()
{
	// ------
	// Obter descrição da FILIAL e adicionar no cabeçalho
	obter_descricao('filiais', 'razao_social', 'codigo', obter_valor_sessao('filial'), '.filial');
	
	
	// ------
	// Obter descrição da TIPO DE PEDIDO e adicionar no cabeçalho
	var tipo_pedido = obter_valor_sessao('tipo_pedido');
	if(tipo_pedido == 'N')
	{
		$('.tipo_pedido').html('Venda Normal');
	}
	else if(tipo_pedido == '*')
	{
		$('.tipo_pedido').html('Orçamento');
	}
	else
	{
		$('.tipo_pedido').html('N/A');
	}
	
	
	// ------
	// Obter descrição do CLIENTE e adicionar no cabeçalho
	if(obter_valor_sessao('descricao_prospect'))
	{
		$('.cabecalho_cliente_prospect').html('Prospect');
		$('.cliente').html(obter_valor_sessao('descricao_prospect'));
	}
	
	// Obter descrição do CLIENTE e adicionar no cabeçalho
	if(obter_valor_sessao('descricao_cliente'))
	{
		$('.cabecalho_cliente_prospect').html('Cliente');
		$('.cliente').html(obter_valor_sessao('descricao_cliente'));
	}
	
	
	// ------
	// Obter descrição da TABELA DE PREÇOS e adicionar no cabeçalho
	obter_descricao_tabela_precos(obter_valor_sessao('filial'), obter_valor_sessao('tabela_precos'), '.tabela_precos');
	
	
	// ------
	// Obter descrição da CONDIÇÃO DE PAGAMENTO e adicionar no cabeçalho
	obter_descricao('condicoes_pagamento', 'descricao', 'codigo', obter_valor_sessao('condicao_pagamento'), '.condicao_pagamento');
	
	// ------
	// Obter DESCONTO do CLIENTE e adicionar no cabeçalho
	$('.desconto_cliente').html(number_format(obter_valor_sessao('desconto_cliente'), 3, ',', '.') + ' %');

	
	// ------
	// Obter REGRA DE DESCONTO e adicionar no cabeçalho
	$('.regra_desconto').html(number_format(obter_valor_sessao('regra_desconto'), 3, ',', '.') + ' %');
	

	//-------CUSTOM CABEÇALHO
	$('.desconto_contrato').html('');
	$('.tipo_frete_cabecalho').html((obter_valor_sessao('tipo_frete') == 'C' ? 'CIF' : 'FOB'));
	$('.tipo_transporte_cabecalho').html(obter_valor_sessao('dados_tipo_transporte').descricao);
	var porto_destino = obter_valor_sessao('dados_porto_destino');
	$('.porto_destino_cabecalho').html((porto_destino.descricao ? porto_destino.descricao : 'N/A'));
	
}

/**
* Metódo:		obter_descricao
* 
* Descrição:	Função Utilizada para obter a descrição e codigo de um valor na sessão
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					tabela				- A tabela do navegador que será utilizada para realizar o "SELECT" do retorna de descrição
* @param		string 					campo_descricao		- O campo "Descrição" da "tabela" passada no primeiro parametro
* @param		string 					campo_codigo		- O campo "Codigo" da "tabela" passada no primeiro parametro
* @param		string 					codigo				- O Codigo que será usado para comparação no "WHERE"
* @param		string 					classe				- Classe html (.classe) que será adiciona o valor do retorno no html
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_descricao(tabela, campo_descricao, campo_codigo, codigo, classe)
{
	
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT ' + campo_descricao + ' AS descricao, ' + campo_codigo + ' AS codigo FROM ' + tabela + ' WHERE ' + campo_codigo + ' = ?', [codigo], function(x, dados) {

				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);
				
					$(classe).html(dado.codigo + ' - ' + dado.descricao);
				}
				else
				{
					$(classe).html('N/A');
				}
				
			}
		);
	});
	
}

//Chamado: 002106 - Descrição tabela errada


function obter_descricao_tabela_precos(codigo_filial, codigo_tabela_precos, classe){
	
	var where = " WHERE codigo > 0 ";
	
	if(info.empresa)
	{
		where += " AND empresa = '" + info.empresa + "' ";
	}
	
	
	//DATA INICIO DE VIGENCIA DA TABELA DE PREÇO
	where += " AND vigencia_inicio <= '" + date('Ymd') + "'";
	
	//DATA FINAL DE VIGENCIA DA TABELA DE PREÇO
	where += " AND (vigencia_final >= '" + date('Ymd') + "' OR vigencia_final = '')";
	
	
	where += " AND filial_saida = '" + codigo_filial + "' ";
	where += " AND codigo = '"+ codigo_tabela_precos +"' "
	

		db.transaction(function(x) {
			x.executeSql('SELECT * FROM tabelas_preco ' + where, [], function(x, dados) {

			if (dados.rows.length) {
				var dado = dados.rows.item(0);

				$(classe).html(dado.codigo + ' - ' + dado.descricao);
			} else {
				$(classe).html('N/A');
			}
			});
		});
	
}



/**
* Metódo:		salvar_produto_sessao
* 
* Descrição:	Função Utilizada para salvar produtos adicionados
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produto_sessao(produtos_paramentro)
{
	var produtos_paramentro = unserialize(produtos_paramentro);
	
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos'])
	{
		var sessao_produtos = sessao_pedido['produtos'];
		
		var produtos = merge_options(sessao_produtos, produtos_paramentro);
	}
	else
	{
		var produtos = produtos_paramentro;
	}
	
	salvar_sessao('produtos', produtos);

}



/**
* Metódo:		salvar_produto_sessao
* 
* Descrição:	Função Utilizada para salvar produtos removidos
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produto_removido_sessao(produtos_paramentro)
{
	var produtos_paramentro = unserialize(produtos_paramentro);
	
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']])
	{
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos_removidos'])
	{
		var sessao_produtos = sessao_pedido['produtos_removidos'];
	
		var produtos = merge_options(sessao_produtos, produtos_paramentro);
	}
	else
	{
		var produtos = produtos_paramentro;
	}
	
	salvar_sessao('produtos_removidos', produtos);

}


/**
* Metódo:		salvar_produtos_excluidos
* 
* Descrição:	Função Utilizada para salvar produtos (Sem os removidos)
* 
* Data:			20/10/2013
* Modificação:	20/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produtos_excluidos(produtos_paramentro)
{
	var produtos_paramentro = unserialize(produtos_paramentro);

	var produtos = produtos_paramentro;
	
	salvar_sessao('produtos', produtos);
}

/**
* Metódo:		verificar_produtos_destaque
* 
* Descrição:	Obtem os produtos em destaque referente a tabela de preços seleciona
* 					e adiciona ao pedido com quantidade 1 (somente para pedidos sem nenhum item adicionado)
* 				CUSTOM 000177/000870
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function verificar_produtos_destaque()
{
	var tipo_venda = obter_valor_sessao('tipo_venda');
	
	if(!in_array(tipo_venda, ['20', '02']))
	{
		var sessao_produtos 	= obter_produtos_sessao();
		var total_produto		= 0;
		if(sessao_produtos)
		{
			var total_produtos 		= Object.keys(sessao_produtos).length;
		}
		
		if(!total_produtos)
		{
			//Obtem os produtos em destaque
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM produtos WHERE tb_codigo = ? AND ptp_filial = "'+obter_valor_sessao('filial')+'" AND produto_situacao_produto = "DE" AND produto_unidade_medida IN ("' + unidade_medida_permitidas[tipo_venda].join('", "') + '")', [obter_valor_sessao('tabela_precos')], function(x, dados) {
					
					var total = dados.rows.length;
					
					if (total)
					{	
						var produtos = new Array();
						
						var html = '<h2>Produtos em Destaque</h2>';
						html += '<table style="width: 100%; border: solid 1px #CCC;">';
						for(i = 0; i < total; i++)
						{
							var dado = dados.rows.item(i);
							
							var imagem = localStorage.getItem('caminho_local') + 'sem_imagem.jpeg';
							window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
								fileSystem.root.getFile('DWFDV/' + dado.produto_codigo + '.jpeg', {}, function(){
									imagem = localStorage.getItem('caminho_local') + dado.produto_codigo + '.jpeg';
								});
							});
							
							html += '<tr style="background-color: #FFF; border-bottom: solid 1px #CCC; padding: 3px;">';
								html += '<td style="text-align:center;"><img src="' + imagem + '">';
								html += '<td><h3>' + dado.produto_codigo + ' - ' + dado.produto_descricao + '</h3></td>';
							html += '</tr>';
							
							//Adiciona os produtos em destaque com quantidade 1
							produtos[dado.produto_codigo] = new Array();
							produtos[dado.produto_codigo]['codigo'] 			= dado.produto_codigo;
							produtos[dado.produto_codigo]['descricao'] 			= dado.produto_descricao;
							produtos[dado.produto_codigo]['preco_tabela'] 		= dado.ptp_preco;
							produtos[dado.produto_codigo]['preco_unitario'] 	= dado.ptp_preco;
							produtos[dado.produto_codigo]['preco_venda'] 		= dado.ptp_preco;
							produtos[dado.produto_codigo]['quantidade'] 		= '0';
							produtos[dado.produto_codigo]['desconto'] 			= '0';
							produtos[dado.produto_codigo]['ipi']				= dado.produto_ipi;
							produtos[dado.produto_codigo]['unidade_medida'] 	= dado.produto_unidade_medida;
							produtos[dado.produto_codigo]['local']				= dado.produto_locpad;
							produtos[dado.produto_codigo]['peso']				= dado.produto_peso;
							produtos[dado.produto_codigo]['converter']			= dado.produto_converter;
							var variacao = dado.ptp_variacao_maxima;
							if(variacao <= 0)
							{
								variacao = obter_valor_sessao('MV_VARPRE2J');
							}
							produtos[dado.produto_codigo]['variacao_maxima']	= (variacao ? variacao : 0);
						}
						html += '</html>';
						html += '<p class="class_info">* Os produtos em destaque serão adicionados ao pedido, podendo ser alterado ou excluído.</p>';
						
						var produtos = serialize(produtos);
						
						salvar_produto_sessao(produtos);
						
						exibir_produtos();
						
						//Exibi o alerta com a listagem de produtos em destaque
						mensagem(html);
					}
				})
			});
		}
	}
}

/**
* Metódo:		validar_lote
* 
* Descrição:	Função Utilizada para validar o Lote caso seja um pedido de TROCA
* 				O formato do lote deverá seguir o seguinte formato:
* 				L99M99999999X
* 				
* 				O primeiro caracter sempre será a letra "L", seguido de 2 numeros (0 a 9),
* 					o quarto caracter sempre será a letra "M", seguido de 8 numeros
* 					e o ultimo caracter deverá ser a letra A, B ou C
* 
* Data:			21/03/2014
* Modificação:	21/03/2014
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function validar_lote(){
	var lote = strtoupper($('input[name=lote_troca]').val());
	var lote = lote.split(''); //Converte a string do lote para array
	
	var valido = true;
	if(lote.length == 13)
	{
		if(lote[0] != 'L')
		{
			valido = false;
		}
		else if(lote[3] != 'M')
		{
			valido = false;
		}
		else if(!is_numeric(lote[1] + lote[2]))
		{
			valido = false;
		}
		else if(!is_numeric(lote[4] + lote[5] + lote[6] + lote[7] + lote[8] + lote[9] + lote[10] + lote[11]))
		{
			valido = false;
		}
		else if(!in_array(lote[12], ['A', 'B', 'C']))
		{
			valido = false;
		}
	}
	else
	{
		valido = false;
	}
	
	return valido;
}
