var permissoes = {
		
				"preposto" : {
								'clientes':{
									"visualizar": true
								},
								'noticias':{
									"visualizar": false	
									
								},
								'consultas':{
									
									'tabela_de_precos':{
										"visualizar": false
										
									},
									'titulos': {
										"visualizar": true,
										"exibir_comissao": false
										},
									'notas_fiscais': {
										"visualizar": false
									},
									'comissoes': {
										"visualizar": false
									},
									'recebimentos': {
										"visualizar": false
									},
									'pedidos_por_rota': {
										"visualizar": false
									},
									'pendencias': {
										"visualizar": false
									},									
									'pedidos': {
										"visualizar": true
									}
									
								},
								'sincronizar':{
									
									"desabilitar": {
										
										'comissoes'			: true,
										'notas_fiscais'		: true,
										'pedidos_por_rota'	: true,
										'recebimentos'		: true,
										'noticias'			:true
										
										
									}
									
								}
					
							},
				"representante": {
					'clientes':{
						"visualizar": true
					},
					'noticias':{
						"visualizar": true	
						
					},
					'consultas':{
						
						'tabela_de_precos':{
							"visualizar": true
							
						},
						'titulos': {
							"visualizar": true,
							"exibir_comissao": true
							},
						'notas_fiscais': {
							"visualizar": true
						},
						'comissoes': {
							"visualizar": true
						},
						'recebimentos': {
							"visualizar": true
						},
						'pedidos_por_rota': {
							"visualizar": true
						},
						'pendencias': {
							"visualizar": true
						},								
						'pedidos': {
							"visualizar": true
						}
						
					},
					'sincronizar':{
						
						"desabilitar": {
							
						
							
							
						}
						
					}
		
				},
		
				};

$(document).ready(function(){
		obter_grupo_permissao(function(grupo_permissao){
			permissao = permissoes[grupo_permissao.grupo];
			
			if($("#index_noticias").length > 0){
			
				
				if(permissao.noticias.visualizar){				
					$("#index_noticias").show();
				}
			}
			
			if($(".consultar_titulos_comissao").length > 0){
				if(permissao.consultas.titulos.exibir_comissao){				
					$(".consultar_titulos_comissao").show();
				}
			}
			
			
			/*Permiss�o pedido usu�rio inativo*/
			
			if($('.pedido_novo_pedido').length > 0){
				if(localStorage.getItem('status') == 'bloqueado'){
					$('.pedido_novo_pedido').hide();
					
				}
				
			}
			
			
			$.each(permissao.consultas, function( index, value ) {
				
				console.log("#consulta_"+ index);
				
				console.log(typeof(permissao.consultas[index].visualizar)+ ' - '+ permissao.consultas[index].visualizar);
				if($("#consulta_"+ index).length > 0){				
					
					if(permissao.consultas[index].visualizar){				
						$("#consulta_"+index).show();
					}
					
					
				}	

				
				
			});
			
			
			
		
		});
	
});


function obter_grupo_permissao(callback){
	
	db.transaction(function(x) {
		x.executeSql(
				'SELECT * FROM informacoes_do_representante', [], function(x, dados) {
					if (dados.rows.length)
					{
							
						var representante = dados.rows.item(0);
						
						
						
						if(representante.codigo_preposto){
							callback({
								"codigo_preposto": representante.codigo_preposto,
								"grupo": "preposto"
							});
							
						}else{
							callback({
								"codigo_preposto": representante.codigo_preposto,
								"grupo": "representante"
							});
						}
						
					}
				}
			);
		
	});
	
}