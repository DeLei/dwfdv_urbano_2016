//CONFIGURAÇÕES GERAIS
var config = {
		versao: 				'1.0.4.3',
		apk:					'Urbano',
		/*Homologacao*/
		/*ws_url: 				'http://201.24.78.182:8081/desenvolvimento/dwfdv/1.0.4.3/ws/',
		dw_fdv: 				'http://201.24.78.182:8081/desenvolvimento/dwfdv/1.0.4.3/ws/',
		ws_url_impostos: 		'http://201.24.78.182:8081/desenvolvimento/dwfdv/1.0.4.3/ws/webservices/impostos/',
		url_online: 			'http://201.24.78.182:8081/desenvolvimento/dwpdr/',*/
		
		
		/*Publicacao*/
		ws_url: 				'http://201.24.78.182:8081/dwfdv/1.0.4.3/ws/',
		dw_fdv: 				'http://201.24.78.182:8081/dwfdv/1.0.4.3/ws/',
		ws_url_impostos: 		'http://201.24.78.182:8081/dwfdv/1.0.4.3/ws/webservices/impostos/',
		url_online: 			'http://201.24.78.182:8081/dwpdr/',
		
		sincronizar: 			'24',
		forca_sincronizar: 		false,
		modulos_obrigatorios: 	{
		
		},
		variacao_maxima_preco:	50 //Percentual máximo permitido acima do preço de tabela
	};

// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA
	var lista_tipo_venda = [{
		    valor: "01",
		    descricao: "Vendas"
		}, 
		{
			valor: "02",
		    descricao: "Bonificação"
		}, 
		{
			valor: "03",
		    descricao: "Malha"
		}, 
		{
			valor: "20",
		    descricao: "Troca"
		}, 
		{
			valor: "30",
		    descricao: "Amostra"
		}, 
		{
			valor: "50",
		    descricao: "Trans"
		}, 
		{
			valor: "99",
		    descricao: "Outras"
		}];		
// FIM CUSTOM: 177 / 000874 - 04/11/2013

// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-BONIFICACAO
	var lista_tipo_bonificacao = [{
		    valor: "02",
		    descricao: "Introdução",
		    descricao_protheus: "INTRODUCAO"
		}, 
		{
			valor: "03",
		    descricao: "Aniversário",
		    descricao_protheus: "ANIVERSARIO"
		}, 
		{
			valor: "06",
		    descricao: "Abertura da Loja",
		    descricao_protheus: "ABERT.LOJA"
		}, 
		{
			valor: "07",
		    descricao: "Encarte",
		    descricao_protheus: "ENCARTE"
		}, 
		{
			valor: "08",
		    descricao: "Estratégia de Mercado",
		    descricao_protheus: "ESTRAT.MERCADO"
		}, 
		{
			valor: "09",
		    descricao: "Publicação",
		    descricao_protheus: "PUBLICIDADE"
		}, 
		{
			valor: "10",
		    descricao: "Contrato",
		    descricao_protheus: "CONTRATO"
		}, 
		{
			valor: "11",
		    descricao: "Doação",
		    descricao_protheus: "DOACAO"
		}];	
// FIM CUSTOM: 177 / 000874 - 04/11/2013



// CUSTOM: 177 / 000874 - 07/11/2013 - Localizar: CUST-BANCO
	var lista_banco = [{
		    valor: "10",
		    descricao: "BANCO DO BRASIL"
		}, 
		{
			valor: "11",
		    descricao: "BRADESCO"
		}, 
		//{
		//	valor: "399",
		//    descricao: "HSBC"
		//}, 
		{
			valor: "84",
		    descricao: "CARTEIRA"
		},		
		{
			valor: "85",
		    descricao: "ESPECIE"
		},
		{
			valor: "341",
		    descricao: "ITAU"
		},
		{
			valor: "033",
		    descricao: "SANTANDER"
		}];		
// FIM CUSTOM: 177 / 000874 - 04/11/2013
	
	
	// CUSTOM: 177 / 000874 - 07/11/2013 - Localizar: CUST-BANCO
	var lista_forma_pagamento = [{
		    valor: "BL",
		    descricao: "BOLETO"
		}, 
		{
			valor: "CH",
		    descricao: "CHEQUE"
		}, 
		{
			valor: "ES",
		    descricao: "ESPECIE"
		} ,
		{
			valor: "CR",
		    descricao: "CRED. CONTA"
		} 
		];	
// FIM CUSTOM: 177 / 000874 - 04/11/2013	
	