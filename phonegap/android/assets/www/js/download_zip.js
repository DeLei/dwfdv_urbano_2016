var DownloadZip = function(arquivoZip, calback, formato, funcaoErro)
{
	
	/**
	* Metódo:		iniciarDownload
	* 
	* Descrição:	Função Utilizada para iniciar o dispositivo cordova
	* 
	**/
	this.iniciarDownload = function()
	{
		$( "#progressbar" ).progressbar({value: 0});
		
		console.log('[DOWNLOAD] - Iniciando Cordova');
		document.addEventListener("deviceready", this.criarDiretorio, false);
	}
	
	
	/**
	* Metódo:		criarDiretorio
	* 
	* Descrição:	Função Utilizada para criar a pasta DWFDV da raiz do android
	* 
	**/
	this.criarDiretorio = function()
	{
		console.log('[DOWNLOAD] - Criar diretorio DWFDV');
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
			
			fs.root.getDirectory('DWFDV', {create: true, exclusive: false}, function(dirEntry) {
				
			    realizarDownload();

			    
			}, this.falha);
			
		}, this.falha);
	}
	
	
	var realizarDownload = function()
	{
		//--------------
		//--- Layout ---
		
		// Carregando
		$('#carregando').show();
		$('#btnCancelar').show();
		$('#btnSincronizar').hide();
		$('#carregando').find('span:first').html('Baixando Pacotes…');
		
		//---------------
		
		
		var remoteFile = arquivoZip;
		
		console.log('[DOWNLOAD] - Realizar Download (' + remoteFile + ')');
		
		//var localFileName = remoteFile.substring(remoteFile.lastIndexOf('/')+1);
		
		var remoteFiles = remoteFile.split('/');
		
		
		if(!formato)
		{
			formato = 'zip';
		}
				
		var localFileName = remoteFiles.pop() + '.' + formato;
		
		console.log('[DOWNLOAD] - remoteFiles - ' + remoteFiles.pop() + '.' + formato);
		
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
			
			fileSystem.root.getFile('DWFDV/' + localFileName, {create: true, exclusive: false}, function(fileEntry) {

				var localPath = fileEntry.fullPath;
				
				
				
				if (device.platform === "Android" && localPath.indexOf("file://") === 0) {
					localPath = localPath.substring(7);
				}

				//--------------------------------------
				//---- Salvando caminho local no storage{
				var localPaths = localPath.split('DWFDV');
		
				var caminho_local = localPaths.shift();
				localStorage.setItem('caminho_local', caminho_local + 'DWFDV/');
				
				console.log('[DOWNLOAD] - caminho_local - <' + localStorage.getItem('caminho_local') + '>');
				
				//---- }
				//--------------------------------------
				
				var ft = new FileTransfer();
				
				//------------------------------
				//-------- progress ------------		
				ft.onprogress = function(progressEvent) {
					
					
					
					if (progressEvent.lengthComputable) {
						
						var perc = Math.floor(progressEvent.loaded / (progressEvent.total * 2) * 100);
						console.log('[DOWNLOAD] - ' + perc + "% loaded...");
						
						$( "#progressbar" ).progressbar({value: perc});
						
					} else {
						
						console.log('[DOWNLOAD] - Loading');
						
					}
				};		
				//-------- progress ------------
				//------------------------------
				
				// Fixando a KEY para todos os DOWNLOADS.
				/*
				var opcoes_download;
				opcoes_download.headers = {"DW_KEY_APP" : localStorage.getItem('key')};
				opcoes_download.params 	= {
					id_usuario: 					localStorage.getItem('id'),
					codigo_representante: 			localStorage.getItem('codigo'),
					latitude: 						localStorage.getItem('gps_latitude'),
					longitude: 						localStorage.getItem('gps_longitude'),
					accuracy: 						localStorage.getItem('gps_accuracy'), 
					altitude: 						localStorage.getItem('gps_altitude'), 
					altitudeAccuracy: 				localStorage.getItem('gps_altitudeAccuracy'), 
					heading: 						localStorage.getItem('gps_heading'), 
					speed: 							localStorage.getItem('gps_speed'),
					timestamp: 						localStorage.getItem('gps_timestamp'),
					macAddress: 					localStorage.getItem('macAddress'),
					versao: 						config.versao
				};
				
				console.log('---------------- opcoes_download ----------------------');
				console.log(opcoes_download);
				*/
				
				ft.download(remoteFile, localPath, function(entry) {
				
					console.log('[DOWNLOAD] - Concluído');
					
					console.log('[DOWNLOAD] - fullPath <' + entry.fullPath + '>');
					
					calback();
					
				}, 
				falha,
				false,
				{
					headers: {
            			"DW_KEY_APP" : 				localStorage.getItem('key'),
            			"DW_ID_USUARIO": 			localStorage.getItem('id'),
						"DW_CODIGO_REPRESENTANTE": 	localStorage.getItem('codigo'),
						"DW_LATITUDE": 				localStorage.getItem('gps_latitude'),
						"DW_LONGITUDE": 			localStorage.getItem('gps_longitude'),
						"DW_ACCURACY": 				localStorage.getItem('gps_accuracy'), 
						"DW_ALTITUDE": 				localStorage.getItem('gps_altitude'), 
						"DW_ALTITUDEACCURACY": 		localStorage.getItem('gps_altitudeAccuracy'), 
						"DW_HEADING": 				localStorage.getItem('gps_heading'), 
						"DW_SPEED": 				localStorage.getItem('gps_speed'),
						"DW_TIMESTAMP": 			localStorage.getItem('gps_timestamp'),
						"DW_MACADDRESS": 			localStorage.getItem('macAddress'),
						"DW_VERSAO": 				config.versao
        			}
        		});
				
			}, falha);
			
		}, falha);
	}
	
	/**
	* Metódo:		falha
	* 
	* Descrição:	Função Utilizada para exibir o erro ocorrido
	* 
	**/
	var falha = function (error) {
		console.log('[DOWNLOAD] - ERRO');
		console.log(JSON.stringify(error));
		
		if(!funcaoErro)
		{
			apprise('A sincronização não foi totalmente concluída. Você deseja tentar novamente?', {
				'verify': true,
				'textYes': 'Sim',
				'textNo': 'Não'
			}, function (dado) {
				if (dado)
				{
					window.location = 'sincronizar.html';
				}
				else
				{
					window.location = 'index.html';
				}
			});
		}
		else
		{
			funcaoErro();
		}
		
	}
	
	
	
}

