$(document).ready(function() {
	var codigo_e_empresa 	= parse_url(location.href).fragment;
	array_codigo_e_empresa 	= codigo_e_empresa.split('|');
	var codigo_da_rota 		= array_codigo_e_empresa[0];
	var codigo_da_filial 	= array_codigo_e_empresa[1];
	
	$('#nome_representante').html('Representante: '+info.nome_representante) ; 
	

	
	obter_pedidos_por_rota_detalhes(codigo_da_rota, codigo_da_filial,sessionStorage['pedidos_por_rota_ver_detalhes_pagina_atual'], sessionStorage['pedidos_por_rota_ver_detalhes_ordem_atual']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_pedidos_por_rota_detalhes(codigo_da_rota, codigo_da_filial,sessionStorage['pedidos_por_rota_ver_detalhes_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_pedidos_por_rota_detalhes(codigo_da_rota, codigo_da_filial,sessionStorage['pedidos_por_rota_ver_detalhes_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['notas_' + name] != 'undefined' ? sessionStorage['notas_' + name] : '');
	});
	
	
	
	
	
	$('#filtrar').click(function() {
		sessionStorage['pedidos_por_rota_ver_detalhes_pagina_atual'] = 1;
				
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['' + name] = $(this).val();
		});
		
		obter_pedidos_por_rota_detalhes(codigo_da_rota, codigo_da_filial,sessionStorage['pedidos_por_rota_ver_detalhes_pagina_atual'], sessionStorage['pedidos_por_rota_ver_detalhes_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['pedidos_por_rota_ver_detalhes_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['notas_' + name] = '';
		});
		
		obter_pedidos_por_rota_detalhes(codigo_da_rota, codigo_da_filial,sessionStorage['pedidos_por_rota_ver_detalhes_pagina_atual'], sessionStorage['pedidos_por_rota_ver_detalhes_ordem_atual']);
	});
	
	
	//-- #ver_titulos -  esta função esta no arquivo geral.js
	//$('#ver_titulos')
	
});

function obter_pedidos_por_rota_detalhes(codigo_rota, codigo_filial, pagina, ordem)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'filial ASC';
	
	
	console.log('CF: '+codigo_filial);
	
	console.log('CL:'+codigo_rota);
	
	// setar pagina e ordem atual
	sessionStorage['pedidos_por_rota_ver_detalhes_pagina_atual'] = pagina;
	sessionStorage['pedidos_por_rota_ver_detalhes_ordem_atual'] = ordem;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['pedidos_por_rota_ver_detalhes_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'ASC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'DESC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['pedidos_por_rota_ver_detalhes_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	
	var wheres = '';
	
	wheres += ' AND filial = ?  AND codigo = ? ';
	
	console.log('SELECT filial, codigo, descricao, total_kg, total_fd, data_emissao,codigo_pedido, codigo_cliente, loja_cliente,situacao_credito,situacao_estoque,data_previsao_entrega FROM pedidos_por_rota WHERE codigo_representante != "" ' + wheres +  ' ORDER BY ' + sessionStorage['pedidos_por_rota_ver_detalhes_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);
		
	db.transaction(function(x) {
		x.executeSql(
			'SELECT filial, codigo, descricao, total_kg, total_fd, data_emissao,codigo_pedido, codigo_cliente, loja_cliente,situacao_credito,situacao_estoque,data_previsao_entrega FROM pedidos_por_rota WHERE codigo_representante != "" ' + wheres +   ' ORDER BY ' + sessionStorage['pedidos_por_rota_ver_detalhes_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [codigo_filial, codigo_rota], function(x, dados) {
			
				if (dados.rows.length)
				{
					$('table tbody').empty();
			
			
					var total_pedidos = 0;
					for (i = 0; i < dados.rows.length; i++)
					{
						var item 				 = dados.rows.item(i);	
						
						var codigo 		 		 = item.codigo ? item.codigo: 'N/A';
						var descricao 		 	 = item.descricao ? item.descricao: 'N/A';
						var codigo_filial 		 = item.filial ? item.filial: 'N/A';
						var codigo_pedido 		 = item.codigo_pedido ? item.codigo_pedido: 'N/A';
						var data_emissao 		 = item.data_emissao ? item.data_emissao: 'N/A';						
						var codigo_cliente 		 = item.codigo_cliente ? item.codigo_cliente: 'N/A';						
						var loja_cliente 		 = item.loja_cliente ? item.loja_cliente: 'N/A';
						var situacao_credito 	 = item.situacao_credito ? item.situacao_credito: 'N/A';
						var situacao_estoque 	 = item.situacao_estoque ? item.situacao_estoque: 'N/A';
						var data_previsao_entrega= item.data_previsao_entrega ? item.data_previsao_entrega: 'N/A';
						
						var total_kg			= item.total_kg ? item.total_kg: 'N/A';	
						var total_fd			= item.total_fd ? item.total_fd: 'N/A';	
						
						$('#codigo_filial').html('Filial: '+codigo_filial) ; 
						$('#rota').html('Rota: '+codigo+'-'+descricao) ; 
						
						var html = '';	
						
						
							html += '<td><a href="pedidos_espelho.html#' + codigo_pedido + '|' + codigo_filial + '" >'+codigo_pedido+'</a></td>';
							//html += '<td align="center">'+codigo_pedido+'</td>';
							html += '<td align="center">'+protheus_data2data_normal(data_emissao)+'</td>';
							html += '<td align="center">'+codigo_cliente+'/'+loja_cliente  +'</td>';
							
							html += '<td align="center">'+obter_situacao(situacao_credito)+'</td>';
							html += '<td align="center">'+obter_situacao(situacao_estoque)+'</td>';
							html += '<td align="center">'+protheus_data2data_normal(data_previsao_entrega)+'</td>';
						
							html += '<td align="center">'+  (number_format(total_kg, 2, ',', '.'))+'</td>';
							html += '<td align="center">'+ (number_format(total_fd, 2, ',', '.'))+'</td>';
							
						$('table tbody').append('<tr>' + html + '</tr>');
					}
					
					
					var somar = 'SUM(total_kg) AS valor_total, ';
					somar += 'SUM(total_fd) AS saldo_total ';
					
							
					console.log('SELECT '+somar+' FROM pedidos_por_rota WHERE codigo_representante != "" ' + wheres );
						
					// calcular totais
					x.executeSql(
						'SELECT '+somar+' FROM pedidos_por_rota WHERE codigo_representante != "" ' + wheres   , [codigo_filial, codigo_rota], function(x, dados) {
							
							var dado = dados.rows.item(0);
							//$('#valor_total').text(number_format(dado.valor_total, 3, ',', '.'));
							//$('#saldo_total').text(number_format(dado.saldo_total, 3, ',', '.'));
							JSON.stringify(dado);
							$('table tbody').append('<tr><td align="left" colspan="6">Total</td><td align="center">'+number_format(dado.valor_total, 2, ',', '.')+'</td><td  align="center">'+number_format(dado.saldo_total, 2, ',', '.')+'</td></tr>');
							
						}
					);
					
					
					
					
					alterarCabecalhoTabelaResolucao();
				
				}
				else
				{
					$('table tbody').html('<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhuma comissão encontrada.</strong></td></tr>');
				}
			}
		);
		
		
		
	});
}
