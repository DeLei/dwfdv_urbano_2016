	/**
	* Metódo:		obter_valor_sessao
	* 
	* Descrição:	Função Utilizada para retornar o valor de campo da sessão
	* 
	* Data:			11/10/2013
	* Modificação:	11/10/2013
	* 
	* @access		public
	* @param		String 			var campo		- Nome do campo que será utilizado para retornar o valor na sessao
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_valor_sessao(campo)
	{
		if(sessionStorage[sessionStorage['sessao_tipo']])
		{
			var sessao = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		}
		
		if(sessao)
		{
			if(sessao[campo])
			{
				return sessao[campo];
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

$(document).ready(function() {
	

	
	
	$('#atualizar_estoque').click(function(e) {
			e.preventDefault();
	
			confirmar('Deseja atualizar o estoque dos produtos?', 
				function () {
					$(this).dialog('close');
					
					//------------------
					//---------------------------------
			
					location.href = "sincronizar.html?produtos=sincronizar";
					
					//---------------------------------
					//------------------
					
					$("#confirmar_dialog").remove();
				});
		
	});
	
	
	
	
	// obter produtos
	obter_produtos(sessionStorage['produtos_pagina_atual'], sessionStorage['produtos_ordem_atual']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_produtos(sessionStorage['produtos_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_produtos(sessionStorage['produtos_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['produtos_' + name] != 'undefined' ? sessionStorage['produtos_' + name] : '');
	});
	
	// obter filiais
	obter_filiais_permitidas(function(filiais){
		var filiais_permitidas = new Array();
		
		$('select[name=produto_filial]').append('<option value="">Todos</option>');			
		$.each(filiais, function(key, dado) {
			filiais_permitidas.push(dado.codigo);
			$('select[name=produto_filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
		});
		
		
		var where = 'filial_saida IN (\'' + filiais_permitidas.join('\', \'') + '\')';
		// obter tabelas
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM tabelas_preco WHERE ' + where, [], function(x, dados) {
					
					var total = dados.rows.length;
					
					$('select[name=tb_codigo]').append('<option value="">Todos</option>');
					
					for(i = 0; i < total; i++)
					{
						var dado = dados.rows.item(i);
						$('select[name=tb_codigo]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
					}
					
				}
			);
		});
	});
	
	
	$('#filtrar').click(function() {
		sessionStorage['produtos_pagina_atual'] = 1;
		sessionStorage['produtos_unidade'] = $('#unidade').val();
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['produtos_' + name] = $(this).val();
		});
		
		obter_produtos(sessionStorage['produtos_pagina_atual'], sessionStorage['produtos_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['produtos_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['produtos_' + name] = '';
		});
		
		obter_produtos(sessionStorage['produtos_pagina_atual'], sessionStorage['produtos_ordem_atual']);
	});
});

function obter_quantidades_fardos_disponiveis(filial, tabela_precos, callback){
	var faixas_fardo = [];
	
	var wheres = '';
	if(filial){
		wheres += " AND codigo_filial = '"+filial+"' ";	
	}
	
	if(tabela_precos){
		wheres += " AND codigo_tabela_precos = '"+tabela_precos+ "' ";
	}
	

	db.transaction(function(x) {
		x.executeSql(
			'SELECT DISTINCT quantidade FROM desconto_por_fardo WHERE codigo_categoria > 0 ' + wheres , [], function(x, dados) {
				
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var item = dados.rows.item(i);
						faixas_fardo.push(item.quantidade);
					}
					callback(faixas_fardo);
				}else{
					callback([10,50,300]);
				}
			});
	});
	
	
	
}


function obter_produtos(pagina, ordem)
{
	
	obter_filiais_permitidas(function(filiais){
		var filiais_permitidas = new Array();
		$.each(filiais, function(key, dado) {
			filiais_permitidas.push(dado.codigo);
		});
		
			
		pagina = pagina ? pagina : 1;
		ordem = ordem ? ordem : 'produto_descricao ASC';
		
		// setar pagina e ordem atual
		sessionStorage['produtos_pagina_atual'] = pagina;
		sessionStorage['produtos_ordem_atual'] = ordem;
		
		// definir seta
		var ordem_atual = explode(' ', sessionStorage['produtos_ordem_atual']);
		
		if (ordem_atual[1] == 'ASC')
		{
			$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'ASC');
			
			$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▲');
		}
		else
		{
			$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'DESC');
			
			$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▼');
		}
		
		// calcular offset
		var offset = (sessionStorage['produtos_pagina_atual'] - 1) * 20;
		
		// gerar filtros
		
		filial = sessionStorage['produtos_produto_filial'];
		codigo = sessionStorage['produtos_produto_codigo'];
		descricao = sessionStorage['produtos_produto_descricao'];
		tabela_de_precos = sessionStorage['produtos_tb_codigo'];
		
		var wheres = '';
		
		if (filial && filial != 'undefined')
		{
			//wheres += " AND produto_filial = '" + filial + "'";
			wheres += " AND (produto_filial = '" + filial + "' OR produto_filial = ' ')";
		}
		else
		{
			wheres += ' AND produto_filial IN (\'' + filiais_permitidas.join('\', \'') + '\')';
		}
		
		if (tabela_de_precos && tabela_de_precos != 'undefined')
		{
			wheres += " AND tb_codigo = '" + tabela_de_precos + "'";
		}
		
		if (codigo && codigo != 'undefined')
		{
			wheres += " AND produto_codigo LIKE '%" + codigo + "%'";
		}
		
		if (descricao && descricao != 'undefined')
		{
			wheres += " AND produto_descricao LIKE '%" + descricao + "%'";
		}
		
		if (info.empresa)
		{
			//wheres += " AND empresa = '" + info.empresa + "'";
		}
		console.log('TB: '+tabela_de_precos);
		console.log('Wheres: '+wheres);
		
		obter_quantidades_fardos_disponiveis(filial,tabela_de_precos,function(faixas_fardo){
			var join = ' ';
			var select = ' ';
			$.each(faixas_fardo,function(i, item){
				join += " LEFT JOIN desconto_por_fardo AS faixa"+(i+1)+" ON ("
					 +	" faixa"+(i+1)+".codigo_categoria 			= produtos.produto_codigo_categoria"
					 +	" AND faixa"+(i+1)+".codigo_filial			= produtos.produto_filial"
					 + 	" AND faixa"+(i+1)+".codigo_tabela_precos	= produtos.ptp_codigo_tabela_precos"
					 +	" AND faixa"+(i+1)+".quantidade				= "+item		
					+")";
				
				select += 'faixa'+(i+1)+'.desconto_maximo_reais AS faixa'+(i+1)+'_desconto_maximo_reais, '
						+ 'faixa'+(i+1)+'.quantidade AS faixa'+(i+1)+'_quantidade, ';
				
				var  faixa_label = '#faixa'+(i+1)+'_header';
				
				
				$(faixa_label).show();
				
			});
			
			
			console.log('SELECT DISTINCT '+select+' produtos.* FROM produtos '+join+' WHERE produto_codigo > 0 ' + wheres + ' ORDER BY ' + sessionStorage['produtos_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);
			
			db.transaction(function(x) {
				x.executeSql(
					'SELECT DISTINCT '+select+' produtos.* FROM produtos '+join+' WHERE produto_codigo > 0 ' + wheres + ' ORDER BY ' + sessionStorage['produtos_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
					
						if (dados.rows.length)
						{
							$('table tbody').empty();				
							for (i = 0; i < dados.rows.length; i++)
							{
								
							//obter_variacao_desconto(dados.rows.item(i),function(item, variacao_parametro){
									var item = dados.rows.item(i);
									var variacao_parametro = 0;
									
									var itens = [];
									
									itens.push('<p style="text-align: center;">'+item.produto_filial+'</p>');
									itens.push('<p style="text-align: center;">'+item.produto_codigo+'</p>');
									itens.push(item.produto_descricao);
									itens.push('<p style="text-align: center;">'+item.tb_codigo+'</p>');
									/*itens.push(number_format(item.quantidade_disponivel_estoque, 0, ',', '.'));*/
									itens.push('<p style="text-align: right;">'+number_format(item.ptp_preco, 3, ',', '.')+'</p>');
									/*itens.push(number_format(item.produto_ipi, 3, ',', '.'));*/
								
								
									var desconto_anterior = 0;
									
									/* CHAMADO: 001962 - TABELA PRECO COLUNA
									obter_preco_minimo(item.ptp_preco,0,variacao_parametro, function(valor_minimo){
										
										itens.push('<p style="text-align: right;">'+number_format(valor_minimo,3,',','.')+'</p>');
										
									
										
									});*/
									
									$.each(faixas_fardo,function(index, faixa){								
										var desconto_maximo_reais = eval('item.faixa'+(index+1)+'_desconto_maximo_reais');
										
										if(!!!desconto_maximo_reais ){
											desconto_maximo_reais = desconto_anterior;
										}
										desconto_anterior = desconto_maximo_reais;
										
										converter = item.produto_converter;
									
										
										obter_preco_minimo(item.ptp_preco,desconto_maximo_reais,variacao_parametro,converter, function(valor_minimo){
											itens.push('<p style="text-align: right;">'+number_format(valor_minimo,3,',','.')+'</p>');
										});
								
									
										
									});
									var html = concatenar_html('<td>', '</td>', itens);
									
									$('table tbody').append('<tr>' + html + '</tr>');	
								//});
								
								
								
							
							}
							
					
							alterarCabecalhoTabelaResolucao();
						
						}
						else
						{
							$('table tbody').html('<tr><td colspan="7" style="color: #900; padding: 10px;"><strong>Nenhum produto encontrado.</strong></td></tr>');
						}
					}
				);
				
				// calcular totais
				x.executeSql(
					'SELECT COUNT(produto_codigo) AS total FROM produtos WHERE produto_codigo > 0 ' + wheres, [], function(x, dados) {
						
			
						var dado = dados.rows.item(0);
	
						
						$('#total').text(number_format(dado.total, 0, ',', '.'));
						
						// paginação
						
						$('#paginacao').html('');
						
						var total = ceil(dado.total / 20);
						
						if (total > 1)
						{
							if (sessionStorage['produtos_pagina_atual'] > 6)
							{
								$('#paginacao').append('<a href="#" onclick="obter_produtos(1, \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
							}
							
							if (sessionStorage['produtos_pagina_atual'] > 1)
							{
								$('#paginacao').append('<a href="#" onclick="obter_produtos(' + (intval(sessionStorage['produtos_pagina_atual']) - 1) + ', \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">&lt;</a> ');
							}
							
							for (i = intval(sessionStorage['produtos_pagina_atual']) - 6; i <= intval(sessionStorage['produtos_pagina_atual']) + 5; i++)
							{
								if (i <= 0 || i > total)
								{
									continue;
								}
								
								if (i == sessionStorage['produtos_pagina_atual'])
								{
									$('#paginacao').append('<strong>' + i + '</strong> ');
								}
								else
								{
									$('#paginacao').append('<a href="#" onclick="obter_produtos(' + i + ', \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">' + i + '</a> ');
								}
							}
							
							if (sessionStorage['produtos_pagina_atual'] < total)
							{
								$('#paginacao').append('<a href="#" onclick="obter_produtos(' + (intval(sessionStorage['produtos_pagina_atual']) + 1) + ', \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">&gt;</a> ');
							}
							
							if (sessionStorage['produtos_pagina_atual'] <= total - 6)
							{
								$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_produtos(' + total + ', \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">Última Página</a> ');
							}
						}
					}
				);
			});
			
			
			
			
		});
	
	});

	

}
