$(document).ready(function() {
	
	//******************************************
	//RETORNAR PENDENCIA
	//******************************************
	var id 	= parse_url(location.href).fragment;	

	db.transaction(function(x) {
		x.executeSql('SELECT * FROM pendencias LEFT JOIN pendencias_usuarios ON (pendencias_usuarios.id = pendencias.id_usuario) WHERE pendencias.id = ?', [id], function(x, dados) {
				var pendencia = dados.rows.item(0);
				
				//Setar 
				var status = '';
				var prioridade = '';
				
				if(!pendencia.exportado)
				{
					$('#mensagemPendencia').hide();
					$('#formMensagem').hide();
					$('#aguardandoSincMensagem').show();
					
					if(!pendencia.editado)
					{
					$('#btnPendenciaEditar').attr('data-id', pendencia.id);
					$('#btnPendenciaEditar').show();
					}
				}
				else
				{
					var pendencias_visualizadas = localStorage.getItem('pendencias_visualizadas');
					if(!pendencias_visualizadas) {
						pendencias_visualizadas = new Object();
					} else {
						pendencias_visualizadas = unserialize(pendencias_visualizadas);
					}
					pendencias_visualizadas[pendencia.id] = time();
					var pendencias_visualizadas = serialize(pendencias_visualizadas);
					localStorage.setItem('pendencias_visualizadas', pendencias_visualizadas);
				}
				
				//Limpar tbody das mensagens
				$('#mensagemPendencia > tbody').empty();
				
				//Formatar Status
				if(pendencia.status == 'em_aberto')
				{
					status = 'Em Aberto';
					if(pendencia.exportado == '1')
					{
						$('#btnPendenciaSolicitarEncerramento').attr('data-id', pendencia.id);
						$('#btnPendenciaSolicitarEncerramento').show();
					}					
				} 
				else if(pendencia.status == 'aguardando_encerramento')
				{
					status = 'Aguardando Encerramento';
				} 
				else if(pendencia.status == 'encerrada')
				{
					status = 'Encerrada';										
					$('#formMensagem').hide();					
				}
				
				if(pendencia.id_usuario == info.id_rep && pendencia.exportado == '1')
				{
					$('#btnPendenciaExcluir').attr('data-id', pendencia.id);
					$('#btnPendenciaExcluir').show();
				}
				
				//Formatar Prioridade
				if(pendencia.prioridade == 'baixa'){
					prioridade = 'Baixa';
				} else if(pendencia.prioridade == 'media'){
					prioridade = 'Média';
				} else if(pendencia.prioridade == 'alta'){
					prioridade = 'Alta';
				}
				
				$('#id_pendencia').val(pendencia.id);
				$('#dePendencia').html((pendencia.nome_real_usuario != 0)?pendencia.nome_real_usuario:pendencia.nome);
				$('#paraPendencia').html(pendencia.para);
				
				$('#tituloPendencia').html(pendencia.titulo);
				
				
				$('#criacaoPendencia').html(date('d/m/Y H:i:s', pendencia.timestamp));
				$('#statusPendencia').html(status);
				
				$('#prioridadePendencia').html(prioridade);
				
				if(pendencia.encerramento_timestamp)
				{
					$('#encerramentoPendencia').html(date('d/m/Y H:i:s', pendencia.encerramento_timestamp));
				}
				
				$('#descricaoPendencia').html(pendencia.descricao);
								
				x.executeSql('SELECT * FROM pendencias_mensagens WHERE id_pendencia = ?', [id], function(x, mensagens) {
					
					var html = '';
					console.log('##################################');
					console.log('Mensagens: '+mensagens.rows.length);
					if(mensagens.rows.length)
					{
					
						for (i = 0; i < mensagens.rows.length; i++)
						{
							var mensagem 	= mensagens.rows.item(i);
							
							console.log('Mensagem: '+mensagem.timestamp);
						
							html += '	<tr>';
							html += '		<td valign="top">'+(mensagem.timestamp ? date('d/m/Y H:i:s', mensagem.timestamp) : 'N/A')+'</td>';
							html += '		<td valign="top">'+(mensagem.usuario ? mensagem.usuario : 'N/A')+'</td>';		
							html += '		<td valign="top">'+(mensagem.conteudo ? mensagem.conteudo : 'N/A')+'</td>';
							html += '	</tr>';
						}
					}
					else
					{
						html += '	<tr>';
					 	html += '		<td valign="top" colspan="3">Não existe mensagens.</td>';
						html += '	</tr>';
					}
					console.log('##################################');
					//Adicionar mensagens
					$('#mensagemPendencia > tbody').append(html);
				});				
			}
		);
	});
	//******************************************
	//RETORNAR PENDENCIA
	//******************************************
	
	//******************************************
	//Adicinar Mensagens Pendência
	//******************************************
	//-----------------------------
	// INICIO - SALVAR PENDENCIA
	//-----------------------------
	$('form[name=formMensagem]').submit(function() {
		$('#carregandoAguarde').show();
		if (!$('textarea[name=conteudo]').val())
		{
			mensagem('Campo <strong>Mensagem</strong> deve ser preenchido.');
		}
		else
		{			
			sleep(1);			
			db.transaction(function(x) {
				//SELECT EM MENSAGEM PENDÊNCIAS
				x.executeSql('SELECT id FROM pendencias_mensagens ORDER BY id DESC LIMIT 1', [], function(x, dados) {
					
					var id = uniqid();
					
					var id_pendencia = $('#id_pendencia').val();
					
					var tmp_1 = ['id'];
					var tmp_2 = ['?'];
					var tmp_3 = [id];
					
					tmp_1.push('timestamp');
					tmp_2.push('?');
					tmp_3.push(time());
					
					tmp_1.push('id_pendencia');
					tmp_2.push('?');
					tmp_3.push(id_pendencia);
					
					tmp_1.push('id_usuario');
					tmp_2.push('?');
					tmp_3.push(info.id_rep);
					
					tmp_1.push('usuario');
					tmp_2.push('?');
					tmp_3.push((info.nome_representante ? info.nome_representante : info.nome_curto_representante));
					
					tmp_1.push('conteudo');
					tmp_2.push('?');
					tmp_3.push($('#textAreaConteudo').val());
					
					//-------------------
					//Inserir
					x.executeSql('INSERT INTO pendencias_mensagens (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')', tmp_3,function(){
						$('#carregandoAguarde').hide();
						navigator.app.loadUrl('file:///android_asset/www/pendencias_espelho.html#'+id_pendencia);
					},function(e, t){
						console.log(JSON.stringify(e));
						console.log(JSON.stringify(t));
					});
					//Inserir
					//-------------------
					
				});
			});
					
		}		
		return false;
	});
	
	//******************************************
	//Excluir Pendência
	//******************************************
	$('#btnPendenciaExcluir').click(function() {
		var id = $(this).attr('data-id');
		confirmar('Você deseja excluir essa pendência?', function(){
			db.transaction(function(x) {
				x.executeSql('UPDATE pendencias SET remover = \'1\', exportado = NULL WHERE id = ?', [id], function(x, dados) {
					window.location = 'pendencias_listar.html';
				});
			});
		});
	});
	
	//******************************************
	//Editar Pendência
	//******************************************
	$('#btnPendenciaEditar').click(function(e) {
		e.preventDefault();
		var id = $(this).attr('data-id');
		confirmar('Deseja editar esta pendência?', 
			function () {
				$(this).dialog('close');
				
				
				//copiar_pedido(codigo_do_pedido, codigo_da_empresa, true, tabela);
				
				
				$("#confirmar_dialog").remove();
				window.location = 'pendencias_editar.html#'+id;
			}
		);
		
		
	});
		
		
	
	
	//******************************************
	//Encerrar Pendência
	//******************************************
	$('#btnPendenciaSolicitarEncerramento').click(function() {
		var id = $(this).attr('data-id');
		confirmar('Você deseja encerrar essa pendência?', function(){
			db.transaction(function(x) {
				x.executeSql('UPDATE pendencias SET status = \'aguardando_encerramento\', editado = \'1\', exportado = NULL WHERE id = ?', [id], function(x, dados) {
					window.location = 'pendencias_listar.html';
				});
			});
		});
	});
});