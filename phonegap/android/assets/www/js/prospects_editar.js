var prospect;
$(document).ready(function() {
	var validadorIE = new ValidaDocumentos();
	//resultado = (validadorIE.validaIE('01.15.241/001-22', 'AC'));	
	
	db.transaction(function(x) {
		// obter feiras
		x.executeSql(
			'SELECT DISTINCT id, nome FROM eventos ORDER BY nome ASC', [], function(x, dados) {
				
				$('select[name=id_feira]').append('<option value="0"> Nenhum </option>');
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=id_feira]').append('<option value="' + dado.id + '">' + dado.nome + '</option>');
					}
				}
			}
		);
		
//		CUSTOM: 177 / 000874 - 16/10/2013 - Localizar: CUST-RAMOS
		//obter_ramos
		x.executeSql(
				'SELECT DISTINCT chave, descricao FROM ramos ORDER BY descricao ASC', [], function(x, dados) {
					
					$('select[name=id_ramo]').append('<option value=""> Nenhum </option>');
					if (dados.rows.length)
					{
						for (i = 0; i < dados.rows.length; i++)
						{
							var dado = dados.rows.item(i);
							
							$('select[name=id_ramo]').append('<option value="' + dado.chave + '">' + dado.descricao + '</option>');
						}
					}
				}
			);
//		FIM CUSTOM: 177 / 000874 - 16/10/2013

//		CUSTOM: 177 / 000874 - 22/10/2013 - Localizar: CUST-CONDICOES-PAGAMENTO
		//obter_CONDICOES_PAGAMENTO
		x.executeSql('SELECT codigo, descricao FROM condicoes_pagamento ORDER BY descricao ASC', [], function(x, dados) {
			$('select[name=condicoes_pagamento]').append('<option value="0"> Nenhum </option>');
			if (dados.rows.length)
			{
				for (i = 0; i < dados.rows.length; i++)
				{
					var dado = dados.rows.item(i);
					
					$('select[name=condicoes_pagamento]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
				}
			}
		});
//		FIM CUSTOM: 177 / 000874 - 22/10/2013
		
		// obter prospect
		var codigo = parse_url(location.href).fragment;
		
		x.executeSql(
			'SELECT * FROM prospects WHERE codigo = ?', [codigo], function(x, dados) {


				if (dados.rows.length)
				{
				
					prospect = dados.rows.item(0);
					
				
					
					
					$('input[name=cgc]').attr('readonly', 'readonly');
					
					if(prospect.acao == 'A'){
						$('#cancelar').attr('href', 'prospects_processados_visualizar.html#' + prospect.codigo);
					}else{							
						$('#cancelar').attr('href', 'prospects_visualizar.html#' + prospect.codigo);
					}
					
					var municipio = '';
					
					
							$.each(prospect, function(i, v) {
					
						if ($('[name=' + i + ']').length)
						{
							if(i == 'estado')
							{
								estado = v;
								
								//correcao  
								if(prospect.codigo_municipio)
								{
									municipio = prospect.codigo_municipio;

									$('#trocar_municipio').show();
								}								
								//fim correcao
								
								obter_municipios(estado);
							}
							
							if(i == 'data_nascimento')
							{
								if(v)
								{
									v = protheus_data2data_normal(v);
								}
							}
							
							if(i == 'telefone')
							{
								if(v)
								{
									v = remover_caracteres_telefone(v);
								}

							}
								
							
							if(i == 'fax')
							{
								if(v)
								{
									v = remover_caracteres_telefone(v);
								}
							}
							
							if(i == 'cgc'){
								if(v){
									if(validar_cnpj(v)){
										$('[name=inscricao_estadual]').addClass('obrigatorio');
										
									}else{
										$('[name=inscricao_estadual]').removeClass('obrigatorio');
									}									
								}								
							}
						
							$('[name=' + i + ']').val(v);
						}
					});
				}
				else
				{
					window.location = 'index.html';
				}
			}
		);
	});
	
	// copiar endereço
	
	var inputs = ['cep', 'endereco', 'numero', 'complemento', 'bairro', 'cidade', 'estado'];
	
	$('#copiar_end_cob').click(function() {
		var _this = this;
		
		$.each(inputs, function(i, v) {
			if ($(_this).is(':checked'))
			{
				$('[name=' + v + ']').val($('[name=' + v + '_cobranca]').val());
			}
			else
			{
				$('[name=' + v + ']').val('');
			}
		});
	});
	
//	CUSTOM: 177 / 000874 - 22/10/2013 - Localizar: CUST-BANCO
	// Obter bancos
 	$('select[name=banco_cobranca]').html('<option value> Selecione... </option>' + bancos_cobranca);	
 	$("select[name=banco_cobranca] option[value='85']").remove();
 	//002388 - Remover Banco Prospect
 	$("select[name=banco_cobranca] option[value='84']").remove();
 	//Fim 002388 - Remover Banco Prospect
 	//	FIM CUSTOM: 177 / 000874 - 22/10/2013
	
	

 	
 	
	// Obter Estados
	$('select[name*=estado]').html('<option value> Selecione... </option>' + ufs);
	
	
	// Obter Municipios
	
	$('select[name=estado]').live('change', function(){
		$('#nome_municipio').attr('disabled', 'disabled');
		$('#nome_municipio').html('Carregando...');

		limpar_municipio();
		
		var estado = $(this).val();
		
		obter_municipios(estado);
	});
	
	function obter_municipios(estado)
	{
		// obter Municipios
		db.transaction(function(x) {
			x.executeSql('SELECT DISTINCT codigo, nome FROM municipios WHERE uf = ? ORDER BY nome ASC', [estado], function(x, dados) {
				var municipios = [];
				
				for (i = 0; i < dados.rows.length; i++)
				{
					var dado = dados.rows.item(i);
					
					municipios.push({ label: dado.nome, codigo: dado.codigo });
				}
				
				buscar_municipios(municipios)
			});
		});
	}
	
	$('#trocar_municipio').click(function(){
		limpar_municipio();
	});
	
	function limpar_municipio()
	{
		$('[name=codigo_municipio]').val('');
		$('#nome_municipio').val('');
		$('#nome_municipio').removeAttr('disabled');
		$('#trocar_municipio').hide();
		
		if($('[name=estado]').val() > 0){
			$('#nome_municipio').focus();
		}
	}
	
	function buscar_municipios(municipios)
	{
		$('input[name=nome_municipio]').autocomplete({
			minLength: 2,
			source: municipios,
			position : { my : "left bottom", at: "left top", collision : "none"},
			select: function( event, ui ) {
				$('[name=codigo_municipio]').val(ui.item.codigo);
				$('#nome_municipio').val(ui.item.label);
				
				$('#nome_municipio').attr('disabled', 'disabled');
				$('#trocar_municipio').show();
				return false;
			}
		});
	}
	
	
	$('select[name=tipo]').live('change', function(){
		
		if($(this).val() == 'L')
		{
			$('input[name=inscricao_rural]').addClass('obrigatorio');
		}
		else
		{
			$('input[name=inscricao_rural]').removeClass('obrigatorio');
		}
	});
	
	//Cancelar
	$('#cancelar').click(function() {
		
		var cancelar_url = $(this).attr('href');
		
		if(prospect.acao == 'A'){
			//CUSTOM 20140610 - Reanálise dos prospects rejeitados
			alterar_status_para_rejeitado(prospect,function(){	
				
				document.location = cancelar_url ;				
			});
		}else{		
			document.location = cancelar_url;
		}
		
	});
	
	// enviar form
	
	
	$('form').submit(function() {
		var validadorIE = new ValidaDocumentos();
	
//		CUSTOM: 177 / 000874 - 15/10/2013 - Localizar: CUST-FILIAL-PROSPECT
//		if (!$('select[name=filial]').val())
//		{
//			mensagem('Selecione uma <strong>Filial</strong>.');
//		}
//		else
//		FIM CUSTOM: 177 / 000874 - 15/10/2013

		prospect_cadastrado_editar($('input[name=cgc]').val(), prospect, function(cgc_valido, status_mensagem){
		
			if (!$('input[name=tipo]').val())
			{
				mensagem('Selecione um <strong>Tipo</strong> de Prospect.',"$('input[name=tipo]').focus()");
			}
			// CUSTOM: 177 / 000874 - 22/10/2013 - Localizar: CUST-RAMOS
	
			else if (!$('select[name=id_ramo]').val())
			{
				mensagem('Preencha o campo <strong>Ramo</strong>.',"$('select[name=id_ramo]').focus()");
			}
			// FIM CUSTOM: 177 / 000874 - 22/10/2013
			else if (!$('input[name=nome]').val())
			{
				mensagem('Preencha o campo <strong>Nome / Razão social</strong>.',"$('input[name=nome]').focus()" );
			}
			else if (!$('input[name=nome_fantasia]').val())
			{
				mensagem('Preencha o campo <strong>Nome Fantasia</strong>.',"$('input[name=nome_fantasia]').focus()");
			}
			else if (!$('input[name=cgc]').val() || !validar_cpf_cnpj($('input[name=cgc]').val()))
			{
				mensagem('Preencha um <strong>CPF / CNPJ</strong> válido.',"$('input[name=cgc]').focus()");
			}
			else if (!cgc_valido)
			{
				mensagem(status_mensagem,"$('input[name=cgc]').focus()");
			}			
			else if ($('select[name=tipo]').val() == 'L' && !$('input[name=inscricao_rural]').val())
			{
				mensagem('Preencha o campo <strong>Inscrição Rural</strong>.',"$('select[name=tipo]').focus()");
			}
			//Chamados: 002230 - Dt Entrega, 002162 - Data Entrega
			else if ($('input[name=data_nascimento]').val().length > 0    && ( !isValidDate($('input[name=data_nascimento]').val()) || $('input[name=data_nascimento]').val().length != 10  ) )
			{
				mensagem('<strong>Data de fundação</strong> inválida.',"$('input[name=data_nascimento]').focus()");
			}	
			// 002389 - Prospect DDD
			else if (!$('input[name=ddd]').val())
			{
				mensagem('Preencha o campo <strong>DDD</strong>.',"$('input[name=ddd]').focus()");
			}	
			else if (!validar_ddd($('input[name=ddd]').val()))
			{
				mensagem('Preencha um <strong>DDD</strong> válido.',"$('input[name=ddd]').focus()");
			}
			// FIM  002389 - Prospect DDD
			else if (!$('input[name=telefone]').val())
			{
				mensagem('Preencha o campo <strong>Telefone</strong>.',"$('input[name=telefone]').focus()");
			}
			else if (!validar_telefone($('input[name=telefone]').val()))
			{
				mensagem('Preencha um <strong>Telefone</strong> válido.',"$('input[name=telefone]').focus()");
			}
			else if ($('input[name=fax]').val() && !validar_telefone($('input[name=fax]').val()))
			{
				mensagem('Preencha um <strong>Fax</strong> válido.',"$('input[name=fax]').focus()");
			}
			else if (!validar_email($('input[name=email]').val()))
			{
				mensagem('Preencha um <strong>E-mail</strong> válido.',"$('input[name=email]').focus()");
				
			}
			else if (!validar_email($('input[name=email_fiscal]').val()))
			{
				mensagem('Preencha um <strong>E-mail NF-e</strong> válido.',"$('input[name=email_fiscal]').focus()");
			}
			else if (!validar_cep($('input[name=cep]').val()))
			{
				mensagem('Digite um CEP válido.',"$('input[name=cep]').focus()");
			}
			else if (!$('input[name=endereco]').val())
			{
				mensagem('Preencha um <strong>Endereço</strong> válido.',"$('input[name=endereco]').focus()");
			}
			else if (!$('input[name=bairro]').val())
			{
				mensagem('Preencha um <strong>Bairro</strong> válido.',"$('input[name=bairro]').focus()");
			}
			else if (!$('select[name=estado]').val())
			{
				mensagem('Preencha um <strong>Estado</strong> válido.',"$('select[name=estado]').focus()");
			}
			//Custom  - Validação da Inscrição Estadual
			else if (validar_cnpj($('input[name=cgc]').val()) && !$('input[name=inscricao_estadual]').val() ){
				mensagem('Preencha uma <strong>Inscrição Estadual</strong> válida.',"$('input[name=inscricao_estadual]').focus()");
			}
			else if (validar_cnpj($('input[name=cgc]').val()) &&  !(validadorIE.validaIE($('input[name=inscricao_estadual]').val(),$('select[name=estado]').val() ))   ){
				mensagem('Digite uma <strong>Inscrição Estadual</strong> válida.',"$('input[name=inscricao_estadual]').focus()");
			}
			//Fim Custom - Validação Inscrição Estadual
			else if (!$('input[name=codigo_municipio]').val())
			{
				mensagem('Preencha um <strong>Município</strong> válido.',"$('select[name=codigo_municipio]').focus()");
			}
			// CUSTOM 177 - 000874 - 21/10/2013 - Localizar: CUST-INFORMACOES-FINANCEIRAS-PROSPECT
			//CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if ($('input[name=faturamento]').val().length > 0 && !validar_campo_numerico($('input[name=faturamento]').val()))			
			{
				mensagem('Preencha o <strong>Faturamento </strong> válido.',"$('input[name=faturamento').focus()");
			}
			//FIM CHAMADO 001953 		
			else if (!$('input[name=faturamento_anual]').val())
			{
				mensagem('Preencha o <strong>Faturamento Anual</strong> válido.',"$('input[name=faturamento_anual]').focus()");
			}
			//CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!validar_campo_numerico($('input[name=faturamento_anual]').val()))
			{
				mensagem('Preencha o <strong>Faturamento Anual</strong> válido.',"$('input[name=faturamento_anual]').focus()");
			}
			else if($('input[name=faturamento_anual]').val() <= 0 ){
				mensagem('<strong>Faturamento Anual</strong> deve ser maior que 0.',"$('input[name=faturamento_anual]').focus()");
			}
			//FIM CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!$('input[name=ano_fat_atual]').val() || ($('input[name=ano_fat_atual]').val().length < 4 || $('input[name=ano_fat_atual]').val() < date('Y', strtotime('-20 years')) || $('input[name=ano_fat_atual]').val() > date('Y')))
			{
				mensagem('Preencha o <strong>Ano Faturamento Atual</strong> válido.',"$('input[name=ano_fat_atual]').focus()");
			}
			else if($('input[name=ano_fat_atual]').val() <= 0 ){
				mensagem('<strong>Ano Faturamento Atual</strong> deve ser maior que 0.',"$('input[name=ano_fat_atual]').focus()");
			}
			else if (!$('input[name=sugestao_lim_credito]').val())
			{
				mensagem('Preencha a <strong>Sugestão de Limite de Crédito</strong> válido.', "$('input[name=sugestao_lim_credito]').focus()");
			}
			//CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!validar_campo_numerico($('input[name=sugestao_lim_credito]').val()))
			{
				mensagem('Preencha a <strong>Sugestão de Limite de Crédito</strong> válido.', "$('input[name=sugestao_lim_credito]').focus()");
			}
			else if($('input[name=sugestao_lim_credito]').val() <= 0 ){
				mensagem('<strong>Sugestão de Limite de Crédito</strong> deve ser maior que 0.',"$('input[name=sugestao_lim_credito]').focus()");
			}	
			//FIM CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!$('select[name=banco_cobranca]').val())
			{
				mensagem('Preencha o <strong>Banco Cobrança</strong> válido.', "$('select[name=banco_cobranca]').focus()");
			}
	//		 FIM CUSTOM 177 - 000874 - 21/10/2013
	//		 CUSTOM 177 - 000874 - 21/10/2013 - Localizar: CUST-OUTRAS-INFORMACOES-PROSPECT
			else if (!$('input[name=area_loja]').val())
			{
				mensagem('Preencha a <strong>Área da Loja (m²)</strong> válido.',"$('input[name=area_loja]').focus()");
			}
			//CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!validar_campo_numerico($('input[name=area_loja]').val()))
			{
					mensagem('Preencha a <strong>Área da Loja (m²)</strong> válido.',"$('input[name=area_loja]').focus()");
			}
			//FIM CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if($('input[name=area_loja]').val() <= 0 ){
				mensagem('<strong>Área da Loja (m²)</strong> deve ser maior que 0.',"$('input[name=area_loja]').focus()");
			}		
			else if (!$('input[name=numero_lojas]').val()  || !validar_campo_numerico($('input[name=numero_lojas]').val()))
			{
				mensagem('Preencha a <strong>Número de Lojas</strong> válido.',"$('input[name=numero_lojas]').focus()");
			}		
			else if($('input[name=numero_lojas]').val() <= 0 ){
				mensagem('<strong>Número de Lojas</strong> deve ser maior que 0.',"$('input[name=numero_lojas]').focus()");
			}		
			else if (!$('input[name=numero_funcionarios]').val() || !validar_campo_numerico($('input[name=numero_funcionarios]').val()) )
			{
				mensagem('Preencha a <strong>Número de Funcionarios</strong> válido.',"$('input[name=numero_funcionarios]').focus()");
			}
			else if($('input[name=numero_funcionarios]').val() <= 0 ){
				mensagem('<strong>Número de Funcionarios</strong> deve ser maior que 0.',"$('input[name=numero_funcionarios]').focus()");
			}
			else if (!$('input[name=numero_veiculos]').val()  || !validar_campo_numerico($('input[name=numero_veiculos]').val()))
			{
				mensagem('Preencha a <strong>Número de Veículos</strong> válido.', "$('input[name=numero_veiculos]').focus()");
			}
			else if($('input[name=numero_veiculos]').val() <= 0 ){
				mensagem('<strong>Número de Veículos</strong> deve ser maior que 0.',"$('input[name=numero_veiculos]').focus()");
			}
			else if (!$('select[name=imovel_proprio]').val())
			{
				mensagem('Preencha o <strong>Imóvel Próprio</strong> válido.', "$('select[name=imovel_proprio]').focus()");
			}
			else if (!$('input[name=socio_1]').val())
			{
				mensagem('Preencha o <strong>Sócio 1</strong> válido.', "$('input[name=socio_1]').focus()");
			}
			else if (!$('input[name=cpf_socio_1]').val() || !validar_cpf_cnpj($('input[name=cpf_socio_1]').val()))
			{
				mensagem('Preencha o <strong>CPF Sócio 1</strong> válido.', "$('input[name=cpf_socio_1]').focus()");
			}
			else if($('input[name=cpf_socio_2]').val() && !validar_cpf_cnpj($('input[name=cpf_socio_2]').val()))
			{
				mensagem('Preencha o <strong>CPF Sócio 2</strong> válido.', "$('input[name=cpf_socio_2]').focus()");
			}
			else if (!$('input[name=responsavel_compra]').val())
			{
				mensagem('Preencha o <strong>Reponsável pela Compra</strong> válido.', "$('input[name=responsavel_compra]').focus()");
			}
			else if (!$('input[name=resp_contas_pagar]').val())
			{
				mensagem('Preencha o <strong>Reponsável pelo Contas à Pagar</strong> válido.', "$('input[name=resp_contas_pagar]').focus()");
			}
			else if (!$('select[name=condicoes_pagamento]').val())
			{
				mensagem('Preencha uma <strong>Condição de Pagamento</strong> válida.',"$('select[name=condicoes_pagamento]').focus()");
			}
			else if (!$('textarea[name=observacao]').val())
			{
				mensagem('Preencha o <strong>Observação</strong>.', "$('textarea[name=observacao]').focus()");
			}
			
	//		 FIM CUSTOM 177 - 000874 - 21/10/2013
			else
			{
				// -----------
				// Isentos
				if (!$('input[name=inscricao_estadual]').val()){
					$('input[name=inscricao_estadual]').val('ISENTO');
				}
				
				if (!$('input[name=inscricao_municipal]').val()){
					$('input[name=inscricao_municipal]').val('ISENTO');
				}
				
				if (!$('input[name=inscricao_rural]').val()){
					$('input[name=inscricao_rural]').val('ISENTO');
				}
				// Isentos
				// -----------
				
	
				
				
				obter_grupo_permissao(function(grupo_permissao){
						
						
					
				
				
				
					db.transaction(function(x) {
						x.executeSql(
							"SELECT codigo FROM prospects WHERE cgc = ? AND codigo != ? " , [$('input[name=cgc]').val(), prospect.codigo], function(x, dados) {
								if ($('input[name=cgc]').val() && dados.rows.length)
								{
									mensagem('O <strong>CPF / CNPJ</strong> digitado já existe.');
								}
								else
								{
									x.executeSql(
										'SELECT codigo FROM prospects ORDER BY codigo DESC LIMIT 1', [], function(x, dados) {
									
											var tmp = new Array();
									
											
											if(grupo_permissao.grupo == 'representante'){
												if(prospect.status == 'P'){
													tmp.push('status = "L"');
													console.log('novo status');
												}else{
													if(!!localStorage.getItem('codigo_preposto') &&  localStorage.getItem('codigo_preposto') != 'null'  ){
														
														tmp.push(' status = "P" ');
													
													}else{
														tmp.push(' status = "L" ');
													}
													
													
												}
											}else{
												if(!!localStorage.getItem('codigo_preposto') &&  localStorage.getItem('codigo_preposto') != 'null'  ){
													
													tmp.push(' status = "P" ');
												
												}else{
													tmp.push(' status = "L" ');
												}
												
											}
											
											
											
											tmp.push("editado = '1'");
											tmp.push("exportado = NULL");
											
											$('input[name], textarea[name], select[name]').each(function() {
												
												if(($(this).attr('name') != 'empresas') && ($(this).attr('name') != 'data_nascimento'))
												{
													tmp.push($(this).attr('name') + ' = "' + $(this).val() + '"');
												}
												
												if($(this).attr('name') == 'data_nascimento')
												{
													if($(this).val())
													{
														tmp.push($(this).attr('name') + ' = "' + data_normal2protheus($(this).val()) + '"');
													}
												}
												
		//										CUSTOM: 177/ 000874 - 15/10/2013 : Localizar: CUST-FILIAL-PROSPECT 				
												tmp.push('filial  = ""');	
		//										FIM CUSTOM: 177 / 000874 - 15/10/2013
												
												
												
												
											});
											
											
											
											
											
											tmp.push('codigo_empresa = "' + info.empresa + '"');
											
											console.log(JSON.stringify(tmp));
											
											x.executeSql('UPDATE prospects SET ' + tmp.join(', ') + ' WHERE codigo = ?', [prospect.codigo], function(x, dados){
											
												window.location = 'prospects_visualizar.html#' + prospect.codigo+'|true';
											
											});
											
									
											
										}
									);
								}
							}
						);
					});
				});
			}
		});
		return false;
	});
	
	/*
	$('form').submit(function() {
		if (!$('input[name=nome]').val())
		{
			mensagem('Digite uma Razão social.');
		}
		else if ($('input[name=cgc]').val() && !validar_cnpj($('input[name=cgc]').val()))
		{
			mensagem('Digite um CNPJ válido.');
		}
		else if ($('input[name=email]').val() && !validar_email($('input[name=email]').val()))
		{
			mensagem('Digite um E-mail válido.');
		}
		else if ($('input[name=cep]').val() && !validar_cep($('input[name=cep]').val()))
		{
			mensagem('Digite um CEP válido.');
		}
		else if ($('input[name=cep_cobranca]').val() && !validar_cep($('input[name=cep_cobranca]').val()))
		{
			mensagem('Digite um CEP de cobrança válido.');
		}
		else if ($('input[name=telefone]').val() && !validar_telefone($('input[name=telefone]').val()))
		{
			mensagem('Digite um telefone válido.');
		}
		else
		{
			db.transaction(function(x) {
				x.executeSql(
					'SELECT id FROM prospects WHERE cgc = ? AND cgc != ?', [$('input[name=cgc]').val(), prospect.cgc], function(x, dados) {
						if ($('input[name=cgc]').val() && dados.rows.length)
						{
							mensagem('O CNPJ digitado já existe.');
						}
						else
						{
							var tmp = prospect.adicionado_offline ? new Array() : new Array('editado_offline = 1');
							
							$('input[name], textarea[name], select[name]').each(function() {
								tmp.push($(this).attr('name') + ' = "' + $(this).val() + '"');
							});
							
							x.executeSql('UPDATE prospects SET ' + tmp.join(', ') + ' WHERE id = ?', [prospect.id]);
							
							window.location = 'prospects_visualizar.html#' + prospect.id;
						}
					}
				);
			});
		}
		
		return false;
	});
	*/
	
	$('input[type=text], textarea').keyup(function(){
		$(this).val($(this).val().toUpperCase());
	});
});


function alterar_status_para_rejeitado (prospect, callback){
	var status_rejeitado = parseInt('5');
	
	db.transaction(function(x) {
		var query = 'UPDATE prospects_processados SET status = '+status_rejeitado+' WHERE codigo = ? AND codigo_loja = ? '; 
		x.executeSql(query, [prospect.codigo, prospect.codigo_loja], function (tx, result) {
            //alert("Query Success");
			
			excluir_socilitacao_analise(prospect, function(){
				callback();            
			});
            
        },function (tx, error) {
           alert("Desculpe, ocorreu um erro  por favor entre em contato.");
           alert(error.message);
           
        });
		
			
	});

}


function excluir_socilitacao_analise(prospect, callback){
	
	db.transaction(function(x) {
		var query = "DELETE FROM prospects  WHERE acao='A' AND codigo = ? AND codigo_loja = ? "; 
				
		x.executeSql(query, [prospect.codigo, prospect.codigo_loja], function (tx, result) {
            //alert("Query Success");
			
			
				callback();            
			
            
        },function (tx, error) {
           alert("Desculpe, ocorreu um erro  por favor entre em contato.");
           alert(error.message);
           
        });
		
			
	});
}



function prospect_cadastrado_editar(cgc, prospect,  callback){	
	if(validar_cpf_cnpj(cgc)) {
		db.transaction(function(x) {			
			x.executeSql('SELECT cgc, nome_fantasia FROM prospects WHERE cgc = ? AND codigo != ? ', [cgc, prospect.codigo], function(x, dados) {
				if (dados.rows.length) {
					var dado = dados.rows.item(0);
					
					
				
					callback(false, 'O CPF/CNPJ informado, já possui um cadastro relacionado ao Prospect: <br/><b>' + dado.nome_fantasia + '</b>');
				} else {
					x.executeSql('SELECT cpf, nome_fantasia FROM clientes WHERE cpf = ?', [cgc], function(x, dados) {
						if (dados.rows.length) {
							var dado = dados.rows.item(0);
							
							
							callback(false,'O CPF/CNPJ informado, já possui um cadastro relacionado ao Cliente: <br/><b>' + dado.nome_fantasia + '</b>');
						} else {							
							callback(true,'Sucesso.');							
						}
					});
				}
			});
		});
	}
	
}