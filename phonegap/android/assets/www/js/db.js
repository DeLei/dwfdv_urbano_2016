var db = openDatabase('dwfdv', '1.0', '', 1024 * 1024 * 98);

//Tabelas do DW Força de Vendas - Criadas Manualmente porque não vem do Web Service
db.transaction(function(x) {
	x.executeSql('CREATE TABLE IF NOT EXISTS sincronizacoes (dt_atualizado INTEGER,   timestamp INTEGER, modulo TEXT, sucesso INTEGER, nunca_sincronizado INTEGER)');
	x.executeSql('CREATE TABLE IF NOT EXISTS localizacoes (timestamp INTEGER, latitude TEXT, longitude TEXT)');
	//x.executeSql('CREATE TABLE IF NOT EXISTS lembretes (timestamp INTEGER, modulo TEXT, titulo TEXT, texto TEXT, lembrar_hora INTEGER)');
});

//Tabelas do WEB SERVICE - Criadas Automaticamente
function criar_tabela(tabela, dados, multiplos_objetos)
{
	
	if (db)
	{
		var campos = obter_campos_tabela(dados, multiplos_objetos);
		
		db.transaction(function(x) {
			x.executeSql('CREATE TABLE IF NOT EXISTS ' + tabela + ' (' + campos + ');');
		});
		
	}
}