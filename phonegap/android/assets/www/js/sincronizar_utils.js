//------------------------------------------------------------------------//
//------------------------------ MÉTODOS ---------------------------------//
//------------------------------------------------------------------------//
//	Autor:		William Reis Fernandes									--//
//	Data:		27/09/2013												--//
//	Versão:		1.0														--//
//																		--//
//	Descrição: SINCRONIZAR OS REGISTROS QUE O REPRESENTANTE DESEJA		--//
// 																		--//
//------------------------------------------------------------------------//
$(document).ready(function() {
	
	//====================================================================
	// INICIAR - FUNÇÕES PARA POPULAR LISTAGEM
	//====================================================================

	//************************************
	//Obter os prospects para sincronizar,
	//************************************
	obter_prospects();
	obter_prospects_historicos();
	//************************************
	//Obter os prospects para sincronizar,
	//************************************
	
	//************************************
	//Obter os históricos de clientes
	//************************************
	obter_clientes_historicos();
	//************************************
	//Obter os históricos de clientes
	//************************************
	
	
	//************************************
	//Obter pendências
	//************************************
	obter_pendencias();
	obter_pendencias_mensagens();
	//************************************
	//Obter pendências
	//************************************
	
	//************************************
	//Obter Agenda
	//************************************
	obter_agenda();	
	//************************************
	//Obter Agenda
	//************************************
	
	
	obter_pedidos_aguardando();
	
	//************************************
	//Obter os orçamentos
	//************************************
	
	obter_orcamentos_aguardando();
	//************************************
	//Obter os orçamentos
	//************************************
	
	//====================================================================
	// FINAL - FUNÇÕES PARA POPULAR LISTAGEM
	//====================================================================
	
	//====================================================================
	// INICIAR - FUNÇÕES PARA SELECIONAR OS REGISTROS E MODULOS
	//====================================================================
	//--------------------------------------------------------------------
	//BOTÃO SELECIONAR TODOS
	//--------------------------------------------------------------------
	$("#btnSelecionarTodos").live('click', function(e){	
		e.preventDefault();
		
		var checkbox = $('#selecionar_todos').is(':checked');	
		if(checkbox == false)
		{
			checkbox = true;
		}
		else
		{
			checkbox = false;
		}
		
		$('#selecionar_todos').attr({checked: checkbox});
		
		if(checkbox){
			$("input[name='modulos[]']").attr({checked: checkbox});
			$("input[type='checkbox']").button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$("input[name='modulos[]']").attr({checked: checkbox});
			$("input[type='checkbox']").button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		
		rotina_selecao_orcamento('orcamentos', checkbox)

	});
	

	//--------------------------------------------------------------------
	//BOTÃO SELECIONAR TODOS
	//--------------------------------------------------------------------	
	
	//--------------------------------------------------------------------
	// Setar checks para Jquery UI 
	//--------------------------------------------------------------------
	$(  "input[name='prospects[]'], " +
		"input[name='historicos-prospects[]'], " +
		"input[name='pendencias[]'], " +
		"input[name='clientes[]']" 
		).button({ 
				icons: {primary:'ui-icon-circle-close'} 
	});
	//--------------------------------------------------------------------
	// Setar checks para Jquery UI 
	//--------------------------------------------------------------------
	
	//--------------------------------------------------------------------
	//
	//--------------------------------------------------------------------
	$("input[name='prospects[]'], input[name='historicos-prospects[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		
		
		$("#modulos-1").attr({checked: 'true'});
		$("#prospects").attr({checked: 'true'});
	});
	
	//--------------------------------------------------------------------
	//
	//--------------------------------------------------------------------
	$("input[name='clientes[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		$("#modulos-2").attr({checked: 'true'});
	});
	//--------------------------------------------------------------------
	
	
	
	//--------------------------------------------------------------------
	// pedidos pendentes
	//--------------------------------------------------------------------
	$("input[name='pedidos_pendentes[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		$("#pedidos_pendentes").attr({checked: 'true'});
	});
	
	//--------------------------------------------------------------------
	//
	//--------------------------------------------------------------------
	$("input[name='pendencias[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		$("#pendencias").attr({checked: 'true'});
	});
	
	//--------------------------------------------------------------------
	// pedidos orçamentos
	//--------------------------------------------------------------------
	$("input[name='orcamentos[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		$("#orcamentos").attr({checked: 'true'});
	});
	
	//--------------------------------------------------------------------
	//Se o usuário clicar no modulo selecionar todos os registros da lista
	//--------------------------------------------------------------------
	$("input[name='modulos[]']").live('click', function(){	
		
		// Capturar modulo que o usuário clicou
		var modulo = $(this).data('modulo');
		
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');	

		rotina_selecao_orcamento(modulo, checkbox);
			
		//Marca todos os campos
		$("input[name='"+modulo+"[]"+"']").attr({checked: checkbox});
		$("input[name='historicos-"+modulo+"[]"+"']").attr({checked: checkbox});
		//Icones
		if(checkbox){
			$("input[name='"+modulo+"[]"+"']").button("option", "icons", {primary: "ui-icon-circle-check"});
			$("input[name='historicos-"+modulo+"[]"+"']").button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$("input[name='"+modulo+"[]"+"']").button("option", "icons", {primary: "ui-icon-circle-close"});
			$("input[name='historicos-"+modulo+"[]"+"']").button("option", "icons", {primary: "ui-icon-circle-close"});
		}		
	});
	//--------------------------------------------------------------------
	//Se o usuário clicar no modulo selecionar todos os registros da lista
	//--------------------------------------------------------------------	
	
	
	//--------------------------------------------------------------------
	//Se selecionar orcamentos, automaticamento selecionar os prospects e bloquealos
	//--------------------------------------------------------------------
	function rotina_selecao_orcamento(modulo, checkbox)
	{
		if(modulo == 'orcamentos')
		{
			if(checkbox){
				$("input[data-modulo='prospects']").attr({checked: checkbox});
				$("input[name='prospects[]']").button("option", "icons", {primary: "ui-icon-circle-check"});
				$("input[name='historicos-prospects[]"+"']").button("option", "icons", {primary: "ui-icon-circle-check"});
				
				$("input[data-modulo='prospects']").attr('disabled', 'disabled');
				$("input[name='prospects[]']").button( "option", "disabled", true );
				$("input[name='historicos-prospects[]"+"']").button( "option", "disabled", true );
			}else{
				$("input[data-modulo='prospects']").attr({checked: checkbox});
				$("input[name='prospects[]']").button("option", "icons", {primary: "ui-icon-circle-close"});
				$("input[name='historicos-prospects[]"+"']").button("option", "icons", {primary: "ui-icon-circle-close"});
				
				$("input[data-modulo='prospects']").removeAttr('disabled');
				$("input[name='prospects[]']").button( "option", "disabled", false );
				$("input[name='historicos-prospects[]"+"']").button( "option", "disabled", false );
				
			}
		}
	}
	//--------------------------------------------------------------------
	//Se selecionar orcamentos, automaticamento selecionar os prospects e bloquealos
	//--------------------------------------------------------------------
	
	
	
	//====================================================================
	// FINAL - FUNÇÕES PARA SELECIONAR OS REGISTROS E MODULOS
	//====================================================================
});


/**
* Metódo:		obter_prospects()
* 
* Descrição:	Função Utilizada para obter os proscpects ainda 
* 				não sincronizados pelo representante
* 
* Data:			28/09/2013
* Modificação:	28/09/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web / William Reis Fernandes
* 
*/
function obter_prospects()
{
	
	
	
	db.transaction(function(x) {
		
		// Exibir Prospects
		x.executeSql('SELECT * FROM prospects WHERE codigo > 0 AND (exportado IS NULL OR editado = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-prospects').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					var prospect = '<input type="checkbox" value="'+item.codigo+'" name="prospects[]" id="prospects-'+i+'" /><label for="prospects-'+i+'" ' + style_erro + '>'+item.nome+' - '+(item.nome_fantasia ? item.nome_fantasia : 'N\A')+' - '+(item.nome_municipio ? item.nome_municipio : 'N\A')+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-prospects').append('<li>'+prospect+'</li>');
				}
			}else{
				$('#lista-prospects').append('<li>Nenhum prospect para sincronizar.</li>');
			}
			
			$("input[name='prospects[]']").button({ icons: {primary:'ui-icon-circle-close'} });
			
		});
	});
}

function obter_prospects_historicos()
{
	db.transaction(function(x) {
		
		// Exibir Prospects Historicos
		x.executeSql('SELECT * FROM historico_prospects WHERE id > 0 AND (exportado IS NULL OR editado = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-historicos-prospects').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);										
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					var prospect = '<input type="checkbox" value="'+item.id+'" data-codigo-prospects="'+item.id_prospect+'" name="historicos-prospects[]" id="historicos-prospects-'+i+'" /><label for="historicos-prospects-'+i+'" ' + style_erro + '>'+item.pessoa_contato+' - '+(item.cargo ? item.cargo : 'N\A')+' - '+(item.email ? item.email : 'N\A')+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-historicos-prospects').append('<li>'+prospect+'</li>');
				}
			}else{
				$('#lista-historicos-prospects').append('<li>Nenhum histórico de prospects para sincronizar.</li>');
			}
			$("input[name='historicos-prospects[]']").button({ icons: {primary:'ui-icon-circle-close'} });
		});
	});
}

function obter_clientes_historicos()
{
	db.transaction(function(x) {
		
		// Exibir Prospects Historicos
		x.executeSql('SELECT * FROM historico_clientes WHERE id > 0 AND (exportado IS NULL OR editado = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-historicos-clientes').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);										
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					var prospect = '<input type="checkbox" value="'+item.id+'" name="historicos-clientes[]" id="clientes-prospects-'+i+'" /><label for="clientes-prospects-'+i+'" ' + style_erro + '>'+item.pessoa_contato+' - '+(item.cargo ? item.cargo : 'N\A')+' - '+(item.email ? item.email : 'N\A')+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-historicos-clientes').append('<li>'+prospect+'</li>');
				}
			}else{
				$('#lista-historicos-clientes').append('<li>Nenhum histórico de clientes para sincronizar.</li>');
			}
			$("input[name='historicos-clientes[]']").button({ icons: {primary:'ui-icon-circle-close'} });
		});
	});
}

function obter_agenda()
{
	db.transaction(function(x) {
		// Exibir Prospects Historicos
		x.executeSql('SELECT * FROM agenda WHERE (exportado IS NULL OR editado = 1 OR remover = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-agenda').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);										
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					if(item.remover == 1)
					{
						style_erro += ' class="erro" ';
						msg_erro += ' Excluir compromisso.';
					}
					
					
					var pendencia = '<input type="checkbox" value="'+item.id+'" name="compromissos[]" id="compromissos-'+i+'" /><label for="compromissos-'+i+'" ' + style_erro + '>'+item.titulo+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-agenda').append('<li>'+pendencia+'</li>');
				}
			}else{
				$('#lista-agenda').append('<li>Nenhum compromisso para sincronizar.</li>');
			}
			$("input[name='compromissos[]']").button({ icons: {primary:'ui-icon-circle-close'} });
		});
	});
}

function obter_pendencias()
{
	db.transaction(function(x) {
		
		// Exibir Prospects Historicos
		x.executeSql('SELECT * FROM pendencias WHERE (exportado IS NULL OR editado = 1 OR remover = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-pendencias').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);										
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					if(item.remover == 1)
					{
						style_erro += ' class="erro" ';
						msg_erro += ' Excluir pendência.';
					}
					
					if(item.status == 'aguardando_encerramento')
					{
						style_erro += ' class="erro" ';
						msg_erro += ' Solicitar encerramento.';
					}
					
					
					var pendencia = '<input type="checkbox" value="'+item.id+'" name="pendencias[]" id="pendencias-'+i+'" /><label for="pendencias-'+i+'" ' + style_erro + '>'+item.titulo+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-pendencias').append('<li>'+pendencia+'</li>');
				}
			}else{
				$('#lista-pendencias').append('<li>Nenhuma pendência para sincronizar.</li>');
			}
			$("input[name='pendencias[]']").button({ icons: {primary:'ui-icon-circle-close'} });
		});
	});
}

function obter_pendencias_mensagens()
{
	db.transaction(function(x) {
		
		// Exibir Prospects Historicos
		x.executeSql('SELECT * FROM pendencias_mensagens WHERE (exportado IS NULL OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-pendencias-mensagens').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);										
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					var pendencia = '<input type="checkbox" value="'+item.id+'" name="pendencias-mensagens[]" id="pendencias-mensagens-'+i+'" /><label for="pendencias-mensagens-'+i+'" ' + style_erro + '>'+item.id+' - '+date('d/m/Y H:i:s', item.timestamp)+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-pendencias-mensagens').append('<li>'+pendencia+'</li>');
				}
			}else{
				$('#lista-pendencias-mensagens').append('<li>Nenhuma mensagem de pendência para sincronizar.</li>');
			}
			$("input[name='pendencias-mensagens[]']").button({ icons: {primary:'ui-icon-circle-close'} });
		});
	});
}

/**
* Metódo:		obter_pedidos_aguardando()
* 
* Descrição:	Função Utilizada para obter os pedidos pendentes ainda 
* 				não sincronizados pelo representante
* 
* Data:			09/11/2013
* Modificação:	09/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web / Robson
* 
*/
function obter_pedidos_aguardando()
{
	db.transaction(function(x) {
		
		// Exibir Prospects
		x.executeSql('SELECT DISTINCT pedido_id_pedido, cliente_nome FROM pedidos_pendentes WHERE pedido_id_pedido != 0 AND (exportado IS NULL OR editado = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-pedidos_pendentes').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					var pedido = '<input type="checkbox" value="'+item.pedido_id_pedido+'" name="pedidos_pendentes[]" id="pedidos_pendentes-'+i+'" /><label for="pedidos_pendentes-'+i+'" ' + style_erro + '>' + item.pedido_id_pedido + ' - ' + item.cliente_nome + '</label>';
					
					$('#lista-pedidos_pendentes').append('<li>'+pedido+'</li>');
				}
			}else{
				$('#lista-pedidos_pendentes').append('<li>Nenhum pedido para sincronizar.</li>');
			}
			
			$("input[name='pedidos_pendentes[]']").button({ icons: {primary:'ui-icon-circle-close'} });
			
		});
	});
}

/**
* Metódo:		obter_orcamentos_aguardando()
* 
* Descrição:	Função Utilizada para obter os pedidos pendentes ainda 
* 				não sincronizados pelo representante
* 
* Data:			16/11/2013
* Modificação:	16/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web / Robson
* 
*/
function obter_orcamentos_aguardando()
{
	db.transaction(function(x) {
		
		// Exibir Prospects
		x.executeSql('SELECT DISTINCT pedido_id_pedido, cliente_nome, prospect_nome FROM orcamentos WHERE pedido_id_pedido != 0 AND (exportado IS NULL OR editado = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-orcamentos').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					if(item.cliente_nome)
					{
						var nome_cliente_prospect = item.cliente_nome;
					}
					else
					{
						var nome_cliente_prospect = item.prospect_nome;
					}
					
					var pedido = '<input type="checkbox" value="'+item.pedido_id_pedido+'" name="orcamentos[]" id="orcamentos-'+i+'" /><label for="orcamentos-'+i+'" ' + style_erro + '>' + item.pedido_id_pedido + ' - ' + nome_cliente_prospect + '</label>';
					
					$('#lista-orcamentos').append('<li>'+pedido+'</li>');
				}
			}else{
				$('#lista-orcamentos').append('<li>Nenhum orçamento para sincronizar.</li>');
			}
			
			$("input[name='orcamentos[]']").button({ icons: {primary:'ui-icon-circle-close'} });
			
		});
	});
}