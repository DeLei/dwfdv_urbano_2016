$(document).ready(function() {  
	// obter pedidos
	
	obter_pedidos(sessionStorage['ped_pend_pagina_atual'], sessionStorage['ped_pend_ordem_atual'], sessionStorage['ped_pend_situacao'], sessionStorage['ped_pend_cliente'], sessionStorage['ped_pend_codigo'], sessionStorage['ped_pend_dt_inicial'], sessionStorage['ped_pend_dt_final'], sessionStorage['ped_pend_filial']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_pedidos(sessionStorage['ped_pend_pagina_atual'], $(this).data('campo') + ' DESC', sessionStorage['ped_pend_situacao'], sessionStorage['ped_pend_cliente'], sessionStorage['ped_pend_codigo'], sessionStorage['ped_pend_dt_inicial'], sessionStorage['ped_pend_dt_final'], sessionStorage['ped_pend_filial']);
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_pedidos(sessionStorage['ped_pend_pagina_atual'], $(this).data('campo') + ' ASC', sessionStorage['ped_pend_situacao'], sessionStorage['ped_pend_cliente'], sessionStorage['ped_pend_codigo'], sessionStorage['ped_pend_dt_inicial'], sessionStorage['ped_pend_dt_final'], sessionStorage['ped_pend_filial']);
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	// obter filiais
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT * FROM filiais', [], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=filial]').append('<option value="">Todos</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
				}
				
				
				
			}
		);
	});
	
	
	$('select[name=situacao]').val(sessionStorage['ped_pend_situacao']);
	$('input[name=cliente]').val(sessionStorage['ped_pend_cliente']);
	$('input[name=codigo]').val(sessionStorage['ped_pend_codigo']);
	$('input[name=dt_inicial]').val(sessionStorage['ped_pend_dt_inicial']);
	$('input[name=dt_final]').val(sessionStorage['ped_pend_dt_final']);
	
	$('#filtrar').click(function() {
		sessionStorage['ped_pend_pagina_atual'] = 1;
		sessionStorage['ped_pend_filial'] = $('select[name=filial]').val();
		sessionStorage['ped_pend_situacao'] = $('select[name=situacao]').val();
		sessionStorage['ped_pend_cliente'] = $('input[name=cliente]').val();
		sessionStorage['ped_pend_codigo'] = $('input[name=codigo]').val();
		sessionStorage['ped_pend_dt_inicial'] = $('input[name=dt_inicial]').val();
		sessionStorage['ped_pend_dt_final'] = $('input[name=dt_final]').val();
		
		obter_pedidos(sessionStorage['ped_pend_pagina_atual'], sessionStorage['ped_pend_ordem_atual'], sessionStorage['ped_pend_situacao'], sessionStorage['ped_pend_cliente'], sessionStorage['ped_pend_codigo'], sessionStorage['ped_pend_dt_inicial'], sessionStorage['ped_pend_dt_final'], sessionStorage['ped_pend_filial']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['ped_pend_pagina_atual'] = 1;
		sessionStorage['ped_pend_situacao'] = '';
		sessionStorage['ped_pend_filial'] = '';
		sessionStorage['ped_pend_cliente'] = '';
		sessionStorage['ped_pend_codigo'] = '';
		sessionStorage['ped_pend_dt_inicial'] = '';
		sessionStorage['ped_pend_dt_final'] = '';

		$('select[name=situacao]').val(sessionStorage['ped_pend_situacao']);
		$('select[name=filial]').val(sessionStorage['ped_pend_filial']);
		$('input[name=cliente]').val(sessionStorage['ped_pend_cliente']);
		$('input[name=codigo]').val(sessionStorage['ped_pend_codigo']);
		$('input[name=dt_inicial]').val(sessionStorage['ped_pend_dt_inicial']);
		$('input[name=dt_final]').val(sessionStorage['ped_pend_dt_final']);
		
		obter_pedidos(sessionStorage['ped_pend_pagina_atual'], sessionStorage['ped_pend_ordem_atual'], sessionStorage['ped_pend_situacao'], sessionStorage['ped_pend_cliente'], sessionStorage['ped_pend_codigo'], sessionStorage['ped_pend_dt_inicial'], sessionStorage['ped_pend_dt_final'], sessionStorage['ped_pend_filial']);
	});
});

function obter_pedidos(pagina, ordem, situacao, cliente, codigo, dt_inicial, dt_final, filial)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'pedido_data_emissao DESC';
	situacao = situacao ? situacao : '';
	filial = filial ? filial : '';
	cliente = cliente ? cliente : '';
	codigo = codigo ? codigo : '';
	dt_inicial = dt_inicial ? dt_inicial : '';
	dt_final = dt_final ? dt_final : '';
	
	// setar pagina e ordem atual
	sessionStorage['ped_pend_pagina_atual'] = pagina;
	sessionStorage['ped_pend_ordem_atual'] = ordem;
	sessionStorage['ped_pend_situacao'] = situacao;
	sessionStorage['ped_pend_filial'] = filial;
	sessionStorage['ped_pend_cliente'] = cliente;
	sessionStorage['ped_pend_codigo'] = codigo;
	sessionStorage['ped_pend_dt_inicial'] = dt_inicial;
	sessionStorage['ped_pend_dt_final'] = dt_final;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['ped_pend_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'ASC');
		
		$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'DESC');
		
		$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['ped_pend_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	var wheres = '';
	
	if (situacao)
	{
		wheres += 'AND pedido_status LIKE "%' + situacao + '%"';
	}
	else
	{
		wheres += ' AND pedido_status IS NOT NULL   ';
	//	wheres += " AND dw_delecao != '1' ";
	}
	
	if (filial > 0)
	{
		wheres += ' AND pedido_filial = "' + filial + '"';
	}
	
	if (cliente)
	{
		wheres += ' AND cliente_nome LIKE "%' + cliente + '%"';
	}
	
	if (codigo)
	{
		wheres += ' AND pedido_id_pedido LIKE "%' + codigo + '%"';
	}
	
	if (dt_inicial && dt_inicial != 'undefined')
	{	
		var ex = explode('/', dt_inicial);
		
		wheres += ' AND pedido_data_emissao >= "' + ex[2] + ex[1] + ex[0] + '"';
	}

	if (dt_final && dt_final != 'undefined')
	{
		var ex = explode('/', dt_final);
	
		wheres += ' AND pedido_data_emissao <= "' + ex[2] + ex[1] + ex[0] + '"';
	}
	
	if (info.empresa)
	{
		wheres += " AND empresa = '" + info.empresa + "'";
	}
	
	wheres += ' AND exportado = "1" ';

	obter_grupo_permissao(function(grupo_usuario){
		
		if(grupo_usuario.grupo == 'preposto'){
			
			wheres += " AND pedido_codigo_preposto = '"+grupo_usuario.codigo_preposto+"' "
		}
	
		// exibir pedido e calcular totais
		db.transaction(function(x) {
			x.executeSql(
				'SELECT pedido_id_pedido, SUM(pedido_preco_venda * pedido_quantidade) AS valor_total, SUM(pedido_quantidade) AS quantidade_vendida, * FROM pedidos_pendentes WHERE  pedido_id_pedido > 0 AND pedido_status != "I" ' + wheres + ' GROUP BY pedido_id_pedido ORDER BY ' + sessionStorage['ped_pend_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
					if (dados.rows.length)
					{
						$('table tbody').empty();
						
						for (i = 0; i < dados.rows.length; i++)
						{
							
							var pedido = dados.rows.item(i);
							
							var pedidos = [];
							pedidos.push('<img src="img/' + (obter_status_pedidos_pendentes(pedido.pedido_status)) + '.png">');
							pedidos.push(pedido.pedido_id_pedido + '/' + pedido.pedido_filial);
							pedidos.push(pedido.cliente_nome);
							pedidos.push(protheus_data2data_normal(pedido.pedido_data_emissao));
							pedidos.push(number_format(pedido.quantidade_vendida, 0, ',', '.'));
							pedidos.push(number_format(pedido.valor_total, 3, ',', '.'));
							pedidos.push('<a href="pedidos_espelho.html#' + pedido.pedido_id_pedido + '|' + pedido.pedido_filial + '|P" class="btn btn-large btn-primary">Visualizar</a>');
							console.log('pedidos_espelho.html#' + pedido.pedido_id_pedido + '|' + pedido.pedido_filial + '|P');
							var html = concatenar_html('<td>', '</td>', pedidos);
						
							$('table tbody').append('<tr>' + html + '</tr>');
							
							alterarCabecalhoTabelaResolucao();
						}
					}
					else
					{
						$('table tbody').html('<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum pedido encontrado.</strong></td></tr>');
					}
				}
			);
			
			// calcular totais
			console.log('SELECT COUNT(DISTINCT(pedido_id_pedido)) AS total, SUM(pedido_preco_venda * pedido_quantidade) AS valor_total FROM pedidos_pendentes WHERE pedido_id_pedido != "" ' + wheres + ' GROUP BY pedido_id_pedido');
			x.executeSql(
				'SELECT COUNT(DISTINCT(pedido_id_pedido)) AS total, SUM(pedido_preco_venda * pedido_quantidade) AS valor_total FROM pedidos_pendentes WHERE pedido_id_pedido != "" AND pedido_status != "I" ' + wheres + ' ', [], function(x, dados) {
					
					var total = dados.rows.length;
						$('#total').text(number_format(0, 0, ',', '.'));
						$('#valor_total').text(number_format(0, 3, ',', '.'));
					
						if(total >0){
						var dado = dados.rows.item(0);
						
						$('#total').text(number_format(dado.total, 0, ',', '.'));
						$('#valor_total').text(number_format(dado.valor_total, 3, ',', '.'));
						
						// paginação
						
						$('#paginacao').html('');
						
						var total = ceil(dado.total / 20);
						
						if (sessionStorage['ped_pend_pagina_atual'] > 6)
						{
							$('#paginacao').append('<a href="#" onclick="obter_pedidos(1, \'' + sessionStorage['ped_pend_ordem_atual'] + '\', \'' + sessionStorage['ped_pend_situacao'] + '\', \'' + sessionStorage['ped_pend_cliente'] + '\', \'' + sessionStorage['ped_pend_codigo'] + '\', \'' + sessionStorage['ped_pend_dt_inicial'] + '\', \'' + sessionStorage['ped_pend_dt_final'] + '\', \'' + sessionStorage['ped_pend_filial'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
						}
						
						if (sessionStorage['ped_pend_pagina_atual'] > 1)
						{
							$('#paginacao').append('<a href="#" onclick="obter_pedidos(' + (intval(sessionStorage['ped_pend_pagina_atual']) - 1) + ', \'' + sessionStorage['ped_pend_ordem_atual'] + '\', \'' + sessionStorage['ped_pend_situacao'] + '\', \'' + sessionStorage['ped_pend_cliente'] + '\', \'' + sessionStorage['ped_pend_codigo'] + '\', \'' + sessionStorage['ped_pend_dt_inicial'] + '\', \'' + sessionStorage['ped_pend_dt_final'] + '\', \'' + sessionStorage['ped_pend_filial'] + '\'); return false;">&lt;</a> ');
						}
						
						for (i = intval(sessionStorage['ped_pend_pagina_atual']) - 6; i <= intval(sessionStorage['ped_pend_pagina_atual']) + 5; i++)
						{
							if (i <= 0 || i > total)
							{
								continue;
							}
							
							if (i == sessionStorage['ped_pend_pagina_atual'])
							{
								$('#paginacao').append('<strong>' + i + '</strong> ');
							}
							else
							{
								$('#paginacao').append('<a href="#" onclick="obter_pedidos(' + i + ', \'' + sessionStorage['ped_pend_ordem_atual'] + '\', \'' + sessionStorage['ped_pend_situacao'] + '\', \'' + sessionStorage['ped_pend_cliente'] + '\', \'' + sessionStorage['ped_pend_codigo'] + '\', \'' + sessionStorage['ped_pend_dt_inicial'] + '\', \'' + sessionStorage['ped_pend_dt_final'] + '\', \'' + sessionStorage['ped_pend_filial'] + '\'); return false;">' + i + '</a> ');
							}
						}
						
						if (sessionStorage['ped_pend_pagina_atual'] < total)
						{
							$('#paginacao').append('<a href="#" onclick="obter_pedidos(' + (intval(sessionStorage['ped_pend_pagina_atual']) + 1) + ', \'' + sessionStorage['ped_pend_ordem_atual'] + '\', \'' + sessionStorage['ped_pend_situacao'] + '\', \'' + sessionStorage['ped_pend_cliente'] + '\', \'' + sessionStorage['ped_pend_codigo'] + '\', \'' + sessionStorage['ped_pend_dt_inicial'] + '\', \'' + sessionStorage['ped_pend_dt_final'] + '\', \'' + sessionStorage['ped_pend_filial'] + '\'); return false;">&gt;</a> ');
						}
						
						if (sessionStorage['ped_pend_pagina_atual'] <= total - 6)
						{
							$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_pedidos(' + total + ', \'' + sessionStorage['ped_pend_ordem_atual'] + '\', \'' + sessionStorage['ped_pend_situacao'] + '\', \'' + sessionStorage['ped_pend_cliente'] + '\', \'' + sessionStorage['ped_pend_codigo'] + '\', \'' + sessionStorage['ped_pend_dt_inicial'] + '\', \'' + sessionStorage['ped_pend_dt_final'] + '\', \'' + sessionStorage['ped_pend_filial'] + '\'); return false;">Última Página</a> ');
						}
					}
				}
			);
		});
	})
}
