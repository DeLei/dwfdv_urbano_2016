$(document).ready(function() {


	$('.campos_autenticacao').keyup(function(e) {
		if(e.keyCode == 13) {
			
			$('input[type=submit]').click();
			
			$('input').focusout();
			$('input').blur();
			
		}
	});

	$('input[type=submit]').click(function() {
		
		console.log('Autenticando...');
		
		var nome_de_usuario = $('input[name=nome_de_usuario]').val();
		var senha = $('input[name=senha]').val();
		
		if (!nome_de_usuario || !senha)
		{			
			//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
			mensagem('Digite seu nome de usuário e senha.');
		}
		else
		{
			
			$.ajax({
				url: config.ws_url + 'autenticacao/autenticar',
				headers : {
					"DW_KEY_APP" : md5(nome_de_usuario.toUpperCase() + senha.toUpperCase() )
				},     
				data: {
					key: md5(nome_de_usuario.toUpperCase()  + senha.toUpperCase())
				},
				dataType: 'json',
				type: 'GET',
				success: function(dados) {
					if (!dados['error'] && dados.status != 'inativo' )
					{
					
						localStorage.clear();
						
						// Irá criar a tabela com os campos retornados do WS automaticamente se ela não existir
						criar_tabela('informacoes_do_representante', dados, false);
						
						db.transaction(function(x) {
							var campos 			= [];
							var interrogacoes  	= [];
							var valores		 	= [];
						
							$.each(dados, function(indice, valor) {
								campos.push(indice);
								interrogacoes.push('?');
								valores.push(valor);
								
								if(indice == 'empresa')
								{
									indice = 'empresa_representante';
								}
								
								// Gravando dados do representante na sessao
								localStorage.setItem(indice, valor);
								
							});
							
							document.addEventListener("deviceready", function(){
								var networkInterface = {};
								
								// Get network interface   
								networkInterface = window.plugins.macaddress.getMacAddress(function(mac){
									localStorage.setItem('macAddress', mac);
								}, function(){
									localStorage.setItem('macAddress', 'NÃO LOCALIZADO');
								});	
							}, false);
						
							x.executeSql('INSERT INTO informacoes_do_representante (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores);
							
							
							exibirMensagem('dialog_lembrar', 'Agora o sistema será instalado.', "window.location = 'sincronizar.html';", 'Atenção');
						});
						
					}
					else
					{
						//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
						mensagem('Dados inválidos.');
					}
					
					
					
				},
				error: function() {
					//$(".autenticar").effect("shake", { times:3}, 100); //specify the effect with how much time u want to shake Effects
					mensagem('Não foi possível se conectar com o servidor, verifique sua conexão com a internet. Por favor tente novamente.');
				}
			});
			
		}
		
		return false;
	});
	

});
