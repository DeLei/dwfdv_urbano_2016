$(document).ready(function(){
	
	ativar_tabs();

	obter_filiais();
	
	obter_tipo_pedido();
	// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA
	obter_tipo_venda();
	// FIM CUSTOM: 177 / 000874 - 04/11/2013
	obter_clientes();
	
	/**
	* 
	* Classe:		select_gravar_sessao
	*
	* Descrição:	Classe utilizada para "select", com a finalizade de gravar na sessão o valor do campo quando o usuário usar o "change"
	*
	*/
	$('.select_gravar_sessao').live('change', function(){
		salvar_sessao($(this).attr('name'), $(this).val());
	});
	
	
	
	
	obter_tabela_precos();
	
	obter_condicao_pagamento();
	
	remover_disabled();
	
	/**
	* 
	* ID:			trocar_cliente
	*
	* Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
	*
	*/
	$('#trocar_cliente').live('click', function(){
		
		salvar_sessao('descricao_cliente', null);
		salvar_sessao('codigo_cliente', null);
		salvar_sessao('loja_cliente', null);
		
		$('input[name="cliente"]').val("");
		$('input[name="codigo_cliente"]').val("");
		$('input[name="loja_cliente"]').val("");
		
		rotina_cliente();
		
		$(this).hide();
		$('#info_cli').hide();
		
		
	});

	
	/**
	* 
	* CLASSE:		avancar
	*
	* Descrição:	Utilizado para validar os campos, se estiver tudo correto, passar para a proxima ABA
	*
	*/
	$('.avancar').live('click', function(e){
		e.preventDefault();
		
		if(sessionStorage['sessao_pedido'])
		{
			var sessao_pedido = unserialize(sessionStorage['sessao_pedido']);
		}
		else
		{
			var sessao_pedido = [];
		}
		
		if(!sessao_pedido.filial)
		{
			mensagem('Selecione uma <strong>Filial</strong>.');
		}
		else if(!sessao_pedido.tipo_pedido)
		{
			mensagem('Selecione um <strong>Tipo de Pedido</strong>.');
		}
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA
		else if(!sessao_pedido.tipo_venda)
		{
			mensagem('Selecione um <strong>Tipo de Venda</strong>.');
		}		
		// FIM CUSTOM: 177 / 000874 - 04/11/2013
		else if(!sessao_pedido.codigo_cliente)
		{
			mensagem('Selecione um <strong>Cliente</strong>.');
		}
		else if(!sessao_pedido.tabela_precos)
		{
			mensagem('Selecione uma <strong>Tabela de Preços</strong>.');
		}
		else if(!sessao_pedido.condicao_pagamento)
		{
			mensagem('Selecione uma <strong>Condição de Pagamento</strong>.');
		}
		else
		{
		
			// Ativando Botao "Adicionar Produtos"
			$('#id_adicionar_produtos').removeClass('tab_desativada');
			ativar_tabs();
			
			//Chamando as funções do arquivo adicionar produtos
			adicionar_produtos();
			
			// Acionando (Click) botao "Adicionar Produtos"
			$("#id_adicionar_produtos").click();
		}
		
	});
	
	
	
	
	

});


/**
* Metódo:		ativar_tabs
* 
* Descrição:	Função Utilizada ativar a biblioteca, e desativar tabs com a classe tab_desativada
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function ativar_tabs()
{
	$( "#tabs" ).tabs();
	
	// Desativando tabs com classe tab_desativada
	var tabs_desativar = [];
			
	$( "#tabs ul li a" ).each(function(e, i){
		var classname = i.className;
		
		if(classname === 'tab_desativada')
		{
			tabs_desativar.push(e);
			
		}
	});
	
	
	$( "#tabs" ).tabs( "option", "disabled", tabs_desativar );

}


/**
* Metódo:		obter_filiais
* 
* Descrição:	Função Utilizada para retornar as filiais
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_filiais()
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM filiais', [], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=filial]').append('<option value="">Selecione...</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
				}
				
				// Selecionar campo se existir na sessão
				if(obter_valor_sessao('filial'))
				{
					$('select[name="filial"] option[value="' + obter_valor_sessao('filial') + '"]').attr('selected', 'selected');
				}
				
			}
		);
	});
}



/**
* Metódo:		obter_tabela_precos
* 
* Descrição:	Função Utilizada para retornar Tabelas Preços
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tabela_precos()
{
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT * FROM tabelas_preco', [], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=tabela_precos]').append('<option value="">Selecione...</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=tabela_precos]').append('<option value="' + dado.codigo + '" data-condicao_pagamento="' + dado.condicao_pagamento + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
				}
				
				rotina_tabelas_preco();
				
			}
		);
	});
}


/**
* Metódo:		obter_condicao_pagamento
* 
* Descrição:	Função Utilizada para retornar Condições de pagamento
* 
* Data:			09/10/2013
* Modificação:	09/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_condicao_pagamento()
{
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT * FROM formas_pagamento', [], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=condicao_pagamento]').append('<option value="">Selecione...</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=condicao_pagamento]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
				}
				
				// Selecionar campo se existir na sessão
				if(obter_valor_sessao('condicao_pagamento'))
				{
					$('select[name="condicao_pagamento"] option[value="' + obter_valor_sessao('condicao_pagamento') + '"]').attr('selected', 'selected');
					$('select[name="condicao_pagamento"]').change();
				}
				
			}
		);
	});
}



/**
* Metódo:		obter_tipo_pedido
* 
* Descrição:	Função Utilizada para retornar Tipos de pedido
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tipo_pedido()
{

	$('select[name=tipo_pedido]').append('<option value="">Selecione...</option>');
	$('select[name=tipo_pedido]').append('<option value="N">Venda Normal</option>');
	
	// Selecionar campo se existir na sessão
	if(obter_valor_sessao('tipo_pedido'))
	{
		$('select[name="tipo_pedido"] option[value="' + obter_valor_sessao('tipo_pedido') + '"]').attr('selected', 'selected');
	}
}


/**
* Metódo:		obter_clientes
* 
* Descrição:	Função Utilizada para retornar Todos os Clientes
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_clientes()
{

	db.transaction(function(x) {
		x.executeSql('SELECT * FROM clientes', [], function(x, dados) {
				if (dados.rows.length)
				{
					var clientes = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						clientes.push({ label: dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf, codigo: dado.codigo, loja: dado.loja, tabela_preco: dado.tabela_preco, desconto: dado.desconto  });
					}
					
					buscar_clientes(clientes);
					
					// Selecionar campo se existir na sessão
					if(obter_valor_sessao('codigo_cliente'))
					{
						$('input[name=cliente]').val(obter_valor_sessao('descricao_cliente'));
						$('input[name=codigo_cliente]').val(obter_valor_sessao('codigo_cliente'));
						$('input[name=loja_cliente]').val(obter_valor_sessao('loja_cliente'));
						
						exibir_informacoes_cliente(obter_valor_sessao('codigo_cliente'), obter_valor_sessao('loja_cliente'));
						
						// Bloquear campo quando selecionar cliente
						$('input[name=cliente]').attr('disabled', 'disabled');
						$('#trocar_cliente').show();
					}
				
				}
			}
		);
	});

}

/**
* Metódo:		buscar_clientes
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_clientes(clientes)
{
	$('input[name=cliente]').autocomplete({
		minLength: 3,
		source: clientes,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=cliente]').val(ui.item.label);
			$('input[name=codigo_cliente]').val(ui.item.codigo);
			$('input[name=loja_cliente]').val(ui.item.loja);
			
			salvar_sessao('descricao_cliente', ui.item.label);
			salvar_sessao('codigo_cliente', ui.item.codigo);
			salvar_sessao('loja_cliente', ui.item.loja);
			salvar_sessao('desconto_cliente', ui.item.desconto);
			
			exibir_informacoes_cliente(ui.item.codigo, ui.item.loja);
			
			rotina_cliente(ui.item.tabela_preco);
			
			// Bloquear campo quando selecionar cliente
			$('input[name=cliente]').attr('disabled', 'disabled');
			$('#trocar_cliente').show();

			return false;
		}
	});
}

/**
* Metódo:		exibir_informacoes_cliente
* 
* Descrição:	Função Utilizada para exibir informações do cliente
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_informacoes_cliente(codigo, loja)
{
	if(codigo && loja)
	{
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						var html = '';
						
						$('.info_nome').html(dado.nome);
						$('.info_cpf').html(dado.cpf);

						
						obter_ultimo_pedido_cliente(codigo, loja);

						$('.info_limite_credito').html(number_format(dado.limite_credito, 3, ',', '.'));
						$('.info_titulos_aberto').html(number_format(dado.total_titulos_aberto, 3, ',', '.'));
						$('.info_titulos_vencidos').html(number_format(dado.total_titulos_ventidos, 3, ',', '.'));
						$('.info_credito_disponivel').html(number_format(dado.limite_credito - dado.total_titulos_aberto, 3, ',', '.'));

						$('.info_endereco').html(dado.endereco);
						$('.info_bairro').html(dado.bairro);
						$('.info_cidade').html(dado.cidade);
						$('.info_estado').html(dado.estado);
						
						$('.info_cep').html(dado.cep);
						$('.info_telefone').html(dado.telefone);
						
						$('#info_cli').show();
						
					}
					
				}
			);
		});
	}
}

/**
* Metódo:		salvar_sessao
* 
* Descrição:	Função Utilizada para salvar dados do pedido na sessão
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		String 			var nome_campo		- Nome do campo que será utilizado na sessao
* @param		String 			var valor			- Valor do campo que será utilizado na sessao
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_sessao(nome_campo, valor)
{
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage['sessao_pedido'])
	{
		sessao_pedido = unserialize(sessionStorage['sessao_pedido']);
	}
	
	// Adicionar novo valor no array
	sessao_pedido[nome_campo] = valor;
	
	// Gravar dados do Array na sessão
	sessionStorage['sessao_pedido'] = serialize(sessao_pedido);
	
	console.log(unserialize(sessionStorage['sessao_pedido']));
	
	remover_disabled();
}

/**
* Metódo:		obter_valor_sessao
* 
* Descrição:	Função Utilizada para retornar o valor de campo da sessão
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @param		String 			var campo		- Nome do campo que será utilizado para retornar o valor na sessao
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_valor_sessao(campo)
{
	if(sessionStorage['sessao_pedido'])
	{
		var sessao = unserialize(sessionStorage['sessao_pedido']);
	}
	
	if(sessao)
	{
		if(sessao[campo])
		{
			return sessao[campo];
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

/**
* Metódo:		obter_ultimo_pedido_cliente
* 
* Descrição:	Função Utilizada para pegar o ultimo pedido do cliente e exibir os valores nas informações do cliente
* 
* Data:			10/10/2013
* Modificação:	10/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do Cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_ultimo_pedido_cliente(codigo, loja)
{
	db.transaction(function(x) {	
		x.executeSql('SELECT SUM(ip_valor_total_item) AS valor_pedido, SUM(ip_total_desconto_item) AS valor_desconto, * FROM pedidos_processados WHERE cliente_codigo = ? AND cliente_loja = ? GROUP BY pedido_codigo ORDER BY pedido_codigo DESC', [codigo, loja], function(x, dados) {
			if(dados.rows.length)
			{
				$('#conteudo_ultimo_pedido ul').show();
				$('#conteudo_ultimo_pedido div').hide();
				
				console.log('Pedidos');
				var dado = dados.rows.item(0);
				console.log('Pedidos - Clientes');
				console.log(dado);
				
				$('.info_pedido_data_emissao').html(protheus_data2data_normal(dado.pedido_data_emissao));
				$('.info_pedido_codigo').html(dado.pedido_codigo);
				$('.info_forma_pagamento').html(dado.forma_pagamento_descricao);
				$('.info_valor_pedido').html('R$ ' + number_format(dado.valor_pedido, 3, ',', '.'));
				$('.info_valor_desconto').html('R$ ' + number_format(dado.valor_desconto, 3, ',', '.'));
			}
			else
			{
				$('#conteudo_ultimo_pedido ul').hide();
				$('#conteudo_ultimo_pedido div').show();
			}
		});
	});
}


//---------------------------------------------------------------------------
//----------  ----------  Rotinas (Regras) da Incusão do Pedido ----------  ----------
//---------------------------------------------------------------------------

/**
* Metódo:		rotina_cliente
* 
* Descrição:	Função Utilizada para selecionar a tabela de preços do cliente (Se existir, se não seleciona a opção vazia "Selecione..." e bloqueia o campo e condicao_pagamento)
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function rotina_cliente(tabela)
{
	if(tabela)
	{
		$('select[name="tabela_precos"] option[value="' + tabela + '"]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').change();
		
	}
	else
	{
		$('select[name="tabela_precos"] option[value=""]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').change();
		$('select[name="tabela_precos"]').attr('disabled', 'disabled');
		
		$('select[name="condicao_pagamento"] option[value=""]').attr('selected', 'selected');
		$('select[name="condicao_pagamento"]').change();
		$('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
	}
}


/**
* Metódo:		rotina_tabelas_preco
* 
* Descrição:	Função Utilizada bucar a condição de pagamento da tabela de preços e depois selecionar a condição de pagamento e bloquea-la (Se não existir condição de pagamento na tabela de preços, o campo de condição de pagamento fica livre para seleção)
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function rotina_tabelas_preco()
{
	$('select[name=tabela_precos]').change(function(){
	
		// Obter condição de pagamento pela tabela de preços
		var codigo_tabela_precos = $(this).val();
		var codigo_condicao_pagamento = $(this).children(":selected").data('condicao_pagamento');
		
		if(!codigo_condicao_pagamento )
		{
			if(obter_valor_sessao('condicao_pagamento'))
			{
				$('select[name="condicao_pagamento"] option[value="' + obter_valor_sessao('condicao_pagamento') + '"]').attr('selected', 'selected');
			}
		}
		
		// Se existir condição de pagamento selecionar e bloquear campo
		if(codigo_condicao_pagamento)
		{
			$('select[name="condicao_pagamento"] option[value="' + codigo_condicao_pagamento + '"]').attr('selected', 'selected');
			$('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
			
			salvar_sessao('condicao_pagamento', codigo_condicao_pagamento);
			
			$('#aviso_condicao_pagamento').html('A condição de pagamento ' + codigo_tabela_precos + ' é fixa para a tabela de preços ' + codigo_condicao_pagamento + '.');
		}
		else
		{
			$('select[name="condicao_pagamento"]').removeAttr('disabled');
			$('#aviso_condicao_pagamento').html('');
		}
		
		// chamando regra de desconto para salvar na sessao
		regra_desconto();

	});
	
	// Selecionar campo se existir na sessão
	if(obter_valor_sessao('tabela_precos'))
	{
		$('select[name="tabela_precos"] option[value="' + obter_valor_sessao('tabela_precos') + '"]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').change();
	}
	
}

/**
* Metódo:		regra_desconto
* 
* Descrição:	Função Utilizada para salvar o valor da regra de desconto na sessao ( A regra padrão e pela tabela de preços)
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function regra_desconto()
{
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT percentual_desconto FROM regra_desconto WHERE tabela_preco = ?', [obter_valor_sessao('tabela_precos')], function(x, dados) {

				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);
					
					salvar_sessao('regra_desconto', dado.percentual_desconto);
				}
				else
				{
					salvar_sessao('regra_desconto', 0);
				}
				
			}
		);
	});
}


/**
* Metódo:		remover_disabled
* 
* Descrição:	Função Utilizada para habilitar os botoes que estao bloqueados
* 
* Data:			09/10/2013
* Modificação:	09/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function remover_disabled()
{
	if(sessionStorage['sessao_pedido'])
	{
		var sessao_pedido = unserialize(sessionStorage['sessao_pedido']);
		
		// Se existir Filial na sessao, habilitar TIPO DE PEDIDO
		if(sessao_pedido.filial)
		{
			
			$('select[name=tipo_pedido]').removeAttr('disabled');
		}
		
		// CUSTOM: 177 / 000874 - 04/11/2013 - Localizar: CUST-TIPO-VENDA
		// Se existir Filial na sessao, habilitar TIPO DE VENDA
		if(sessao_pedido.filial)
		{
			$('select[name=tipo_venda]').removeAttr('disabled');
		}
		// FIM CUSTOM: 177 / 000874 - 04/11/2013 
		
		// Se existir Tipo de Pedido na sessao, habilitar CLIENTE
		if(sessao_pedido.tipo_pedido && !sessao_pedido.codigo_cliente)
		{
			$('input[name=cliente]').removeAttr('disabled');
		}

		// Se existir Cliente na sessao, habilitar TABELA de PREÇOS
		if(sessao_pedido.codigo_cliente)
		{
			$('select[name=tabela_precos]').removeAttr('disabled');
		}
		
		//-------------------------------------------------------------------------------------------------------------
		//------------ Desativar botao "Adicionar Produtos" se uma opção nao for selecionada --------------------------
		//-------------------------------------------------------------------------------------------------------------
		
		if(!sessao_pedido.filial)
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.tipo_pedido)
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.codigo_cliente)
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.tabela_precos)
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.condicao_pagamento)
		{
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		ativar_tabs();

	}
}