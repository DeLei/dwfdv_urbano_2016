var cliente;
var ver_endereco;

$(document).ready(function() {

	db.transaction(function(x) {
	
		var fragment 		= parse_url(location.href).fragment;			
			fragment_real 	= fragment;
			fragment 		= fragment.split('_');
		
		x.executeSql(
			'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [fragment[0], fragment[1]], function(x, dados) {
				if (dados.rows.length)
				{
					cliente = dados.rows.item(0);
					
					$('.codigo').html(cliente.codigo);
					$('.loja').html(cliente.loja);
					$('.nome').html(cliente.nome);
					$('.cpf').html(formatar_cpfCNPJ(cliente.cpf));
					$('.endereco').html(cliente.endereco);
					$('.bairro').html(cliente.bairro);
					$('.cep').html(formatar_cep(cliente.cep));
					$('.cidade').html(cliente.cidade);
					$('.estado').html(cliente.estado);
					$('.email').html(cliente.email ? cliente.email : 'N/A');
					
					/*Tratamento telefone*/
					var telefone = '';
					if(cliente.ddd)
					{
						telefone = '('+cliente.ddd+') ' + cliente.telefone;
					}
					else
					{
						if(cliente.telefone)
						{
							telefone = cliente.telefone;
						}						
					}
					$('.telefone').html(telefone ? telefone : 'N/A');
					/*Tratamento telefone*/
					
					$('.limite_credito').html(number_format(cliente.limite_credito, 3, ',', '.'));
					$('.total_titulos_aberto').html(number_format(cliente.total_titulos_aberto, 3, ',', '.'));
					$('.total_titulos_ventidos').html(number_format(cliente.total_titulos_ventidos, 3, ',', '.'));
					$('.limite_disponivel').html(number_format(cliente.limite_credito - cliente.total_titulos_aberto, 3, ',', '.'));					
					//---------
					
					$('input[name=pessoa_contato]').val(cliente.pessoa_contato);
					$('input[name=email]').val(cliente.email);
					
					// Adicionar botoes no menu latral
					var botoes = '';						
						botoes += '<li class="pedido_novo_pedido" ><a class="btn btn-large btn-primary btnCadastro" href="javascript: obter_cliente_pedido(\''+cliente.codigo+'\', \''+cliente.loja+'\', \'N\');">Novo Pedido</a></li>';
						//botoes += '<li><a class="btn btn-large btn-primary btnCadastro" href="javascript: obter_cliente_pedido(\''+cliente.codigo+'\', \''+cliente.loja+'\', \'*\');">Novo Orçamento</a></li>';
						botoes += '<li><a class="btn btn-large btn-primary" href="pedidos_processados.html#' + cliente.codigo + '_' + cliente.loja + '">Consultar Pedidos</a></li>';
						
						
						
						
						
						//Verificar o aparelho
						if(verificarAparelho())
						{
							botoes += '<li><a class="btn btn-large btn-primary" href="clientes_endereco.html#'+fragment_real+'" id="ver_mapa">Como chegar</a></li>';
						}
						
						
						botoes += '<li><a class="btn btn-large btn-primary" href="pendencias_adicionar.html#' + cliente.codigo + '|' + cliente.loja + '|C">Solicitar Alterações</a></li>';
						
					$('.caixa ul').append(botoes);
					
					//Setar os botões
					//$('.caixa ul li a').button();

					//---------
					// Último Pedido
					
					
					obter_ultimo_pedido_cliente(cliente.codigo,cliente.loja, function(ultimo_pedido){
						console.log(ultimo_pedido);
						db.transaction(function(x) {	
						x.executeSql(
								'SELECT * FROM pedidos_processados WHERE ip_codigo_pedido=? ', [ultimo_pedido], function(x, dados) {
									
									if (dados.rows.length){
									var dado = dados.rows.item(0);
										if (dado)
										{
											$('.numero_pedido').html(dado.pedido_codigo);	
											$('.pedido_data_emissao').html(protheus_data2data_normal(dado.pedido_data_emissao));
											$('.forma_pagamento_descricao').html(dado.forma_pagamento_descricao);
											
											
											var valor_total_pedido = 0;
											
											for (i = 0; i < dados.rows.length; i++)
											{
												valor_total_pedido += parseFloat(dados.rows.item(i).ip_valor_total_item);
											}
											
											
											$('.valor_total_pedido').html(number_format(valor_total_pedido, 3, ',', '.'));
											
										}
									}
								});
						})
					});
					
					
					
					//---------
					
					
					x.executeSql(
						'SELECT * FROM historico_clientes WHERE cpf = ? ORDER BY id DESC', [cliente.cpf], function(x, dados) {
							if (dados.rows.length)
							{
								for (i = 0; i < dados.rows.length; i++)
								{
									var dado = dados.rows.item(i);
									
									$('table:last').append('<tr><td>' + date('d/m/Y', dado.timestamp) + '</td><td>' + dado.pessoa_contato + '</td><td>' + (dado.cargo ? dado.cargo : 'N/A') + '</td><td>' + (dado.email ? dado.email : 'N/A') + '</td><td>' + (dado.protocolo ? dado.protocolo : 'N/A') + '</td><td><a href="#" id="ver_historico" data-id="' + dado.id + '">Ver Detalhes</a></td></tr>');
								}
							}

							
							alterarCabecalhoTabelaConteudo();	
							alterarCabecalhoTabelaResolucao();
						}
					);					
					
					alterarCabecalhoTabelaConteudo();	
					alterarCabecalhoTabelaResolucao();
					
				}
				else
				{
					window.location = 'index.html';
				}
			}
		);
	});
	
	// ver histórico
	
	$('#ver_historico').live('click', function() {
		var id = $(this).attr('data-id');
		
		db.transaction(function(x) {
		
			x.executeSql(
				'SELECT * FROM historico_clientes WHERE id = ?', [id], function(x, dados) {
					
					var historico = dados.rows.item(0);
					
					var html = '<table class="tabela" width="100%" style="min-width: 420px;">';
						html += '<tr><th>Cliente</th><th>Contato</th><th>Cargo</th><th>E-mail</th></tr>'
						html += '<tr><td>' + cliente.nome + '</td>';
						html += '<td>' + historico.pessoa_contato + '</td>';
						html += '<td>' + (historico.cargo ? historico.cargo : 'N/A') + '</td>';
						html += '<td>' + (historico.email ? historico.email : 'N/A') + '</td></tr>';
						html += '</table>';
					
						html+= '<p>' + nl2br(historico.descricao) + '</p>';
					
					//$.colorbox({ html: html });
					exibirMensagem('dialog_historico', html, '', 'Histórico');
					
					alterarCabecalhoTabelaConteudo();	
				}
			);
		});
		
		return false;
	});
	
	// novo histórico
	
	$('#link_novo_historico').click(function() {
		$(this).parents('p').hide();
		
		$('#novo_historico').show();
		
		return false;
	});
	
	// cancelar
	
	$('#cancelar').click(function() {
		$('#link_novo_historico').parents('p').show();
		
		$('#novo_historico').hide();
		
		return false;
	});
	
	// enviar form
	
	$('form').submit(function() {
		if (!$('input[name=pessoa_contato]').val())
		{
			mensagem('Digite uma pessoa de contato.');
		}
		else if ($('input[name=email]').val() && !validar_email($('input[name=email]').val()))
		{
			mensagem('Digite um e-mail válido.');
		}
		else if (!$('textarea[name=descricao]').val())
		{
			mensagem('Digite uma descrição para o histórico.');
		}
		else
		{
			db.transaction(function(x) {
				var id = parse_url(location.href).fragment;
				
				x.executeSql(
					'SELECT id FROM historico_clientes ORDER BY id DESC LIMIT 1', [], function(x, dados) {
						
						var id = uniqid();
						
						var tmp_1 = ['id', 'timestamp', 'codigo_representante', 'codigo_cliente', 'loja_cliente', 'cpf'];
						var tmp_2 = ['?', '?', '?', '?', '?', '?'];
						var tmp_3 = [id, time(), info.cod_rep, cliente.codigo, cliente.loja, cliente.cpf];
						
						$('input[type!=submit], textarea').each(function() {
							tmp_1.push($(this).attr('name'));
							tmp_2.push('?');
							tmp_3.push($(this).val());
						});
						
						x.executeSql('INSERT INTO historico_clientes (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')', tmp_3);
						
						mensagem('Histórico adicionado com sucesso.');
						
						$('table:last').prepend('<tr><td>' + date('d/m/Y', time()) + '</td><td>' + $('input[name=pessoa_contato]').val() + '</td><td>' + ($('input[name=cargo]').val() ? $('input[name=cargo]').val() : 'N/A') + '</td><td>' + ($('input[name=email]').val() ? $('input[name=email]').val() : 'N/A') + '</td><td>' + ($('input[name=protocolo]').val() ? $('input[name=protocolo]').val() : 'N/A') + '</td><td><a href="#" id="ver_historico" data-id="' + id + '">Ver Detalhes</a></td></tr>');

						
						$('input[name=cargo], textarea[name=descricao]').val('');
						
						$('#cancelar').trigger('click');
						
						scroll('h3:last');
						
						alterarCabecalhoListagem('#historicos .novo_grid');
					}
				);
			});
		}
		
		return false;
	});

});

function obter_cliente_pedido(codigo_cliente, codigo_loja, tipo)
{	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [codigo_cliente, codigo_loja], 
			function(x, dados) 
			{				
				var dado = dados.rows.item(0);
				
				//Cabeçalho pedido
				var pedido_copiado = [];
							
				pedido_copiado['cliente_prospect'] 				= 'C';
				pedido_copiado['codigo_cliente'] 				= dado.codigo;
				pedido_copiado['loja_cliente']					= dado.loja;
				pedido_copiado['descricao_cliente']				= dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf;
				pedido_copiado['desconto_cliente']				= dado.desconto;
				pedido_copiado['tipo_pedido']					= tipo;
				
				
				
				pedido_copiado['codigo_cliente_entrega']		= dado.codigo;
				pedido_copiado['loja_cliente_entrega']			= dado.loja;
			
				pedido_copiado['descricao_cliente_entrega']		=  dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf;
		
				
				
//				CUSTOM: 177/ 000870- 24/10/2013 - Localizar: CUST-PEDIDO-CLIENTE-BLOQUEADO
				dados_cliente =  {};			 
				dados_cliente.situacao = dado.situacao;									
//				FIM CUSTOM: 177/ 000870- 24/10/2013 
				dados_cliente.situacao_sintegra = dado.situacao_sintegra;	// CUSTOM: 177/ 000870- 24/10/2013 - Localizar: CUST-PEDIDO-CLIENTE-SITUACAO-SINTEGRA
				
				pedido_copiado['dados_cliente']					=  dados_cliente;
				if(tipo == 'N')
				{
					sessionStorage['sessao_pedido'] = serialize(pedido_copiado);
					window.location = "pedidos_adicionar.html";
				}
				else
				{
					sessionStorage['sessao_orcamento'] = serialize(pedido_copiado);
					window.location = "pedidos_adicionar.html#O";
				}

			
			}
		)
	});
}


function obter_ultimo_pedido_cliente(codigo, loja, callback){

	var ultimo_pedido = null;
	
	if(codigo && loja){
		
		var sql_ultimo_pedido = '';
		sql_ultimo_pedido += 'select ip_codigo_pedido ';
		sql_ultimo_pedido +=	'from pedidos_processados ';
		sql_ultimo_pedido +='where pedido_codigo_cliente = ?';
		sql_ultimo_pedido +='and  pedido_loja_cliente = ?';
		sql_ultimo_pedido +='order by pedido_data_emissao DESC, recno desc limit 1';
		
		db.transaction(function(x) {	
			x.executeSql(
					sql_ultimo_pedido, [codigo,loja], function(x, dados) {
					
					var total = dados.rows.length;
					var dado = dados.rows.item(0);
					if(total > 0){
						
						ultimo_pedido = dado.ip_codigo_pedido;
					}
					
					callback(ultimo_pedido);
				});
		});
		
	}else{
		callback(ultimo_pedido);
		
	}
	
	
	

	
	
}