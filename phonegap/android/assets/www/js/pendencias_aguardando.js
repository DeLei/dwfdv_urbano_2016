$(document).ready(function() {
	// obter notícias
	
	
	obter_pendencias(sessionStorage['pendencias_aguardando_pagina_atual'], sessionStorage['pendencias_aguardando_ordem_atual'], sessionStorage['pendencias_aguardando_criacao_inicial'], sessionStorage['pendencias_aguardando_criacao_final'], sessionStorage['pendencias_aguardando_prazo_solucao_inicial'], sessionStorage['pendencias_aguardando_prazo_solucao_final'], sessionStorage['pendencias_aguardando_prioridade'], sessionStorage['pendencias_aguardando_status'], sessionStorage['pendencias_aguardando_titulo']);
	
	//Ordenação do grid
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_pendencias(sessionStorage['pendencias_aguardando_pagina_atual'], $(this).data('campo') + ' DESC', sessionStorage['pendencias_aguardando_criacao_inicial'], sessionStorage['pendencias_aguardando_criacao_final'], sessionStorage['pendencias_aguardando_prazo_solucao_inicial'], sessionStorage['pendencias_aguardando_prazo_solucao_final'], sessionStorage['pendencias_aguardando_prioridade'], sessionStorage['pendencias_aguardando_status'], sessionStorage['pendencias_aguardando_titulo']);
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_pendencias(sessionStorage['pendencias_aguardando_pagina_atual'], $(this).data('campo') + ' ASC', sessionStorage['pendencias_aguardando_criacao_inicial'], sessionStorage['pendencias_aguardando_criacao_final'], sessionStorage['pendencias_aguardando_prazo_solucao_inicial'], sessionStorage['pendencias_aguardando_prazo_solucao_final'], sessionStorage['pendencias_aguardando_prioridade'], sessionStorage['pendencias_aguardando_status'], sessionStorage['pendencias_aguardando_titulo']);
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	//Filtrar dados
	$('input[name=titulo]').val(sessionStorage['pendencias_aguardando_titulo']);
	$('input[name=criacao_inicial]').val(sessionStorage['pendencias_aguardando_criacao_inicial']);
	$('input[name=criacao_final]').val(sessionStorage['pendencias_aguardando_criacao_final']);
	$('input[name=prazo_solucao_inicial]').val(sessionStorage['pendencias_aguardando_prazo_solucao_inicial']);
	$('input[name=prazo_solucao_final]').val(sessionStorage['pendencias_aguardando_prazo_solucao_final']);
	$('input[name=prioridade]').val(sessionStorage['pendencias_aguardando_prioridade']);
	$('input[name=status]').val(sessionStorage['pendencias_aguardando_status']);

	$('#filtrar').click(function() {
		
		sessionStorage['pendencias_aguardando_pagina_atual'] 			= 1;
		sessionStorage['pendencias_aguardando_titulo'] 				= $('input[name=titulo]').val();
		sessionStorage['pendencias_aguardando_criacao_inicial'] 		= $('input[name=criacao_inicial]').val();
		sessionStorage['pendencias_aguardando_criacao_final'] 			= $('input[name=criacao_final]').val();
		
		sessionStorage['pendencias_aguardando_prazo_solucao_inicial']	= $('input[name=prazo_solucao_inicial]').val();
		sessionStorage['pendencias_aguardando_prazo_solucao_final'] 	= $('input[name=prazo_solucao_final]').val();
		sessionStorage['pendencias_aguardando_prioridade'] 			= $('select[name=prioridade]').val();
		sessionStorage['pendencias_aguardando_status'] 				= $('select[name=status]').val();
		
		obter_pendencias(sessionStorage['pendencias_aguardando_pagina_atual'], sessionStorage['pendencias_aguardando_ordem_atual'], sessionStorage['pendencias_aguardando_criacao_inicial'], sessionStorage['pendencias_aguardando_criacao_final'], sessionStorage['pendencias_aguardando_prazo_solucao_inicial'], sessionStorage['pendencias_aguardando_prazo_solucao_final'], sessionStorage['pendencias_aguardando_prioridade'], sessionStorage['pendencias_aguardando_status'], sessionStorage['pendencias_aguardando_titulo']);
	});
	
	$('#limpar_filtros').click(function() {
		
		sessionStorage['pendencias_aguardando_pagina_atual'] 			= 1;
		sessionStorage['pendencias_aguardando_titulo'] 				= '';
		sessionStorage['pendencias_aguardando_criacao_inicial'] 		= '';
		sessionStorage['pendencias_aguardando_criacao_final'] 			= '';
		
		sessionStorage['pendencias_aguardando_prazo_solucao_inicial'] 	= '';
		sessionStorage['pendencias_aguardando_prazo_solucao_final'] 	= '';
		sessionStorage['pendencias_aguardando_prioridade'] 			= '';
		sessionStorage['pendencias_aguardando_status'] 				= '';

		$('input[name=titulo]').val(sessionStorage['pendencias_aguardando_titulo']);
		$('input[name=criacao_inicial]').val(sessionStorage['pendencias_aguardando_criacao_inicial']);
		$('input[name=criacao_final]').val(sessionStorage['pendencias_aguardando_criacao_final']);
		
		$('input[name=prazo_solucao_inicial]').val(sessionStorage['pendencias_aguardando_prazo_solucao_inicial']);
		$('input[name=prazo_solucao_final]').val(sessionStorage['pendencias_aguardando_prazo_solucao_final']);
		$('select[name=prioridade]').val(sessionStorage['pendencias_aguardando_prioridade']);
		$('select[name=status]').val(sessionStorage['pendencias_aguardando_status']);
		
		//
		
		obter_pendencias(sessionStorage['pendencias_aguardando_pagina_atual'], sessionStorage['pendencias_aguardando_ordem_atual'], sessionStorage['pendencias_aguardando_criacao_inicial'], sessionStorage['pendencias_aguardando_criacao_final'], sessionStorage['pendencias_aguardando_prazo_solucao_inicial'], sessionStorage['pendencias_aguardando_prazo_solucao_final'], sessionStorage['pendencias_aguardando_prioridade'], sessionStorage['pendencias_aguardando_status'], sessionStorage['pendencias_aguardando_titulo']);
	});
	
});

function obter_pendencias(pagina, ordem, dt_inicial, dt_final, prazo_inicial, prazo_final, prioridadeX, statusX, titulo)
{
	//Filtros
	pagina 					= pagina 		?	pagina 			: 1;
	ordem 					= ordem 		?	ordem 			: 'timestamp DESC';
	titulo 					= titulo 		?	titulo 			: '';
	criacao_inicial 		= dt_inicial 	?	dt_inicial 		: '';
	criacao_final 			= dt_final 		? 	dt_final 		: '';
	prazo_solucao_inicial 	= prazo_inicial ? 	prazo_inicial 	: '';
	prazo_solucao_final 	= prazo_final 	? 	prazo_final 	: '';
	prioridade 				= prioridadeX 	? 	prioridadeX 	: '';
	status  				= statusX 		? 	statusX 		: '';
		
	// setar pagina e ordem atual
	sessionStorage['pendencias_aguardando_pagina_atual'] 			= pagina;
	sessionStorage['pendencias_aguardando_ordem_atual'] 			= ordem;
	sessionStorage['pendencias_aguardando_titulo'] 				= titulo;
	sessionStorage['pendencias_aguardando_criacao_inicial'] 		= criacao_inicial;
	sessionStorage['pendencias_aguardando_criacao_final'] 			= criacao_final;
	sessionStorage['pendencias_aguardando_prazo_solucao_inicial'] = prazo_solucao_inicial;
	sessionStorage['pendencias_aguardando_prazo_solucao_final'] 	= prazo_solucao_final;
	sessionStorage['pendencias_aguardando_prioridade'] 			= prioridade;
	sessionStorage['pendencias_aguardando_status'] 				= status;
	
	// definir seta
	var ordem = explode(' ', sessionStorage['pendencias_aguardando_ordem_atual']);
	
	if (ordem[1] == 'ASC')
	{
		$('a[data-campo=' + ordem[0] + ']').data('ordem', 'ASC');
		
		$('a[data-campo=' + ordem[0] + ']').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo=' + ordem[0] + ']').data('ordem', 'DESC');
		
		$('a[data-campo=' + ordem[0] + ']').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['pendencias_aguardando_pagina_atual'] - 1) * 20;
	
	//Where
	var wheres = '';
	
	if (titulo && titulo != 'undefined')
	{
		wheres += ' AND titulo LIKE "%' + titulo + '%"';
	}
	
	if (status && status != 'undefined')
	{
		wheres += ' AND status = "' + status + '"';
	}
	
	if (prioridade && prioridade != 'undefined')
	{
		wheres += ' AND prioridade = "' + prioridade + '"';
	}
	
	if (criacao_inicial && criacao_inicial != 'undefined')
	{	
		var ex = explode('/', criacao_inicial);
		
		wheres += ' AND timestamp >= "' + mktime(0, 0, 0, ex[1], ex[0], ex[2]) + '"';
	}

	if (criacao_final && criacao_final != 'undefined')
	{
		var ex = explode('/', criacao_final);
	
		wheres += ' AND timestamp <= "' + mktime(23, 59, 59, ex[1], ex[0], ex[2]) + '"';
	}
	
	///PRAZO
	if (prazo_solucao_inicial && prazo_solucao_inicial != 'undefined')
	{	
		var ex = explode('/', prazo_solucao_inicial);
		
		wheres += ' AND prazo_solucao_timestamp >= "' + mktime(0, 0, 0, ex[1], ex[0], ex[2]) + '"';
	}

	if (prazo_solucao_final && prazo_solucao_final != 'undefined')
	{
		var ex = explode('/', prazo_solucao_final);
	
		wheres += ' AND prazo_solucao_timestamp <= "' + mktime(23, 59, 59, ex[1], ex[0], ex[2]) + '"';
	}
	
	wheres += ' AND (remover = \'1\' OR editado = \'1\' OR erro = \'1\' OR exportado IS NULL)';
	
	// exibir pedido e calcular totais
	console.log('SELECT * FROM pendencias WHERE id > 0 ' + wheres + ' ORDER BY ' + sessionStorage['pendencias_aguardando_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pendencias WHERE id > 0 ' + wheres + '  GROUP BY pendencias.id ORDER BY ' + sessionStorage['pendencias_aguardando_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
				if (dados.rows.length)
				{
					$('table tbody').empty();

					for (i = 0; i < dados.rows.length; i++)
					{
						
						var status 		= '';
						var pendencia 	= dados.rows.item(i);
						
						if(pendencia.status == 'em_aberto'){
							status = '<img alt="Em Aberto" src="img/status/vermelho.png">';
						} else if(pendencia.status == 'aguardando_encerramento'){
							status = '<img alt="Aguardando Encerramento" src="img/status/laranja.png">';
						} else if(pendencia.status == 'encerrada'){
							status = '<img alt="Encerrada" src="img/status/verde.png">';
						}
						
						var html = '';
							html += '<td align="center">'+status+'</td>';
							html += '<td align="center">'+date('d/m/Y H:i:s', pendencia.timestamp)+'</td>';							
							html += '<td>'+pendencia.titulo+'</td>';
							html += '<td align="center">'+protheus_data2data_normal(pendencia.prazo_solucao)+'</td>';
							html += '<td align="center">'+ucwords(pendencia.prioridade)+'</td>';
							html += '<td width="160"><a href="pendencias_espelho.html#' + pendencia.id + '" id="ver_pendencia" data-id="' + pendencia.id + '" class="btn btn-large btn-primary">Ver Detalhes</a></td>';
					
						$('table tbody').append('<tr>' + html + '</tr>');
						
					//	alterarCabecalhopendenciaDescricaoTabelaResolucao();
					}
				}
				else
				{
					$('table tbody').html('<tr><td colspan="6" style="color: #900; padding: 10px;"><strong>Nenhuma pendência encontrada.</strong></td></tr>');
				}
			});
		
		// calcular totais
		x.executeSql(
				'SELECT COUNT(DISTINCT id) AS total FROM pendencias WHERE id != "" ' + wheres, [], function(x, dados) {
				var dado = dados.rows.item(0);

				$('#total').text(number_format(dado.total, 0, ',', '.'));
				// paginação

				$('#paginacao').html('');

				var total = ceil(dado.total / 20);

				if (sessionStorage['pendencias_aguardando_pagina_atual'] > 6)
				{
					$('#paginacao').append('<a href="#" onclick="obter_pedidos(1, \'' + sessionStorage['pendencias_aguardando_ordem_atual'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_inicial'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_final'] + '\', \'' + sessionStorage['pendencias_aguardando_titulo'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
				}

				if (sessionStorage['pendencias_aguardando_pagina_atual'] > 1)
				{
					$('#paginacao').append('<a href="#" onclick="obter_pendencias(' + (intval(sessionStorage['pendencias_aguardando_pagina_atual']) - 1) + ', \'' + sessionStorage['pendencias_aguardando_ordem_atual'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_inicial'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_final'] + '\', \'' + sessionStorage['pendencias_aguardando_titulo'] + '\'); return false;">&lt;</a> ');
				}

				for (i = intval(sessionStorage['pendencias_aguardando_pagina_atual']) - 6; i <= intval(sessionStorage['pendencias_aguardando_pagina_atual']) + 5; i++)
				{
					if (i <= 0 || i > total)
					{
						continue;
					}

					if (i == sessionStorage['pendencias_aguardando_pagina_atual'])
					{
						$('#paginacao').append('<strong>' + i + '</strong> ');
					}
					else
					{
						$('#paginacao').append('<a href="#" onclick="obter_pendencias(' + i + ', \'' + sessionStorage['pendencias_aguardando_ordem_atual'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_inicial'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_final'] + '\', \'' + sessionStorage['pendencias_aguardando_titulo'] + '\'); return false;">' + i + '</a> ');
					}
				}

				if (sessionStorage['pendencias_aguardando_pagina_atual'] < total)
				{
					$('#paginacao').append('<a href="#" onclick="obter_pendencias(' + (intval(sessionStorage['pendencias_aguardando_pagina_atual']) + 1) + ', \'' + sessionStorage['pendencias_aguardando_ordem_atual'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_inicial'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_final'] + '\', \'' + sessionStorage['pendencias_aguardando_titulo'] + '\'); return false;">&gt;</a> ');
				}

				if (sessionStorage['pendencias_aguardando_pagina_atual'] <= total - 6)
				{
					$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_pendencias(' + total + ', \'' + sessionStorage['pendencias_aguardando_ordem_atual'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_inicial'] + '\', \'' + sessionStorage['pendencias_aguardando_criacao_final'] + '\', \'' + sessionStorage['pendencias_aguardando_titulo'] + '\'); return false;">Última Página</a> ');
				}
		});
	});
}