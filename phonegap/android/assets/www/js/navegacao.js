$(document).ready(function() {

	//<li><a href="enviar_pedidos.html"><img src="img/back.png" style="-webkit-transform: rotate(180deg);"><br>Enviar Pedidos</a></li>
	//<ul>
	//<li><a href="index.html"><img src="img/folder_home.png"><br>Principal</a></li>x
	//<li><a href="prospects.html"><img src="img/agt_family-off.png"><br>Prospects</a></li>x
	//<li><a href="clientes.html"><img src="img/agt_family.png"><br>Clientes</a></li>x
	//<li><a href="orcamentos_menu.html"><img src="img/kspread.png"><br>Orçamentos</a></li>x
	//<li><a href="pedidos.html"><img src="img/money.png"><br>Pedidos</a></li><li>x
	//<a href="consultas.html"><img src="img/mail_find.png"><br>Consultas</a></li>x
	//<li><a href="sincronizar.html"><img src="img/reload.png"><br>Sincronizar</a></li>xx
	//<li><a href="info.html"><img src="img/gear.png"><br>Manutenção</a></li>x
	//</ul>
	/*var menu = '<ul>';
		menu += '	<li><a href="index.html"><img src="img/folder_home.png"><br>Principal</a></li>';
		menu += '	<li><a href="consultas.html"><img src="img/mail_find.png"><br>Consultas</a></li>';
		menu += '	<li><a href="clientes.html"><img src="img/agt_family.png"><br>Clientes</a></li>';
		menu += '	<li><a href="prospects.html"><img src="img/prospects.png" width="32"><br>Prospects</a></li>';	
		menu += '	<li><a href="orcamentos_menu.html"><img src="img/orcamentos.png" width="32"><br>Orçamentos</a></li>';
		menu += '	<li><a href="pedidos.html"><img src="img/pedido.png" width="32"><br>Pedidos</a></li><li>';
		menu += '	<li><a href="pendencias.html"><img src="img/kalarm.png"><br>Pendências</a></li><li>';	
		menu += '	<li><a href="noticias.html"><img src="img/news-icon.png"><br>Notícias</a></li><li>';			
		menu += '	<li><a class="btnOffline" href="sincronizar.html"><img src="img/reload.png"><br>Sincronizar</a></li>';
		menu += '	<li><a href="info.html"><img src="img/gear.png"><br>Manutenção</a></li>';
		menu += '</ul>';
	$('#navegacao').html(menu);
	 
	$('#navegacao li a + ul').hide();

	$('#navegacao li').hover(function() {
		$(this).children('ul').show();
	}, function() {
		$(this).children('ul').hide();
	});
	*/
	
	$('#navegacao').remove();
	
	$('.enviar_pedidos a').click(function () {
		var _this = this;

		apprise('Você realmente deseja enviar os pedidos?', {
			'verify': true,
			'textYes': 'Sim',
			'textNo': 'Não'
		}, function (dado) {
			if (dado)
			{
				window.location = $(_this).attr('href');
			}
		});

		return false;
	});

	$('.sincronizar_tudo a').click(function () {
		var _this = this;

		apprise('Você realmente deseja sincronizar tudo?', {
			'verify': true,
			'textYes': 'Sim',
			'textNo': 'Não'
		}, function (dado) {
			if (dado)
			{
				window.location = $(_this).attr('href');
			}
		});

		return false;
	});
});