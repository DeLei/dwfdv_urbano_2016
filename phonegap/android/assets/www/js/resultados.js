$(document).ready(function() {
	
	Highcharts.setOptions({
		credits: {
			enabled: false
		},
		lang: {
			weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
			months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
		},
		title: {
			text: ' '
		},
		subtitle: {
			text: ' '
		},
		yAxis: {
			title: ' ',
			labels: {
				formatter: function() {
					return number_format(this.value, 2, ',', '.');
				}
			}
		}
	});
	
	
	
	/**
	 * Grafico de comissoes (Pagas e A Pagar)
	 */
	var comissao_receber = 4000.00;
	var comissao_recebida = 6000.00;
	$('#comissoes').highcharts({
		chart: {
		    plotBackgroundColor: null,
		    plotBorderWidth: 0.3,
		    plotShadow: false
		},
		title: {
		    text: 'Comissões <br/><strong>' + obter_meses(date('m')) + '</strong>',
			align: 'center',
			verticalAlign: 'middle',
			y: 50
		},
		tooltip: {
			enabled: false,
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
			    cursor: 'pointer',
			    dataLabels: {
				    enabled: true,
				    color: '#000000',
					connectorColor: '#000000',
					formatter: function() {
					    return '<b>'+ this.point.name +'</b>: '+ round(this.percentage, 2) +' %';
					}
				},
				startAngle: -90,
				endAngle: 90,
				center: ['50%', '75%']
		    }
		},
		series: [{
			type: 'pie',
			name: 'Comissões',
			innerSize: '50%',
			data: [{
				name: 'Recebido<br/>(R$ ' + number_format(comissao_recebida, 2, ',', '.') + ')',
				color: '#5ECA3C',
				y: 60
			},{
				name: 'A Receber<br/>(R$ ' + number_format(comissao_receber, 2, ',', '.') + ')',
				color: '#FFDD54',
				y: 40
			}]
		}]
	});
	
    /**
     * Grafico de titulos (Pagos, A Pagar e Vencidos)
     */
	var titulos_pagos = 6000.00;
	var titulos_a_pagar = 2500.00;
	var titulos_vencidos = 1500.00;
    $('#titulos').highcharts({
		chart: {
		    plotBackgroundColor: null,
		    plotBorderWidth: 0.3,
		    plotShadow: false
		},
		title: {
		    text: 'Títulos <br/><strong>' + obter_meses(date('m')) + '</strong>',
			align: 'center',
			verticalAlign: 'middle',
		    y: 50
		},
		tooltip: {
			enabled: false,
		},
		plotOptions: {
		    pie: {
		    	allowPointSelect: true,
		        cursor: 'pointer',
				dataLabels: {
				    enabled: true,
				    color: '#000000',
					connectorColor: '#000000',
					formatter: function() {
				    	return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
				    }
				},
				startAngle: -90,
				endAngle: 90,
				center: ['50%', '75%']
		    }
		},
		series: [{
		    type: 'pie',
			name: 'Títulos',
			innerSize: '50%',
			data: [{
				name: 'Pago<br/>(R$ ' + number_format(titulos_pagos, 2, ',', '.') + ')',
				color: '#5ECA3C',
				y: 60
			}, {
				name: 'A Pagar<br/>(R$ ' + number_format(titulos_a_pagar, 2, ',', '.') + ')',
				color: '#FFDD54',
				y: 25
			}, {
				name: 'Vencidos<br/>(R$ ' + number_format(titulos_vencidos, 2, ',', '.') + ')',
				color: '#FF3232',
		    	y: 15
		    }]
		}]
    });
    
    
    var dias = new Array();
    var metas = new Array();
    var realizado = new Array();
    var venda_dia = 0;
    for(dia = 0; dia < date('t'); dia++)
    {
    	venda_dia += Math.random() * 20000.00;
    	dias[dia] = dia+1;
    	metas[dia] = 300000.00;
    	realizado[dia] = venda_dia;
    }
    $('#acompanhamento_vendas').highcharts({
        title: {
            text: 'Acompanhamento de Vendas',
            x: -20 //center
        },
        subtitle: {
            text: obter_meses(date('m')) + ' de ' + date('Y'),
            x: -20
        },
        xAxis: {
            categories: dias
        },
        yAxis: {
        	min: 0
        },
        tooltip: {
            formatter: function() {
				return '<strong>' + this.series.name + ', Dia ' + this.x + '</strong><br>R$ ' + Highcharts.numberFormat(this.y, 2, ',', '.');
			}
        },
        series: [{
            name: 'Meta',
            data: metas
        }, {
            name: 'Realizado',
            data: realizado
        }]
    });
});