$(document).ready(function() {
	
	var codigo_e_loja = parse_url(location.href).fragment;
	
	if(codigo_e_loja)
	{
		codigo_e_loja = codigo_e_loja.split('_');
		sessionStorage['ped_pro_cliente'] = codigo_e_loja[0];
	}
	
	// obter pedidos
	
	obter_pedidos(sessionStorage['ped_pro_pagina_atual'], sessionStorage['ped_pro_ordem_atual'], sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_pedidos(sessionStorage['ped_pro_pagina_atual'], $(this).data('campo') + ' DESC', sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial']);
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_pedidos(sessionStorage['ped_pro_pagina_atual'], $(this).data('campo') + ' ASC', sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial']);
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	
	obter_filiais_permitidas(function(filiais){	
		$('select[name=filial]').append('<option value="">Todos</option>');			
		$.each(filiais, function(key, dado) {			
				$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
		});	
	});
	
	
	$('select[name=situacao]').val(sessionStorage['ped_pro_situacao']);
	$('input[name=cliente]').val(sessionStorage['ped_pro_cliente']);
	$('input[name=codigo]').val(sessionStorage['ped_pro_codigo']);
	$('input[name=codigo_pedido_portal]').val(sessionStorage['ped_pro_codigo_portal']); // Solicitação C5_TABLET	
	$('input[name=dt_inicial]').val(sessionStorage['ped_pro_dt_inicial']);
	$('input[name=dt_final]').val(sessionStorage['ped_pro_dt_final']);
	
	$('#filtrar').click(function() {
		sessionStorage['ped_pro_pagina_atual'] = 1;
		sessionStorage['ped_pro_filial'] = $('select[name=filial]').val();
		sessionStorage['ped_pro_situacao'] = $('select[name=situacao]').val();
		sessionStorage['ped_pro_cliente'] = $('input[name=cliente]').val();
		sessionStorage['ped_pro_codigo'] = $('input[name=codigo]').val();
		sessionStorage['ped_pro_codigo_portal'] = $('input[name=codigo_pedido_portal]').val();  // Solicitação C5_TABLET
		sessionStorage['ped_pro_dt_inicial'] = $('input[name=dt_inicial]').val();
		sessionStorage['ped_pro_dt_final'] = $('input[name=dt_final]').val();
		
		obter_pedidos(sessionStorage['ped_pro_pagina_atual'], sessionStorage['ped_pro_ordem_atual'], sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial'], sessionStorage['ped_pro_codigo_portal']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['ped_pro_pagina_atual'] = 1;
		sessionStorage['ped_pro_situacao'] = '';
		sessionStorage['ped_pro_filial'] = '';
		sessionStorage['ped_pro_cliente'] = '';
		sessionStorage['ped_pro_codigo'] = '';
		sessionStorage['ped_pro_codigo_portal'] = ''; //C5_TABLET
		sessionStorage['ped_pro_dt_inicial'] = '';
		sessionStorage['ped_pro_dt_final'] = '';

		$('select[name=situacao]').val(sessionStorage['ped_pro_situacao']);
		$('select[name=filial]').val(sessionStorage['ped_pro_filial']);
		$('input[name=cliente]').val(sessionStorage['ped_pro_cliente']);
		$('input[name=codigo]').val(sessionStorage['ped_pro_codigo']);
		$('input[name=codigo_pedido_portal]').val(sessionStorage['ped_pro_codigo_portal']); // C5_TABLET
		$('input[name=dt_inicial]').val(sessionStorage['ped_pro_dt_inicial']);
		$('input[name=dt_final]').val(sessionStorage['ped_pro_dt_final']);
		
		obter_pedidos(sessionStorage['ped_pro_pagina_atual'], sessionStorage['ped_pro_ordem_atual'], sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial'],sessionStorage['ped_pro_codigo_portal']);
	});
});

function obter_pedidos(pagina, ordem, situacao, cliente, codigo, dt_inicial,dt_final, filial, codigo_pedido_portal) {

	// Exibir apenas pedidos processados do preposto
	obter_grupo_permissao(function(grupo_permissao) {
		
	
		
		
		pagina = pagina ? pagina : 1;
		ordem = ordem ? ordem : 'pedido_data_emissao DESC';
		situacao = situacao ? situacao : '';
		filial = filial ? filial : '';
		cliente = cliente ? cliente : '';
		codigo = codigo ? codigo : '';
		dt_inicial = dt_inicial ? dt_inicial : '';
		dt_final = dt_final ? dt_final : '';
		codigo_pedido_portal = codigo_pedido_portal ? codigo_pedido_portal : ''; // C5_tablet

		// setar pagina e ordem atual
		sessionStorage['ped_pro_pagina_atual'] = pagina;
		sessionStorage['ped_pro_ordem_atual'] = ordem;
		sessionStorage['ped_pro_situacao'] = situacao;
		sessionStorage['ped_pro_filial'] = filial;
		sessionStorage['ped_pro_cliente'] = cliente;
		sessionStorage['ped_pro_codigo'] = codigo;
		sessionStorage['ped_pro_codigo_portal'] = codigo_pedido_portal; // C5_TABLET
		sessionStorage['ped_pro_dt_inicial'] = dt_inicial;
		sessionStorage['ped_pro_dt_final'] = dt_final;

		// definir seta
		var ordem_atual = explode(' ', sessionStorage['ped_pro_ordem_atual']);

		if (ordem_atual[1] == 'ASC') {
			$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'ASC');

			$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▲');
		} else {
			$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'DESC');

			$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▼');
		}

		// calcular offset
		var offset = (sessionStorage['ped_pro_pagina_atual'] - 1) * 20;

		// gerar filtros

		var wheres = '';

		if (situacao) {
			wheres += 'AND status LIKE "%' + situacao + '%"';
		} else {
			wheres += 'AND status IS NOT NULL';
		}

		if (filial > 0) {
			// wheres += ' AND pedido_filial = "' + filial + '"';
			wheres += ' AND (pedido_filial = "' + filial
					+ '" OR pedido_filial = " ")';
		}

		if (cliente) {
			wheres += ' AND cliente_nome LIKE "%' + cliente + '%" OR '
					+ ' cliente_codigo LIKE "%' + cliente + '%"';
		}

		if (codigo) {
			wheres += ' AND pedido_codigo LIKE "%' + codigo + '%"';
		}

		// C5_TABLET
		if (codigo_pedido_portal) {
			wheres += ' AND pedido_codigo_pedido_portal LIKE "%'
					+ codigo_pedido_portal + '%"';
		}
		// FIM C5_TABLET

		// CUSTOM PREPOSTO
		
		if (grupo_permissao.grupo == 'preposto') {
			
			wheres += ' AND pedido_codigo_preposto = '+ grupo_permissao.codigo_preposto;
		}

		if (dt_inicial && dt_inicial != 'undefined') {
			var ex = explode('/', dt_inicial);

			wheres += ' AND pedido_data_emissao >= "' + ex[2] + ex[1] + ex[0]
					+ '"';
		}

		if (dt_final && dt_final != 'undefined') {
			var ex = explode('/', dt_final);

			wheres += ' AND pedido_data_emissao <= "' + ex[2] + ex[1] + ex[0]
					+ '"';
		}

		if (info.empresa) {
			wheres += " AND empresa = '" + info.empresa + "'";
		}

		
		console.log('SELECT pedido_codigo, pedido_codigo_pedido_portal,  SUM(ip_valor_total_item) AS valor_total, SUM(ip_preco_produto * ip_quantidade_faturada_produto) AS valor_faturado, SUM(ip_quantidade_vendida_produto) AS quantidade_vendida, SUM(ip_quantidade_faturada_produto) AS quantidade_faturada, * FROM pedidos_processados WHERE pedido_codigo > 0 '
				+ wheres
				+ ' GROUP BY pedido_codigo ORDER BY '
				+ sessionStorage['ped_pro_ordem_atual']
				+ ' LIMIT 20 OFFSET ' + offset);
		
		
		// exibir pedido e calcular totais
		db
				.transaction(function(x) {
					x
							.executeSql(
									'SELECT pedido_codigo, pedido_codigo_pedido_portal,  SUM(ip_valor_total_item) AS valor_total, SUM(ip_preco_produto * ip_quantidade_faturada_produto) AS valor_faturado, SUM(ip_quantidade_vendida_produto) AS quantidade_vendida, SUM(ip_quantidade_faturada_produto) AS quantidade_faturada, * FROM pedidos_processados WHERE pedido_codigo > 0 '
											+ wheres
											+ ' GROUP BY pedido_codigo ORDER BY '
											+ sessionStorage['ped_pro_ordem_atual']
											+ ' LIMIT 20 OFFSET ' + offset,
									[],
									function(x, dados) {
										console.log(' - - - - ');
										console.log('SELECT pedido_codigo, pedido_codigo_pedido_portal,  SUM(ip_valor_total_item) AS valor_total, SUM(ip_preco_produto * ip_quantidade_faturada_produto) AS valor_faturado, SUM(ip_quantidade_vendida_produto) AS quantidade_vendida, SUM(ip_quantidade_faturada_produto) AS quantidade_faturada, * FROM pedidos_processados WHERE pedido_codigo > 0 '
											+ wheres
											+ ' GROUP BY pedido_codigo ORDER BY '
											+ sessionStorage['ped_pro_ordem_atual']
											+ ' LIMIT 20 OFFSET ' + offset);		
												
												
										if (dados.rows.length) {

											$('table tbody').empty();

											for (i = 0; i < dados.rows.length; i++) {

												var pedido = dados.rows.item(i);
												var pedidos = [];
												pedidos.push('<img src="img/'
														+ pedido.status
														+ '.png">');
												pedidos
														.push(pedido.pedido_codigo
																+ '/'
																+ pedido.pedido_filial);
												pedidos
														.push((pedido.pedido_codigo_pedido_portal) ? pedido.pedido_codigo_pedido_portal
																: 'N/A');
												pedidos
														.push(pedido.cliente_nome);
												pedidos
														.push(protheus_data2data_normal(pedido.pedido_data_emissao));
												pedidos
														.push(number_format(
																pedido.quantidade_vendida,
																0, ',', '.'));
												pedidos
														.push(number_format(
																pedido.quantidade_faturada,
																0, ',', '.'));
												pedidos.push(number_format(
														pedido.valor_faturado,
														2, ',', '.'));
												pedidos.push(number_format(
														pedido.valor_total, 2,
														',', '.'));

												pedidos
														.push('<a href="pedidos_espelho.html#'
																+ pedido.pedido_codigo
																+ '|'
																+ pedido.pedido_filial
																+ '" class="btn btn-large btn-primary">+ Opções</a>');

												var html = concatenar_html(
														'<td>', '</td>',
														pedidos);

												$('table tbody')
														.append(
																'<tr>'
																		+ html
																		+ '</tr>');

												alterarCabecalhoTabelaResolucao();
											}
										} else {
											$('table tbody')
													.html(
															'<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum pedido encontrado.</strong></td></tr>');
										}
									});

					// calcular totais
					console.log('totais: ');
					console.log('SELECT COUNT(DISTINCT pedido_codigo) AS total, SUM(ip_valor_total_item) AS valor_total FROM pedidos_processados WHERE pedido_codigo != "" '
											+ wheres);	
					x
							.executeSql(
									'SELECT COUNT(DISTINCT pedido_codigo) AS total, SUM(ip_valor_total_item) AS valor_total FROM pedidos_processados WHERE pedido_codigo != "" '
											+ wheres,
									[],
									function(x, dados) {
										var dado = dados.rows.item(0);

										$('#total').text(
												number_format(dado.total, 0,
														',', '.'));
										$('#valor_total').text(
												number_format(dado.valor_total,
														2, ',', '.'));

										// paginação

										$('#paginacao').html('');

										var total = ceil(dado.total / 20);

										if (sessionStorage['ped_pro_pagina_atual'] > 6) {
											$('#paginacao')
													.append(
															'<a href="#" onclick="obter_pedidos(1, \''
																	+ sessionStorage['ped_pro_ordem_atual']
																	+ '\', \''
																	+ sessionStorage['ped_pro_situacao']
																	+ '\', \''
																	+ sessionStorage['ped_pro_cliente']
																	+ '\', \''
																	+ sessionStorage['ped_pro_codigo']
																	+ '\', \''
																	+ sessionStorage['ped_pro_dt_inicial']
																	+ '\', \''
																	+ sessionStorage['ped_pro_dt_final']
																	+ '\', \''
																	+ sessionStorage['ped_pro_filial']
																	+ '\', \''
																	+ sessionStorage['ped_pro_codigo_portal']
																	+ '\'  ); return false;">Primeira Página</a>&nbsp;&nbsp;');
										}

										if (sessionStorage['ped_pro_pagina_atual'] > 1) {
											$('#paginacao')
													.append(
															'<a href="#" onclick="obter_pedidos('
																	+ (intval(sessionStorage['ped_pro_pagina_atual']) - 1)
																	+ ', \''
																	+ sessionStorage['ped_pro_ordem_atual']
																	+ '\', \''
																	+ sessionStorage['ped_pro_situacao']
																	+ '\', \''
																	+ sessionStorage['ped_pro_cliente']
																	+ '\', \''
																	+ sessionStorage['ped_pro_codigo']
																	+ '\', \''
																	+ sessionStorage['ped_pro_dt_inicial']
																	+ '\', \''
																	+ sessionStorage['ped_pro_dt_final']
																	+ '\', \''
																	+ sessionStorage['ped_pro_filial']
																	+ '\', \''
																	+ sessionStorage['ped_pro_codigo_portal']
																	+ '\' ); return false;">&lt;</a> ');
										}

										for (i = intval(sessionStorage['ped_pro_pagina_atual']) - 6; i <= intval(sessionStorage['ped_pro_pagina_atual']) + 5; i++) {
											if (i <= 0 || i > total) {
												continue;
											}

											if (i == sessionStorage['ped_pro_pagina_atual']) {
												$('#paginacao').append(
														'<strong>' + i
																+ '</strong> ');
											} else {
												// ped_pro_codigo_portal
												$('#paginacao')
														.append(
																'<a href="#" onclick="obter_pedidos('
																		+ i
																		+ ', \''
																		+ sessionStorage['ped_pro_ordem_atual']
																		+ '\', \''
																		+ sessionStorage['ped_pro_situacao']
																		+ '\', \''
																		+ sessionStorage['ped_pro_cliente']
																		+ '\', \''
																		+ sessionStorage['ped_pro_codigo']
																		+ '\', \''
																		+ sessionStorage['ped_pro_dt_inicial']
																		+ '\', \''
																		+ sessionStorage['ped_pro_dt_final']
																		+ '\', \''
																		+ sessionStorage['ped_pro_filial']
																		+ '\', \''
																		+ sessionStorage['ped_pro_codigo_portal']
																		+ '\'); return false;">'
																		+ i
																		+ '</a> ');
											}
										}

										if (sessionStorage['ped_pro_pagina_atual'] < total) {
											$('#paginacao')
													.append(
															'<a href="#" onclick="obter_pedidos('
																	+ (intval(sessionStorage['ped_pro_pagina_atual']) + 1)
																	+ ', \''
																	+ sessionStorage['ped_pro_ordem_atual']
																	+ '\', \''
																	+ sessionStorage['ped_pro_situacao']
																	+ '\', \''
																	+ sessionStorage['ped_pro_cliente']
																	+ '\', \''
																	+ sessionStorage['ped_pro_codigo']
																	+ '\', \''
																	+ sessionStorage['ped_pro_dt_inicial']
																	+ '\', \''
																	+ sessionStorage['ped_pro_dt_final']
																	+ '\', \''
																	+ sessionStorage['ped_pro_filial']
																	+ '\', \''
																	+ sessionStorage['ped_pro_codigo_portal']
																	+ '\'); return false;">&gt;</a> ');
										}

										if (sessionStorage['ped_pro_pagina_atual'] <= total - 6) {
											$('#paginacao')
													.append(
															'&nbsp;&nbsp;<a href="#" onclick="obter_pedidos('
																	+ total
																	+ ', \''
																	+ sessionStorage['ped_pro_ordem_atual']
																	+ '\', \''
																	+ sessionStorage['ped_pro_situacao']
																	+ '\', \''
																	+ sessionStorage['ped_pro_cliente']
																	+ '\', \''
																	+ sessionStorage['ped_pro_codigo']
																	+ '\', \''
																	+ sessionStorage['ped_pro_dt_inicial']
																	+ '\', \''
																	+ sessionStorage['ped_pro_dt_final']
																	+ '\', \''
																	+ sessionStorage['ped_pro_filial']
																	+ '\', \''
																	+ sessionStorage['ped_pro_codigo_portal']
																	+ '\'); return false;">Última Página</a> ');
										}
									});
				});

	});
}
