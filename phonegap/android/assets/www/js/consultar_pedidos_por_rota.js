$(document).ready(function() {
	$('#nome_representante').html('Representante '+info.nome_representante) ; 
	// obter produtos
	
	obter_pedidos_por_rota(sessionStorage['pedidos_por_rota_pagina_atual'], sessionStorage['pedidos_por_rota_ordem_atual']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_pedidos_por_rota(sessionStorage['pedidos_por_rota_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_pedidos_por_rota(sessionStorage['pedidos_por_rota_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['notas_' + name] != 'undefined' ? sessionStorage['notas_' + name] : '');
	});
	
	
	
	
	
	$('#filtrar').click(function() {
		sessionStorage['pedidos_por_rota_pagina_atual'] = 1;
				
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['' + name] = $(this).val();
		});
		
		obter_pedidos_por_rota(sessionStorage['pedidos_por_rota_pagina_atual'], sessionStorage['pedidos_por_rota_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['pedidos_por_rota_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['notas_' + name] = '';
		});
		
		obter_pedidos_por_rota(sessionStorage['pedidos_por_rota_pagina_atual'], sessionStorage['pedidos_por_rota_ordem_atual']);
	});
	
	
	//-- #ver_titulos -  esta função esta no arquivo geral.js
	//$('#ver_titulos')
	
});

function obter_pedidos_por_rota(pagina, ordem)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'filial ASC';
	
	// setar pagina e ordem atual
	sessionStorage['pedidos_por_rota_pagina_atual'] = pagina;
	sessionStorage['pedidos_por_rota_ordem_atual'] = ordem;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['pedidos_por_rota_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'ASC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'DESC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['pedidos_por_rota_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	
	var wheres = '';
	
	var group_by = ' GROUP BY filial, codigo, descricao ';
	
	console.log('SELECT filial, codigo, descricao, sum(total_kg) as total_kg, sum(total_fd) as total_fd , count(codigo) as \'qtd_pedidos\' FROM pedidos_por_rota WHERE codigo_representante != "" ' + wheres + group_by +  ' ORDER BY ' + sessionStorage['pedidos_por_rota_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT filial, codigo, descricao, sum(total_kg) as total_kg, sum(total_fd) as total_fd , count(codigo) as \'qtd_pedidos\' FROM pedidos_por_rota WHERE codigo_representante != "" ' + wheres + group_by +  ' ORDER BY ' + sessionStorage['pedidos_por_rota_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
			
				if (dados.rows.length)
				{
					$('table tbody').empty();
			
			
					var total_pedidos = 0;
					for (i = 0; i < dados.rows.length; i++)
					{
						var item 				= dados.rows.item(i);				
						var filial		 		= item.filial ? item.filial: 'N/A';
						var codigo 				= item.codigo ? item.codigo: 'N/A';
						var descricao			= item.descricao ? item.descricao: 'N/A';
						var total_kg			= item.total_kg ? item.total_kg: 'N/A';	
						var total_fd			= item.total_fd ? item.total_fd: 'N/A';	
						var qtd_pedidos			= item.qtd_pedidos ? item.qtd_pedidos: 'N/A';	
							total_pedidos += 	qtd_pedidos;
						var html = '';						
							html += '<td align="center">'+filial+'</td>';
							html += '<td align="center">'+codigo+'</td>';
							html += '<td align="center">'+descricao+'</td>';
							html += '<td align="center">'+  (number_format(total_kg, 2, ',', '.'))+'</td>';
							html += '<td align="center">'+ (number_format(total_fd, 2, ',', '.'))+'</td>';
							html += '<td align="center">';
							
							html += '<a href="consultar_pedidos_por_rota_ver_detalhes.html#' + codigo + '|' + filial + '" >';
							
							html += (number_format(qtd_pedidos, 0, ',', '.'));
							html+='</a>'	;
							html += '</td>';
							
						$('table tbody').append('<tr>' + html + '</tr>');
					}
					
					
					var somar = 'SUM(total_kg) AS valor_total, ';
					somar += 'SUM(total_fd) AS saldo_total ';
					
							
					console.log('SELECT '+somar+' FROM pedidos_por_rota WHERE codigo_representante != "" ' + wheres + group_by);
						
					// calcular totais
					x.executeSql(
						'SELECT '+somar+' FROM pedidos_por_rota WHERE codigo_representante != "" ' + wheres  + group_by, [], function(x, dados) {
							
							var dado = dados.rows.item(0);
							//$('#valor_total').text(number_format(dado.valor_total, 3, ',', '.'));
							//$('#saldo_total').text(number_format(dado.saldo_total, 3, ',', '.'));
							
							
							$('table tbody').append('<tr><td align="left" colspan="3">Total</td><td align="center">'+number_format(dado.valor_total, 2, ',', '.')+'</td><td  align="center">'+number_format(dado.saldo_total, 2, ',', '.')+'</td><td  align="center">'+total_pedidos+'</td></tr>');
							
						}
					);
					
					
					
					
					alterarCabecalhoTabelaResolucao();
				
				}
				else
				{
					$('table tbody').html('<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhuma comissão encontrada.</strong></td></tr>');
				}
			}
		);
		
		
		
	});
}
