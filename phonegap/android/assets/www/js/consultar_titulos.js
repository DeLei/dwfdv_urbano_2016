$(document).ready(function() {
	// obter produtos
	
	obter_produtos(sessionStorage['titulos_pagina_atual'], sessionStorage['titulos_ordem_atual']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_produtos(sessionStorage['titulos_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_produtos(sessionStorage['titulos_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['titulos_' + name] != 'undefined' ? sessionStorage['titulos_' + name] : '');
	});
	
	// obter filiais
	obter_filiais_permitidas(function(filiais){	
		$('select[name=titulo_filial]').append('<option value="">Todos</option>');			
		$.each(filiais, function(key, dado) {			
				$('select[name=titulo_filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
		});	
	});
	
	
	$('#filtrar').click(function() {
		sessionStorage['titulos_pagina_atual'] = 1;
				
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['titulos_' + name] = $(this).val();
		});
		
		obter_produtos(sessionStorage['titulos_pagina_atual'], sessionStorage['titulos_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['titulos_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['titulos_' + name] = '';
		});
		
		obter_produtos(sessionStorage['titulos_pagina_atual'], sessionStorage['titulos_ordem_atual']);
	});
});

function obter_produtos(pagina, ordem)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'titulo_data_vencimento ASC';
	
	// setar pagina e ordem atual
	sessionStorage['titulos_pagina_atual'] = pagina;
	sessionStorage['titulos_ordem_atual'] = ordem;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['titulos_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'ASC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'DESC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['titulos_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	filial 				 = sessionStorage['titulos_titulo_filial'];
	codigo 				 = sessionStorage['titulos_titulo_codigo'];
	codigo_cliente 		 = sessionStorage['titulos_titulo_codigo_cliente'];
	cliente_nome 		 = sessionStorage['titulos_cliente_nome'];	
	titulo_codigo 		 = sessionStorage['titulos_titulo_codigo'];
	titulo_status 		 = sessionStorage['titulos_titulo_status'];
	titulo_data_inicial	 = sessionStorage['titulos_titulo_data_vencimento_inicial'];
	titulo_data_final	 = sessionStorage['titulos_titulo_data_vencimento_final'];
	titulo_codigo_pedido = sessionStorage['titulos_titulo_codigo_pedido'];
	
	titulo_data_baixa_inicial	 	= sessionStorage['titulos_titulo_data_baixa_inicial'];
	titulo_data_baixa_final	 		= sessionStorage['titulos_titulo_data_baixa_final'];
	
	var wheres = '';
	
	if (filial && filial != 'undefined')
	{
		wheres += " AND titulo_filial = '" + filial + "'";
	}
	
	if (codigo && codigo != 'undefined')
	{
		wheres += " AND titulo_codigo = '" + codigo + "'";
	}
	
	if (codigo_cliente && codigo_cliente != 'undefined')
	{
		wheres += " AND titulo_codigo_cliente = '" + codigo_cliente + "'";
	}
	
	if (cliente_nome && cliente_nome != 'undefined')
	{
		wheres += " AND cliente_nome LIKE '%" + cliente_nome + "%'";
	}
	
	if (titulo_codigo && titulo_codigo != 'undefined')
	{
		wheres += " AND titulo_codigo = '" + titulo_codigo + "'";
	}
	
	if (titulo_status && titulo_status != 'undefined')
	{
		wheres += " AND titulo_status_extenso = '" + titulo_status + "'";
	}
	//Datas
	if (titulo_data_inicial && titulo_data_inicial != 'undefined')
	{
		var ex = explode('/', titulo_data_inicial);
		wheres += " AND titulo_data_vencimento >= '" + ex[2] + ex[1] + ex[0] + "'";
	}
	
	if (titulo_data_final && titulo_data_final != 'undefined')
	{
		var ex = explode('/', titulo_data_final);
		wheres += " AND titulo_data_vencimento <= '" + ex[2] + ex[1] + ex[0] + "'";
	}
	
	if (titulo_data_baixa_inicial && titulo_data_baixa_inicial != 'undefined')
	{
		var ex = explode('/', titulo_data_baixa_inicial);
		wheres += " AND titulo_data_baixa >= '" + ex[2] + ex[1] + ex[0] + "'";
	}
	
	if (titulo_data_baixa_final && titulo_data_baixa_final != 'undefined')
	{
		var ex = explode('/', titulo_data_baixa_final);
		wheres += " AND titulo_data_baixa <= '" + ex[2] + ex[1] + ex[0] + "'";
	}
	
	if (titulo_codigo_pedido && titulo_codigo_pedido != 'undefined')
	{
		wheres += " AND titulo_codigo_pedido LIKE '%" + titulo_codigo_pedido + "%'";
	}
	
	if (info.empresa)
	{
		wheres += " AND empresa = '" + info.empresa + "'";
	}
	
	console.log('SELECT * FROM titulos WHERE titulo_codigo > 0 ' + wheres + ' ORDER BY ' + sessionStorage['titulos_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM titulos WHERE titulo_codigo > 0 ' + wheres + ' ORDER BY ' + sessionStorage['titulos_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
			
				if (dados.rows.length)
				{
				
					
					$('table tbody').empty();
			
					for (i = 0; i < dados.rows.length; i++)
					{
						var item = dados.rows.item(i);
						
						//-------------------------------------------------------------
						//Status do Titulos
						var dias					= '';
						var status 					= '';
						var timestamp_vencimento 	= strtotime(strtotime_data2data_normal(item.titulo_data_vencimento));
						var timestamp_baixa		 	= strtotime(strtotime_data2data_normal(item.titulo_data_baixa));
						
						if(timestamp_baixa){
							dias = round((timestamp_baixa - timestamp_vencimento) / (3600 * 24));
							//console.log(dias);
							if (timestamp_baixa > timestamp_vencimento){
								status = '<img src="img/status/verde.png" title="Pago com atraso de '+dias+' dia(s)" />';
							}else{
							    status = '<img src="img/status/verde.png" title="Pago em dia" />';
							}
							
						}else{
							dias = abs(round((time() - timestamp_vencimento) / (3600 * 24)));
							
							if (time() > timestamp_vencimento){
								status = '<img src="img/status/vermelho.png" title="'+dias+' dia(s) vencido" />';
							}else{
							    status = '<img src="img/status/laranja.png" title="'+dias+' dia(s) para vencer" />';
							}					            
						}
						//Status do Titulos
						//-------------------------------------------------------------
						
						var parcela = item.titulo_parcela ? item.titulo_parcela : '1';
						
						var html = '';						
							html += '<td width="60" align="center">'+status+'</td>';
							//html += '<td width="50" align="center">'+item.titulo_filial+'</td>';
							html += '<td width="70" align="center">'+item.titulo_codigo + (item.titulo_prefixo ? '/' + item.titulo_prefixo : '' ) + '</td>';
							html += '<td width="70" align="center">' + item.titulo_codigo_pedido + '</td>';
							html += '<td><a href="clientes_visualizar.html#'+item.cliente_codigo+'_'+item.cliente_loja+'">'+item.cliente_nome+'</a></td>';
							html += '<td width="50" align="center">'+parcela+'</td>';						
							html += '<td width="120" align="right">'+number_format(item.titulo_valor, 3, ',', '.')+'</td>';
							
							if(permissao.consultas.titulos.exibir_comissao){
								html += '<td align="right">'+number_format(item.titulo_comissao, 3, ',', '.')+'</td>';
								html += '<td align="right">'+number_format(item.titulo_valor * (item.titulo_comissao / 100), 3, ',', '.')+'</td>';
							}
							html += '<td width="120" align="center">'+protheus_data2data_normal(item.titulo_data_vencimento)+'</td>';
							html += '<td width="120" align="center">'+ (item.titulo_data_baixa ? protheus_data2data_normal(item.titulo_data_baixa) : ' - ' ) +'</td>';
												
						
						$('table tbody').append('<tr>' + html + '</tr>');
					}
					
					alterarCabecalhoTabelaResolucao();
				}
				else
				{
					$('table tbody').html('<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Nenhum título encontrado.</strong></td></tr>');
				}
			}
		);
		
		// calcular totais
		x.executeSql(
			'SELECT COUNT(titulo_codigo) AS total, SUM(titulo_valor) as valor_total FROM titulos WHERE titulo_codigo > 0 ' + wheres, [], function(x, dados) {
				
	
				var dado = dados.rows.item(0);

				
				$('#total').text(number_format(dado.total, 0, ',', '.'));
				$('#valor_total').text(number_format(dado.valor_total, 3, ',', '.'));
				
				// paginação
				
				$('#paginacao').html('');
				
				var total = ceil(dado.total / 20);
				
				if (total > 1)
				{
					if (sessionStorage['titulos_pagina_atual'] > 6)
					{
						$('#paginacao').append('<a href="#" onclick="obter_produtos(1, \'' + sessionStorage['titulos_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
					}
					
					if (sessionStorage['titulos_pagina_atual'] > 1)
					{
						$('#paginacao').append('<a href="#" onclick="obter_produtos(' + (intval(sessionStorage['titulos_pagina_atual']) - 1) + ', \'' + sessionStorage['titulos_ordem_atual'] + '\'); return false;">&lt;</a> ');
					}
					
					for (i = intval(sessionStorage['titulos_pagina_atual']) - 6; i <= intval(sessionStorage['titulos_pagina_atual']) + 5; i++)
					{
						if (i <= 0 || i > total)
						{
							continue;
						}
						
						if (i == sessionStorage['titulos_pagina_atual'])
						{
							$('#paginacao').append('<strong>' + i + '</strong> ');
						}
						else
						{
							$('#paginacao').append('<a href="#" onclick="obter_produtos(' + i + ', \'' + sessionStorage['titulos_ordem_atual'] + '\'); return false;">' + i + '</a> ');
						}
					}
					
					if (sessionStorage['titulos_pagina_atual'] < total)
					{
						$('#paginacao').append('<a href="#" onclick="obter_produtos(' + (intval(sessionStorage['titulos_pagina_atual']) + 1) + ', \'' + sessionStorage['titulos_ordem_atual'] + '\'); return false;">&gt;</a> ');
					}
					
					if (sessionStorage['titulos_pagina_atual'] <= total - 6)
					{
						$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_produtos(' + total + ', \'' + sessionStorage['titulos_ordem_atual'] + '\'); return false;">Última Página</a> ');
					}
				}
			}
		);
	});
}
