$(document).ready(function() {
	
	//Adiciona o nome do representante como remetente
	$('input[name="nome_real_usuario"]').val(((info.nome_curto_representante)?info.nome_curto_representante:info.nome_representante));
	$('input[name=id_usuario]').val(info.id_rep);
	
	
	var codigo_e_empresa 	= parse_url(location.href).fragment;
	if(codigo_e_empresa){
	
		console.log('IN');
		array_codigo_e_empresa 	= codigo_e_empresa.split('|');
		var codigo_do_pedido 	= array_codigo_e_empresa[0];
		var codigo_da_empresa 	= array_codigo_e_empresa[1];
		var tipo_pedido 		= array_codigo_e_empresa[2];
	
	
		
	
		obter_gestor_responsavel(info.codigo_gestor, function(gestor){
			$('input[name=para]').val(gestor.label);
			$('input[name=codigo_usuario]').val(gestor.id);			
		//	$('input[name=nome_real_usuario]').val((gestor.nome_real ? gestor.nome_real : gestor.nome));
			// Bloquear campo quando selecionar cliente
			$('input[name=para]').attr('disabled', 'disabled');
			$('#trocar_destinatario').hide();
			preencher_campos(codigo_do_pedido, codigo_da_empresa, tipo_pedido );
		});
	
	}else{
		obter_gestor_responsavel(info.codigo_gestor, function(gestor){
			$('input[name=para]').val(gestor.label);
			$('input[name=codigo_usuario]').val(gestor.id);			
		//	$('input[name=nome_real_usuario]').val((gestor.nome_real ? gestor.nome_real : gestor.nome));
			// Bloquear campo quando selecionar cliente
			$('input[name=para]').attr('disabled', 'disabled');
			$('#trocar_destinatario').hide();
			
		});
		
	}
	//Obter destinatarios para autocomplete
	obter_destinatarios();
	
	//-----------------------------
	// INICIO - SALVAR PENDENCIA
	//-----------------------------
	$('form[name=formPendencia]').submit(function() {
		if (!$('input[name=para]').val())
		{
			mensagem('Campo <strong>PARA</strong> deve ser preenchido.');
		}
		else if (!$('input[name=titulo]').val())
		{
			mensagem('Campo <strong>TÍTULO</strong> deve ser preenchido.');
		}
		else if (!$('select[name=prioridade]').val())
		{
			mensagem('Selecione uma <strong>Prioridade</strong>.');
		}
		else if (!$('input[name=prazo_solucao]').val())
		{
			mensagem('Campo <strong>PRAZO PARA SOLUÇÃO</strong> deve ser preenchido.');
		}
		else if (!$('textarea[name=descricao]').val())
		{
			mensagem('Campo <strong>CONTEÚDO</strong> deve ser preenchido.');
		}else{
			db.transaction(function(x) {
				
				//SELECT EM PENDÊNCIAS
				x.executeSql('SELECT id FROM pendencias ORDER BY id DESC LIMIT 1', [], function(x, dados) {
					
					var codigo = uniqid();
					
					var tmp_1 = ['id'];
					var tmp_2 = ['?'];
					var tmp_3 = [codigo];
					
					$('input[name], textarea[name], select[name]').each(function() {
						var campo_nome 	= $(this).attr('name');
						var campo_valor = $(this).val();
						
						
						if(campo_nome == 'prazo_solucao')
						{
							if(campo_valor)
							{
								campo_valor = data_normal2protheus(campo_valor);
							}
						}
						
						tmp_1.push(campo_nome);
						tmp_2.push('?');
						tmp_3.push(campo_valor);
					});
					
					//////////////////////////////////////////////////////
					tmp_1.push('status');
					tmp_2.push('?');
					tmp_3.push('em_aberto');
					
					tmp_1.push('timestamp');
					tmp_2.push('?');
					tmp_3.push(time());
					
					tmp_1.push('latitude');
					tmp_2.push('?');
					tmp_3.push(localStorage.getItem('gps_latitude'));
					
					tmp_1.push('longitude');
					tmp_2.push('?');
					tmp_3.push(localStorage.getItem('gps_longitude'));
					
					tmp_1.push('versao');
					tmp_2.push('?');
					tmp_3.push(config.versao);					
					//////////////////////////////////////////////////////
					
					//-------------------
					//Inserir
					x.executeSql('INSERT INTO pendencias (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')', tmp_3,function(){
						window.location = 'pendencias_aguardando.html';
					});
					//Inserir
					//-------------------
				});
				//SELECT EM PENDÊNCIAS
			});
		}
		
		return false;
	});
	//-----------------------------
	// FIM - SALVAR PENDENCIA
	//-----------------------------
	
	
	$('.cancelar').click(function(e){
		e.preventDefault();
		
		
		confirmar('Deseja cancelar essa pendência?', 
			function () {
				$(this).dialog('close');
		
				//-----------------
				
				sessionStorage[sessionStorage['sessao_tipo']] = '';
				//window.location = 'pendencias.html';
				history.go(-1);
				//-----------------
				
				$("#confirmar_dialog").remove();
				
			}
		);
	});
	
	
});

/**
* Metódo:		obter_destinatarios
* 
* Descrição:	Função Utilizada para buscar os destinatários para pendencias
* 
* Data:			03/09/2013
* Modificação:	03/09/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_destinatarios()
{
	// Obter Pedido Pendentes
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pendencias_usuarios', [], 
			function(x, dados) {
				//Verificar total de itens
				var total_itens = dados.rows.length;				
				//Total de itens
				if (total_itens)
				{
					//Setar variavel
					var destinatario = [];
					//Varrer itens
					for(i = 0; i < total_itens; i ++)
					{
						//Pegar indeice
						var usuario = dados.rows.item(i);
						
						//Destinatario
						destinatario.push({
							label: (usuario.codigo ? usuario.codigo : usuario.id)+' - '+(usuario.nome_real ? usuario.nome_real : usuario.nome),
							id: usuario.id,
							codigo: usuario.codigo,
							nome_real: usuario.nome_real,
							nome: usuario.nome
						});
					}
					
					//Adicionar destinatários
					buscar_destinatarios(destinatario);
				}				
			}
		)
	});	
	
	/**
	* 
	* ID:			trocar_destinatario
	*
	* Descrição:	Utilizado para apagar todos os valores dos campos ligados
	*
	*/
	$('#trocar_destinatario').live('click', function(){
		
		limpar_dados_destinatario();
		
	});
}
/**
* Metódo:		buscar_destinatarios
* 
* Descrição:	Função Utilizada para pesquisar os destinatarios pelo autocomplete
* 
* Data:			03/09/2013
* Modificação:	03/09/2013
* 
* @access		public
* @param		array 			var destinatario		- Todos os destinatários
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_destinatarios(destinatario)
{
	$('input[name=para]').autocomplete({
		minLength: 3,
		source: destinatario,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {

			$('input[name=para]').val(ui.item.label);
			$('input[name=codigo_usuario]').val(ui.item.id);
			//$('input[name=nome_real_usuario]').val((ui.item.nome_real ? ui.item.nome_real : ui.item.nome));
			// Bloquear campo quando selecionar cliente
			$('input[name=para]').attr('disabled', 'disabled');
			$('#trocar_destinatario').show();
			
			return false;
			
		}
	});
}

/**
* Metódo:		limpar_dados_destinatario
* 
* Descrição:	Função Utilizada para apagar todos os campos ligados ao destinatario
* 
* Data:			03/09/2013
* Modificação:	03/03/2013
* 
* @access		public

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function limpar_dados_destinatario()
{
	$('input[name="para"]').val("");
	$('input[name="codigo_usuario"]').val("");
	//$('input[name="id_usuario"]').val("");
	//$('input[name="nome_real_usuario"]').val("");
	
	$('#trocar_destinatario').hide();
	$('input[name="para"]').removeAttr('disabled');	
}
function preencher_campos(codigo_do_pedido, codigo_da_empresa, tipo ){
	
	var titulo = '';
	var descricao = '';
	if (tipo == 'P'){
		titulo = 'Solicitação de Alteração de Pedido - Código '+codigo_do_pedido+' Filial: '+ codigo_da_empresa;
		descricao += 'Código: '+codigo_do_pedido+"\n";
		descricao += 'Filial: '+codigo_da_empresa+"\n";
	}else if(tipo == 'C'){
		titulo = 'Solicitação de Alteração do Cliente Código '+codigo_do_pedido+' Loja: '+ codigo_da_empresa;
		descricao += 'Código: '+codigo_do_pedido+"\n";
		descricao += 'Loja: '+codigo_da_empresa+"\n";
	}
	$("#titulo").val(titulo);
	$("#descricao").html(descricao);
}
/**
* Metódo:		obter_gestor_responsavel
* 
* Descrição:	Função Utilizada para buscar o gestor responsável pelo representante
                Hoje todos os represntantes utilizam o mesmo gestor, assim que for alterado para gestor por região por exemplo, basta alterar apenas nessa função
*    
* Data:			10/07/2014
* Modificação:	10/07/2014
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_gestor_responsavel(codigo_gestor, callback)
{

	console.log('Gestor: '+ codigo_gestor);

	// Obter Pedido Pendentes
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pendencias_usuarios  WHERE codigo = ?', [codigo_gestor], 
			function(x, dados) {
				//Verificar total de itens
				var total_itens = dados.rows.length;				
				//Total de itens
				console.log('itens: '+ total_itens);
				if (total_itens)
				{
					//Setar variavel
					var gestor_responsavel ;
					//Varrer itens
										
					var usuario = dados.rows.item(0);
						
						//Destinatario
						gestor_responsavel={
							label: (usuario.codigo ? usuario.codigo : usuario.id)+' - '+(usuario.nome_real ? usuario.nome_real : usuario.nome),
							id: usuario.id,
							codigo: usuario.codigo,
							nome_real: usuario.nome_real,
							nome: usuario.nome
						};
								
					callback(gestor_responsavel);
					
				}				
			}
		)
	});	
	
	}