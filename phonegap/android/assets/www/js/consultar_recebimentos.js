$(document).ready(function() {
	$('#nome_representante').html('Representante '+info.nome_representante) ; 
	// obter produtos
	
	obter_recebimentos(sessionStorage['recebimentos_pagina_atual'], sessionStorage['recebimentos_ordem_atual']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_recebimentos(sessionStorage['recebimentos_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_recebimentos(sessionStorage['recebimentos_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['notas_' + name] != 'undefined' ? sessionStorage['notas_' + name] : '');
	});
	
	
	
	
	
	$('#filtrar').click(function() {
		sessionStorage['recebimentos_pagina_atual'] = 1;
				
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['' + name] = $(this).val();
		});
		
		obter_recebimentos(sessionStorage['recebimentos_pagina_atual'], sessionStorage['recebimentos_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['recebimentos_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['notas_' + name] = '';
		});
		
		obter_recebimentos(sessionStorage['recebimentos_pagina_atual'], sessionStorage['recebimentos_ordem_atual']);
	});
	
	
	//-- #ver_titulos -  esta função esta no arquivo geral.js
	//$('#ver_titulos')
	
});

function obter_recebimentos(pagina, ordem)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'data_periodo ASC';
	
	// setar pagina e ordem atual
	sessionStorage['recebimentos_pagina_atual'] = pagina;
	sessionStorage['recebimentos_ordem_atual'] = ordem;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['recebimentos_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'ASC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'DESC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['recebimentos_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	
	var wheres = '';
	
	console.log('SELECT * FROM recebimentos WHERE codigo_representante != "" ' + wheres + ' ORDER BY ' + sessionStorage['recebimentos_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM recebimentos WHERE codigo_representante != "" ' + wheres + ' ORDER BY ' + sessionStorage['recebimentos_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
			
				if (dados.rows.length)
				{
					$('table tbody').empty();
			
					for (i = 0; i < dados.rows.length; i++)
					{
						var item 				= dados.rows.item(i);				
						var data_periodo 		= item.data_periodo ? item.data_periodo: 'N/A';
						var valor 				= item.valor ? number_format(item.valor,2,',','.'): 'N/A';
						var saldo				= item.saldo ? item.saldo: 'N/A';
					
								
						var html = '';						
							
							
							html += '<td align="center">'+protheus_data2data_mesano(data_periodo)+'</td>';
							html += '<td align="right">'+valor+'</td>';
							html += '<td align="right">'+number_format(saldo,2,',','.')+'</td>';
							
						$('table tbody').append('<tr>' + html + '</tr>');
					}
					
					
					var somar = 'SUM(valor) AS valor_total, ';
					somar += 'SUM(saldo) AS saldo_total ';
							
					console.log('SELECT  '+somar+' FROM recebimentos WHERE rnum > 0 ' + wheres);
						
					// calcular totais
					x.executeSql(
						'SELECT  '+somar+' FROM recebimentos WHERE codigo_representante != "" ' + wheres, [], function(x, dados) {
							
							var dado = dados.rows.item(0);
							//$('#valor_total').text(number_format(dado.valor_total, 3, ',', '.'));
							//$('#saldo_total').text(number_format(dado.saldo_total, 3, ',', '.'));
							
							
							$('table tbody').append('<tr><td align="left">Total</td><td align="right">'+number_format(dado.valor_total, 3, ',', '.')+'</td><td  align="right">'+number_format(dado.saldo_total, 3, ',', '.')+'</td></tr>');
							
						}
					);
					
					
					
					
					alterarCabecalhoTabelaResolucao();
				
				}
				else
				{
					$('table tbody').html('<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhuma comissão encontrada.</strong></td></tr>');
				}
			}
		);
		
		
		
	});
}
