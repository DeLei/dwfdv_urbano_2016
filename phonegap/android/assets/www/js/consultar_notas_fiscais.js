$(document).ready(function() {
	// obter produtos
	
	obter_produtos(sessionStorage['notas_pagina_atual'], sessionStorage['notas_ordem_atual']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_produtos(sessionStorage['notas_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_produtos(sessionStorage['notas_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['notas_' + name] != 'undefined' ? sessionStorage['notas_' + name] : '');
	});
	
	// obter filiais
	obter_filiais_permitidas(function(filiais){	
		$('select[name=nota_filial]').append('<option value="">Todos</option>');			
		$.each(filiais, function(key, dado) {			
				$('select[name=nota_filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
		});	
	});
	
	
	$('#filtrar').click(function() {
		sessionStorage['notas_pagina_atual'] = 1;
				
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['notas_' + name] = $(this).val();
		});
		
		obter_produtos(sessionStorage['notas_pagina_atual'], sessionStorage['notas_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['notas_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['notas_' + name] = '';
		});
		
		obter_produtos(sessionStorage['notas_pagina_atual'], sessionStorage['notas_ordem_atual']);
	});
	
	
	//-- #ver_titulos -  esta função esta no arquivo geral.js
	//$('#ver_titulos')
	
});

function obter_produtos(pagina, ordem)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'nota_filial ASC';
	
	// setar pagina e ordem atual
	sessionStorage['notas_pagina_atual'] = pagina;
	sessionStorage['notas_ordem_atual'] = ordem;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['notas_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'ASC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'DESC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['notas_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	filial 					= sessionStorage['notas_nota_filial'];
	codigo 					= sessionStorage['notas_nota_codigo'];
	codigo_pedido			= sessionStorage['notas_item_pedido_codigo'];
	codigo_cliente			= sessionStorage['notas_nota_codigo_cliente'];
	nome_cliente 			= sessionStorage['notas_cliente_nome'];	
	nota_emissao_inicial	= sessionStorage['notas_nota_emissao_inicial'];	
	nota_emissao_final		= sessionStorage['notas_nota_emissao_final'];
	transportadora_nome		= sessionStorage['notas_transportadora_nome'];
	
	var wheres = '';
	
	if (filial && filial != 'undefined')
	{
		wheres += " AND nota_filial = '" + filial + "'";
	}
	
	if (codigo && codigo != 'undefined')
	{
		wheres += " AND nota_codigo = '" + codigo + "'";
	}
	
	if (codigo_pedido && codigo_pedido != 'undefined')
	{
		wheres += " AND item_codigo_pedido = '" + codigo_pedido + "'";
	}

	if (nome_cliente && nome_cliente != 'undefined')
	{
		wheres += " AND cliente_nome LIKE '%" + nome_cliente + "%'";
	}
	
	if (nota_emissao_inicial && nota_emissao_inicial != 'undefined')
	{
		var ex = explode('/', nota_emissao_inicial);
		wheres += " AND nota_data_emissao >= '"+ ex[2]+ex[1]+ex[0] +"'";
	}
	
	if (nota_emissao_final && nota_emissao_final != 'undefined')
	{
		var ex = explode('/', nota_emissao_final);
		wheres += " AND nota_data_emissao <= '" + ex[2]+ex[1]+ex[0] + "'";
	}
	
	if (transportadora_nome && transportadora_nome != 'undefined')
	{
		wheres += " AND transportadora_nome LIKE '%" + transportadora_nome + "%'";
	}
	
	if (info.empresa)
	{
		wheres += " AND empresa = '" + info.empresa + "'";
	}
	
	console.log('SELECT * FROM notas_fiscais WHERE nota_codigo > 0 ' + wheres + ' GROUP BY nota_codigo, nota_serie ORDER BY ' + sessionStorage['notas_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM notas_fiscais WHERE nota_codigo > 0 ' + wheres + ' GROUP BY nota_codigo, nota_serie ORDER BY ' + sessionStorage['notas_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
			
				if (dados.rows.length)
				{
				
					
					$('table tbody').empty();
			
					for (i = 0; i < dados.rows.length; i++)
					{
						var item 					= dados.rows.item(i);				
						var frete 					= item.nota_tipo_frete ? 'CIF' : 'FOB';
						var transportadora 			= item.transportadora_nome ? item.transportadora_nome: '';
						var transportadora_telefone = transportadora_telefone ? transportadora_telefone : '';
						
						var html = '';						
							html += '<td align="center">'+item.nota_codigo+'/'+item.nota_serie+'</td>';
							html += '<td align="center">'+item.item_codigo_pedido+'</td>';
							html += '<td align="right"><a href="clientes_visualizar.html#'+item.cliente_codigo+'_'+item.cliente_loja+'">'+item.cliente_nome+'</a></td>';
							html += '<td align="center">'+protheus_data2data_normal(item.nota_data_emissao)+'</td>';					
							html += '<td align="right">'+number_format(item.nota_valor_mercadoria, 3, ',', '.')+'</td>';
							html += '<td align="right">'+number_format(item.nota_valor_icms, 3, ',', '.')+'</td>';
							html += '<td align="right">'+number_format(item.nota_valor_ipi, 3, ',', '.')+'</td>';
							html += '<td align="center">'+frete+'</td>';
							html += '<td>'+transportadora+'</td>';
							html += '<td>'+transportadora_telefone+'</td>';
							html += '<td><a href="#" id="ver_titulos" data-serie="'+item.nota_serie+'" data-codigo="'+item.nota_codigo+'">Ver Títulos</a> | <a href="notas_fiscais_visualizar.html#'+item.nota_codigo+'_'+item.nota_serie+'_'+item.nota_filial+'">Ver Itens</a></td>';
						
						$('table tbody').append('<tr>' + html + '</tr>');
					}
					
					alterarCabecalhoTabelaResolucao();
				
				}
				else
				{
					$('table tbody').html('<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhuma nota fiscal encontrada.</strong></td></tr>');
				}
			}
		);
		
		
		var somar = 'SUM(item_preco_unitario*item_quantidade_entrega) AS valor_total, ';
			somar += 'SUM(item_valor_icms) AS valor_total_icms, ';
			somar += 'SUM(item_ipi) AS valor_total_ipi ';
			
			console.log('SELECT COUNT(DISTINCT(codigo_generico)) AS total, '+somar+' FROM notas_fiscais WHERE nota_codigo > 0 ' + wheres);
			
		// calcular totais
		x.executeSql(
			'SELECT COUNT(DISTINCT(codigo_generico)) AS total, '+somar+' FROM notas_fiscais WHERE nota_codigo > 0 ' + wheres, [], function(x, dados) {
				
				var dado = dados.rows.item(0);
				
				console.log(dado);
				
				$('#total').text(number_format(dado.total, 0, ',', '.'));
				$('#valor_total').text(number_format(dado.valor_total, 3, ',', '.'));
				
				$('#valor_total_icms').text(number_format(dado.valor_total_icms, 3, ',', '.'));
				$('#valor_total_ipi').text(number_format(dado.valor_total_ipi, 3, ',', '.'));
				
				// paginação
				
				$('#paginacao').html('');
				
				var total = ceil(dado.total / 20);
				
				if (total > 1)
				{
					if (sessionStorage['notas_pagina_atual'] > 6)
					{
						$('#paginacao').append('<a href="#" onclick="obter_produtos(1, \'' + sessionStorage['notas_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
					}
					
					if (sessionStorage['notas_pagina_atual'] > 1)
					{
						$('#paginacao').append('<a href="#" onclick="obter_produtos(' + (intval(sessionStorage['notas_pagina_atual']) - 1) + ', \'' + sessionStorage['notas_ordem_atual'] + '\'); return false;">&lt;</a> ');
					}
					
					for (i = intval(sessionStorage['notas_pagina_atual']) - 6; i <= intval(sessionStorage['notas_pagina_atual']) + 5; i++)
					{
						if (i <= 0 || i > total)
						{
							continue;
						}
						
						if (i == sessionStorage['notas_pagina_atual'])
						{
							$('#paginacao').append('<strong>' + i + '</strong> ');
						}
						else
						{
							$('#paginacao').append('<a href="#" onclick="obter_produtos(' + i + ', \'' + sessionStorage['notas_ordem_atual'] + '\'); return false;">' + i + '</a> ');
						}
					}
					
					if (sessionStorage['notas_pagina_atual'] < total)
					{
						$('#paginacao').append('<a href="#" onclick="obter_produtos(' + (intval(sessionStorage['notas_pagina_atual']) + 1) + ', \'' + sessionStorage['notas_ordem_atual'] + '\'); return false;">&gt;</a> ');
					}
					
					if (sessionStorage['notas_pagina_atual'] <= total - 6)
					{
						$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_produtos(' + total + ', \'' + sessionStorage['notas_ordem_atual'] + '\'); return false;">Última Página</a> ');
					}
				}
			}
		);
	});
}
