$(document).ready(function() {
	$.ajaxSetup({
		timeout: 120 * 1000
	});	
	sincronizar_peds();
});


function sincronizar_peds()
{
	$('#carregando').find('span:first').html('Enviado pedidos...');
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pedidos WHERE exportado = 0 AND codigo != "" AND orc = 0', [], function(x, dados) {
				var total = dados.rows.length;

				if (!total)
				{
					exibirMensagem('dialog_lembrar', 'Pedidos enviados com sucesso.', "window.location = 'pedidos_analisados.html'", 'Atenção');
				
				}
				else
				{
					$('progress').attr('value', 0);
					
					var tmp = 0;
					
					for (var i = 0; i < total; i++)
					{
						// o sleep previne o bloqueio de muitas requisições
						sleep(0.5);
						
						var dado = dados.rows.item(i);
						
						
						var ajax = $.ajax({
							url: config.ws_url + '/importar_peds_e_orcs',
							type: 'POST',
							data: {
								pedido: serialize({
									versao: config.versao,
									unidade: dado.unidade,
									id_do_representante: info.id_rep,
									codigo_do_representante: info.cod_rep,
									timestamp: dado.timestamp,
									codigo: dado.codigo,
									codigo_do_cliente: dado.codigo_do_cliente,
									loja_do_cliente: dado.loja_do_cliente,
									codigo_da_tabela_de_preco: dado.codigo_da_tabela_de_preco,
									codigo_do_produto: dado.codigo_do_produto,
									quantidade: dado.quantidade,
									preco: dado.preco,
									total_sem_ipi: dado.total_sem_ipi,
									valor_do_frete_do_pedido: dado.valor_do_frete_do_pedido,
									codigo_da_forma_de_pagamento: dado.codigo_da_forma_de_pagamento,
									tipo_de_frete: dado.tipo_de_frete,
									codigo_da_transportadora: dado.codigo_da_transportadora,
									id_do_evento: dado.id_do_evento,
									ordem_de_compra: dado.ordem_de_compra,
									data_de_entrega: dado.data_de_entrega,
									observacao: dado.observacao,
									desconto: dado.desconto,
									valor_st: dado.valor_st,
									valor_total_do_pedido: dado.valor_total_do_pedido,
									cultura: dado.cultura,
									tipo_de_pedido: dado.tipo_de_pedido,
									pedido_autorizado: dado.pedido_autorizado,
									orc: dado.orc,
									id_pro: dado.id_pro,
									raz_soc_pro: dado.raz_soc_pro,
									nro_itens: dado.nro_itens
								})
							},
							dataType: 'jsonp',
							success: function(dados) {
								tmp++;
								
								db.transaction(function(x) {
									x.executeSql('DELETE FROM pedidos WHERE codigo = ? AND codigo_do_produto = ? AND codigo_da_tabela_de_preco = ?', [dados.pedidos[0].codigo, dados.pedidos[0].codigo_do_produto, dados.pedidos[0].codigo_da_tabela_de_preco]);
								});
								
								$('progress').attr('value', round(((i / total) * 100)));
								
								if (tmp == total)
								{
									apprise('Pedidos enviados com sucesso.', {}, function (dado) {
										window.location = 'pedidos_analisados.html';
									});
								}
							},
							timeout: 120 * 1000
						});
						
						ajax.fail(function () {
							apprise('Não foi possível enviar todos os pedidos. Você deseja tentar novamente?', {
								'verify': true,
								'textYes': 'Sim',
								'textNo': 'Não'
							}, function (dado) {
								if (dado)
								{
									window.location = 'enviar_pedidos.html';
								}
								else
								{
									window.location = 'pedidos_analisados.html';
								}
							});
						});
					}
				}
			}
		);
	});
}