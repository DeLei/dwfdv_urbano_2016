/**
* Met�do:		obter_filiais_permitidas
* 
* Descri��o:	Fun��o Utilizada para retornar as filiais permitidas do representante.
Se o representante possui acesso apenas a filial 01 , n�o exibir as demais para sele��o.
* 
* Data:			03/07/2014
* Modifica��o:	03/07/2014
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Solu��es Web
* 
*/
function obter_filiais_permitidas( callback)
{
	//Obtem somente as filiais em que o representante/clientes possuem tabela de pre�o
	var sql = '';
	sql += 'SELECT filial_saida ';
	sql += 'FROM clientes ';
	sql += 'JOIN tabelas_preco ON (tabelas_preco.codigo = clientes.tabela_preco OR tabelas_preco.vinculo_representante = "' + localStorage.getItem('codigo') + '") ';
	sql += 'WHERE (clientes.codigo_representante = "' + localStorage.getItem('codigo') + '" OR clientes.codigo_representante2 = "' + localStorage.getItem('codigo') + '") ';
	sql += 'GROUP BY filial_saida';
	
	db.transaction(function(x) {
		x.executeSql(sql, [], function(x, dados){
			var total = dados.rows.length;
			var filiais_permitidas = new Array();
			if(total > 0)
			{
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					
					filiais_permitidas[i] = '"' + dado.filial_saida + '"';
				}
			}
			filiais_permitidas = implode(', ', filiais_permitidas);
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM filiais WHERE codigo IN (' + filiais_permitidas + ')', [], function(x, dados) {
					var total = dados.rows.length;
					var filiais = [];
					
					for(i = 0; i < total; i++)
					{
						var dado = dados.rows.item(i);
						var filial = {
							"codigo": dado.codigo,
							"razao_social": dado.razao_social
						
						};
						
						filiais.push(filial);
						
						
					}
					callback(filiais);
										
				});
			});
			
		})
	});
}