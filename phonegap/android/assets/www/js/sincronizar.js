//------------------------------------------------------------------------//
//------------------------------ MÉTODOS ---------------------------------//
//------------------------------------------------------------------------//


/**
* Metódo:		sincronizar_modulo
* 
* Descrição:	Função Utilizada para chamar o modulo que será sincronizado
* 
* Data:			17/09/2013
* Modificação:	17/09/2013
* 
* @access		public
* @param		string 		$modulo					- Modulo que sera sincronizado
* @param		string 		$descricao_modulo		- Descrição do modulo que será exibida no carregando
* @param		string 		$proxima_modulo			- Proximo modulo que ser asincronizado quando finalizar o primeiro
* @param		array 		$parametros				- array para customização no ws
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function sincronizar_modulo(modulo, descricao_modulo, proxima_modulo, parametros)
{
	
		// Salvando informações das sincronizações
		db.transaction(function(x) {
			x.executeSql('INSERT INTO sincronizacoes (timestamp, modulo, sucesso) VALUES (?, ?, ?)', [time(), modulo, 0]);
		});
	
		// Carregando
		$('#carregando').show();
		$('#btnCancelar').show();
		$('#btnSincronizar').hide();
		$('#carregando').find('span:first').html('Sincronizando ' + descricao_modulo + '…');
	
		//----------------------------------------------------------------------------------------------
		// Chamando o total de pacotes via ajax, e depois sincronizar dados do pacote
		//---------------------------------------------------------
		
		console.log("Modulo: "+ modulo);
		var ajax;
		
		if(modulo != 'pendencias'  && modulo != 'pedidos_pendentes'  && modulo != 'pendencias_usuarios' && modulo != 'pendencias_mensagens' && modulo != 'prospects' )
		{
			ajax = $.ajax({
				url: localStorage.getItem('caminho_local') + modulo + '_total.json',
				success: function(dados) {			
					
					var total_pacotes = dados.total;
					
					//-----------
					
					db.transaction(function(x) {
						x.executeSql('SELECT * FROM ' + modulo, [],
						function(x, dados){
						
							// A tabela existe, entao apaga os dados e contia a sincronização
						
							
							//1º Tentamos deletar os dados exportados das tabelas (Como somente as tabelas de exportação tem os campos "exportado", ira ocorrer um erro de sql)
							//2ª Se ocorrer um erro de SQL, vamos deletar os dados das tabelas que não existem os campos "exportado" 
							
							
							var where = '';
						    
							console.log('Módulo: '+modulo);
						    console.log(dados);
							//console.log(JSON.stringify(dados));
							
							// Alterada função para que seja excluído se o pedido retornar da ZB7
						    if(modulo == 'pedidos_pendentes' || modulo == 'orcamentos') {
						    	//Exclui somente os pedidos que já estão na SC5
						    	where = ' AND lower(pedido_id_pedido) IN (SELECT lower(pedido_codigo_pedido_portal) FROM pedidos_processados GROUP BY pedido_codigo_pedido_portal)';
						    	
						    }
						    console.log('|| - - - - - - - - - - - - - ');
						    console.log('DELETE FROM ' + modulo + ' WHERE exportado = \'1\' ' + where);
						    
						    
						    x.executeSql('DELETE FROM ' + modulo + ' WHERE exportado = \'1\' ' + where, [],
									
						    function(){
								console.log('--==-- Apagando dados antigos do modulo ' + modulo + ' (EXPORTADO)');
								sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
							},
							function(){
								x.executeSql('DELETE FROM ' + modulo, [], function(){
									console.log('--==-- Apagando dados antigos do modulo ' + modulo);
									sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
								});
							});
							
						},
						function(){ // Erro de SQL, irá continuar a sincronização normalmente
							sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
						});
					});
					
				},
				error: function(){
					console.log("ERRO NA REQUISIÇÃO AJAX -> Módulo: "+descricao_modulo+" -> PROXIMA CHAMADA");
					
					//Erro na requisição
					apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível se conectar ao servidor para realizar o envio: <b><br /></b><br />Tente novamente mais tarde.', {
						'textYes': 'OK'
					}, function (dado) {
						
						
						reiniciar_instalacao();
					});
				}
			
			});
			
		} else {
			
			var codigo_usuario = localStorage.getItem('id');
			var codigo_representante = localStorage.getItem('codigo');
			
			ajax = $.ajax({
				url: config.ws_url + modulo + '/total?id_usuario='+codigo_usuario+'&codigo_representante='+codigo_representante+'&gerar_pacote=1',
				success: function(dados) {
					
					var total_pacotes = dados.total;
					
					db.transaction(function(x) {
						// A tabela existe, entao apaga os dados e contia a sincronização
						x.executeSql('SELECT * FROM ' + modulo, [], function(x, dados){
							
							//1º Tentamos deletar os dados exportados das tabelas (Como somente as tabelas de exportação tem os campos "exportado", ira ocorrer um erro de sql)
							//2ª Se ocorrer um erro de SQL, vamos deletar os dados das tabelas que não existem os campos "exportado" 
							
							var where = '';
						    if(modulo == 'pedidos_pendentes' || modulo == 'orcamentos') {
							    //Exclui somente os pedidos que já estão na SC5
							    //where = ' AND lower(pedido_id_pedido) IN (SELECT lower(pedido_codigo_pedido_portal) FROM pedidos_processados GROUP BY pedido_codigo_pedido_portal)';
						    }
						    
						    x.executeSql('DELETE FROM ' + modulo + ' WHERE exportado = \'1\' ' + where, [], function(){
								console.log('--==-- Apagando dados antigos do modulo ' + modulo + ' (EXPORTADO)');
								sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
							}, function(){
								x.executeSql('DELETE FROM ' + modulo, [], function(){
									console.log('--==-- Apagando dados antigos do modulo ' + modulo);
									sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
								});
							});
							
						},
						function(){ // Erro de SQL, irá continuar a sincronização normalmente
							sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
						});
					});
					
				},
				error: function(){
					console.log("ERRO NA REQUISIÇÃO AJAX -> Módulo: "+descricao_modulo+" -> PROXIMA CHAMADA");
					
					reiniciar_instalacao();
					
					//Erro na requisição
					
				}
			
			});
		}
}

/**
* Metódo:		verificar_campos
* 
* Descrição: 		Verifica se os campos retornados pelo WS existem no banco de dados local
* 					se não existir gerar SQL ALTER e cria os campos todos para tipo TEXT.
* 
* Data:			16/04/2013
* Modificação:	16/04/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		json 		$dados					- dados retornados do json
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		number 		$percentagem_pacotes	- percentagem total de pacotes
* @param		string 		$proxima_funcao			- funcao que será executada quando finalizar
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, inserir)
{
	db.transaction(function(x) {
		
		var total_itens = dados.length;
		var item_atual 	= 0;
		var campos 		= [];
		
		//--------------------
		$.each(dados, function(i, objeto) {
			
			
			//--------------------
			$.each(objeto, function(indice, valor) {
				
				if(indice != 'rownum') 
				{
					campos.push(indice);
				}
				
			});
			//--------------------
			
			return false;
		});
		
		
		console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
		console.log('TABELA: '+ tabela);
		console.log('CAMPOS: '+ campos.join(', '));
		console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
		
		
		x.executeSql('SELECT ' + campos.join(', ') + ' FROM ' + tabela, [], function(){
			if(inserir)
			{
				inserir_dados(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao);
			}
			else
			{
				finalizar_sincronizacao(tabela, proxima_funcao);
			}
			
		},
		function(xe, erro)
		{
			if(erro.code == 5)
			{
				var mensagem_erro = erro.message;
				var retorno = mensagem_erro.split(":");
				var campo = trim(retorno[1]);
				
				if(campo)
				{
					console.log(' -- ERRO COLUNA -- ');
					console.log('Coluna não existe: ' + campo);
					
					x.executeSql('ALTER TABLE ' + tabela + ' ADD ' + campo + ' TEXT', [], function () {
						console.log('Tabela: ' + tabela + ' SQL: ' + campo + ' Tabela alterada com sucesso.');
						
						verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, inserir);

						
					}, function(xy, error){
						console.log('Tabela: ' + tabela + ' SQL: ' + campo + ' Não foi possível alterar: ' + error.message);
					});
					
					console.log(' -- FIM -- ');
					
				}
				else
				{
					console.log(erro);
				}
			}
			
		});

	});
}

/**
* Metódo:		inserir_dados
* 
* Descrição:	Função Utilizada para inserir os dados json na tabela SQLLITE
* 
* Data:			16/04/2013
* Modificação:	16/04/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		json 		$dados					- dados retornados do json
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		number 		$percentagem_pacotes	- percentagem total de pacotes
* @param		string 		$proxima_funcao			- funcao que será executada quando finalizar
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function inserir_dados(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao)
{
	
	
	var total_itens = dados.length;
	var item_atual = 0;
	
	

	db.transaction(function(x) {
	
		$.each(dados, function(i, objeto) {
			var campos 			= [];
			var interrogacoes  	= [];
			var valores		 	= [];
		
			
		
			$.each(objeto, function(indice, valor) {
				
				//Formatar telefone
				/*if(indice == 'telefone' || indice == 'telex' || indice == 'fax')
				{
					if(valor)
					{
						valor = formatar_telefone(valor);
					};
				}*/
				
				if(indice != 'rownum') //o campo rownum, vem automatico por conta do LIMIT no sqlsrv, entao por padrao, vamos remover esse retorno
				{
					campos.push(indice);
					interrogacoes.push('?');
					valores.push(valor);
				}
				
			});
			
			//console.log('@@@@#####################################################@@@@');
			//console.log('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')');
			//console.log('@@@@#####################################################@@@@');
			
			//console.log('@@@@@@');
			//console.log('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (\'' + valores.join('\', \'') + '\')');

			
	
			
			x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(a1, a2){
	
				item_atual++;
				
				
				var percontagem_item = percentagem_pacotes / total_itens;
				
				$( "#progressbar" ).progressbar({value: parseFloat($('#progressbar').attr('aria-valuenow')) + percontagem_item});

				/*
				if(tabela == 'pedidos_processados' && item_atual > 2)
				{
					console.log('A1------------------------------');
					console.log(JSON.stringify(a1));
					console.log('A2------------------------------');
					console.log(JSON.stringify(a2));
					console.log('stop execution');
					//throw "stop execution";
				}
				*/
			
				
				if(total_itens == item_atual)
				{
					console.log('Pacote ' + pacote + ' foi finalizado!');
					
					$( "#progressbar" ).progressbar({value: round(percentagem_pacotes * pacote)});
				
					if(pacote < total_pacotes)
					{
						pacote++;
						sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao);
					}
					else
					{
						finalizar_sincronizacao(tabela, proxima_funcao);
					}
				}
				
			},
			function(x, erro){
				console.log(' ----- ');
				console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
				console.log(erro);
				console.log('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ") VALUES ('" + valores.join("', '") + "');");
				console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
				console.log(' ----- ');
			});
			
			

			
		});
		

	});

	
}


/**
* Metódo:		sincronizar_dados
* 
* Descrição:	Função Utilizada para salvar dados do web service no banco do navegador
* 
* Data:			17/09/2013
* Modificação:	17/09/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		array 		$parametros				- array para customização no ws
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao, parametros)
{

	var percentagem_pacotes = 100 / total_pacotes; //Calculo de porcentagem por pacote
	if(pacote == 1)
	{
		$( "#progressbar" ).progressbar({value: 0});
	}
	
	//Mensagem no console
	console.log('Sincronizando ' + tabela + ' | Pacote = ' + pacote + ' | Total de Pacotes = ' + total_pacotes);
	
	var ajax;

	
	if(tabela != 'pendencias'  && tabela != 'pedidos_pendentes'  && tabela != 'pendencias_usuarios' && tabela != 'pendencias_mensagens' && tabela != 'prospects' ){
		 ajax = $.ajax({
			url: localStorage.getItem('caminho_local') + tabela + '_' + pacote + '.json',
			data: parametros,
			success: function(dados) {
				
				// Criar Tabela
				criar_tabela(tabela, dados, true);
				
				// Salvar Dados
				if(verificar_dados(dados))
				{
					verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, true);
				}
				else
				{
					verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, false);				
				}
			},
			error: function(){
				console.log("ERRO NA REQUISIÇÃO AJAX -> Módulo: "+tabela+" -> PROXIMA CHAMADA");
				
				reiniciar_instalacao();
			}
		});
	}else{
		
		var codigo_usuario = localStorage.getItem('id');
		var codigo_representante = localStorage.getItem('codigo');
		
		console.log('alternativa: '+ tabela);
		
		ajax = $.ajax({
			
			//pendencias_mensagens/exportar?id_usuario=30&codigo_representante=121&pacote=1&total_pacotes=1&gerar_pacote=1
			url: config.ws_url +tabela+'/exportar?id_usuario='+codigo_usuario+'&codigo_representante='+codigo_representante+'&pacote='+pacote+'&total_pacotes='+total_pacotes+'&gerar_pacote=1',
			data: parametros,
			success: function(dados) {
				
				// Criar Tabela
				criar_tabela(tabela, dados, true);
				
				// Salvar Dados
				if(verificar_dados(dados))
				{
					
					verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, true);
				
					
				}
				else
				{
					verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, false);				
				}
				
				
				
			},
			error: function(falha){
				console.log(falha);
				console.log("ERRO NA REQUISIÇÃO AJAX -> Módulo: "+descricao_modulo+" -> PROXIMA CHAMADA");
				//Erro na requisição
				reiniciar_instalacao();
			}
		});
		
		

		
	}
	
	ajax.fail(function () {
		apprise('A sincronização não foi totalmente concluída. Você deseja tentar novamente?', {
			'verify': true,
			'textYes': 'Sim',
			'textNo': 'Não'
		}, function (dado) {
			if (dado)
			{
				window.location = 'sincronizar.html';
			}
			else
			{
				window.location = 'index.html';
			}
		});
	});
	
}

/**
* Metódo:		finalizar_sincronizacao
* 
* Descrição:	Função Utilizada para finalizar um pacote, e chamar a proxima funcao
* 
* Data:			22/09/2013
* Modificação:	22/09/2013
* 
* @access		public
* @param		string 		$tabela							- Tabela que esta sendo sincronizada para ser exibida no console
* @param		string 		$proxima_funcao					- Proxima funcao que sera chamada quando finalziar a sincronização de um pacote
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function finalizar_sincronizacao(tabela, proxima_funcao)
{

	
	console.log('Sincronização Tabela: '+tabela);
	
	var modulos = new Object();
		modulos['empresas'] 							= 'Importação de Empresas';
		modulos['filiais'] 								= 'Importação de Filiais';
		modulos['exportacao_prospects'] 				= 'Exportação de Prospects';
		modulos['prospects'] 							= 'Importação de Prospects';
		modulos['exportacao_historico_prospects'] 		= 'Exportação de Históricos de Prospects';
		modulos['historico_prospects'] 					= 'Importação de Históricos de Prospects';
		modulos['clientes'] 							= 'Importação de Clientes';
		modulos['exportacao_historico_clientes'] 		= 'Exportação de Históricos de Clientes';
		modulos['historico_clientes'] 					= 'Importação de Históricos de Clientes';
		modulos['tabelas_preco'] 						= 'Importação de Tabelas de Preço';
		modulos['produtos'] 							= 'Importação de Produtos';
		modulos['formas_pagamento'] 					= 'Importação de Formas de Pagamento';
		modulos['formas_pagamento_real'] 				= 'Importação de Formas de Pagamento';
		modulos['condicoes_pagamento'] 					= 'Importação de Condições de Pagamento';
		modulos['transportadoras'] 						= 'Importação de Transportadoras';
		
		// chamado 480
		
			//modulos['bancos']							= 'Importação de Bancos';
		
		// fim chamado 480
		
		modulos['eventos'] 								= 'Importação de Eventos';
		modulos['municipios'] 							= 'Importação de Municípios';
		modulos['pedidos_processados'] 					= 'Importação de Pedidos Processados';
		modulos['pedidos_pendentes'] 					= 'Importação de Pedidos Pendentes';
		modulos['orcamentos'] 							= 'Importação de Orçamentos';
		modulos['exportacao_orcamentos'] 				= 'Exportação de Orçamentos';
		modulos['exportacao_pedidos_pendentes'] 		= 'Exportação de Pedidos';
		modulos['titulos'] 								= 'Importação de Títulos';
		modulos['notas_fiscais'] 						= 'Importação de Notas Fiscais';
		modulos['regra_desconto'] 						= 'Importação de Regra de Desconto';
		modulos['noticias'] 							= 'Importação de Notícias';
		modulos['representante'] 						= 'Importação de Representante';
		modulos['prospects_processados'] 				= 'Importação de Prospects na Empresa';
		modulos['unidade_medidas_venda'] 				= 'Importação de Unidades de Medida';
		modulos['configuracao'] 						= 'Importação de Configuração de Pedidos';
		
		modulos['comissoes'] 							= 'Importação de Comissões';
		modulos['recebimentos']							= 'Importação de Recebimentos';
		modulos['pedidos_por_rota']						= 'Importação de Pedidos por Rota';
		modulos['desconto_por_fardo']					= 'Importação de Faixas de Desconto';
		
		//--------------------------
		// Pendências
		//--------------------------
		modulos['exportacao_pendencias_mensagens'] 		= 'Exportação de Pendências Mensagens';
		modulos['exportacao_pendencias'] 				= 'Exportação de Pendências';
		modulos['pendencias'] 							= 'Importação de Pendências';
		modulos['pendencias_usuarios'] 					= 'Importação de Pendências Destinatários';
		modulos['pendencias_mensagens'] 				= 'Importação de Pendências Mensagens';
		
		//--------------------------
		// Agenda
		//--------------------------
		modulos['exportacao_agenda'] 					= 'Exportação de Agenda';
		modulos['agenda'] 								= 'Importação de Agenda';
		
		//CUSTOM - 000177/000870
		modulos['parametros']							= 'Importação de Parâmetros';
		modulos['ramos']								= 'Importação de Ramos';
		modulos['tipos_transporte']						= 'Importação de Tipos de Transporte';
		modulos['portos_destino']						= 'Importação de Portos de Destino';
		
		modulos['clientes_setor']						= 'Importação de Clientes por Setor';
		modulos['zonas_setor']							= 'Importação de Zonas por Setor';
		modulos['rotas']								= 'Importação de Rotas';
		modulos['prazos_entrega']						= 'Importação de Prazos de Entrega';
		modulos['motivos_troca']						= 'Importação de Motivos de Troca';
		//modulos['catalogos']							= 'Importação de Catálogos';
		
		
	//Salvando Informações da sincronização
	db.transaction(function(x) {
		
		
		// Assim que todos os dados forem atualizado será alterado o campo dt_atualizado, desta forma em uma próxima sincronização ( após erro) 
		// todos os dados serão sincronizados novamente, evitando problema de dados não atualizados.
		
		
		
		
		x.executeSql('SELECT MAX(dt_atualizado) AS ultima_atualizacao FROM ' + tabela + ' ', [], 
				function(y, dados) {
					var dt_atualizado = dados.rows.item(0)['ultima_atualizacao'];
					x.executeSql('UPDATE sincronizacoes SET sucesso = ?, dt_atualizado = ? WHERE modulo = ?', [1, dt_atualizado, tabela], function (tx, result) {
						console.log("Módulo: "+tabela+" - Atualizado registro maximo");
	            
	            
	            
	            
			
					});
				},
				function (tx, error) {
					
					
					
					x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, tabela], function (tx, result) {
						console.log("Módulo: "+tabela+" - Não incremental");
						
	            
	            
	            
			
					});
                }
			);
	});

	//Mensagem para o usuário
	$('#'+tabela).after('<span class="ui-icon-white ui-icon-check"></span> ');
	console.log(tabela + ' - ' + modulos[tabela]);
	$('#modulos-sincronizados').prepend('<li><b>Sucesso.</b> - '+modulos[tabela]+'</li>').show();
	
	//Mensagem no console	
	console.log('A sincronização de ' + tabela + ' foi finalizada!');
	console.log('-------------------------------------------------');
	
	//Proxima funcao
	if(proxima_funcao)
	{
		eval(proxima_funcao);
	}
	else
	{
		mensagem('Sincronização concluída com sucesso.', "window.location = 'index.html';");																			
	}

}

//-- -- -- -- -- -- -- -- -- -- -- --

/**
* Metódo:		exportar_dados
* 
* Descrição:	Função Utilizada para enviar dados d eum modulo do DWFORÇA DE VENDAS para o WEBSERVICE
* 
* Data:			24/09/2013
* Modificação:	25/09/2013
* 
* @param		string 		$codigo_modulo					- ID do modulo (Campo Chave) para identificar a chave
* @param		string 		$modulo							- Modulo que sera sincronizado (nome da tabela no WS)
* @param		string 		$descricao_modulo				- Descrição do modulo que será exibida no carregando
* @param		string 		$proxima_funcao					- Proxima funcao que sera chamada quando finalziar a sincronização de um pacote
* @param		array 		$codigo							- Array utilizado para especificar quais dados serão enviados (EXP: vamos enviar somento o prospect "54321" => codigos[0] = '54321';)
*
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function exportar_dados(codigo_modulo, modulo, descricao_modulo, proxima_funcao, exportar_codigos)
{
	//Nova rotina de sincronismo de pedidos/orçamentos
	if(modulo == 'pedidos_pendentes' || modulo == 'orcamentos') 
	{
		
		$('#carregando').show();
		$('#btnCancelar').show();
		$('#btnSincronizar').hide();
		$( "#progressbar" ).progressbar({value: 0});
		
		var pedidos = new Array();
		
		db.transaction(function(x) {
			
			var where_codigos = "";
			if(exportar_codigos && exportar_codigos.length > 0) {
				where_codigos = " AND " + codigo_modulo + " IN ('" + implode('\', \'', exportar_codigos) + "')";
			}
			//Obtem os pedidos/orçamento a serem sincronizados
			x.executeSql('SELECT ' + codigo_modulo + ' FROM ' + modulo + ' WHERE exportado IS NULL '+ where_codigos + ' GROUP BY ' + codigo_modulo, [], function(x, dados) {
				var total = dados.rows.length;
				
				if(total > 0) {
					for(i = 0; i < total; i++) {
						var dado = dados.rows.item(i);
						
						pedidos[i] = dado[codigo_modulo];
					}
					
					//Chama a funçao enviarPedidos() para realizar o sincronismo dos pedidos individualmente
					enviarPedidos(pedidos, false, modulo, codigo_modulo, descricao_modulo, proxima_funcao);
				} else {
					finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
				}
			},function (){ // Se a tabela não existir entrar nessa condição
				finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
			});
		});
		
	} else {
		
		// Carregando
		$('#carregando').show();
		$('#btnCancelar').show();
		$('#btnSincronizar').hide();
		$('#carregando').find('span:first').html('Enviando ' + descricao_modulo + '…');
		$( "#progressbar" ).progressbar({value: 30});
		
		db.transaction(function(x) {
			
			//-- Script para Enviar dados específicos
			var where_codigos = "";
	
			
			
			
			var exportado_por_codigo = false;
			if(exportar_codigos && exportar_codigos.length > 0)
			{
				where_codigos = " WHERE " + codigo_modulo + " IN ('" + implode('\', \'', exportar_codigos) + "')";
				exportado_por_codigo = true;
			}
			
			
			/** Como os pedidos pendentes ficam armazenados no tablet até que seja importado para o protheus 
			 * 
			 * Foi necessário adicionar o código abaixo para que a exportação de outros módulos não fosse afetada.
			 * 
			 * */		
			if(modulo != 'pedidos_pendentes' && exportado_por_codigo ){			
				where_codigos += ' AND exportado IS NULL  ';
			}else if(modulo != 'pedidos_pendentes'){
				where_codigos += ' WHERE exportado IS NULL  ';			
			}
			
			console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
			console.log('SELECT * FROM ' + modulo + ' WHERE exportado IS NULL ' + where_codigos);
			console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
			
			x.executeSql('SELECT * FROM ' + modulo + ' '+ where_codigos, [], function(x, dados) {
				
				var total_registros = dados.rows.length;
				
				console.log('Total = ' + total_registros);
				
				if (total_registros)
				{
					
					var retorno_json = new Array();
					for(registro = 0; registro < total_registros; registro++)
					{
						retorno_json[registro] = dados.rows.item(registro);
					}
					
					var ajax = $.ajax({
						url: config.ws_url + modulo + '/importar',
						type: 'POST',
						data: {
							retorno: json_encode(retorno_json)
						},
						success: function(dados) {
							if(dados.sucesso == 'ok')
							{
								//Apagando os dados com erro na sessao
								localStorage.setItem('erros_' + modulo, '');
								
								
								
								if(exportar_codigos && exportar_codigos.length > 0)
								{
									// Marcando os dados especificados no "exportar_codigos" como exportados
									console.log('teste 01');
									db.transaction(function(x) {
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\',  erro = \'0\'  WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\')', [], function(){
											$( "#progressbar" ).progressbar({value: 100});
											finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
										});
									});
								}
								else
								{
									// Marcando todos os dados como exportados
								
									db.transaction(function(x) {
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\', erro = \'0\' WHERE exportado IS NULL', [], function(){
											$( "#progressbar" ).progressbar({value: 100});
											finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
										});
									});
								}
	
							}
							else if(dados.erro)
							{
								var codigos_sessao = [];
								var codigos = [];
								$.each(dados.erro, function(i, codigo) {
									codigos.push("'" + codigo + "'");
									
									if(is_numeric(codigo))
									{
										codigos_sessao.push(codigo);
									}
									
								});
								
								//Gravando na sessao dos dados com erro
								localStorage.setItem('erros_' + modulo, codigos_sessao);
								
								
								var descricoes = [];
								$.each(dados.erro_descricao, function(i, descricao) {
									descricoes.push(descricao);
								});
							
								db.transaction(function(x) {
								
									//Precisamos marcar quais dados estao com erro, e depois apagar todos os dados que não estao com erro
									
									//Atualiza os pedidoss que deu erro
									x.executeSql('UPDATE ' + modulo + ' SET erro = \'1\' WHERE ' + codigo_modulo + ' IN (' + codigos.join(', ') + ')');
									
									// Deletando dados enviados
									//x.executeSql('DELETE FROM ' + modulo + ' WHERE exportado IS NULL AND erro IS NULL');
									
									if(exportar_codigos && exportar_codigos.length > 0)
									{
										// Marcando os dados especificados no "exportar_codigos" como exportados, exceto os pedidos em que ocorreu erro
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\') AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
									}
									else
									{
										// Marcando todos os dados como exportados, exceto os pedidos em que ocorreu erro
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE exportado IS NULL AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
									}
											
									
									console.info(descricoes.join(', '));
									apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível sincronizar:<b><br />(' + descricoes.join(', ') + ').</b><br />Entre em contato com TI da sua empresa.', {
										'textYes': 'OK'
									}, function (dado) {
									
										$( "#progressbar" ).progressbar({value: 100});
										finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
										
									});
	
									
								});
								
							}
						},
						error: function(){
							console.log("ERRO NA REQUISIÇÃO AJAX -> Módulo: "+descricao_modulo+" -> PROXIMA CHAMADA");
							
							//Erro na requisição
							reiniciar_instalacao();
						}
					});
				}
				else
				{
					db.transaction(function(x) {
						var where = '';
	 
					    if(modulo == 'pedidos_pendentes' || modulo == 'orcamentos') {
					      //Exclui somente os pedidos que já estão na SC5
					      where = ' WHERE lower(pedido_id_pedido) IN (SELECT lower(pedido_codigo_pedido_portal) FROM pedidos_processados GROUP BY pedido_codigo_pedido_portal)';
					    }
	 
					    x.executeSql('DELETE FROM ' + modulo + where);
						console.log('DELETE FROM ' + modulo + where);
	
						$( "#progressbar" ).progressbar({value: 100});
						finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
					});
				}
				
			},
			function (){ // Se a tabela não existir entrar nessa condição
				finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
			});
			
		});
		
	}
	
}



function enviarPedidos(pedidos, iteracaoAtual, modulo, codigo_modulo, descricao_modulo, proxima_funcao) 
{
	if(iteracaoAtual == false) {
		var iteracao = 0;
	} else {
		var iteracao = iteracaoAtual;
	}
	
	if(iteracao == pedidos.length) { //Termino de envio dos pedidos/orçamentos
		
		finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
		
	} else { //Envia o próximo pedido/orçamento
		
		//Atualiza a barra de progresso
		$('#carregando').find('span:first').html('Enviando pedido ' + pedidos[iteracao].toUpperCase() + ' (' + (iteracao+1) + ' de ' + pedidos.length + ')…');
		var progress = $('#progressbar').progressbar('option', 'value') + (100 / pedidos.length);
		$( "#progressbar" ).progressbar({value: progress});
		
		//Obtem os dados do pedido/orçamento para realizar o sincronismo 
		db.transaction(function(x) {
		    x.executeSql('SELECT * FROM ' + modulo + ' WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '" AND exportado IS NULL', [], function(x, dados) {
	    		var total = dados.rows.length;
				
	    		var pedido = new Array();
	    		for(i = 0; i < total; i++) {
	    			pedido[i] = dados.rows.item(i);
	    		}
	    		console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	    		console.log("ITERACAO " + iteracao + " >> ENVIANDO PEDIDO " + pedidos[iteracao]);
	    		
	    		//Envia o pedido selecionado
	    		var ajax = $.ajax({
					url: config.ws_url + modulo + '/importar',
					type: 'POST',
					data: {
						retorno: json_encode(pedido)
					},
					success: function(dados) {
						
						
						if(dados.sucesso == 'ok') {
							
							//Marcando o pedido como exportado
							db.transaction(function(x) {
								x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\', editado = \'0\', erro = \'0\' WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '"', [], function(){
									console.log("SQL SUCESSO >> " + 'UPDATE ' + modulo + ' SET exportado = \'1\', erro = \'0\' WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '"');
									console.log("PEDIDO " + pedidos[iteracao] + " ENVIADO");
									console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
									
									enviarPedidos(pedidos, iteracao + 1, modulo, codigo_modulo, descricao_modulo, proxima_funcao);
								});
							});
							
						} else if(dados.erro) {
							
							var mensagem ='Entre em contato com TI da sua empresa.';
							if(dados.erro_descricao.length > 0 ){
								
								mensagem = dados.erro_descricao;
							}
							//Marcando o pedido como erro
							db.transaction(function(x) {
								x.executeSql('UPDATE ' + modulo + ' SET exportado = NULL, erro = \'1\' WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '"', [], function(){
									
									apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível sincronizar o pedido:<b><br />(' + pedidos[iteracao] + ').</b><br />'+mensagem, {
										'textYes': 'OK'
									}, function (dado) {
										console.log("SQL ERRO >> " + 'UPDATE ' + modulo + ' SET exportado = NULL, erro = \'1\' WHERE ' + codigo_modulo + ' = "' + pedidos[iteracao] + '"');
										console.log("ERRO PEDIDO " + pedidos[iteracao] + " -> PROXIMA CHAMADA");
										console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
										
										enviarPedidos(pedidos, iteracao + 1, modulo, codigo_modulo, descricao_modulo, proxima_funcao);
									});
									
								});
							});
						}
						
					},
					error: function(){
						console.log("ERRO NA REQUISIÇÃO AJAX -> PEDIDO " + pedidos[iteracao] + " -> PROXIMA CHAMADA");
						reiniciar_instalacao();
						//Erro na requisição
						apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível se conectar ao servidor para realizar o envio do pedido/orçamento: <b><br />(' + pedidos[iteracao] + ').</b><br />Tente novamente mais tarde.', {
							'textYes': 'OK'
						}, function (dado) {
							enviarPedidos(pedidos, iteracao + 1, modulo, codigo_modulo, descricao_modulo, proxima_funcao);
						});
					}
	    		});

		    });
		});
		
	}
}



/**
 * 
 */
function reiniciar_instalacao(){

	db.transaction(function(x) {
		
		x.executeSql('DROP TABLE sincronizacoes ', [], function () {
			
			
			apprise('A sincronização não foi totalmente concluída. Você deseja tentar novamente?', {
				'verify': true,
				'textYes': 'Sim',
				'textNo': 'Não'
			}, function (dado) {
				if (dado)
				{
					window.location = 'sincronizar.html';
				}
				else
				{
					window.location = 'autenticar.html';
				}
			});
			
			
			
	
			
		}, function(xy, error){
			console.log('Tabela: ' + tabela + ' SQL: ' + campo + ' Não foi possível alterar: ' + error.message);
		});
	});


}