var total_pagina = 20;
$(document).ready(function() {	
	// obter Catálogos	
	obter_catalogos(sessionStorage['catalogos_pagina_atual'], sessionStorage['catalogos_ordem_atual'], sessionStorage['catalogos_criacao_inicial'], sessionStorage['catalogos_criacao_final'], sessionStorage['catalogos_titulo']);
	
	//Ordenação do grid
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_noticias(sessionStorage['catalogos_pagina_atual'], $(this).data('campo') + ' DESC', sessionStorage['catalogos_criacao_inicial'], sessionStorage['catalogos_criacao_final'], sessionStorage['catalogos_titulo']);
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_noticias(sessionStorage['catalogos_pagina_atual'], $(this).data('campo') + ' ASC', sessionStorage['catalogos_criacao_inicial'], sessionStorage['catalogos_criacao_final'], sessionStorage['catalogos_titulo']);
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	//Filtrar dados
	$('input[name=titulo]').val(sessionStorage['catalogos_titulo']);
	$('input[name=criacao_inicial]').val(sessionStorage['catalogos_criacao_inicial']);
	$('input[name=criacao_final]').val(sessionStorage['catalogos_criacao_final']);

	$('#filtrar').click(function() {
		
		sessionStorage['catalogos_pagina_atual'] 	= 1;
		sessionStorage['catalogos_titulo'] 			= $('input[name=titulo]').val();
		sessionStorage['catalogos_criacao_inicial'] = $('input[name=criacao_inicial]').val();
		sessionStorage['catalogos_criacao_final'] 	= $('input[name=criacao_final]').val();
		
		obter_catalogos(sessionStorage['catalogos_pagina_atual'], sessionStorage['catalogos_ordem_atual'], sessionStorage['catalogos_criacao_inicial'], sessionStorage['catalogos_criacao_final'], sessionStorage['catalogos_titulo']);
	});
	
	$('#limpar_filtros').click(function() {
		
		sessionStorage['catalogos_pagina_atual'] 		= 1;
		sessionStorage['catalogos_titulo'] 			= '';
		sessionStorage['catalogos_criacao_inicial'] 	= '';
		sessionStorage['catalogos_criacao_final'] 	= '';

		$('input[name=titulo]').val(sessionStorage['catalogos_titulo']);
		$('input[name=criacao_inicial]').val(sessionStorage['catalogos_criacao_inicial']);
		$('input[name=criacao_final]').val(sessionStorage['catalogos_criacao_final']);
		
		obter_catalogos(sessionStorage['catalogos_pagina_atual'], sessionStorage['catalogos_ordem_atual'], sessionStorage['catalogos_criacao_inicial'], sessionStorage['catalogos_criacao_final'], sessionStorage['catalogos_titulo']);
	});
	
	//Abrir catálogo
	$('#ver_catalogo').live('click', function() {
		var id = $(this).attr('data-id');
		
		console.log('id = ' + id);
		console.log(localStorage.getItem('caminho_local'));
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM catalogos WHERE id = ?', [id], function(x, dados) {
					var catalogo = dados.rows.item(0);
					
					var html = '<table cellspacing="5">';
						html += '<tr><td><h2><img src="img/catalog-icon.png" width="25"> ' + catalogo.titulo + '</h2></td></tr>';						
						html += '<tr><td><div class="aviso">Se você tentar abrir um arquivo PDF, ele será aberto normalmente pelo leitor padrão. Se isso não acontecer, você poderá definir o leitor para esse tipo de arquivo.</div></td></tr>';  //CUSTOM Catalogo
						html += '</table>';
					//CUSTOM Catalogo
						//file_path = ('201.24.78.182:8081/dwpdr/uploads/catalogos/'+ (catalogo.url));
						
						file_path = ('file://'+localStorage.getItem('caminho_local')+ (catalogo.url));
					//console.log(file_path); 
					
					//(window.open(file_path, '_system', 'location=no'));
				//	alert('A'); 
					
					//window.open((file_path), '_system', 'location=no');
					
					comando = "window.open('"+encodeURI(file_path)+"', '_system')";
					console.log(comando);
				//	eval(comando);
				//	alert('B');
					
					//$.colorbox({ html: html, maxWidth: '995px' });
					//exibirMensagem('dialog_catalogos', html, "window.open(encodeURI("+file_path+"), '_system', 'location=yes')", 'Detalhes');
					//exibirMensagem('dialog_catalogos', html, "window.open('"+file_path+"', '_system')", 'Detalhes');
					exibirMensagem('dialog_catalogos', html, comando, 'Detalhes');
					//FIM CUSTOM Catalogo
				}
			);
		});
		
		return false;
	});
	
});

function obter_catalogos(pagina, ordem, dt_inicial, dt_final, titulo)
{
	
	//Filtros
	pagina 				= pagina 		?	pagina 			: 1;
	ordem 				= ordem 		?	ordem 			: 'id DESC';
	titulo 				= titulo 		?	titulo 			: '';
	criacao_inicial 	= dt_inicial 	?	dt_inicial 		: '';
	criacao_final 		= dt_final 		? 	dt_final 		: '';
	
	// setar pagina e ordem atual
	sessionStorage['catalogos_pagina_atual'] 	= pagina;
	sessionStorage['catalogos_ordem_atual'] 	= ordem;
	sessionStorage['catalogos_titulo'] 			= titulo;
	sessionStorage['catalogos_criacao_inicial'] = criacao_inicial;
	sessionStorage['catalogos_criacao_final'] 	= criacao_final;
	
	// definir seta
	var ordem = explode(' ', sessionStorage['catalogos_ordem_atual']);
	
	if (ordem[1] == 'ASC')
	{
		$('a[data-campo=' + ordem[0] + ']').data('ordem', 'ASC');
		
		$('a[data-campo=' + ordem[0] + ']').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo=' + ordem[0] + ']').data('ordem', 'DESC');
		
		$('a[data-campo=' + ordem[0] + ']').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['catalogos_pagina_atual'] - 1) * total_pagina;
	
	
	//Where
	var wheres = '';
	
	if (titulo)
	{
		wheres += 'AND titulo LIKE "%' + titulo + '%"';
	}
	
	if (criacao_inicial && criacao_inicial != 'undefined')
	{	
		var ex = explode('/', criacao_inicial);
		
		wheres += ' AND timestamp >= "' + mktime(0, 0, 0, ex[1], ex[0], ex[2]) + '"';
	}

	if (criacao_final && criacao_final != 'undefined')
	{
		var ex = explode('/', criacao_final);
	
		wheres += ' AND timestamp <= "' + mktime(23, 59, 59, ex[1], ex[0], ex[2]) + '"';
	}
	
	// exibir pedido e calcular totais
	// 	id	timestamp	status	link_imagem	titulo	conteudo
	console.log('Ordenar por: '+sessionStorage['catalogos_ordem_atual']);
	//SELECT count(*) FROM sqlite_master WHERE type='table' AND name='table_name';
	console.log('SELECT * FROM catalogos WHERE id > 0 ' + wheres + ' ORDER BY ' + sessionStorage['catalogos_ordem_atual'] + ' LIMIT '+total_pagina+' OFFSET ' + offset);
	
	
	db.transaction(function(x) {
		x.executeSql(
			"SELECT name FROM sqlite_master WHERE type='table' AND name='catalogos'", [], function(x, dados) {
				
				if (dados.rows.length)
				{
					x.executeSql(
						'SELECT * FROM catalogos WHERE id > 0 ' + wheres + ' ORDER BY ' + sessionStorage['catalogos_ordem_atual'] + ' LIMIT '+total_pagina+' OFFSET ' + offset, [], function(x, dados) {
							if (dados.rows.length)
							{
								$('.catalogos').empty();

								for (i = 0; i < dados.rows.length; i++)
								{
									
									var catalogo = dados.rows.item(i);
									
									var conteudo = trim(strip_tags(catalogo.descricao));
									
									var html = '';
										//html += '<td align="center">'+date('d/m/Y H:i:s', noticia.timestamp)+'</td>';
										//html += '<td>'+noticia.titulo+'</td>';
										//html += '<td><a href="#" id="ver_noticia" data-id="' + noticia.id + '">Ver Detalhes</a></td>';
										
									
									html += '<li>';					
										html += '<div class="titulo">';
											html += '<span class="data">' +  datetime2date(catalogo.data_cadastro) + '</span> - ' + catalogo.titulo;
										html += '</div>';
										
										html += '<div class="descricao">';
											html += conteudo.substring('0', '250');
											html += (strlen(conteudo) > '250' ? '...' : '');
										html += '</div>';
										
										html += '<div class="vermais">';
												html += '<a href="#" id="ver_catalogo" data-id="' + catalogo.id + '" class="btn btn-large btn-primary">Ler Catálogo</a>';
										html += '</div>';
										
									html += '</li>';
									
									
									$('.catalogos').append(html);
									
									//alterarCabecalhoTabelaResolucao();
								}
							}
							else
							{
								
								$('.catalogos').empty();
								$('.catalogos').append('<li><div style="color: #900; padding: 10px;"><strong>Nenhum catálogo encontrado.</strong></div></li>');
							}
					
						});
					
				}else{
					$('.catalogos').empty();
					$('.catalogos').append('<li><div style="color: #900; padding: 10px;"><strong>Nenhum catálogo encontrado. Realize a sincronização de catálogos.</strong></div></li>');
				
				}
			});
		
	
		
		
		
		// calcular totais
		x.executeSql(
				'SELECT COUNT(DISTINCT id) AS total FROM catalogos WHERE id != "" ' + wheres, [], function(x, dados) {
				var dado = dados.rows.item(0);

				$('#total').text(number_format(dado.total, 0, ',', '.'));
				// paginação

				$('#paginacao').html('');

				var total = ceil(dado.total / total_pagina);

				if (sessionStorage['catalogos_pagina_atual'] > 6)
				{
					obter_catalogos(sessionStorage['catalogos_pagina_atual'], sessionStorage['catalogos_ordem_atual'], sessionStorage['catalogos_criacao_inicial'], sessionStorage['catalogos_criacao_final'], sessionStorage['catalogos_titulo']);
					$('#paginacao').append('<a href="#" onclick="obter_catagolos(1, \'' + sessionStorage['catalogos_pagina_atual'] + '\', \'' + sessionStorage['catalogos_ordem_atual'] + '\', \'' + sessionStorage['catalogos_criacao_inicial'] + '\', \'' + sessionStorage['catalogos_criacao_final'] + '\', \'' + sessionStorage['catalogos_titulo'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
				}

				if (sessionStorage['catalogos_pagina_atual'] > 1)
				{
					$('#paginacao').append('<a href="#" onclick="obter_catalogos(' + (intval(sessionStorage['catalogos_pagina_atual']) - 1) + ', \'' + sessionStorage['catalogos_ordem_atual'] + '\', \'' + sessionStorage['catalogos_criacao_inicial'] + '\', \'' + sessionStorage['catalogos_criacao_final'] + '\', \'' + sessionStorage['catalogos_titulo'] + '\'); return false;">&lt;</a> ');
				}

				for (i = intval(sessionStorage['catalogos_pagina_atual']) - 6; i <= intval(sessionStorage['catalogos_pagina_atual']) + 5; i++)
				{
					if (i <= 0 || i > total)
					{
						continue;
					}

					if (i == sessionStorage['catalogos_pagina_atual'])
					{
						$('#paginacao').append('<strong>' + i + '</strong> ');
					}
					else
					{
						$('#paginacao').append('<a href="#" onclick="obter_catalogos(' + i + ', \'' + sessionStorage['catalogos_ordem_atual'] + '\', \'' + sessionStorage['catalogos_criacao_inicial'] + '\', \'' + sessionStorage['catalogos_criacao_final'] + '\', \'' + sessionStorage['catalogos_titulo'] + '\'); return false;">' + i + '</a> ');
					}
				}

				if (sessionStorage['catalogos_pagina_atual'] < total)
				{
					$('#paginacao').append('<a href="#" onclick="obter_catalogos(' + (intval(sessionStorage['catalogos_pagina_atual']) + 1) + ', \'' + sessionStorage['catalogos_ordem_atual'] + '\', \'' + sessionStorage['catalogos_criacao_inicial'] + '\', \'' + sessionStorage['catalogos_criacao_final'] + '\', \'' + sessionStorage['catalogos_titulo'] + '\'); return false;">&gt;</a> ');
				}

				if (sessionStorage['catalogos_pagina_atual'] <= total - 6)
				{
					$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_catalogos(' + total + ', \'' + sessionStorage['catalogos_pagina_atual'] + '\', \'' + sessionStorage['catalogos_ordem_atual'] + '\', \'' + sessionStorage['catalogos_criacao_inicial'] + '\', \'' + sessionStorage['catalogos_criacao_final'] + '\', \'' + sessionStorage['catalogos_titulo'] + '\'); return false;">Última Página</a> ');
				}
		});
	});
}