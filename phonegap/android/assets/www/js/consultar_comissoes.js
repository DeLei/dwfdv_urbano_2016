$(document).ready(function() {
	
	
	
	
	// obter produtos
	
	obter_comissoes(sessionStorage['comissoes_pagina_atual'], sessionStorage['comissoes_ordem_atual']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_comissoes(sessionStorage['comissoes_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_comissoes(sessionStorage['comissoes_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['notas_' + name] != 'undefined' ? sessionStorage['notas_' + name] : '');
	});
	
	// obter filiais
	obter_filiais_permitidas(function(filiais){
		
	
		$('select[name=filial_pedido]').append('<option value="">Todos</option>');
			
	
			
			$.each(filiais, function(key, dado) {			
					$('select[name=filial_pedido]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
			});
	
	});
	
	
	
	
	
	
	
	//obter_situacoes
	var situacoes =  [
		{
			"codigo":"2",
			"descricao": "Pago"
		},
		{
			"codigo":"1",
			"descricao": "À pagar"
		}
	];
	
	$('select[name=situacao]').append('<option value="">Todos</option>');
	
	$.each(situacoes, function(key, item) {
		$('select[name=situacao]').append('<option value="'+item.codigo+'">'+item.descricao+'</option>');		
	});
	
	$('#filtrar').click(function() {
		sessionStorage['comissoes_pagina_atual'] = 1;
				
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['' + name] = $(this).val();
		});
		
		obter_comissoes(sessionStorage['comissoes_pagina_atual'], sessionStorage['comissoes_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['comissoes_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['notas_' + name] = '';
		});
		
		obter_comissoes(sessionStorage['comissoes_pagina_atual'], sessionStorage['comissoes_ordem_atual']);
	});
	
	
	//-- #ver_titulos -  esta função esta no arquivo geral.js
	//$('#ver_titulos')
	
});

function obter_comissoes(pagina, ordem)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'data_emissao DESC';
	
	// setar pagina e ordem atual
	sessionStorage['comissoes_pagina_atual'] = pagina;
	sessionStorage['comissoes_ordem_atual'] = ordem;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['comissoes_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'ASC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'DESC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['comissoes_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	situacao				= sessionStorage['situacao'];	
	data_emissao_inicial	= sessionStorage['data_emissao_inicial'];
	data_emissao_final		= sessionStorage['data_emissao_final'];
	filial_pedido 			= sessionStorage['filial_pedido'];
	numero_pedido			= sessionStorage['numero_pedido'];	
	data_vencimento_comissao_inicial	= sessionStorage['data_vencimento_comissao_inicial'];
	data_vencimento_comissao_final		= sessionStorage['data_vencimento_comissao_final'];
	
	var wheres = '';
	
	
	if (situacao && situacao != 'undefined')
	{
		wheres += " AND situacao = '" + situacao + "'";
	}
	
	
	if (filial_pedido && filial_pedido != 'undefined')
	{
		wheres += " AND filial_pedido = '" + filial_pedido + "'";
	}
	
	if (numero_pedido && numero_pedido != 'undefined')
	{
		wheres += " AND numero_pedido = '" + numero_pedido + "'";
	}
	
	if (data_emissao_inicial && data_emissao_inicial != 'undefined')
	{
		var ex = explode('/', data_emissao_inicial);
		wheres += " AND data_emissao >= '"+ ex[2]+ex[1]+ex[0] +"'";
	}
	
	if (data_emissao_final && data_emissao_final != 'undefined')
	{
		var ex = explode('/', data_emissao_final);
		wheres += " AND data_emissao <= '" + ex[2]+ex[1]+ex[0] + "'";
	}
	
	if (data_vencimento_comissao_inicial && data_vencimento_comissao_inicial != 'undefined')
	{
		var ex = explode('/', data_vencimento_comissao_inicial);
		wheres += " AND data_vencimento_comissao >= '"+ ex[2]+ex[1]+ex[0] +"'";
	}
	
	if (data_vencimento_comissao_final && data_vencimento_comissao_final != 'undefined')
	{
		var ex = explode('/', data_vencimento_comissao_final);
		wheres += " AND data_vencimento_comissao <= '" + ex[2]+ex[1]+ex[0] + "'";
	}
	
	
	
	
	//console.log('SELECT * FROM comissoes WHERE rnum > 0 ' + wheres + ' ORDER BY ' + sessionStorage['comissoes_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset);
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM comissoes WHERE codigo_representante != "" ' + wheres + ' ORDER BY ' + sessionStorage['comissoes_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
			
				if (dados.rows.length)
				{
				
					
					$('table tbody').empty();
			
					for (i = 0; i < dados.rows.length; i++)
					{
						var item 				= dados.rows.item(i);
						var situacao			= item.situacao ? item.situacao : 'N/A';
						var data_emissao 		= item.data_emissao ? item.data_emissao: 'N/A';
						var filial_pedido		= item.filial_pedido ? item.filial_pedido: 'N/A';
						var numero_pedido   	= item.numero_pedido ? item.numero_pedido : 'N/A';
						var codigo_cliente 		= item.codigo_cliente ? item.codigo_cliente: 'N/A';
						var loja_cliente 		= item.loja_cliente ? item.loja_cliente: 'N/A';
						var nome_fantasia_cliente 		= item.nome_fantasia_cliente ? item.nome_fantasia_cliente: 'N/A';
						var numero_titulo 		= item.numero_titulo ? item.numero_titulo: 'N/A';
						var valor_base_pedido 	= item.valor_base_pedido ? number_format(item.valor_base_pedido,2,',','.'): 'N/A';
						var percentual_comissao = item.percentual_comissao ? item.percentual_comissao: 'N/A';
						var valor_comissao		= item.valor_comissao ? item.valor_comissao: 'N/A';
						var data_vencimento_comissao = item.data_vencimento_comissao? item.data_vencimento_comissao: 'N/A';

								
						var html = '';						
							
							
							html += '<td align="center">'+'<img src="img/status/' + ((situacao == '2')? 'verde' : 'laranja') + '.png">'+'</td>';
							html += '<td align="center">'+protheus_data2data_normal(data_emissao)+'</td>';
							html += '<td align="center">'+filial_pedido+'</td>';
							html += '<td align="center">'+numero_pedido+'</td>';									
							html += '<td align="right"><a href="clientes_visualizar.html#'+codigo_cliente+'_'+loja_cliente+'">'+codigo_cliente+'/'+loja_cliente+'</a></td>';
							html += '<td align="left">'+nome_fantasia_cliente+'</td>';
							html += '<td align="center">'+numero_titulo+'</td>';
							html += '<td align="center">'+valor_base_pedido+'</td>';
							html += '<td align="center">'+number_format(percentual_comissao,2,',','.')+'</td>';
							html += '<td align="center">'+number_format(valor_comissao,2,',','.')+'</td>';
							html += '<td align="center">'+protheus_data2data_normal(data_vencimento_comissao)+'</td>';
							
							//html += '<td align="center">'+protheus_data2data_normal(item.nota_data_emissao)+'</td>';					
							//html += '<td align="right">'+number_format(item.nota_valor_mercadoria, 3, ',', '.')+'</td>';
							//html += '<td align="right">'+number_format(item.nota_valor_icms, 3, ',', '.')+'</td>';
							//html += '<td align="right">'+number_format(item.nota_valor_ipi, 3, ',', '.')+'</td>';
							//html += '<td align="center">'+frete+'</td>';
							//html += '<td>'+transportadora+'</td>';
							//html += '<td>'+transportadora_telefone+'</td>';
							//html += '<td><a href="#" id="ver_titulos" data-serie="'+item.nota_serie+'" data-codigo="'+item.nota_codigo+'">Ver Títulos</a> | <a href="notas_fiscais_visualizar.html#'+item.nota_codigo+'_'+item.nota_serie+'_'+item.nota_filial+'">Ver Itens</a></td>';
						
						$('table tbody').append('<tr>' + html + '</tr>');
					}
					
					alterarCabecalhoTabelaResolucao();
				
				}
				else
				{
					$('table tbody').html('<tr><td colspan="11" style="color: #900; padding: 10px;"><strong>Nenhuma comissão encontrada.</strong></td></tr>');
				}
			}
		);
		
		
		var somar = 'SUM(valor_comissao) AS valor_total ';
			
			
			
		//console.log('SELECT COUNT(DISTINCT(rnum)) AS total, '+somar+' FROM comissoes WHERE rnum > 0 ' + wheres);
			
		// calcular totais
		x.executeSql(
			'SELECT COUNT(*) AS total, '+somar+' FROM comissoes WHERE codigo_representante != "" ' + wheres, [], function(x, dados) {
				
				var dado = dados.rows.item(0);
				
				console.log(dado);
				
				$('#total').text(number_format(dado.total, 0, ',', '.'));
				$('#valor_total').text(number_format(dado.valor_total, 3, ',', '.'));
				
				// paginação
				
				$('#paginacao').html('');
				
				var total = ceil(dado.total / 20);
				
				if (total > 1)
				{
					if (sessionStorage['comissoes_pagina_atual'] > 6)
					{
						$('#paginacao').append('<a href="#" onclick="obter_comissoes(1, \'' + sessionStorage['comissoes_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
					}
					
					if (sessionStorage['comissoes_pagina_atual'] > 1)
					{
						$('#paginacao').append('<a href="#" onclick="obter_comissoes(' + (intval(sessionStorage['comissoes_pagina_atual']) - 1) + ', \'' + sessionStorage['comissoes_ordem_atual'] + '\'); return false;">&lt;</a> ');
					}
					
					for (i = intval(sessionStorage['comissoes_pagina_atual']) - 6; i <= intval(sessionStorage['comissoes_pagina_atual']) + 5; i++)
					{
						if (i <= 0 || i > total)
						{
							continue;
						}
						
						if (i == sessionStorage['comissoes_pagina_atual'])
						{
							$('#paginacao').append('<strong>' + i + '</strong> ');
						}
						else
						{
							$('#paginacao').append('<a href="#" onclick="obter_comissoes(' + i + ', \'' + sessionStorage['comissoes_ordem_atual'] + '\'); return false;">' + i + '</a> ');
						}
					}
					
					if (sessionStorage['comissoes_pagina_atual'] < total)
					{
						$('#paginacao').append('<a href="#" onclick="obter_comissoes(' + (intval(sessionStorage['comissoes_pagina_atual']) + 1) + ', \'' + sessionStorage['comissoes_ordem_atual'] + '\'); return false;">&gt;</a> ');
					}
					
					if (sessionStorage['comissoes_pagina_atual'] <= total - 6)
					{
						$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_comissoes(' + total + ', \'' + sessionStorage['comissoes_ordem_atual'] + '\'); return false;">Última Página</a> ');
					}
				}
			}
		);
	});
}
