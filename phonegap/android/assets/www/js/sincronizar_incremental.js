/**
 * 
 * Processo de sincronismo
 * 
 * Passo 1 - iniciarSincronismo()
 * 		1º Inicia no indice 0 e incrementa a cada novo módulo sincronizado
 * 			Se existir o indice realiza o processo de sincronismo do módulo
 * 			Caso contrário o processo de sincronismo chegou ao fim e é exibida a mensagem de sincronização concluida
 * 
 * 		2º Seleciona a ultima data de atualização e envia a requisição para o WS gerar os dados a partir daquela data
 * 
 * 		3º Se não existir a coluna "dt_atualizado" o módulo é referente a uma tabela do MySQL
 * 			Neste caso irá gerar o pacote com todos os dados
 * 
 * 		4º Se gerou o arquivo corretamente faz o download do módulo <<Passo 2>>
 * 			Senão chama o sincronismo do proximo módulo incrementando o indice <<Passo 1>> iniciarSincronismo(atual + 1)
 * 
 * 
 * Passo 2 - realizarDownloadModulo(modulo)
 * 		1º Realiza o download do módulo informado por parametro
 * 
 * 		2º Descompacta o arquivo zip baixado
 * 
 * 		3º Obtem o total de pacotes referente ao módulo <<Passo 3>>
 * 
 * 
 * Passo 3 - obterTotalPacotesModulo(modulo)
 * 		1º Obtem o total de pacotes referente ao módulo informado por parametro
 * 
 * 		2º Realiza o sincronismo dos dados <<Passo 4>>
 * 			Passa por parametro o nome do módulo, total de pacotes do módulo e iteração atual iniciando em 1 
 * 
 * 
 * Passo 4 - sincronizarDadosModulo(modulo, total_pacotes, iteracao)
 * 		1º Faz a leitura do pacote informado por parametro
 * 
 * 		2º Realiza o UPDATE caso o recno já exista na tabela
 * 			Caso contrário faz o INSERT
 * 
 * 		3º Para tabelas do MySQL
 * 				Os dados são excluidos e reinseridos a cada sincronismo
 * 
 * 		4º Se o total de pacotes for igual a iteração do sincronismo e todos os registros do pacote foram sincronizados
 * 			Retorna ao <<Passo 1>> para realizar a sincronização de um novo módulo
 * 			
 * 			Senão chama o <<Passo 4>> incrementando o parametro de iteração
 * 				sincronizarDadosModulo(modulo, total_pacotes, iteracao + 1)
 * 
 * 
 * Obs.: A cada erro detectado é exibida a mensagem e o processo de sincronismo continua com o próximo módulo
 * 
 * 
 * 
 * 
 * Função exibirStatusSincronismo() utilizada para exibir os status do sincronismo
 * 
 * 		Padrões de numeração:
 * 			1xx - Informativo
 * 			2xx - Sucesso
 * 			3xx - Notificação
 * 			4xx - Erro
 * 
 * 100 - Iniciando atualização
 * 102 - Iniciando download
 * 103 - Gerando arquivos
 * 104 - Atualizando módulo
 * 
 * 200 - Atualizado com sucesso
 * 201 - Arquivo gerado com sucesso
 * 202 - Download realizado com sucesso
 * 
 * 304 - Informativo/Sucesso - Nenhuma informação modificada
 * 
 * 404 - Erro ao realizado download
 * 405 - Erro ao enviar dado
 * 406 - Erro ao realizar a leitura do arquivo
 * 417 - Erro ao gerar o arquivo
 */

var sincronizado = false;



var modulos = new Object();
	modulos['pedidos_pendentes'] 					= 'Pedidos Pendentes';			//0
	modulos['prospects'] 							= 'Prospects';					//1
	
	//modulos['agenda'] 								= 'Agenda';
	modulos['clientes'] 							= 'Clientes';					//2
	modulos['clientes_setor']						= 'Clientes por Setor';			//3
	modulos['comissoes'] 							= 'Comissões';					//4
	modulos['condicoes_pagamento'] 					= 'Condições de Pagamento';		//5
	modulos['empresas'] 							= 'Empresas';					//6
	modulos['eventos'] 								= 'Eventos';					//7
	modulos['desconto_por_fardo']					= 'Faixas de Desconto';			//8
	modulos['filiais'] 								= 'Filiais';					//9
	modulos['formas_pagamento'] 					= 'Formas de Pagamento';		//10
	modulos['historico_clientes'] 					= 'Históricos de Clientes';		//11
	modulos['historico_prospects'] 					= 'Históricos de Prospects';	//12
	modulos['motivos_troca']						= 'Motivos de Troca';			//13
	modulos['municipios'] 							= 'Municípios';					//14
	modulos['notas_fiscais'] 						= 'Notas Fiscais';				//15
	modulos['noticias'] 							= 'Notícias';					//16
	modulos['parametros']							= 'Parâmetros';					//17
	modulos['pendencias'] 							= 'Pendências';					//18
	modulos['pendencias_usuarios'] 					= 'Pendências Destinatários';	//19
	modulos['pendencias_mensagens'] 				= 'Pendências Mensagens';		//20
	modulos['pedidos_por_rota']						= 'Pedidos por Rota';			//21
	modulos['pedidos_processados'] 					= 'Pedidos Processados';		//22
	modulos['portos_destino']						= 'Portos de Destino';			//23
	modulos['prazos_entrega']						= 'Prazos de Entrega';			//24
	modulos['produtos'] 							= 'Produtos';					//25
	modulos['prospects_processados'] 				= 'Prospects na Empresa';		//26
	modulos['ramos']								= 'Ramos';						//27
	modulos['recebimentos']							= 'Recebimentos';				//28
	modulos['regra_desconto'] 						= 'Regra de Desconto';			//29
	modulos['rotas']								= 'Rotas';						//30
	modulos['tabelas_preco'] 						= 'Tabelas de Preço';			//31
	modulos['tipos_transporte']						= 'Tipos de Transporte';		//32
	modulos['titulos'] 								= 'Títulos';					//33
	modulos['transportadoras'] 						= 'Transportadoras';			//34
	//modulos['bancos']   							= 'Bancos';						//38
	//modulos['catalogos']   							= 'Catálogos';
	modulos['zonas_setor']							= 'Zonas por Setor';			//35
	modulos['unidade_medidas_venda']				= 'Unidades de Medida para Venda';	//36
	modulos['configuracao']							= 'Configuração de Pedidos';	//37
	
	
	
	document.addEventListener("deviceready", onDeviceReady, false);

	function onDeviceReady() 
	{
		exibirModulosSincronizar();
	
	$("#btnSincronizarDados").click(function(e){
		e.preventDefault();
		
		confirmar('Tem certeza que deseja realizar a sincronização dos dados?', function () {
			$(this).dialog('close');
			$("#confirmar_dialog").remove();
			
			$("#boxLabelSincronismo").show();
			$("#labelStatusSincronismo").html("Sincronização em andamento...");
			$("#progressbar").show();
			$("#botoesSincronismo").hide();
			$("#statusSincronismo").html("");
			$("#statusSincronismo").show();
			$("#modulos").hide();
			$("#btnVisualizarLogSync").hide();
			$("#btnVisualizarModulosSync").hide();
			sincronizado = false;
			iniciarEnvioDados();
		});
		
	});
	
	$("#btnSincronizarCatalogos").live('click', function(e){	
		e.preventDefault();
		
		confirmar('Tem certeza que deseja sincronizar os catálogos?', 
			function () {
				$(this).dialog('close');
				
				sincronizarCatalogos();
				
				$("#confirmar_dialog").remove();
				
			}
		);

		
	});
	
	
	//Marcar/Desmarcar todos
	$("#btnSelecionarTodos").click(function(){
		if(!$(".modulos").attr('checked')) {
			$(".modulos").attr('checked', true);
		} else {
			$(".modulos").removeAttr('checked');
		}
	});
	
	//Visualizar log de sincronização
	$("#btnVisualizarLogSync").click(function(){
		$("#statusSincronismo").show();
		$("#modulos").hide();
		$("#btnVisualizarModulosSync").show();
		$("#btnVisualizarLogSync").hide();
	});
	
	//Visualizar modulos
	$("#btnVisualizarModulosSync").click(function(){
		$("#statusSincronismo").hide();
		$("#modulos").show();
		$("#btnVisualizarLogSync").show();
		$("#btnVisualizarModulosSync").hide();
	});
	
};

function iniciarEnvioDados() {
	
	
		
	var selecionado = $('[name="modulos[prospects]"]').attr('checked');
	
	if(selecionado) {
		exportarDados('codigo', 'prospects', 'Prospects', 'enviar_historico_prospects()');
	} else {
		enviar_historico_prospects();
	}
}

function enviar_historico_prospects() {
	var selecionado = $('[name="modulos[historico_prospects]"]').attr('checked');
	
	if(selecionado) {
		exportarDados('id', 'historico_prospects', 'Histórico de Prospects', 'enviar_historico_clientes()');
	} else {
		enviar_historico_clientes();
	}
}

function enviar_historico_clientes() {
	var selecionado = $('[name="modulos[historico_clientes]"]').attr('checked');
	
	if(selecionado) {
		exportarDados('id', 'historico_clientes', 'Histórico de Clientes', 'enviar_pendencias()');
	} else {
		enviar_pendencias();
	}
}

function enviar_pendencias() {
	var selecionado = $('[name="modulos[pendencias]"]').attr('checked');
	
	if(selecionado) {
		console.log('Enviando Pendências');		
		exportarDados('id', 'pendencias', 'Pendências', 'enviar_pendencias_mensagens()');
	} else {
		enviar_pendencias_mensagens();
	}
}

function enviar_pendencias_mensagens() {
	var selecionado = $('[name="modulos[pendencias_mensagens]"]').attr('checked');
	
	if(selecionado) {
		exportarDados('id', 'pendencias_mensagens', 'Pendências Mensagens', 'enviar_pedidos()');
	} else {
		enviar_pedidos();
	}
}

function enviar_pedidos() {
	var sinc_pedidos_pendentes = $('[name="modulos[pedidos_pendentes]"]').attr('checked');
	
	if(sinc_pedidos_pendentes) {
		console.log('sincronizar pedidos pendentes');
		exportarPedidos(false, function(){
			iniciarSincronismo(0, function(retorno){				
				finalizarSincronismo(retorno);				
			});
		});
		
	} else {
		console.log('iniciado Sincronismo');
		iniciarSincronismo(0, function(retorno){
			console.log('Retorno: '+ retorno);
			finalizarSincronismo(retorno);
			
		});
		
	}
}


/**
 * Exibe a mensagem de acordo com o situação ao terminar o sincronismo
 * 
 * @param situacao = Código da situação do sincronismo
 * 						0 => Nenhum módulo selecionado/atualizado
 * 						2 => Sincronização realizada com sucesso
 * 					 	4 => Sincronização concluida com erros
 */
function finalizarSincronismo(situacao, proxima_funcao) {
	
	if(proxima_funcao) {
		
		eval(proxima_funcao);
		
	} else {
		
		$("#boxLabelSincronismo").hide();
		$("#progressbar").hide();
		$("#botoesSincronismo").show();
		$("#modulos").show();
		$("#statusSincronismo").hide();
		
		$("#btnVisualizarLogSync").show();
		$("#btnVisualizarModulosSync").hide();
		
		obterRegistrosNaoExportados();
		
		if(situacao == 0) {
			
			apprise('<b>ATENÇÃO</b><br/><br/>Nenhum módulo foi selecionado para realizar o sincronismo. <p> Selecione ao menos um módulo e tente novamente.</p>', {
				'textYes': 'OK'
			});
		
		} else if(situacao == 2) {
			
			mensagem('Sincronização realizada com sucesso.');
			
		} else if(situacao == 4) {
			
			mensagem('Sincronização finalizada com erros.');
			
		}
		
		
		
	}
}

/**
 * 
 * Monta a listagem dos módulos para seleção do sincronismo de acordo com o objeto "modulos" definido no inicio do arquivo
 * 
 */
function exibirModulosSincronizar() {
	var html = '';
	
	obter_grupo_permissao(function(grupo_usuario){
		var permissao = permissoes[grupo_usuario.grupo];
		
		$.each(modulos, function(modulo, descricao){
			
			if(!permissao.sincronizar.desabilitar[modulo]){
			//	console.log(permissao.sincronizar.desabilitar.modulo);
				
				
				var classe = '';
				if(in_array(modulo,['pedidos_pendentes', 'prospects'])) {
					classe = 'importante';
				}
				
				html += '<div id="modulo">';
					html += '<div class="titulo ' + classe + '">';
						html += '<input type="checkbox" class="modulos" name="modulos[' + modulo + ']" value="' + modulo + '"/>';
						html += '<label for="' + modulo + '"> ' + descricao + '</label>';
					html += '</div>';
					
					html += '<div class="conteudo">';
						html += '<div class="descricao">';
							html += 'Obtem as alterações realizadas no modulo de <b>' + descricao + '</b>.'; 
						html += '</div>';
						
						
						//Módulos que realizam o envio de dados
						if(in_array(modulo,['pedidos_pendentes', 'prospects', 'historico_prospects', 'historico_clientes', 'pendencias', 'agenda', 'pendencias_mensagens'])) {
							html += '<h3>' + descricao + '</h3>';
							html += '<div class="lista">';
								html += '<ul id="lista_' + modulo + '_nao_exportados">';
								html += '</ul>';
							html += '</div>';
						}
					html += '</div>';
					
				html += '</div>';
			}
			
		});
		
		
		
		
		
		$("#modulos").html(html);
		
/////////////
		var query = parse_url(location.href).query;
		
		if(query) {
			var parametros_array = explode('?', parse_url(location.href).query);
			var parametros = explode('=', parametros_array[0]);
			
			var codigos = new Array();
				codigos['1'] = parametros['1'];
				
				$("#boxLabelSincronismo").show();
				$("#labelStatusSincronismo").html("Sincronização em andamento...");
				$("#progressbar").show();
				$("#botoesSincronismo").hide();
				$("#statusSincronismo").html("");
				$("#statusSincronismo").show();
				$("#modulos").hide();
				$("#btnVisualizarLogSync").hide();
				$("#btnVisualizarModulosSync").hide();
				
				if(parametros['0'] == 'codigo_pedido') {
					
					$('[name="modulos[pedidos_pendentes]"]').attr('checked', 'checked');
						
					exportarPedidos(codigos, function(){
						iniciarSincronismo(0, function(retorno){
							finalizarSincronismo(sincronizado);
						});
					});
					
					
				} else if(parametros['0'] == 'codigo_prospect') {
					
					$('[name="modulos[prospects]"]').attr('checked', 'checked');
					
					exportarDados('codigo', 'prospects', 'Prospects', '', codigos);
				}
		}
		////////////
		
	});
	
}


/**
 * 
 * Realiza a chamada para sincronização de um módulo específico.
 * 
 * Obs: Chama a função recursivamente até encerrar todos os módulos
 * Obs: Realiza somente a sincronização dos módulos selecionados no checkbox
 * 
 * @param iteracao	= Indice do modulo no objeto "modulos"
 */
function iniciarSincronismo(iteracao, callback) {
	var modulo = null;
	
	if(Object.keys(modulos)[iteracao] !== undefined) {
		modulo = Object.keys(modulos)[iteracao];
	} else {
		modulo = false;
	}
	
	if(modulo) {
		var modulo_selecionado = $('[name="modulos[' + modulo + ']"]').attr('checked');
		
		if(modulo_selecionado) {
			
			if(Object.keys(modulos)[iteracao]) {
				
				if(!sincronizado || sincronizado != 4) {
					sincronizado = 2;
				}
				
				var codigo_representante = localStorage.getItem('codigo');
				var id_usuario = localStorage.getItem('usuarios_id');
				
				$("#labelStatusSincronismo").html("Sincronizando " + modulos[modulo]);
				
				db.transaction(function(x) {
					x.executeSql('INSERT INTO sincronizacoes (timestamp, modulo, sucesso) VALUES (?, ?, ?)', [time(), modulo, 0]);
					
					
					x.executeSql('SELECT dt_atualizado AS ultima_atualizacao FROM ' + modulo + ' ORDER BY dt_atualizado DESC LIMIT 1', [], function(y, dados) {
						
						
						x.executeSql('SELECT dt_atualizado AS ultima_atualizacao FROM sincronizacoes where modulo = ?   ORDER BY dt_atualizado DESC LIMIT 1', [modulo], function(y, dados) {
						
						
						var ultima_atualizacao = false;
						var incremental = false;
						exibirStatusSincronismo(103, modulos[modulo]);
						
						$("#progressbar").progressbar({value: 0});
						
						if(dados.rows.length > 0) {
							var dado = dados.rows.item(0);
								ultima_atualizacao = dado.ultima_atualizacao;
								incremental = true;
						}
						
						var ajax = $.ajax({
							url: config.ws_url + 'incremental/gerar',
							type: 'POST',
							data: {
								codigo_representante: codigo_representante,
								id_usuario: id_usuario,
								modulo: modulo,
								versao : config.versao,
								ultima_sincronizacao: ultima_atualizacao,
								incremental: incremental
							},
							beforeSend: function(){
								$("#progressbar").progressbar({value: 50});
							},
							success: function(dados) {
												
								if(dados.status == 1) {
									
									db.transaction(function(x) {
										x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, modulo]);
									});
									
									exibirStatusSincronismo(201, modulos[modulo]);
									
									realizarDownloadModulo(modulo, function(){
										iniciarSincronismo(iteracao + 1, callback);
									});
								}
								
							},
							error: function(){
								
								exibirStatusSincronismo(417, modulos[modulo]);
								sincronizado = 4;
								iniciarSincronismo(iteracao + 1, callback);
								
							}
						});
						
						
						});	
					/////////   - - - - - --
					}, function(e, dados){ //Não existe a coluna dt_atualizado (tabelas do MySQL)
						
						exibirStatusSincronismo(103, modulos[modulo]);
						
						var ajax = $.ajax({
							url: config.ws_url + 'incremental/gerar',
							type: 'POST',
							data: {
								codigo_representante: codigo_representante,
								id_usuario: id_usuario,
								modulo: modulo,
								ultima_sincronizacao: false,
								incremental: false
							},
							beforeSend: function(){
								$("#progressbar").progressbar({value: 50});
							},
							success: function(dados) {
								
								if(dados.status == 1) {
									db.transaction(function(x) {
										x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, modulo]);
									});
									
									exibirStatusSincronismo(201, modulos[modulo]);
		
									realizarDownloadModulo(modulo, function(){
										iniciarSincronismo(iteracao + 1, callback);
									});
									
								}
								
							},
							error: function(retorno){
								
								exibirStatusSincronismo(417, modulos[modulo]);
								sincronizado = 4;
								iniciarSincronismo(iteracao + 1, callback);
							}
						});
						
					});
				});
				
			}
			
		} else {
			
			iniciarSincronismo(iteracao + 1, callback);
			
		}
		
	} else {
		
		if(!sincronizado) {
			
			callback(0);
			
		} else if(sincronizado == 2) {
			
			callback(2);
			
		} else if(sincronizado == 4) {
			
			callback(4);
			
		}
		
	}
}

/**
 * 
 * Função utilizada para realizar o download do arquivo zip referente a um módulo específico.
 * 
 * @param modulo	= Módulo a ser realizado o download
 * @param callback	= Função de callback
 */
function realizarDownloadModulo(modulo, callback) {
	var codigo_representante = localStorage.getItem('codigo');
	var id_usuario = localStorage.getItem('usuarios_id');
	$("#progressbar").progressbar({value: 0});
	
	console.log('@@@ - Iniciar Download 1 <' + config.ws_url + 'download/incremental/' + codigo_representante + '/' + id_usuario + '/' + modulo + '>');
	
	exibirStatusSincronismo(102, modulos[modulo]);
	
		// Realizando Download do pacote zip Unicos
		var download = new DownloadZip(config.ws_url + 'download/incremental/' + codigo_representante + '/' + id_usuario + '/' + modulo, function(){
			
			exibirStatusSincronismo(202, modulos[modulo]);
			
			console.log('@@@ - Extraindo Pacotes');
			// Extraindo pacote zip do Representante
			var ZipClient = new ExtractZipFilePlugin();
			
			ZipClient.extractFile(localStorage.getItem('caminho_local') + modulo + ".zip", function(){
				
				obterTotalPacotesModulo(modulo, function(){
					callback();
				});
				
			}, zipErro);
			
			function zipErro(error) {
			    console.log('[DOWNLOAD] - ERRO');
				console.log(JSON.stringify(error));
				
				exibirStatusSincronismo(404, modulos[modulo]);
				
				sincronizado = 4;
			}
		});
		
		download.iniciarDownload();
}

/**
 * 
 * Obtem o total de pacotes do modulo para realizar o sincronismo
 * 
 * @param modulo	= Nome do módulo a ser sincronizado
 * @param callback	= Função de callback
 */
function obterTotalPacotesModulo(modulo, callback) {
	console.log('@@@@@@@@@@@@@@@@ INICIO SINCRONIZACAO DADOS @@@@@@@@@@@@@@@@@@');
	ajax = $.ajax({
		url: localStorage.getItem('caminho_local') + modulo + '_total.json',
		success: function(dados) {
			var total_pacotes = dados.total;
			if(total_pacotes == 0) {
				total_pacotes = 1;
			} 
			console.log('[SYNC] - ' + total_pacotes + ' pacotes do modulo <' + modulo + '> a serem sincronizados');
			sincronizarDadosModulo(modulo, total_pacotes, 1, function(){
				callback();
			});
		}
	});
}

/**
 * 
 * Realiza a inserção ou atualização dos dados do pacote para a base local.
 * Se o registro já existir realiza o UPDATE caso contrário faz o INSERT.
 *
 * Obs.: Somente para as tabelas do Protheus, tabelas do MySQL são deletadas e reinseridas
 * Obs: Chama a função recursivamente até encerrar todos os pacotes referente ao módulo
 * 
 * @param modulo			= Nome do módulo
 * @param total_pacotes		= Quantidade de pacotes referente ao módulo
 * @param pacote_atual		= Iteração atual do pacote
 * @param callback			= Função de callback
 */
function sincronizarDadosModulo(modulo, total_pacotes, pacote_atual, callback) {
	
	
	
	
	var registro_atual = 0;
	var descricaoModulo = modulos[modulo];
	if(modulo == 'catalogos') {
		descricaoModulo = 'Catálogos';
	}
	$("#progressbar").progressbar({value: 0});
	
	ajax = $.ajax({
		url: localStorage.getItem('caminho_local') + modulo + '_' + pacote_atual + '.json',
		success: function(result) {
			console.log('pacote atual: '+pacote_atual );
			if(pacote_atual == 1) { //Exibe a mensagem somente na primeira iteração
				exibirStatusSincronismo(104, descricaoModulo);
			}
			
			exibirStatusSincronismo(100, descricaoModulo, pacote_atual, total_pacotes);
			
			

			var validar_campos = new Array();
			$.each(result[0], function(indice, valor) {
				if(indice != 'dw_delecao' && indice != 'rnum') {
					validar_campos.push(indice);
				}
			});
			
	db.transaction(function(x) {
		x.executeSql('SELECT ' + validar_campos.join(', ') + ' FROM ' + modulo + ' LIMIT 1', [], function(){
			
			var dados = result;
			sincronizarRegistro(pacote_atual, total_pacotes, registro_atual, modulo, dados, callback);
			
			
		},
		function(xe, erro)
		{
			
			if(erro.code == 5)
			{
				
			
				
				var mensagem_erro = erro.message;
				var retorno = mensagem_erro.split(":");
				var campo = trim(retorno[1]);
				
				
				
				campo = str_replace(')','', campo);
				
				console.log(retorno);
				
				if(campo)
				{
					console.log(' -- ERRO COLUNA -- ');
					console.log('Coluna não existe: ' + campo);
					
					x.executeSql('ALTER TABLE ' + modulo + ' ADD ' + campo + ' TEXT', [], function () {
						console.log('Tabela: ' + modulo + ' SQL: ' + campo + ' Tabela alterada com sucesso.');
						
						console.log('Sincronizando novamente');
						sincronizarDadosModulo(modulo, total_pacotes, pacote_atual, callback);
						return false;
			//			verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, inserir);

						
					}, function(xy, error){
						console.log('Tabela: ' + modulo + ' SQL: ' + campo + ' Não foi possível alterar: ' + error.message);
					});
					
					console.log(' -- FIM -- ');
					
				}
				else
				{
					console.log(erro);
				}
			}
			
		});
				
			});
			
		},
		error: function(e){
			console.log('erro 2');
			
			exibirStatusSincronismo(406, descricaoModulo);
			sincronizado = 4;
			callback();
		}
	});
}

function sincronizarRegistro(pacote_atual, total_pacotes, registro_atual, modulo, registros, callback){
	//console.log('REgistro Atual: '+ registro_atual);
	
	var descricaoModulo = modulos[modulo];
	var registro = registros[registro_atual];
		
	var campos					= new Array();
	var interrogacoes			= new Array();
	var valores					= new Array();
	var recno					= null;
	var total_registros_pacote	= registros.length;
	var deletar					= false;
	var tabela_precos			= false;
	var codigo_produto			= false;
	var tabela_mysql			= true;	
	var debug = new Array();
		
		
		// Barra de progresso
	$("#progressbar").progressbar({value: (100 / total_registros_pacote) * registro_atual});
		
	
	$.each(registro, function(indice, valor) {
		if(indice != 'dw_delecao' && indice != 'rnum') {
			campos.push(indice);
			interrogacoes.push('?');
			valores.push(valor);
			
			
			debug.push(indice + " = '"+valor+"' ");
			
			if(indice == 'recno') {
				tabela_mysql = false;
				recno = valor;
			}
			
			if(indice == 'ptp_codigo_tabela_precos') {
				tabela_precos = valor;
			} else if(modulo == 'formas_pagamento' && indice == 'codigo_tabela') {
				tabela_precos = valor;
			}
			
			
			
			if(modulo == 'notas_fiscais'  && indice == 'produto_codigo'){
				codigo_produto = valor;
				
			}
		} else if(indice == 'dw_delecao') {
			if(valor == '*') {
				deletar = true;
			}
		}
	});
	console.log('RECNO: '+typeof(recno) + ': '+ recno);	
	console.log('Tab: '+ typeof(tabela_mysql) +':'+ tabela_mysql);
	if(deletar) { //Remove o registro					
		var where = '';
		if(modulo == 'produtos') {
			where += 'AND ptp_codigo_tabela_precos = "' + tabela_precos + '"';
		} else if(modulo == 'formas_pagamento') {
			where += 'AND codigo_tabela = "' + tabela_precos + '"';
		}
		
		db.transaction(function(x) {
			x.executeSql('DELETE FROM ' + modulo + ' WHERE recno = ? ' + where, [recno], function(x, dados) {
				registro_atual++;
				
				console.log('[SYNC] Pacote <' + pacote_atual + ' de ' + total_pacotes + '> - Registro <' + registro_atual + ' de ' + total_registros_pacote + '> - Módulo <' + modulo + '>');
				console.log('[SYNC] - Registro <' + recno + '> deletado');
				$("#progressbar").progressbar({value: (100 / total_registros_pacote) * registro_atual});
				
				if(registro_atual === total_registros_pacote && pacote_atual === total_pacotes){
				
				
					db.transaction(function(x) {	
						x.executeSql('SELECT MAX(dt_atualizado) AS ultima_atualizacao FROM ' + modulo + ' ', [], 
								function(y, dados) {
									var dt_atualizado = dados.rows.item(0)['ultima_atualizacao'];
									
									x.executeSql('UPDATE sincronizacoes SET sucesso = ?, dt_atualizado = ? WHERE modulo = ?', [1, dt_atualizado, modulo], function (tx, result) {
										console.log("Módulo: "+modulo+" - Atualizado registro maximo");							            
										callback();									
									});
								},
								function (tx, error) {								
									x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, modulo], function (tx, result) {
										console.log("Módulo: "+modulo+" - Não incremental");
										callback();							            									
									});
				                }
							);
						
						});
				
				}else if(registro_atual === total_registros_pacote && pacote_atual < total_pacotes){					
					sincronizarDadosModulo(modulo, total_pacotes, pacote_atual+1, callback);					
				}else{				
					sincronizarRegistro(pacote_atual, total_pacotes, registro_atual, modulo, registros, callback);
					
				}
				/*
				if(registro_atual == total_registros_pacote) {
					exibirStatusSincronismo(100, descricaoModulo, pacote_atual, total_pacotes);
				}
				
				if(pacote_atual == total_pacotes && registro_atual == total_registros_pacote) {
					
					exibirStatusSincronismo(200, descricaoModulo);
					
					console.log('@@@@@@@@@@@@@@@@ FIM SINCRONIZACAO DADOS @@@@@@@@@@@@@@@@@@');
					
					//20141228 - Aqui salva a nova data de atualização.
					
					db.transaction(function(x) {	
					x.executeSql('SELECT MAX(dt_atualizado) AS ultima_atualizacao FROM ' + modulo + ' ', [], 
							function(y, dados) {
								var dt_atualizado = dados.rows.item(0)['ultima_atualizacao'];
								
								x.executeSql('UPDATE sincronizacoes SET sucesso = ?, dt_atualizado = ? WHERE modulo = ?', [1, dt_atualizado, modulo], function (tx, result) {
									console.log("Módulo: "+modulo+" - Atualizado registro maximo");							            
									callback();									
								});
							},
							function (tx, error) {								
								x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, modulo], function (tx, result) {
									console.log("Módulo: "+modulo+" - Não incremental");
									callback();							            									
								});
			                }
						);
					
					});
				} else if(total_pacotes > 1 && registro_atual == total_registros_pacote) {
					console.log('Passou');
					console.log('Total pacote: '+ total_pacotes+' total_registros: '+ total_registros_pacote );
					console.log('Registro Atual: '+ total_pacotes+' Registro Atual: '+ registro_atual);
					
					$("#progressbar").progressbar({value: (100 / total_pacotes) * pacote_atual});
					sincronizarDadosModulo(modulo, total_pacotes, pacote_atual + 1, callback);
					
				}*/
			});
		});
		
	} else if(recno){ //Tabela do Protheus
											
			var where = '';
			if(modulo == 'produtos') {
				where += 'AND ptp_codigo_tabela_precos = "' + tabela_precos + '"';
			} else if(modulo == 'formas_pagamento') {
				where += 'AND codigo_tabela = "' + tabela_precos + '"';
			}else if(modulo == 'notas_fiscais'){
				where += " AND produto_codigo = '"+codigo_produto+"'";				
			}
			
			var sql =  'UPDATE ' + modulo + ' SET ' + debug.join(', ') + '  WHERE recno = "' + recno + '" ' + where; 
			
			console.log('sql: '+sql);
			
			
			//x.executeSql('UPDATE ' + modulo + ' SET ' + campos.join(' = ?, ') + ' = ? WHERE recno = "' + recno + '" ' + where, valores, function(x, dados){
			db.transaction(function(x) {
				x.executeSql(sql, [], function(x, dados){
					
					if(dados.rowsAffected < 1) {
						
						x.executeSql('INSERT INTO ' + modulo + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
							registro_atual++;
							console.log('Registro Atual<INSERT>: '+registro_atual + ' ' + total_registros_pacote + 'pacote atual: '+ pacote_atual+ ' total pacotes: '+ total_pacotes);
							
							
							if(registro_atual === total_registros_pacote && pacote_atual === total_pacotes){								
								exibirStatusSincronismo(200, modulo);
								console.log('@@@@@@@@@@@@@@@@ FIM SINCRONIZACAO DADOS @@@@@@@@@@@@@@@@@@');
								db.transaction(function(x) {
									
									x.executeSql('SELECT MAX(dt_atualizado) AS ultima_atualizacao FROM ' + modulo + ' ', [], 
											function(y, dados) {
												var dt_atualizado = dados.rows.item(0)['ultima_atualizacao'];
												
												x.executeSql('UPDATE sincronizacoes SET sucesso = ?, dt_atualizado = ? WHERE modulo = ?', [1, dt_atualizado, modulo], function (tx, result) {
													console.log("Módulo: "+modulo+" - Atualizado registro maximo");									            
													callback();									            
												});
											},
											function (tx, error) {	
												
												x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, modulo], function (tx, result) {
													console.log("Módulo: "+modulo+" - Não incremental");
													callback();
								            
										
												});
							                }
										);
										
										
									});
								
							}else if(registro_atual === total_registros_pacote && pacote_atual < total_pacotes){
								
								sincronizarDadosModulo(modulo, total_pacotes, pacote_atual+1, callback);
								
								
							}else{
							
								sincronizarRegistro(pacote_atual, total_pacotes, registro_atual, modulo, registros, callback);
								
							}
						});
					
					}else{
											
						registro_atual++;
						console.log('Registro Atual<UPDATE - ('+recno+')>: '+registro_atual + ' ' + total_registros_pacote + 'pacote atual: '+ pacote_atual+ ' total pacotes: '+ total_pacotes);
					
						if(registro_atual === total_registros_pacote && pacote_atual === total_pacotes){
							
							
							
							
							exibirStatusSincronismo(200, modulo);
							console.log('@@@@@@@@@@@@@@@@ FIM SINCRONIZACAO DADOS @@@@@@@@@@@@@@@@@@');
							db.transaction(function(x) {
								
								x.executeSql('SELECT MAX(dt_atualizado) AS ultima_atualizacao FROM ' + modulo + ' ', [], 
										function(y, dados) {
											var dt_atualizado = dados.rows.item(0)['ultima_atualizacao'];
											
											x.executeSql('UPDATE sincronizacoes SET sucesso = ?, dt_atualizado = ? WHERE modulo = ?', [1, dt_atualizado, modulo], function (tx, result) {
												console.log("Módulo: "+modulo+" - Atualizado registro maximo");									            
												callback();									            
											});
										},
										function (tx, error) {	
											
											x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, modulo], function (tx, result) {
												console.log("Módulo: "+modulo+" - Não incremental");
												callback();
							            
									
											});
						                }
									);
									
									
								});
							
							
							
						}else if(registro_atual === total_registros_pacote && pacote_atual < total_pacotes){
							sincronizarDadosModulo(modulo, total_pacotes, pacote_atual+1, callback);							
						}else{						
							sincronizarRegistro(pacote_atual, total_pacotes, registro_atual, modulo, registros, callback);							
						}
					}
					
					
					/*$("#progressbar").progressbar({value: (100 / total_registros_pacote) * registro_atual});
					
					console.log('[SYNC] Pacote <' + pacote_atual + ' de ' + total_pacotes + '> - Registro <' + registro_atual + ' de ' + total_registros_pacote + '> - Módulo <' + modulo + '>');
					if(registro_atual == total_registros_pacote) {
						exibirStatusSincronismo(100, descricaoModulo, pacote_atual, total_pacotes);
					}
					
					if(dados.rowsAffected < 1) {
						
						x.executeSql('INSERT INTO ' + modulo + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
							console.log('[SYNC] - Registro <' + recno + '> inserido');
							
							//Se o pacote atual for igual ao total de pacotes e o registro inserido for igual ao total de registros do pacote
							//Encerra o processo de sincronismo
							if(pacote_atual == total_pacotes && registro_atual == total_registros_pacote) {
								
								exibirStatusSincronismo(200, descricaoModulo);
								console.log('@@@@@@@@@@@@@@@@ FIM SINCRONIZACAO DADOS @@@@@@@@@@@@@@@@@@');
								
								db.transaction(function(x) {											
									x.executeSql('SELECT MAX(dt_atualizado) AS ultima_atualizacao FROM ' + modulo + ' ', [], 
											function(y, dados) {
												var dt_atualizado = dados.rows.item(0)['ultima_atualizacao'];
												
												x.executeSql('UPDATE sincronizacoes SET sucesso = ?, dt_atualizado = ? WHERE modulo = ?', [1, dt_atualizado, modulo], function (tx, result) {
													console.log("Módulo: "+modulo+" - Atualizado registro maximo");
								            
													callback(); 
										
												});
											},
											function (tx, error) {
												
												x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, modulo], function (tx, result) {
													console.log("Módulo: "+modulo+" - Não incremental");
													callback();
								            	});
							                }
										);												
									});
								
								//callback();
								
							} else if(total_pacotes > 1 && registro_atual == total_registros_pacote) {
								mensagem('Proximo pacote');
								$("#progressbar").progressbar({value: (100 / total_pacotes) * pacote_atual});
								sincronizarDadosModulo(modulo, total_pacotes, pacote_atual + 1, callback);
							}
						});
						
					} else {
						
						console.log('[SYNC] - Registro <' + recno + '> atualizado');
						
						//Se o pacote atual for igual ao total de pacotes e o registro atualizado for igual ao total de registros do pacote
						//Encerra o processo de sincronismo
						if(pacote_atual == total_pacotes && registro_atual == total_registros_pacote) {
							
							exibirStatusSincronismo(200, descricaoModulo);
							console.log('@@@@@@@@@@@@@@@@ FIM SINCRONIZACAO DADOS @@@@@@@@@@@@@@@@@@');
							db.transaction(function(x) {
								
								x.executeSql('SELECT MAX(dt_atualizado) AS ultima_atualizacao FROM ' + modulo + ' ', [], 
										function(y, dados) {
											var dt_atualizado = dados.rows.item(0)['ultima_atualizacao'];
											
											x.executeSql('UPDATE sincronizacoes SET sucesso = ?, dt_atualizado = ? WHERE modulo = ?', [1, dt_atualizado, modulo], function (tx, result) {
												console.log("Módulo: "+modulo+" - Atualizado registro maximo");									            
												callback();									            
											});
										},
										function (tx, error) {	
											
											x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, modulo], function (tx, result) {
												console.log("Módulo: "+modulo+" - Não incremental");
												callback();
							            
									
											});
						                }
									);
									
									
								});
							
						} else if(total_pacotes > 1 && registro_atual == total_registros_pacote) {
							
							$("#progressbar").progressbar({value: (100 / total_pacotes) * pacote_atual});
							sincronizarDadosModulo(modulo, total_pacotes, pacote_atual + 1, callback);
							
						}
						
					}*/
					
					
				}, function(transaction, error){
					console.log('Ocorreu um erro durante a sincronização');
					console.log(' x x x x x x x x x x ');
					console.log(sql);
					console.log(' x x x x x x x x x x ');
					sincronizado = 4;
					
					callback();
					//sincronizarDadosModulo(modulo, total_pacotes, pacote_atual + 1, callback);
					
				});
			});
			
		} else if(recno == null && !tabela_mysql) { //Pacote vazio
			registro_atual++;
			console.log('Passou aqui');
			if(registro_atual === total_registros_pacote && pacote_atual === total_pacotes){
				
				exibirStatusSincronismo(304, descricaoModulo);
				console.log('@@@@@@@@@@@@@@@@ FIM SINCRONIZACAO DADOS @@@@@@@@@@@@@@@@@@');
				
				callback();
				
			}
			
		} else { //Tabelas do MySQL
			console.log('mysql');
		
			db.transaction(function(x) {
				//Se for a primeira iteração, remove os registros antigos e reinsere os novos dados
				if(pacote_atual == 1 && registro_atual == 0) {
					x.executeSql('DELETE FROM ' + modulo);
				}
				
				var vazio = true;
				$.each(valores,function(i,campo){
					if(!is_null(campo)){
						vazio = false;
					}
					
				});
				
				
				if(!vazio){
						console.log('inserindo');
					
						x.executeSql('INSERT INTO ' + modulo + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
						registro_atual++;
						
						$("#progressbar").progressbar({value: (100 / total_registros_pacote) * registro_atual});
						
						console.log('[SYNC] - Registro <' + registro_atual + '> inserido');
						
						
						
						
						if(registro_atual === total_registros_pacote && pacote_atual === total_pacotes){
							
							exibirStatusSincronismo(200, descricaoModulo);
							console.log('@@@@@@@@@@@@@@@@ FIM SINCRONIZACAO DADOS @@@@@@@@@@@@@@@@@@');							
							
							db.transaction(function(x) {							
							x.executeSql('SELECT MAX(dt_atualizado) AS ultima_atualizacao FROM ' + modulo + ' ', [], 
									function(y, dados) {
										var dt_atualizado = dados.rows.item(0)['ultima_atualizacao'];										
										x.executeSql('UPDATE sincronizacoes SET sucesso = ?, dt_atualizado = ? WHERE modulo = ?', [1, dt_atualizado, modulo], function (tx, result) {
											console.log("Módulo: "+modulo+" - Atualizado registro maximo");						            
											callback();   
										});
									},
									function (tx, error) {			
										
										
										x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, modulo], function (tx, result) {
											console.log("Módulo: "+modulo+" - Não incremental");
											callback();
										});
					                }
								);
							
							});
							
						} else if(registro_atual === total_registros_pacote && pacote_atual < total_pacotes){
							sincronizarDadosModulo(modulo, total_pacotes, pacote_atual+1, callback);
						}else{
							sincronizarRegistro(pacote_atual, total_pacotes, registro_atual, modulo, registros, callback);
						}
						
					});
				}else{
					
					callback();
					
				}
			});
			
		}
		
		
		
}


/**
 * 
 * Exibe o status da sincronização do módulo
 * 
 * Mensagens de info/erro/sucesso
 * 	Padrão de Numeração:
 * 		1xx - Informativo
 * 		2xx - Sucesso
 * 		4xx - Erro
 * 
 * @param tipo			= Tipo de mensagem (erro, info ou sucesso)
 * @param modulo		= Descrição do Módulo
 * @param pacoteAtual	= Iteração do Pacote
 * @param totalPacotes	= Total de Pacotes do Módulo
 */
function exibirStatusSincronismo(tipo, modulo, pacoteAtual, totalPacotes) {
	var classe = "info";
	var mensagem = "";
	
	switch(tipo) {
		case 100: //Info
				mensagem = "Atualizando pacote " + pacoteAtual + " de " + totalPacotes + " do módulo <b>" + modulo + "</b>.";
			break;
			
		case 102: //Info Download
			mensagem = "Realizando download do módulo de <b>" + modulo + "</b>.";
		break;
		
		case 103: //Info Gerar Arquivo
			mensagem = "Gerando dados do módulo <b>" + modulo + "</b>.";
		break;
		
		case 104: //Atualizando módulo
			mensagem = "Atualizando dados do módulo <b>" + modulo + "</b>.";
		break;
		
		case 200: //Sucesso
				classe = "sucesso";
				mensagem = "Módulo de <b>" + modulo + "</b> atualizado com sucesso.";
			break;
		
		case 201: //Sucesso Gerar Arquivo
			classe = "sucesso";
			mensagem = "Arquivo do módulo de <b>" + modulo + "</b> gerado com sucesso.";
		break;
		
		case 202: //Sucesso Download
				classe = "sucesso";
				mensagem = "Download do módulo de <b>" + modulo + "</b> realizado com sucesso.";
			break;
		
		case 304: //Sucesso Sem Atualização
			classe = "sucesso";
			mensagem = "Nenhuma modificação nos dados do módulo de <b>" + modulo + "</b>.";
		break;
		
		case 404: //Erro Download
				classe = "erro";
				mensagem = "Erro ao realizar o download do módulo de <b>" + modulo + "</b>.";
			break;
		
		case 406: //Erro Leitura do Arquivo 
			classe = "erro";
			mensagem = "Não foi possível sincronizar o módulo <b>" + modulo + "</b> - Erro ao realizar a Leitura do Arquivo.";
		break;
		
		case 417: //Erro Gerar Arquivo
				classe = "erro";
				mensagem = "Erro ao gerar o arquivo do módulo de <b>" + modulo + "</b>.";
			break;
	}
	
	
	if(classe == "sucesso") {
		$("#progressbar").progressbar({value: 100});
	} else if(classe == "erro") {
		$("#progressbar").progressbar({value: 0});
	}
	
	$("#statusSincronismo").prepend("<div class=" + classe + ">" + tipo + " - " + mensagem + "</div>");
}





/**
 * Sincronização de catálogos
 */
function sincronizarCatalogos() {
	$(this).dialog('close');
	$("#confirmar_dialog").remove();
	
	$("#boxLabelSincronismo").show();
	$("#labelStatusSincronismo").html("Sincronização em andamento...");
	$("#progressbar").show();
	$("#botoesSincronismo").hide();
	$("#statusSincronismo").html("");
	$("#statusSincronismo").show();
	$("#modulos").hide();
	$("#btnVisualizarLogSync").hide();
	$("#btnVisualizarModulosSync").hide();
	
	console.log('@@@ - Iniciar modulos selecionados');
	
	exibirStatusSincronismo(103, 'Catálogos');
	
	var codigo_representante = localStorage.getItem('codigo');
	var id_usuario = localStorage.getItem('usuarios_id');
	
	db.transaction(function(x) {
		x.executeSql('INSERT INTO sincronizacoes (timestamp, modulo, sucesso) VALUES (?, ?, ?)', [time(), 'catalogos', 0]);
	});
	
	var ajax = $.ajax({
		url: config.ws_url + 'incremental/gerar',
		type: 'POST',
		data: {
			codigo_representante: codigo_representante,
			id_usuario: id_usuario,
			modulo: 'catalogos',
			ultima_sincronizacao: false,
			incremental: false
		},
		beforeSend: function(){
			$("#progressbar").progressbar({value: 50});
		},
		success: function(dados) {
			
			if(dados.status == 1) {
				db.transaction(function(x) {
					x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, 'catalogos']);
				});
				
				exibirStatusSincronismo(201, 'Catálogos');
				
				iniciarDownloadCatalogos(function(){
					
					sincronizarDadosCatalogo(function(){
						finalizarSincronismo(2);
					});
					
				});
				
			}
			
		},
		error: function(retorno){
			
			exibirStatusSincronismo(417, 'Catálogos');
			finalizarSincronismo(4);
			
		}
	});
		
}

function iniciarDownloadCatalogos(callback)
{
	var codigo_representante = localStorage.getItem('codigo');
	var id_usuario = localStorage.getItem('usuarios_id');
	
	console.log('@@@ - Iniciar Download 3 <' + config.ws_url + 'download/incremental/' + codigo_representante + '/' + id_usuario + '/catalogos>');
	// Realizando Download do pacote zip Catálogos
	
	var download = new DownloadZip(config.ws_url + 'download/incremental/' + codigo_representante + '/' + id_usuario + '/catalogos', function(){
		console.log('@@@ - Extraindo Pacotes');
		// Extraindo pacote zip dos Catalogos 
		var ZipClient = new ExtractZipFilePlugin();
		
		$('#carregando').find('span:first').html('Extraindo Pacotes…');
		ZipClient.extractFile(localStorage.getItem('caminho_local') + "catalogos.zip", function(){
			
			console.log('Catálogos Extraidos.');
			
			callback();	
			
		}, zipErro);
		

		function zipErro(error) 
		{ 
			console.log('[DOWNLOAD] - ERRO');
			console.log(JSON.stringify(error));
			
			apprise('A sincronização dos catálogos não foi concluída. Você deseja tentar novamente?', {
				'verify': true,
				'textYes': 'Sim',
				'textNo': 'Não'
			}, function (dado) {
				if (dado)
				{
					window.location = 'sincronizar_incremental.html';
				}
				else
				{
					window.location = 'index.html';
				}
			});
		}
							
	});
	download.iniciarDownload();		
}

function sincronizarDadosCatalogo() {
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM catalogos LIMIT 1', [], function(){ //Verifica se a tabela ja existe
			x.executeSql('DELETE FROM catalogos');
			
			ajax = $.ajax({
				url: localStorage.getItem('caminho_local') + 'catalogos_total.json',
				success: function(dados) {
					sincronizarDadosModulo('catalogos', dados.total, 1, function(){
						finalizarSincronismo(2, '');
					});
				}
			});
			
		}, function(){
			
			ajax = $.ajax({
				url: localStorage.getItem('caminho_local') + 'catalogos_1.json',
				success: function(dados) {
					criar_tabela('catalogos', dados, true);
					
					ajax = $.ajax({
						url: localStorage.getItem('caminho_local') + 'catalogos_total.json',
						success: function(dados) {
							criar_tabela('catalogos', dados, true);
							
							sincronizarDadosModulo('catalogos', dados.total, 1, function(){
								finalizarSincronismo(2, '');
							});
						}
					});
				}
			});
			
		});
	});
}