$(document).ready(function() {
	
	//Código da nota fiscal e serie
	var	codigo_e_serie 		= parse_url(location.href).fragment;
	//Array criado com codigo e serie
	var	array_codigo_e_serie 	= codigo_e_serie.split('_');
	//Código nota fiscal
	var codigo_da_nota 			= array_codigo_e_serie[0];
	//Serie da nota fiscal
	var serie_da_nota 			= array_codigo_e_serie[1];
	//Filial da nota fiscal
	var filial_da_nota 			= array_codigo_e_serie[2];
	
	// Obter Pedido Processado
	db.transaction(function(x) {
		x.executeSql(
				'SELECT * FROM notas_fiscais WHERE nota_codigo = ? AND nota_serie = ? AND nota_filial = ? ORDER BY nota_codigo DESC', [codigo_da_nota, serie_da_nota, filial_da_nota], function(x, dados) {
					
					console.log('SELECT * FROM notas_fiscais WHERE nota_codigo = '+codigo_da_nota+' AND nota_serie = '+serie_da_nota+' AND nota_filial = '+filial_da_nota+' ORDER BY nota_codigo DESC');
					
					if (dados.rows.length)
					{
						var nota = dados.rows.item(0);
						
						console.log(nota);
						
						//Informações do pedido
						$('.item_codigo_pedido').text(nota.item_codigo_pedido);
						$('.nota_filial').text(nota.nota_filial);
						$('.clinete_codigo_loja').text(nota.cliente_codigo+' / '+nota.cliente_loja);
						
						//Informações gerais
						$('.codigo_e_representante').text(info.cod_rep + ' / ' + info.nome_representante);
						$('.codigo_e_serie').text(nota.codigo_generico);
						
						//Cliente
						$('.cliente_nome').text(nota.cliente_nome);
						$('.cliente_cnpj_cpf').text(formatar_cpfCNPJ(nota.cliente_cpf));
						$('.cliente_inscricao_estadual').text(nota.cliente_inscricao_estadual);
						$('.cliente_inscricao_municipal').text(nota.cliente_inscricao_municipal);
						$('.cliente_municipio').text(nota.cliente_cidade);
						$('.cliente_bairro').text(nota.cliente_bairro);
						$('.cliente_uf').text(nota.cliente_estado);
						$('.cliente_cep').text(nota.cliente_cep);
						
						//Nota Fiscal
						if(nota.nota_data_emissao){
							$('.nota_emissao').text(protheus_data2data_normal(nota.nota_data_emissao));
						}
						if(nota.nota_data_entrega){
							$('.nota_data_de_entrega').text(protheus_data2data_normal(nota.nota_data_entrega));
						}						
						$('.transportadora_nome').text(nota.transportadora_codigo+ ' / ' +nota.transportadora_nome);
						if(nota.transportadora_telefone){
							$('.transportadora_telefone').text(nota.transportadora_telefone);
						}
						
						//Cálculo do Imposto
						$('.nota_valor_icms').text(formatar_moeda(nota.nota_valor_icms, 3));
						$('.nota_valor_icms_st').text(formatar_moeda(nota.nota_valor_icms_st, 3));
						$('.nota_base_cal_icms').text(formatar_moeda(nota.nota_base_calculo_icms, 3));
						$('.nota_base_cal_icms_st').text(formatar_moeda(nota.nota_base_calculo_icms_st, 3));
						
						$('.nota_valor_total_produto').text(formatar_moeda(nota.nota_valor_total_produto, 3));
						$('.nota_valor_frete').text(formatar_moeda(nota.nota_valor_frete, 3));
						$('.nota_valor_seguro').text(formatar_moeda(nota.nota_valor_seguro, 3));
						$('.nota_valor_desconto').text(formatar_moeda(nota.nota_valor_desconto, 3));
						
						$('.nota_valor_ipi').text(formatar_moeda(nota.nota_valor_ipi, 3));
						$('.nota_valor_total').text(formatar_moeda(nota.nota_valor_total, 3));
						$('.nota_despesas_necessarias').text(formatar_moeda(nota.nota_despesas_necessarias, 3));
						
						//---------------------------------------------------------------------------------------
						// ITENS DA NOTA FISCAL
						//---------------------------------------------------------------------------------------
						var itens 				= dados.rows;
						var total_itens 		= dados.rows.length;
						var total_quantidade 	= 0;
						var total_preco 		= 0;
						var valor_total  		= 0;
						if(total_itens > 0)
						{
							//Varrer itens da nota fiscal
							for( i=0 ; i < total_itens; i++)
							{
								item 				= itens.item(i);
								
								console.log(item);
								
								total_quantidade 	+= parseFloat(item.item_quantidade_entrega);
								total_preco 		+= parseFloat(item.item_total_faturado);
							
								var html = '<tr>';
									html += '<td>'+item.item_codigo_produto+'</td>';
									html += '<td>'+item.produto_descricao+'</td>';
									html += '<td>'+item.item_unidade_medida+'</td>';
									html += '<td align="right">'+formatar_moeda(item.item_quantidade_entrega, 3)+'</td>';
									html += '<td align="right">'+formatar_moeda(item.item_preco_unitario, 3)+'</td>';
									html += '<td align="right">'+formatar_moeda(item.item_ipi, 3)+'</td>';
									html += '<td align="right">'+formatar_moeda(item.item_st, 3)+'</td>';
									html += '<td align="right">'+formatar_moeda(item.item_base_calc_icms, 3)+'</td>';
									html += '<td align="right">'+formatar_moeda(item.item_valor_icms, 3)+'</td>';
									html += '<td align="right">'+formatar_moeda(item.item_aliq_icms, 3)+'</td>';
									html += '<td align="right">'+formatar_moeda(item.item_total_faturado, 3)+'</td>';									
									html += '</tr>';
								
								$('table#produtos  tbody').append(html);
							}
						}else{
							$('table#produtos  tbody').html('<tr><td colspan="11" style="color: #900; padding: 10px;">Nenhum produto encontrado.</td></tr>')
						}
						//---------------------------------------------------------------------------------------
						// ITENS DA NOTA FISCAL
						//---------------------------------------------------------------------------------------
												
						//Totais
						$('.forma_pagamento_descricao').text(nota.forma_pagamento_descricao);
						$('.total_quantidade').text(formatar_moeda(total_quantidade, 3));
						$('.total_faturado').text(formatar_moeda(nota.nota_valor_total, 3));

						alterarCabecalhoListagem('#produtos');
						alterarCabecalhoTabelaConteudo();
					}
				});
	});
});