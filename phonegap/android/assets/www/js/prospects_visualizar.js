var prospect;

$(document).ready(function() {
	
	//$('.caixa ul li a').button();
	var parametros = parse_url(location.href).fragment;
	var array_codigo_e_inclusao 	= parametros.split('|');
	
	var codigo_prospect = array_codigo_e_inclusao[0];
	var id = codigo_prospect;
	
	console.log(array_codigo_e_inclusao);
	
	var limpar_retorno		=isset(array_codigo_e_inclusao[1])?true:false;
	
	
	
	
	db.transaction(function(x) {
		
		
		x.executeSql(
			'SELECT prospects.*, eventos.nome AS nome_evento , ramos.descricao AS nome_ramo FROM prospects LEFT JOIN eventos ON eventos.id = prospects.id_feira LEFT JOIN ramos ON ramos.chave  = prospects.id_ramo WHERE codigo = ? ', [id], function(x, dados) {
				if (dados.rows.length)
				{
					prospect = dados.rows.item(0);
					
					if(!prospect.exportado)
					{
						
						$('#excluir_prospect').live('click', function(){

							apprise('Você deseja excluir esse prospect?', {
								'verify': true,
								'textYes': 'Sim',
								'textNo': 'Não'
							}, function (dado) {
								if (dado)
								{
									db.transaction(function(x) {
										x.executeSql('DELETE FROM prospects WHERE codigo = ?', [prospect.codigo], function(){
											window.location = 'prospects_aguardando.html';
										});
									});
								}
							});
						
						});
						
						
						$('#excluir_prospect').show();
						$('#editar_prospect').show();
						
						$('.enviar').show();
						
						
	
					}
					else if(prospect.status == 'A')
					{
						$('#solicitar_positivacao').live('click', function(){

							apprise('Você deseja positivar esse prospect?', {
								'verify': true,
								'textYes': 'Sim',
								'textNo': 'Não'
							}, function (dado) {
								if (dado)
								{
									db.transaction(function(x) {
										
										var tmp = new Array();
							
										tmp.push("editado = '1'");
										tmp.push("exportado = NULL");
										tmp.push("status = 'L'");
										
										console.log('Solicitando Positivação...');
										
										x.executeSql('UPDATE prospects SET ' + tmp.join(', ') + ' WHERE cgc = ?', [prospect.cgc], function(x, dados){
									
											console.log('Positivação Solicitada = ' + prospect.cgc);
											
											window.location = 'prospects_aguardando.html';
										
										});
					
										
									});
								}
							});
						
						});
						
						$('.solicitar_positivacao').show();
					}
					else if(prospect.status == 'L')
					{
						$('#editar_prospect').hide();
					}else if(prospect.status == 'P'){
						obter_grupo_permissao(function(grupo_permissao){
							
							if(grupo_permissao.grupo == 'representante'){
								
								$('.ver_detalhes_aprovar_preposto').show();
								$('.editar').show();
								$('.ver_detalhes_reprovar_preposto').show();
								$(".enviar").hide();
							}else{
								
								$(".enviar").show();
							}
						});
						
						
						
						
						
					}else if(prospect.status == 'R' &&  prospect.dw_delecao != '1' ){
						obter_grupo_permissao(function(grupo_permissao){
							$(".copiar").hide();
							if(grupo_permissao.grupo == 'preposto'){
								$('.editar').hide();
								$('.ver_detalhes_motivo_rejeicao_preposto').show();									
								$('.ver_detalhes_excluir_prospect_preposto').show();
								$(".enviar_pedido").hide();
							}else{
								$('.editar').show();
								$('.ver_detalhes_motivo_rejeicao_preposto').show();	
								
							}
						});
						
					}else if(prospect.dw_delecao == '1'){
						
						$(".copiar").hide();
						$('.editar').hide();
						$('#excluir').hide();
					}
					
					
					$('#editar_prospect').attr('href', 'prospects_editar.html#' + prospect.codigo);
					$('#criar_orcamento_prospect').attr('href', 'javascript: obter_prospect_orcamento(\''+prospect.codigo+'\');');
					
					
					$('td[id]').each(function() {
						
						var html = (prospect[$(this).attr('id')] ? prospect[$(this).attr('id')] : 'N/A');
					
						if ($(this).attr('id') == 'tipo')
						{
							html = obter_tipo_cliente(html);
						}
						
						if ($(this).attr('id') == 'id_feira')
						{
							if(prospect.id_feira && prospect.id_feira!= 0)
							{
								html = prospect.id_feira + ' - ' + prospect.nome_evento;
							}
							else
							{
								html = 'N/A';
							}
						}
						// CUSTOM: 177 / 000874 - 16/10/2013 - Localizar: CUST-RAMOS
						if ($(this).attr('id') == 'id_ramo')
						{
							console.log("ID_ramo: "+ prospect.id_ramo);
							
							if(prospect.id_ramo && prospect.id_ramo!= 0)
							{
								html = prospect.id_ramo + ' - ' + prospect.nome_ramo;
							}
							else
							{
								html = 'N/A';
							}
						}
						// FIM CUSTOM: 177 / 000874 - 16/10/2013
						
						
						// Verificação para Prospects Aguardando Sincronização
						if($(this).attr('id') == 'telefone')
						{
							var telefone 	= prospect.telefone;
							var ddd 		= prospect.ddd;
						
							if(telefone.length >= 8)
							{
								html = formatar_telefone(ddd+telefone);								
							}
							else
							{
								html = 'N/A';
							}
						}
						
						// Verificação para Prospects Aguardando Sincronização
						if($(this).attr('id') == 'fax')
						{
							var fax = prospect.fax;
							var ddd = prospect.ddd;
						
							if(fax.length >= 8)
							{
								html = formatar_telefone(ddd+fax);
							}
							else
							{
								html = 'N/A';
							}
						}
						
					
						
						//--------------
						
						if($(this).attr('id') == 'data_nascimento')
						{
							if(prospect.data_nascimento)
							{
								html = protheus_data2data_normal(prospect.data_nascimento);
							}
							else
							{
								html = 'N/A';
							}
						}
						
						if($(this).attr('id') == 'condicoes_pagamento')
						{
							if(prospect.condicoes_pagamento && prospect.condicoes_pagamento != '0')
							{
								obter_descricao_condicao(prospect.condicoes_pagamento);
							}
							else
							{
								html = 'N/A';
							}
						}
						
						$(this).html(html);

					});
					
					$('.nome').html(prospect.nome);
					$('.cgc').html(formatar_cpfCNPJ(prospect.cgc));
					
					
					$('input[name=pessoa_contato]').val(prospect.contato);
					$('input[name=email]').val(prospect.email);
					
					
					var cgc_prospect = prospect.cgc;
					
					x.executeSql(
						'SELECT * FROM historico_prospects WHERE cpf = ? ORDER BY id DESC', [cgc_prospect], function(x, dados) {
							if (dados.rows.length)
							{
								for (i = 0; i < dados.rows.length; i++)
								{
									var dado = dados.rows.item(i);
									
									$('table:last').append('<tr><td>' + date('d/m/Y', dado.timestamp) + '</td><td>' + dado.pessoa_contato + '</td><td>' + (dado.cargo ? dado.cargo : '-') + '</td><td>' + (dado.email ? dado.email : '-') + '</td><td>'+ (dado.protocolo ? dado.protocolo : '-') +'</td><td><a href="#" id="ver_historico" data-id="' + dado.id + '">Ver Detalhes</a></td></tr>');
									
									
								}
							}
							
							alterarCabecalhoTabelaConteudo();	
							alterarCabecalhoTabelaResolucao();
						}
					);
				}
				else
				{
					window.location = 'index.html';
				}
			}
		);
	});
	
	
	//CHAMADO 
	$('#enviar_prospect').click(function(e) {
		e.preventDefault();

		confirmar('Deseja enviar este Prospect?', 
		function () {
			$(this).dialog('close');
			
			//------------------
			//---------------------------------
	
			location.href = "sincronizar_incremental.html?codigo_prospect=" + codigo_prospect;
			
			//---------------------------------
			//------------------
			
			$("#confirmar_dialog").remove();
		});
	
});
	
	//FIM CHAMADO
	
	// ver histórico
	
	$('#ver_historico').live('click', function() {
		var id = $(this).attr('data-id');
		
		console.log('id = ' + id);
		
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM historico_prospects WHERE id = ?', [id], function(x, dados) {
					var historico = dados.rows.item(0);
					
					var html = '<table cellspacing="5">';
					html += '<tr><td><strong>Prospect:</strong></td><td>' + prospect.nome + '</td></tr>';
					html += '<tr><td><strong>Contato:</strong></td><td>' + historico.pessoa_contato + '</td></tr>';
					html += '<tr><td><strong>Cargo:</strong></td><td>' + (historico.cargo ? historico.cargo : '-') + '</td></tr>';
					html += '<tr><td><strong>E-mail:</strong></td><td>' + (historico.email ? historico.email : '-') + '</td></tr>';
					html += '</table>';
					
					html+= '<p>' + nl2br(historico.descricao) + '</p>';
					
					$.colorbox({ html: html, maxWidth: '50%' });
				}
			);
		});
		
		return false;
	});
	
	// novo histórico
	
	$('#link_novo_historico').click(function() {
		$(this).parents('p').hide();
		
		$('#novo_historico').show();
		
		return false;
	});
	
	// cancelar
	
	$('#cancelar').click(function() {
		$('#link_novo_historico').parents('p').show();
		
		$('#novo_historico').hide();
		
		return false;
	});
	
	// enviar form
	
	$('form').submit(function() {
		if (!$('input[name=pessoa_contato]').val())
		{
			mensagem('Digite uma pessoa de contato.');
		}
		else if ($('input[name=email]').val() && !validar_email($('input[name=email]').val()))
		{
			mensagem('Digite um e-mail válido.');
		}
		else if (!$('textarea[name=descricao]').val())
		{
			mensagem('Digite uma descrição para o histórico.');
		}
		else
		{
			db.transaction(function(x) {
				var id = parse_url(location.href).fragment;
				
				x.executeSql(
					'SELECT id FROM historico_prospects ORDER BY id DESC LIMIT 1', [], function(x, dados) {

						var id = uniqid();
						
						var tmp_1 = ['id', 'timestamp', 'id_prospect', 'cpf'];
						var tmp_2 = ['?', '?', '?', '?'];
						var tmp_3 = [id, String(time()), prospect.codigo, prospect.cgc];
						
						$('input[type!=submit], textarea').each(function() {
							tmp_1.push($(this).attr('name'));
							tmp_2.push('?');
							tmp_3.push($(this).val());
						});
						
						x.executeSql('INSERT INTO historico_prospects (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')', tmp_3);
						
						mensagem('Histórico adicionado com sucesso.');
						
						$('table:last').prepend('<tr><td>' + date('d/m/Y', time()) + '</td><td>' + $('input[name=pessoa_contato]').val() + '</td><td>' + ($('input[name=cargo]').val() ? $('input[name=cargo]').val() : 'N/A') + '</td><td>' + ($('input[name=email]').val() ? $('input[name=email]').val() : 'N/A') + '</td><td>' + ($('input[name=protocolo]').val() ? $('input[name=protocolo]').val() : 'N/A') + '</td><td><a href="#" id="ver_historico" data-id="' + id + '">Ver Detalhes</a></td></tr>');

						
						$('input[name=cargo], textarea[name=descricao]').val('');
						
						$('#cancelar').trigger('click');
						
						scroll('h3:last');
						
						alterarCabecalhoListagem('#historicos .novo_grid');
					}
				);
			});
		}
		
		return false;
	});
	
	
//Preposto
	
	$('#aprovar').click(function(e) {
		 e.preventDefault();
		 
		 mensagem = 'Deseja aprovar este Prospect?';
		
		 
		 confirmar(mensagem, function () {
			 $(this).dialog('close');
		  
			 aprovar_prospect(codigo_prospect);
			 
		  	 $("#confirmar_dialog").remove();
		 });
	});
	
	
	function aprovar_prospect(codigo_do_prospect){
		db.transaction(function(x) {
			x.executeSql("UPDATE prospects SET status = 'L' , exportado = null , editado=1 WHERE codigo = ?", [codigo_do_prospect],function(){
				window.location = 'prospects_aguardando.html';
				
			});
		});
	}
	
	
	
	
	$('#reprovar').click(function(e) {
		 e.preventDefault();
		 
		 mensagem = 'Motivo da rejeição: <br/>';
		 mensagem += '<textarea style="width:80%; heigth:60px;" maxlength="60" name="motivo_reprovacao">';
		 
		 mensagem += '</textarea>';
		 
		 confirmar(mensagem, function () {
			 var motivo_reprovacao = $('textarea[name=motivo_reprovacao]').val();
			 reprovar_prospect(codigo_prospect, motivo_reprovacao);
			 $(this).dialog('close');
		  	 $("#confirmar_dialog").remove();
		 });
	});
	
	
	
	function reprovar_prospect(codigo_prospect, motivo_reprovacao){
		
		console.log('Reprovado: '+ codigo_prospect);
		console.log('Motivo: '+motivo_reprovacao);
		db.transaction(function(x) {
			x.executeSql("UPDATE prospects SET status = 'R' , motivo_reprovacao=? , exportado = null , editado=1 WHERE codigo = ?", [motivo_reprovacao, codigo_prospect],function(){
				window.location = 'prospects_aguardando.html';
				
			});
		});
	}
	
	
	$(".ver_detalhes_motivo_rejeicao_preposto").live('click',function(){
		
		
		exibir_motivo_rejeicao(codigo_prospect);
		
	});
	
	
	function exibir_motivo_rejeicao(codigo_prospect){
						
		db.transaction(function(x) {
		x.executeSql(
				'SELECT motivo_reprovacao  FROM prospects  WHERE codigo = ? ', [codigo_prospect, ], function(x, dados) {
					if (dados.rows.length)
					{
						dado = dados.rows.item(0);
						mensagem( dado.motivo_reprovacao , '', 'Motivo da Rejeição');
					}
					
				}
			);
		});	
		
	}
	
	
	
	$('#excluir_prospect_preposto').click(function(e) {
		e.preventDefault();
		
		confirmar('Deseja excluir este Prospect?', 
			function () {
				$(this).dialog('close');
				
				//------------------
				//---------------------------------
				db.transaction(function(x) {
					x.executeSql('UPDATE prospects SET dw_delecao = ?, editado = ?, exportado = ? WHERE codigo = "' + codigo_prospect + '"', ['1','1',null], 
					function(){
						
							// Apagando pedido da sessao
							location.href="prospects_aguardando.html";
						

					});
				});
				
				//---------------------------------
				//------------------
				
				$("#confirmar_dialog").remove();
				
			}
		);
		
	});
	
	

	
	
});

function obter_descricao_condicao(condicao)
{
	if(condicao)
	{
		db.transaction(function(x) {
			x.executeSql('SELECT codigo, descricao FROM condicoes_pagamento WHERE codigo = ?', [condicao], function(x, dados){
				var dado = dados.rows.item(0);
				
				$('#condicoes_pagamento').html(dado.codigo + ' - ' + dado.descricao);
			})
		});
	}
}

function obter_prospect_orcamento(codigo_proscpet)
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM prospects WHERE codigo = ?', [codigo_proscpet], 
			function(x, dados) 
			{				
				var dado = dados.rows.item(0);
				
				//Cabeçalho pedido
				var pedido_copiado = [];
							
				pedido_copiado['filial'] 						= dado.filial;
				pedido_copiado['tipo_pedido'] 					= '*';
				pedido_copiado['cliente_prospect'] 				= 'P';
				pedido_copiado['codigo_prospect'] 				= dado.codigo;
				pedido_copiado['loja_prospect']					= dado.codigo_loja;
				pedido_copiado['descricao_prospect']			= dado.codigo + '/' + dado.codigo_loja + ' - ' + dado.nome + ' - ' + dado.cgc;
				
				sessionStorage['sessao_orcamento'] = serialize(pedido_copiado);
				window.location = "pedidos_adicionar.html#O";
			
			}
		)
	});
}