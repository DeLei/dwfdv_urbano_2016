$(document).ready(function() {
	// obter prospects
	sessionStorage['prospects_processados_ordem_atual'] = 'codigo DESC'; 
	
	obter_prospects(sessionStorage['prospects_processados_pagina_atual'], sessionStorage['prospects_processados_ordem_atual']);
	
	
	// Obter Estados
	$('select[name*=estado]').html('<option value> Selecione... </option>' + ufs);
	
	// Obter Municipios
	$('select[name=estado]').live('change', function(){
		$('select[name=municipio]').attr('disabled', 'disabled');
		$('select[name=municipio]').html('<option value> Carregando... </option>');
		
		var estado = $(this).val();
		
		obter_municipios(estado);
	});
	
	function obter_municipios(estado, municipio) {
		// obter Municipios
		db.transaction(function(x) {
			x.executeSql('SELECT DISTINCT codigo, nome FROM municipios WHERE uf = ? ORDER BY nome ASC', [estado], function(x, dados) {
              $('select[name=municipio]').empty();
              $('select[name=municipio]').removeAttr('disabled');
              $('select[name=municipio]').append('<option value> Selecione... </option>');
              if (dados.rows.length) {
                for (i = 0; i < dados.rows.length; i++) {
                  var dado = dados.rows.item(i);

                  var selected = '';

                  if(municipio == dado.codigo) {
                    selected = 'selected="selected"';
                  }

                  $('select[name=municipio]').append('<option value="' + dado.codigo + '" ' + selected + '>' + dado.nome + '</option>');
                }
              } else {
                $('select[name=municipio]').empty();
                $('select[name=municipio]').attr('disabled', 'disabled');
                $('select[name=municipio]').append('<option value> Selecione um Estado </option>');
              }
            });
		});
	}

	
	$('select[name=municipio]').live('change', function(){
		var nome_municipio = $('select[name=municipio] option:selected').html();
		$('input[name=nome_municipio]').val(nome_municipio);
	});
	
	// ordenação
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC') {
			obter_prospects(sessionStorage['prospects_processados_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		} else {
			obter_prospects(sessionStorage['prospects_processados_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['prospects_processados_' + name] != 'undefined' ? sessionStorage['prospects_processados_' + name] : '');
	});
	
	$('#filtrar').click(function() {
		sessionStorage['prospects_processados_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['prospects_processados_' + name] = $(this).val();
		});
		
		obter_prospects(sessionStorage['prospects_processados_pagina_atual'], sessionStorage['prospects_processados_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['prospects_processados_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['prospects_processados_' + name] = '';
		});
		
		obter_prospects(sessionStorage['prospects_processados_pagina_atual'], sessionStorage['prospects_processados_ordem_atual']);
	});
});

function obter_prospects(pagina, ordem) {
	
	obter_grupo_permissao(function(grupo_permissao) {
		
		pagina = pagina ? pagina : 1;
		ordem = ordem ? ordem : 'codigo DESC';
		
		// setar pagina e ordem atual
		sessionStorage['prospects_processados_pagina_atual'] 	= pagina;
		sessionStorage['prospects_processados_ordem_atual'] 	= ordem;
		
		// definir seta
		var ordem_atual = explode(' ', sessionStorage['prospects_processados_ordem_atual']);
		
		if (ordem_atual[1] == 'ASC') {
			$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'ASC');
			
			$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▲');
		} else {
			$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'DESC');
			
			$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▼');
		}
		
		// calcular offset
		var offset = (sessionStorage['prospects_processados_pagina_atual'] - 1) * 20;
		
		// gerar filtros
		
		prospects_status = sessionStorage['prospects_processados_status'];
		nome 			= sessionStorage['prospects_processados_nome'];
		nome_fantasia 	= sessionStorage['prospects_processados_nome_fantasia'];
		estado 			= sessionStorage['prospects_processados_estado'];
		municipio 		= sessionStorage['prospects_processados_municipio'];
		telefone 		= sessionStorage['prospects_processados_telefone'];
		
		var wheres = '';
		
		if (prospects_status && prospects_status != 'undefined')
		{
			wheres += " AND status = '" + prospects_status + "'";
		}
		
		if (nome && nome != 'undefined')
		{
			wheres += ' AND nome LIKE "%' + nome + '%"';
		}
		
		if (nome_fantasia && nome_fantasia != 'undefined')
		{
			wheres += ' AND nome_fantasia LIKE "%' + nome_fantasia + '%"';
		}
		
		if (estado && estado != 'undefined')
		{
			wheres += ' AND estado = "' + estado + '"';
		}
		
		if (municipio && municipio != 'undefined')
		{
			wheres += ' AND codigo_municipio = "' + municipio + '"';
		}
		
		if (telefone && telefone != 'undefined')
		{
			wheres += ' AND telefone LIKE "%' + telefone + '%"';
		}
		
		if (info.empresa)
		{
			wheres += " AND empresa = '" + info.empresa + "'";
		}
		
		
		if (grupo_permissao.grupo == 'preposto') {
			
			wheres += ' AND codigo_preposto = '+ grupo_permissao.codigo_preposto;
		}
		
		
		//Não exibir prospects processados com o Status 9	
		wheres += " AND status <> 9.0 ";
		
		
		
		db.transaction(function(x) {
		
			// Exibir Prospects
			x.executeSql('SELECT * FROM prospects_processados WHERE codigo > 0   ' + wheres + ' ORDER BY ' + sessionStorage['prospects_processados_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados){
				
				if(dados.rows.length)
				{
					$('table tbody').empty();
				
					for (i = 0; i < dados.rows.length; i++)
					{
						var item = dados.rows.item(i);
						
						var status = '';
						
						if(item.status == '1')
						{
							status = 'laranja';
						}
						else if(item.status == '5')
						{
							status = 'vermelho';
						}else if(item.status == '6')
						{
							status = 'verde';
						}
						
						var html = '';
							html += '<td align="center"><img src="img/status/' + status + '.png"></td>';
							html += '<td>'+item.nome+'</td>';
							html += '<td>'+(item.nome_fantasia ? item.nome_fantasia : 'N\A')+'</td>';
							html += '<td>'+ (item.estado ? item.estado : '' ) + (item.nome_municipio ? '/' + item.nome_municipio : 'N\A') + '</td>';
							html += '<td width="150" align="center">'+(item.telefone ? formatar_telefone(item.ddd+item.telefone) : 'N\A')+'</td>';
							html += '<td width="80" align="center">'+'<a href="prospects_processados_visualizar.html#' + item.codigo + '" class="btn btn-large btn-primary">+ Opções</a>'+'</td>';
						
						$('table tbody').append('<tr>' + html + '</tr>');
						
						alterarCabecalhoTabelaResolucao();
					}
				}
				else
				{
					$('table tbody').html('<tr><td colspan="7" style="color: #900; padding: 10px;"><strong>Nenhum registro encontrado.</strong></td></tr>');
				}
				
			});
			
			
			//Exibir totais Prospects
			
			x.executeSql(
				'SELECT COUNT(codigo) AS total FROM prospects_processados WHERE codigo > 0 ' + wheres, [], function(x, dados) {
					var dado = dados.rows.item(0);
					
					$('#total').text(number_format(dado.total, 0, ',', '.'));
					
					// paginação
					
					$('#paginacao').html('');
					
					var total = ceil(dado.total / 20);
					
					if (total > 1)
					{
						if (sessionStorage['prospects_processados_pagina_atual'] > 6)
						{
							$('#paginacao').append('<a href="#" onclick="obter_prospects(1, \'' + sessionStorage['prospects_processados_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
						}
						
						if (sessionStorage['prospects_processados_pagina_atual'] > 1)
						{
							$('#paginacao').append('<a href="#" onclick="obter_prospects(' + (intval(sessionStorage['prospects_processados_pagina_atual']) - 1) + ', \'' + sessionStorage['prospects_processados_ordem_atual'] + '\'); return false;">&lt;</a> ');
						}
						
						for (i = intval(sessionStorage['prospects_processados_pagina_atual']) - 6; i <= intval(sessionStorage['prospects_processados_pagina_atual']) + 5; i++)
						{
							if (i <= 0 || i > total)
							{
								continue;
							}
							
							if (i == sessionStorage['prospects_processados_pagina_atual'])
							{
								$('#paginacao').append('<strong>' + i + '</strong> ');
							}
							else
							{
								$('#paginacao').append('<a href="#" onclick="obter_prospects(' + i + ', \'' + sessionStorage['prospects_processados_ordem_atual'] + '\'); return false;">' + i + '</a> ');
							}
						}
						
						if (sessionStorage['prospects_processados_pagina_atual'] < total)
						{
							$('#paginacao').append('<a href="#" onclick="obter_prospects(' + (intval(sessionStorage['prospects_processados_pagina_atual']) + 1) + ', \'' + sessionStorage['prospects_processados_ordem_atual'] + '\'); return false;">&gt;</a> ');
						}
						
						if (sessionStorage['prospects_processados_pagina_atual'] <= total - 6)
						{
							$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_prospects(' + total + ', \'' + sessionStorage['prospects_processados_ordem_atual'] + '\'); return false;">Última Página</a> ');
						}
					}
				}
			);
		});
	});
}