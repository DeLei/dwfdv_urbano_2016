/**
 * Validação de descontos de acordo com a quantidade de fardos do pedido e
 * limite máximo de descontos.
 * 
 */
function validar_descontos(callback) {
	var itens_pedido = obter_produtos_sessao();
	var total_produtos = Object.keys(itens_pedido).length;
	var variacao_parametro = obter_valor_sessao('MV_VARPREJ');
	var filial = obter_valor_sessao('filial');
	var tabela_preco = obter_valor_sessao('tabela_precos');
	
	var resultados = [];
	
	obter_quantidade_fardos(itens_pedido, function(total_fardos) {
		
		$.each(itens_pedido, function(key, item) {
			var codigo_produto = item.codigo;
			var desconto = item.desconto
			var preco_venda = item.preco_venda;
			var quantidade = item.quantidade;
			var preco_tabela = item.preco_tabela;

			obter_categoria_produto(codigo_produto,	
					function(categoria_produto) {						
						validar_desconto_categoria_vs_fardos(categoria_produto,	preco_venda, preco_tabela, total_fardos, variacao_parametro, filial, tabela_preco, item,
								function(status) {
									resultados.push(status);									
									if(resultados.length == total_produtos){
										callback(resultados);
									}
									
								});
					});

		});
	});
	
	
}

/** Validação de um único item do pedido de acordo com os produtos no pedido.
 * 
 * */
function validar_desconto_item(codigo_produto, callback){
	var itens_pedido = obter_produtos_sessao();
		codigo_produto = remover_zero_esquerda(codigo_produto);
	var variacao_parametro = obter_valor_sessao('MV_VARPREJ');
	var filial = obter_valor_sessao('filial');
	var tabela_preco = obter_valor_sessao('tabela_precos');
	
	var item = itens_pedido[codigo_produto];
	
	obter_quantidade_fardos(itens_pedido, function(total_fardos) {	
			var codigo_produto = item.codigo;
			var desconto = item.desconto
			var preco_venda = item.preco_venda;
			var quantidade = item.quantidade;
			var preco_tabela = item.preco_tabela;

			obter_categoria_produto(codigo_produto,	
					function(categoria_produto) {
						validar_desconto_categoria_vs_fardos(categoria_produto,	preco_venda, preco_tabela, total_fardos, variacao_parametro, filial, tabela_preco,item,
								function(status) {
									callback(status);
									
								});
					});

		
	});
}

/**
 * validar_desconto_categoria_vs_fardos
 * 
 */
function validar_desconto_categoria_vs_fardos(codigo_categoria, preco_venda, preco_tabela, total_fardos, variacao_parametro, filial, codigo_tabela_precos, item, callback) {
	var status = {};
	
	obter_desconto_maximo_quantidade_fardos_categoria(filial, codigo_tabela_precos, codigo_categoria, total_fardos, item, 
			function(desconto_maximo_reais) {				
							
				var preco_minimo =   (preco_tabela - desconto_maximo_reais) - ( (preco_tabela - desconto_maximo_reais) * (variacao_parametro/100) );
												
				if(parseFloat(preco_venda) < parseFloat(preco_minimo)){
					status.codigo = 2;
					status.mensagem = 'está abaixo do permitido de R$ '+number_format(preco_minimo,2,',','.')+'.';
				}else{
					status.codigo = 1;
					status.mensagem = 'Sucesso.';
				}
				callback(status);		
			});
}


function obter_preco_minimo(preco_tabela, desconto_maximo_reais, variacao_parametro, converter, callback){

	
	desconto_maximo_reais = ( desconto_maximo_reais  * converter)/30;
	
	var preco_minimo =   (preco_tabela - desconto_maximo_reais) - ( (preco_tabela - desconto_maximo_reais) * (variacao_parametro/100) );
	
	callback(preco_minimo);		
}

/**
 * Obtem o valor máximo em reais de desconto por item.
 * 
 */
function obter_desconto_maximo_quantidade_fardos_categoria(filial, codigo_tabela_precos, codigo_categoria, total_fardos, item, callback) {
	var wheres = 'codigo_filial = ? AND codigo_tabela_precos = ? AND codigo_categoria = ? AND quantidade <= ?';
	

	db.transaction(function(x) {
		x.executeSql(
						'SELECT desconto_maximo_reais FROM desconto_por_fardo WHERE ' + wheres + ' ORDER BY quantidade DESC LIMIT 1',
						[ filial, codigo_tabela_precos,	codigo_categoria, total_fardos ],
						function(x, dados) {	
							var total = dados.rows.length;
							var desconto_maximo_reais = 0;
							if (total > 0) {
								desconto_maximo_reais = dados.rows.item(0).desconto_maximo_reais;
								desconto_maximo_reais = (desconto_maximo_reais  * item.converter)/30;
							}
							callback(desconto_maximo_reais);
						});
			});

}

/**
 * obter_categoria_produto
 * 
 */
function obter_categoria_produto(codigo_produto, callback) {
	
	if (obter_valor_sessao('filial')
			&& obter_valor_sessao('filial') != 'undefined') {
		var wheres = " AND (produto_filial = '" + obter_valor_sessao('filial')
				+ "' OR produto_filial = ' ')";
	}
	wheres += ' AND ptp_ativo = "1" AND ptp_data_vigencia <= "' + date('Ymd') + '"';
	db
			.transaction(function(x) {
				x
						.executeSql(
								'SELECT produto_codigo_categoria FROM produtos WHERE produto_codigo = ?' + wheres,
								[ codigo_produto ],
								function(x, dados) {

									var total = dados.rows.length;
									codigo_categoria = dados.rows.item(0).produto_codigo_categoria;
									callback(codigo_categoria);

								});
			});
}

/**
 * Função obtem o valor de fardos adicionados no pedido.
 */
function obter_quantidade_fardos(itens_pedido, callback) {
	var total_fardos = 0;
	$.each(itens_pedido, function(key, item) {
		total_fardos += parseInt(item.quantidade);
	});

	callback(total_fardos);
}


function obter_variacao_desconto(item, callback){
	
	
	
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM parametros WHERE filial = ? AND mv = ? ', [item.produto_filial,'MV_VARPREJ'], function(x, dados) {
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						callback(item,dado.valor);
					}
				}
			})
		});
	
	
}