function redirecionar(){
	window.location = 'index.html';
}


$(document).ready(function() {
	$(window).on('beforeunload', function(){
			return "\n\nATENÇÃO! \n\n";
	});
	
	
	function executarApk()
	{
		
		var apk = config.apk;
		
		var urlApk = 'file://' + localStorage.getItem('caminho_local') + apk + '.apk';
		
		window.plugins.webintent.startActivity({
	        action: window.plugins.webintent.ACTION_VIEW,
	        url: urlApk,
	        type: 'application/vnd.android.package-archive'
	        },
	        function(){
	        },
	        function(e){
	        	
	        	console.log('Error launching app update <' + urlApk +'>');
	        	
	        	mensagem('Não foi possível iniciar a aplicação de atualização.', function(){
	        	
	        		window.location = 'index.html';
	        		
	        	});
	        }
	    );     
		
	}
	
	$('#btn_executarApk').click(function(){
		
		executarApk();
		
	});
	
	
	var download = new DownloadZip(config.ws_url + 'apk/'+config.apk, function(){
		
		$('#btn_executarApk').show();
		$('#carregando h2').html('Donwload Concluído!');
		$(window).unbind('beforeunload');
		$('.barra').show();
		
		//------------------------------------------
		
	  	executarApk();
		
		//------------------------------------------
		
		
	}
	,'apk'
	, function(){
		
			confirmar('O download não foi concluído. Você deseja tentar novamente?', 
			function () {
				$(this).dialog('close');
				
				//------------------
				//---------------------------------
		
				$(window).unbind('beforeunload');
				location.href = "atualizacao.html";
				
				//---------------------------------
				//------------------
				
				$("#confirmar_dialog").remove();
			},
			function () {
				$(this).dialog('close');
				
				//------------------
				//---------------------------------
		
				$(window).unbind('beforeunload');
				location.href = "index.html";
				
				//---------------------------------
				//------------------
				
				$("#confirmar_dialog").remove();
			});
		
		
		}
		
	);
		
	
	document.addEventListener("deviceready", onDeviceReady, false);

	function onDeviceReady() {
		if(window.navigator.onLine)
		{
			download.iniciarDownload();
		}
		else
		{
			$(window).unbind('beforeunload');
			mensagem('Você não pode atualizar porque o sistema não encontrou conexão à internet.', "redirecionar()");
		}
	}
	
	
});