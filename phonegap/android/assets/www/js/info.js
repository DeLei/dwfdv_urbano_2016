var modulos = {
	empresas: {
		tabela: 'empresas',
		nome: 'Empresas'
	},
	filiais: {
		tabela: 'filiais',
		nome: 'Filiais'
	},
	prospects: {
		tabela: 'prospects',
		nome: 'Prospects'
	},
	historico_prospects: {
		tabela: 'historico_prospects',
		nome: 'Histórico de Prospects'
	},
	clientes: {
		tabela: 'clientes',
		nome: 'Clientes'
	},
	historico_clientes: {
		tabela: 'historico_clientes',
		nome: 'Histórico de Clientes'
	},
	tabelas_preco: {
		tabela: 'tabelas_preco',
		nome: 'Tabelas de Preços'
	},
	produtos: {
		tabela: 'produtos',
		nome: 'Produtos'
	},
	formas_pagamento: {
		tabela: 'formas_pagamento',
		nome: 'Formas de Pagamento'
	},
	condicoes_pagamento: {
		tabela: 'condicoes_pagamento',
		nome: 'Condições de Pagamento'
	},
	transportadoras: {
		tabela: 'transportadoras',
		nome: 'Transportadoras'
	},
	
//	CUSTOM: 177 / 000874 - 15/10/2013 - Localizar: CUST-TIPOS-TRANSPORTE
	tipos_transporte: {
		tabela: 'tipos_transporte',
		nome: 'Tipos de Transporte'
	},	
	ramos: {
		tabela: 'ramos',
		nome: 'Ramos'
	},	
	portos_destino: {
		tabela: 'portos_destino',
		nome: 'Porto Destino'
	},	
	parametros: {
		tabela: 'parametros',
		nome: 'Parâmetros'
	},	
// FIM CUSTOM: 000177 / 000870 - 07/12/2013	
	rotas: {
		tabela: 'rotas',
		nome: 'Rotas'
	},	
	zonas_setor: {
		tabela: 'zonas_setor',
		nome: 'Zonas por Setor'
	},	
	clientes_setor: {
		tabela: 'clientes_setor',
		nome: 'Clientes por Setor'
	},	
	prazos_entrega: {
		tabela: 'prazos_entrega',
		nome: 'Prazos de Entrega'
	},
	motivos_troca: {
		tabela: 'motivos_troca',
		nome: 'Motivos de Troca'
	},	
	catalogos: {
		tabela: 'catalogos',
		nome: 'Catálogos'
	},
	eventos: {
		tabela: 'eventos',
		nome: 'Eventos'
	},
	municipios: {
		tabela: 'municipios',
		nome: 'Municípios'
	},
	pedidos_processados: {
		tabela: 'pedidos_processados',
		nome: 'Pedidos Processados'
	},
	pedidos_pendentes: {
		tabela: 'pedidos_pendentes',
		nome: 'Pedidos Pendentes'
	},
	pendencias:{
		tabela: 'pendencias',
		nome: 'Pendências'
	},
	pendencias_mensagens:{
		tabela: 'pendencias_mensagens',
		nome: 'Pendências Mensagens'
	},
	pendencias_usuarios:{
		tabela: 'pendencias_usuarios',
		nome: 'Pendências Usuários'
	},
	noticias:{
		tabela: 'noticias',
		nome: 'Notícias'
	},
	notas_fiscais:{
		tabela: 'notas_fiscais',
		nome: 'Notas Fiscais'
	},
	orcamentos:{
		tabela: 'orcamentos',
		nome: 'Orçamentos'
	},
	titulos: {
		tabela: 'titulos',
		nome: 'Títulos'
	},
	regra_desconto: {
		tabela: 'regra_desconto',
		nome: 'Regras de Desconto'
	},
	informacoes_do_representante: {
		tabela: 'informacoes_do_representante',
		nome: 'Informações do Representante'
	},
	sincronizacoes: {
		tabela: 'sincronizacoes',
		nome: 'Sincronizações'
	},
	prospects_processados: {
		tabela: 'prospects_processados',
		nome: 'Prospects na Empresa'
	}
	//CUSTOM E0179/2014  - Adicionar comissoes
	,
	comissoes: {
		tabela: 'comissoes',
		nome: 'Comissões'
	}
	//CUSTOM E0178/2014  - Adicionar Recebimentos
	,
	recebimentos: {
		tabela: 'recebimentos',
		nome: 'Recebimentos'
	}
	//CUSTOM E0174/2014  - Relatório de pedidos por rota
	,
	pedidos_por_rota: {
		tabela: 'pedidos_por_rota',
		nome: 'Pedidos por Rota'
	}
	//CUSTOM E0169/2014  - Relatório de pedidos por rota
	,
	desconto_por_fardo: {
		tabela: 'desconto_por_fardo',
		nome: 'Faixa de descontos'
	},
	unidade_medidas_venda: {
		tabela: 'unidade_medidas_venda',
		nome: 'Unidade de Medidas para Venda'
	}
	
};

$(document).ready(function() {
	
	if($('.versao_apk').length > 0){
		$('.versao_apk').html(config.versao);
	}
	
	//-------------------------------------------------------
	// Verificar se existe atualização
	//-------------------------------------------------------
	$('#verificarAtualizacao').click(function(e){
		//Para link
		e.preventDefault();
		
		//Remover
		$('#atualizacao_dwfdv').remove();
		
		//SHOW
		console.log('[INICIO] - Verificar versão');
		
		//Verificar versão por ajax
		if(window.navigator.onLine)
		{
			console.log('[INICIO] - AJAX VERIFICANDO');
			
			$.ajax({
				type: 	"GET",
				url: 	config.ws_url + 'atualizacao/ultima_versao',
				data: {
						versao : config.versao,						
						id_usuario : info.id_rep,
						macAddress:  localStorage.getItem('macAddress')
					},
				success: function(data) {
			
					console.log('[INICIO] - AJAX SUCESSO');
					if(data.situacao == '001')
					{
						
						document.addEventListener("deviceready", function(){
							//Avisar que existe pendencias							
							if (typeof plugins !== "undefined") 
							{
			                    plugins.localNotification.add({
			                        date : new Date(),
			                        message : 'O sistema encontrou uma nova versão. Versão: '+ data.versao.versao,
			                        ticker : 'O sistema encontrou uma nova versão.',
			                        repeatDaily : false,
			                        id : 4
			                    });
			                    console.log('Exibir alerta depois.');
							}else{
								console.log('Não carregou os plugins.');
							}
						}, false);
					
						var html = '<div id="atualizacao_dwfdv" class="info message"><div class="clear"></div>';
							html += '	<b>Atualização de Software</b> - O sistema encontrou uma nova versão.<br /><br />';
							html += '	<p>Versão em execução atualmente: <b>' + config.versao + '</b>.<br />';
							html += '	Versão disponível para atualização: <b>' + data.versao.versao + '</b>.</p>';
							html += '	<br />';
							html += '	<p>Deseja atualizar a versão?<br /><br />';
							html += ' 	<button id="btn_atualizar" type="button" class="btn btn-large btn-success">SIM</button> ou ';
							html += ' 	<button id="btn_naoAtualizar"type="button" class="btn btn-large btn-warning">AGORA NÃO</button>';
							html += ' 	</p>';
							html += '</div>';
							
						$('#conteudo').before(html);	
					}
					else
					{
						mensagem('<b>Atualização de Software</b> - ' + data.mensagem);
					}
					
				}
			});
		}
		else
		{
			console.log('[INICIO] - SEM INTERNET');
			mensagem('Sem acesso a <strong>Internet</strong>.');
		}
	});
	//-------------------------------------------------------
	// Verificar se existe atualização
	//-------------------------------------------------------
	
	
	$.each(modulos, function (i, v) {
		db.transaction(function (x) {
			x.executeSql(
				'SELECT * FROM sincronizacoes WHERE modulo = ? ORDER BY dt_atualizado DESC, timestamp DESC LIMIT 1', [v.tabela], function(x, dados) {
					if (dados.rows.length)
					{
						var sinc = dados.rows.item(0);
						
						$('table tbody').append('<tr><td><strong>' + v.nome + '</strong></td><td>' + date('d/m/Y H:i:s', sinc.timestamp) + '</td><td>' + formatar_datahora( sinc.dt_atualizado) + '</td><td>' + (sinc.sucesso == 1 ? 'SIM' : 'NÃO') + '</td><td><div id="remover_'+v.tabela+'">Normal</div></td></tr>');
					}
					else if(v.tabela != 'sincronizacoes' && v.tabela != 'informacoes_do_representante')
					{
						$('table tbody').append('<tr><td><strong>' + v.nome + '</strong></td><td>  </td><td>  </td><td> NÃO </td><td><div id="remover_'+v.tabela+'">Cancelado</div></td></tr>');
					}
					
					alterarCabecalhoTabelaResolucao();
					
				}
			);
		});
	});
	
});