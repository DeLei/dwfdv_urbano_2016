$(document).ready(function() {
	
	/*
	$('form input, form select, form textarea').each(
		    function(index){  
		     //   var input = $(this);
		    //    console.log('Type: ' + input.attr('type') + 'Name: ' + input.attr('name') + 'Value: ' + input.val());
		    }
	);*/
	
	var validadorIE = new ValidaDocumentos();
	//resultado = (validadorIE.validaIE('01.15.241/001-22', 'AC'));
	
	//Validar CGC durante a digitação
	$('[name=cgc]').focusout(function(){
		
		var cgc = $(this).val();
		
		if(validar_cnpj(cgc)){
			$('[name=inscricao_estadual]').addClass('obrigatorio');

		}else{
			$('[name=inscricao_estadual]').removeClass('obrigatorio');
		}
		
		if(validar_cpf_cnpj(cgc)) {
			db.transaction(function(x) {
				x.executeSql('SELECT cgc, nome_fantasia FROM prospects WHERE cgc = ?', [cgc], function(x, dados) {
					if (dados.rows.length) {
						var dado = dados.rows.item(0);
						
						mensagem('O CPF/CNPJ informado, já possui um cadastro relacionado ao Prospect: <br/><b>' + dado.nome_fantasia + '</b>');
					} else {
						x.executeSql('SELECT cpf, nome_fantasia FROM clientes WHERE cpf = ?', [cgc], function(x, dados) {
							if (dados.rows.length) {
								var dado = dados.rows.item(0);
								
								mensagem('O CPF/CNPJ informado, já possui um cadastro relacionado ao Cliente: <br/><b>' + dado.nome_fantasia + '</b>');
							} else {
								x.executeSql('SELECT cgc, nome_fantasia FROM prospects_processados WHERE cgc = ?', [cgc], function(x, dados) {
									if (dados.rows.length) {
										var dado = dados.rows.item(0);
										
										mensagem('O CPF/CNPJ informado, já possui um cadastro relacionado ao Prospect: <br/><b>' + dado.nome_fantasia + '</b>');
									}
								});
							}
						});
					}
				});
			});
		} else {
			mensagem('CPF/CNPJ inválido, favor verificar.');
		}
	});

	db.transaction(function(x) {
		// obter feiras
		x.executeSql(
			'SELECT DISTINCT id, nome FROM eventos ORDER BY nome ASC', [], function(x, dados) {
				
				$('select[name=id_feira]').append('<option value="0"> Nenhum </option>');
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=id_feira]').append('<option value="' + dado.id + '">' + dado.nome + '</option>');
					}
				}
			}
		);
		
//		CUSTOM: 177 / 000874 - 22/10/2013 - Localizar: CUST-RAMOS
		//obter_ramos
		x.executeSql(
				'SELECT DISTINCT chave, descricao FROM ramos ORDER BY descricao ASC', [], function(x, dados) {
					
					$('select[name=id_ramo]').append('<option value=""> Nenhum </option>');
					if (dados.rows.length)
					{
						for (i = 0; i < dados.rows.length; i++)
						{
							var dado = dados.rows.item(i);
							
							$('select[name=id_ramo]').append('<option value="' + dado.chave + '">' + dado.descricao + '</option>');
						}
					}
				}
			);
//		FIM CUSTOM: 177 / 000874 - 22/10/2013

//		CUSTOM: 177 / 000874 - 22/10/2013 - Localizar: CUST-CONDICOES-PAGAMENTO
		//obter_CONDICOES_PAGAMENTO
		x.executeSql('SELECT codigo, descricao FROM condicoes_pagamento ORDER BY descricao ASC', [], function(x, dados) {
			$('select[name=condicoes_pagamento]').append('<option value="0"> Nenhum </option>');
			if (dados.rows.length)
			{
				for (i = 0; i < dados.rows.length; i++)
				{
					var dado = dados.rows.item(i);
					
					$('select[name=condicoes_pagamento]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
				}
			}
		});
//		FIM CUSTOM: 177 / 000874 - 22/10/2013
		
//		CUSTOM: 177 / 000874 - 15/10/2013	- Localizar: CUST-FILIAL-PROSPECT	
//		Obter Filiais
//		x.executeSql(
//			'SELECT DISTINCT codigo, razao_social FROM filiais ORDER BY codigo ASC', [], function(x, dados) {
//		
//				$('select[name=filial]').append('<option value> Selecione... </option>');
//				if (dados.rows.length)
//				{
//					for (i = 0; i < dados.rows.length; i++)
//					{
//						var dado = dados.rows.item(i);
//
//						$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
//					}
//				}
//			}
//		);
//	FIM CUSTOM: 177 / 000874 - 15/10/2013

		
	});
	

//	CUSTOM: 177 / 000874 - 22/10/2013 - Localizar: CUST-BANCO
	// Obter bancos
	$('select[name=banco_cobranca]').html('<option value> Selecione... </option>' + bancos_cobranca);	
	$("select[name=banco_cobranca] option[value='85']").remove();
	//002388 - Remover Banco Prospect
	$("select[name=banco_cobranca] option[value='84']").remove();
	// FIM 002388 - Remover Banco Prospect
	//	FIM CUSTOM: 177 / 000874 - 22/10/2013
	
	//Cancelar
	$('#cancelar').click(function() {
		
			//-----------------
					
					document.location = 'prospects.html';			
				
		
		
	});
	
	// Obter Estados
	$('select[name*=estado]').html('<option value> Selecione... </option>' + ufs);
	
	
	// Obter Municipios
	
	$('select[name=estado]').live('change', function(){
		
		$('#nome_municipio').attr('disabled', 'disabled');
		$('#nome_municipio').html('Carregando...');
		
		limpar_municipio();
		
		var estado = $(this).val();
		
		// obter Municipios
		db.transaction(function(x) {
			x.executeSql('SELECT DISTINCT codigo, nome FROM municipios WHERE uf = ? ORDER BY nome ASC', [estado], function(x, dados) {
				var municipios = [];
				
				for (i = 0; i < dados.rows.length; i++)
				{
					var dado = dados.rows.item(i);
					
					municipios.push({ label: dado.nome, codigo: dado.codigo });
				}
				
				buscar_municipios(municipios)
			});
		});
		
	});
	
	$('#trocar_municipio').click(function(){
		limpar_municipio();
	});
	
	function limpar_municipio()
	{
		$('[name=codigo_municipio]').val('');
		$('#nome_municipio').val('');
		$('#nome_municipio').removeAttr('disabled');
		$('#trocar_municipio').hide();
	
		if($('[name=estado]').val() > 0){
			$('#nome_municipio').focus();
		}
	}
	
	function buscar_municipios(municipios)
	{
		$('input[name=nome_municipio]').autocomplete({
			minLength: 2,
			source: municipios,
			position : { my : "left bottom", at: "left top", collision : "none"},
			select: function( event, ui ) {
				$('[name=codigo_municipio]').val(ui.item.codigo);
				$('#nome_municipio').val(ui.item.label);
				
				$('#nome_municipio').attr('disabled', 'disabled');
				$('#trocar_municipio').show();
				return false;
			}
		});
	}
	
	
	$('select[name=tipo]').live('change', function(){
		
		if($(this).val() == 'L')
		{
			$('input[name=inscricao_rural]').addClass('obrigatorio');
		}
		else
		{
			$('input[name=inscricao_rural]').removeClass('obrigatorio');
		}
	});
	
	
	// enviar form
	
	$('form').submit(function() {
		var validadorIE = new ValidaDocumentos();
	
//		CUSTOM: 177 / 000874 - 15/10/2013 - Localizar: CUST-FILIAL-PROSPECT
//		if (!$('select[name=filial]').val())
//		{
//			mensagem('Selecione uma <strong>Filial</strong>.');
//		}
//		else
//		FIM CUSTOM: 177 / 000874 - 15/10/2013

		
		prospect_cadastrado($('input[name=cgc]').val(), function(cgc_valido, status_mensagem){
			
			
			
			
		
		
		
			if (!$('input[name=tipo]').val())
			{
				mensagem('Selecione um <strong>Tipo</strong> de Prospect.',"$('input[name=tipo]').focus()");
			}
			// CUSTOM: 177 / 000874 - 22/10/2013 - Localizar: CUST-RAMOS
	
			else if (!$('select[name=id_ramo]').val())
			{
				mensagem('Preencha o campo <strong>Ramo</strong>.',"$('select[name=id_ramo]').focus()");
			}
			// FIM CUSTOM: 177 / 000874 - 22/10/2013
			else if (!$('input[name=nome]').val())
			{
				mensagem('Preencha o campo <strong>Nome / Razão social</strong>.',"$('input[name=nome]').focus()" );
			}
			else if (!$('input[name=nome_fantasia]').val())
			{
				mensagem('Preencha o campo <strong>Nome Fantasia</strong>.',"$('input[name=nome_fantasia]').focus()");
			}
			else if (!$('input[name=cgc]').val() || !validar_cpf_cnpj($('input[name=cgc]').val()))
			{
				mensagem('Preencha um <strong>CPF / CNPJ</strong> válido.',"$('input[name=cgc]').focus()");
			}
			else if (!cgc_valido)
			{
				mensagem(status_mensagem,"$('input[name=cgc]').focus()");
			}			
			else if ($('select[name=tipo]').val() == 'L' && !$('input[name=inscricao_rural]').val())
			{
				mensagem('Preencha o campo <strong>Inscrição Rural</strong>.',"$('select[name=tipo]').focus()");
			}
			//Chamados: 002230 - Dt Entrega, 002162 - Data Entrega
			else if ($('input[name=data_nascimento]').val().length > 0    && ( !isValidDate($('input[name=data_nascimento]').val()) || $('input[name=data_nascimento]').val().length != 10  ) )
			{
				mensagem('<strong>Data de fundação</strong> inválida.',"$('input[name=data_nascimento]').focus()");
			}
			
			// 002389 - Prospect DDD
			
			else if (!$('input[name=ddd]').val())
			{
				mensagem('Preencha o campo <strong>DDD</strong>.',"$('input[name=ddd]').focus()");
			}
			else if (!validar_ddd($('input[name=ddd]').val()))
			{
				mensagem('Preencha um <strong>DDD</strong> válido.',"$('input[name=ddd]').focus()");
			}
			// FIM 002389 - Prospect DDD
			
			
			else if (!$('input[name=telefone]').val())
			{
				mensagem('Preencha o campo <strong>Telefone</strong>.',"$('input[name=telefone]').focus()");
			}
			else if (!validar_telefone($('input[name=telefone]').val()))
			{
				mensagem('Preencha um <strong>Telefone</strong> válido.',"$('input[name=telefone]').focus()");
			}
			else if ($('input[name=fax]').val() && !validar_telefone($('input[name=fax]').val()))
			{
				mensagem('Preencha um <strong>Fax</strong> válido.',"$('input[name=fax]').focus()");
			}
			else if (!validar_email($('input[name=email]').val()))
			{
				mensagem('Preencha um <strong>E-mail</strong> válido.',"$('input[name=email]').focus()");
				
			}
			else if (!validar_email($('input[name=email_fiscal]').val()))
			{
				mensagem('Preencha um <strong>E-mail NF-e</strong> válido.',"$('input[name=email_fiscal]').focus()");
			}
			else if (!validar_cep($('input[name=cep]').val()))
			{
				mensagem('Digite um CEP válido.',"$('input[name=cep]').focus()");
			}
			else if (!$('input[name=endereco]').val())
			{
				mensagem('Preencha um <strong>Endereço</strong> válido.',"$('input[name=endereco]').focus()");
			}
			else if (!$('input[name=bairro]').val())
			{
				mensagem('Preencha um <strong>Bairro</strong> válido.',"$('input[name=bairro]').focus()");
			}
			else if (!$('select[name=estado]').val())
			{
				mensagem('Preencha um <strong>Estado</strong> válido.',"$('select[name=estado]').focus()");
			}
			//Custom  - Validação da Inscrição Estadual
			else if (validar_cnpj($('input[name=cgc]').val()) && !$('input[name=inscricao_estadual]').val() ){
				mensagem('Preencha uma <strong>Inscrição Estadual</strong> válida.',"$('input[name=inscricao_estadual]').focus()");
			}
			else if (validar_cnpj($('input[name=cgc]').val()) &&  !(validadorIE.validaIE($('input[name=inscricao_estadual]').val(),$('select[name=estado]').val() ))   ){
				mensagem('Digite uma <strong>Inscrição Estadual</strong> válida.',"$('input[name=inscricao_estadual]').focus()");
			}
			//Fim Custom - Validação Inscrição Estadual
			else if (!$('input[name=codigo_municipio]').val())
			{
				mensagem('Preencha um <strong>Município</strong> válido.',"$('select[name=codigo_municipio]').focus()");
			}
			// CUSTOM 177 - 000874 - 21/10/2013 - Localizar: CUST-INFORMACOES-FINANCEIRAS-PROSPECT
			//CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if ($('input[name=faturamento]').val().length > 0 && !validar_campo_numerico($('input[name=faturamento]').val()))			
			{
				mensagem('Preencha o <strong>Faturamento </strong> válido.',"$('input[name=faturamento').focus()");
			}
			//FIM CHAMADO 001953 
			else if (!$('input[name=faturamento_anual]').val())
			{
				mensagem('Preencha o <strong>Faturamento Anual</strong> válido.',"$('input[name=faturamento_anual]').focus()");
			}
			//CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!validar_campo_numerico($('input[name=faturamento_anual]').val()))
			{
				mensagem('Preencha o <strong>Faturamento Anual</strong> válido.',"$('input[name=faturamento_anual]').focus()");
			}
			else if($('input[name=faturamento_anual]').val() <= 0 ){
				mensagem('<strong>Faturamento Anual</strong> deve ser maior que 0.',"$('input[name=faturamento_anual]').focus()");
			}
			//FIM CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!$('input[name=ano_fat_atual]').val() || ($('input[name=ano_fat_atual]').val().length < 4 || $('input[name=ano_fat_atual]').val() < date('Y', strtotime('-20 years')) || $('input[name=ano_fat_atual]').val() > date('Y')))
			{
				mensagem('Preencha o <strong>Ano Faturamento Atual</strong> válido.',"$('input[name=ano_fat_atual]').focus()");
			}
			else if($('input[name=ano_fat_atual]').val() <= 0 ){
				mensagem('<strong>Ano Faturamento Atual</strong> deve ser maior que 0.',"$('input[name=ano_fat_atual]').focus()");
			}
			else if (!$('input[name=sugestao_lim_credito]').val())
			{
				mensagem('Preencha a <strong>Sugestão de Limite de Crédito</strong> válido.', "$('input[name=sugestao_lim_credito]').focus()");
			}
			//CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!validar_campo_numerico($('input[name=sugestao_lim_credito]').val()))
			{
				mensagem('Preencha a <strong>Sugestão de Limite de Crédito</strong> válido.', "$('input[name=sugestao_lim_credito]').focus()");
			}
			else if($('input[name=sugestao_lim_credito]').val() <= 0 ){
				mensagem('<strong>Sugestão de Limite de Crédito</strong> deve ser maior que 0.',"$('input[name=sugestao_lim_credito]').focus()");
			}	
			//FIM CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!$('select[name=banco_cobranca]').val())
			{
				mensagem('Preencha o <strong>Banco Cobrança</strong> válido.', "$('select[name=banco_cobranca]').focus()");
			}
	//		 FIM CUSTOM 177 - 000874 - 21/10/2013
	//		 CUSTOM 177 - 000874 - 21/10/2013 - Localizar: CUST-OUTRAS-INFORMACOES-PROSPECT
			else if (!$('input[name=area_loja]').val())
			{
				mensagem('Preencha a <strong>Área da Loja (m²)</strong> válido.',"$('input[name=area_loja]').focus()");
			}
			//CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if (!validar_campo_numerico($('input[name=area_loja]').val()))
			{
					mensagem('Preencha a <strong>Área da Loja (m²)</strong> válido.',"$('input[name=area_loja]').focus()");
			}
			//FIM CHAMADO 001953 - Possibilitar o cadastro apenas de números válidos
			else if($('input[name=area_loja]').val() <= 0 ){
				mensagem('<strong>Área da Loja (m²)</strong> deve ser maior que 0.',"$('input[name=area_loja]').focus()");
			}		
			else if (!$('input[name=numero_lojas]').val()  || !validar_campo_numerico($('input[name=numero_lojas]').val()))
			{
				mensagem('Preencha a <strong>Número de Lojas</strong> válido.',"$('input[name=numero_lojas]').focus()");
			}		
			else if($('input[name=numero_lojas]').val() <= 0 ){
				mensagem('<strong>Número de Lojas</strong> deve ser maior que 0.',"$('input[name=numero_lojas]').focus()");
			}		
			else if (!$('input[name=numero_funcionarios]').val() || !validar_campo_numerico($('input[name=numero_funcionarios]').val()) )
			{
				mensagem('Preencha a <strong>Número de Funcionarios</strong> válido.',"$('input[name=numero_funcionarios]').focus()");
			}
			else if($('input[name=numero_funcionarios]').val() <= 0 ){
				mensagem('<strong>Número de Funcionarios</strong> deve ser maior que 0.',"$('input[name=numero_funcionarios]').focus()");
			}
			else if (!$('input[name=numero_veiculos]').val()  || !validar_campo_numerico($('input[name=numero_veiculos]').val()))
			{
				mensagem('Preencha a <strong>Número de Veículos</strong> válido.', "$('input[name=numero_veiculos]').focus()");
			}
			else if($('input[name=numero_veiculos]').val() <= 0 ){
				mensagem('<strong>Número de Veículos</strong> deve ser maior que 0.',"$('input[name=numero_veiculos]').focus()");
			}
			else if (!$('select[name=imovel_proprio]').val())
			{
				mensagem('Preencha o <strong>Imóvel Próprio</strong> válido.', "$('select[name=imovel_proprio]').focus()");
			}
			else if (!$('input[name=socio_1]').val())
			{
				mensagem('Preencha o <strong>Sócio 1</strong> válido.', "$('input[name=socio_1]').focus()");
			}
			else if (!$('input[name=cpf_socio_1]').val() || !validar_cpf_cnpj($('input[name=cpf_socio_1]').val()))
			{
				mensagem('Preencha o <strong>CPF Sócio 1</strong> válido.', "$('input[name=cpf_socio_1]').focus()");
			}
			else if($('input[name=cpf_socio_2]').val() && !validar_cpf_cnpj($('input[name=cpf_socio_2]').val()))
			{
				mensagem('Preencha o <strong>CPF Sócio 2</strong> válido.', "$('input[name=cpf_socio_2]').focus()");
			}
			else if (!$('input[name=responsavel_compra]').val())
			{
				mensagem('Preencha o <strong>Reponsável pela Compra</strong> válido.', "$('input[name=responsavel_compra]').focus()");
			}
			else if (!$('input[name=resp_contas_pagar]').val())
			{
				mensagem('Preencha o <strong>Reponsável pelo Contas à Pagar</strong> válido.', "$('input[name=resp_contas_pagar]').focus()");
			}
			else if (!$('select[name=condicoes_pagamento]').val())
			{
				mensagem('Preencha uma <strong>Condição de Pagamento</strong> válida.',"$('select[name=condicoes_pagamento]').focus()");
			}
			else if (!$('textarea[name=observacao]').val())
			{
				mensagem('Preencha o <strong>Observação</strong>.', "$('textarea[name=observacao]').focus()");
			}
			
	//		 FIM CUSTOM 177 - 000874 - 21/10/2013
			else
			{
				// -----------
				// Isentos
				if (!$('input[name=inscricao_estadual]').val()){
					$('input[name=inscricao_estadual]').val('ISENTO');
				}
				
				if (!$('input[name=inscricao_municipal]').val()){
					$('input[name=inscricao_municipal]').val('ISENTO');
				}
				
				if (!$('input[name=inscricao_rural]').val()){
					$('input[name=inscricao_rural]').val('ISENTO');
				}
				// Isentos
				// -----------
				
				
				
				
				
				db.transaction(function(x) {
					x.executeSql(
						'SELECT codigo FROM prospects WHERE cgc = ?', [$('input[name=cgc]').val()], function(x, dados) {
							if ($('input[name=cgc]').val() && dados.rows.length)
							{
								mensagem('O <strong>CPF / CNPJ</strong> digitado já existe.');
							}
							else
							{
								x.executeSql(
									'SELECT codigo FROM prospects ORDER BY codigo DESC LIMIT 1', [], function(x, dados) {
								
										var codigo = uniqid();
	
										
										var tmp_1 = ['codigo', 'codigo_representante'];
										var tmp_2 = ['?', '?'];
										var tmp_3 = [codigo, info.cod_rep];
										var telefone = '';
										
										$('input[name], textarea[name], select[name]').each(function() {
											var campo_nome 	= $(this).attr('name');
											var campo_valor = $(this).val();
											
											
											if(campo_nome == 'data_nascimento')
											{
												if(campo_valor)
												{
													campo_valor = data_normal2protheus(campo_valor);
												}
											}else if(campo_nome == 'sugestao_lim_credito'){
												campo_valor = obter_numero(campo_valor);
											}
	
											
											tmp_1.push(campo_nome);
											tmp_2.push('?');
											tmp_3.push(campo_valor);
										});
										
	//									CUSTOM: 177/ 000874 - 15/10/2013 : Localizar: CUST-FILIAL-PROSPECT 				
										tmp_1.push('filial');
										tmp_2.push('?');
										tmp_3.push('');
	//									FIM CUSTOM: 177 / 000874 - 15/10/2013
										
										
										tmp_1.push('data_emissao');
										tmp_2.push('?');
										tmp_3.push(date('Ymd'));
										
										tmp_1.push('time_emissao');
										tmp_2.push('?');
										tmp_3.push(time());
										
										tmp_1.push('empresa');
										tmp_2.push('?');
										tmp_3.push(info.empresa);
										
										tmp_1.push('codigo_empresa');
										tmp_2.push('?');
										tmp_3.push(info.empresa);
										
										tmp_1.push('empresa');
										tmp_2.push('?');
										tmp_3.push(info.empresa);
										
										tmp_1.push('latitude');
										tmp_2.push('?');
										tmp_3.push(localStorage.getItem('gps_latitude'));
										
										tmp_1.push('longitude');
										tmp_2.push('?');
										tmp_3.push(localStorage.getItem('gps_longitude'));
										
										tmp_1.push('versao');
										tmp_2.push('?');
										tmp_3.push(config.versao);
										
										//CUSTOM 20140610 - Reanálise dos prospects rejeitados
										tmp_1.push('acao');
										tmp_2.push('?');
										tmp_3.push('I');
										// FIM CUSTOM 20140610 
										
										//CUSTOM Preposto
									
										if(!!localStorage.getItem('codigo_preposto') &&  localStorage.getItem('codigo_preposto') != 'null'  ){
											
											tmp_1.push('codigo_preposto');
											tmp_2.push('?');
											tmp_3.push(localStorage.getItem('codigo_preposto'));
																					
											tmp_1.push('status');
											tmp_2.push('?');
											tmp_3.push('P');
										
										}else{
											tmp_1.push('status');
											tmp_2.push('?');
											tmp_3.push('L');
										}
										
										x.executeSql('INSERT INTO prospects (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')', tmp_3);
									
										console.log('SQL Inclusão do prospect: ');
										
										console.log('INSERT INTO prospects (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')');
										console.log(' - Dados: ');
										console.log(tmp_3);
										
										
										
										window.location = 'prospects_visualizar.html#' + codigo+'|true';
									}
								);
							}
						}
					);
				});
			}
		
		});
		
		return false;
	});
	
	
	
	
	
	$('input[type=text], textarea').keyup(function(){
		$(this).val($(this).val().toUpperCase());
	});
});

function obter_numero(valor){
	var resultado = trim(valor);	
	if( is_numeric(resultado)){
			return resultado;
	}else{
		return 0;
	}		
}



function prospect_cadastrado(cgc, callback){
	
	if(validar_cpf_cnpj(cgc)) {
		db.transaction(function(x) {
			x.executeSql('SELECT cgc, nome_fantasia FROM prospects WHERE cgc = ?', [cgc], function(x, dados) {
				if (dados.rows.length) {
					var dado = dados.rows.item(0);
					
					
				
					callback(false, 'O CPF/CNPJ informado, já possui um cadastro relacionado ao Prospect: <br/><b>' + dado.nome_fantasia + '</b>');
				} else {
					x.executeSql('SELECT cpf, nome_fantasia FROM clientes WHERE cpf = ?', [cgc], function(x, dados) {
						if (dados.rows.length) {
							var dado = dados.rows.item(0);
							
							
							callback(false,'O CPF/CNPJ informado, já possui um cadastro relacionado ao Cliente: <br/><b>' + dado.nome_fantasia + '</b>');
						} else {
							x.executeSql('SELECT cgc, nome_fantasia FROM prospects_processados WHERE cgc = ?', [cgc], function(x, dados) {
								if (dados.rows.length) {
									var dado = dados.rows.item(0);
									
									
									callback(false,'O CPF/CNPJ informado, já possui um cadastro relacionado ao Prospect: <br/><b>' + dado.nome_fantasia + '</b>');
								}else{
									callback(true,'Sucesso.');
								}
							});
						}
					});
				}
			});
		});
	}
	
}


